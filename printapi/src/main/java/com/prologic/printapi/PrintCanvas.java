package com.prologic.printapi;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HtetAung on 9/25/17.
 */

public class PrintCanvas {

    public enum PAPERSIZE  {_58MM,_80MM};
    public enum ALIGN {CENTER,LEFT,RIGHT}

    private String tag="Print Canvas :";
    private PAPERSIZE papersize;
    private int width=0;
    private int currentX;
    private int currentY=50;
    private int trackingY=50;
    private int tempY=50;
    private Bitmap bitmap;
    private Paint paintC;
    private Paint paintL;
    private Paint paintR;
    private Paint paintDot;

    private Canvas canvas;
    private int textSize=20;
    private int lineSize=3;
    private int yGap=10;
    private int xGap=5;
    private Typeface typeface;


    /**
     * default constructor with paper size = 80 mm,no font,text size=30
     */

    public PrintCanvas() {
        papersize= PAPERSIZE._80MM;
        this.bitmap=createBitmapToDraw(500);
        preparingPaintToDraw();
        canvas=new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
    }

    /**
     *
     * @param papersize define paper size of slip printer either 58 mm or 80 mm
     *                  Default is 80 mm
     * @param typeface custom font
     */
    public PrintCanvas(@NonNull PAPERSIZE papersize, Typeface typeface, int textSize) {
        this.typeface=typeface;
        this.papersize=papersize;
        this.textSize=textSize;
        this.bitmap=createBitmapToDraw(500);
        preparingPaintToDraw();
        canvas=new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
    }
    /**
     *
     * @param papersize define paper size of slip printer either 58 mm or 80 mm
     *                  Default is 80 mm
     */
    public PrintCanvas(@NonNull PAPERSIZE papersize) {
        this.papersize = papersize;
        this.bitmap=createBitmapToDraw(500);
        preparingPaintToDraw();
        canvas=new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
    }

    /**
     * creating bitmap according to paper size
     * @param heigh can change according to contents but width is fix bcoz of paper size
     */
    private Bitmap createBitmapToDraw(@NonNull int heigh){
        if(papersize== PAPERSIZE._80MM){
            width=600;
            return Bitmap.createBitmap(600,heigh, Bitmap.Config.ARGB_8888);
        }else if(papersize== PAPERSIZE._58MM){
            width=400;
            return Bitmap.createBitmap(400,heigh, Bitmap.Config.ARGB_8888);
        }else {
            Log.e(tag,"Unsupported paper size");
        }
        return null;
    }

    /**
     * preparing text align to draw on canvas
     */
    private void preparingPaintToDraw(){
        //for Center Align
        paintC = new Paint();
        paintC.setColor(Color.BLACK);
        paintC.setTypeface(typeface);
        paintC.setTextSize(textSize);
        paintC.setStyle(Paint.Style.FILL);
        paintC.setTextAlign(Paint.Align.CENTER);

        //for Right Align
        paintR = new Paint();
        paintR.setColor(Color.BLACK);
        paintR.setTextSize(textSize);
        paintR.setTypeface(typeface);
        paintR.setStyle(Paint.Style.FILL);
        paintR.setTextAlign(Paint.Align.RIGHT);

        //for Left Align
        paintL = new Paint();
        paintL.setColor(Color.BLACK);
        paintL.setStyle(Paint.Style.FILL);
        paintL.setTextSize(textSize);
        paintL.setTypeface(typeface);
        paintL.setTextAlign(Paint.Align.LEFT);

    }

    /**
     *
     * @param alignPaper LEFT,RIGHT OR CENTER in paper
     * @param text to be written
     * @param containerWidth the width of the text container in percentage depends on paper size
     * @param textAlign text alignment in text container
     */
    public  void writeText(ALIGN alignPaper,String text,ALIGN textAlign,int containerWidth){
        trackingY=currentY;
        if(alignPaper== ALIGN.CENTER){
            currentX=width/2;
            calculateXPlaceForCenterPaper(textAlign,calculatePercentage(containerWidth));
            writeTextAlign(textAlign,text,currentX,calculatePercentage(containerWidth));
            //writeTextCenter(text,calculatePercentage(containerWidth),currentX);
        }else if(alignPaper== ALIGN.RIGHT){
            currentX=width;
            calculateXPlaceForRightPaper(textAlign,calculatePercentage(containerWidth));
            writeTextAlign(textAlign,text,currentX,calculatePercentage(containerWidth));
        }else if(alignPaper== ALIGN.LEFT) {
            currentX=xGap;
            calculateXPlaceForLeftPaper(textAlign,calculatePercentage(containerWidth));
            writeTextAlign(textAlign,text,currentX,calculatePercentage(containerWidth));
        }

    }

    private void calculateXPlaceForCenterPaper(ALIGN align,int containerWidth){
        if(align== ALIGN.CENTER){

            //nothing need to do

        }else if(align== ALIGN.RIGHT){

            currentX=currentX-20+containerWidth/2;



        }else if(align== ALIGN.LEFT){

            currentX=currentX+20-containerWidth/2;
        }
    }

    private void calculateXPlaceForLeftPaper(ALIGN align,int containerWidth){
        if(align== ALIGN.CENTER){

            //nothing need to do

            currentX+=containerWidth/2;

        }else if(align== ALIGN.RIGHT){

            currentX=currentX-20+containerWidth;



        }else if(align== ALIGN.LEFT){

            //currentX=currentX+20-containerWidth/2;
        }
    }

    private void calculateXPlaceForRightPaper(ALIGN align,int containerWidth){
        if(align== ALIGN.CENTER){

            //nothing need to do

            currentX-=containerWidth/2;

        }else if(align== ALIGN.RIGHT){

            //currentX=currentX-20+containerWidth;

        }else if(align== ALIGN.LEFT){

            currentX=currentX+20-containerWidth;
        }
    }

    public int calculatePercentage(int containerWidth){

        double i=containerWidth/100.0;
        return (int)(i*width);
    }

    public void writeTextAlign(ALIGN textAlign,String text,int x,int width){
        if(textAlign== ALIGN.CENTER){
            writeTextCenter(text,width,x);
        }else if(textAlign== ALIGN.RIGHT){
            writeTextRight(text,width,x);
        }else if(textAlign== ALIGN.LEFT) {
            writeTextLeft(text,width,x);
        }

    }

    private void writeTextCenter(String text,int width,int x){

        Log.e(tag,"Width in No R" +width);

        writeCenterRecursive(text,width,x);



    }

    private void writeTextLeft(String text,int width,int x){

        writeLeftRecursive(text,width,x);

    }



    private void writeTextRight(String text,int width,int x){

        writeRightRecursive(text,width,x);

    }

    private void writeCenterRecursive(String text,int width,int x){

        int textCount=getCapableTextCount(width);
        Log.e(tag,"Txt count "+textCount);
        Log.e(tag,"Width :"+width);
        Log.e(tag,"Text :"+text);
        if(textCount>text.length()){
            textCount=text.length();
        }
        if(paintC.measureText(text)>width){

            //draw the first part of the string

            writeTextLn(ALIGN.CENTER,text.substring(0,textCount),x,tempY);

            //call this function again to draw the rest part of the string  
            writeCenterRecursive(text.substring(textCount,text.length()),width,x);

        }else {

            writeTextLn(ALIGN.CENTER,text,x,tempY);
        }

    }


    private void writeLeftRecursive(String text,int width,int x){

        int textCount=getCapableTextCount(width);

        if(textCount>text.length()){
            textCount=text.length();
        }

        if(paintL.measureText(text)>width){

            //draw the first part of the string

            writeTextLn(ALIGN.LEFT,text.substring(0,textCount),x,tempY);

            //call this function again to draw the rest part of the string  
            writeLeftRecursive(text.substring(textCount,text.length()),width,x);


        }else {
            writeTextLn(ALIGN.LEFT,text,x,tempY);
        }

    }

    private void writeRightRecursive(String text,int width ,int x){

        int textCount=getCapableTextCount(width);

        if(textCount>text.length()){
            textCount=text.length();
        }

        if(paintR.measureText(text)>width){

            //draw the first part of the string

            writeTextLn(ALIGN.RIGHT,text.substring(0,textCount),x,tempY);

            //call this function again to draw the rest part of the string  
            writeRightRecursive(text.substring(textCount,text.length()),width,x);

        }else {
            writeTextLn(ALIGN.RIGHT,text,x,tempY);

        }
    }

    private void writeTextLn(ALIGN align, String text, int x, int y){

        y+=textSize+yGap;
        increaseY(textSize);

        Log.e(tag,text);

        if(align== ALIGN.CENTER){

            canvas.drawText(text,x,y,paintC);

        }else if(align== ALIGN.LEFT){

            canvas.drawText(text,x,y,paintL);

        }else if(align== ALIGN.RIGHT){

            canvas.drawText(text,x,y,paintR);

        }

        //increase the Y to ready to draw next 

    }

    private void writeText(ALIGN align, String text, int x, int y){

        if(align== ALIGN.CENTER){

            canvas.drawText(text,x,y,paintC);

        }else if(align== ALIGN.LEFT){

            canvas.drawText(text,x,y,paintL);

        }else if(align== ALIGN.RIGHT){

            canvas.drawText(text,x,y,paintR);

        }

        //increase the Y to ready to draw next 
    }



    /**
     *
     * @param width the width of text container
     * @return  textCount that fit in @width
     */
    private int getCapableTextCount(int width){
        return (width/15);
    }

    public Bitmap getBitmap() {

        return bitmap;
    }
    public byte[] getPrintableImage() {

        PrintImage printImage=new PrintImage(bitmap);

        return printImage.getPrintImageData();
    }
    public void drawWaterMarkDiagonal(){
        Paint paint=new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(90);
        paint.setStyle(Paint.Style.STROKE);
        String rotatedtext = "P.A.I.D";

//Draw bounding rect before rotating text:
        Rect rect = new Rect();
        paint.getTextBounds(rotatedtext, 0, rotatedtext.length(), rect);

        int x = width/2-(int)(paint.measureText(rotatedtext)/2);
        int y = bitmap.getHeight()/2;
        canvas.translate(x,y);
        canvas.translate(-x,-y);

        canvas.rotate(-45, x + rect.exactCenterX(),y + rect.exactCenterY());
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2f);
        canvas.drawText(rotatedtext, x, y, paint);

    }


    private void increaseY(int heigh){

        tempY+=yGap+heigh;
        currentX=20;
        measureY();
        if(tempY>=bitmap.getHeight()-100){
            increaseBitmapHeigh();
        }
    }

    private void measureY(){

        if(tempY>currentY){
            currentY=tempY;
        }
    }


    private void increaseBitmapHeigh(){
        Bitmap temp=bitmap;
        bitmap=createBitmapToDraw(bitmap.getHeight()+200);
        canvas=new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(temp,0,0,new Paint());
        Log.e(tag,"Increasing Bitmap ");
    }



    public void writeRow(Row row){

        trackingY=currentY;
        for(Column c:row.columnList){
            tempY=trackingY;
            currentX=c.x;
            Log.e("WIDTH Column",c.getColumnWidth()+" ");
            writeTextAlign(c.align,c.text,c.x,c.getColumnWidth());

        }
        tempY=currentY;
    }

    public void writeLine(){

        Log.w("LINE ",currentY+" Before increase before draw");

        increaseY(lineSize+yGap);

        Log.w("LINE ",currentY+" After  increase before draw");
        Paint paint=new Paint();
        paint.setColor(Color.BLACK);
        //canvas.drawLine(0,currentY,width,currentY,paint);
        Log.w("LINE ",currentY+" Before  increase After draw");
        for(int x=0;x<width;x+=10){
            canvas.drawLine(x,currentY,x+5,currentY,paint);
        }
        increaseY(lineSize);

        Log.w("LINE ",currentY+" After  increase After draw");



    }

    public void nextLine(){
        increaseY(textSize);
    }

    public class Row{
        List<Column> columnList;
        int rowLength;
        int currentRowX;

        public Row() {
            this.rowLength=0;
            this.currentRowX=xGap;
            this.columnList = new ArrayList<>();
        }

        public void addColumn(Column column){
            columnList.add(column);
            rowLength+=column.columnWidthPercentage;
            calculateX(column);
            if(rowLength>100){
                Log.w(tag,"Row width is heigher than the prinable size!!");
            }
        }

        private void calculateX(Column column){

            if(column.align== ALIGN.CENTER){
                column.x=currentRowX+column.getColumnWidth()/2;
                currentRowX+=column.getColumnWidth();
            }else if(column.align== ALIGN.RIGHT){
                column.x=currentRowX+column.getColumnWidth();
                currentRowX+=column.getColumnWidth();
            }else if(column.align== ALIGN.LEFT){
                column.x=currentRowX;
                currentRowX+=column.getColumnWidth();
            }

        }

    }

    public class Column{
        int columnWidthPercentage;
        String text;
        ALIGN align;
        int x;

        public Column(int columnWidthPercentage, String text, ALIGN align) {
            this.columnWidthPercentage = columnWidthPercentage;
            this.text = text;
            this.align = align;
        }

        int getColumnWidth(){

            return  calculatePercentage(columnWidthPercentage)-20;

        }

    }

    public void setTextSize(int textSize){
        this.textSize=textSize;
    }

}
