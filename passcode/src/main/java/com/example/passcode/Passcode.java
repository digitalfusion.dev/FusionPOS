package com.example.passcode;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.passcode.interfaces.ChangeUserClickListener;
import com.example.passcode.interfaces.PasscodeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 10/16/17.
 */

public class Passcode extends RelativeLayout implements View.OnClickListener {

    private PasscodeListener passcodeListener;
    private ChangeUserClickListener changeUserClickListener;

    private String passcode="";
    private int length=4;
    private boolean isNewPasscode;

    private TextView infoTextView;
    private TextView forgetPasswordTextView;
    private View passcodeLayout;
    private List<TextView> pascodeViews=new ArrayList<>();

    public Passcode(Context context) {
        super(context);
        init(context);
    }

    public Passcode(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.Passcode, 0, 0);
        try {
            isNewPasscode = ta.getBoolean(R.styleable.Passcode_new_password_layout, false);
            Log.e("NewPasscode", isNewPasscode +"");
        } finally {
            ta.recycle();
        }
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public Passcode(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }


    private void init(Context context){
        Log.e("NewPasscode again", isNewPasscode +"");
        if (isNewPasscode) {
            passcodeLayout = LayoutInflater.from(context).inflate(R.layout.new_pass_code_layout, this, true);
        } else {
            passcodeLayout = LayoutInflater.from(context).inflate(R.layout.pass_code_layout, this, true);
            passcodeLayout.findViewById(R.id.changeUserButton).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    changeUserClickListener.onClick();
                }
            });
        }
        LinearLayout linearLayout= (LinearLayout) passcodeLayout.findViewById(R.id.pass_code);

        forgetPasswordTextView = passcodeLayout.findViewById(R.id.forget_password_text_view);
        forgetPasswordTextView.setText(Html.fromHtml("<u>Forgot Passcode</u>"));
        TextView passcodeWord= (TextView) LayoutInflater.from(context).inflate(R.layout.passcod_word_view,null);

        infoTextView = passcodeLayout.findViewById(R.id.passcode_error_text_view);

        // ((ViewGroup)passcodeWord.getParent()).removeAllViews();
//        View view=new View(context);
//        view.setLayoutParams(new ViewGroup.LayoutParams(20, 20));
//        linearLayout.addView(passcodeWord);
//        linearLayout.addView(view);
//        pascodeViews.add(passcodeWord);
//
//        passcodeWord= (TextView) LayoutInflater.from(context).inflate(R.layout.passcod_word_view,null);
//        view=new View(context);
//        view.setLayoutParams(new ViewGroup.LayoutParams(20, 20));
//        linearLayout.addView(passcodeWord);
//        linearLayout.addView(view);
//        pascodeViews.add(passcodeWord);
//
//        passcodeWord= (TextView) LayoutInflater.from(context).inflate(R.layout.passcod_word_view,null);
//        view=new View(context);
//        view.setLayoutParams(new ViewGroup.LayoutParams(20,20));
//        linearLayout.addView(passcodeWord);
//        linearLayout.addView(view);
//        pascodeViews.add(passcodeWord);
//
//        passcodeWord= (TextView) LayoutInflater.from(context).inflate(R.layout.passcod_word_view,null);
//        view=new View(context);
//        view.setLayoutParams(new ViewGroup.LayoutParams(20,20));
//        linearLayout.addView(passcodeWord);
//        linearLayout.addView(view);
//        pascodeViews.add(passcodeWord);

        for(int index=0; index<((ViewGroup)linearLayout).getChildCount(); ++index) {
            passcodeWord = (TextView) ((ViewGroup) linearLayout).getChildAt(index);
            pascodeViews.add(passcodeWord);
        }

        passcodeLayout.findViewById(R.id.zero).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.one).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.two).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.three).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.four).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.five).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.six).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.seven).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.eight).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.nine).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.delete).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!passcode.equalsIgnoreCase("")&&passcode.length()!=0){
                    pascodeViews.get(passcode.length()-1).setText("");
                    passcode=passcode.substring(0,passcode.length()-1);
                }
            }
        });
        passcodeLayout.findViewById(R.id.clear).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                clearInput();
            }
        });

    }

    @Override
    public void onClick(View v) {
        Log.e("passcode_length", passcode.length() + " d");

        if (passcode.length() == 0){

            Button textView= (Button) v;
            passcode+= textView.getText();
            pascodeViews.get(passcode.length()-1).setText("*");
            if (passcodeListener != null){
                passcodeListener.onStartOfPassCode();
            }
        } else if(passcode.length()<length-1){

            Button textView= (Button) v;
            passcode+= textView.getText();

            pascodeViews.get(passcode.length()-1).setText("*");

        }else if(passcode.length()==length-1){
            Button textView= (Button) v;
            passcode+= textView.getText();
            Log.w("end of pass code"," FINISH");
            pascodeViews.get(passcode.length()-1).setText("*");
            if(passcodeListener!=null){
//                if (passcode.length() == 0){
//                    passcodeListener.onStartOfPassCode();
//                }


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        passcodeListener.onEndOfPassCode(passcode);
                    }
                },100);
            }


        }
        Log.w("PassCode"," SS "+passcode);
    }

    public void setPasscodeListener(PasscodeListener passcodeListener) {
        this.passcodeListener = passcodeListener;
    }

    public void setChangeUserClickListener(ChangeUserClickListener changeUserClickListener) {
        this.changeUserClickListener = changeUserClickListener;
    }

    public void setLable(String msg){
        ((TextView)passcodeLayout.findViewById(R.id.label)).setText(msg);

    }


    public void clearInput(){
        passcode="";
        for(TextView textView:pascodeViews){
            textView.setText("");
        }
    }

    public void loginPasscodeSuccess(){
        infoTextView.setText("Success");
        infoTextView.setTextColor(Color.parseColor("#FF095E0F"));
    }

}
