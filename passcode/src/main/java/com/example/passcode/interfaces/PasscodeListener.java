package com.example.passcode.interfaces;

/**
 * Created by lyx on 2/16/18.
 */

public interface PasscodeListener {

  void onStartOfPassCode();

  void onEndOfPassCode(String passcode);
}
