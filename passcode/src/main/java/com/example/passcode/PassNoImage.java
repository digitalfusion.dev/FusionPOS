package com.example.passcode;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 10/16/17.
 */

public class PassNoImage extends RelativeLayout implements View.OnClickListener {

    private View passcodeLayout;
    private String passcode="";
    private int length=4;

    private PassCodeListener passcodeListener;

    private List<TextView> pascodeViews=new ArrayList<>();

    public PassNoImage(Context context) {
        super(context);
        init(context);
    }

    public PassNoImage(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PassNoImage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PassNoImage(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }


    private void init(Context context){
        passcodeLayout= LayoutInflater.from(context).inflate(R.layout.pass_code_no_image,this,true);

        LinearLayout linearLayout= (LinearLayout) passcodeLayout.findViewById(R.id.pass_code);

        TextView passcodeWord= (TextView) LayoutInflater.from(context).inflate(R.layout.passcod_word_view,null);


        // ((ViewGroup)passcodeWord.getParent()).removeAllViews();
        View view=new View(context);
        view.setLayoutParams(new ViewGroup.LayoutParams(20, 20));
        linearLayout.addView(passcodeWord);
        linearLayout.addView(view);
        pascodeViews.add(passcodeWord);

        passcodeWord= (TextView) LayoutInflater.from(context).inflate(R.layout.passcod_word_view,null);
        view=new View(context);
        view.setLayoutParams(new ViewGroup.LayoutParams(20, 20));
        linearLayout.addView(passcodeWord);
        linearLayout.addView(view);
        pascodeViews.add(passcodeWord);

        passcodeWord= (TextView) LayoutInflater.from(context).inflate(R.layout.passcod_word_view,null);
        view=new View(context);
        view.setLayoutParams(new ViewGroup.LayoutParams(20,20));
        linearLayout.addView(passcodeWord);
        linearLayout.addView(view);
        pascodeViews.add(passcodeWord);

        passcodeWord= (TextView) LayoutInflater.from(context).inflate(R.layout.passcod_word_view,null);
        view=new View(context);
        view.setLayoutParams(new ViewGroup.LayoutParams(20,20));
        linearLayout.addView(passcodeWord);
        linearLayout.addView(view);
        pascodeViews.add(passcodeWord);


        passcodeLayout.findViewById(R.id.one).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.two).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.three).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.four).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.five).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.six).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.seven).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.eight).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.nine).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.zero).setOnClickListener(this);
        passcodeLayout.findViewById(R.id.delete).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!passcode.equalsIgnoreCase("")&&passcode.length()!=0){
                    pascodeViews.get(passcode.length()-1).setText("");
                    passcode=passcode.substring(0,passcode.length()-1);
                }
            }
        });
        passcodeLayout.findViewById(R.id.clear).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                clearInput();
            }
        });
    }

    @Override
    public void onClick(View v) {

        if (passcode.length() == 0){

            Button textView= (Button) v;
            passcode+= textView.getText();
            pascodeViews.get(passcode.length()-1).setText("*");
            if (passcodeListener != null){
                passcodeListener.onStartOfPassCode();
            }
        }else if(passcode.length()<length-1){

            Button textView= (Button) v;
            passcode+= textView.getText();

            pascodeViews.get(passcode.length()-1).setText("*");

        }else if(passcode.length()==length-1){
            Button textView= (Button) v;
            passcode+= textView.getText();
            Log.w("end of pass code"," FINISH");
            pascodeViews.get(passcode.length()-1).setText("*");
            if(passcodeListener!=null){
                long t1= System.currentTimeMillis();

                Log.w("t1 in Pass",t1+ "SS");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        passcodeListener.onEndOfPassCode(passcode);
                    }
                },100);
            }


        }
        Log.w("PassCode"," SS "+passcode);
    }

    public void setPassCodeListener(PassCodeListener passcodeListener) {
        this.passcodeListener = passcodeListener;
    }

    public interface PassCodeListener{
        void onEndOfPassCode(String passcode);
        void onStartOfPassCode();
    }

    public void setLable(String msg){
        ((TextView)passcodeLayout.findViewById(R.id.label)).setText(msg);

    }

    public void setMsg(String msg){
        ((TextView)passcodeLayout.findViewById(R.id.passcode_error_text_view)).setText(msg);

    }

//    public void setMsgColor(){
//        ((TextView)passcodeLayout.findViewById(R.id.passcode_error_text_view)).setTextColor();
//    }

    public void clearInput(){
        passcode="";
        for(TextView textView:pascodeViews){
            textView.setText("");
        }
    }
}
