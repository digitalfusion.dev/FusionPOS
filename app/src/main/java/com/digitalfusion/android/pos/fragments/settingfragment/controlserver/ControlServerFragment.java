package com.digitalfusion.android.pos.fragments.settingfragment.controlserver;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.ServerTabsPagerAdapter;
import com.digitalfusion.android.pos.network.Server;
import com.digitalfusion.android.pos.util.ConnectivityUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;


import java.io.Serializable;

/**
 * Created by MD003 on 8/24/16.
 */
public class ControlServerFragment extends Fragment implements Serializable {

    private TextView ipAddressView;
    private TextView serverStatusView;
    private Button startStopButton;
    private ImageView statusDotView;
    private ViewPager tabPages;
    private TabLayout tabLayout;

    private Server server;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(ThemeUtil.getString(getContext(), R.attr.str_control_server));
        MainActivity.setCurrentFragment(this);

        return inflater.inflate(R.layout.fragment_control_server, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize(view);
        bindViews();

        view.getViewTreeObserver().addOnWindowFocusChangeListener(new ViewTreeObserver.OnWindowFocusChangeListener() {
            @Override
            public void onWindowFocusChanged(boolean b) {
                if (ConnectivityUtil.isWifiConnected(context)) {
                    startStopButton.setEnabled(true);
                    ipAddressView.setText(ConnectivityUtil.getIpAdress());
                } else {
                    ipAddressView.setText("Please connect to WiFi");
                    startStopButton.setEnabled(false);
                }

                
            }
        });
    }

    private void initialize(View view) {
        server  = Server.getInstance();
        context = getContext();

        ipAddressView    = view.findViewById(R.id.ipAddressView   );
        serverStatusView = view.findViewById(R.id.serverStatusView);
        startStopButton  = view.findViewById(R.id.startStopButton );
        statusDotView    = view.findViewById(R.id.statusDot       );
        tabLayout        = view.findViewById(R.id.tabLayout);
        tabPages         = view.findViewById(R.id.tabPages);
    }

    private void bindViews() {
        ipAddressView.setText(ConnectivityUtil.getIpAdress());
        if (server.isServerRunning()) {
            statusDotView.setActivated(true);
            serverStatusView.setText("Online");
            startStopButton.setText("Stop Server");
            if (startStopButton.getText().toString().equalsIgnoreCase("Start Server")) {
                startStopButton.setText("Stop Server");
            }
        } else {
            statusDotView.setActivated(false);
            serverStatusView.setText("Offline");
            startStopButton.setText("Start Server");
            if (startStopButton.getText().toString().equalsIgnoreCase("Stop Server")) {
                startStopButton.setText("Startw Server");
            }
        }

        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleServer();
            }
        });

        tabPages.setAdapter(new ServerTabsPagerAdapter(getFragmentManager(), new String[] {"Devices", "Online Users"}, context));
        tabLayout.setupWithViewPager(tabPages);
    }

    private void toggleServer() {
        if (server != null) {
            if (!server.isServerRunning()) {
                server.startServer(getActivity());
                bindViews();
            } else {
                server.stopServer();
                bindViews();
            }
        }
    }

}

