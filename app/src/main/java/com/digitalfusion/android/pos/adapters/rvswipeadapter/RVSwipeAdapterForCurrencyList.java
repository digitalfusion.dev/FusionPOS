package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Currency;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD003 on 8/25/16.
 */
public class RVSwipeAdapterForCurrencyList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private List<Currency> currencyList;

    private ClickListener editClickListener;

    private ClickListener deleteClickListener;


    public RVSwipeAdapterForCurrencyList(List<Currency> currencyList) {
        this.currencyList = currencyList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_list_item_view, parent, false);

        return new CurrencyViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof CurrencyViewHolder) {

            final CurrencyViewHolder CurrencyViewHolder = (CurrencyViewHolder) holder;

            POSUtil.makeZebraStrip(CurrencyViewHolder.itemView, position);

            CurrencyViewHolder.currencyNameTextView.setText(currencyList.get(position).getDisplayName());

            Log.w("currecy", currencyList.get(position).getDisplayName() + currencyList.get(position).getSign());

            CurrencyViewHolder.currencySymbolTextView.setText(currencyList.get(position).getSign());

            if (currencyList.get(position).isActive()) {

                CurrencyViewHolder.isDefaultImageView.setVisibility(View.VISIBLE);

            } else {

                CurrencyViewHolder.isDefaultImageView.setVisibility(View.GONE);

            }

            if (currencyList.get(position).getId() == 1l) {
                ((CurrencyViewHolder) holder).deleteButton.setVisibility(View.GONE);
            }

            CurrencyViewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editClickListener != null) {

                        CurrencyViewHolder.swipeLayout.close(true);

                        editClickListener.onClick(position);

                    }
                }
            });

            CurrencyViewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteClickListener != null) {
                        CurrencyViewHolder.swipeLayout.close(true);
                        deleteClickListener.onClick(position);
                    }
                }
            });

            mItemManger.bindView(CurrencyViewHolder.view, position);

        }

    }

    @Override
    public int getItemCount() {
        return currencyList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public List<Currency> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<Currency> currencyList) {
        this.currencyList = currencyList;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    public class CurrencyViewHolder extends RecyclerView.ViewHolder {

        TextView currencyNameTextView;

        TextView currencySymbolTextView;

        TextView currencyDescTextView;

        AppCompatTextView isDefaultImageView;

        ImageButton editButton;

        ImageButton deleteButton;

        SwipeLayout swipeLayout;

        View view;

        public CurrencyViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            editButton = (ImageButton) itemView.findViewById(R.id.edit);
            deleteButton = (ImageButton) itemView.findViewById(R.id.delete);
            currencyNameTextView = (TextView) itemView.findViewById(R.id.currency_name);
            currencySymbolTextView = (TextView) itemView.findViewById(R.id.currency_sign);
            //  currencyDescTextView =(TextView)itemView.findViewById(R.id.currency_desc_in_add_currency_et);
            isDefaultImageView = (AppCompatTextView) itemView.findViewById(R.id.is_defalut_imgv);


        }

    }
}
