package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.AuthorizationManager;
import com.digitalfusion.android.pos.database.model.PurchaseHistory;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 9/5/16.
 */
public class RVSwiperAdapterForPurchaseTransactions extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    public int currentDate;
    protected boolean showLoader = false;
    private List<PurchaseHistory> purchaseHistoryList;
    private ClickListener editClickListener;
    private ClickListener viewDetailClickListener;
    private ClickListener viewPaymentsClickListerner;
    private ClickListener deleteClickListener;
    private LoaderViewHolder loaderViewHolder;
    private int editPos;

    public RVSwiperAdapterForPurchaseTransactions(List<PurchaseHistory> purchaseHistoryList) {

        Calendar now = Calendar.getInstance();

        currentDate = Integer.parseInt(DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(now.get(Calendar.DAY_OF_MONTH))));

        this.purchaseHistoryList = purchaseHistoryList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEWTYPE_LOADER) {

            // Your Loader XML view here
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            // Your LoaderViewHolder class
            return new LoaderViewHolder(view);
        } else {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_transaction_item_view, parent, false);

            return new SaleHistoryViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof LoaderViewHolder) {

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) holder;

            this.loaderViewHolder = loaderViewHolder;

            if (showLoader) {

                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {

                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);

            return;
        } else if (holder instanceof SaleHistoryViewHolder) {

            final SaleHistoryViewHolder viewHolder = (SaleHistoryViewHolder) holder;

            POSUtil.makeZebraStrip(viewHolder.itemView, position);

            viewHolder.saleIdTextView.setText("#" + purchaseHistoryList.get(position).getVoucherNo());

            if (purchaseHistoryList.get(position).getSupplierName() != null) {

                if (purchaseHistoryList.get(position).getSupplierName().equalsIgnoreCase(AppConstant.DEFAULT_SUPPLIER)) {

                    viewHolder.supplierTextView.setText(purchaseHistoryList.get(position).getSupplierName());
                } else {

                    viewHolder.supplierTextView.setText(purchaseHistoryList.get(position).getSupplierName());
                }
            }

            viewHolder.totalAmountTextView.setText(POSUtil.NumberFormat(purchaseHistoryList.get(position).getTotalAmount()));



            //String dayDes[] = DateUtility.dayDes(purchaseHistoryList.get(position).getDate());

            // String yearMonthDes = DateUtility.monthYearDes(purchaseHistoryList.get(position).getDate());

            int date = Integer.parseInt(purchaseHistoryList.get(position).getDate());

            if (date == currentDate) {
                viewHolder.dateTextView.setText(viewHolder.today);
            } else if (date == currentDate - 1) {
                viewHolder.dateTextView.setText(viewHolder.yesterday);
            } else {

                viewHolder.dateTextView.setText(DateUtility.makeDateFormatWithSlash(purchaseHistoryList.get(position).getYear(),
                        purchaseHistoryList.get(position).getMonth(),
                        purchaseHistoryList.get(position).getDay()));

                //viewHolder.dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));
            }

            viewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    editPos = position;
                    if (editClickListener != null) {

                        editClickListener.onClick(position);
                    }
                }
            });
            if (purchaseHistoryList.get(position).isHold()) {
                viewHolder.viewDetailsButton.setVisibility(View.GONE);
                viewHolder.viewPayments.setVisibility(View.GONE);
            }
            if (purchaseHistoryList.get(position).isHold()) {
                viewHolder.editButton.setVisibility(View.VISIBLE);
            }

            viewHolder.viewDetailsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewDetailClickListener != null) {

                        viewDetailClickListener.onClick(position);

                    }

                }
            });

            viewHolder.viewPayments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewPaymentsClickListerner != null) {
                        viewPaymentsClickListerner.onClick(position);
                    }
                }
            });
            viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
                @Override
                public void onStartOpen(SwipeLayout layout) {

                }

                @Override
                public void onOpen(SwipeLayout layout) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            viewHolder.swipeLayout.close();

                        }
                    }, 2000);
                }

                @Override
                public void onStartClose(SwipeLayout layout) {

                }

                @Override
                public void onClose(SwipeLayout layout) {

                }

                @Override
                public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

                }

                @Override
                public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

                }
            });

            viewHolder.deletImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteClickListener != null) {
                        deleteClickListener.onClick(holder.getLayoutPosition());
                    }
                }
            });

            mItemManger.bindView(viewHolder.itemView, position);
        }

    }

    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position

        Log.w("IN item view type", "item view type");
        if (position != 0 && position == getItemCount() - 1) {

            if (purchaseHistoryList != null && purchaseHistoryList.size() != 0) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;

            }

        }

        return VIEWTYPE_ITEM;
    }


    public ClickListener getViewPaymentsClickListerner() {
        return viewPaymentsClickListerner;
    }

    public void setViewPaymentsClickListerner(ClickListener viewPaymentsClickListerner) {
        this.viewPaymentsClickListerner = viewPaymentsClickListerner;
    }

    @Override
    public int getItemCount() {

        if (purchaseHistoryList == null || purchaseHistoryList.size() == 0) {

            Log.w("jhrere", "return 0");

            return 0;

        } else {
            Log.w("jhrere", "return 0" + purchaseHistoryList.size() + 1 + " Ddd");
            return purchaseHistoryList.size() + 1;

        }

    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    public void setShowLoader(boolean showLoader) {

        this.showLoader = showLoader;

        if (loaderViewHolder != null) {

            if (showLoader) {

                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);

            } else {

                loaderViewHolder.mProgressBar.setVisibility(View.GONE);

            }

        } else {

            Log.w("view null", "view null");
        }

    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public ClickListener getViewDetailClickListener() {
        return viewDetailClickListener;
    }

    public void setViewDetailClickListener(ClickListener viewDetailClickListener) {

        this.viewDetailClickListener = viewDetailClickListener;

    }

    public List<PurchaseHistory> getPurchaseHistoryList() {

        return purchaseHistoryList;

    }

    public void setPurchaseHistoryList(List<PurchaseHistory> purchaseHistoryList) {

        this.purchaseHistoryList = purchaseHistoryList;

    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {

        this.editClickListener = editClickListener;

    }

    public void updateItem(PurchaseHistory purchaseHistory) {

        Log.w("here updateRegistration", purchaseHistory.getId() + "");

        // Log.w("position",purchaseHistoryList.indexOf(purchaseHistory)+" S");

        //Log.w("position",purchaseHistoryList.get(purchaseHistoryList.indexOf(purchaseHistory)).getSupplierName()+" S");

        // Log.w("position",purchaseHistory.getSupplierName()+" S");

        purchaseHistoryList.set(editPos, purchaseHistory);

        notifyItemChanged(editPos, purchaseHistory);

        //  notifyDataSetChanged();


    }

    public class SaleHistoryViewHolder extends RecyclerView.ViewHolder {



        TextView saleIdTextView;

        TextView supplierTextView;

        TextView dateTextView;

        TextView totalAmountTextView;

        LinearLayout linearLayout;

        ImageButton editButton;

        ImageButton viewDetailsButton;

        ImageButton viewPayments;

        SwipeLayout swipeLayout;

        ImageButton deletImageButton;

        View view;

        String yesterday;
        String today;

        public SaleHistoryViewHolder(View itemView) {

            super(itemView);
            Context context = itemView.getContext();

            deletImageButton = (ImageButton) itemView.findViewById(R.id.delete);

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);

            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll);

            editButton = (ImageButton) itemView.findViewById(R.id.edit_purchase_trans);

            viewDetailsButton = (ImageButton) itemView.findViewById(R.id.view_detail);

            viewPayments = (ImageButton) itemView.findViewById(R.id.view_payments);

            saleIdTextView = (TextView) itemView.findViewById(R.id.purchase_id_in_purchase_transaction_view);

            supplierTextView = (TextView) itemView.findViewById(R.id.supplier_in_purchase_transaction_view);

            dateTextView = (TextView) itemView.findViewById(R.id.date_in_purchase_transaction_view);

            totalAmountTextView = (TextView) itemView.findViewById(R.id.total_amount_in_purchase_transaction_view);


            yesterday = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday}).getString(0);

            today = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.today}).getString(0);

            AccessLogManager     accessLogManager     = new AccessLogManager(context);
            AuthorizationManager authorizationManager = new AuthorizationManager(context);
            Long                 deviceId             = authorizationManager.getDeviceId(Build.SERIAL);
            User                 currentUser          = accessLogManager.getCurrentlyLoggedInUser(deviceId);

            if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Admin.toString())) {
            } else if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Manager.toString())) {
            } else if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Supervisor.toString())) {
                deletImageButton.setVisibility(View.GONE);
            } else if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Sale.toString())) {
                viewPayments.setVisibility(View.GONE);

                editButton.setVisibility(View.GONE);

                deletImageButton.setVisibility(View.GONE);

            }

        }

    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar mProgressBar;

        public View mView;

        public LoaderViewHolder(View itemView) {

            super(itemView);

            mView = itemView;

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);

        }
    }
}