package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForUserRoleMD;
import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.views.FusionToast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 3/8/17.
 */

public class AddEditUserFragment extends Fragment {

    public static final String KEY = "user";
    public User user;
    public boolean isEdit = false;
    private View mainLayoutView;
    //MD Add ID
    private EditText userName;
    private EditText passcodeView;
    //    private EditText userPassword;
    //    private ImageButton showPassword;
    //    private EditText userConfirmPassword;
    //    private ImageButton showConfirmPassword;
    private TextView userRole;
    private EditText userDescription;
    private MDButton saveMdButton;
    private TextView passLable;
    private LinearLayout passLL;
    //    private TextView passConfirmLable;
    //    private LinearLayout passConfirmLL;
    //    private ImageView userPicture;

    //MD's UserRole MD
    private RVAdapterForUserRoleMD rvAdapterForUserRoleMD;
    private MaterialDialog userRoleChooserMD;
    private List<String> roleList;
    private String role;
    private ApiDAO apiDao;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.add_user_role, container, false);
        context = getContext();
        apiDao = ApiDAO.getApiDAOInstance(context);

        setHasOptionsMenu(true);

        //        String admin= context.getTheme().obtainStyledAttributes(new int[]{R.attr.admin}).getString(0);
        //        String manager= context.getTheme().obtainStyledAttributes(new int[]{R.attr.manager}).getString(0);
        //        String supervisor= context.getTheme().obtainStyledAttributes(new int[]{R.attr.supervisor}).getString(0);
        //        String staff= context.getTheme().obtainStyledAttributes(new int[]{R.attr.staff}).getString(0);
        //        String sale = POSUtil.getString(context, R.attr.sale);
        //        String purchase = POSUtil.getString(context, R.attr.purchase);

        roleList = new ArrayList<>();
        roleList.add(User.ROLE.Admin.toString());
        //        roleList.add(manager);
        //        roleList.add(supervisor);
        //        roleList.add(staff);
        roleList.add(User.ROLE.Sale.toString());
        loadUI();

        userRole.setText(User.ROLE.Admin.toString());

        buildUserRoleChooserDialog(roleList);

        if (getArguments().getSerializable(KEY) == null) {
            String addUser = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_user_role}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(addUser);
            isEdit = false;
        } else {
            String editUser = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_user_role}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(editUser);
            isEdit = true;
            user = (User) getArguments().get(KEY);
            initializeOldData();
            //            userPassword.setVisibility(View.GONE);
            //            passLL.setVisibility(View.GONE);
            //            passLable.setVisibility(View.GONE);
            //            passConfirmLL.setVisibility(View.GONE);
            //            passConfirmLable.setVisibility(View.GONE);
        }

        //        showPassword.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //                Toast.makeText(context, "hi", Toast.LENGTH_SHORT).show();
        //            }
        //        });
        //
        //        userConfirmPassword.setInputType(129);
        //
        //        showConfirmPassword.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //                if (showConfirmPassword.isSelected()) {
        //                    showConfirmPassword.setSelected(false);
        //                    userConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        //                } else {
        //                    userConfirmPassword.setInputType(InputType.TYPE_NUMBER_VARIATION_NORMAL);
        //                    showConfirmPassword.setSelected(true);
        //                }
        //                userConfirmPassword.setSelection(userConfirmPassword.getText().length());
        //            }
        //        });


        userRole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userRoleChooserMD.show();
            }
        });

        rvAdapterForUserRoleMD.setmItemClickListener(new RVAdapterForUserRoleMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                userRole.setText(roleList.get(position).toString());

                role = roleList.get(position);
                userRoleChooserMD.dismiss();

            }
        });

        //TODO: Uncomment these lines to let admin add pictures for users
        //        userPicture.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View view) {
        //                GrantPermission grantPermission = new GrantPermission(AddEditUserFragment.this);
        //                if (grantPermission.permission()) {
        //                    Intent chooseImageIntent = ImagePicker.getPickImageIntent(context);
        //                    startActivityForResult(chooseImageIntent, REQUEST_SELECT_PICTURE);
        //                }
        //            }
        //        });

        return mainLayoutView;
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    private void initializeOldData() {

        userName.setText(user.getUserName());
        userRole.setText(user.getRole());
        passcodeView.setText(user.getPasscode());
        userDescription.setText(user.getDescription());

        if (user.getId() == 1) {
            userRole.setEnabled(false);
        }
        //TODO: Uncomment these lines to let admin add pictures for users
        //        if (user.getPicture() != null) {
        //
        //            Bitmap image = POSUtil.getBitmapFromByteArray(user.getPicture());
        //            userPicture.setImageBitmap(image);
        //        }
    }

    private void clearData() {
        userName.setText(null);
        userName.setError(null);
        userRole.setText(null);
        userRole.setError(null);
        userDescription.setText(null);
        userDescription.setError(null);
        passcodeView.setError(null);
        passcodeView.setText(null);
        //        userPassword.setText(null);
        //        userPassword.setError(null);
        //        userConfirmPassword.setText(null);
        //        userConfirmPassword.setError(null);

        //TODO: Uncomment these lines to let admin add pictures for users
        //        userPicture.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.add_image));
    }


    private boolean checkValidation() {
        boolean status = true;

        if (userName.getText().toString().trim().length() < 1) {

            String enterUsername = ThemeUtil.getString(context, R.attr.please_enter_username);
            userName.setError(enterUsername);

            status = false;
        } else {
            String inUserName = userName.getText().toString().trim();
            if (isEdit) {
                if (apiDao.userNameAlreadyExist(inUserName)) {
                    if (!inUserName.equalsIgnoreCase(user.getUserName())) {

                        userName.setError(ThemeUtil.getString(context, R.attr.username_already_exists));
                        status = false;
                    }
                }
            } else {
                if (apiDao.userNameAlreadyExist(inUserName)) {
                    userName.setError(ThemeUtil.getString(context, R.attr.username_already_exists));
                    status = false;
                }
            }

        }


        if (passcodeView.getText().toString().trim().length() < 4 || passcodeView.getText().toString().trim().length() > 4) {
            String error = ThemeUtil.getString(context, R.attr.passcode_error);
            passcodeView.setError(error);

            status = false;
        }


        return status;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save) {

            if (!isEdit) {
                saveUser();
            } else {

                //                    Boolean status = userBusiness.updateUserRole(userName.getText().toString(), user.getPassword(), userRole.getText().toString(),
                //                            userDescription.getText().toString(), user.getId());
                updateUser();

            }

        }
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void saveUser() {

        if (checkValidation()) {

            Log.w("check  user role", userRole.getText().toString());

            //            String role;
            //            if(userRole.getText().toString()==roleList.get(0)){
            //                role = "Admin";
            //            }else if (userRole.getText().toString()==roleList.get(0)){
            //                role = "Sale";
            //            }else {
            //                role = "Purchase";
            //            }

            String username = userName.getText().toString().trim();
            String passcode = passcodeView.getText().toString();
            String role     = userRole.getText().toString();
            //TODO: Uncomment these lines to let admin add pictures for users
            //            Bitmap picture = ((BitmapDrawable)userPicture.getDrawable()).getBitmap();
            //            apiDAO.addNewUser(username, passcode, role, POSUtil.bitmapToBiteArray(picture));
            apiDao.addNewUser(username, passcode, role, null);
            clearData();
            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
            getActivity().onBackPressed();

        }
    }

    private void updateUser() {
        if (checkValidation()) {
            long   id       = user.getId();
            String username = userName.getText().toString().trim();
            String passcode = passcodeView.getText().toString();
            String role     = userRole.getText().toString();
            //TODO: Uncomment these lines to let admin add pictures for users
            //            Bitmap picture = ((BitmapDrawable)userPicture.getDrawable()).getBitmap();
            //            apiDAO.updateUserRole(id, username, passcode, role, POSUtil.bitmapToBiteArray(picture));
            apiDao.updateUserRole(id, username, passcode, role, null);
            clearData();
            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
            getActivity().onBackPressed();
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);

      /*  TypedArray save=context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        FontTextView fontTextView= (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.menu_text,null);

        menu.getItem(0).setActionView(fontTextView);

        fontTextView.setText(save.getString(0));


        fontTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menu.getItem(0));
            }
        });*/


        super.onCreateOptionsMenu(menu, inflater);

    }

    private void loadUI() {
        //Add User Role MD
        //        passLable = (TextView) mainLayoutView.findViewById(R.id.pass_label);
        //        passLL = (LinearLayout) mainLayoutView.findViewById(R.id.pass_field);
        //        passConfirmLable = (TextView) mainLayoutView.findViewById(R.id.pass_confirm_label);
        //        passConfirmLL = (LinearLayout) mainLayoutView.findViewById(R.id.pass_confirm_field);

        userName = (EditText) mainLayoutView.findViewById(R.id.name_in_add_user_role_TIET);
        passcodeView = mainLayoutView.findViewById(R.id.pass_code_view);
        //        userPassword = (EditText) mainLayoutView.findViewById(R.id.password_in_add_user_role_TIET);
        //        showPassword = (ImageButton) mainLayoutView.findViewById(R.id.showpassword);
        //        userConfirmPassword = (EditText) mainLayoutView.findViewById(R.id.confirm_password_in_add_user_role_TIET);
        //        showConfirmPassword = (ImageButton) mainLayoutView.findViewById(R.id.showconfirmpassword);
        userRole = (TextView) mainLayoutView.findViewById(R.id.role_in_add_user_role_TIET);
        //TODO: Uncomment these lines to let admin add pictures for users
        //        userPicture = mainLayoutView.findViewById(R.id.userPicture);
        userDescription = (EditText) mainLayoutView.findViewById(R.id.description_in_add_user_role_TIET);

    }

    private void buildUserRoleChooserDialog(List<String> userRoles) {
        rvAdapterForUserRoleMD = new RVAdapterForUserRoleMD(userRoles);
        userRoleChooserMD = new MaterialDialog
                .Builder(getContext())
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForUserRoleMD, null)
                .build();

    }


    //TODO: Uncomment these lines to let admin add pictures for users
    //    @Override
    //    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    //
    //        if (resultCode == Activity.RESULT_OK) {
    //            switch (requestCode) {
    //                case REQUEST_SELECT_PICTURE:
    //                    final Uri selectedUri = ImagePicker.getUriFromResult(context, resultCode, data);
    //                    if (selectedUri != null) {
    //                        File imageFile = new File(POSUtil.getRealPathFromURI(selectedUri, context));
    //                        Bitmap picture = POSUtil.decodeFile(imageFile);
    //
    //                        userPicture.setImageBitmap(picture);
    //                    } else {
    //                        String cannotRetrieve=POSUtil.getString(context,R.attr.cannot_retrieve_selected_image);
    //                        Toast.makeText(context, cannotRetrieve, Toast.LENGTH_SHORT).show();
    //
    //                    }
    //
    //            }
    //
    //        }
    //
    //
    //    }


}
