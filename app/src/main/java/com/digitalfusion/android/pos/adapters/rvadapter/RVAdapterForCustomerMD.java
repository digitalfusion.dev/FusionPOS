package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Customer;

import java.util.List;

/**
 * Created by MD003 on 9/20/16.
 */
public class RVAdapterForCustomerMD extends RecyclerView.Adapter<RVAdapterForCustomerMD.CategoryViewHolder> {

    private List<Customer> customerList;

    private OnItemClickListener mItemClickListener;

    public RVAdapterForCustomerMD(List<Customer> customerList) {

        this.customerList = customerList;

    }

    @Override
    public RVAdapterForCustomerMD.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item_view_md, parent, false);

        return new CategoryViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RVAdapterForCustomerMD.CategoryViewHolder holder, final int position) {

        holder.categoryTextView.setText(customerList.get(position).getName());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mItemClickListener != null) {

                    mItemClickListener.onItemClick(v, position);

                }
            }
        });


    }

    @Override
    public int getItemCount() {

        return customerList.size();

    }

    public List<Customer> getCustomerList() {

        return customerList;

    }

    public void setCustomerList(List<Customer> categoryVOList) {

        this.customerList = categoryVOList;

    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {

        TextView categoryTextView;

        View view;

        public CategoryViewHolder(View itemView) {

            super(itemView);

            this.view = itemView;

            categoryTextView = (TextView) itemView.findViewById(R.id.category_name_tv);

        }

    }
}

