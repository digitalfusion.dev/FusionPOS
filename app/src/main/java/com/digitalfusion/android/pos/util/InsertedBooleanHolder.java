package com.digitalfusion.android.pos.util;

/**
 * Created by MD002 on 8/19/16.
 */
public class InsertedBooleanHolder {
    private boolean isInserted;

    public InsertedBooleanHolder() {
        this.isInserted = false;
    }

    public boolean isInserted() {
        return isInserted;
    }

    public void setInserted(boolean inserted) {
        isInserted = inserted;
    }

}
