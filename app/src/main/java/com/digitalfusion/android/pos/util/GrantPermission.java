package com.digitalfusion.android.pos.util;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.widget.Button;

/**
 * Created by MD002 on 9/15/16.
 */
public class GrantPermission {
    public static final int PERMISSION_ALL = 111;
    public static final String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE};

    private Fragment fragment;
    private ChildFragmentPermissionCallback childFragmentPermissionCallback;
    private ChildFragmentPermissionRegistry childFragmentPermissionRegistry;

    public GrantPermission(Fragment fragment) {
        this.fragment = fragment;
    }

    public void setChildFragmentPermissionCallback(ChildFragmentPermissionCallback childFragmentPermissionCallback) {
        this.childFragmentPermissionCallback = childFragmentPermissionCallback;
    }

    public void setChildFragmentPermissionRegistry(ChildFragmentPermissionRegistry childFragmentPermissionRegistry) {
        this.childFragmentPermissionRegistry = childFragmentPermissionRegistry;
    }

    public boolean hasPermissions(Context context) {
        if (context != null && PERMISSIONS != null) {
            for (String permission : PERMISSIONS) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean requestPermissions() {
        final Context context = fragment.getContext();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!hasPermissions(context)) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(fragment.getActivity(),
                        Manifest.permission.READ_PHONE_STATE)) {
                    askAgainPermission();
                } else {
                    Fragment parentFragment = fragment.getParentFragment();
                    if (parentFragment != null) {
                        try {
                            ChildFragmentPermissionRegistry registry = (ChildFragmentPermissionRegistry) parentFragment;
                            registry.registerPermissionListener((ChildFragmentPermissionCallback) parentFragment);
                            parentFragment.requestPermissions(PERMISSIONS, PERMISSION_ALL);
                        } catch (ClassCastException e) {
                            Log.e("requestPermissions", e.getMessage());
                        }
                    } else {
                        fragment.requestPermissions(PERMISSIONS, PERMISSION_ALL);
                    }
                }
            }
        }
        return true;
    }

    public void askAgainPermission() {
        AlertDialog alertDialog;
        alertDialog = new AlertDialog.Builder(fragment.getContext())
                .setTitle(Html.fromHtml("<font color='#424242'>Inform and request</font>"))
                .setMessage(Html.fromHtml("<font color='#424242'>These permissions are needed for this app to function correctly!</font>")).setNegativeButton(Html.fromHtml("<font color='#000000'>No</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(Html.fromHtml("<font color='#424242'>Ok</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.requestPermissions.WRITE_EXTERNAL_STORAGE}, RC_PERMISSION_WRITE_EXTERNAL_STORAGE);
                        //   ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.requestPermissions.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_RC);
                        forcePermissionSetting();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        fragment.getActivity().finishAffinity();
                    }
                })
                .show();
        Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        nbutton.setTextColor(Color.parseColor("#424242"));


    }

    public void forcePermissionSetting() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", fragment.getActivity().getPackageName(), null);
        intent.setData(uri);
        fragment.startActivity(intent);
    }

    public interface ChildFragmentPermissionCallback {
        void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);
    }

    public interface ChildFragmentPermissionRegistry {
        void registerPermissionListener(ChildFragmentPermissionCallback callback);
    }


}
