package com.digitalfusion.android.pos.database.business;

import android.content.Context;
import android.util.Log;

import com.digitalfusion.android.pos.database.dao.purchase.PurchaseDAO;
import com.digitalfusion.android.pos.database.dao.purchase.PurchaseDetailDAO;
import com.digitalfusion.android.pos.database.dao.purchase.PurchaseHoldDAO;
import com.digitalfusion.android.pos.database.dao.purchase.PurchaseHoldDetailDAO;
import com.digitalfusion.android.pos.database.dao.SupplierDAO;
import com.digitalfusion.android.pos.database.model.PurchaseHeader;
import com.digitalfusion.android.pos.database.model.PurchaseHistory;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.InvoiceNoGenerator;

import java.util.List;

/**
 * Created by MD002 on 9/5/16.
 */
public class PurchaseManager {
    private Context context;
    private PurchaseDAO purchaseDAO;
    private PurchaseHoldDAO purchaseHoldDAO;
    private PurchaseHoldDetailDAO purchaseHoldDetailDAO;
    private PurchaseDetailDAO purchaseDetailDAO;
    private String invoiceNo;
    private InvoiceNoGenerator invoiceNoGenerator;
    private Long supplierID;
    private SupplierDAO supplierDAO;
    private boolean flag;

    public PurchaseManager(Context context) {
        this.context = context;
        purchaseHoldDetailDAO = PurchaseHoldDetailDAO.getPurchaseDetailDaoInstance(context);
        purchaseHoldDAO = PurchaseHoldDAO.getPurchaseDaoInstance(context);
        purchaseDAO = PurchaseDAO.getPurchaseDaoInstance(context);
        purchaseDetailDAO = PurchaseDetailDAO.getPurchaseDetailDaoInstance(context);
        invoiceNoGenerator = new InvoiceNoGenerator(context);
        supplierDAO = SupplierDAO.getSupplierDapInstance(context);
    }

    public String getPurchaseInvoiceNo() {
        return invoiceNoGenerator.getInvoiceNo(AppConstant.PURCHASE_INVOICE_NO);
    }

    public boolean addNewPurchase(String voucherNo, String date, String supplierName, String phoneNo,String supplierAddress, String businessName, double totalAmt, String discount, Long taxID, double subTotal,
                                  double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year,
                                  Double taxRate, List<SalesAndPurchaseItem> purchaseDetailItemList, String taxType, Long discountID, Long userId) {

        Log.w("here", "here");
        if (!supplierDAO.checkSupplierAlreadyExists(supplierName)) {
            supplierID = supplierDAO.addNewSupplier(supplierName, businessName, supplierAddress, phoneNo, new InsertedBooleanHolder());
        } else {
            supplierID = supplierDAO.findIDByName(supplierName);

            if (phoneNo != null && phoneNo.length() > 0) {
                supplierDAO.updateSupplierByID(phoneNo,supplierAddress, supplierID);
            }

        }

        Log.w("purchase", supplierID + " Supplier 0");

        flag = purchaseDAO.addNewPurchase(voucherNo, date, supplierID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, taxType, purchaseDetailItemList, discountID, userId);
        if (flag) {
            invoiceNoGenerator.updateInvoiceNextNum();
        }
        return flag;
    }

    public boolean addNewHoldPurchase(String voucherNo, String date, String supplierName, String phoneNo, String businessName, double totalAmt, String discount, Long taxID, double subTotal,
                                      double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year,
                                      Double taxRate, List<SalesAndPurchaseItem> purchaseDetailItemList, String taxType, Long discountID , Long userId) {

        Log.w("here", "here");
        if (!supplierDAO.checkSupplierAlreadyExists(supplierName)) {
            supplierID = supplierDAO.addNewSupplier(supplierName, businessName, "", phoneNo, new InsertedBooleanHolder());
        } else {
            supplierID = supplierDAO.findIDByName(supplierName);

            if (phoneNo != null && phoneNo.length() > 0) {
                supplierDAO.updateSupplierByID(phoneNo,"", supplierID);
            }

        }

        Log.w("purchase", supplierID + " Supplier 0");

        flag = purchaseHoldDAO.addNewPurchase(voucherNo, date, supplierID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, taxType, purchaseDetailItemList, discountID ,userId);
        if (flag) {
            invoiceNoGenerator.updateInvoiceNextNum();
        }
        return flag;
    }

    public List<PurchaseHistory> getAllPurchases(int startLimit, int endLimit, InsertedBooleanHolder flag, String startDate, String endDate , Long userId    ) {
        return purchaseDAO.getAllPurchases(startLimit, endLimit, flag, startDate, endDate , userId);
    }

    public List<PurchaseHistory> getAllHoldPurchases(int startLimit, int endLimit, InsertedBooleanHolder flag, String startDate, String endDate , String userId) {
        return purchaseHoldDAO.getAllPurchases(startLimit, endLimit, flag, startDate, endDate , userId);
    }

    public PurchaseHeader getPurchaseHeaderByPurchaseID(Long id, InsertedBooleanHolder flag) {
        return purchaseDAO.getPurchaseByPurchaseID(id, flag);
    }

    public PurchaseHeader getHoldPurchaseHeaderByPurchaseID(Long id, InsertedBooleanHolder flag) {
        return purchaseHoldDAO.getPurchaseByPurchaseID(id, flag);
    }

    public List<SalesAndPurchaseItem> getPurchaseDetailsByPurchaseID(Long id) {
        return purchaseDetailDAO.getPurchaseDetailsByPurchaseID(id);
    }

    public List<SalesAndPurchaseItem> getHoldPurchaseDetailsByPurchaseID(Long id) {
        return purchaseHoldDetailDAO.getPurchaseDetailsByPurchaseID(id);
    }

    public boolean updatePurchaseByID(Long id, String date, String supplierName, String phoneNo,String supplierAddress, double totalAmt, String discount, Long taxID, double subTotal,
                                      double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year,
                                      Double taxRate, List<SalesAndPurchaseItem> updateDetailList, List<Long> deletedIDList, Long discountID) {

        Log.w("here", "here");
        if (!supplierDAO.checkSupplierAlreadyExists(supplierName)) {
            supplierID = supplierDAO.addNewSupplier(supplierName, "", supplierAddress, phoneNo, new InsertedBooleanHolder());


        } else {
            supplierID = supplierDAO.findIDByName(supplierName);
            if (phoneNo != null && phoneNo.length() > 0) {
                supplierDAO.updateSupplierByID(phoneNo,supplierAddress, supplierID);
            }

            if (supplierAddress != null && supplierAddress.length() > 0) {
                supplierDAO.updateSupplierByID(phoneNo,supplierAddress, supplierID);
            }

        }

        return purchaseDAO.updatePurchaseByID(id, date, supplierID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, updateDetailList, deletedIDList, discountID);
    }

    public boolean updateHoldPurchaseByID(Long id, String date, String supplierName, String phoneNo, double totalAmt, String discount, Long taxID, double subTotal,
                                          double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year,
                                          Double taxRate, List<SalesAndPurchaseItem> updateDetailList, List<Long> deletedIDList, Long discountID) {

        Log.w("here", "here");
        if (!supplierDAO.checkSupplierAlreadyExists(supplierName)) {
            supplierID = supplierDAO.addNewSupplier(supplierName, "", "", phoneNo, new InsertedBooleanHolder());


        } else {
            supplierID = supplierDAO.findIDByName(supplierName);
            if (phoneNo != null && phoneNo.length() > 0) {
                supplierDAO.updateSupplierByID(phoneNo, "",supplierID);
            }
        }

        return purchaseHoldDAO.updatePurchaseByID(id, date, supplierID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, updateDetailList, deletedIDList, discountID);
    }

    public boolean deletePurchase(Long id) {
        return purchaseDAO.deletePurchase(id);
    }

    public boolean deleteHoldPurchase(Long id) {
        return purchaseHoldDAO.deletePurchase(id);
    }

    public List<PurchaseHistory> getSaleVoucherCustomerNameOnSearch(String searchStr) {
        return purchaseDAO.getPurchaseByVoucherCustomerNameOnSearch(searchStr);
    }

    public List<PurchaseHistory> getPurchaseWithVoucherNoOrSupplier(int startLimit, int endLimit, String searchStr) {
        return purchaseDAO.getPurchaseWithVoucherNoOrSupplier(startLimit, endLimit, searchStr);
    }

    public List<PurchaseHistory> getHoldPurchaseWithVoucherNoOrSupplier(int startLimit, int endLimit, String searchStr) {
        return purchaseHoldDAO.getPurchaseWithVoucherNoOrSupplier(startLimit, endLimit, searchStr);
    }

    public List<PurchaseHistory> getPurchaseByVoucherNoOnSearch(String voucherNo) {
        return purchaseDAO.getPurchaseByVoucherNoOnSearch(voucherNo);
    }

    public Long findIDByPurchaseInvoice(String voucherNo) {
        return purchaseDAO.findIDByPurchaseInvoice(voucherNo);
    }

    public List<PurchaseHistory> getPurchaseByCustomerNameOnSearch(String customerName) {
        return purchaseDAO.getPurchaseByCustomerNameOnSearch(customerName);
    }

    public PurchaseHistory getPurchase(Long id) {
        return purchaseDAO.getPurchase(id);
    }

}
