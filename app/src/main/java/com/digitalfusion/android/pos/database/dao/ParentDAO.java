package com.digitalfusion.android.pos.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.database.sqlite.SQLiteTransactionListener;
import android.util.Log;

import com.digitalfusion.android.pos.database.DatabaseHelper;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

/**
 * Created by MD002 on 8/25/16.
 */
public class ParentDAO {
    protected static DatabaseHelper databaseHelper;
    protected SQLiteDatabase database;
    protected String query;
    protected SQLiteStatement statement;
    protected Cursor cursor;
    protected InsertedBooleanHolder flag;
    protected int count;
    protected Long genID;
    protected Long id;
    protected ContentValues contentValues;
    protected String date;
    private Context context;

    public ParentDAO(Context context) {

        Log.w("making new obj", "Parent");

        databaseHelper = DatabaseHelper.getHelperInstance(context);
        flag = new InsertedBooleanHolder();
        contentValues = new ContentValues();
        this.context = context;
    }

    public void databaseWriteTransaction(final InsertedBooleanHolder status) {
        database = databaseHelper.getWritableDatabase();
        database.beginTransactionWithListenerNonExclusive(new SQLiteTransactionListener() {
            @Override
            public void onBegin() {
                status.setInserted(false);
                Log.e("on", "begin");
            }

            @Override
            public void onCommit() {
                Log.e("on", "commit");
                status.setInserted(true);
            }

            @Override
            public void onRollback() {
                Log.e("on", "roll back");
                status.setInserted(false);
            }
        });
    }

    public void databaseWriteTransaction() {
        database = databaseHelper.getWritableDatabase();
        database.beginTransactionWithListenerNonExclusive(new SQLiteTransactionListener() {
            @Override
            public void onBegin() {
                Log.e("on", "begin");
            }

            @Override
            public void onCommit() {
                Log.e("on", "commit");
            }

            @Override
            public void onRollback() {
                Log.e("on", "roll back");
            }
        });
    }

    public void databaseReadTransaction(final InsertedBooleanHolder status) {
        database = databaseHelper.getReadableDatabase();
        database.beginTransactionWithListenerNonExclusive(new SQLiteTransactionListener() {
            @Override
            public void onBegin() {
                Log.e("on", "begin");
                status.setInserted(false);
            }

            @Override
            public void onCommit() {
                Log.e("on", "commit");
                status.setInserted(true);
            }

            @Override
            public void onRollback() {
                Log.e("on", "roll back");
                status.setInserted(false);
            }
        });
    }

    /**
     * @param endDate
     * @param startDate
     * @return
     */
    public boolean createPurchasePriceTempTable(String startDate, String endDate) {
        return databaseHelper.createPurchasePriceTempTable(startDate, endDate);
    }


    public boolean createPurchasePriceTempTableForStockBalance(String startDate, String endDate) {
        return databaseHelper.createPurchasePriceTempTableForStockBalance(startDate, endDate);
    }

    public boolean createTempTableForProfitAndLoss(String startDate, String endDate) {
        return databaseHelper.createPurchasePriceTempTableForProfitAndLoss(startDate, endDate);
    }

    public boolean createPurchasePriceTempTableForValuation(String endDate) {
        return databaseHelper.createPurchasePriceTempTableForValuation(endDate);
    }

    public boolean dropPurchasePriceTempTable() {
        return databaseHelper.dropPurchasePriceTempTable();
    }

    public String formatDateWithDash(String day, String month, String year) {
        date = "";
        if (Integer.parseInt(month) < 10) {
            date = year + "-0" + month;

        } else {
            date = year + "-" + month;
        }
        date += "-";
        if (Integer.parseInt(day) < 10) {
            date += "0" + day;
        } else {
            date += day;
        }
        // Log.e("date",date);
        return date;
    }

    public void deactivateHelper() {
        databaseHelper.shutDownHelper();
        databaseHelper = DatabaseHelper.getHelperInstance(context);
    }


}
