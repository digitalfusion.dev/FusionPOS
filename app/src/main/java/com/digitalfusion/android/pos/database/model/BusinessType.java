package com.digitalfusion.android.pos.database.model;

/**
 * Created by MD002 on 1/2/18.
 */

public class BusinessType {
    private String name;
    private boolean isSelected;

    public BusinessType(String name) {
        this.name = name;
        this.isSelected = false;
    }

    public BusinessType(String name, boolean isSelected) {
        this.name = name;
        this.isSelected = isSelected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
