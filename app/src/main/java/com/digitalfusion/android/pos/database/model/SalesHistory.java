package com.digitalfusion.android.pos.database.model;

import java.io.Serializable;

/**
 * Created by MD003 on 8/29/16.
 */
public class SalesHistory implements Serializable {
    private Long id;
    private String voucherNo;
    private String customer;
    private Long customerID;
    private String date;
    private Double totalAmount;
    private String day;
    private String month;
    private String year;
    private String type;

    public String getChangeBalance() {
        return changeBalance;
    }

    public void setChangeBalance(String changeBalance) {
        this.changeBalance = changeBalance;
    }

    private String changeBalance;

    public String getUserRoleName() {
        return userRoleName;
    }

    public void setUserRoleName(String userRoleName) {
        this.userRoleName = userRoleName;
    }

    private String  userRoleName;
    private Double balance;
    private Double paidAmt;
    private Double voucherDiscount;
    private boolean isFinishDelivery;
    private Double totalBal;
    private int isHold = 0;

    public SalesHistory() {
    }

    public Double getVoucherDiscount() {
        return voucherDiscount;
    }

    public void setVoucherDiscount(Double voucherDiscount) {
        this.voucherDiscount = voucherDiscount;
    }


    public boolean isFinishDelivery() {
        return isFinishDelivery;
    }

    public void setFinishDelivery(boolean finishDelivery) {
        isFinishDelivery = finishDelivery;
    }

    public Double getPaidAmt() {
        return paidAmt;
    }

    public void setPaidAmt(Double paidAmt) {
        this.paidAmt = paidAmt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Long getCustomerID() {

        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getTotalBal() {
        return totalBal;
    }

    public void setTotalBal(Double totalBal) {
        this.totalBal = totalBal;
    }

    public int getIsHold() {
        return isHold;
    }

    public void setIsHold(int isHold) {
        this.isHold = isHold;
    }

    public boolean isHold() {
        return isHold == 1;
    }
}
