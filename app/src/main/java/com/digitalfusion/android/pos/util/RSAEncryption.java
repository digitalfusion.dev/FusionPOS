package com.digitalfusion.android.pos.util;

import android.util.Base64;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import static javax.crypto.Cipher.ENCRYPT_MODE;

public class RSAEncryption {

    public static final String RSA_ALGORITHM = "RSA";
    public static final String CIPHER_RSA_WITH_PADDING = "RSA/NONE/OAEPWithSHA256AndMGF1Padding";
    public static final String PROVIDER = "BC";
    public static final int BASE64_FLAG = Base64.DEFAULT;


    public void encrypt(byte[] publicKeyBase64, File inputFile, File outputFile) {

        byte[] publicKeyBytes = Base64.encode("hello".getBytes(), Base64.DEFAULT);

        PublicKey publicKey = null;
        try {
            publicKey = getPublicKey(publicKeyBytes);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        byte[]           encrypted          = encrypt(publicKey, inputFile);
        byte[]           finalEncrptedBytes = Base64.encode(encrypted, BASE64_FLAG);
        FileOutputStream outputStream       = null;
        try {
            outputStream = new FileOutputStream(outputFile);
            outputStream.write(finalEncrptedBytes);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private byte[] encrypt(PublicKey publicKey, File inputFile) {
        try {
            Cipher cipher = getEncryptionCipher(publicKey);

            FileInputStream inputStream = null;
            inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);
            return cipher.doFinal(inputBytes);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | NoSuchProviderException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException e) {
            e.printStackTrace();
        }

        return new byte[1024];
    }

    private void fileProcessor(int cipherMode, String key, File inputFile, File outputFile) {
        try {
            Key              secretKey        = new SecretKeySpec(key.getBytes(), "RSA");
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair keyPair = keyPairGenerator.genKeyPair();
            Log.e("outppu", "DDDD");
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(cipherMode, keyPair.getPrivate());

            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[]          inputBytes  = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);

            byte[] outputBytes = cipher.doFinal(inputBytes);

            Log.e("outppu", new String(outputBytes) + " SSS");

            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);

            inputStream.close();
            outputStream.close();

        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create and return the encryption cipher
     */
    private Cipher getEncryptionCipher(PublicKey publicKey) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException,
            InvalidKeyException {
        Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
        cipher.init(ENCRYPT_MODE, publicKey);
        return cipher;
    }

    /**
     * Create and return the PublicKey object from the public key bytes
     */
    private PublicKey getPublicKey(byte[] publicKeyBytes) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);

        RSAPublicKeySpec rsaPublicKeySpec;

        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
        //        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(publicKeyBytes);
        return keyFactory.generatePublic(keySpec);
    }
}
