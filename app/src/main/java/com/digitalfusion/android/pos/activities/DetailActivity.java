package com.digitalfusion.android.pos.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.SalesHeader;
import com.digitalfusion.android.pos.fragments.AddEditCustomerFragment;
import com.digitalfusion.android.pos.fragments.AddEditSupplierFragment;
import com.digitalfusion.android.pos.fragments.CustomerDetailView;
import com.digitalfusion.android.pos.fragments.ExpenseDetailView;
import com.digitalfusion.android.pos.fragments.SupplierDetailView;
import com.digitalfusion.android.pos.fragments.inventoryfragments.CategoryFragmentDialog;
import com.digitalfusion.android.pos.fragments.inventoryfragments.DamagedDetailNewFragment;
import com.digitalfusion.android.pos.fragments.inventoryfragments.LostDetailNewFragment;
import com.digitalfusion.android.pos.fragments.inventoryfragments.StockDetailViewFragment;
import com.digitalfusion.android.pos.fragments.inventoryfragments.StockFormFragment;
import com.digitalfusion.android.pos.fragments.inventoryfragments.UnitFragment;
import com.digitalfusion.android.pos.fragments.outstanding.PayableDetailFragment;
import com.digitalfusion.android.pos.fragments.outstanding.PayableItemViewDetailFragment;
import com.digitalfusion.android.pos.fragments.outstanding.ReceivableDetailFragment;
import com.digitalfusion.android.pos.fragments.outstanding.ReceivableItemViewDetailFragment;
import com.digitalfusion.android.pos.fragments.purchasefragments.AddEditNewPurchaseFragment;
import com.digitalfusion.android.pos.fragments.purchasefragments.PurchaseDetailFragment;
import com.digitalfusion.android.pos.fragments.purchasefragments.PurchasePaymentFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.ReportTypeReportFragment;
import com.digitalfusion.android.pos.fragments.salefragments.AddEditNewSaleFragment;
import com.digitalfusion.android.pos.fragments.salefragments.SaleDetailFragment;
import com.digitalfusion.android.pos.fragments.salefragments.SalePaymentFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.AddEdiDiscountFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.AddEditCategoryFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.AddEditCurrencyFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.AddEditTaxFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.AddEditUnitFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.AddEditUserFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.BussinessSettingFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.controlserver.ControlServerFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.CurrencyFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.DataManagementFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.DiscountSetupFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.DocumentSettingFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.MyanmarFontSetting;
import com.digitalfusion.android.pos.fragments.settingfragment.PrintVoucherSettingFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.SettingFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.StockListPreviewFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.TaxSetupFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.ThemeSettingListViewFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.UserRoleSettingFragment;
import com.digitalfusion.android.pos.util.POSUtil;

import static com.digitalfusion.android.pos.activities.MainActivity.currentFragment;
import static com.digitalfusion.android.pos.activities.MainActivity.result;

public class DetailActivity extends ParentActivity {

    public static final String TYPE_KEY = "DETAIL_TYPE";
    private Toolbar toolbar;
    private DETAILS_TYPE detailsType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(POSUtil.getDefaultThemeNoActionBar(this));
        setContentView(R.layout.activity_stock_detail);

        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        detailsType = (DETAILS_TYPE) getIntent().getSerializableExtra(TYPE_KEY);
        Bundle bundle = getIntent().getExtras();




        if (detailsType == DETAILS_TYPE.STOCK_DETAIL) {
            StockDetailViewFragment stockDetailFragment = new StockDetailViewFragment();
            stockDetailFragment.setArguments(bundle);
            replaceFragment(stockDetailFragment);

        } else if (detailsType == DETAILS_TYPE.SALE_DETAIL) {
            SaleDetailFragment printPreview = new SaleDetailFragment();
            printPreview.setArguments(bundle);
            replaceFragment(printPreview);

        } else if (detailsType == DETAILS_TYPE.PURCHASE_DETAIL) {
            PurchaseDetailFragment purchaseDetailFragment = new PurchaseDetailFragment();
            purchaseDetailFragment.setArguments(bundle);
            replaceFragment(purchaseDetailFragment);

        } else if (detailsType == DETAILS_TYPE.RECEIVABLE_PAYMENT_DETAIL) {
            ReceivableItemViewDetailFragment receivableItemViewDetailFragment = new ReceivableItemViewDetailFragment();
            receivableItemViewDetailFragment.setArguments(bundle);
            replaceFragment(receivableItemViewDetailFragment);

        } else if (detailsType == DETAILS_TYPE.PAYABLE_PAYMENT_DETAIL) {
            PayableItemViewDetailFragment payableItemViewDetailFragment = new PayableItemViewDetailFragment();
            payableItemViewDetailFragment.setArguments(bundle);
            replaceFragment(payableItemViewDetailFragment);

        } else if (detailsType == DETAILS_TYPE.CUSTOMER_DETAIL) {
            CustomerDetailView customerDetailView = new CustomerDetailView();
            customerDetailView.setArguments(bundle);
            replaceFragment(customerDetailView);

        } else if (detailsType == DETAILS_TYPE.SUPPLIER_DETAIL) {
            SupplierDetailView supplierDetailView = new SupplierDetailView();
            supplierDetailView.setArguments(bundle);
            replaceFragment(supplierDetailView);

        } else if (detailsType == DETAILS_TYPE.EXPENSE_DETAIL || detailsType == DETAILS_TYPE.INCOME_DETAIL) {
            ExpenseDetailView expense_income_DetailView = new ExpenseDetailView();
            expense_income_DetailView.setArguments(bundle);
            replaceFragment(expense_income_DetailView);

        } else if (detailsType == DETAILS_TYPE.BUSSINESS_SETTING) {
            BussinessSettingFragment bussinessSettingFragment = new BussinessSettingFragment();
            replaceFragment(bussinessSettingFragment);

        } else if (detailsType == DETAILS_TYPE.CURRENCY_SETTING) {
            CurrencyFragment currencyFragment = new CurrencyFragment();
            replaceFragment(currencyFragment);

        } else if (detailsType == DETAILS_TYPE.MYANMAR_FONT_SETTING) {
            MyanmarFontSetting myanmarFontSetting = new MyanmarFontSetting();
            replaceFragment(myanmarFontSetting);

        } else if (detailsType == DETAILS_TYPE.DOCUMENT_SETTING) {
            DocumentSettingFragment documentSettingFragment = new DocumentSettingFragment();
            replaceFragment(documentSettingFragment);

        } else if (detailsType == DETAILS_TYPE.PRINT_VOUCHER_SETTING) {
            PrintVoucherSettingFragment printVoucherSettingFragment = new PrintVoucherSettingFragment();
            replaceFragment(printVoucherSettingFragment);

        } else if (detailsType == DETAILS_TYPE.USER_SETTING) {
            UserRoleSettingFragment userRoleSettingFragment = new UserRoleSettingFragment();
            replaceFragment(userRoleSettingFragment);

        } else if (detailsType == DETAILS_TYPE.THEME_SETTING) {
            ThemeSettingListViewFragment themeSettingFragment = new ThemeSettingListViewFragment();
            replaceFragment(themeSettingFragment);

        } else if (detailsType == DETAILS_TYPE.TAX_SETUP) {
            TaxSetupFragment taxSetupFragment = new TaxSetupFragment();
            replaceFragment(taxSetupFragment);

        } else if (detailsType == DETAILS_TYPE.UNIT_SETTING) {
            UnitFragment unitFragment = new UnitFragment();
            replaceFragment(unitFragment);

        } else if (detailsType == DETAILS_TYPE.CATEGORY_SETTING) {
            CategoryFragmentDialog categoryFragment = new CategoryFragmentDialog();
            replaceFragment(categoryFragment);

        } else if (detailsType == DETAILS_TYPE.DATA_MANAGEMENT_SETTING) {
            DataManagementFragment dataManagementFragment = new DataManagementFragment();
            replaceFragment(dataManagementFragment);

        } else if (detailsType == DETAILS_TYPE.DAMAGED_DETAIL) {
            DamagedDetailNewFragment damagedDetailNewFragment = new DamagedDetailNewFragment();
            damagedDetailNewFragment.setArguments(bundle);
            replaceFragment(damagedDetailNewFragment);

        } else if (detailsType == DETAILS_TYPE.LOST_DETAIL) {
            LostDetailNewFragment lostDetailNewFragment = new LostDetailNewFragment();
            lostDetailNewFragment.setArguments(bundle);
            replaceFragment(lostDetailNewFragment);

        } else if (detailsType == DETAILS_TYPE.RECEIVABLE_DETAIL_VOUCHER) {
            ReceivableDetailFragment receivableDetailFragment = new ReceivableDetailFragment();
            receivableDetailFragment.setArguments(bundle);
            replaceFragment(receivableDetailFragment);

        } else if (detailsType == DETAILS_TYPE.PAYABLE_DETIL_VOUCHER) {
            PayableDetailFragment payableDetailFragment = new PayableDetailFragment();
            payableDetailFragment.setArguments(bundle);
            replaceFragment(payableDetailFragment);

        } else if (detailsType == DETAILS_TYPE.ADD_EDIT_CURRENCY) {
            AddEditCurrencyFragment addEditCurrencyFragment = new AddEditCurrencyFragment();
            addEditCurrencyFragment.setArguments(bundle);
            replaceFragment(addEditCurrencyFragment);

        } else if (detailsType == DETAILS_TYPE.ADD_EDIT_CATEGORY) {
            AddEditCategoryFragment addEditCategoryFragment = new AddEditCategoryFragment();
            addEditCategoryFragment.setArguments(bundle);
            replaceFragment(addEditCategoryFragment);

        } else if (detailsType == DETAILS_TYPE.ADD_EDIT_UNIT) {
            AddEditUnitFragment addEditUnitFragment = new AddEditUnitFragment();
            addEditUnitFragment.setArguments(bundle);
            replaceFragment(addEditUnitFragment);

        } else if (detailsType == DETAILS_TYPE.SALE_REPORT) {
            //Go to sale report list
            ReportTypeReportFragment reportTypeReportFragment = new ReportTypeReportFragment();
            reportTypeReportFragment.setArguments(bundle);
            replaceFragment(reportTypeReportFragment);

        } else if (detailsType == DETAILS_TYPE.PURCHASE_REPORT) {
            ReportTypeReportFragment reportTypeReportFragment = new ReportTypeReportFragment();
            reportTypeReportFragment.setArguments(bundle);
            replaceFragment(reportTypeReportFragment);

        } else if (detailsType == DETAILS_TYPE.INVENTORY_REPORT) {
            //Go to sale report list
            ReportTypeReportFragment reportTypeReportFragment = new ReportTypeReportFragment();
            reportTypeReportFragment.setArguments(bundle);
            replaceFragment(reportTypeReportFragment);

        } else if (detailsType == DETAILS_TYPE.OUTSTANDING_REPORT) {
            //Go to sale report list
            ReportTypeReportFragment reportTypeReportFragment = new ReportTypeReportFragment();
            reportTypeReportFragment.setArguments(bundle);
            replaceFragment(reportTypeReportFragment);

        } else if (detailsType == DETAILS_TYPE.FINANCAIL_REPORT) {
            //Go to sale report list
            ReportTypeReportFragment reportTypeReportFragment = new ReportTypeReportFragment();
            reportTypeReportFragment.setArguments(bundle);
            replaceFragment(reportTypeReportFragment);

        } else if (detailsType == DETAILS_TYPE.ADD_EDIT_TAX) {
            AddEditTaxFragment addEditTaxFragment = new AddEditTaxFragment();
            addEditTaxFragment.setArguments(bundle);
            replaceFragment(addEditTaxFragment);

        } else if (detailsType == DETAILS_TYPE.ADD_EDIT_USER) {
            AddEditUserFragment addEditUserFragment = new AddEditUserFragment();
            addEditUserFragment.setArguments(bundle);
            replaceFragment(addEditUserFragment);

        } else if (detailsType == DETAILS_TYPE.ADD_EDIT_SALE) {
            AddEditNewSaleFragment addEditNewSaleFragment = new AddEditNewSaleFragment();
            addEditNewSaleFragment.setArguments(bundle);
            replaceFragment(addEditNewSaleFragment);

        } else if (detailsType == DETAILS_TYPE.ADD_EDIT_PURCHASE) {
            AddEditNewPurchaseFragment addEditNewPurchaseFragment = new AddEditNewPurchaseFragment();
            addEditNewPurchaseFragment.setArguments(bundle);
            replaceFragment(addEditNewPurchaseFragment);

        } else if (detailsType == DETAILS_TYPE.ADD_EDIT_STOCK) {
            StockFormFragment stockFormFragment = new StockFormFragment();
            stockFormFragment.setArguments(bundle);
            replaceFragment(stockFormFragment);

        } else if (detailsType == DETAILS_TYPE.ADD_EDIT_CUSTOMER) {
            AddEditCustomerFragment addEditCustomerFragment = new AddEditCustomerFragment();
            addEditCustomerFragment.setArguments(bundle);
            replaceFragment(addEditCustomerFragment);

        } else if (detailsType == DETAILS_TYPE.ADD_EDIT_SUPPLIER) {
            AddEditSupplierFragment addEditSupplierFragment = new AddEditSupplierFragment();
            addEditSupplierFragment.setArguments(bundle);
            replaceFragment(addEditSupplierFragment);

        } else if (detailsType == DETAILS_TYPE.STOCK_LIST_PREVIEW) {
            StockListPreviewFragment stockListPreviewFragment = new StockListPreviewFragment();
            stockListPreviewFragment.setArguments(bundle);
            replaceFragment(stockListPreviewFragment);

        } else if (detailsType == DETAILS_TYPE.DISCOUNT_SETUP) {
            DiscountSetupFragment discountSetupFragment = new DiscountSetupFragment();
            replaceFragment(discountSetupFragment);

        } else if (detailsType == DETAILS_TYPE.ADD_EDIT_DISCOUNT) {
            AddEdiDiscountFragment addEdiDiscountFragment = new AddEdiDiscountFragment();
            addEdiDiscountFragment.setArguments(bundle);
            replaceFragment(addEdiDiscountFragment);

        } else if (detailsType == DETAILS_TYPE.ADD_EDIT_DISCOUNT) {
            AddEdiDiscountFragment addEdiDiscountFragment = new AddEdiDiscountFragment();
            addEdiDiscountFragment.setArguments(bundle);
            replaceFragment(addEdiDiscountFragment);

        } else if (detailsType == DETAILS_TYPE.CONTROL_SERVER) {
            ControlServerFragment controlServerFragment = new ControlServerFragment();
            replaceFragment(controlServerFragment);

        } else if ( detailsType == DETAILS_TYPE.SALE_PAYMENT)
        {
            //salesHeader = (SalesHeader) getIntent().getSerializableExtra("saleHeader");
            SalePaymentFragment paymentFragment = new SalePaymentFragment();
            paymentFragment.setArguments(bundle);
            replaceFragment(paymentFragment);
        }else if ( detailsType == DETAILS_TYPE.PURCHASE_PAYMENT)
        {
            //salesHeader = (SalesHeader) getIntent().getSerializableExtra("saleHeader");
            PurchasePaymentFragment paymentFragment = new PurchasePaymentFragment();
            paymentFragment.setArguments(bundle);
            replaceFragment(paymentFragment);
        }

        else {
            if (bundle != null && bundle.getSerializable("frag") != null) {
                Fragment fragment = (Fragment) bundle.getSerializable("frag");
                fragment.setArguments(bundle);
                replaceFragment(fragment);
            }

        }

    }

    // DO NOT REMOVE THIS!
    // WILL CAUSE java.io.NotSerializableException
    @Override
    protected void onSaveInstanceState(Bundle outState) {

    }

    public void replaceFragment(Fragment fragment) {

        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        fragmentTransaction.replace(R.id.frame_replace, fragment);

        fragmentTransaction.commit();

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == android.R.id.home) {
            Log.w("on item", "slelected");
            if (currentFragment instanceof ThemeSettingListViewFragment) {
                currentFragment = new SettingFragment();
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("navi", true);
                startActivity(intent);
                finishAffinity();
            } else {
                onBackPressed();
            }

            Log.w("herer Detail Activity", "On OPtion Item Selected here");
        }


        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {

        if (currentFragment instanceof ThemeSettingListViewFragment) {
            currentFragment = new SettingFragment();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("navi", true);
            startActivity(intent);
            finishAffinity();
        } else {
            finish();
            currentFragment = null;
            super.onBackPressed();
        }
        Log.e("herer Detail Activity", "On back press here");
    }

    public enum DETAILS_TYPE {
        PRINT_PREVIEW_SAIL,
        STOCK_DETAIL, SALE_DETAIL, PURCHASE_DETAIL, RECEIVABLE_PAYMENT_DETAIL, PAYABLE_PAYMENT_DETAIL, MYANMAR_FONT_SETTING,
        CUSTOMER_DETAIL, SUPPLIER_DETAIL, EXPENSE_DETAIL, INCOME_DETAIL, BUSSINESS_SETTING, CURRENCY_SETTING, DOCUMENT_SETTING, PRINT_VOUCHER_SETTING,
        USER_SETTING, TAX_SETUP, UNIT_SETTING, CATEGORY_SETTING, DATA_MANAGEMENT_SETTING, THEME_SETTING, DAMAGED_DETAIL, LOST_DETAIL,
        RECEIVABLE_DETAIL_VOUCHER, PAYABLE_DETIL_VOUCHER, ADD_EDIT_CURRENCY, ADD_EDIT_CATEGORY, SALE_REPORT, PURCHASE_REPORT, INVENTORY_REPORT, OUTSTANDING_REPORT, FINANCAIL_REPORT,
        ADD_EDIT_TAX, ADD_EDIT_USER, ADD_EDIT_SALE, ADD_EDIT_PURCHASE, ADD_EDIT_STOCK, ADD_EDIT_CUSTOMER, ADD_EDIT_SUPPLIER, ADD_EDIT_UNIT, STOCK_LIST_PREVIEW, ADD_EDIT_DISCOUNT, DISCOUNT_SETUP,
        CHANGE_PASSWORD, CONTROL_SERVER ,SALE_PAYMENT,PURCHASE_PAYMENT;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1:
                    AddEditNewSaleFragment addEditNewSaleFragment = new AddEditNewSaleFragment();
                    replaceFragment(addEditNewSaleFragment);
                    break;
                case 2:
                    AddEditNewPurchaseFragment addEditNewPurchaseFragment = new AddEditNewPurchaseFragment();
                    replaceFragment(addEditNewPurchaseFragment);
                    break;
            }
        }
    }
}
