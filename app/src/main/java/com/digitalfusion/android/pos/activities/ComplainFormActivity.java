package com.digitalfusion.android.pos.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetFileDescriptor;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.database.business.ApiManager;
import com.digitalfusion.android.pos.information.Feedback;
import com.digitalfusion.android.pos.network.ApiRetrofit;
import com.digitalfusion.android.pos.util.FileUtils;
import com.digitalfusion.android.pos.network.NetworkClient;
import com.digitalfusion.android.pos.util.GrantPermissionFromActivity;
import com.digitalfusion.android.pos.util.ImagePicker;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.FusionToast;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComplainFormActivity extends AppCompatActivity {
    private static final int REQUEST_SELECT_PICTURE = 0x01;
    Uri resultUri;
    ApiRetrofit apiRetrofit;
    private Toolbar toolbar;
    private TextView typeTextView;
    private RVAdapterForFilter rvAdapterForFilter;
    private ArrayList<String> typeList;
    private MaterialDialog typeMaterialDialog;
    private TextView messageTextView;
    private View view;
    private ImageView imageView;
    private GrantPermissionFromActivity grantPermission;
    private KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(POSUtil.getDefaultThemeNoActionBar(this));
        setContentView(R.layout.activity_complain_form);
        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        typeTextView = (TextView) findViewById(R.id.feedbackType);

        messageTextView = (TextView) findViewById(R.id.message);

        imageView = findViewById(R.id.img);
        grantPermission = new GrantPermissionFromActivity(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String feedbackStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.feedback}).getString(0);

        getSupportActionBar().setTitle(feedbackStr);


        typeList = new ArrayList<>();
        typeList.add("ERROR");
        typeList.add("Customer Service");
        typeList.add("Feature Request");

        rvAdapterForFilter = new RVAdapterForFilter(typeList);
        typeMaterialDialog = new MaterialDialog.Builder(this).adapter(rvAdapterForFilter, new LinearLayoutManager(this)).build();
        typeTextView.setText("ERROR");

        typeTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (typeTextView.getText().toString().equalsIgnoreCase("ERROR")) {

                    findViewById(R.id.photo).setVisibility(View.VISIBLE);

                } else {

                    findViewById(R.id.photo).setVisibility(View.GONE);
                }
            }
        });

        typeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeMaterialDialog.show();
            }
        });

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                typeTextView.setText(typeList.get(position));
                typeMaterialDialog.dismiss();
            }
        });
        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                view = v;
                proceedWithApi();
                //finish();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (grantPermission.permission()) {

                    onPickImage();

                }
            }
        });

        Log.w("here", "in complain");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (resultCode == Activity.RESULT_OK) {

            switch (requestCode) {

                case REQUEST_SELECT_PICTURE:

                    final Uri selectedUri = ImagePicker.getUriFromResult(this, resultCode, data);


                    if (selectedUri != null) {

                        startCropActivity(selectedUri);

                    } else {
                        TypedArray cannotRetrieve = getTheme().obtainStyledAttributes(new int[]{R.attr.cannot_retrieve_selected_image});

                        Toast.makeText(this, cannotRetrieve.getString(0), Toast.LENGTH_SHORT).show();

                    }

                    break;

                case UCrop.REQUEST_CROP:


                    resultUri = UCrop.getOutput(data);


                    BitmapFactory.Options options = new BitmapFactory.Options();

                    AssetFileDescriptor fileDescriptor = null;
                    try {

                        fileDescriptor = this.getContentResolver().openAssetFileDescriptor(resultUri, "r");

                    } catch (FileNotFoundException e) {

                        e.printStackTrace();

                    }

                    Bitmap actuallyUsableBitmap = BitmapFactory.decodeFileDescriptor(

                            fileDescriptor.getFileDescriptor(), null, options);


                    // Bitmap bitmap = ImagePicker.getCropImage(data,resultCode);
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), actuallyUsableBitmap);

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();

                    actuallyUsableBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);

                    imageView.setImageURI(resultUri);

                default:

                    super.onActivityResult(requestCode, resultCode, data);

                    break;

            }

        } else if (resultCode == UCrop.RESULT_ERROR) {

            handleCropError(data);

        }


    }

    private void handleCropError(@NonNull Intent result) {

        final Throwable cropError = UCrop.getError(result);

        if (cropError != null) {

            Log.e("ERROR", "handleCropError: ", cropError);

            Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_LONG).show();

        } else {

            Toast.makeText(this, "I Don't know this error", Toast.LENGTH_SHORT).show();

        }
    }

    public void onPickImage() {

        Intent chooseImageIntent = ImagePicker.getPickImageIntent(this);

        startActivityForResult(chooseImageIntent, REQUEST_SELECT_PICTURE);

    }

    private void startCropActivity(@NonNull Uri uri) {

        String destinationFileName = "temp.png";

        TypedArray allTrans = getTheme().obtainStyledAttributes(new int[]{R.attr.pickerAccentColor});

        TypedArray allTran2 = getTheme().obtainStyledAttributes(new int[]{R.attr.colorPrimaryDark});

        int valueInPixels = (int) getResources().getDimension(R.dimen.imageBtn_100dp);

        UCrop.Options options = new UCrop.Options();

        options.setToolbarColor(allTrans.getColor(0, 0));

        options.setStatusBarColor(allTran2.getColor(0, 0));

        options.setActiveWidgetColor(allTrans.getColor(0, 0));

        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)))

                .withOptions(options)

                .withAspectRatio(1, 1)

                .withMaxResultSize(valueInPixels, valueInPixels);

        uCrop.start(this);

    }

    private void proceedWithApi() {
        Log.w("IN SUMBIT", "TESTGING");
        View      processDialog = View.inflate(this, R.layout.process_dialog_custom_layout, null);
        ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
        imageView.setBackgroundResource(R.drawable.spin_animation);
        AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
        drawable.start();

        //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setDetailsLabel("")
                .setBackgroundColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);
        //final ApiRetrofit
        apiRetrofit = NetworkClient.createService(ApiRetrofit.class, "digital_fusion", "digital_fusion");
        //        apiRetrofit =  NetworkClient.getClient().create(ApiRetrofit.class);
        final Feedback feedback = new Feedback();
        feedback.setFeedbackType(typeTextView.getText().toString());
        feedback.setCreatedDate(new Date());
        feedback.setDeleted(false);
        feedback.setUid("0001");
        feedback.setReportedCustomer("hnin hsu");
        feedback.setMessage(messageTextView.getText().toString());
        hud.show();
        Call<Feedback> feedbackCall = apiRetrofit.giveFeedback(feedback);
        feedbackCall.enqueue(new Callback<Feedback>() {
            @Override
            public void onResponse(Call<Feedback> call, Response<Feedback> response) {

                Log.e("Response Code", response.code() + " code");

                Log.e("feedback", feedback.toString());
                messageTextView.setText("");
                FusionToast.toast(ComplainFormActivity.this, FusionToast.TOAST_TYPE.SUCCESS);
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<Feedback> call, Throwable t) {
                t.printStackTrace();
                hud.dismiss();
                FusionToast.toast(ComplainFormActivity.this, FusionToast.TOAST_TYPE.ERROR);
            }
        });

        File file = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            file = FileUtils.getFile(this, resultUri);
        }

        if (file != null) {
            Log.w("File is not null", MediaType.parse(FileUtils.getMimeType(file)).toString() + " :D");
            // create RequestBody instance from file
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse(FileUtils.getMimeType(file)),
                            file
                    );

            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("file", file.getName(), requestFile);

            // add another part within the multipart request
            ApiManager apiManager        = new ApiManager(this);
            String     descriptionString = apiManager.getRegistration().getUserId();
            RequestBody description =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, descriptionString);

            Log.w("Hello File Size", file.getName());


            Call<ResponseBody> feedbackSS = apiRetrofit.feedbackScreenshot(description, body);
            feedbackSS.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    FusionToast.toast(ComplainFormActivity.this, FusionToast.TOAST_TYPE.SUCCESS);

                    if (response.code() == 200) {
                        Log.w("HERER", "success");
                    } else {
                        Log.w("A last", "Response" + response.code());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                    Log.w("ON FAIL", "FAIL SSS UPload");
                    FusionToast.toast(ComplainFormActivity.this, FusionToast.TOAST_TYPE.ERROR);
                }
            });
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == android.R.id.home) {

            super.onBackPressed();

            Log.w("here ComplainFActivity", "OnItem Selected here");
        }


        return false;
    }
}
