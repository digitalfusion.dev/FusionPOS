package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.digitalfusion.android.pos.database.model.ReportItem;

import java.util.List;

/**
 * Created by MD002 on 12/28/16.
 */

public class ParentRVAdapterForReports extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<ReportItem> reportItemList;
    public RecyclerView.ViewHolder view;
    public boolean showLoader = false;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return view;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return reportItemList.size();
    }

    public void setShowLoader(boolean showLoader) {
        this.showLoader = showLoader;

    }
}
