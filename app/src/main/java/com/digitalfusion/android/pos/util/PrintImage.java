package com.digitalfusion.android.pos.util;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class PrintImage {
    private static /* synthetic */ int[] $SWITCH_TABLE$mate$bluetoothprint$PrintImage$dither;
    private int bm_height;
    private Bitmap bm_print;
    private Bitmap bm_source;
    private int bm_width;
    private int[] pixels;

    public PrintImage(Bitmap _source) {
        this.bm_source = _source;
        int width  = this.bm_source.getWidth();
        int height = this.bm_source.getHeight();
        if (width > 576) {
            this.bm_source = getResizedBitmap(this.bm_source, 576, (int) Math.floor((double) (((float) height) * (((float) 576) / ((float) width)))));
        } else if (Math.floor((double) (width / 8)) != ((double) width) / 8.0d) {
            int newWidth = (int) Math.floor((double) (width / 8));
            this.bm_source = getResizedBitmap(this.bm_source, newWidth, (int) Math.floor((double) (((float) height) * (((float) newWidth) / ((float) width)))));
        }
        this.bm_width = this.bm_source.getWidth();
        this.bm_height = this.bm_source.getHeight();
        this.pixels = new int[(width * height)];
        this.Dither_Floyd_Steinberg(128);
    }

    static /* synthetic */ int[] $SWITCH_TABLE$mate$bluetoothprint$PrintImage$dither() {
        int[] iArr = $SWITCH_TABLE$mate$bluetoothprint$PrintImage$dither;
        if (iArr == null) {
            iArr = new int[dither.values().length];
            try {
                iArr[dither.floyd_steinberg.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[dither.matrix_2x2.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[dither.matrix_4x4.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[dither.threshold.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$mate$bluetoothprint$PrintImage$dither = iArr;
        }
        return iArr;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int    width       = bm.getWidth();
        int    height      = bm.getHeight();
        float  scaleWidth  = ((float) newWidth) / ((float) width);
        float  scaleHeight = ((float) newHeight) / ((float) height);
        Matrix matrix      = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public Bitmap getPrintImage() {
        return this.bm_print;
    }

    public Bitmap getSourceImage() {
        return this.bm_source;
    }

    public byte[] getPrintImageData() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte                  wh  = (byte) ((this.bm_width / 8) / 256);
        byte                  wl  = (byte) ((this.bm_width / 8) % 256);
        byte                  hh  = (byte) (this.bm_height / 256);
        byte                  hl  = (byte) (this.bm_height % 256);
        try {
            byte[] bArr = new byte[8];
            bArr[0] = (byte) 29;
            bArr[1] = (byte) 118;
            bArr[2] = (byte) 48;
            bArr[4] = wl;
            bArr[5] = wh;
            bArr[6] = hl;
            bArr[7] = hh;
            out.write(bArr);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int y = 0; y < this.bm_height; y++) {
            for (int xbyte = 0; xbyte < this.bm_width / 8; xbyte++) {
                int PrintByte = 0;
                for (int bit = 0; bit < 8; bit++) {
                    PrintByte <<= 1;
                    if (this.pixels[((xbyte * 8) + bit) + (this.bm_width * y)] == -16777216) {
                        PrintByte |= 1;
                    }
                }
                out.write((byte) PrintByte);
            }
        }
        return out.toByteArray();
    }

    public int getWidth() {
        return this.bm_width;
    }

    public int getHeight() {
        return this.bm_height;
    }

    public void Dither_Floyd_Steinberg(int bright_value) {
        int x;
        int w = this.bm_width;
        int h = this.bm_height;
        bright_value -= 128;
        if (this.bm_print != null) {
            this.bm_print.recycle();
        }
        this.bm_print = this.bm_source.copy(this.bm_source.getConfig(), true);
        this.bm_print.getPixels(this.pixels, 0, this.bm_width, 0, 0, this.bm_width, this.bm_height);
        w++;
        h++;
        int[] tab = new int[(w * h)];
        int   y   = 0;
        while (y < h - 1) {
            for (x = 0; x < w - 1; x++) {
                if (x == w - 1 || y == h - 1) {
                    tab[(w * y) + x] = 0;
                } else {
                    int Pixel = this.pixels[(this.bm_width * y) + x];
                    tab[(w * y) + x] = ((((Color.red(Pixel) * 76) + (Color.blue(Pixel) * 151)) + (Color.green(Pixel) * 29)) / 256) + bright_value;
                }
            }
            y++;
        }
        for (y = 0; y < h - 2; y++) {
            for (x = 0; x < w - 2; x++) {
                int g;
                int offset = x + (y * w);
                int gc     = tab[offset];
                if (gc < 128) {
                    g = 0;
                } else {
                    g = 255;
                }
                gc -= g;
                tab[offset] = g;
                tab[offset + 1] = tab[offset + 1] + ((gc * 7) / 16);
                tab[(offset - 1) + w] = tab[(offset - 1) + w] + ((gc * 3) / 16);
                tab[offset + w] = tab[offset + w] + ((gc * 5) / 16);
                tab[(offset + 1) + w] = tab[(offset + 1) + w] + (gc / 16);
            }
        }
        for (y = 0; y < h - 1; y++) {
            for (x = 0; x < w - 1; x++) {
                int l;
                if (tab[(w * y) + x] == 0) {
                    l = -16777216;
                } else {
                    l = -1;
                }
                this.pixels[(this.bm_width * y) + x] = l;
            }
        }
        this.bm_print.setPixels(this.pixels, 0, this.bm_width, 0, 0, this.bm_width, this.bm_height);
    }

    public void Dither_Threshold(int bright_value) {
        bright_value -= 128;
        if (this.bm_print != null) {
            this.bm_print.recycle();
        }
        this.bm_print = this.bm_source.copy(this.bm_source.getConfig(), true);
        this.bm_print.getPixels(this.pixels, 0, this.bm_width, 0, 0, this.bm_width, this.bm_height);
        for (int y = 0; y < this.bm_height; y++) {
            for (int x = 0; x < this.bm_width; x++) {
                int Pixel = this.pixels[(this.bm_width * y) + x];
                this.pixels[(this.bm_width * y) + x] = ((((Color.red(Pixel) * 76) + (Color.blue(Pixel) * 150)) + (Color.green(Pixel) * 30)) / 256) + bright_value < 128 ? -16777216 : -1;
            }
        }
        this.bm_print.setPixels(this.pixels, 0, this.bm_width, 0, 0, this.bm_width, this.bm_height);
    }

    public void Dither_Matrix_2x2(int bright_value) {
        int[] matrix = new int[]{32, 160, 222, 96};
        bright_value -= 128;
        if (this.bm_print != null) {
            this.bm_print.recycle();
        }
        this.bm_print = this.bm_source.copy(this.bm_source.getConfig(), true);
        this.bm_print.getPixels(this.pixels, 0, this.bm_width, 0, 0, this.bm_width, this.bm_height);
        for (int y = 0; y < this.bm_height; y++) {
            for (int x = 0; x < this.bm_width; x++) {
                int Pixel = this.pixels[(this.bm_width * y) + x];
                this.pixels[(this.bm_width * y) + x] = ((((Color.red(Pixel) * 76) + (Color.blue(Pixel) * 150)) + (Color.green(Pixel) * 30)) / 256) + bright_value < matrix[(x % 2) + ((y % 2) * 2)] ? -16777216 : -1;
            }
        }
        this.bm_print.setPixels(this.pixels, 0, this.bm_width, 0, 0, this.bm_width, this.bm_height);
    }

    public void Dither_Matrix_4x4(int bright_value) {
        int[] matrix = new int[]{15, 143, 47, 175, 207, 79, 239, 111, 63, 191, 31, 159, 255, 127, 223, 95};
        bright_value -= 128;
        if (this.bm_print != null) {
            this.bm_print.recycle();
        }
        this.bm_print = this.bm_source.copy(this.bm_source.getConfig(), true);
        this.bm_print.getPixels(this.pixels, 0, this.bm_width, 0, 0, this.bm_width, this.bm_height);
        for (int y = 0; y < this.bm_height; y++) {
            for (int x = 0; x < this.bm_width; x++) {
                int Pixel = this.pixels[(this.bm_width * y) + x];
                this.pixels[(this.bm_width * y) + x] = ((((Color.red(Pixel) * 76) + (Color.blue(Pixel) * 150)) + (Color.green(Pixel) * 30)) / 256) + bright_value < matrix[(x % 4) + ((y % 4) * 4)] ? -16777216 : -1;
            }
        }
        this.bm_print.setPixels(this.pixels, 0, this.bm_width, 0, 0, this.bm_width, this.bm_height);
    }

    public enum dither {
        floyd_steinberg,
        matrix_2x2,
        matrix_4x4,
        threshold
    }
}
