package com.digitalfusion.android.pos.fragments.inventoryfragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForCategoryList;
import com.digitalfusion.android.pos.database.business.CategoryManager;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.views.FusionToast;

import java.util.List;

/**
 * Created by MD003 on 8/24/16.
 */
public class CategoryFragment extends Fragment {

    private View mainLayoutView;
    private TextInputEditText categoryNameTextInputEditText;
    private TextInputLayout categoryTextInputLayout;
    private TextInputEditText categoryDescTextInputEditText;

    private Button categoryAddButton;
    private RecyclerView recyclerView;


    private RVSwipeAdapterForCategoryList rvSwipeAdapterForCategoryList;
    private List<Category> categoryList;

    private Context context;
    private CategoryManager categoryManager;
    private String categoryName;
    private String categoryDesc;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.category_new, null);

        context = getContext();


        final TypedArray typedArray = context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_category_added_successfully});


        categoryManager = new CategoryManager(context);
        categoryList = categoryManager.getAllCategories();

        loadUI();
        configRecyclerView();

        MainActivity.setCurrentFragment(this);

        categoryAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkCategoryValidation()) {
                    categoryManager.addNewCategory(categoryName, categoryDesc, null, 0);
                    refreshCategoryList();
                    clearInput();
                    //  POSUtil.showSnackBar(mainLayoutView, typedArray.getString(0));
                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                }
            }
        });


        return mainLayoutView;
    }

    public void refreshCategoryList() {
        categoryList = categoryManager.getAllCategories();
        rvSwipeAdapterForCategoryList.setCategoryList(categoryList);
        rvSwipeAdapterForCategoryList.notifyDataSetChanged();
    }


    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        rvSwipeAdapterForCategoryList = new RVSwipeAdapterForCategoryList(categoryList);
        recyclerView.setAdapter(rvSwipeAdapterForCategoryList);

    }

    private void clearInput() {

        categoryDescTextInputEditText.setText(null);
        categoryNameTextInputEditText.setText(null);
        categoryTextInputLayout.setError(null);

    }

    private boolean checkCategoryValidation() {
        boolean status = true;

        if (categoryNameTextInputEditText.getText().toString().trim().length() < 0) {
            status = false;
        }

        categoryName = categoryNameTextInputEditText.getText().toString().trim();
        categoryDesc = categoryDescTextInputEditText.getText().toString().trim();

        return status;

    }


    public void loadUI() {

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.category_list_rv);
        categoryNameTextInputEditText = (TextInputEditText) mainLayoutView.findViewById(R.id.category_name_et);
        categoryTextInputLayout = (TextInputLayout) mainLayoutView.findViewById(R.id.category_name_TIL);
        categoryDescTextInputEditText = (TextInputEditText) mainLayoutView.findViewById(R.id.category_desc_et);

        categoryAddButton = (Button) mainLayoutView.findViewById(R.id.category_add);
    }


}
