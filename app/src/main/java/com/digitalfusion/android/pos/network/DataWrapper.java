package com.digitalfusion.android.pos.network;

import com.digitalfusion.android.pos.database.model.Currency;
import com.digitalfusion.android.pos.database.model.BusinessSetting;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.database.model.Discount;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.database.model.Tax;
import com.digitalfusion.android.pos.database.model.Unit;
import com.digitalfusion.android.pos.database.model.User;

import java.util.List;

/**
 * Created by MD003 on 2/27/18.
 */

public class DataWrapper {

    private List<StockItem> stockItemList;
    private List<Tax> taxList;
    private List<Unit> unitList;
    private List<Category> categoryList;
    private BusinessSetting businessSetting;
    private List<Discount> discountList;
    private List<Currency> currencyList;
    private List<Customer> customerList;
    private List<User> userList;

    public List<StockItem> getStockItemList() {
        return stockItemList;
    }

    public void setStockItemList(List<StockItem> stockItemList) {
        this.stockItemList = stockItemList;
    }

    public List<Tax> getTaxList() {
        return taxList;
    }

    public void setTaxList(List<Tax> taxList) {
        this.taxList = taxList;
    }

    public List<Unit> getUnitList() {
        return unitList;
    }

    public void setUnitList(List<Unit> unitList) {
        this.unitList = unitList;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public BusinessSetting getBusinessSetting() {
        return businessSetting;
    }

    public void setBusinessSetting(BusinessSetting businessSetting) {
        this.businessSetting = businessSetting;
    }

    public List<Discount> getDiscountList() {
        return discountList;
    }

    public void setDiscountList(List<Discount> discountList) {
        this.discountList = discountList;
    }

    public List<Currency> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<Currency> currencyList) {
        this.currencyList = currencyList;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
