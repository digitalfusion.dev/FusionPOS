package com.digitalfusion.android.pos.fragments.reportfragments;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForReorderQty;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.views.Spinner;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SimpleReportFragment extends Fragment implements Serializable {


    private String reportName;
    private View mainLayoutView;
    private Context context;
    private RecyclerView recyclerView;
    private ReportManager reportManager;
    private List<ReportItem> reportItemList;
    private ParentRVAdapterForReports rvAdapterForReport;
    private String startDate;
    private String endDate;
    private int startLimit;
    private int endLimit;
    private boolean shouldLoad = true;
    public SimpleReportFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_simple_report2, container, false);
        if (getArguments() != null) {
            reportName = getArguments().getString("reportType");
        }
        if (reportName.equalsIgnoreCase("reorder qty")) {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.low_stock_product}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        }

        loadIngUI();

        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        initializeVariables();

        new LoadProgressDialog().execute("");

    }

    private void loadIngUI() {
        recyclerView = mainLayoutView.findViewById(R.id.simple_report_recycler_view);
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);
        setDatesAndLimits();
        recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {
                if (shouldLoad) {
                    rvAdapterForReport.setShowLoader(true);
                    loadmore();
                }
            }
        });

    }

    private void initializeList() {
        if (reportName.equalsIgnoreCase("reorder qty")) {
            reportItemList = reportManager.lowStockList(startLimit, endLimit);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void setDatesAndLimits() {
        //if (!isTwelve){
        startDate = Calendar.getInstance().get(Calendar.YEAR) + "0101";
        //Log.e("start", startDate );
        endDate = Calendar.getInstance().get(Calendar.YEAR) + "1231";
        Log.e("start", endDate);
        //}
        startLimit = 0;
        endLimit = 10;
    }

    private void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        if (reportName.equalsIgnoreCase("reorder qty")) {
            rvAdapterForReport = new RVAdapterForReorderQty(reportItemList);
            recyclerView.setAdapter(rvAdapterForReport);
        }
        //recyclerView.setAdapter(rvAdapterForValuation);
    }

    public void loadmore() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                reportItemList.addAll(loadMore(reportItemList.size(), 9));
                rvAdapterForReport.setShowLoader(false);

                configRecyclerView();
            }
        }, 500);
    }

    public List<ReportItem> loadMore(int startLimit, int endLimit) {
        List<ReportItem> reportItemList = new ArrayList<>();
        if (reportName.equalsIgnoreCase("reorder qty")) {
            reportItemList = reportManager.lowStockList(startLimit, endLimit);
        }
        return reportItemList;
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private Spinner spinner = new Spinner(context);

        @Override
        protected String doInBackground(String... params) {
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            spinner.show();
        }

        @Override
        protected void onPostExecute(String a) {
            configRecyclerView();
            spinner.dismiss();
        }

    }

}
