package com.digitalfusion.android.pos.information.wrapper;

public class SubscriptionsRequest {
    private String userId;
    private int year;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "SubscriptionsRequest{" +
                "userId='" + userId + '\'' +
                ", year=" + year +
                '}';
    }
}
