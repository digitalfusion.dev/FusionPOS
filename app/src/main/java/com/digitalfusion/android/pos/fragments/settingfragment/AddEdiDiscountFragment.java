package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.model.Discount;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.FusionToast;

public class AddEdiDiscountFragment extends Fragment {
    public static final String KEY = "discount";
    public Discount discount;
    public boolean isEdit = false;
    private EditText discountNameTextInputEditText;
    private EditText discountAmountTextInputEditText;
    private EditText discountDescTextInputEditText;
    private RadioGroup discountTypeRadioGroup;
    private RadioButton amountRadioButton;
    private RadioButton percentRadioButton;
    private CheckBox useDefaultCheckBox;
    private SettingManager settingManager;
    private View mainLayoutView;
    private int isPercent;

    private boolean isDefault = false;

    //For value
    private String discountName;

    private Double discountAmount;

    private String discountDesc;

    private String discountType = "Inclusive";

    private Integer useDefault;

    private Context context;

    private TextView symbolTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.fragment_add_edi_discount, container, false);

        settingManager = new SettingManager(getContext());

        setHasOptionsMenu(true);

        loadUI();

        amountRadioButton.setText(AppConstant.CURRENCY);

        isPercent = 1;

        context = getContext();

        useDefaultCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                isDefault = isChecked;

            }
        });
        if (getArguments().getSerializable(KEY) == null) {
            String newDiscount = context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_discount}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(newDiscount);
            isEdit = false;
        } else {
            String editDiscount = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_discount}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(editDiscount);
            isEdit = true;
            discount = (Discount) getArguments().get(KEY);
            initializeOldData();
        }


        return mainLayoutView;

    }

    @Override
    public void onResume() {
        super.onResume();


        useDefaultCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                isDefault = isChecked;
                if (isChecked) {
                    useDefault = 1;
                } else {
                    useDefault = 0;
                }
            }
        });
        discountTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.e("checkek " + checkedId, amountRadioButton.getId() + " ioajsgkja " + percentRadioButton.getId());
                if (amountRadioButton.getId() == checkedId) {
                    symbolTextView.setText(AppConstant.CURRENCY);
                    isPercent = 0;
                } else if (percentRadioButton.getId() == checkedId) {
                    symbolTextView.setText("%");
                    isPercent = 1;
                }

            }
        });

    }

    private void takeValueFromView() {

        discountName = discountNameTextInputEditText.getText().toString().trim();

        discountAmount = Double.parseDouble(discountAmountTextInputEditText.getText().toString().trim());

        discountDesc = discountDescTextInputEditText.getText().toString().trim();

        /*if (percentRadioButton.isChecked()){
            isPercent = 1;
        } else {
            isPercent = 0;
        }*/

        if (isDefault) {

            useDefault = 1;

        } else {

            useDefault = 0;

        }

    }

    private void initializeOldData() {


        discountName = discount.getName();
        discountDesc = discount.getDescription();
        discountAmount = discount.getAmount();

        discountNameTextInputEditText.setText(discountName);
        discountDescTextInputEditText.setText(discountDesc);
        discountAmountTextInputEditText.setText(
                POSUtil.doubleToString(discount.getAmount()));
        useDefaultCheckBox.setChecked(discount.isActive());

        if (discount.getIsPercent() == 0) {
            amountRadioButton.setChecked(true);
        } else {
            percentRadioButton.setChecked(true);
        }
    }

    private void clearData() {


        discountNameTextInputEditText.setText(null);
        discountNameTextInputEditText.setError(null);
        discountAmountTextInputEditText.setText(null);
        discountAmountTextInputEditText.setError(null);
        discountDescTextInputEditText.setText(null);
        discountDescTextInputEditText.setError(null);
        useDefaultCheckBox.setChecked(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save) {

            if (!isEdit) {
                saveCurrency();
            } else {

                if (checkValidation()) {
                    takeValueFromView();

                    Log.e("use default", isPercent + " SS");

                    settingManager.updateDiscount(discountName, discountAmount, isPercent, discountDesc, useDefault, discount.getId());
                    getActivity().onBackPressed();
                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                }
            }

        }
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void saveCurrency() {
        if (checkValidation()) {

            takeValueFromView();

            settingManager.addNewDiscount(discountName, discountAmount, isPercent, discountDesc, useDefault);

            clearData();
            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
            getActivity().onBackPressed();
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);
       /* TypedArray save=context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        FontTextView fontTextView= (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.menu_text,null);

        menu.getItem(0).setActionView(fontTextView);

        fontTextView.setText(save.getString(0));


        fontTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menu.getItem(0));
            }
        });*/


        super.onCreateOptionsMenu(menu, inflater);

    }

    private boolean checkValidation() {

        boolean status = true;

        if (discountNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;

            String discountName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_discount_name}).getString(0);
            discountNameTextInputEditText.setError(discountName);

        }

        if (discountAmountTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;

            String discountAmount = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_discount_amount}).getString(0);
            discountAmountTextInputEditText.setError(discountAmount);

        }

        return status;

    }

    private void loadUI() {
        discountNameTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.discount_name_in_add_discount_et);

        discountAmountTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.discount_amount_in_add_discount_et);

        discountDescTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.discount_desc_in_add_discount_et);


        discountTypeRadioGroup = (RadioGroup) mainLayoutView.findViewById(R.id.discount_type_in_add_discount_rg);

        amountRadioButton = (RadioButton) mainLayoutView.findViewById(R.id.amount_radio_button);

        percentRadioButton = (RadioButton) mainLayoutView.findViewById(R.id.percent_radio_button);

        useDefaultCheckBox = (CheckBox) mainLayoutView.findViewById(R.id.use_default_in_add_discount);

        symbolTextView = (TextView) mainLayoutView.findViewById(R.id.symbol_text_view);
    }
}
