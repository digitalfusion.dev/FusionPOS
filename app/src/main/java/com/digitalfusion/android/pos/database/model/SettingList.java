package com.digitalfusion.android.pos.database.model;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * Created by MD003 on 8/24/16.
 */
public class SettingList implements Serializable {

    private String settingName;
    private Drawable settingImage;

    public SettingList() {
    }

    public SettingList(Drawable settingImage, String settingName) {
        this.settingImage = settingImage;
        this.settingName = settingName;
    }

    public String getSettingName() {
        return settingName;
    }

    public void setSettingName(String settingName) {
        this.settingName = settingName;
    }

    public Drawable getSettingImage() {
        return settingImage;
    }

    public void setSettingImage(Drawable settingImage) {
        this.settingImage = settingImage;
    }
}
