package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForMonthAmtReport;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.DefaultYAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class BarChartWithYearViewFragment extends Fragment implements Serializable {
    public static int[] colorArr = new int[]{
            ColorTemplate.rgb("#03A9F4"),
            ColorTemplate.rgb("#CDDC39"),
            ColorTemplate.rgb("#FF9800"),
            ColorTemplate.rgb("#e040fb"),
            ColorTemplate.rgb("#80DEEA"),
            ColorTemplate.rgb("#3F51B5"),
            ColorTemplate.rgb("#FFEB3B"),
            ColorTemplate.rgb("#00BFA5"),
            ColorTemplate.rgb("#757575"),
            ColorTemplate.rgb("#F44336"),
            ColorTemplate.rgb("#2E7D32"),
            ColorTemplate.rgb("#795548")};
    protected String[] mMonths = new String[]{
            " Jan", " Feb", " Mar", " Apr", " May", " Jun", " Jul", " Aug", " Sep", " Oct", " Nov", " Dec"
    };
    Layout titleLayout;
    private String reportName;
    private View mainLayoutView;
    private Context context;
    private TextView dateFilterTextView;
    private RVAdapterForFilter rvAdapterForDateFilter;
    private MaterialDialog dateFilterDialog;
    private List<String> filterList;
    private RecyclerView recyclerView;
    private ParentRVAdapterForReports rvAdapterForReport;
    private BarChart barChart;
    private ReportManager reportManager;
    private List<ReportItem> reportItemList;
    //private ReportItem reportItemView;
    private List<Double> yValList;
    private String startDate;
    private String endDate;
    private int startLimit;
    private int endLimit;
    private ArrayList<String> xValues;
    private List<BarEntry> yValues;
    private String thisYear;
    private String lastYear;
    private String last12Month;
    private String orderby;
    private boolean isTwelve;
    private String[] dateFilterArr;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_bar_chart_with_year_view, container, false);


        if (getArguments() != null) {
            reportName = getArguments().getString("reportType");
        }
        switch (reportName) {
            case "monthly gross profit":
                String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.monthly_sales_profit_gross}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                break;
            case "Income":
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.monthly_income}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                break;
            case "Expense":
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.monthly_expense}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                break;
            case "sales tax report":
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.sales_tax_report}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                break;
        }

        loadIngUI();

        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        initializeVariables();


        configFilter();

        buildDateFilterDialog();

        setDateFilterTextView(thisYear);

        clickListeners();


        new LoadProgressDialog().execute("");

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void clickListeners() {
        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                dateFilterDialog.dismiss();

                if (filterList.get(position).equalsIgnoreCase(thisYear)) {
                    isTwelve = false;
                    orderby = "asc";
                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    int year = Calendar.getInstance().get(Calendar.YEAR);
                    for (int i = 0; i < 12; i++) {
                        dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
                    }


                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {
                    isTwelve = false;
                    orderby = "asc";

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    int year = Calendar.getInstance().get(Calendar.YEAR) - 1;
                    dateFilterArr = new String[12];
                    for (int i = 0; i < 12; i++) {
                        dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
                    }
                } else if (filterList.get(position).equalsIgnoreCase(last12Month)) {
                    orderby = "asc";
                    isTwelve = true;

                    startDate = DateUtility.getLast12MonthStartDate();

                    endDate = DateUtility.getLast12MotnEndDate();

                    Log.e("1 ", startDate + "  " + endDate);

                    dateFilterArr = new String[12];
                    int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
                    int year  = Calendar.getInstance().get(Calendar.YEAR);
                    for (int i = 0; i < 12; i++) {
                        if (month < 1) {
                            year -= 1;
                            month = 12;
                        }
                        dateFilterArr[i] = "(" + (month) + "," + year + ")";
                        month -= 1;
                    }


                }

                new LoadProgressDialog().execute("");

                setDateFilterTextView(filterList.get(position));
            }
        });
    }

    private void configFilter() {
        TypedArray mthisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year});

        TypedArray mlastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year});

        thisYear = mthisYear.getString(0);

        lastYear = mlastYear.getString(0);


        last12Month = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_12_month}).getString(0);


        filterList = new ArrayList<>();

        filterList.add(thisYear);

        filterList.add(lastYear);

        filterList.add(last12Month);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    private void loadIngUI() {

        dateFilterTextView = (TextView) mainLayoutView.findViewById(R.id.date_filter);

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.bar_chart_recycler_view);

        barChart = (BarChart) mainLayoutView.findViewById(R.id.bar_chart);

    }

    private void initializeVariables() {

        reportItemList = new ArrayList<>();

        reportManager = new ReportManager(context);

        setDatesAndLimits();

    }

    private void setDateFilterTextView(String msg) {

        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {

        String filter = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);
        dateFilterDialog = new MaterialDialog.Builder(context).

                title(filter)

                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

    }


    private void initializeList() {
        switch (reportName) {
            case "monthly gross profit":
                reportManager.createPurchasePriceTempTable(startDate, endDate);
                reportItemList = reportManager.monthlyGrossProfit(startDate, endDate, dateFilterArr, orderby);
                reportManager.dropPurchasePriceTempTable();
                break;
            case "Income":
            case "Expense":
                reportItemList = reportManager.monthlyIncomeExpenseTransactions(reportName, startDate, endDate, dateFilterArr, orderby);
                break;
            case "sales tax report":
                reportItemList = reportManager.monthlySalesTax(startDate, endDate, dateFilterArr, orderby);
                break;
        }

        /*if (reportName.equalsIgnoreCase("top sale by qty")){
            reportItemList = reportManager.topSalesByQty(startDate, endDate, startLimit, endLimit);
        } else if (reportName.equalsIgnoreCase("bottom sale items")){
            reportItemList = reportManager.bottomSalesByQty(startDate, endDate, startLimit, endLimit);
        } else if (reportName.equalsIgnoreCase("top sales by customer")) {
            reportItemList = reportManager.topSalesCustomers(startDate, endDate, startLimit, endLimit);
        } else if (reportName.equalsIgnoreCase("top sale by products")) {
            reportItemList = reportManager.topSalesByProducts(startDate, endDate, startLimit, endLimit);
        } else if (reportName.equalsIgnoreCase("top outstanding suppliers")) {
            reportItemList = reportManager.topOutstandingSupplier(startLimit, endLimit);
        } else if (reportName.equalsIgnoreCase("bottom sales by products")){
            reportItemList = reportManager.bottomSalesByProducts(startDate, endDate, startLimit, endLimit);
        } else if (reportName.equalsIgnoreCase("top suppliers")) {
            reportItemList = reportManager.topSuppliers(startDate, endDate, startLimit, endLimit);
        }*/

        //Log.e("size",reportItemList.size()+" nsss");
    }

    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        /*if (reportName.equalsIgnoreCase("top sale by qty") || reportName.equalsIgnoreCase("bottom sale items")) {
            rvAdapterForBarChartReportWithNameQty = new RVAdapterForBarChartReportWithNameQty(reportItemList);
            recyclerView.setAdapter(rvAdapterForBarChartReportWithNameQty);
        }else if (reportName.equalsIgnoreCase("top sales by customer") || reportName.equalsIgnoreCase("top sale by products") ||
                reportName.equalsIgnoreCase("top outstanding suppliers") || reportName.equalsIgnoreCase("bottom sales by products")
                || reportName.equalsIgnoreCase("top suppliers")){
            rvAdapterForBarChartReportWithNameAmt = new RVAdapterForBarChartReportWithNameAmt(reportItemList);
            recyclerView.setAdapter(rvAdapterForBarChartReportWithNameAmt);
        }*/
        rvAdapterForReport = new RVAdapterForMonthAmtReport(reportItemList);
        recyclerView.setAdapter(rvAdapterForReport);

    }

    private void setDatesAndLimits() {
        startDate = Calendar.getInstance().get(Calendar.YEAR) + "0101";
        endDate = Calendar.getInstance().get(Calendar.YEAR) + "1231";
        startLimit = 0;
        endLimit = 10;
        orderby = "asc";
        isTwelve = false;
        dateFilterArr = new String[12];
        int year = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 0; i < 12; i++) {
            dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
        }
    }

    public void settingBarChart() {

        // barChart.setDescriptionPosition(Float.parseFloat(XAxis.XAxisPosition.TOP.toString()),Float.parseFloat(YAxis.AxisDependency.RIGHT.toString()));
        barChart.setDrawBorders(false);
        barChart.setPinchZoom(true);
        barChart.setPivotY(10);
        barChart.setDrawBarShadow(false);
        barChart.setMaxVisibleValueCount(30);
        barChart.setDescription("");
        barChart.setDrawGridBackground(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setTouchEnabled(false);
        barChart.animateXY(1400, 1400, Easing.EasingOption.EaseInBack, Easing.EasingOption.EaseInBack);
        barChart.getLegend().setEnabled(false);
        if (POSUtil.isLabelWhite(context)) {
            barChart.getAxisLeft().setTextColor(Color.WHITE);
            barChart.getXAxis().setTextColor(Color.WHITE);
        }
        Legend l = barChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setYOffset(2f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f);
        l.setXOffset(3f);

        configXAxis();

        configYAxis();

        setBarDataToChart();

        barChart.invalidate();
    }

    private void setBarDataToChart() {
        xValues = new ArrayList<>();
        yValList = new ArrayList<>();

        xValues = new ArrayList<String>();
        if (isTwelve) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -11);
            int cal = calendar.get(Calendar.MONTH);
            for (int i = 0; i < 12; i++) {
                Log.e("cal", cal + " endar");
                xValues.add(mMonths[cal]);
                cal += 1;
                if (cal > 11) {
                    cal = 0;
                }

            }
        } else {
            for (int i = 0; i < 12; i++) {
                //Log.e("ccccc",cal+"");
                xValues.add(mMonths[i]);
            }
        }

        for (ReportItem reportItem : reportItemList) {
            //Log.e("ccccc",cal+"");
            yValList.add(reportItem.getBalance());
        }
        /*if (reportName.equalsIgnoreCase("top sale by qty") || reportName.equalsIgnoreCase("bottom sale items")) {
            for (ReportItem reportItemView : reportItemList) {
                //Log.e("ccccc",cal+"");
                yValList.add((double) reportItemView.getQty());
            }
        }else if (reportName.equalsIgnoreCase("top sales by customer") ||
                reportName.equalsIgnoreCase("top sale by products") || reportName.equalsIgnoreCase("top suppliers") ||
                reportName.equalsIgnoreCase("bottom sales by products")){*/
        for (ReportItem reportItem : reportItemList) {
            //Log.e("ccccc",cal+"");
            //yValList.add(reportItem.getBalance());
        }
        //}


        Log.e("x " + xValues.size(), "y " + yValList.size());
        yValues = getYAxisBarEntries(yValList);


        BarData data = configBarData(yValues, xValues);

        barChart.setData(data);
        for (IBarDataSet set : barChart.getData().getDataSets())
            set.setDrawValues(!set.isDrawValuesEnabled());

    }

    private void configYAxis() {
        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setValueFormatter(new DefaultYAxisValueFormatter(1));
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(10f);
        leftAxis.setAxisMinValue(0);
        leftAxis.setLabelCount(5, true);
        barChart.getAxisRight().setEnabled(false);
    }

    private void configXAxis() {
        XAxis xl = barChart.getXAxis();

        xl.setLabelRotationAngle(270);
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setDrawGridLines(false);
        xl.setSpaceBetweenLabels(1);
        //xl.setDrawLabels(false);
    }

    /**
     * @param yValue is data want to add to bar entry.
     * @return bart entry.
     */
    @NonNull
    private List<BarEntry> getYAxisBarEntries(List<Double> yValue) {
        List<BarEntry> yVals1 = new ArrayList<BarEntry>();
        int            j      = 0;
        for (Double t : yValue) {
            Float f = Float.valueOf(t.toString());
            yVals1.add(new BarEntry(f, j));
            ++j;
        }
        return yVals1;
    }

    @NonNull
    private BarData configBarData(List<BarEntry> yVals, List<String> xVals) {
        BarDataSet set1 = new BarDataSet(yVals, "");
        set1.setColors(colorArr);
        List<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(xVals, dataSets);

        // add space between the dataset groups in percent of bar-width
        data.setGroupSpace(80f);

        //mChart.setDescription(calendar.get(Calendar.YEAR) + " " + xValues.get(0) + " - " + xValues.get(11));

        return data;
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String label = "";

            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {

            settingBarChart();
            configRecyclerView();

            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
