package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.SupplierManager;
import com.digitalfusion.android.pos.database.model.Supplier;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 12/28/17.
 */

public class SupplierSearchAdapter extends ArrayAdapter<Supplier> {


    List<Supplier> suggestion;

    SupplierManager supplierManager;

    int lenght = 0;

    String queryText = "";

    Supplier supplier;

    Context context;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {

            String str = ((Supplier) resultValue).getName();

            supplier = (Supplier) resultValue;

            return str;

        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            if (constraint != null && !constraint.equals("")) {

                suggestion.clear();

                queryText = constraint.toString();

                lenght = constraint.length();

                suggestion = supplierManager.getAllSuppliersByNameOnSearch(0, 30, constraint.toString());

                FilterResults filterResults = new FilterResults();

                filterResults.values = suggestion;

                filterResults.count = suggestion.size();

                return filterResults;
            } else {

                return new FilterResults();

            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            List<Supplier> filterList = (ArrayList<Supplier>) results.values;

            if (results != null && results.count > 0) {

                clear();

                for (Supplier customerVO : filterList) {

                    add(customerVO);

                    notifyDataSetChanged();

                }

            }

        }

    };

    public SupplierSearchAdapter(Context context, SupplierManager supplierManager) {

        super(context, -1);

        this.context = context;

        this.supplierManager = supplierManager;

        suggestion = new ArrayList<>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.supplier_search_suggestion, parent, false);

        }

        viewHolder = new ViewHolder(convertView);

        if (suggestion.size() > 0 && !suggestion.isEmpty()) {

            if (suggestion.get(position).getName().toLowerCase().startsWith(queryText.toLowerCase())) {

                Spannable spanText = new SpannableString(suggestion.get(position).getName());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.supplierNameTextView.setText(spanText);

            }

            viewHolder.businessNameTextView.setText(suggestion.get(position).getName().toString());

        }

        return convertView;

    }

    @Override
    public int getCount() {
        return suggestion.size();
    }

    public List<Supplier> getSuggestion() {

        return suggestion;

    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    public Supplier getSelectedItem() {
        return supplier;
    }

    static class ViewHolder {

        TextView businessNameTextView;

        TextView supplierNameTextView;

        public ViewHolder(View itemView) {

            this.supplierNameTextView = (TextView) itemView.findViewById(R.id.name_text_view);

            this.businessNameTextView = (TextView) itemView.findViewById(R.id.business_text_view);

        }

    }

}