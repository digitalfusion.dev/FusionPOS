package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Menu;

import java.util.List;

/**
 * Created by MD001 on 11/2/16.
 */

public class RVAdapterForMenuMD extends RecyclerView.Adapter<RVAdapterForMenuMD.MenuViewHolder> {

    private List<Menu> menuList;
    private OnItemClickListener mItemClickListener;


    public RVAdapterForMenuMD(List<Menu> menuList) {
        this.menuList = menuList;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_item_view_md, parent, false);

        return new RVAdapterForMenuMD.MenuViewHolder(v);

    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, final int position) {


        holder.menuTextView.setText(menuList.get(position).getName().toString());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(v, position);

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }

    public List<Menu> getmenuVOList() {
        return menuList;
    }

    public void setmenuVOList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {
        TextView menuTextView;

        View view;

        public MenuViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            menuTextView = (TextView) itemView.findViewById(R.id.menu_name_tv);


        }

    }
}

