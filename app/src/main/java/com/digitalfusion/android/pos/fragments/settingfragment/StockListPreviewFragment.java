package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForStockListPreview;
import com.digitalfusion.android.pos.database.business.CategoryManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.business.UnitManager;
import com.digitalfusion.android.pos.database.model.StockImage;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.Spinner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 3/16/17.
 */

public class StockListPreviewFragment extends Fragment {
    public static final String KEY = "list";

    private View mainLayout;
    private RecyclerView recyclerView;
    private TextView newCategoryCountTextView;
    private TextView conflictCountTextView;
    private TextView stockCountTextView;

    private List<StockItem> stockItemList;
    private int newCategoryCount = 0;
    private int newUnitCount = 0;
    private int conflictCount = 0;

    private RVAdapterForStockListPreview rvAdapterForStockListPreview;
    private CategoryManager categoryManager;
    private UnitManager unitManager;
    private StockManager stockManager;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainLayout = inflater.inflate(R.layout.import_stock_preview, container, false);

        recyclerView = (RecyclerView) mainLayout.findViewById(R.id.stock_list_rv);
        newCategoryCountTextView = (TextView) mainLayout.findViewById(R.id.new_category_count);
        stockCountTextView = (TextView) mainLayout.findViewById(R.id.stock_count);
        conflictCountTextView = (TextView) mainLayout.findViewById(R.id.conflict_count);

        setHasOptionsMenu(true);

        context = getContext();

        stockItemList = (List<StockItem>) getArguments().getSerializable(KEY);
        categoryManager = new CategoryManager(getContext());
        unitManager = new UnitManager(getContext());
        stockManager = new StockManager(getContext());

        String preview = context.getTheme().obtainStyledAttributes(new int[]{R.attr.preview}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(preview);

        // Log.w("size",stockItemList.size()+" Sixe");


        //preparing to updateRegistration or insert into database
        for (StockItem s : stockItemList) {

            if (s.getCategoryName() == null || s.getCategoryName().trim().equalsIgnoreCase("")) {
                s.setCategoryID(1l);
            } else {
                Long categoryId = categoryManager.checkNameExistLong(s.getCategoryName());

                if (0 == categoryId) {
                    s.setNewCategory(true);
                    newCategoryCount++;
                } else {
                    s.setCategoryID(categoryId);
                }
            }


            if (s.getUnitName() == null || s.getUnitName().trim().equalsIgnoreCase("")) {

                s.setUnitID(null);
            } else {
                Long unitId = unitManager.checkUnitExistsLong(s.getUnitName());

                if (null == unitId) {
                    s.setNewUnit(true);
                    newUnitCount++;
                } else {
                    s.setUnitID(unitId);
                }
            }

            if (stockManager.checkStockCodeExists(s.getCodeNo())) {
                s.setId(stockManager.getStockIDByStockCode(s.getCodeNo()));
                s.setConflict(true);
                conflictCount++;
            }

        }

        return mainLayout;

    }


    @Override
    public void onResume() {
        super.onResume();
        rvAdapterForStockListPreview = new RVAdapterForStockListPreview(stockItemList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(rvAdapterForStockListPreview);
        rvAdapterForStockListPreview.notifyDataSetChanged();

        newCategoryCountTextView.setText(POSUtil.NumberFormat(newCategoryCount));
        conflictCountTextView.setText(POSUtil.NumberFormat(conflictCount));
        stockCountTextView.setText(POSUtil.NumberFormat(stockItemList.size()));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.save) {
            item.setEnabled(false);
            new SaveTask().execute();
        }


        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);

        super.onCreateOptionsMenu(menu, inflater);

    }

        class SaveTask extends AsyncTask<Void, Void, Void> {
        Spinner spinner = new Spinner(context);

        @Override
        protected void onPreExecute() {
            spinner.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (StockItem s : stockItemList) {
                if (s.isVaild()) {
                    if (s.isConflict()) {
                        if (s.isNewCategory()) {
                            s.setCategoryID(categoryManager.addNewCategoryFromStock(s.getCategoryName(), "", null, 0));
                        }

                        if (s.isNewUnit()) {
                            s.setUnitID(unitManager.addNewUnitFromStock(s.getUnitName(), "", new InsertedBooleanHolder()));
                        }

                        Log.w("HERER TO STRInG", s.toString());

                        stockManager.updateStock(s.getCodeNo(), s.getBarcode(), s.getName(), s.getCategoryID(), s.getUnitID(), s.getInventoryQty(), s.getReorderLevel(), s.getPurchasePrice(), s.getWholesalePrice(),
                                s.getNormalPrice(), s.getRetailPrice(), new ArrayList<Long>(), new ArrayList<StockImage>(), s.getDescription(), s.getId());
                    } else {
                        if (s.isNewCategory()) {
                            s.setCategoryID(categoryManager.addNewCategoryFromStock(s.getCategoryName(), "", null, 0));
                        }

                        if (s.isNewUnit()) {
                            s.setUnitID(unitManager.addNewUnitFromStock(s.getUnitName(), "", new InsertedBooleanHolder()));
                        }

                        stockManager.addNewStock(s.getCodeNo(), s.getBarcode(), s.getName(), s.getCategoryID(), s.getUnitID(), s.getInventoryQty(), s.getReorderLevel(), s.getPurchasePrice(), s.getWholesalePrice(),
                                s.getNormalPrice(), s.getRetailPrice(), new ArrayList<StockImage>(), s.getDescription());

                    }
                }
            }
        return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            spinner.dismiss();
            Toast.makeText(context, "Added Successfully", Toast.LENGTH_SHORT).show();
            getActivity().onBackPressed();
        }
    }
}
