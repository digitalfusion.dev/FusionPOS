package com.digitalfusion.android.pos.fragments.salefragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.CustomerAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForCustomerMD;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForImageList;
import com.digitalfusion.android.pos.database.business.CategoryManager;
import com.digitalfusion.android.pos.database.business.CustomerManager;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.business.UnitManager;
import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.database.model.SalesHeader;
import com.digitalfusion.android.pos.database.model.StockImage;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.interfaces.OnTrasactionFinishListener;
import com.digitalfusion.android.pos.util.GrantPermission;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.views.FusionToast;
import com.example.imagedialogswipe.ImageDialogSwipe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.digitalfusion.android.pos.util.AppConstant.DEFAULT_CUSTOMER;


public class
SalePaymentFragment extends android.support.v4.app.Fragment {

    @BindView(R.id.customer_in_sale_change)
    AutoCompleteTextView customerNameTV;
    @BindView(R.id.ph_in_sale_change)
    EditText customerPhNoTV;
    @BindView(R.id.address_in_sale_change)
    EditText customerAddress;
    @BindView(R.id.total_amount_in_sale_change)
    EditText totalAmountInput;
    @BindView(R.id.paid_amount_in_sale_change)
    EditText paidAmountInput;
    @BindView(R.id.change_in_sale_change)
    EditText changeAmountInput;
    @BindView(R.id.save)
    Button saveBtn;
    @BindView(R.id.print)
    Button printBtn;

    //Business
    private CustomerManager customerManager;
    private SalesManager salesManager;
    private OnTrasactionFinishListener onTrasactionFinishListener;

    private Context context;
    private Unbinder unbinder;
    private Double paidAmount = 0.0;
    private Double totalAmount = 0.0;
    private Boolean isSaleEdit = false;
    private Boolean isHold = false;
    private String customerName;
    private String saleInvoiceNo;
    private SalesHeader salesHeader;
    private String discountPercent;
    private ArrayList<Customer> customers;
    private List<Customer> customerList;
    private RVAdapterForCustomerMD rvAdapterForCustomerMD;
    private CustomerAutoCompleteAdapter customerAutoCompleteAdapter;
    private String phoneNo;
    private String cusAddress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View mainLayoutView = inflater.inflate(R.layout.sale_change, null);
        context = getContext();
        unbinder = ButterKnife.bind(this, mainLayoutView);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        String title = ThemeUtil.getString(context, R.attr.payment);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        MainActivity.setCurrentFragment(this);

        POSUtil.hideKeyboard(mainLayoutView, context);
        Bundle arguments = getArguments();
        salesHeader = (SalesHeader) arguments.getSerializable("saleHeader");
        totalAmount = salesHeader.getTotal();
        paidAmount = salesHeader.getPaidAmt();
        customerManager = new CustomerManager(context);
        salesManager = new SalesManager(context);
        customers = (ArrayList<Customer>) customerManager.getAllCustomers(0, 100);
        customerList = customerManager.getAllCustomers(0, 100);
        rvAdapterForCustomerMD = new RVAdapterForCustomerMD(customerList);

        if(salesHeader.getSaleEdit())
        {
            isSaleEdit = true;
            isHold = salesHeader.getSaleHold();
            if(salesHeader.getCustomerName().equalsIgnoreCase(DEFAULT_CUSTOMER))
            {
                customerNameTV.setText("");
            }
            else
            {
                customerNameTV.setText(salesHeader.getCustomerName().toString().trim());
                customerPhNoTV.setText(customerManager.getCustomerByID(salesHeader.getCustomerID()).getPhoneNo());
                customerAddress.setText(customerManager.getCustomerByID(salesHeader.getCustomerID()).getAddress());
            }
            saleInvoiceNo = salesHeader.getSaleVocherNo();

        }
        else
        {
            isSaleEdit = false ;
        }

        if(customerNameTV.getText().toString().equalsIgnoreCase(DEFAULT_CUSTOMER))
        {
            customerPhNoTV.setText("");
            customerAddress.setText("");
        }
        customerNameTV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                customerPhNoTV.setText(customerAutoCompleteAdapter.getSuggestion().get(i).getPhoneNo());
                customerAddress.setText(customerAutoCompleteAdapter.getSuggestion().get(i).getAddress());
            }
        });

        customerNameTV.setThreshold(1);

        customerAutoCompleteAdapter = new CustomerAutoCompleteAdapter(context, customers);

        customerNameTV.setAdapter(customerAutoCompleteAdapter);



        rvAdapterForCustomerMD.setmItemClickListener(new RVAdapterForCustomerMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                customerNameTV.setText(customerList.get(position).getName().trim());

                //customerListMaterialDialog.dismiss();
            }
        });

        customerNameTV.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    //  phoneNoEditText.requestFocus();
                }
                return false;
            }
        });


        changeAmountInput.setEnabled(false);
        paidAmountInput.setError(null);
        totalAmountInput.setEnabled(false);
        totalAmountInput.setText(POSUtil.doubleToString(salesHeader.getTotal()));

        if (isSaleEdit) {
            paidAmountInput.setText(POSUtil.doubleToString(paidAmount));
        } else {
            paidAmountInput.setText(POSUtil.doubleToString(totalAmount));
        }
        paidAmountInput.setSelectAllOnFocus(true);


        paidAmountInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (paidAmountInput.getText().toString().trim().length() > 0) {

                    Double changeAmount = Double.parseDouble(paidAmountInput.getText().toString()) - totalAmount;

                    if (changeAmount > 0) {

                        changeAmountInput.setText(POSUtil.doubleToString(changeAmount));

                        //saleOustandting.setText("0.0");

                    } else {

                        changeAmountInput.setText("0");

                        // saleOustandting.setText(POSUtil.NumberFormat(Math.abs(changeAmount)));
                    }
                }
            }
        });


        saveBtn.setOnClickListener(view -> {
            if (isInputDataValid()) {
                customerName = customerNameTV.getText().toString().trim();
                phoneNo      = customerPhNoTV.getText().toString().trim();
                cusAddress   = customerAddress.getText().toString().trim();

                if(customerName == null || customerName.equalsIgnoreCase(""))
                {
                    customerName = DEFAULT_CUSTOMER;
                }
                POSUtil.hideKeyboard(mainLayoutView, context);
                if (!isSaleEdit && !isHold) {
                    saleInvoiceNo = saveTransaction();
                } else if (isHold) {
                    saveHeldTransaction(saleInvoiceNo);
                } else if (!isHold) {
                    updateTransaction();
                }
                if(saleInvoiceNo != null)
                {

                    getActivity().setResult(Activity.RESULT_OK);
                    getActivity().finish();
                }
            }
        });

        printBtn.setOnClickListener(view -> {
            if(isInputDataValid())
            {
                customerName = customerNameTV.getText().toString().trim();
                phoneNo      = customerPhNoTV.getText().toString().trim();
                cusAddress   = customerAddress.getText().toString().trim();

                if(customerName == null || customerName.equalsIgnoreCase(""))
                {
                    customerName = DEFAULT_CUSTOMER;
                }

                long saleID = 0;
                POSUtil.hideKeyboard(mainLayoutView, context);
                if (!isSaleEdit && !isHold) {
                    saleInvoiceNo = saveTransaction();
                    saleID = salesManager.findIDBySalesInvoice(saleInvoiceNo);
                } else if (isHold) {
                    saveHeldTransaction(saleInvoiceNo);
                    saleID = salesManager.findIDBySalesInvoice(saleInvoiceNo);
                } else if (!isHold) {
                    updateTransaction();
                    saleID = salesManager.findIDBySalesInvoice(saleInvoiceNo);
                }

                Bundle bundle = new Bundle();
                bundle.putLong(SaleDetailFragment.KEY, saleID);

                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.SALE_DETAIL);
                detailIntent.putExtras(bundle);

                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
                getActivity().startActivityForResult(detailIntent, 1);
            }
        });

        return mainLayoutView;
    }



    private void saveHeldTransaction(String saleInvoiceNo) {

        String change_balance = changeAmountInput.getText().toString();

        Double balance = totalAmount - paidAmount;

        if (balance < 0) {
            balance = 0.0;
        }

        String noRemark = ThemeUtil.getString(context, R.attr.no_remark);
        //                    boolean status = salesManager.updateHoldToSalesBySalesID(saleHeaderView.getId(), date, customerName, phoneNo, totalAmount, discountPercent,
        //                            tax.getId(), subtotal, voucherDiscount, noRemark, paidAmount, balance, taxAmt,
        //                            Integer.toString(saleDay), Integer.toString(saleMonth), Integer.toString(saleYear), type.toString(),
        //                            tax.getRate(), updateList, deleteList, null, deliveryView, tax.getType(), discount.getId());
        boolean isTransactionSuccessful = salesManager.addHeldSales(saleInvoiceNo, customerName, phoneNo, cusAddress, totalAmount, discountPercent, salesHeader.getTaxID(), salesHeader.getSubtotal(), salesHeader.getType(),
                salesHeader.getDiscountAmt(), noRemark, paidAmount, balance, salesHeader.getTaxAmt(), salesHeader.getTaxRate(), salesHeader.getDay(),
               salesHeader.getMonth(), salesHeader.getYear(), salesHeader.getSalesAndPurchaseItemList(), null, salesHeader.getSaleDelivery(), salesHeader.getTaxType(), salesHeader.getDiscountID(),salesHeader.getUserId(), change_balance);

        if (isTransactionSuccessful) {

            salesManager.deleteHoldSales(salesHeader.getId());
            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
        }
    }

    private String saveTransaction() {

        String change_balance = changeAmountInput.getText().toString();

        Double balance = totalAmount - paidAmount;

        if (balance < 0) {
            balance = 0.0;
        }


        String noRemark = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_remark}).getString(0);


        String invoiceNo = salesManager.addNewSales(customerName, phoneNo, cusAddress, totalAmount, salesHeader.getDisInPercent(), salesHeader.getTaxID(), salesHeader.getSubtotal(), salesHeader.getType(),
                salesHeader.getDiscountAmt(), noRemark, paidAmount, balance, salesHeader.getTaxAmt(), salesHeader.getTaxRate(), salesHeader.getDay(),
                salesHeader.getMonth(), salesHeader.getYear(), salesHeader.getSalesAndPurchaseItemList(), null, salesHeader.getSaleDelivery(), salesHeader.getTaxType(), salesHeader.getDiscountID(), salesHeader.getUserId(), change_balance);

        if (invoiceNo != null) {

            //  tax=settingManager.getDefaultTax();
            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
          // gotoParentFragment();
            return invoiceNo;
            //getActivity().onBackPressed();
        }

        return null;
    }

    private void updateTransaction() {

        String change_balance = changeAmountInput.getText().toString();
        Double balance = totalAmount - paidAmount;
        if (balance < 0) {
            balance = 0.0;
        }
        String noRemark = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_remark}).getString(0);
        boolean status = salesManager.updateSalesBySalesID(salesHeader.getId(), salesHeader.getSaleDate(), customerName, phoneNo, cusAddress, totalAmount, discountPercent,
               salesHeader.getTaxID(), salesHeader.getSubtotal(), salesHeader.getDiscountAmt(), noRemark, paidAmount, balance, salesHeader.getTaxAmt(),
               salesHeader.getDay(), salesHeader.getMonth(), salesHeader.getYear(), salesHeader.getType(),
               salesHeader.getTaxRate(), salesHeader.getSalesAndPurchaseItemList(), salesHeader.getDeleteList(), null, salesHeader.getSaleDelivery(), salesHeader.getTaxType(), salesHeader.getDiscountID(),salesHeader.getUserId(),change_balance);

        if (status) {

            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public boolean isInputDataValid() {

        boolean status = true;

        if (paidAmountInput.getText().toString().trim().length() < 1) {

            status = false;

            String enterAmt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_paid_amount}).getString(0);
            paidAmountInput.setError(enterAmt);
        } else {

            paidAmount = Double.valueOf(paidAmountInput.getText().toString());

            if (paidAmount > totalAmount) {
                paidAmount = totalAmount;
            }
        }

        double paidamt = 0.0;
        if (paidAmountInput.getText().toString().trim().length() > 0) {
            paidamt = Double.parseDouble(paidAmountInput.getText().toString());
        }

        if (customerNameTV.getText().toString().trim().length() < 1  && paidamt < totalAmount) {
            customerNameTV.setError("Enter Customer Name");
            customerPhNoTV.setError("Enter Customer PhoneNumber");
            status = false;
        }

        if(customerNameTV.getText().toString().trim().length()>1 && paidamt < totalAmount)
        {
            if(customerPhNoTV.getText().toString().trim().length()<1) {
                customerPhNoTV.setError("Enter Customer PhoneNumber");
                status = false;
            }
        }

        if (customerPhNoTV.getText().toString().trim().length() > 0) {
            if (customerNameTV.getText().toString().trim().length() < 1) {
                customerNameTV.setError("Enter Customer Name");
                status = false;
            }
        }
        return status;
    }




}
