package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.Cursor;

import com.digitalfusion.android.pos.database.model.Menu;
import com.digitalfusion.android.pos.util.AppConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/19/16.
 */
public class MenuDAO extends ParentDAO {
    private static ParentDAO menuDaoInstance;
    private Context context;
    private List<Menu> menuList;
    private Menu menu;

    private MenuDAO(Context context) {
        super(context);
        this.context = context;
        menu = new Menu();
    }

    public static MenuDAO getMenuDaoInstance(Context context) {
        if (menuDaoInstance == null) {
            menuDaoInstance = new MenuDAO(context);
        }
        return (MenuDAO) menuDaoInstance;
    }

    public List<Menu> getAllMenus() {
        menuList = new ArrayList<>();
        database = databaseHelper.getWritableDatabase();
        String query  = "select " + AppConstant.MENU_ID + ", " + AppConstant.MENU_NAME + " from " + AppConstant.MENU_TABLE_NAME;
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                menu = new Menu();
                menu.setId(cursor.getLong(0));
                menu.setName(cursor.getString(1));
                menuList.add(menu);
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        return menuList;
    }

    public Long findIdByname(String name) {
        database = databaseHelper.getWritableDatabase();
        String query  = "select " + AppConstant.MENU_ID + " from " + AppConstant.MENU_TABLE_NAME + " where " + AppConstant.MENU_NAME + " = ?";
        Cursor cursor = database.rawQuery(query, new String[]{name});
        id = 0l;
        if (cursor.moveToFirst()) {
            id = cursor.getLong(0);
        }
        if (cursor != null) {
            cursor.close();
        }
        return id;
    }
}
