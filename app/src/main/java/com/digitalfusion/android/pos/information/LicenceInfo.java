package com.digitalfusion.android.pos.information;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by MD003 on 8/15/17.
 */

public class LicenceInfo implements Serializable {

    private String licenceType;
    private Date startDate;
    private Date endDate;
    private String duration;
    private String licenceKey;
    private String licenseStatus;

    public LicenceInfo() {
        //this.startDate = new Date(2017, 6, 12);
        //this.endDate = new Date(2017, 6, 12);
    }

    public LicenceInfo(String licenceType, Date startDate, Date endDate, String duration, String licenceKey, String licenceStatus) {
        this.licenceType = licenceType;
        this.startDate = startDate;
        this.endDate = endDate;
        this.duration = duration;
        this.licenceKey = licenceKey;
        this.licenseStatus = licenceStatus;
    }

    public String getLicenceType() {
        return licenceType;
    }

    public void setLicenceType(String licenceType) {
        this.licenceType = licenceType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getLicenceKey() {
        return licenceKey;
    }

    public void setLicenceKey(String licenceKey) {
        this.licenceKey = licenceKey;
    }

    public String getLicenseStatus() {
        return licenseStatus;
    }

    public void setLicenseStatus(String licenseStatus) {
        this.licenseStatus = licenseStatus;
    }

    @Override
    public String toString() {
        return "LicenceInfo{" +
                "licenceType='" + licenceType + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", duration='" + duration + '\'' +
                ", licenceKey='" + licenceKey + '\'' +
                ", licenceStatus='" + licenseStatus + '\'' +
                '}';
    }
}
