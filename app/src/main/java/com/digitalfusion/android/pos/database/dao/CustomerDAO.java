package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/29/16.
 */
public class CustomerDAO extends ParentDAO {
    private static ParentDAO customerDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private List<Customer> customerList;
    private Customer customer;

    private CustomerDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
    }

    public static CustomerDAO getCustomerDaoInstance(Context context) {
        if (customerDaoInstance == null) {
            customerDaoInstance = new CustomerDAO(context);
        }
        return (CustomerDAO) customerDaoInstance;
    }

    public Long addNewCustomer(String name, String address, String phoneNo, double balance, InsertedBooleanHolder flag) {
        genID = idGeneratorDAO.getLastIdValue("Customer");
        genID += 1;
        databaseWriteTransaction(flag);
        query = "insert into " + AppConstant.CUSTOMER_TABLE_NAME + "(" + AppConstant.CUSTOMER_ID + ", " + AppConstant.CUSTOMER_NAME + ", " + AppConstant.CUSTOMER_ADDRESS + ", " +
                AppConstant.CUSTOMER_PHONE_NUM + ", " + AppConstant.CUSTOMER_BALANCE + ", " + AppConstant.CREATED_DATE + ") values(" + genID + ",?,?,?," + balance + ",?)";
        try {
            database.execSQL(query, new String[]{name.trim(), address, phoneNo, DateUtility.getTodayDate()});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }

        return genID;
    }

    public boolean checkCustomerAlreadyExists(String name) {
        query = "select " + AppConstant.CUSTOMER_ID + " from " + AppConstant.CUSTOMER_TABLE_NAME + " where " + AppConstant.CUSTOMER_NAME + "=?";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{name.trim()});


            if (cursor.moveToFirst()) {

                cursor.close();

                database.setTransactionSuccessful();
                return true;
            }

        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return false;
    }

    public Long findIdByName(String name) {
        query = "select " + AppConstant.CUSTOMER_ID + " from " + AppConstant.CUSTOMER_TABLE_NAME + " where " + AppConstant.CUSTOMER_NAME + "=?";
        id = 0l;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{name});
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }

    public List<Customer> getAllCustomers(int startLimit, int endLimit, int isDeleted) {
        databaseReadTransaction(flag);
        query = "select " + AppConstant.CUSTOMER_ID + ", " + AppConstant.CUSTOMER_NAME + ", " + AppConstant.CUSTOMER_ADDRESS + ", " +
                AppConstant.CUSTOMER_PHONE_NUM + ", " + AppConstant.CUSTOMER_BALANCE + " from " + AppConstant.CUSTOMER_TABLE_NAME + " where " +
                AppConstant.CUSTOMER_ID + " <> 1 and " + AppConstant.CUSTOMER_IS_DELETED + " <> " + isDeleted + " order by " + AppConstant.CUSTOMER_NAME + " asc " + " limit " + startLimit + ", " + endLimit;
        cursor = database.rawQuery(query, null);
        customerList = new ArrayList<>();
        try {
            if (cursor.moveToFirst()) {
                do {
                    customer = new Customer();
                    customer.setId(cursor.getLong(0));
                    customer.setName(cursor.getString(1));
                    customer.setAddress(cursor.getString(2));
                    customer.setPhoneNo(cursor.getString(3));
                    customer.setBalance(cursor.getDouble(4));
                    //                    Log.e(cursor.getLong(0)+ "", cursor.getDouble(4) + "");
                    customerList.add(customer);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return customerList;
    }

    public List<Customer> getAllCustomers(int isDeleted) {
        databaseReadTransaction(flag);
        query = "select " + AppConstant.CUSTOMER_ID + ", " + AppConstant.CUSTOMER_NAME + ", " + AppConstant.CUSTOMER_ADDRESS + ", " +
                AppConstant.CUSTOMER_PHONE_NUM + ", " + AppConstant.CUSTOMER_BALANCE + " from " + AppConstant.CUSTOMER_TABLE_NAME + " where " +
                AppConstant.CUSTOMER_ID + " <> 1 and " + AppConstant.CUSTOMER_IS_DELETED + " <> " + isDeleted + " order by " + AppConstant.CUSTOMER_NAME + " asc ";
        cursor = database.rawQuery(query, null);
        customerList = new ArrayList<>();
        try {
            if (cursor.moveToFirst()) {
                do {
                    customer = new Customer();
                    customer.setId(cursor.getLong(0));
                    customer.setName(cursor.getString(1));
                    customer.setAddress(cursor.getString(2));
                    customer.setPhoneNo(cursor.getString(3));
                    customer.setBalance(cursor.getDouble(4));
                    //                    Log.e(cursor.getLong(0)+ "", cursor.getDouble(4) + "");
                    customerList.add(customer);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return customerList;
    }

    public Customer getCustomerByID(long id) {
        databaseReadTransaction(flag);
        query = "select " + AppConstant.CUSTOMER_ID + ", " + AppConstant.CUSTOMER_NAME + ", " + AppConstant.CUSTOMER_ADDRESS + ", " +
                AppConstant.CUSTOMER_PHONE_NUM + ", " + AppConstant.CUSTOMER_BALANCE + " from " + AppConstant.CUSTOMER_TABLE_NAME + " where " + AppConstant.CUSTOMER_ID + " = " + id;
        cursor = database.rawQuery(query, null);
        Customer customer = new Customer();
        try {
            if (cursor.moveToFirst()) {
                do {

                    customer.setId(cursor.getLong(0));
                    customer.setName(cursor.getString(1));
                    customer.setAddress(cursor.getString(2));
                    customer.setPhoneNo(cursor.getString(3));
                    customer.setBalance(cursor.getDouble(4));
                    //                    Log.e(cursor.getLong(0)+ "", cursor.getDouble(4) + "");
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return customer;
    }

    public List<Customer> getAllCustomersOustanding(int startLimit, int endLimit) {
        databaseReadTransaction(flag);
        query = "select " + AppConstant.CUSTOMER_ID + ", " + AppConstant.CUSTOMER_NAME + ", " + AppConstant.CUSTOMER_ADDRESS + ", " +
                AppConstant.CUSTOMER_PHONE_NUM + ", " + AppConstant.CUSTOMER_BALANCE + " from " + AppConstant.CUSTOMER_TABLE_NAME + " where " + AppConstant.CUSTOMER_BALANCE + ">0 limit " + startLimit + ", " + endLimit;
        cursor = database.rawQuery(query, null);
        customerList = new ArrayList<>();
        try {
            if (cursor.moveToFirst()) {
                do {
                    customer = new Customer();
                    customer.setId(cursor.getLong(0));
                    customer.setName(cursor.getString(1));
                    customer.setAddress(cursor.getString(2));
                    customer.setPhoneNo(cursor.getString(3));
                    customer.setBalance(cursor.getDouble(4));
                    customerList.add(customer);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return customerList;
    }


    public boolean updateCustomer(String name, String address, String phoneNo, Long id) {
        databaseWriteTransaction(flag);
        contentValues.put(AppConstant.CUSTOMER_NAME, name);
        contentValues.put(AppConstant.CUSTOMER_ADDRESS, address);
        contentValues.put(AppConstant.CUSTOMER_PHONE_NUM, phoneNo);
        try {
            database.update(AppConstant.CUSTOMER_TABLE_NAME, contentValues, AppConstant.CUSTOMER_ID + "=?", new String[]{id.toString()});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean updateCustomer(String phoneNo, String customerAddress, Long id) {
        databaseWriteTransaction(flag);
        contentValues.put(AppConstant.CUSTOMER_PHONE_NUM, phoneNo);
        contentValues.put(AppConstant.CUSTOMER_ADDRESS,customerAddress);
        try {
            database.update(AppConstant.CUSTOMER_TABLE_NAME, contentValues, AppConstant.CUSTOMER_ID + "=?", new String[]{id.toString()});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean deleteCustomer(Long id) {
        databaseWriteTransaction(flag);
        query = "update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_IS_DELETED + " = 1 where " + AppConstant.CUSTOMER_ID + " = " + id;
        Log.e("query", query);
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        Log.e("flag", flag.isInserted() + "dkjf");
        return flag.isInserted();
    }

    public boolean checkCustomerBalance(Long id) {
        boolean success = false;
        databaseReadTransaction(flag);
        try {
            query = "select " + AppConstant.CUSTOMER_BALANCE + " from " + AppConstant.CUSTOMER_TABLE_NAME + " where " + AppConstant.CUSTOMER_ID + " = " + id;
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                if (cursor.getDouble(0) > 0) {
                    success = false;
                } else {
                    success = true;
                }
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        Log.e("flag", flag.isInserted() + "dkjf");
        return success;
    }

    public List<Customer> getAllCustomersByNameOnSearch(int startLimit, int endLimit, String queryStr) {
        databaseReadTransaction(flag);
        query = "select " + AppConstant.CUSTOMER_ID + ", " + AppConstant.CUSTOMER_NAME + ", " + AppConstant.CUSTOMER_ADDRESS + ", " +
                AppConstant.CUSTOMER_PHONE_NUM + ", " + AppConstant.CUSTOMER_BALANCE + " from " + AppConstant.CUSTOMER_TABLE_NAME + " where " +
                AppConstant.CUSTOMER_NAME + " like ? || '%' limit " + startLimit + ", " + endLimit;
        cursor = database.rawQuery(query, new String[]{queryStr});
        customerList = new ArrayList<>();
        try {
            if (cursor.moveToFirst()) {
                do {
                    customer = new Customer();
                    customer.setId(cursor.getLong(0));
                    customer.setName(cursor.getString(1));
                    customer.setAddress(cursor.getString(2));
                    customer.setPhoneNo(cursor.getString(3));
                    customer.setBalance(cursor.getDouble(4));
                    customerList.add(customer);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return customerList;
    }
}
