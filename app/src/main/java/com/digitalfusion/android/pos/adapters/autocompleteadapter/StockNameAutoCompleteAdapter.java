package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.model.StockAutoComplete;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 11/9/16.
 */

public class StockNameAutoCompleteAdapter extends ArrayAdapter<StockAutoComplete> {


    List<StockAutoComplete> suggestion;

    List<StockAutoComplete> searchList;

    Callback callback;

    String queryTxt = "";

    StockManager stockManager;

    int lenght = 0;

    StockAutoComplete stockAutoComplete;

    Context context;

    boolean flag = false;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((StockAutoComplete) resultValue).getItemName();
            stockAutoComplete = (StockAutoComplete) resultValue;

            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {


            if (constraint != null) {

                searchList = new ArrayList<>();

                queryTxt = constraint.toString();

                searchList = stockManager.findStockByNameForAutoComplete(constraint.toString());

              /*  if(suggestionList.size()==1){

                    if(callback!=null){

                        callback.onOneResut(suggestionList.get(0));
                        return new FilterResults();

                    }else {


                        lenght=constraint.length();

                        FilterResults filterResults = new FilterResults();
                        filterResults.values = suggestionList;
                        filterResults.count = suggestionList.size();
                        return filterResults;
                    }
*/

                //    }else {

                lenght = constraint.length();

                FilterResults filterResults = new FilterResults();
                filterResults.values = searchList;
                filterResults.count = searchList.size();
                return filterResults;
                //   }


            } else {

                return new FilterResults();

            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<StockAutoComplete> filterList = (ArrayList<StockAutoComplete>) results.values;

            if (filterList == null) {
                suggestion = new ArrayList<>();
            } else {
                Log.w("helre", filterList.size() + " reslutle sixe");
                setSuggestion(filterList);
            }
            notifyDataSetChanged();

        }
    };

    public StockNameAutoCompleteAdapter(Context context, StockManager stockManager, Callback callback) {

        super(context, -1);

        this.context = context;

        this.stockManager = stockManager;

        suggestion = new ArrayList<>();

        this.callback = callback;
        searchList = new ArrayList<>();

    }

    public StockNameAutoCompleteAdapter(Context context, StockManager stockManager) {

        super(context, -1);

        this.context = context;

        this.stockManager = stockManager;

        suggestion = new ArrayList<>();

        this.callback = callback;

        searchList = new ArrayList<>();

    }


    @Override
    public int getCount() {
        return suggestion.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.stock_item_auto_complete_view1, parent, false);

        }

        viewHolder = new ViewHolder(convertView);

        if (!suggestion.isEmpty() && suggestion.size() > 0) {

            if (queryTxt.equalsIgnoreCase(suggestion.get(position).getItemName())) {
                Spannable spanText = new SpannableString(suggestion.get(position).getItemName());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.nameTextView.setText(spanText);
            } else {
                viewHolder.nameTextView.setText(suggestion.get(position).getItemName());
            }


            // viewHolder.customerNameTextView.setText(salesHistoryViewList.get(position).getItemName());

            viewHolder.codeTextView.setText(suggestion.get(position).getCodeNo());


        }

        return convertView;

    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    public StockAutoComplete getStockItem() {
        return stockAutoComplete;
    }

    public void setCustomer(StockAutoComplete stockAutoComplete) {

        this.stockAutoComplete = stockAutoComplete;

    }

    public List<StockAutoComplete> getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(List<StockAutoComplete> suggestion) {
        this.suggestion = suggestion;
    }

    @Nullable
    @Override
    public StockAutoComplete getItem(int position) {

        return suggestion.get(position);
    }


    public interface Callback {

        void onOneResut(StockAutoComplete stockAutoComplete);

    }

    static class ViewHolder {

        TextView codeTextView;

        TextView nameTextView;

        public ViewHolder(View itemView) {

            this.nameTextView = (TextView) itemView.findViewById(R.id.name);

            this.codeTextView = (TextView) itemView.findViewById(R.id.code);

        }

    }

}
