package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;

import java.util.List;

/**
 * Created by MD002 on 12/28/16.
 */

public class RVAdapterForReorderQty extends ParentRVAdapterForReports {

    private static final int VIEWTYPE_ITEM = 2;
    private static final int VIEWTYPE_LOADER = 3;
    private final int HEADER_TYPE = 10000;
    private List<ReportItem> reportItemList;

    /**
     * 1 for stock, category, amount, qty, unit
     * 2 for category, amount
     */
    public RVAdapterForReorderQty(List<ReportItem> reportItemList) {
        super();
        this.reportItemList = reportItemList;
        Log.w("hello", reportItemList.size() + " ss");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_name_two_qty_header, parent, false);

            return new ReorderHeaderViewHolder(v);
        } else if (viewType == VIEWTYPE_LOADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);
            return new LoaderViewHolder(v);
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_name_two_qty, parent, false);

        return new ReorderViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        Log.e("vl", "daf");
        if (viewHolder instanceof LoaderViewHolder) {

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);

        } else if (viewHolder instanceof ReorderViewHolder && !reportItemList.isEmpty()) {
            ReorderViewHolder reorderViewHolder = (ReorderViewHolder) viewHolder;
            reorderViewHolder.nameTextView.setText(reportItemList.get(position - 1).getName());
            reorderViewHolder.inventoryQtyTextView.setText(Integer.toString(reportItemList.get(position - 1).getQty()) + "(" + reportItemList.get(position - 1).getTotalQty() + ")");
            reorderViewHolder.reorderQtyTextView.setText(Integer.toString(reportItemList.get(position - 1).getQty1()));
        } else if (viewHolder instanceof ReorderHeaderViewHolder && reportItemList.isEmpty()) {
            ReorderHeaderViewHolder reorderHeaderViewHolder = (ReorderHeaderViewHolder) viewHolder;
            reorderHeaderViewHolder.itemViewLayout.setVisibility(View.GONE);
            reorderHeaderViewHolder.noDataTextView.setVisibility(View.VISIBLE);
            reorderHeaderViewHolder.line.setVisibility(View.GONE);
        } else {
            ReorderHeaderViewHolder reorderHeaderViewHolder = (ReorderHeaderViewHolder) viewHolder;
            reorderHeaderViewHolder.noDataTextView.setVisibility(View.GONE);
            reorderHeaderViewHolder.itemViewLayout.setVisibility(View.VISIBLE);
            reorderHeaderViewHolder.line.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {

        return reportItemList.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_TYPE;
        }
        if (position != 0 && position == getItemCount() - 1) {

            if (reportItemList != null) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;
            }


        }
        return VIEWTYPE_ITEM;
    }


    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public class ReorderViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView;
        TextView inventoryQtyTextView;
        TextView reorderQtyTextView;

        public ReorderViewHolder(View itemView) {
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            inventoryQtyTextView = (TextView) itemView.findViewById(R.id.inventory_qty_text_view);
            reorderQtyTextView = (TextView) itemView.findViewById(R.id.reorder_qty_text_view);

        }
    }

    public class ReorderHeaderViewHolder extends RecyclerView.ViewHolder {

        LinearLayout itemViewLayout;
        TextView noDataTextView;
        View line;

        public ReorderHeaderViewHolder(View itemView) {
            super(itemView);

            itemViewLayout = (LinearLayout) itemView.findViewById(R.id.item_view_layout);
            noDataTextView = (TextView) itemView.findViewById(R.id.no_data_text_view);
            line = itemView.findViewById(R.id.line);
        }
    }
}
