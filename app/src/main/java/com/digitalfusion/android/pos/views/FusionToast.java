package com.digitalfusion.android.pos.views;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalfusion.android.pos.R;

/**
 * Created by MD003 on 12/28/17.
 */

public class FusionToast {

    public static void toast(Context context, TOAST_TYPE toast_type) {
        String         msg      = "";
        Toast          toast    = new Toast(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View           view     = inflater.inflate(R.layout.toast, null);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView message = view.findViewById(R.id.message);
        if (toast_type == TOAST_TYPE.ERROR) {
            msg = context.getTheme().obtainStyledAttributes(new int[]{R.attr.error}).getString(0);
            view.setBackgroundResource(R.drawable.toast_error);
        } else {

            msg = context.getTheme().obtainStyledAttributes(new int[]{R.attr.success}).getString(0);
            view.setBackgroundResource(R.drawable.toast_success);
        }

        message.setText(msg);


        toast.setView(view);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void toast(Context context) {
        String         msg      = "";
        Toast          toast    = new Toast(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View           view     = inflater.inflate(R.layout.toast, null);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView message = view.findViewById(R.id.message);
        msg = context.getTheme().obtainStyledAttributes(new int[]{R.attr.error}).getString(0);
        view.setBackgroundResource(R.drawable.toast_error);

        message.setText("License Expired.");
        toast.setView(view);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void toast(Context context, TOAST_TYPE toast_type, String description) {
        String         msg      = description;
        Toast          toast    = new Toast(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View           view     = inflater.inflate(R.layout.toast, null);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView message = view.findViewById(R.id.message);
        if (toast_type == TOAST_TYPE.ERROR) {
            view.setBackgroundResource(R.drawable.toast_error);
        } else {
            view.setBackgroundResource(R.drawable.toast_success);
        }

        message.setText(msg);

        toast.setView(view);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    public enum TOAST_TYPE {
        ERROR, SUCCESS
    }
}
