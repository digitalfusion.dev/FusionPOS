package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForProfitAndLossExpMgr;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ProfitAndLossFragment extends Fragment implements Serializable {

    private TextView dateFilterTextView;
    private TextView salesAmtTextView;
    private TextView soldItemValueTextView;
    private TextView purchaseDiscountTextView;
    private TextView salesDiscountTextView;
    private TextView salesTaxTextView;
    private TextView purchaseTaxTextView;
    private TextView totalIncomeTextView;
    private TextView totalExpenseTextView;
    private TextView profitLossTextView;

    private RecyclerView expenseRecyclerView;
    private RecyclerView incomeRecyclerView;

    private RVAdapterForFilter rvAdapterForDateFilter;
    private MaterialDialog dateFilterDialog;
    private List<String> filterList;

    private View mainLayoutView;
    private Context context;

    private ReportManager reportManager;
    private List<ReportItem> incomeReportItemList;
    private List<ReportItem> expenseReportItemList;
    private ReportItem reportItem;

    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;
    private String thisWeek;
    private String lastWeek;

    private Double totalIncome;
    private Double totalExpense;
    private Double profitLoss;

    private String customRange;

    private MaterialDialog customRangeDialog;

    private TextView startDateTextView, endDateTextView;

    private String customStartDate,
            customEndDate;

    private TextView traceDate;

    private Button customRangeOkBtn, customRangeCancelBtn;

    private DatePickerDialog startDatePickerDialog;

    private String startDate;
    private String endDate;

    private RVAdapterForProfitAndLossExpMgr rvAdapterForProfitAndLossExpMgr;

    private RelativeLayout incomeRelativeLayout;
    private RelativeLayout expenseRelativeLayout;
    private ImageView incomeUpDownArrowBtn;
    private ImageView expenseUpDownArrowBtn;
    private TextView incomeValueTextView;
    private TextView expenseValueTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_profit_and_loss, container, false);

        String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.profit_or_lost}).getString(0);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);

        loadIngUI();

        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        initializeVariables();

        configFilters();

        buildDateFilterDialog();

        setDateFilterTextView(thisWeek);

        buildingCustomRangeDialog();

        Log.e(startDate, endDate);

        new LoadProgressDialog().execute("");

        clicklisteners();


        incomeRelativeLayout.setVisibility(View.GONE);

        expenseRelativeLayout.setVisibility(View.GONE);

        /*
        setting listener for show or hide view of income
         */
        mainLayoutView.findViewById(R.id.income_ll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (incomeRelativeLayout.isShown()) {

                    incomeUpDownArrowBtn.setSelected(true);

                    incomeRelativeLayout.setVisibility(View.GONE);

                    incomeValueTextView.setVisibility(View.VISIBLE);


                } else {

                    incomeUpDownArrowBtn.setSelected(false);

                    incomeRelativeLayout.setVisibility(View.VISIBLE);

                    incomeValueTextView.setVisibility(View.GONE);

                }
            }
        });

         /*
        setting listener for show or hide view of expense
         */
        mainLayoutView.findViewById(R.id.expense_ll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expenseRelativeLayout.isShown()) {

                    expenseUpDownArrowBtn.setSelected(true);

                    expenseRelativeLayout.setVisibility(View.GONE);

                    expenseValueTextView.setVisibility(View.VISIBLE);

                } else {

                    expenseUpDownArrowBtn.setSelected(false);

                    expenseRelativeLayout.setVisibility(View.VISIBLE);

                    expenseValueTextView.setVisibility(View.GONE);

                }
            }
        });


    }

    private void initializeList() {
        //reportManager.dropPurchasePriceTempTable();
        reportManager.createTempTableForProfitAndLoss(startDate, endDate);

        reportItem = reportManager.profitAndLossAmt(startDate, endDate);

        reportManager.dropPurchasePriceTempTable();

        incomeReportItemList = reportManager.profitAndLossIncomeList(startDate, endDate);
        expenseReportItemList = reportManager.profitAndLossExpenseList(startDate, endDate);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void loadIngUI() {
        dateFilterTextView = (TextView) mainLayoutView.findViewById(R.id.date_filter);
        incomeRecyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.income_rv);
        expenseRecyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.expense_rv);
        salesAmtTextView = (TextView) mainLayoutView.findViewById(R.id.sales_amt_text_view);
        soldItemValueTextView = (TextView) mainLayoutView.findViewById(R.id.sold_item_value_text_view);
        purchaseDiscountTextView = (TextView) mainLayoutView.findViewById(R.id.purchase_discount_text_view);
        salesDiscountTextView = (TextView) mainLayoutView.findViewById(R.id.sale_discount_text_view);
        salesTaxTextView = (TextView) mainLayoutView.findViewById(R.id.sale_tax_text_view);
        purchaseTaxTextView = (TextView) mainLayoutView.findViewById(R.id.purchase_tax_text_view);
        totalIncomeTextView = (TextView) mainLayoutView.findViewById(R.id.total_income_text_view);
        totalExpenseTextView = (TextView) mainLayoutView.findViewById(R.id.total_expense_text_view);
        profitLossTextView = (TextView) mainLayoutView.findViewById(R.id.profit_or_loss_text_view);
        incomeRelativeLayout = mainLayoutView.findViewById(R.id.rl_income);
        expenseRelativeLayout = mainLayoutView.findViewById(R.id.rl_expense);
        expenseUpDownArrowBtn = mainLayoutView.findViewById(R.id.expense_arrow);
        incomeUpDownArrowBtn = mainLayoutView.findViewById(R.id.income_arrow);
        incomeValueTextView = mainLayoutView.findViewById(R.id.income_value);
        expenseValueTextView = mainLayoutView.findViewById(R.id.expense_value);


    }

    private void initializeVariables() {

        incomeReportItemList = new ArrayList<>();

        reportManager = new ReportManager(context);

        totalIncome = 0.0;
        totalExpense = 0.0;
        profitLoss = 0.0;

        setDatesAndLimits();

    }

    private void setDatesAndLimits() {
        startDate = DateUtility.getStartDateOfWeekString();
        endDate = DateUtility.getEndDateOfWeekString();
        Log.e(startDate, endDate);
    }

    private void clicklisteners() {
        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                dateFilterDialog.dismiss();
                if (filterList.get(position).equalsIgnoreCase(thisMonth)) {

                    startDate = DateUtility.getThisMonthStartDate();

                    endDate = DateUtility.getThisMonthEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {


                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {


                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(thisWeek)) {
                    startDate = DateUtility.getStartDateOfWeekString();

                    endDate = DateUtility.getEndDateOfWeekString();
                    Log.e(startDate + "oi", endDate);
                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));
                } else if (filterList.get(position).equalsIgnoreCase(lastWeek)) {
                    startDate = DateUtility.getStartDateOfLastWeekString();

                    endDate = DateUtility.getEndDateOfLastWeekString();

                    Log.e(startDate + "oi", endDate);
                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));
                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {

                    customRangeDialog.show();

                }


            }
        });

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startDate = customStartDate;

                endDate = customEndDate;

                /*String startDayDes[]= DateUtility.dayDes(startDate);

                String startYearMonthDes= DateUtility.monthYearDes(startDate);


                String endDayDes[]= DateUtility.dayDes(endDate);

                String endYearMonthDes= DateUtility.monthYearDes(endDate);


                dateFilterTextView.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));
*/
                dateFilterTextView.setText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));
                new LoadProgressDialog().execute("");
                customRangeDialog.dismiss();
            }
        });

        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRangeDialog.dismiss();
            }
        });
    }

    private void configFilters() {


        TypedArray mthisWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_week});

        TypedArray mlastWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week});

        TypedArray mthisMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month});

        TypedArray mlastMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month});

        TypedArray mthisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year});

        TypedArray mlastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year});

        TypedArray mcustomRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_range});

        thisMonth = mthisMonth.getString(0);

        lastMonth = mlastMonth.getString(0);

        thisYear = mthisYear.getString(0);

        lastYear = mlastYear.getString(0);

        thisWeek = mthisWeek.getString(0);

        lastWeek = mlastWeek.getString(0);

        customRange = mcustomRange.getString(0);


        filterList = new ArrayList<>();

        filterList.add(thisWeek);

        filterList.add(lastWeek);

        filterList.add(thisMonth);

        filterList.add(lastMonth);

        filterList.add(thisYear);

        filterList.add(lastYear);


        filterList.add(customRange);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });


    }

    public void buildingCustomRangeDialog() {
        buildingCustomRangeDatePickerDialog();
        String date = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range}).getString(0);
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(date)
                .customView(R.layout.custome_range, true).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.negativeText("Cancel")
                //.positiveText("OK")
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    private void setDateFilterTextView(String msg) {

        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {

        String filter = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);
        dateFilterDialog = new MaterialDialog.Builder(context).

                title(filter)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))

                .build();

    }

    private void configUI() {
        salesAmtTextView.setText(POSUtil.NumberFormat(reportItem.getTotalAmt()));
        soldItemValueTextView.setText("(" + POSUtil.NumberFormat(reportItem.getValue()) + ")");
        purchaseDiscountTextView.setText(POSUtil.NumberFormat(reportItem.getBalance()));
        salesDiscountTextView.setText("(" + POSUtil.NumberFormat(reportItem.getAmount()) + ")");
        salesTaxTextView.setText("(" + POSUtil.NumberFormat(reportItem.getSalesTax()) + ")");
        purchaseTaxTextView.setText("(" + POSUtil.NumberFormat(reportItem.getPurTax()) + ")");

        totalIncome = reportItem.getTotalAmt() - reportItem.getValue() + reportItem.getBalance();
        totalExpense = reportItem.getAmount() + reportItem.getSalesTax() + reportItem.getPurTax();

        incomeRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        rvAdapterForProfitAndLossExpMgr = new RVAdapterForProfitAndLossExpMgr(incomeReportItemList);
        incomeRecyclerView.setAdapter(rvAdapterForProfitAndLossExpMgr);

        for (ReportItem r : incomeReportItemList) {
            totalIncome += r.getAmount();
        }

        expenseRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        rvAdapterForProfitAndLossExpMgr = new RVAdapterForProfitAndLossExpMgr(expenseReportItemList);
        expenseRecyclerView.setAdapter(rvAdapterForProfitAndLossExpMgr);

        for (ReportItem r : expenseReportItemList) {
            totalExpense += r.getAmount();
        }

        totalIncomeTextView.setText(POSUtil.NumberFormat(totalIncome));
        incomeValueTextView.setText(POSUtil.NumberFormat(totalIncome));
        totalExpenseTextView.setText(POSUtil.NumberFormat(totalExpense));
        expenseValueTextView.setText(POSUtil.NumberFormat(totalExpense));

        profitLoss = totalIncome - totalExpense;
        if (profitLoss > 0) {
            profitLossTextView.setText(POSUtil.NumberFormat(profitLoss).toString());
            profitLossTextView.setTextColor(getResources().getColor(R.color.income));
        } else {
            profitLossTextView.setText(POSUtil.NumberFormat(Math.abs(profitLoss)) + "");
            profitLossTextView.setTextColor(getResources().getColor(R.color.expense));
        }
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializeList();
            return "success";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String    label         = "";
            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {

            configUI();
            // settingBarChart();
            //configRecyclerView();

            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
