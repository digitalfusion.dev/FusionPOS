package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.Tax;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD003 on 8/25/16.
 */
public class RVSwipeAdapterForTaxList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private List<Tax> taxList;

    private ClickListener editClickListener;
    private ClickListener deleteClickListener;


    public RVSwipeAdapterForTaxList(List<Tax> taxList) {
        this.taxList = taxList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tax_item_view, parent, false);

        return new TaxViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof TaxViewHolder) {

            final TaxViewHolder taxViewHolder = (TaxViewHolder) holder;

            POSUtil.makeZebraStrip(taxViewHolder.itemView, position);

            taxViewHolder.taxNameTextView.setText(taxList.get(position).getName());

            taxViewHolder.taxRateTextView.setText(taxList.get(position).getRate().toString());

            if (taxList.get(position).getId() == 1) {
                ((TaxViewHolder) holder).deleteBtn.setVisibility(View.GONE);
            }

            if (taxList.get(position).getType().equals("Inclusive")) {

                taxViewHolder.taxTypeTextView.setText(taxViewHolder.inclusive);
            } else {

                taxViewHolder.taxTypeTextView.setText(taxViewHolder.exclusive);
            }


            if (taxList.get(position).getIsDefault() > 0) {

                taxViewHolder.isDefaultImageView.setVisibility(View.VISIBLE);

            } else {

                taxViewHolder.isDefaultImageView.setVisibility(View.GONE);

            }

            if (taxList.get(position).getDescription() != null && !taxList.get(position).getDescription().equalsIgnoreCase("")) {

                taxViewHolder.desTextView.setText(taxList.get(position).getDescription());

            } else {
                taxViewHolder.desTextView.setText(null);
                taxViewHolder.desTextView.setHint(taxViewHolder.nodes);
            }

            taxViewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editClickListener != null) {

                        taxViewHolder.swipeLayout.close(true);

                        editClickListener.onClick(position);

                    }
                }
            });

            taxViewHolder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteClickListener != null) {
                        taxViewHolder.swipeLayout.close(true);
                        deleteClickListener.onClick(position);
                    }
                }
            });


            mItemManger.bindView(taxViewHolder.view, position);

        }

    }

    @Override
    public int getItemCount() {
        return taxList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public List<Tax> getTaxList() {
        return taxList;
    }

    public void setTaxList(List<Tax> taxList) {
        this.taxList = taxList;
    }

    public void deletePos(int deletePos) {

        taxList.remove(deletePos);

        notifyItemRemoved(deletePos);

        notifyItemRangeChanged(deletePos, taxList.size());


    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    public class TaxViewHolder extends RecyclerView.ViewHolder {

        TextView taxNameTextView;

        TextView taxRateTextView;

        TextView taxTypeTextView;

        AppCompatTextView isDefaultImageView;

        ImageButton editButton;

        SwipeLayout swipeLayout;

        TextView desTextView;

        ImageButton deleteBtn;

        View view;

        String nodes;
        String inclusive;
        String exclusive;

        public TaxViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            desTextView = (TextView) itemView.findViewById(R.id.desc);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            editButton = (ImageButton) itemView.findViewById(R.id.edit_tax_in_tax_item_view);
            taxNameTextView = (TextView) itemView.findViewById(R.id.tax_nam_tv);
            taxRateTextView = (TextView) itemView.findViewById(R.id.tax_rate_tv);
            taxTypeTextView = (TextView) itemView.findViewById(R.id.tax_type_tv);
            isDefaultImageView = (AppCompatTextView) itemView.findViewById(R.id.is_defalut_imgv);

            deleteBtn = (ImageButton) itemView.findViewById(R.id.delete);


            nodes = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.no_description}).getString(0);
            inclusive = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.inclusive}).getString(0);
            exclusive = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.exclusive}).getString(0);
        }

    }
}
