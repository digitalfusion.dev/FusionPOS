package com.digitalfusion.android.pos.database.model;

import java.io.Serializable;

/**
 * Created by MD002 on 11/8/16.
 */

public class ReportItem implements Serializable {
    private int qty;
    private int qty1;
    private String name;
    //private String customerSupplierName;
    private Double balance;
    private float percent;
    private String name1;
    private String name2;
    private String date;
    private String date1;
    private int totalQty;
    private Double totalAmt;
    private Double amount;
    private Double salesTax;
    private Double purTax;
    private Double value;
    private int month;
    private Long id;
    private String type;
    private Double totalPurchase;
    private Long id1;

    public ReportItem() {
    }

    public ReportItem(String name, String date, Double amount) {
        this.name = name;
        this.date = date;
        this.amount = amount;
    }

    public Long getId1() {
        return id1;
    }

    public void setId1(Long id1) {
        this.id1 = id1;
    }

    public Double getAmount() {

        if (amount == null) {
            return 0.0;
        }
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getTotalAmt() {
        if (totalAmt == null) {
            return 0.0;
        }
        return totalAmt;
    }

    public void setTotalAmt(Double totalAmt) {
        this.totalAmt = totalAmt;
    }

    public int getQty1() {
        return qty1;
    }

    public void setQty1(int qty1) {
        this.qty1 = qty1;
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getBalance() {
        if (balance == null) {
            return 0.0;
        }
        return balance;
    }
/*public String getCustomerSupplierName() {
        return customerSupplierName;
    }

    public void setCustomerSupplierName(String customerSupplierName) {
        this.customerSupplierName = customerSupplierName;
    }*/

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getSalesTax() {

        if (salesTax == null) {
            return 0.0;
        }
        return salesTax;
    }

    public void setSalesTax(Double salesTax) {
        this.salesTax = salesTax;
    }

    public Double getPurTax() {

        if (purTax == null) {
            return 0.0;
        }

        return purTax;
    }

    public void setPurTax(Double purTax) {
        this.purTax = purTax;
    }

    public Double getValue() {
        if (value == null) {
            return 0.0;
        }
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getTotalPurchase() {
        if (totalPurchase == null) {
            return 0.0;
        }
        return totalPurchase;
    }

    public void setTotalPurchase(Double totalPurchase) {
        this.totalPurchase = totalPurchase;
    }

    @Override
    public String toString() {
        return "ReportItem{" +
                "qty=" + qty +
                ", qty1=" + qty1 +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", percent=" + percent +
                ", name1='" + name1 + '\'' +
                ", name2='" + name2 + '\'' +
                ", date='" + date + '\'' +
                ", date1='" + date1 + '\'' +
                ", totalQty=" + totalQty +
                ", totalAmt=" + totalAmt +
                ", amount=" + amount +
                ", salesTax=" + salesTax +
                ", purTax=" + purTax +
                ", value=" + value +
                ", month=" + month +
                ", id=" + id +
                ", type='" + type + '\'' +
                ", totalPurchase=" + totalPurchase +
                ", id1=" + id1 +
                '}';
    }

    public enum ReportType {
        top_sale_by_products, top_sales_by_category, top_sales_by_customer, top_sale_by_qty,
        bottom_sales_by_products, monthly_sales_transactions, monthly_gross_profit, monthly_gross_profit_by_product

    }
}
