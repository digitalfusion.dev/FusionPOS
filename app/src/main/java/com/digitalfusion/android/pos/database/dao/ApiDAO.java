package com.digitalfusion.android.pos.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteTransactionListener;
import android.util.Log;

import com.digitalfusion.android.pos.database.ApiDatabaseHelper;
import com.digitalfusion.android.pos.database.model.Device;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.information.LicenceInfo;
import com.digitalfusion.android.pos.information.Registration;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by MD002 on 1/17/18.
 */

public class ApiDAO {
    private ApiDatabaseHelper apiDatabaseHelper;
    private SQLiteDatabase db;
    private InsertedBooleanHolder flag;
    private static ApiDAO apiDAOInstance;

    private ApiDAO(Context context) {
        apiDatabaseHelper = ApiDatabaseHelper.getHelperInstance(context);
        flag = new InsertedBooleanHolder();
    }

    public static ApiDAO getApiDAOInstance(Context context) {
        if (apiDAOInstance == null) {
            apiDAOInstance = new ApiDAO(context);
        }
        return apiDAOInstance;
    }

    private void sqLiteDatabaseReadTransaction(final InsertedBooleanHolder status) {
        db = apiDatabaseHelper.getReadableDatabase();
        db.beginTransactionWithListenerNonExclusive(new SQLiteTransactionListener() {
            @Override
            public void onBegin() {
                Log.e("on", "begin");
                status.setInserted(false);
            }

            @Override
            public void onCommit() {
                Log.e("on", "commit");
                status.setInserted(true);
            }

            @Override
            public void onRollback() {
                Log.e("on", "roll back");
                status.setInserted(false);
            }
        });
    }

    private void databaseWriteTransaction(final InsertedBooleanHolder status) {
        db = apiDatabaseHelper.getWritableDatabase();
        db.beginTransactionWithListenerNonExclusive(new SQLiteTransactionListener() {
            @Override
            public void onBegin() {
                status.setInserted(false);
                Log.e("on", "begin");
            }

            @Override
            public void onCommit() {
                Log.e("on", "commit");
                status.setInserted(true);
            }

            @Override
            public void onRollback() {
                Log.e("on", "roll back");
                status.setInserted(false);
            }
        });
    }

    public boolean updateRegistration(String username, String userID, String businessName, String userStatus) {
        databaseWriteTransaction(flag);
        try {
            String query = "update " + AppConstant.REGISTRATION_TABLE_NAME + " set " + AppConstant.REGISTRATION_USERNAME + "= ?, " + AppConstant.REGISTRATION_USER_ID + "=?, " +
                    AppConstant.REGISTRATION_BUSINESS_NAME + " = ?, " + AppConstant.REGISTRATION_USER_STATUS + "=?";
            db.execSQL(query, new String[]{username, userID, businessName, userStatus});
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean addLicense(String duration, Date startDate, Date endDate) {
        databaseWriteTransaction(flag);
        try {
            SimpleDateFormat sdf   = new SimpleDateFormat("yyyy-MM-dd");
            String           start = sdf.format(startDate);
            String           end   = sdf.format(endDate);
            Log.e("startdate", start + " " + end);
            String query = "insert into " + AppConstant.LICENSE_INFO_TABLE_NAME + "(" + AppConstant.LICENSE_INFO_DURATION + ", " + AppConstant.LICENSE_INFO_START_DATE +
                    ", " + AppConstant.LICENSE_INFO_END_DATE + ") values (?, ?, ?)";
            db.execSQL(query, new String[]{duration, start, end});
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return flag.isInserted();
    }

    public Registration getRegistration() {
        Registration registration = new Registration();
        sqLiteDatabaseReadTransaction(flag);
        Cursor cursor = null;
        try {
            String query = "select " + AppConstant.REGISTRATION_USERNAME + ", " + AppConstant.REGISTRATION_USER_ID + ", " +
                    AppConstant.REGISTRATION_BUSINESS_NAME + ", " + AppConstant.REGISTRATION_USER_STATUS + " from " + AppConstant.REGISTRATION_TABLE_NAME;
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                registration.setUserName(cursor.getString(0));
                registration.setUserId(cursor.getString(1));
                registration.setBusinessName(cursor.getString(2));
                registration.setUserStatus(cursor.getString(3));
            }
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return registration;
    }

    public LicenceInfo getLicense() {
        LicenceInfo licenceInfo = new LicenceInfo();
        sqLiteDatabaseReadTransaction(flag);
        Cursor cursor = null;
        try {
            String query = "select " + AppConstant.LICENSE_INFO_DURATION + ", " + AppConstant.LICENSE_INFO_START_DATE +
                    ", " + AppConstant.LICENSE_INFO_END_DATE + " from " + AppConstant.LICENSE_INFO_TABLE_NAME;
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                licenceInfo.setDuration(cursor.getString(0));
                //                licenceInfo.setStartDate(cursor.getColumnName(1));
                Log.e("datehey", cursor.getString(1));

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                licenceInfo.setStartDate(sdf.parse(cursor.getString(1)));
                licenceInfo.setEndDate(sdf.parse(cursor.getString(2)));
            }
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return licenceInfo;
    }

    public boolean updateLicense(String duration, Date startDate, Date endDate) {
        databaseWriteTransaction(flag);
        try {
            SimpleDateFormat sdf   = new SimpleDateFormat("yyyy/MM/dd");
            String           start = sdf.format(startDate);
            String           end   = sdf.format(endDate);
            String query = "update " + AppConstant.LICENSE_INFO_TABLE_NAME + " set " + AppConstant.LICENSE_INFO_DURATION + " = ?, " + AppConstant.LICENSE_INFO_START_DATE +
                    "= ?, " + AppConstant.LICENSE_INFO_END_DATE + "=?";
            db.execSQL(query, new String[]{duration, start, end});
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean updatePasscode(int passcode_id, String passcode) {
        String query = "update " + AppConstant.PASSCODE_TABLE_NAME + " set " + AppConstant.PASSCODE_PASSCODE + " = ? where " + AppConstant.PASSCODE_ID + " = " + passcode_id;
        databaseWriteTransaction(flag);
        try {
            db.execSQL(query, new String[]{passcode});
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean changePasscode(long userId, String passcode) {
        ContentValues values = new ContentValues();
        values.put("passcode", passcode);
        String query = "insert into " + AppConstant.PASSCODE_TABLE_NAME + " values (?, ?);";
        databaseWriteTransaction(flag);
        try {
            db.update(AppConstant.TABLE_USER_ROLES, values, " id=?", new String[]{userId + ""});
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean isPasscodeSet(long userId) {
        String query = "SELECT " + AppConstant.COLUMN_USER_PASSCODE +
                " FROM " + AppConstant.TABLE_USER_ROLES +
                " WHERE " + AppConstant.COLUMN_USER_ID + "=" + userId;

        sqLiteDatabaseReadTransaction(flag);
        try (Cursor cursor = db.rawQuery(query, null)) {
            if (cursor.moveToFirst()) {
                return !cursor.getString(0).equals("");
            }
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
            return false;
        } finally {
            db.endTransaction();
        }
        return false;
    }

    //    public String getPasscode(){
    //        String query = "select " + AppConstant.PASSCODE_PASSCODE + " from " + AppConstant.PASSCODE_TABLE_NAME + " where " + AppConstant.PASSCODE_ID  + " = 1 ;";
    //        sqLiteDatabaseReadTransaction(flag);
    //        try {
    //            Cursor cursor = db.rawQuery(query, null);
    //            if (cursor.moveToFirst()){
    //                return true;
    //            }
    //            db.setTransactionSuccessful();
    //        }catch (SQLiteException e){
    //            e.printStackTrace();
    //            return false;
    //        } finally {
    //            db.endTransaction();
    //        }
    //        return false;
    //    }

    //region User Role table
    public boolean validatePasscode(long userId, String passcode) {
        String query = "select passcode from " + AppConstant.TABLE_USER_ROLES + " where " + AppConstant.COLUMN_USER_ID + " = ? ";
        sqLiteDatabaseReadTransaction(flag);
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(query, new String[]{userId + ""});
            if (cursor.moveToFirst()) {
                return cursor.getString(0).equals(passcode);
            }
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
            return false;
        } finally {
            db.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return false;
    }

    public boolean addNewUser(String name, String passcode, String role, byte[] picture) {
        db = apiDatabaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(AppConstant.COLUMN_USER_NAME, name);
        values.put(AppConstant.COLUMN_USER_PASSCODE, passcode);
        values.put(AppConstant.COLUMN_USER_ROLE, role);
        values.put(AppConstant.COLUMN_USER_PICTURE, picture);

        long id = -1;
        try {
            id = db.insert(AppConstant.TABLE_USER_ROLES, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return id > -1;
    }

    public List<User> getAllUserRoles() {
        db = apiDatabaseHelper.getReadableDatabase();
        Cursor     cursor   = null;
        List<User> userList = new ArrayList<>();
        String     query    = "select * from " + AppConstant.TABLE_USER_ROLES;
        try {
            cursor = db.rawQuery(query, null);
            Log.d("OK", cursor.getCount() + "");
            if (cursor.moveToFirst()) {
                do {
                    User user = new User();
                    user.setId(cursor.getLong(0));
                    user.setUserName(cursor.getString(1));
                    user.setPasscode(cursor.getString(2));
                    user.setRole(cursor.getString(3));
                    // 4 is description
                    user.setPicture(cursor.getBlob(5));

                    //                    user.setDescription(cursor.getString(3));
                    userList.add(user);
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            // if (databaseHelper != null)
            //databaseHelper.close();
            if (cursor != null)
                cursor.close();
        }
        return userList;
    }

    public Long getOwnerId() {
        db = apiDatabaseHelper.getReadableDatabase();
        String projection[]    = new String[]{AppConstant.COLUMN_USER_ID};
        String selection       = AppConstant.COLUMN_USER_ROLE + " = ? ";
        String selectionArgs[] = new String[]{User.ROLE.Owner.toString()};

        Long ownerId = null;
        Cursor cursor = db.query(AppConstant.TABLE_USER_ROLES,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null);
        if (cursor.moveToFirst()) {
            ownerId = cursor.getLong(0);
        }
        cursor.close();

        return ownerId;
    }



    public int updateUserRole(long id, String username, String passcode, String role, byte[] picture) {
        db = apiDatabaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(AppConstant.COLUMN_USER_NAME, username);
        values.put(AppConstant.COLUMN_USER_PASSCODE, passcode);
        values.put(AppConstant.COLUMN_USER_ROLE, role);
        values.put(AppConstant.COLUMN_USER_PICTURE, picture);

        int returnedInt = -1;
        try {
            returnedInt = db.update(AppConstant.TABLE_USER_ROLES, values, AppConstant.COLUMN_USER_ID + " =?", new String[]{id + ""});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnedInt;
    }

    public boolean deleteUser(long id) {
        db = apiDatabaseHelper.getWritableDatabase();
        long deletedRows = 0;
        try {
            deletedRows = db.delete(AppConstant.TABLE_USER_ROLES, " id=?", new String[]{id + ""});
        } catch (Exception e) {
            e.printStackTrace();
        }

        return deletedRows > 0;
    }

    public User getUser(long id) {
        db = apiDatabaseHelper.getReadableDatabase();
        try (Cursor cursor = db.query(
                AppConstant.TABLE_USER_ROLES,
                null,
                " id=?",
                new String[]{id + ""},
                null,
                null,
                null)) {
            if (cursor.moveToFirst()) {
                int idIndex          = cursor.getColumnIndex(AppConstant.COLUMN_USER_ID);
                int nameIndex        = cursor.getColumnIndex(AppConstant.COLUMN_USER_NAME);
                int descriptionIndex = cursor.getColumnIndex(AppConstant.COLUMN_USER_DESCRIPTION);
                int roleIndex        = cursor.getColumnIndex(AppConstant.COLUMN_USER_ROLE);
                int passcodeIndex    = cursor.getColumnIndex(AppConstant.COLUMN_USER_PASSCODE);
                int pictureIndex     = cursor.getColumnIndex(AppConstant.COLUMN_USER_PICTURE);

                return new User(cursor.getLong(idIndex), cursor.getString(nameIndex), cursor.getString(passcodeIndex), cursor.getString(roleIndex));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean userNameAlreadyExist(String name) {
        db = apiDatabaseHelper.getReadableDatabase();

        boolean exists = false;
        /*
        SELECT name FROM userRoles
        WHERE LOWER(name)=LOWER(name)
         */
        try (Cursor cursor = db.rawQuery(
                "SELECT " + AppConstant.COLUMN_USER_NAME + " FROM " + AppConstant.TABLE_USER_ROLES +
                        " WHERE LOWER(" + AppConstant.COLUMN_USER_NAME + ")=LOWER('" + name + "')", null)) {
            exists = cursor.getCount() > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return exists;
    }
    //endregion

    //region Authorized Devices table
    public long insertDevice(Device device) {
        db = apiDatabaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(AppConstant.COLUMN_DEVICE_SERIAL_NO, device.getSerialNo());
        values.put(AppConstant.COLUMN_DEVICE_NAME, device.getDeviceName());
        values.put(AppConstant.COLUMN_DEVICE_PERMISSION, device.getPermission());

        return db.replace(AppConstant.TABLE_AUTHORIZED_DEVICES, null, values);
    }

    public void updateDevicePermission(Device device) {
        db = apiDatabaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(AppConstant.COLUMN_DEVICE_PERMISSION, device.getPermission());
        db.update(AppConstant.TABLE_AUTHORIZED_DEVICES, values, AppConstant.COLUMN_DEVICE_ID + "=?", new String[]{device.getId() + ""});
    }

    public Device getDevice(String deviceId) {
        db = apiDatabaseHelper.getReadableDatabase();
        Device device = null;

        try (Cursor cursor = db.query(AppConstant.TABLE_AUTHORIZED_DEVICES, null, AppConstant.COLUMN_DEVICE_ID + "=?",
                new String[]{deviceId}, null, null, null, null)) {

            if (cursor.moveToFirst()) {
                device = new Device();
                int deviceIdIndex         = cursor.getColumnIndex(AppConstant.COLUMN_DEVICE_ID);
                int deviceNameIndex       = cursor.getColumnIndex(AppConstant.COLUMN_DEVICE_NAME);
                int deviceSerialNoIndex   = cursor.getColumnIndex(AppConstant.COLUMN_DEVICE_SERIAL_NO);
                int devicePermissionIndex = cursor.getColumnIndex(AppConstant.COLUMN_DEVICE_PERMISSION);

                device.setId(cursor.getLong(deviceIdIndex));
                device.setSerialNo(cursor.getString(deviceSerialNoIndex));
                device.setDeviceName(cursor.getString(deviceNameIndex));
                device.setPermission(cursor.getString(devicePermissionIndex));
            }
        }

        return device;
    }

    public List<Device> getAllDevices() {
        db = apiDatabaseHelper.getReadableDatabase();
        String selection = AppConstant.COLUMN_DEVICE_ID + " != ?";
        String[] selectionArgs = new String[] {"1"};
        /*
        CASE
        WHEN permission IS NULL THEN 1
        ELSE 2
        END, permission
         */
        String orderBy = "CASE " +
                "WHEN " + AppConstant.COLUMN_DEVICE_PERMISSION + " is NULL THEN 1 " +
                "ELSE 2 " +
                "END, " + AppConstant.COLUMN_DEVICE_PERMISSION;

        List<Device> deviceList = new ArrayList<>();
        try (Cursor cursor = db.query(AppConstant.TABLE_AUTHORIZED_DEVICES,
                null,
                selection,
                selectionArgs,
                null,
                null,
                orderBy)) {

            if (cursor.moveToFirst()) {
                int deviceIdIndex         = cursor.getColumnIndex(AppConstant.COLUMN_DEVICE_ID);
                int deviceNameIndex       = cursor.getColumnIndex(AppConstant.COLUMN_DEVICE_NAME);
                int deviceSerialNoIndex   = cursor.getColumnIndex(AppConstant.COLUMN_DEVICE_SERIAL_NO);
                int devicePermissionIndex = cursor.getColumnIndex(AppConstant.COLUMN_DEVICE_PERMISSION);

                do {
                    Device device = new Device();
                    device.setId(cursor.getLong(deviceIdIndex));
                    device.setDeviceName(cursor.getString(deviceNameIndex));
                    device.setSerialNo(cursor.getString(deviceSerialNoIndex));
                    device.setPermission(cursor.getString(devicePermissionIndex));

                    deviceList.add(device);
                } while (cursor.moveToNext());
            }
        }

        return deviceList;
    }

    public String getDevicePermissionStatus(String serialNo) {
        db = apiDatabaseHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + AppConstant.TABLE_AUTHORIZED_DEVICES +
                " WHERE " + AppConstant.COLUMN_DEVICE_SERIAL_NO + "= ?", new String[]{serialNo});

        if (cursor.getCount() <= 0) {
            return null;
        }

        String permission = null;
        try {
            cursor.moveToFirst();
            int permissionIndex = cursor.getColumnIndex(AppConstant.COLUMN_DEVICE_PERMISSION);
            permission = cursor.getString(permissionIndex);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }

        return permission;
    }

    public void deleteDevice(Long id) {
        db = apiDatabaseHelper.getWritableDatabase();
        db.delete(AppConstant.TABLE_AUTHORIZED_DEVICES, AppConstant.COLUMN_DEVICE_ID + "=?", new String[]{id.toString()});
    }

    public int getAllowedDeviceCount() {
        db = apiDatabaseHelper.getReadableDatabase();

        /*
        SELECT * FROM authorized_devices
        FROM permission='Allowed'
         */
        Cursor cursor = db.rawQuery("SELECT * FROM " + AppConstant.TABLE_AUTHORIZED_DEVICES +
                " WHERE " + AppConstant.COLUMN_DEVICE_PERMISSION + "='" + AppConstant.PERMISSION_ALLOWED + "'", null);
        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    public Long getDeviceId(String serialNo) {
        db = apiDatabaseHelper.getReadableDatabase();

        String[] projections = new String[] {AppConstant.COLUMN_DEVICE_ID};
        String selection = AppConstant.COLUMN_DEVICE_SERIAL_NO + " = ? ";
        String[] selectionArgs = new String[] {serialNo};

        Cursor cursor = db.query(AppConstant.TABLE_AUTHORIZED_DEVICES,
                projections,
                selection,
                selectionArgs,
                null,
                null,
                null,
                null);

        Long deviceId = null;
        if (cursor.moveToFirst()) {
            deviceId = cursor.getLong(0);
        }
        cursor.close();

        return deviceId;
    }

    public String getDeviceSerialNo(Long deviceId) {
        db = apiDatabaseHelper.getReadableDatabase();

        String[] projections = new String[] {AppConstant.COLUMN_DEVICE_SERIAL_NO};
        String selection = AppConstant.COLUMN_DEVICE_ID + " = ? ";
        String[] selectionArgs = new String[] {deviceId.toString()};

        Cursor cursor = db.query(AppConstant.TABLE_AUTHORIZED_DEVICES,
                projections,
                selection,
                selectionArgs,
                null,
                null,
                null,
                null);

        String serialNo = null;
        if (cursor.moveToFirst()) {
            serialNo = cursor.getString(0);
        }
        cursor.close();

        return serialNo;
    }
    //endregion
}
