package com.digitalfusion.android.pos.database.model;

import com.digitalfusion.android.pos.util.POSUtil;

import java.io.Serializable;

/**
 * Created by MD002 on 2/6/17.
 */

public class StockValue implements Serializable {
    private Double value;
    private int qty;

    public StockValue() {
    }

    public StockValue(Double value, int qty) {
        this.value = value;
        this.qty = qty;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getValueString() {
        return POSUtil.NumberFormat(value);
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getQtyString() {
        return POSUtil.NumberFormat(qty);
    }

    public String getTotalValueString() {

        return POSUtil.NumberFormat(qty * value);
    }

}
