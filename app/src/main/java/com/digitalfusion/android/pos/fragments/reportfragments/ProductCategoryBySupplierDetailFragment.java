package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForProCatBySupCategory;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.model.ReportItem;

import java.io.Serializable;
import java.util.List;

public class ProductCategoryBySupplierDetailFragment extends Fragment implements Serializable {
    private View mainLayoutView;
    private Context context;
    private TextView headerTextView;
    private RecyclerView recyclerView;
    private ParentRVAdapterForReports rvAdapterForReport;
    private List<ReportItem> reportItemList;
    private ReportManager reportManager;
    private ReportItem reportItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_product_category_by_supplier_detail, container, false);
        if (getArguments() != null) {
            reportItem = (ReportItem) getArguments().getSerializable("supplier");
            //name = getArguments().getString("supplier name");

        }
        context = getContext();
        String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.product_category_by_supplier}).getString(0);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        headerTextView = (TextView) mainLayoutView.findViewById(R.id.supplier_name);
        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.recycler_view);
        Log.e("dddname", reportItem.getName());
        headerTextView.setText(reportItem.getName());

        reportManager = new ReportManager(context);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        reportItemList = reportManager.productCategoryBySupplier(reportItem.getDate(), reportItem.getDate1(), 0, 10, reportItem.getId());
        rvAdapterForReport = new RVAdapterForProCatBySupCategory(reportItemList, context, reportItem.getDate(), reportItem.getDate1());
        recyclerView.setAdapter(rvAdapterForReport);
        //headerTextView.setText(name);
        return mainLayoutView;
    }


}
