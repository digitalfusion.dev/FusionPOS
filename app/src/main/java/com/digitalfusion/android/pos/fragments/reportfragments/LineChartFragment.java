package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForMonthAmtReport;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForTwoColumnsReports;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class LineChartFragment extends Fragment implements Serializable {

    private TextView dateFilterTextView;
    private RVAdapterForFilter rvAdapterForDateFilter;
    private MaterialDialog dateFilterDialog;
    private List<String> filterList;
    private View mainLayoutView;
    private Context context;
    private List<ReportItem> reportItemList;
    private ReportManager reportManager;
    private RecyclerView recyclerView;
    private ParentRVAdapterForReports rvAdapterForReport;
    private LineChart lineChart;
    private String reportType;
    private String startDate;
    private String endDate;
    private ArrayList<Entry> e1;
    private ArrayList<String> xVals;
    private String thisYear;
    private String lastYear;
    private String last12Month;
    private String[] dateFilterArr;
    private boolean isTwelve;
    private String orderby;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_line_chart, container, false);
        context = getContext();
        loadIngUI();
        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            reportType = getArguments().getString("reportType");
        }
        switch (reportType) {
            case "monthly sales transactions":
                String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.monthly_sales_transaction}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                break;
            case "monthly purchase transactions":
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.monthly_purchase_transaction}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                break;
            case "Income":
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.monthly_income}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                break;
            case "Expense":
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.monthly_expense}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                break;
        }

        initializeVariables();
        setDatesAndLimits();

        new LoadProgressDialog().execute("");


        configFilter();

        buildDateFilterDialog();

        setDateFilterTextView(thisYear);

        clickListeners();

    }

    private void configFilter() {


        thisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year}).getString(0);

        lastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString(0);

        last12Month = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_12_month}).getString(0);

        filterList = new ArrayList<>();

        filterList.add(thisYear);

        filterList.add(lastYear);

        filterList.add(last12Month);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void clickListeners() {
        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                dateFilterDialog.dismiss();


                if (filterList.get(position).equalsIgnoreCase(thisYear)) {
                    orderby = "asc";
                    isTwelve = false;

                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    int year = Calendar.getInstance().get(Calendar.YEAR);
                    for (int i = 0; i < 12; i++) {
                        dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
                    }

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {
                    orderby = "asc";
                    isTwelve = false;

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();
                    int year = Calendar.getInstance().get(Calendar.YEAR) - 1;
                    dateFilterArr = new String[12];
                    for (int i = 0; i < 12; i++) {
                        dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
                    }

                } else if (filterList.get(position).equalsIgnoreCase(last12Month)) {
                    orderby = "asc";
                    isTwelve = true;

                    startDate = DateUtility.getLast12MonthStartDate();

                    endDate = DateUtility.getLast12MotnEndDate();

                    Log.e("1 ", startDate + "  " + endDate);

                    dateFilterArr = new String[12];
                    int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
                    int year  = Calendar.getInstance().get(Calendar.YEAR);
                    for (int i = 0; i < 12; i++) {
                        if (month < 1) {
                            year -= 1;
                            month = 12;
                        }
                        dateFilterArr[i] = "(" + (month) + "," + year + ")";
                        month -= 1;
                    }
                }

                new LoadProgressDialog().execute("");

                setDateFilterTextView(filterList.get(position));
            }
        });
    }

    private void setDateFilterTextView(String msg) {

        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {

        String filter = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);
        dateFilterDialog = new MaterialDialog.Builder(context)
                .title(filter)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))

                .build();

    }

    private void loadIngUI() {
        dateFilterTextView = (TextView) mainLayoutView.findViewById(R.id.date_filter);

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.line_chart_recycler_view);
        lineChart = (LineChart) mainLayoutView.findViewById(R.id.line_chart);
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);
        isTwelve = false;
        dateFilterArr = new String[12];
        orderby = "asc";
    }

    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        //        if (reportType.equalsIgnoreCase("monthly sales transactions") || reportType.equalsIgnoreCase("monthly purchase transactions")) {
        //        }else if (reportType.equalsIgnoreCase("monthly gross profit")){
        //            rvAdapterForReport = new RVAdapterForMonthAmtReport(reportItemList);
        //        }

        switch (reportType) {
            case "monthly sales transactions":
            case "monthly purchase transactions":
                rvAdapterForReport = new RVAdapterForTwoColumnsReports(reportItemList, RVAdapterForTwoColumnsReports.Type.MONTH_TRANSACTION, context);
                break;
            case "Expense":
            case "Income":
                rvAdapterForReport = new RVAdapterForMonthAmtReport(reportItemList);
                break;
        }
        recyclerView.setAdapter(rvAdapterForReport);
    }


    private void initializeList() {
        //        if (reportType.equalsIgnoreCase("monthly sales transactions")) {
        //
        //        } else if (reportType.equalsIgnoreCase("monthly gross profit")){
        //            reportManager.createPurchasePriceTempTable(startDate, endDate);
        //            reportItemList = reportManager.monthlyGrossProfit(startDate, endDate, dateFilterArr, orderby);
        //            reportManager.dropPurchasePriceTempTable();
        //           // Log.e("size", reportItemList.size()+ " ida");
        //        } else if (reportType.equalsIgnoreCase("monthly purchase transactions")){
        //        }

        switch (reportType) {
            case "monthly sales transactions":
                reportItemList = reportManager.monthlySalesTransactions(startDate, endDate, dateFilterArr, orderby);
                break;
            case "monthly purchase transactions":
                reportItemList = reportManager.monthlyPurchaseTransactions(startDate, endDate, dateFilterArr, orderby);
                break;
            case "Expense":
            case "Income":
                reportItemList = reportManager.monthlyIncomeExpenseTransactions(reportType, startDate, endDate, dateFilterArr, orderby);
                break;
        }
    }

    private void setDatesAndLimits() {
        startDate = Calendar.getInstance().get(Calendar.YEAR) + "0101";
        //Log.e("start", startDate );
        endDate = Calendar.getInstance().get(Calendar.YEAR) + "1231";
        Log.e("start", endDate);
            /*Calendar c = Calendar.getInstance();
            c.set(2016,0,1,0,0,0);
            startTime = c.getTimeInMillis();
            c.set(2017,0,1,0,0,0);
            endTime = c.getTimeInMillis();*/

        int year = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 0; i < 12; i++) {
            dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
        }
        //startLimit = 0;
        //endLimit = 10;
    }

    public void setLineChartConfiguration() {
        lineChart.setDrawGridBackground(false);
        lineChart.animateX(1400, Easing.EasingOption.EaseInOutQuart);
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelRotationAngle(270);
        xAxis.setYOffset(10f);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);
        xAxis.setSpaceBetweenLabels(1);

        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);


        // change the position of the y-labels

        YAxis leftAxis = lineChart.getAxisLeft();
        //leftAxis.setValueFormatter(new MyYAxisValueFormatter());
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        //leftAxis.setLabelCount(1, false);
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)
        lineChart.getAxisRight().setEnabled(false);
        //leftAxis.setSpaceTop(15f);

        //leftAxis.setLabelCount(5, true);        // no description text
        lineChart.setDescription("");
        String need = context.getTheme().obtainStyledAttributes(new int[]{R.attr.u_need_to_provide_data_for_chart}).getString(0);
        lineChart.setNoDataTextDescription(need);

        // enable touch gestures
        lineChart.setTouchEnabled(true);


        // set the marker to the chart
        // enable scaling and dragging
        lineChart.setDragEnabled(false);
        lineChart.setScaleEnabled(false);
        lineChart.setDrawBorders(false);
        lineChart.getLegend().setEnabled(false);

        if (POSUtil.isLabelWhite(context)) {
            lineChart.getAxisLeft().setTextColor(Color.WHITE);
            lineChart.getXAxis().setTextColor(Color.WHITE);
        }


        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(false);
        lineChart.setDrawGridBackground(false);


        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setLabelCount(5, false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)

        // set data
        // do not forget to refresh the chart
        // holder.chart.invalidate();
        //lineChart.setDescriptionPosition(Float.parseFloat(XAxis.XAxisPosition.TOP.toString()),Float.parseFloat(YAxis.AxisDependency.RIGHT.toString()));


        setLineDataToChart();


        lineChart.invalidate();
    }

    private void setLineDataToChart() {

        initializingDataForLineChart();
        LineDataSet d1 = new LineDataSet(e1, "");
        d1.setLineWidth(2.5f);
        d1.setCircleRadius(4.5f);
        d1.setHighLightColor(Color.rgb(244, 117, 117));
        d1.setDrawValues(false);
        d1.setColor(Color.parseColor("#4dd0e1"));
        d1.setCircleColor(Color.parseColor("#4dd0e1"));
        d1.setDrawFilled(true);
        // fill drawable only supported on api level 18 and above
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.fade_blue);
        d1.setFillDrawable(drawable);

        ArrayList<ILineDataSet> sets = new ArrayList<ILineDataSet>();
        sets.add(d1);
        LineData cd = new LineData(xVals, sets);
        lineChart.setData(cd);
    }

    public void initializingDataForLineChart() {
        reportItemList = new ArrayList<>();
        e1 = new ArrayList<Entry>();
        initializeList();
        int i = 0;
        //        }else if (reportType.equalsIgnoreCase("monthly gross profit")) {
        //            for (ReportItem reportItemView : reportItemList) {
        //                e1.add(new Entry(Float.parseFloat(reportItemView.getBalance().toString()), i));
        //                i++;
        //            }
        //        }

        switch (reportType) {
            case "monthly sales transactions":
            case "monthly purchase transactions":
                for (ReportItem reportItem : reportItemList) {
                    Log.e(reportItem.getQty() + " ", "id");
                    e1.add(new Entry(Float.parseFloat(Integer.toString(reportItem.getQty())), i));
                    i++;
                }
                break;
            case "Expense":
            case "Income":
                for (ReportItem reportItem : reportItemList) {
                    Log.e(reportItem.getQty() + " ", "id");
                    e1.add(new Entry(Float.parseFloat(Double.toString(reportItem.getBalance())), i));
                    i++;
                }
                break;
        }
        //Log.e("e", e1.size()+ " if");
        xVals = new ArrayList<String>();
        if (isTwelve) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -11);
            int cal = calendar.get(Calendar.MONTH);
            for (i = 0; i < 12; i++) {
                xVals.add(AppConstant.MONTHS[cal]);
                cal += 1;
                if (cal > 11) {
                    cal = 0;
                }

            }
        } else {
            for (i = 0; i < 12; i++) {
                //Log.e("ccccc",cal+"");
                xVals.add(AppConstant.MONTHS[i]);
            }
        }
    }


    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String label = "";

            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {
            setLineChartConfiguration();
            configRecyclerView();
            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
