package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForBarChartReportWithNameAmt;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForBarChartReportWithNameQty;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.DefaultYAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BarChartFragment extends Fragment implements Serializable {

    public static int[] colorArr = new int[]{ColorTemplate.rgb("#03A9F4"),
            ColorTemplate.rgb("#CDDC39"),
            ColorTemplate.rgb("#FF9800"),
            ColorTemplate.rgb("#e040fb"),
            ColorTemplate.rgb("#80DEEA"),
            ColorTemplate.rgb("#3F51B5"),
            ColorTemplate.rgb("#FFEB3B"),
            ColorTemplate.rgb("#00BFA5"),
            ColorTemplate.rgb("#757575"),
            ColorTemplate.rgb("#F44336")};
    private TextView dateFilterTextView;
    private RVAdapterForFilter rvAdapterForDateFilter;
    private MaterialDialog dateFilterDialog;
    private List<String> filterList;
    private String reportName;
    private View mainLayoutView;
    private Context context;
    private RecyclerView recyclerView;
    private ParentRVAdapterForReports rvAdapterForReport;
    private BarChart barChart;
    private ReportManager reportManager;
    private List<ReportItem> reportItemList;
    //private ReportItem reportItemView;
    private List<Double> yValList;
    private String startDate;
    private String endDate;
    private int startLimit;
    private int endLimit;
    private ArrayList<String> xValues;
    private List<BarEntry> yValues;
    private String allTrans;
    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;
    private String customRange;
    private MaterialDialog customRangeDialog;
    private TextView startDateTextView, endDateTextView;
    private String customStartDate,
            customEndDate;
    private TextView traceDate;
    private Button customRangeOkBtn, customRangeCancelBtn;
    private DatePickerDialog startDatePickerDialog;
    private TextView noChartDataText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_bar_chart, container, false);
        if (getArguments() != null) {
            reportName = getArguments().getString("reportType");
        }
        if (reportName.equalsIgnoreCase("top sales by customer")) {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.top_sales_by_customer}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        } else if (reportName.equalsIgnoreCase("top sale by qty")) {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.top_sales_by_quantity}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        } else if (reportName.equalsIgnoreCase("top suppliers")) {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.top_supplier}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        }

        loadIngUI();

        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initializeVariables();
        configFilters();
        buildDateFilterDialog();
        setDateFilterTextView(thisMonth);
        buildingCustomRangeDialog();
        Log.e(startDate, endDate);
        new LoadProgressDialog().execute("");

        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                dateFilterDialog.dismiss();

                if (filterList.get(position).equalsIgnoreCase(thisMonth)) {
                    startDate = DateUtility.getThisMonthStartDate();
                    endDate = DateUtility.getThisMonthEndDate();

                    new LoadProgressDialog().execute("");
                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {
                    startDate = DateUtility.getLastMonthStartDate();
                    endDate = DateUtility.getLastMonthEndDate();

                    new LoadProgressDialog().execute("");
                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {
                    startDate = DateUtility.getThisYearStartDate();
                    endDate = DateUtility.getThisYearEndDate();

                    new LoadProgressDialog().execute("");
                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {
                    startDate = DateUtility.getLastYearStartDate();
                    endDate = DateUtility.getLastYearEndDate();

                    new LoadProgressDialog().execute("");
                    setDateFilterTextView(filterList.get(position));
                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {
                    customRangeDialog.show();
                }


            }
        });

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDate = customStartDate;
                endDate = customEndDate;

                /*String startDayDes[]= DateUtility.dayDes(startDate);

                String startYearMonthDes= DateUtility.monthYearDes(startDate);


                String endDayDes[]= DateUtility.dayDes(endDate);

                String endYearMonthDes= DateUtility.monthYearDes(endDate);


                dateFilterTextView.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));
*/
                dateFilterTextView.setText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));
                new LoadProgressDialog().execute("");
                customRangeDialog.dismiss();
            }
        });

        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRangeDialog.dismiss();
            }
        });


    }

    private void configFilters() {

        thisMonth   = ThemeUtil.getString(context, R.attr.this_month  );
        lastMonth   = ThemeUtil.getString(context, R.attr.last_month  );
        thisYear    = ThemeUtil.getString(context, R.attr.this_year   );
        lastYear    = ThemeUtil.getString(context, R.attr.last_year   );
        customRange = ThemeUtil.getString(context, R.attr.custom_range);

        filterList = new ArrayList<>();
        filterList.add(thisMonth);
        filterList.add(lastMonth);
        filterList.add(thisYear);
        filterList.add(lastYear);
        filterList.add(customRange);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    public void buildingCustomRangeDialog() {
        String dateRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range}).getString(0);
        buildingCustomRangeDatePickerDialog();
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(dateRange)
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.negativeText("Cancel")
                //.positiveText("OK")
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    private void setDateFilterTextView(String msg) {

        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {

        String filter = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);
        dateFilterDialog = new MaterialDialog.Builder(context)
                .title(filter)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))
                .build();

    }

    private void loadIngUI() {
        noChartDataText = (TextView) mainLayoutView.findViewById(R.id.no_chart_data_text);
        dateFilterTextView = (TextView) mainLayoutView.findViewById(R.id.date_filter);
        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.bar_chart_recycler_view);
        barChart = (BarChart) mainLayoutView.findViewById(R.id.bar_chart);
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);

        setDatesAndLimits();
    }

    private void initializeList() {

        if (reportName.equalsIgnoreCase("top sale by qty")) {

            reportItemList = reportManager.topSalesByQty(startDate, endDate, startLimit, endLimit);

        } else if (reportName.equalsIgnoreCase("bottom sale items")) {

            reportItemList = reportManager.bottomSalesByQty(startDate, endDate, startLimit, endLimit);

        } else if (reportName.equalsIgnoreCase("top sales by customer")) {

            reportItemList = reportManager.topSalesCustomers(startDate, endDate, startLimit, endLimit);

        } else if (reportName.equalsIgnoreCase("top sale by products")) {
            Log.e(startDate, endDate + "d");

        } else if (reportName.equalsIgnoreCase("top outstanding suppliers")) {

            reportItemList = reportManager.topOutstandingSupplier(startLimit, endLimit);

        } else if (reportName.equalsIgnoreCase("top suppliers")) {

            reportItemList = reportManager.topSuppliers(startDate, endDate, startLimit, endLimit);

        }

    }

    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        if (reportItemList.size() < 1) {
            noChartDataText.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);

        } else {

            noChartDataText.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            switch (reportName) {
                case "top sale by qty":
                case "bottom sale items":
                    rvAdapterForReport = new RVAdapterForBarChartReportWithNameQty(reportItemList);
                    break;
                case "top sales by customer":
                case "top outstanding suppliers":
                case "top suppliers":
                    rvAdapterForReport = new RVAdapterForBarChartReportWithNameAmt(reportItemList);
                    break;
            }
            recyclerView.setAdapter(rvAdapterForReport);
        }


    }

    private void setDatesAndLimits() {
        startDate = DateUtility.getThisMonthStartDate();
        endDate = DateUtility.getThisMonthEndDate();
        startLimit = 0;
        endLimit = 10;
        Log.e(startDate, endDate);
    }

    public void settingBarChart() {

        // barChart.setDescriptionPosition(Float.parseFloat(XAxis.XAxisPosition.TOP.toString()),Float.parseFloat(YAxis.AxisDependency.RIGHT.toString()));
        barChart.setDrawBorders(false);
        barChart.setPinchZoom(true);
        barChart.setPivotY(10);
        barChart.setDrawBarShadow(false);
        barChart.setMaxVisibleValueCount(30);
        barChart.setDescription("");

        String noChartData = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_chart_data_available}).getString(0);
        String forstr      = context.getTheme().obtainStyledAttributes(new int[]{R.attr.for_t}).getString(0);

        barChart.setDrawGridBackground(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setTouchEnabled(false);
        barChart.animateXY(1400, 1400, Easing.EasingOption.EaseInBack, Easing.EasingOption.EaseInBack);
        barChart.getLegend().setEnabled(false);

        if (POSUtil.isLabelWhite(context)) {
            barChart.getAxisLeft().setTextColor(Color.WHITE);
            barChart.getXAxis().setTextColor(Color.WHITE);
        }

        Legend l = barChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setYOffset(2f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f);
        l.setXOffset(3f);


        configXAxis();
        configYAxis();
        setBarDataToChart();
        barChart.invalidate();
    }

    private void setBarDataToChart() {
        xValues = new ArrayList<>();
        yValList = new ArrayList<>();
        switch (reportName) {
            case "top sale by qty":
            case "bottom sale items":
                for (ReportItem reportItem : reportItemList) {
                    //Log.e("ccccc",cal+"");
                    xValues.add(reportItem.getName());
                    yValList.add((double) reportItem.getQty());
                }
                break;
            case "top sales by customer":
            case "top outstanding suppliers":
            case "top suppliers":
                for (ReportItem reportItem : reportItemList) {
                    //Log.e("ccccc",cal+"");
                    xValues.add(reportItem.getName());
                    yValList.add(reportItem.getBalance());
                }
                break;
        }


        yValues = getYAxisBarEntries(yValList);


        BarData data = configBarData(yValues, xValues);

        barChart.setData(data);
        for (IBarDataSet set : barChart.getData().getDataSets())
            set.setDrawValues(!set.isDrawValuesEnabled());

    }

    private void configYAxis() {
        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setValueFormatter(new DefaultYAxisValueFormatter(1));
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(30f);
        leftAxis.setAxisMinValue(0);

        barChart.getAxisRight().setEnabled(false);
    }

    private void configXAxis() {
        XAxis xl = barChart.getXAxis();
        xl.setLabelRotationAngle(270);
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setDrawGridLines(false);
        xl.setSpaceBetweenLabels(1);
        xl.setDrawLabels(false);
    }

    /**
     * @param yValue is data want to add to bar entry.
     * @return bart entry.
     */
    @NonNull
    private List<BarEntry> getYAxisBarEntries(List<Double> yValue) {
        List<BarEntry> yVals1 = new ArrayList<BarEntry>();
        int            j      = 0;
        for (Double t : yValue) {
            Float f = Float.valueOf(t.toString());
            yVals1.add(new BarEntry(f, j));
            ++j;
        }
        return yVals1;
    }

    @NonNull
    private BarData configBarData(List<BarEntry> yVals, List<String> xVals) {
        BarDataSet set1 = new BarDataSet(yVals, "");
        set1.setColors(colorArr);
        List<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(xVals, dataSets);

        // add space between the dataset groups in percent of bar-width
        data.setGroupSpace(80f);

        //mChart.setDescription(calendar.get(Calendar.YEAR) + " " + xValues.get(0) + " - " + xValues.get(11));

        return data;
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String label = "";

            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDimAmount(0.5f)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {

            settingBarChart();
            configRecyclerView();

            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
