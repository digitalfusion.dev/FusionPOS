package com.digitalfusion.android.pos.database.model;


/**
 * Created by lyx on 3/30/18.
 *
 * So, I created this model because I don't want to risk breaking things if I modified DeviceInfo
 */

public class Device {
    private long id;
    private String serialNo;
    private String deviceName;
    private String permission;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
