package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.database.dao.StockDAO;
import com.digitalfusion.android.pos.database.model.Stock;
import com.digitalfusion.android.pos.database.model.StockAutoComplete;
import com.digitalfusion.android.pos.database.model.StockImage;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/18/16.
 */
public class StockManager {

    private Context context;
    private StockDAO stockDAO;
    private List<StockItem> stockItemList;

    public StockManager(Context context) {
        this.context = context;
        stockDAO = StockDAO.getStockDaoInstance(context);
        stockItemList = new ArrayList<>();
    }

    public boolean addNewStock(String codeNo, String barcode, String name, Long categoryID, Long unitID, Integer inventoryQty, Integer reorderLevel,
                               Double purchasePrice, Double wholesalePrice, Double normalPrice, Double retailPrice, List<StockImage> imageList, String description) {
        InsertedBooleanHolder isInserted = new InsertedBooleanHolder();
        stockDAO.addNewStock(codeNo, barcode, name, categoryID, unitID, inventoryQty, reorderLevel, purchasePrice, wholesalePrice, normalPrice, retailPrice, imageList, description, isInserted);
        return isInserted.isInserted();
    }

    public boolean checkStockCodeExists(String stockCode) {
        return stockDAO.checkStockCodeAlreadyExists(stockCode);
    }

    public Long getStockIDByStockCode(String stockCode) {
        return stockDAO.getStockIDByStockCode(stockCode);
    }

    public boolean checkBarcodeExists(String barcode) {
        return stockDAO.checkBarcodeAlreadyExists(barcode);
    }

    public List<StockItem> getAllStocks(int startLimit, int endLimit) {
        stockItemList = stockDAO.getAllStocks(startLimit, endLimit, 2);
        return stockItemList;
    }

    public StockItem getStocksByID(Long id) {
        StockItem stockItem = stockDAO.getStockByID(id);
        return stockItem;
    }

    public List<StockItem> getAllActiveStocks(int startLimit, int endLimit) {
        stockItemList = stockDAO.getAllStocks(startLimit, endLimit, 1);
        return stockItemList;
    }

    public List<StockItem> getAllActiveStocks() {
        stockItemList = stockDAO.getAllStocks(1);
        return stockItemList;
    }

    public List<StockItem> getAllStocksWithNoOfSuppliers(int startLimit, int endLimit) {
        stockItemList = stockDAO.getAllStocksWithNoOfSuppliers(startLimit, endLimit);
        return stockItemList;
    }

    public List<StockItem> getAllStocksWithNoOfSuppliersByCategoryID(int startLimit, int endLimit, Long categoryID) {
        return stockDAO.getAllStocksWithNoOfSuppliersByCategoryID(startLimit, endLimit, categoryID);
    }


    public List<StockItem> getAllStocksWithNoOfSuppliersByStockID(Long stockID) {
        return stockDAO.getAllStocksWithNoOfSuppliersByStockID(stockID);
    }

    public List<StockItem> getStocksByNameOrCode(int startLimit, int endLimit, String nameOrCode) {
        return stockDAO.getAllStocksSearchByNameOrCode(startLimit, endLimit, nameOrCode, 2);
    }

    public List<StockItem> getActiveStocksByNameOrCode(int startLimit, int endLimit, String nameOrCode) {
        return stockDAO.getAllStocksSearchByNameOrCode(startLimit, endLimit, nameOrCode, 1);
    }

    public int getReorderCount() {
        return stockDAO.allStockReorderCounts();
    }

    public List<StockItem> getStockListByCategory(int startLimit, int endLimit, Long categoryId) {
        stockItemList = stockDAO.getStockListByCategory(startLimit, endLimit, categoryId);
        return stockItemList;
    }

    public int getInProcessAtyById(Long stockId) {
        return stockDAO.inProcessQty(stockId);

    }

    public List<StockItem> getAllReorderStocks(int startLimit, int endLimit) {
        stockItemList = stockDAO.getAllReorderStocks(startLimit, endLimit);
        return stockItemList;
    }

    public List<StockItem> getAllReorderStocks(int startLimit, int endLimit, Long categoryId) {
        stockItemList = stockDAO.getAllReorderStocks(startLimit, endLimit, categoryId);
        return stockItemList;
    }

    public int getAllStockCounts() {
        return stockDAO.allStockCounts();
    }

    public int getStockCountsByCategory(Long categoryID) {
        return stockDAO.allStockCountsByCategory(categoryID);
    }

    public Stock findStockByStockCode(String codeNo) {
        return stockDAO.findStockByStockCode(codeNo);
    }

    public Stock findStockByBarCode(String codeNo) {
        return stockDAO.findStockByBarCode(codeNo);
    }

    public Stock findStockByName(String name) {
        return stockDAO.findStockByName(name);
    }

    public Stock findStockByID(Long id) {
        return stockDAO.findStockByID(id);
    }

    public boolean updateStock(String codeNo, String barcode, String name, Long categoryID, Long unitID, int inventoryQty, int reorderLevel,
                               Double purchasePrice, Double wholesalePrice, Double normalPrice, Double retailPrice, List<Long> deletedImageIdList, List<StockImage> insertImgList,
                               String description, Long id) {
        return stockDAO.updateStock(codeNo, barcode, name, categoryID, unitID, inventoryQty, reorderLevel, purchasePrice, wholesalePrice, normalPrice, retailPrice, deletedImageIdList, insertImgList, description, id);
    }

    public List<StockAutoComplete> findStockByBarcodeForAutoComplete(String barcode) {
        return stockDAO.findStockByBarcodeForAutoComplete(barcode);
    }

    public List<StockAutoComplete> findStockByCodeNoForAutoComplete(String codeNo) {
        return stockDAO.findStockByCodeNoForAutoComplete(codeNo);
    }

    public List<StockAutoComplete> findStockByNameForAutoComplete(String name) {
        return stockDAO.findStockByNameForAutoComplete(name);
    }


    public List<StockImage> getImagesByStockID(Long stockID) {
        return stockDAO.getImagesByStockID(stockID);
    }


    public Long addQuickStockSetupSale(String name, String stockCode, Double retailPrice, Double purchasePrice) {

        return stockDAO.addQuickStockSetup(name, stockCode, retailPrice, purchasePrice);
    }

    public Long addQuickStockSetupPurchase(String name, String stockCode, Double purchasePrice, Double retailPrice) {

        return stockDAO.addQuickStockSetupPurchase(name, stockCode, purchasePrice, retailPrice);
    }

    public boolean deleteStock(Long id) {
        return stockDAO.deleteStock(id);
    }

    public List<StockItem> getDamagesForSearch(int startLimit, int endLimit, String searchStr) {
        return stockDAO.getDamagesForSearch(startLimit, endLimit, searchStr);
    }

    public List<StockItem> getLostForSearch(int startLimit, int endLimit, String searchStr) {
        return stockDAO.getLostForSearch(startLimit, endLimit, searchStr);
    }
}
