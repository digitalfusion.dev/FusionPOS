package com.digitalfusion.android.pos.fragments.dashboardfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.digitalfusion.android.pos.R;

/**
 * Created by MD002 on 5/31/17.
 */

public class TabsPagerAdapter extends FragmentStatePagerAdapter {

    private DashboardFragmentV1 dashboardFragmentForDay;
    private DashboardFragmentV1 dashboardFragmentForWeek;
    private DashboardFragmentV1 dashboardFragmentForMonth;
    private DashboardFragmentV1 dashboardFragmentForYear;

    private Context context;

    public TabsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        dashboardFragmentForDay = new DashboardFragmentV1();
        dashboardFragmentForWeek = new DashboardFragmentV1();
        dashboardFragmentForMonth = new DashboardFragmentV1();
        dashboardFragmentForYear = new DashboardFragmentV1();
    }


    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();

        Log.e("position", "tab " + position);
        switch (position) {

            case 0:
                Log.e("tab pager adapter", "day");
                bundle.putString("title", "day");
                dashboardFragmentForDay.setArguments(bundle);
                return dashboardFragmentForDay;
            // MainActivity.replacingTabFragmentWithoutSearchView(dashboardFragmentForDay);

            case 1:
                Log.e("tab pager adapter", "week");
                bundle.putString("title", "week");
                dashboardFragmentForWeek.setArguments(bundle);
                return dashboardFragmentForWeek;
            // MainActivity.replacingTabFragmentWithoutSearchView(dashboardFragmentForDay);

            case 2:
                Log.e("tab pager adapter", "month");
                bundle.putString("title", "month");
                dashboardFragmentForMonth.setArguments(bundle);
                return dashboardFragmentForMonth;
            //  MainActivity.replacingTabFragmentWithoutSearchView(dashboardFragmentForDay);

            case 3:
                Log.e("tab pager adapter", "year");
                bundle.putString("title", "year");
                dashboardFragmentForYear.setArguments(bundle);
                return dashboardFragmentForYear;
            //MainActivity.replacingTabFragmentWithoutSearchView(dashboardFragmentForDay);

        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "apple";
        Log.e("title P", "O " + position);
        switch (position) {
            case 0:
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.day}).getString(0);
                break;
            case 1:
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.week}).getString(0);
                break;
            case 2:
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.month_title}).getString(0);
                break;
            case 3:
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.year}).getString(0);
                break;
        }
        return title;
    }
}
