package com.digitalfusion.android.pos.fragments.inventoryfragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockNameOrCodeAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockSearchAdapterForDamage;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterforStockListMaterialDialog;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForDamageItemList;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.AuthorizationManager;
import com.digitalfusion.android.pos.database.business.DamageManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.DamagedItem;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.example.searchview.MaterialSearchView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 11/15/16.
 */

public class DamageListFragmentWithAddDailog extends Fragment {

    TypedArray addDamagedItem;
    TypedArray editDamagedItem;
    TypedArray areUSureWantToDelete;
    private View mainLayoutView;
    private FloatingActionButton addNewDamageFab;
    //For damage list
    private RecyclerView recyclerView;
    private DamageManager damageManager;

    //private UserManager userBusiness;
    private Context context;
    private MaterialSearchView searchView;
    private StockSearchAdapterForDamage stockSearchAdapterForDamage;
    private List<String> filterList;
    private TextView filterTextView;
    private RVAdapterForFilter rvAdapterForFilter;
    private DatePickerDialog customeDatePickerDialog;
    private MaterialDialog filterDialog;
    private String startDate;


    //View from add Item Dialog
    private String endDate;
    private Calendar calendar;
    private MaterialDialog addItemMaterialDialog;
    private AutoCompleteTextView itemCodeEditText;
    private AutoCompleteTextView itemNameEditText;
    private EditText itemQtyEditText;
    private Button cancelAddItemDamageDialogButton;
    private Button addItemDamageDialogButton;
    private AppCompatImageButton qtyPlusButton;
    private AppCompatImageButton qtyMinusButton;
    private AppCompatImageButton stockListButton;
    private EditText remarkEditText;
    private RVSwipeAdapterForDamageItemList rvSwipeAdapterForDamageItemList;
    private List<DamagedItem> damageItemList;
    private StockNameOrCodeAutoCompleteAdapter stockNameAutoCompleteAdapter;
    private StockNameOrCodeAutoCompleteAdapter stockCodeAutoCompleteAdapter;
    private DamagedItem damageItemInDetailView;
    private int qty;

    // private List<StockItem> stockItemList;
    private StockManager stockManager;
    private MaterialDialog stockListMaterialDialog;
    private RVAdapterforStockListMaterialDialog rvAdapterforStockListMaterialDialog;
    private boolean isEdit = false;
    private int editPosition;
    private DatePickerDialog datePickerDialog;
    private DatePickerDialog editDatePickerDialog;
    private int day;
    private int month;
    private int damageYear;
    private String date;

    // private DamageSearchAdapter damageSearchAdapter;
    private Calendar now;
    private LinearLayout dateLinearLayout;
    private TextView dateTextView;
    private boolean isSearch;
    private boolean shouldLoad = true;
    private TextView noTransactionTextView;
    private DatePickerDialog startDatePickerDialog;
    private MaterialDialog customRangeDialog;
    private TextView startDateTextView, endDateTextView;
    private String customStartDate,
            customEndDate;
    private TextView traceDate;
    private Button customRangeOkBtn, customRangeCancelBtn;
    private MaterialDialog deleteAlertDialog;
    private Button yesDeleteMdButton;
    private TextView searchedResultTxt;
    private String allTrans;
    private String thisWeek;
    private String lastWeek;
    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;
    private String customRange;
    private String customDate;
    private Long stockID;
    private List<StockItem> stockItemList;
    private boolean darkmode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.damage_list_fragment, null);

        context = getContext();
        stockID = 0l;
        isSearch = false;

        addDamagedItem = mainLayoutView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.add_damaged_item});

        editDamagedItem = mainLayoutView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.edit_damaged_item});

        areUSureWantToDelete = mainLayoutView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete});

        ///((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Damage Transactions");

        darkmode = POSUtil.isNightMode(context);

        now = Calendar.getInstance();

        day = now.get(Calendar.DAY_OF_MONTH);

        month = now.get(Calendar.MONTH) + 1;

        damageYear = now.get(Calendar.YEAR);

        damageManager = new DamageManager(context);

        stockManager = new StockManager(context);

        stockItemList = stockManager.getAllStocks(0, 100);

        setHasOptionsMenu(true);

        //userBusiness=new UserManager(context);

        calendar = Calendar.getInstance();

        //MainActivity.setCurrentFragment(this);

        damageItemList = new ArrayList<>();

        ///  damageItemList=damageManager.getAllDamages(0,10);

        rvSwipeAdapterForDamageItemList = new RVSwipeAdapterForDamageItemList(damageItemList);

        loadAddItemDialog();

        stockCodeAutoCompleteAdapter = new StockNameOrCodeAutoCompleteAdapter(context, stockManager);

        stockNameAutoCompleteAdapter = new StockNameOrCodeAutoCompleteAdapter(context, stockManager);

        initializeDateFilter();

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();

        loadUI();

        configRecyclerView();

        loadStockListDialog();

        buildingCustomRangeDialog();

        deleteAlertDialog();

        listeners();

        return mainLayoutView;

    }

    private void listeners() {
        yesDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                damageManager.deleteDamage(damageItemList.get(editPosition).getId());

                deleteAlertDialog.dismiss();

                damageItemList.remove(editPosition);

                rvSwipeAdapterForDamageItemList.setDamageItemViewList(damageItemList);

                if (editPosition != 0) {

                    rvSwipeAdapterForDamageItemList.notifyItemRemoved(editPosition);

                    rvSwipeAdapterForDamageItemList.notifyItemRangeChanged(editPosition, damageItemList.size());

                } else {
                    rvSwipeAdapterForDamageItemList.notifyDataSetChanged();
                }


            }
        });

        rvSwipeAdapterForDamageItemList.setViewDetailClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Intent detailIntent = new Intent(context, DetailActivity.class);

                Bundle bundle = new Bundle();

                bundle.putSerializable(DamagedDetailNewFragment.KEY, damageItemList.get(postion));

                detailIntent.putExtras(bundle);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.DAMAGED_DETAIL);

                startActivity(detailIntent);

            }
        });

        recyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {

                Log.w("V X", velocityX + " S");

                Log.w("V Y", velocityY + " S");

                if (velocityY > 100) {
                    addNewDamageFab.hide();
                } else if (velocityY < -100) {
                    addNewDamageFab.show();
                }


                return false;
            }
        });
    }

    private void initializeDateFilter() {
        allTrans = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);
        thisMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month}).getString(0);
        lastMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month}).getString(0);
        thisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year}).getString(0);
        lastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString(0);
        customRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_range}).getString(0);
        customDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_date}).getString(0);
        thisWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_week}).getString(0);
        lastWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week}).getString(0);

        filterList = new ArrayList<>();
        filterList.add(allTrans);
        filterList.add(thisWeek);
        filterList.add(lastWeek);
        filterList.add(thisMonth);
        filterList.add(lastMonth);
        filterList.add(thisYear);
        filterList.add(lastYear);
        filterList.add(customRange);
        filterList.add(customDate);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(rvSwipeAdapterForDamageItemList);

        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                //searchedResultTxt.setVisibility(View.INVISIBLE);

                shouldLoad = true;
                if (!isSearch) {
                    stockID = 0l;
                }
                recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
                    @Override
                    public void onScrollUp() {

                    }

                    @Override
                    public void onScrollDown() {

                    }

                    @Override
                    public void onLoadMore() {

                        Log.w("Load more", "load more");


                        if (shouldLoad) {

                            rvSwipeAdapterForDamageItemList.setShowLoader(true);
                            //                rvSwipeAdapterForSaleTransactions.notifyDataSetChanged();
                            loadmore();
                        }


                    }
                });
                if (filterList.get(position).equalsIgnoreCase(allTrans)) {

                    setFilterTextView(filterList.get(position));

                    startDate = "0000000";
                    endDate = "999999999999999";

                    refreshDamagedList(0, 10);


                } else if (filterList.get(position).equalsIgnoreCase(thisMonth)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisMonthStartDate();

                    endDate = DateUtility.getThisMonthEndDate();

                    refreshDamagedList(0, 10);

                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    refreshDamagedList(0, 10);

                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    refreshDamagedList(0, 10);


                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    refreshDamagedList(0, 10);

                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {

                    //setFilterTextView(filterList.get(position));


                    customRangeDialog.show();

                } else if (filterList.get(position).equalsIgnoreCase(customDate)) {

                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "customeDate");

                } else if (filterList.get(position).equalsIgnoreCase(thisWeek)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getStartDateOfWeekString();

                    endDate = DateUtility.getEndDateOfWeekString();

                    refreshDamagedList(0, 10);

                } else if (filterList.get(position).equalsIgnoreCase(lastWeek)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getStartDateOfLastWeekString();

                    endDate = DateUtility.getEndDateOfLastWeekString();

                    refreshDamagedList(0, 10);

                }

                filterDialog.dismiss();
            }
        });


        // damageSearchAdapter=new DamageSearchAdapter(context, nameFilter);
        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDate = customStartDate;

                endDate = customEndDate;

                //String startDayDes[]= DateUtility.dayDes(startDate);

                //String startYearMonthDes= DateUtility.monthYearDes(startDate);


                //String endDayDes[]= DateUtility.dayDes(endDate);

                //  String endYearMonthDes= DateUtility.monthYearDes(endDate);


                //filterTextView.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));
                filterTextView.setText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));

                refreshDamagedList(0, 10);

                customRangeDialog.dismiss();
            }
        });


        customeDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        String date = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        filterTextView.setText(DateUtility.makeDateFormatWithSlash(date));

                        refreshDamagedList(0, 10);
                    }
                },
                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)
        );


        if (darkmode)
            customeDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        customeDatePickerDialog.setThemeDark(darkmode);


        addNewDamageFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                damageItemInDetailView = new DamagedItem();

                clearAddDialog();

                clearErrorAddDialog();

                addItemMaterialDialog.setTitle(addDamagedItem.getString(0));
                addItemMaterialDialog.show();

            }
        });


        searchViewListeners();

        addItemDamageDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEdit) {

                    if (checkVaildation()) {

                        damageManager.updateDamage(damageItemList.get(editPosition).getId(), damageItemInDetailView.getStockQty(),

                                damageItemInDetailView.getRemark(), 1l, Integer.toString(day), Integer.toString(month), Integer.toString(damageYear), damageItemInDetailView.getStockId());

                        damageItemList.get(editPosition).setDay(String.valueOf(day));

                        damageItemList.get(editPosition).setMonth(String.valueOf(month));

                        damageItemList.get(editPosition).setYear(String.valueOf(damageYear));

                        rvSwipeAdapterForDamageItemList.notifyItemChanged(editPosition);

                        addItemMaterialDialog.dismiss();
                    }

                } else {

                    if (checkVaildation()) {
                        AccessLogManager accessLogManager = new AccessLogManager(context);
                        AuthorizationManager authorizationManager = new AuthorizationManager(context);
                        Long deviceId = authorizationManager.getDeviceId(Build.SERIAL);

                        damageManager.addNewDamage(damageItemInDetailView.getStockQty(), damageItemInDetailView.getStockId(),
                                damageItemInDetailView.getRemark(), Integer.toString(day), Integer.toString(month), Integer.toString(damageYear), accessLogManager.getCurrentlyLoggedInUserId(deviceId));

                        damageItemList.add(damageItemInDetailView);

                        refreshDamagedList(0, 10);

                        addItemMaterialDialog.dismiss();

                    }
                }


            }
        });

        itemNameEditText.setThreshold(1);

        itemNameEditText.setAdapter(stockNameAutoCompleteAdapter);

        itemNameEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                itemCodeEditText.setText(stockNameAutoCompleteAdapter.getSuggestionList().get(position).getCodeNo());

                itemQtyEditText.setText("1");

                damageItemInDetailView.setStockId(stockNameAutoCompleteAdapter.getSuggestionList().get(position).getId());

                damageItemInDetailView.setStockCode(stockNameAutoCompleteAdapter.getSuggestionList().get(position).getCodeNo());

                damageItemInDetailView.setUnitId(stockNameAutoCompleteAdapter.getSuggestionList().get(position).getUnitID());

                damageItemInDetailView.setStockName(stockNameAutoCompleteAdapter.getSuggestionList().get(position).getName());

                //damageItemInDetailView.set(stockNameAutoCompleteAdapter.getSuggestionList().get(position).getPurchasePrice());

                damageItemInDetailView.setStockQty(1);

                damageItemInDetailView.setTotalValue(0.0);

                qty = 1;
            }
        });

        itemCodeEditText.setAdapter(stockCodeAutoCompleteAdapter);

        itemCodeEditText.setThreshold(1);

        itemCodeEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                itemNameEditText.setText(stockCodeAutoCompleteAdapter.getSuggestionList().get(position).getName());

                itemQtyEditText.setText("1");

                damageItemInDetailView.setStockId(stockCodeAutoCompleteAdapter.getSuggestionList().get(position).getId());

                damageItemInDetailView.setStockCode(stockCodeAutoCompleteAdapter.getSuggestionList().get(position).getCodeNo());

                damageItemInDetailView.setUnitId(stockCodeAutoCompleteAdapter.getSuggestionList().get(position).getUnitID());

                damageItemInDetailView.setStockName(stockCodeAutoCompleteAdapter.getSuggestionList().get(position).getName());

                // damageItemInDetailView.set(stockCodeAutoCompleteAdapter.getSuggestionList().get(position).getPurchasePrice());

                damageItemInDetailView.setStockQty(1);
                damageItemInDetailView.setTotalValue(0.0);
                qty = 1;
            }
        });

        itemCodeEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    itemQtyEditText.requestFocus();
                }
                return false;
            }
        });

        itemNameEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    itemCodeEditText.requestFocus();
                }
                return false;
            }
        });

        qtyPlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (itemQtyEditText != null && itemQtyEditText.getText().toString().trim().length() > 0) {

                    qty = Integer.parseInt(itemQtyEditText.getText().toString());

                    damageItemInDetailView.setStockQty(qty);

                }

                if (qty >= 0) {

                    qty++;

                    itemQtyEditText.setText(Integer.toString(qty));

                    damageItemInDetailView.setStockQty(qty);

                    itemQtyEditText.setSelection(itemQtyEditText.getText().length());

                }

            }
        });

        qtyMinusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (itemQtyEditText != null) {

                    qty = Integer.parseInt(itemQtyEditText.getText().toString());

                    damageItemInDetailView.setStockQty(qty);

                }

                if (qty > 0) {

                    qty--;

                    itemQtyEditText.setText(Integer.toString(qty));

                    damageItemInDetailView.setStockQty(qty);

                    itemQtyEditText.setSelection(itemQtyEditText.getText().length());

                }

            }
        });


        itemQtyEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0)
                    damageItemInDetailView.setStockQty(Integer.parseInt(s.toString()));
            }
        });

        rvAdapterforStockListMaterialDialog.setmItemClickListener(new RVAdapterforStockListMaterialDialog.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                itemCodeEditText.setText(stockItemList.get(position).getCodeNo());

                itemNameEditText.setText(stockItemList.get(position).getName());

                itemQtyEditText.setText("1");

                damageItemInDetailView.setStockId(stockItemList.get(position).getId());

                damageItemInDetailView.setStockCode(stockItemList.get(position).getCodeNo());

                damageItemInDetailView.setUnitId(stockItemList.get(position).getUnitID());

                damageItemInDetailView.setStockName(stockItemList.get(position).getName());

                // damageItemInDetailView.setPurchasePrice(stockItemList.get(position).getPurchasePrice());

                qty = 1;

                damageItemInDetailView.setStockQty(1);

                damageItemInDetailView.setTotalValue(0.0);

                stockListMaterialDialog.dismiss();

                itemNameEditText.clearFocus();

                itemQtyEditText.requestFocus();

            }
        });

        stockListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stockListMaterialDialog.show();

            }
        });

        rvSwipeAdapterForDamageItemList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                isEdit = true;

                editPosition = postion;

                setDataAddDamagedItemDialog(damageItemList.get(postion));

                day = Integer.parseInt(damageItemList.get(postion).getDay());

                month = Integer.parseInt(damageItemList.get(postion).getMonth());

                damageYear = Integer.parseInt(damageItemList.get(postion).getYear());

                buildEditDatePickerDialog();

                addItemMaterialDialog.setTitle(editDamagedItem.getString(0));
                addItemMaterialDialog.show();

            }
        });
        cancelAddItemDamageDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItemMaterialDialog.dismiss();
            }
        });

        rvSwipeAdapterForDamageItemList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                editPosition = postion;

                deleteAlertDialog.show();

            }
        });

        addItemMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                qty = 0;
                isEdit = false;

            }
        });

        buildDatePickerDialog();

        dateLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEdit) {
                    Log.w("edit", "show");
                    editDatePickerDialog.show(getActivity().getFragmentManager(), "show");
                } else {
                    datePickerDialog.show(getActivity().getFragmentManager(), "show");
                }


            }
        });

        rvAdapterForFilter.setCurrentPos(3);

        stockSearchAdapterForDamage = new StockSearchAdapterForDamage(context);

        //damageSearchAdapter.setFilter(nameFilter);

        searchView.setAdapter(stockSearchAdapterForDamage);

        recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                Log.w("Load more", "load more");


                if (shouldLoad) {

                    rvSwipeAdapterForDamageItemList.setShowLoader(true);
                    //                rvSwipeAdapterForSaleTransactions.notifyDataSetChanged();
                    loadmore();
                }


            }
        });

    }

    private void searchViewListeners() {
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                shouldLoad = true;
                //isSearch=true;
                // searchText=query;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Log.e("on", "item clikck");
                // shouldLoad=false;


                //damageItemList = new ArrayList<>();

                stockID = stockSearchAdapterForDamage.getSuggestion().get(position).getId();

                damageItemList = damageManager.getDamagesByDateRangeOnSearch(startDate, endDate, 0, 10, stockID);

                Log.e("id s", stockID + " d");

                isSearch = true;
                //rvAdapterForFilter.setCurrentPos(0);
                //loadMore();

                //refreshRecyclerView();

                searchView.closeSearch();

                Intent detailIntent = new Intent(context, DetailActivity.class);

                Bundle bundle = new Bundle();

                Log.e("search_list_pos", position + " " + id);
                bundle.putSerializable(DamagedDetailNewFragment.KEY, damageItemList.get(position));

                detailIntent.putExtras(bundle);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.DAMAGED_DETAIL);

                startActivity(detailIntent);
                //filterTextView.setText("-");

                //searchedResultTxt.setVisibility(View.VISIBLE);
            }
        });
    }

    public void loadmore() {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("is sea", isSearch + "oo0");

                if (stockID != 0l) {
                    Log.e("is search", "true");
                    damageItemList.addAll(loadMore(damageItemList.size(), 9, stockID));
                } else {
                    damageItemList.addAll(loadMore(damageItemList.size(), 9));
                }

                //damageItemList.addAll(loadMore(damageItemList.size(),9));

                rvSwipeAdapterForDamageItemList.setShowLoader(false);

                refreshList();
            }
        }, 500);
    }


    private void refreshDamageList() {
        refreshList();
    }


    public void buildDatePickerDialog() {

        datePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        day = dayOfMonth;

                        month = monthOfYear + 1;

                        damageYear = year;

                        configDateUI();

                    }
                },

                now.get(Calendar.YEAR),

                now.get(Calendar.MONTH),

                now.get(Calendar.DAY_OF_MONTH)
        );
        configDateUI();


        if (darkmode)
            datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        datePickerDialog.setThemeDark(darkmode);


    }

    public void buildEditDatePickerDialog() {

        editDatePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        day = dayOfMonth;

                        month = monthOfYear + 1;

                        damageYear = year;

                        configDateUI();

                    }
                },

                damageYear,

                month - 1,

                day
        );
        configDateUI();


        if (darkmode)
            editDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        editDatePickerDialog.setThemeDark(darkmode);


    }

    private void configDateUI() {

        date = DateUtility.makeDate(Integer.toString(damageYear), Integer.toString(month), Integer.toString(day));

        //String dayDes[]= DateUtility.dayDes(date);

        // String yearMonthDes= DateUtility.monthYearDes(date);

        dateTextView.setText(DateUtility.makeDateFormatWithSlash(date));

        // dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

    }

    public void setDataAddDamagedItemDialog(DamagedItem dataAddDamagedItemDialog) {

        damageItemInDetailView = dataAddDamagedItemDialog;

        itemCodeEditText.setText(damageItemInDetailView.getStockCode());

        itemNameEditText.setText(damageItemInDetailView.getStockName());

        itemQtyEditText.setText(Integer.toString(damageItemInDetailView.getStockQty()));

        dateTextView.setText(DateUtility.makeDateFormatWithSlash(damageItemInDetailView.getDate()));

    }

    private boolean checkVaildation() {

        boolean status = true;


        TypedArray pleaseEnterItemCode = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_itemcode_or_choose});
        TypedArray pleaseEnterItemName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_name_or_choose});
        TypedArray pleaseEnterItemQty  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_qty_greater_than_0});


        if (itemCodeEditText.getText().toString().trim().length() < 1) {
            status = false;
            itemCodeEditText.setError(pleaseEnterItemCode.getString(0));
        }

        if (itemNameEditText.getText().toString().trim().length() < 1) {
            status = false;
            itemNameEditText.setError(pleaseEnterItemName.getString(0));

        }

        if (itemQtyEditText.getText().toString().trim().length() < 1 || Integer.parseInt(itemQtyEditText.getText().toString().trim()) < 1) {
            status = false;
            itemQtyEditText.setError(pleaseEnterItemQty.getString(0));

        }


        damageItemInDetailView.setRemark(remarkEditText.getText().toString().trim());

        Log.w("damage item reamkar", damageItemInDetailView.getRemark() + " SS");

        return status;

    }


    private void deleteAlertDialog() {

        //TypedArray no=context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        //TypedArray yes=context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        TextView textView = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(areUSureWantToDelete.getString(0));

        yesDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });


    }

    public void loadStockListDialog() {

        rvAdapterforStockListMaterialDialog = new RVAdapterforStockListMaterialDialog(stockItemList);

        stockListMaterialDialog = new MaterialDialog.Builder(context).


                adapter(rvAdapterforStockListMaterialDialog, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

    }

    private void clearAddDialog() {

        itemCodeEditText.setText(null);

        itemNameEditText.setText(null);

        itemQtyEditText.setText(null);


    }

    private void clearErrorAddDialog() {

        itemCodeEditText.setError(null);

        itemNameEditText.setError(null);

        itemQtyEditText.setError(null);

    }

    private void buildDateFilterDialog() {
        TypedArray filterByDate = mainLayoutView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date});

        filterDialog = new MaterialDialog.Builder(context).

                title(filterByDate.getString(0))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")

                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))

                .build();
    }

    public void loadAddItemDialog() {

        //TypedArray save=mainLayoutView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel=mainLayoutView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        addItemMaterialDialog = new MaterialDialog.Builder(context).

                customView(R.layout.add_damage_stock_dialog, true).title(addDamagedItem.getString(0)).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").

                //   positiveText(save.getString(0)).

                //  negativeText(cancel.getString(0)).

                        build();

    }

    private void setFilterTextView(String msg) {

        filterTextView.setText(msg);

    }


    public void configRecyclerView() {
        //     damageItemList=damageManager.getAllDamages(0,100);

        rvSwipeAdapterForDamageItemList = new RVSwipeAdapterForDamageItemList(damageItemList);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(rvSwipeAdapterForDamageItemList);

    }

    public void refreshDamagedList(int startLimit, int endLimit) {

        if (stockID == 0l) {
            damageItemList = damageManager.getDamageListByDateRange(startDate, endDate, startLimit, endLimit);
            Log.e("size", damageItemList.size() + "");
        } else {
            damageItemList = damageManager.getDamagesByDateRangeOnSearch(startDate, endDate, startLimit, endLimit, stockID);
        }
        refreshList();

    }

    public List<DamagedItem> loadMore(int startLimit, int endLimit) {

        return damageManager.getDamageListByDateRange(startDate, endDate, startLimit, endLimit);


    }

    public List<DamagedItem> loadMore(int startLimit, int endLimit, Long stockID) {
        Log.e(startLimit + "startLimit", endLimit + "");

        return damageManager.getDamagesByDateRangeOnSearch(startDate, endDate, startLimit, endLimit, stockID);


    }

    private void refreshList() {

        if (damageItemList.size() < 1) {

            recyclerView.setVisibility(View.GONE);

            noTransactionTextView.setVisibility(View.VISIBLE);

        } else {

            recyclerView.setVisibility(View.VISIBLE);

            noTransactionTextView.setVisibility(View.GONE);

            rvSwipeAdapterForDamageItemList.setDamageItemViewList(damageItemList);

            rvSwipeAdapterForDamageItemList.notifyDataSetChanged();
        }

    }

    private void loadUIFromToolbar() {
        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        searchView.setCursorDrawable(attributeResourceId);
    }

    public void loadUI() {

        noTransactionTextView = (TextView) mainLayoutView.findViewById(R.id.no_transaction);
        filterTextView = (TextView) mainLayoutView.findViewById(R.id.filter_one);
        searchedResultTxt = (TextView) mainLayoutView.findViewById(R.id.searched_result_txt);
        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.damage_list_rv);
        addNewDamageFab = (FloatingActionButton) mainLayoutView.findViewById(R.id.add_new_damage);
        loadUIFromToolbar();
        loadUIFromAdditemDialog();
    }


    public void loadUIFromAdditemDialog() {

        dateLinearLayout = (LinearLayout) addItemMaterialDialog.findViewById(R.id.date_ll);

        dateTextView = (TextView) addItemMaterialDialog.findViewById(R.id.date);

        itemCodeEditText = (AutoCompleteTextView) addItemMaterialDialog.findViewById(R.id.item_code_in_add_damage_item_dialog);

        itemNameEditText = (AutoCompleteTextView) addItemMaterialDialog.findViewById(R.id.item_name_in_add_damage_item_dialog);

        itemQtyEditText = (EditText) addItemMaterialDialog.findViewById(R.id.item_qty_in_add_damage_item_dialog);

        cancelAddItemDamageDialogButton = (Button) addItemMaterialDialog.findViewById(R.id.cancel);

        addItemDamageDialogButton = (Button) addItemMaterialDialog.findViewById(R.id.save);

        qtyPlusButton = (AppCompatImageButton) addItemMaterialDialog.findViewById(R.id.plus_btn);

        stockListButton = (AppCompatImageButton) addItemMaterialDialog.findViewById(R.id.stock_list_code_in_add_damage_item_dialog);

        qtyMinusButton = (AppCompatImageButton) addItemMaterialDialog.findViewById(R.id.minus_btn);

        remarkEditText = (EditText) addItemMaterialDialog.findViewById(R.id.remark_et);

    }

    public void buildingCustomRangeDialog() {

        TypedArray dateRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range});
        TypedArray cancel    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});
        TypedArray ok        = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ok});

        buildingCustomRangeDatePickerDialog();
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(dateRange.getString(0))
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.negativeText(cancel.getString(0))
                // .positiveText(ok.getString(0))
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });


        if (darkmode)
            startDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        startDatePickerDialog.setThemeDark(darkmode);


    }
}
