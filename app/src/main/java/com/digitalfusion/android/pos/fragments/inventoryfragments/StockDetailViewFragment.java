package com.digitalfusion.android.pos.fragments.inventoryfragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockImagePagerAdapter;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.database.model.StockImage;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;


/**
 * Created by MD003 on 9/22/16.
 */
public class StockDetailViewFragment extends Fragment {


    private View mainLayoutView;

    //For stock form view
    private TextView stockNameTextView;
    private CardView stockNameNoImgCardView;
    private CardView stockNameCardView;
    private TextView stockNameNoImgTextView;
    private TextView stockBarcodeTextView;
    private TextView stockCodeTextView;
    private TextView categoryNameTextView;
    private TextView unitNameTextView;
    private TextView inventoryQtyTextView;
    private TextView reorderLevelTextView;
    private TextView purchasePriceTextView;
    private TextView normalPriceTextView;
    private TextView wholeSalePricePercentTextView;
    private TextView wholeSalePriceTextView;
    private TextView retailPriceTextView;
    private TextView retailPricePercentTextView;
    private LinearLayout titleShowOrhideBtn;
    private TextView descriptionTextView;
    private LinearLayout generalInformationShowOrhideBtn;
    private LinearLayout priceShowOrHideBtn;
    private ImageView titleShowOrhideBtn_arrow;
    private ImageView generalInformationShowOrhideBtn_arrow;
    private ImageView priceShowOrHideBtn_arrow;
    private LinearLayout titleLinearLayout;
    private LinearLayout generalInformationLinearLayout;
    private LinearLayout priceLinearLayout;
    private TextView inProcessQtyTextView;
    private StockItem stockItem;
    private ImageView imageView;

    private boolean generalShow = true;
    private boolean priceShow = true;
    private StockImagePagerAdapter stockImagePagerAdapter;
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private List<StockImage> stockImageList;
    private Context context;
    private StockManager stockManager;
    private LinearLayout imageViewLinearLayout;

    private int inProcessQty;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.stock_item_detail_view, null);

        context = getContext();

        stockItem = (StockItem) getArguments().getSerializable("stock");

        String detail = context.getTheme().obtainStyledAttributes(new int[]{R.attr.detail}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(detail);
        stockImageList = new ArrayList<>();

        stockManager = new StockManager(context);


        stockImageList = stockManager.getImagesByStockID(stockItem.getId());

        inProcessQty = stockManager.getInProcessAtyById(stockItem.getId());

        loadUI();

        configUI();

        MainActivity.setCurrentFragment(this);

        titleShowOrhideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (titleLinearLayout.isShown()) {

                    titleShowOrhideBtn_arrow.setSelected(true);

                    titleLinearLayout.setVisibility(View.GONE);

                } else {

                    titleShowOrhideBtn_arrow.setSelected(false);

                    titleLinearLayout.setVisibility(View.VISIBLE);

                }
            }
        });

        generalInformationShowOrhideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (generalShow) {

                    generalShow = false;
                    generalInformationShowOrhideBtn_arrow.setSelected(true);
                    generalInformationLinearLayout.setVisibility(View.GONE);

                } else {

                    generalShow = true;
                    generalInformationShowOrhideBtn_arrow.setSelected(false);
                    generalInformationLinearLayout.setVisibility(View.VISIBLE);

                }

            }
        });


        priceShowOrHideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (priceShow) {

                    priceShow = false;
                    priceShowOrHideBtn_arrow.setSelected(true);
                    priceLinearLayout.setVisibility(View.GONE);

                } else {

                    priceShow = true;
                    priceShowOrHideBtn_arrow.setSelected(false);
                    priceLinearLayout.setVisibility(View.VISIBLE);

                }

            }
        });

        if (stockItem.getPurchasePrice() != null) {

            if (stockItem.getWholesalePrice() != null) {

                setWholeSalePecent();
            }

            if (stockItem.getRetailPrice() != null) {

                setRetailPricePercent();

            }
        }

        if (stockImageList.size() < 1) {

            stockNameCardView.setVisibility(View.GONE);

            stockNameNoImgCardView.setVisibility(View.VISIBLE);


        } else {

            stockNameNoImgCardView.setVisibility(View.GONE);

            stockNameCardView.setVisibility(View.VISIBLE);

        }

        stockImagePagerAdapter = new StockImagePagerAdapter(getContext(), stockImageList);

        viewPager.setAdapter(stockImagePagerAdapter);

        indicator.setViewPager(viewPager);

        return mainLayoutView;

    }

    private void setWholeSalePecent() {
        Double wholeSalePricePercent = stockItem.getWholesalePrice() - stockItem.getPurchasePrice();
        wholeSalePricePercent = (wholeSalePricePercent / stockItem.getPurchasePrice()) * 100;

        wholeSalePricePercentTextView.setText(POSUtil.PercentFromat(wholeSalePricePercent));
    }

    private void setRetailPricePercent() {

        Double retailPricePercent = stockItem.getRetailPrice() - stockItem.getPurchasePrice();

        retailPricePercent = (retailPricePercent / stockItem.getPurchasePrice()) * 100;

        retailPricePercentTextView.setText(POSUtil.PercentFromat(retailPricePercent));

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.edit) {
            Bundle bundle = new Bundle();

            bundle.putSerializable("stock", stockItem);
            bundle.putString("stockedit", "stockedit");

            //   StockFormFragment editStockFormFragment = new StockFormFragment();
            //   editStockFormFragment.setArguments(bundle);
            //   MainActivity.replaceFragment(editStockFormFragment);

            Intent addCurrencyIntent = new Intent(context, DetailActivity.class);
            addCurrencyIntent.putExtras(bundle);
            addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_STOCK);
            startActivity(addCurrencyIntent);


        }
        //noinspection SimplifiableIfStatement

        return false;
    }


    private void configUI() {
        stockNameTextView.setText(stockItem.getName());
        stockNameNoImgTextView.setText(stockItem.getName());
        stockBarcodeTextView.setText(stockItem.getBarcode());
        stockCodeTextView.setText(stockItem.getCodeNo());

        categoryNameTextView.setText(stockItem.getCategoryName());

        if (!stockItem.isUnitEmpty()) {

            unitNameTextView.setText(stockItem.getUnitName());

        } else {

            unitNameTextView.setText(null);

            unitNameTextView.setHint("-");

        }

        inProcessQtyTextView.setText(Integer.toString(inProcessQty));

        inventoryQtyTextView.setText(Integer.toString(stockItem.getInventoryQty()));

        reorderLevelTextView.setText(Integer.toString(stockItem.getReorderLevel()));

        if (stockItem.getDescription() != null) {

            if (!stockItem.getDescription().equalsIgnoreCase("") & stockItem.getDescription().trim().length() > 0) {

                descriptionTextView.setText(stockItem.getDescription());

            } else {

                descriptionTextView.setText(null);
                String noDes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_description}).getString(0);

                descriptionTextView.setHint(noDes);

            }


        } else {

            descriptionTextView.setText(null);
            String noDes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_description}).getString(0);

            descriptionTextView.setHint(noDes);

        }


        if (stockItem.getPurchasePrice() != null) {
            purchasePriceTextView.setText(POSUtil.NumberFormat(stockItem.getPurchasePrice()));
        } else {
            purchasePriceTextView.setText("0");
        }


        if (stockItem.getNormalPrice() != null) {

            normalPriceTextView.setText(POSUtil.NumberFormat(stockItem.getNormalPrice()));

        } else {

            normalPriceTextView.setText("0");

        }

        if (stockItem.getWholesalePrice() != null) {
            wholeSalePriceTextView.setText(POSUtil.NumberFormat(stockItem.getWholesalePrice()));
        } else {
            wholeSalePriceTextView.setText("0");
        }


        retailPriceTextView.setText(POSUtil.NumberFormat(stockItem.getRetailPrice()));

        titleShowOrhideBtn_arrow.setSelected(true);

        titleLinearLayout.setVisibility(View.GONE);

    }

    private void loadUI() {
        imageViewLinearLayout = (LinearLayout) mainLayoutView.findViewById(R.id.imageView_ll);
        viewPager = (ViewPager) mainLayoutView.findViewById(R.id.stock_img);
        indicator = (CircleIndicator) mainLayoutView.findViewById(R.id.indicator);
        stockNameTextView = (TextView) mainLayoutView.findViewById(R.id.stock_name);
        stockNameNoImgTextView = (TextView) mainLayoutView.findViewById(R.id.stock_name_no_img);
        stockNameCardView = (CardView) mainLayoutView.findViewById(R.id.card_name);
        stockNameNoImgCardView = (CardView) mainLayoutView.findViewById(R.id.card_name_no_img);
        stockBarcodeTextView = mainLayoutView.findViewById(R.id.barcode_no);
        stockCodeTextView = (TextView) mainLayoutView.findViewById(R.id.code_no);
        categoryNameTextView = (TextView) mainLayoutView.findViewById(R.id.category_name);
        unitNameTextView = (TextView) mainLayoutView.findViewById(R.id.unit_name);
        inventoryQtyTextView = (TextView) mainLayoutView.findViewById(R.id.inventory_qty);
        reorderLevelTextView = (TextView) mainLayoutView.findViewById(R.id.reorde_level);
        purchasePriceTextView = (TextView) mainLayoutView.findViewById(R.id.purchase_price);
        normalPriceTextView = (TextView) mainLayoutView.findViewById(R.id.normal_price);
        wholeSalePriceTextView = (TextView) mainLayoutView.findViewById(R.id.whole_sale_price);
        wholeSalePricePercentTextView = (TextView) mainLayoutView.findViewById(R.id.whole_sale_price_percent);
        imageView = (ImageView) mainLayoutView.findViewById(R.id.img);
        retailPriceTextView = (TextView) mainLayoutView.findViewById(R.id.retail_price);
        retailPricePercentTextView = (TextView) mainLayoutView.findViewById(R.id.retail_price_percent);
        descriptionTextView = (TextView) mainLayoutView.findViewById(R.id.description);
        titleShowOrhideBtn = (LinearLayout) mainLayoutView.findViewById(R.id.titleView);
        generalInformationShowOrhideBtn = (LinearLayout) mainLayoutView.findViewById(R.id.dd_btn1);
        priceShowOrHideBtn = (LinearLayout) mainLayoutView.findViewById(R.id.dd_btn2);
        titleShowOrhideBtn_arrow = (ImageView) mainLayoutView.findViewById(R.id.title_arrow);
        generalInformationShowOrhideBtn_arrow = (ImageView) mainLayoutView.findViewById(R.id.dd_btn1_arrow);
        priceShowOrHideBtn_arrow = (ImageView) mainLayoutView.findViewById(R.id.dd_btn2_arrow);
        titleLinearLayout = (LinearLayout) mainLayoutView.findViewById(R.id.imageView_ll);
        generalInformationLinearLayout = (LinearLayout) mainLayoutView.findViewById(R.id.general_information_ll);
        priceLinearLayout = (LinearLayout) mainLayoutView.findViewById(R.id.price_ll);
        inProcessQtyTextView = (TextView) mainLayoutView.findViewById(R.id.inventory_qty_in_process);

    }


}