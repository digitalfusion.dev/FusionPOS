package com.digitalfusion.android.pos.fragments.inventoryfragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.ReorderHistorySearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForReorderList;
import com.digitalfusion.android.pos.database.business.CategoryManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.fragments.purchasefragments.AddEditNewPurchaseFragment;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.example.searchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 10/18/16.
 */

public class ReorderListFragment extends Fragment {

    private View mainLayoutView;

    //for stock recycler view
    private RecyclerView recyclerView;

    private RVSwipeAdapterForReorderList rvSwipeAdapterForReorderList;

    private List<StockItem> stockItemList;

    private Context context;

    //FAB add new stock

    private StockManager stockManager;


    private List<String> filterList;

    private boolean isFilter = false;

    private Long choseCategoryId = 0l;

    private MaterialDialog filterDialog;

    private RVAdapterForFilter rvAdapterForFilter;

    private List<Category> categoryList;

    private CategoryManager categoryManager;

    private TextView filterTextView;

    private TextView searchedResultTxt;

    private TextView noTransactionTextView;

    private ReorderHistorySearchAdapter reorderHistorySearchAdapter;

    private MaterialSearchView searchView;

    private boolean isAll = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.reorder_list_view_fragment, null);

        context = getContext();

        setHasOptionsMenu(true);

        stockManager = new StockManager(context);

        categoryManager = new CategoryManager(context);

        categoryList = categoryManager.getAllCategories();

        //((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Reorder List");

        String all = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);

        filterList = new ArrayList<>();

        filterList.add(all);

        for (Category c : categoryList) {

            filterList.add(c.getName());

        }
        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();

        loadIngUI();

        //MainActivity.setCurrentFragment(this);

        initializingStockViewList();

        configRecyclerView();

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                filterTextView.setText(filterList.get(position));

                isFilter = true;
                recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
                    @Override
                    public void onScrollUp() {

                    }

                    @Override
                    public void onScrollDown() {

                    }

                    @Override
                    public void onLoadMore() {

                        rvSwipeAdapterForReorderList.setShowLoader(true);


                        Log.w("loa d more", isFilter + " SS");

                        loadMore(stockItemList.size(), 10);


                    }
                });
                if (position != 0) {

                    isAll = false;

                    choseCategoryId = categoryList.get(position - 1).getId();

                    refreshStockList(0, 13);
                } else {

                    isAll = true;

                    refreshStockList(0, 13);

                }

                filterDialog.dismiss();
            }
        });

        rvSwipeAdapterForReorderList.setReoderClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                Bundle bundle = new Bundle();

                bundle.putSerializable("reorderstock", stockItemList.get(postion));

                Log.w("in reorder list", stockItemList.get(postion).getId() + " sss");

                AddEditNewPurchaseFragment addEditNewPurchaseFragment = new AddEditNewPurchaseFragment();

                addEditNewPurchaseFragment.setArguments(bundle);

                MainActivity.replacingFragment(addEditNewPurchaseFragment);
            }
        });

        reorderHistorySearchAdapter = new ReorderHistorySearchAdapter(context, stockManager);

        searchView.setAdapter(reorderHistorySearchAdapter);

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                stockItemList = new ArrayList<>();

                stockItemList.add(reorderHistorySearchAdapter.getSuggestion().get(position));

                refreshRecyclerView();

                searchView.closeSearch();

                filterTextView.setText("-");

                searchedResultTxt.setVisibility(View.VISIBLE);

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });

        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });

        rvAdapterForFilter.setCurrentPos(0);

        recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                rvSwipeAdapterForReorderList.setShowLoader(true);


                Log.w("loa d more", isFilter + " SS");

                loadMore(stockItemList.size(), 10);


            }
        });
        refreshRecyclerView();

        return mainLayoutView;
    }


    private void buildDateFilterDialog() {
        TypedArray filterByCategory = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_category});
        filterDialog = new MaterialDialog.Builder(context).
                title(filterByCategory.getString(0))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))
                .build();
    }


    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});

        int attributeResourceId = a.getResourceId(0, 0);

        searchView.setCursorDrawable(attributeResourceId);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.no_menu, menu);


        super.onCreateOptionsMenu(menu, inflater);
    }

    public void loadIngUI() {

        noTransactionTextView = (TextView) mainLayoutView.findViewById(R.id.no_transaction);


        filterTextView = (TextView) mainLayoutView.findViewById(R.id.filter_one);
        searchedResultTxt = (TextView) mainLayoutView.findViewById(R.id.searched_result_txt);
        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.stock_list_rv);
        loadUIFromToolbar();
    }

    public void initializingStockViewList() {

        stockItemList = stockManager.getAllReorderStocks(0, 10);

    }


    public void loadMore(int startLimit, int endLimit) {

        if (isAll) {
            stockItemList.addAll(stockManager.getAllReorderStocks(startLimit, endLimit));
        } else {
            stockItemList.addAll(stockManager.getAllReorderStocks(startLimit, endLimit, choseCategoryId));
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                refreshRecyclerView();

                rvSwipeAdapterForReorderList.setShowLoader(false);

            }
        }, 500);
    }


    private void refreshRecyclerView() {

        if (stockItemList.size() > 0) {

            noTransactionTextView.setVisibility(View.GONE);

            recyclerView.setVisibility(View.VISIBLE);

            rvSwipeAdapterForReorderList.setStockItemList(stockItemList);

            rvSwipeAdapterForReorderList.notifyDataSetChanged();

        } else {

            noTransactionTextView.setVisibility(View.VISIBLE);

            recyclerView.setVisibility(View.GONE);

        }

    }

    public void refreshStockList(int startLimit, int endLimit) {

        if (isAll && isFilter) {
            stockItemList = stockManager.getAllReorderStocks(startLimit, endLimit);
        } else if (!isAll && isFilter) {

            //reoder category filter

            stockItemList = stockManager.getAllReorderStocks(startLimit, endLimit, choseCategoryId);

        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshRecyclerView();
                rvSwipeAdapterForReorderList.setShowLoader(false);
            }
        }, 500);
    }

    public void configRecyclerView() {
        rvSwipeAdapterForReorderList = new RVSwipeAdapterForReorderList(stockItemList);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(rvSwipeAdapterForReorderList);
    }


}
