package com.digitalfusion.android.pos.activities;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.SubscriptionHistoryListAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.database.business.ApiManager;
import com.digitalfusion.android.pos.information.LicenceInfo;
import com.digitalfusion.android.pos.information.Subscription;
import com.digitalfusion.android.pos.information.wrapper.SubscriptionsRequest;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.network.ApiRetrofit;
import com.digitalfusion.android.pos.network.NetworkClient;
import com.digitalfusion.android.pos.util.ConnectivityUtil;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriptionHistoryActivity extends ParentActivity {
    String pleaseWait;
    String label = "";
    TextView textView;
    TextView remainDayTextView;
    TextView endDateTextView;
    private Toolbar toolbar;
    private RVAdapterForFilter rvAdapterForDateFilter;
    private TextView dateFilterTextView;
    private MaterialDialog dateFilterDialog;
    private List<String> filterList;
    private RecyclerView recyclerView;
    private SubscriptionHistoryListAdapter adapter;
    private List<Subscription> subscriptionList;
    private String startDate;
    private String endDate;
    private int startLimit;
    private int endLimit;
    private String thisYear;
    private String lastYear;
    private String last12Month;
    private String orderby;
    private boolean isTwelve;
    private String[] dateFilterArr;
    private View view;
    private KProgressHUD hud;
    private int year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(POSUtil.getDefaultThemeNoActionBar(this));
        setContentView(R.layout.activity_subscription_history);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String subscribeStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.lincense}).getString(0);
        getSupportActionBar().setTitle(subscribeStr);

        textView = findViewById(R.id.uid);
        remainDayTextView = findViewById(R.id.day_remain);
        ApiManager  apiManager  = new ApiManager(this);
        LicenceInfo licenceInfo = apiManager.getLicense();
        endDateTextView = findViewById(R.id.end_date_sub);
        textView.setText(apiManager.getRegistration().getUserId());
        year = Calendar.getInstance().get(Calendar.YEAR);
        Calendar c = Calendar.getInstance();
        c.setTime(licenceInfo.getEndDate());
        Log.w("end days ", licenceInfo.getEndDate() + " SS");
        Log.w("Remain days ", DateUtility.dateDifference(c) + " SS");

        remainDayTextView.setText(Integer.toString(DateUtility.dateDifference(c)) + " days");
        endDateTextView.setText(new SimpleDateFormat("dd/MM/yyyy").format(licenceInfo.getEndDate()));

        loadIngUI();
        configFilter();

        if (!ConnectivityUtil.isConnected(this)) {
            findViewById(R.id.no_in).setVisibility(View.VISIBLE);

            findViewById(R.id.no_transaction).setVisibility(View.GONE);

            findViewById(R.id.recycler_view).setVisibility(View.GONE);

        } else {
            findViewById(R.id.no_in).setVisibility(View.GONE);
        }

        findViewById(R.id.try_again).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceedWithApi();
            }
        });

        View      processDialog = View.inflate(this, R.layout.process_dialog_custom_layout, null);
        ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
        imageView.setBackgroundResource(R.drawable.spin_animation);
        AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
        drawable.start();

        //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setDetailsLabel(label)
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);

        buildDateFilterDialog();

        clickListeners();
        setDateFilterTextView(thisYear);

        rvAdapterForDateFilter.setCurrentPos(0);
        findViewById(R.id.copy_paste).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData         clip      = ClipData.newPlainText("USER ID", textView.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(v.getContext(), "User ID Copied", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void configFilter() {
        TypedArray mthisYear = getTheme().obtainStyledAttributes(new int[]{R.attr.this_year});

        TypedArray mlastYear = getTheme().obtainStyledAttributes(new int[]{R.attr.last_year});

        thisYear = mthisYear.getString(0);

        lastYear = mlastYear.getString(0);


        last12Month = getTheme().obtainStyledAttributes(new int[]{R.attr.last_12_month}).getString(0);


        filterList = new ArrayList<>();

        filterList.add(thisYear);

        filterList.add(lastYear);

        //filterList.add(last12Month);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    private void setDateFilterTextView(String msg) {

        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {

        String filter = getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);
        dateFilterDialog = new MaterialDialog.Builder(this).
                title(filter)
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(this))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

    }

    private void clickListeners() {
        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                dateFilterArr = new String[12];
                dateFilterDialog.dismiss();
                if (filterList.get(position).equalsIgnoreCase(thisYear)) {
                    isTwelve = false;
                    orderby = "asc";
                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    year = Calendar.getInstance().get(Calendar.YEAR);
                    for (int i = 0; i < 12; i++) {
                        dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
                    }
                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {
                    isTwelve = false;
                    orderby = "asc";

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    year = Calendar.getInstance().get(Calendar.YEAR) - 1;
                    dateFilterArr = new String[12];
                    for (int i = 0; i < 12; i++) {
                        dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
                    }
                } else if (filterList.get(position).equalsIgnoreCase(last12Month)) {
                    orderby = "asc";
                    isTwelve = true;

                    startDate = DateUtility.getLast12MonthStartDate();

                    endDate = DateUtility.getLast12MotnEndDate();

                    //Log.e("1 ", startDate + "  " + endDate);

                    dateFilterArr = new String[12];
                    int month = Calendar.getInstance().get(Calendar.MONTH) + 1;

                    year = Calendar.getInstance().get(Calendar.YEAR);
                    for (int i = 0; i < 12; i++) {
                        if (month < 1) {
                            year -= 1;
                            month = 12;
                        }
                        dateFilterArr[i] = "(" + (month) + "," + year + ")";
                        month -= 1;
                    }


                }

                Log.e(startDate, endDate);

                hud.show();
                proceedWithApi();

                //new LoadProgressDialog().execute("");

                setDateFilterTextView(filterList.get(position));
            }
        });
    }

    private void loadIngUI() {
        dateFilterTextView = (TextView) findViewById(R.id.date_filter);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        pleaseWait = getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);

        view = dateFilterTextView;
    }

    private void initializeVariables() {
        setDatesAndLimits();

    }

    private void proceedWithApi() {
        final ApiRetrofit    apiRetrofit          = NetworkClient.createService(ApiRetrofit.class, "digital_fusion", "digital_fusion");
        SubscriptionsRequest subscriptionsRequest = new SubscriptionsRequest();
        ApiManager           apiManager           = new ApiManager(this);
        subscriptionsRequest.setUserId(apiManager.getRegistration().getUserId());
        subscriptionsRequest.setYear(year);


        Log.w("HERERE", subscriptionsRequest.toString());

        RequestBody description =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, subscriptionsRequest.getUserId());
        subscriptionList = new ArrayList<>();
        Call<List<Subscription>> call = apiRetrofit.findAllSubscriptions(description);
        Log.e("call", "call");
        call.enqueue(new Callback<List<Subscription>>() {
            @Override
            public void onResponse(Call<List<Subscription>> call, Response<List<Subscription>> response) {
                Log.e("response", response.code() + " code");
                if (response.code() == 200) {
                    subscriptionList = response.body();

                    Log.e("size", subscriptionList.size() + " pcs");
                    Log.e("list", "done");
                    configRecyclerView();

                    if (hud.isShowing()) {
                        hud.dismiss();
                    }
                } else {
                    if (hud.isShowing()) {
                        hud.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Subscription>> call, Throwable t) {
                if (hud.isShowing()) {
                    hud.dismiss();
                }
                t.printStackTrace();
                ConnectivityUtil.handleFailureProcess(t, view);
            }
        });
        if (hud.isShowing()) {
            hud.dismiss();
        }
        // configRecyclerView();
    }

    public void configRecyclerView() {

        ApiManager apiManager = new ApiManager(this);
        apiManager.updateLicense(subscriptionList.get(0).getLicenseInfo().getDuration(), subscriptionList.get(0).getLicenseInfo().getStartDate(), subscriptionList.get(0).getLicenseInfo().getEndDate());
        Calendar c = Calendar.getInstance();
        c.setTime(apiManager.getLicense().getEndDate());
        remainDayTextView.setText(Integer.toString(DateUtility.dateDifference(c)) + " days");
        endDateTextView.setText(new SimpleDateFormat("dd/MM/yyyy").format(apiManager.getLicense().getEndDate()));


        for (Subscription subscription : subscriptionList) {
            if (subscription.getLicenseInfo().getLicenseStatus().equalsIgnoreCase("NEW")
                    || subscription.getPaymentInfo().getAgentName().equalsIgnoreCase("") || subscription.getPaymentInfo().getAgentName() == null || subscription.getLicenseInfo().getLicenceType().equalsIgnoreCase("TRIAL")) {

                subscriptionList.remove(subscription);
            }
        }
        if (subscriptionList.size() > 0) {

            recyclerView.setVisibility(View.VISIBLE);

            findViewById(R.id.no_in).setVisibility(View.GONE);

            findViewById(R.id.no_transaction).setVisibility(View.GONE);

        } else {
            recyclerView.setVisibility(View.GONE);
            findViewById(R.id.no_in).setVisibility(View.GONE);

            findViewById(R.id.no_transaction).setVisibility(View.VISIBLE);

        }
         /*   if(subscriptionList.get(0).getLicenseInfo().getLicenseStatus().equalsIgnoreCase("PENDING")){
               // int day=Integer.parseInt(subscriptionList.get(0).getLicenseInfo().getDuration())-Integer.parseInt(subscriptionList.get(1).getLicenseInfo().getDuration());

                int a=subscriptionList.get(0).getLicenseInfo().getDuration().indexOf(" ");

                String day1String=subscriptionList.get(0).getLicenseInfo().getDuration().substring(0,a);

                int b=subscriptionList.get(1).getLicenseInfo().getDuration().indexOf(" ");
                String day2String=subscriptionList.get(1).getLicenseInfo().getDuration().substring(0,b);
                Log.w("DIFFERENCE",day1String+" sss"+day2String);
                remainDayTextView.setText(day2String+" days");
                endDateTextView.setText(new SimpleDateFormat("dd/MM/yyyy").format(subscriptionList.get(1).getLicenseInfo().getEndDate()));
            //    remainDayTextView.setText(Integer.toString(day)+" days");
//                endDateTextView.setText(new SimpleDateFormat("dd/MM/yyyy").format(subscriptionList.get(subscriptionList.size()-2).getLicenseInfo().getEndDate()));
            }else {
                int a=subscriptionList.get(0).getLicenseInfo().getDuration().indexOf(" ");

                String day1String=subscriptionList.get(0).getLicenseInfo().getDuration().substring(0,a);

                remainDayTextView.setText(subscriptionList.get(0).getLicenseInfo().getDuration());
                endDateTextView.setText(new SimpleDateFormat("dd/MM/yyyy").format(subscriptionList.get(0).getLicenseInfo().getEndDate()));

            }*/


        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SubscriptionHistoryListAdapter(subscriptionList, this);
        recyclerView.setAdapter(adapter);

        adapter.setClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                Log.e("on", "click");
                Intent intent = new Intent(SubscriptionHistoryActivity.this, SubscriptionHistoryDetailActivity.class);
                intent.putExtra("subscription", subscriptionList.get(postion));
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setDatesAndLimits() {
        startDate = DateUtility.getThisMonthStartDate();
        endDate = DateUtility.getThisMonthEndDate();
        startLimit = 0;
        endLimit = 10;
    }
}
