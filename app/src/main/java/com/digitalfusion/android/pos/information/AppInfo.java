package com.digitalfusion.android.pos.information;

import com.digitalfusion.android.pos.BuildConfig;

import java.io.Serializable;

/**
 * Created by MD003 on 8/14/17.
 */

public class AppInfo implements Serializable {

    private final String appName = "FusionPOS";
    private final String version = String.valueOf(BuildConfig.VERSION_NAME);
    ;

    public AppInfo() {
    }

    public String getAppName() {
        return appName;
    }

    public String getVersion() {
        return version;
    }


    @Override
    public String toString() {
        return "AppInfo{" +
                "appName='" + appName + '\'' +
                ", version='" + version + '\'' +
                '}';
    }
}
