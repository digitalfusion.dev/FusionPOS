package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Customer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 9/7/16.
 */
public class CustomerAutoCompleteAdapter extends ArrayAdapter<Customer> {


    List<Customer> suggestion;

    ArrayList<Customer> temExpenseNameInfoObjList;

    List<Customer> expenseNameVOList;

    Customer customer;

    Context context;


    int lenght = 0;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {

            String str = ((Customer) resultValue).getName();

            customer = (Customer) resultValue;

            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {


            if (constraint != null) {

                List<Customer> filterList = new ArrayList<>();

                for (Customer vehicle : temExpenseNameInfoObjList) {


                    if (vehicle.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {

                        filterList.add(vehicle);

                    }

                }
                lenght = constraint.length();

                FilterResults filterResults = new FilterResults();

                filterResults.values = filterList;

                filterResults.count = filterList.size();

                return filterResults;

            } else {

                return new FilterResults();

            }

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            //suggestionList = (ArrayList<Customer>) results.values;

           /* if (results != null && results.count > 0) {

                clear();

                for (Customer vehicle1 : suggestionList) {

                    add(vehicle1);

                    notifyDataSetChanged();

                }
            }*/


            if (results != null && results.count > 0) {

                suggestion = (List<Customer>) results.values;


            } else {
                suggestion = new ArrayList<>();
            }
            notifyDataSetChanged();
        }
    };

    public CustomerAutoCompleteAdapter(Context context, ArrayList<Customer> objects) {

        super(context, -1, objects);


        this.context = context;

        this.expenseNameVOList = objects;

        temExpenseNameInfoObjList = (ArrayList<Customer>) objects.clone();

        suggestion = new ArrayList<>();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.customer_auto_complete_suggest_view, parent, false);

        }


        viewHolder = new ViewHolder(convertView);

        if (suggestion.size() > 0) {


            Spannable spanText = new SpannableString(suggestion.get(position).getName());
            spanText.setSpan(new ForegroundColorSpan(getContext().getResources()
                    .getColor(R.color.accent)), 0, lenght, 0);


            viewHolder.customerName.setText(suggestion.get(position).getName());

        }
        return convertView;

    }

    public List<Customer> getSuggestion() {
        return suggestion;
    }

    public List<Customer> getExpenseNameVOList() {
        return expenseNameVOList;
    }

    public void setExpenseNameVOList(ArrayList<Customer> expenseNameVOList) {

        this.expenseNameVOList = expenseNameVOList;

        temExpenseNameInfoObjList = (ArrayList<Customer>) expenseNameVOList.clone();

        suggestion = new ArrayList<>();

    }

    @Override
    public int getCount() {
        return suggestion.size();
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Nullable
    @Override
    public Customer getItem(int position) {
        return suggestion.get(position);
    }

    static class ViewHolder {

        TextView customerName;

        public ViewHolder(View itemView) {

            this.customerName = (TextView) itemView.findViewById(R.id.customer_name);

        }

    }

}
