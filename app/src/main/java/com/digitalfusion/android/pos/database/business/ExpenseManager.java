package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.database.dao.ExpenseDAO;
import com.digitalfusion.android.pos.database.dao.ExpenseNameDAO;
import com.digitalfusion.android.pos.database.model.ExpenseName;
import com.digitalfusion.android.pos.database.model.ExpenseIncome;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.List;

/**
 * Created by MD002 on 9/6/16.
 */
public class ExpenseManager {
    private Context context;
    private ExpenseNameDAO expenseNameDAO;
    private ExpenseDAO expenseDAO;
    private Long id;
    private InsertedBooleanHolder flag;

    public ExpenseManager(Context context) {
        this.context = context;
        expenseNameDAO = ExpenseNameDAO.getExpenseNameDaoInstance(context);
        expenseDAO = ExpenseDAO.getExpenseDaoInstance(context);
        flag = new InsertedBooleanHolder();
    }

    public boolean addNewIncomeOrExpense(String name, String type, Double amount, String remark, String day, String month, String year, String date) {

        return expenseDAO.addNewExpenseOrIncome(date, amount, remark, day, month, year, name, type);
    }

    public boolean updateExpenseOrIncome(String date, Double amount, String name, String type, String remark, String day, String month, String year, Long expenseID) {

        return expenseDAO.updateExpenseOrIncome(date, amount, remark, day, month, year, expenseID, name, type);
    }

    public List<ExpenseName> getAllExpenseOrIncomeNames() {
        return expenseNameDAO.getAllExpenseOrIncomeNames();
    }

    public List<ExpenseName> getAllExpenseNames() {
        return expenseNameDAO.getAllExpenseNames();
    }

    public List<ExpenseName> getAllIncomeNames() {
        return expenseNameDAO.getAllIncomeNames();
    }

    public List<ExpenseName> incomeNamesSearch(int startLimit, int endLimit, String queryText) {
        return expenseNameDAO.incomeNamesSearch(startLimit, endLimit, queryText);
    }

    public List<ExpenseName> expenseNamesSearch(int startLimit, int endLimit, String queryText) {
        return expenseNameDAO.expenseNamesSearch(startLimit, endLimit, queryText);
    }

    public List<ExpenseIncome> getAllExpenseOrIncomeNameSearch(String name, int startLimit, int endLimit) {
        return expenseDAO.getAllIncomeExpensesSearch(startLimit, endLimit, name);
    }

    public List<ExpenseIncome> getAllExpenseNameSearch(String name, int startLimit, int endLimit) {
        return expenseDAO.getAllExpensesSearch(startLimit, endLimit, name);
    }

    public List<ExpenseIncome> getAllIncomeNameSearch(String name, int startLimit, int endLimit) {
        return expenseDAO.getAllIncomeExpensesSearch(startLimit, endLimit, name);
    }

    public ExpenseIncome getIncomeExpenseByID(Long id) {
        return expenseDAO.getIncomeExpenseByID(id);
    }

    public List<ExpenseIncome> getAllIncomeExpenses(int startLimit, int endLimit, String startDate, String endDate, String type) {
        return expenseDAO.getAllIncomeExpenses(startLimit, endLimit, startDate, endDate, type);
    }

    public List<ExpenseIncome> getAllExpenses(int startLimit, int endLimit, String startDate, String endDate) {
        return expenseDAO.getAllExpenses(startLimit, endLimit, startDate, endDate);
    }

    public List<ExpenseIncome> getAllIncome(int startLimit, int endLimit, String startDate, String endDate) {
        return expenseDAO.getAllIncome(startLimit, endLimit, startDate, endDate);
    }

    public boolean deleteIncomeExpense(Long id) {
        return expenseDAO.deleteIncomeExpense(id);
    }

    public List<ExpenseIncome> getAllExpenseSearch(int startLimit, int endLimit, String queryText) {
        return expenseDAO.getAllExpenseSearch(startLimit, endLimit, queryText);
    }


    public enum ExpenseType {
        Expense, Income
    }
}
