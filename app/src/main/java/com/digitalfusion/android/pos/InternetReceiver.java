package com.digitalfusion.android.pos;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.digitalfusion.android.pos.information.AppUpdateRequest;
import com.digitalfusion.android.pos.network.ApiRetrofit;
import com.digitalfusion.android.pos.network.NetworkClient;
import com.digitalfusion.android.pos.util.ConnectivityUtil;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MD003 on 1/20/18.
 */

public class InternetReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.w("Connection ", "change");

        if (ConnectivityUtil.isConnected(context)) {

            ApiRetrofit apiRetrofit = NetworkClient.createService(ApiRetrofit.class, "digital_fusion", "digital_fusion");

            Log.w("Connection ", "change");
            AppUpdateRequest appUpdateRequest = new AppUpdateRequest(context);

            Call<ResponseBody> call = apiRetrofit.upadateVersion(appUpdateRequest);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Log.w("SUCCESS", response.code() + "  ss");
                    if (response.code() == 200) {
                        Log.w("SUCCESS", "SUCCESS UPDated");
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.w("FAIL FAIL", "FAIL");
                }
            });

        }
    }
}
