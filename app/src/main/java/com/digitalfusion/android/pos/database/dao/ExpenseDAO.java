package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.model.ExpenseIncome;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 9/6/16.
 */
public class ExpenseDAO extends ParentDAO {
    private static ParentDAO expenseDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private List<ExpenseIncome> expenseIncomeList;
    private ExpenseIncome expenseIncome;
    private ExpenseNameDAO expenseNameDAO;

    private ExpenseDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        expenseNameDAO = ExpenseNameDAO.getExpenseNameDaoInstance(context);
    }

    public static ExpenseDAO getExpenseDaoInstance(Context context) {
        if (expenseDaoInstance == null) {
            expenseDaoInstance = new ExpenseDAO(context);
        }
        return (ExpenseDAO) expenseDaoInstance;
    }

    public boolean addNewExpenseOrIncome(String date, Double amount, String remark, String day, String month, String year, String name, String type) {
        genID = idGeneratorDAO.getLastIdValue("Expense");
        genID++;
        databaseWriteTransaction(flag);
        try {
            id = expenseNameDAO.checkNameAndTypeAlreadyExists(name, type);

            if (id == null) {
                id = expenseNameDAO.addNewExpenseOrIncomeName(name, type);
            }


            query = "insert into " + AppConstant.EXPENSE_TABLE_NAME + " (" + AppConstant.EXPENSE_ID + ", " + AppConstant.EXPENSE_DATE + ", " + AppConstant.EXPENSE_AMOUNT +
                    ", " + AppConstant.EXPENSE_EXPENSE_NAME_ID + ", " + AppConstant.EXPENSE_REMARK + ", " + AppConstant.EXPENSE_DAY + ", " + AppConstant.EXPENSE_MONTH + ", " + AppConstant.EXPENSE_YEAR +
                    ", " + AppConstant.CREATED_DATE + ", " + AppConstant.EXPENSE_TIME + ") values(" + genID + ", ?, " + amount + ", " + id + ", ?, ?, ?, ?, ?, strftime('%s', ?, time('now', 'localtime')))";

            database.execSQL(query, new String[]{date, remark, day, month, year, DateUtility.getTodayDate(), formatDateWithDash(day, month, year)});


            id = genID;
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean updateExpenseOrIncome(String date, Double amount, String remark, String day, String month, String year, Long expenseID, String name, String type) {

        databaseWriteTransaction(flag);
        try {
            id = expenseNameDAO.checkNameAndTypeAlreadyExists(name, type);
            if (id == null) {
                id = expenseNameDAO.addNewExpenseOrIncomeName(name, type);
            }
            query = "update " + AppConstant.EXPENSE_TABLE_NAME + " set " + AppConstant.EXPENSE_DATE + " = ?, " + AppConstant.EXPENSE_AMOUNT + " = " + amount + ", " +
                    AppConstant.EXPENSE_EXPENSE_NAME_ID + " = " + id + ", " + AppConstant.EXPENSE_REMARK + " = ?, " + AppConstant.EXPENSE_DAY + " = ?, " + AppConstant.EXPENSE_MONTH +
                    " = ?, " + AppConstant.EXPENSE_YEAR + " = ? where " + AppConstant.EXPENSE_ID + " = " + expenseID;
            database.execSQL(query, new String[]{date, remark, day, month, year});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public List<ExpenseIncome> getAllIncomeExpenses(int startLimit, int endLimit, String startDate, String endDate, String type) {
        expenseIncomeList = new ArrayList<>();
        query = "select e." + AppConstant.EXPENSE_ID + ", e." + AppConstant.EXPENSE_DATE + ", e."
                + AppConstant.EXPENSE_AMOUNT + ", e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + ", e." + AppConstant.EXPENSE_REMARK + ", e." + AppConstant.EXPENSE_DAY + ", e."
                + AppConstant.EXPENSE_MONTH + ", e." + AppConstant.EXPENSE_YEAR + ", n." + AppConstant.EXPENSE_NAME_NAME + ", n." + AppConstant.EXPENSE_NAME_TYPE + ", datetime(time, 'unixepoch')" + " from " +
                AppConstant.EXPENSE_TABLE_NAME + " e , " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_NAME_ID
                + " and e." + AppConstant.EXPENSE_DATE + " >= ? and e." + AppConstant.EXPENSE_DATE + " <= ? and n." + AppConstant.EXPENSE_NAME_TYPE + " <> ? order by " + AppConstant.EXPENSE_TIME + " desc limit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, type});
            if (cursor.moveToFirst()) {
                do {
                    expenseIncome = new ExpenseIncome();
                    expenseIncome.setId(cursor.getLong(0));
                    expenseIncome.setDate(cursor.getString(1));
                    expenseIncome.setAmount(cursor.getDouble(2));
                    expenseIncome.setNameID(cursor.getLong(3));
                    expenseIncome.setRemark(cursor.getString(4));
                    expenseIncome.setDay(cursor.getString(5));
                    expenseIncome.setMonth(cursor.getString(6));
                    expenseIncome.setYear(cursor.getString(7));
                    expenseIncome.setName(cursor.getString(8));

                    if (cursor.getString(9).equalsIgnoreCase(ExpenseIncome.Type.Expense.toString())) {
                        expenseIncome.setType(ExpenseIncome.Type.Expense);
                    } else {
                        expenseIncome.setType(ExpenseIncome.Type.Income);
                    }
                    Log.e("time", cursor.getString(10) + " " + cursor.getLong(0));
                    expenseIncomeList.add(expenseIncome);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return expenseIncomeList;
    }

    public List<ExpenseIncome> getAllExpenses(int startLimit, int endLimit, String startDate, String endDate) {
        expenseIncomeList = new ArrayList<>();
        query = "select e." + AppConstant.EXPENSE_ID + ", e." + AppConstant.EXPENSE_DATE + ", e."
                + AppConstant.EXPENSE_AMOUNT + ", e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + ", e." + AppConstant.EXPENSE_REMARK + ", e." + AppConstant.EXPENSE_DAY + ", e."
                + AppConstant.EXPENSE_MONTH + ", e." + AppConstant.EXPENSE_YEAR + ", n." + AppConstant.EXPENSE_NAME_NAME + ", n." + AppConstant.EXPENSE_NAME_TYPE + " from " +
                AppConstant.EXPENSE_TABLE_NAME + " e, " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where n." + AppConstant.EXPENSE_NAME_TYPE + "=? and e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_NAME_ID
                + " and e." + AppConstant.EXPENSE_DATE + " >= ? and e." + AppConstant.EXPENSE_DATE + " <= ? order by " + AppConstant.EXPENSE_TIME + " desc limit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{ExpenseIncome.Type.Expense.toString(), startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    expenseIncome = new ExpenseIncome();
                    expenseIncome.setId(cursor.getLong(0));
                    expenseIncome.setDate(cursor.getString(1));
                    expenseIncome.setAmount(cursor.getDouble(2));
                    expenseIncome.setNameID(cursor.getLong(3));
                    expenseIncome.setRemark(cursor.getString(4));
                    expenseIncome.setDay(cursor.getString(5));
                    expenseIncome.setMonth(cursor.getString(6));
                    expenseIncome.setYear(cursor.getString(7));
                    expenseIncome.setName(cursor.getString(8));

                    if (cursor.getString(9).equalsIgnoreCase(ExpenseIncome.Type.Expense.toString())) {
                        expenseIncome.setType(ExpenseIncome.Type.Expense);
                    } else {
                        expenseIncome.setType(ExpenseIncome.Type.Income);
                    }


                    expenseIncomeList.add(expenseIncome);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return expenseIncomeList;
    }


    public List<ExpenseIncome> getAllIncome(int startLimit, int endLimit, String startDate, String endDate) {

        expenseIncomeList = new ArrayList<>();

        query = "select e." + AppConstant.EXPENSE_ID + ", e." + AppConstant.EXPENSE_DATE + ", e."
                + AppConstant.EXPENSE_AMOUNT + ", e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + ", e." + AppConstant.EXPENSE_REMARK + ", e." + AppConstant.EXPENSE_DAY + ", e."
                + AppConstant.EXPENSE_MONTH + ", e." + AppConstant.EXPENSE_YEAR + ", n." + AppConstant.EXPENSE_NAME_NAME + ", n." + AppConstant.EXPENSE_NAME_TYPE + " from " +
                AppConstant.EXPENSE_TABLE_NAME + " e, " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where n." + AppConstant.EXPENSE_NAME_TYPE + "=? and e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_NAME_ID
                + " and e." + AppConstant.EXPENSE_DATE + " >= ? and e." + AppConstant.EXPENSE_DATE + " <= ? order by " + AppConstant.EXPENSE_TIME + " desc limit " + startLimit + ", " + endLimit + ";";

        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{ExpenseIncome.Type.Income.toString(), startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    expenseIncome = new ExpenseIncome();
                    expenseIncome.setId(cursor.getLong(0));
                    expenseIncome.setDate(cursor.getString(1));
                    expenseIncome.setAmount(cursor.getDouble(2));
                    expenseIncome.setNameID(cursor.getLong(3));
                    expenseIncome.setRemark(cursor.getString(4));
                    expenseIncome.setDay(cursor.getString(5));
                    expenseIncome.setMonth(cursor.getString(6));
                    expenseIncome.setYear(cursor.getString(7));
                    expenseIncome.setName(cursor.getString(8));

                    if (cursor.getString(9).equalsIgnoreCase(ExpenseIncome.Type.Expense.toString())) {
                        expenseIncome.setType(ExpenseIncome.Type.Expense);
                    } else {
                        expenseIncome.setType(ExpenseIncome.Type.Income);
                    }


                    expenseIncomeList.add(expenseIncome);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return expenseIncomeList;
    }


    public List<ExpenseIncome> getAllIncomeExpensesSearch(int startLimit, int endLimit, String queryText) {
        expenseIncomeList = new ArrayList<>();
        query = "select e." + AppConstant.EXPENSE_ID + ", e." + AppConstant.EXPENSE_DATE + ", e."
                + AppConstant.EXPENSE_AMOUNT + ", e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + ", e." + AppConstant.EXPENSE_REMARK + ", e." + AppConstant.EXPENSE_DAY + ", e."
                + AppConstant.EXPENSE_MONTH + ", e." + AppConstant.EXPENSE_YEAR + ", n." + AppConstant.EXPENSE_NAME_NAME + ", n." + AppConstant.EXPENSE_NAME_TYPE + " from " +
                AppConstant.EXPENSE_TABLE_NAME + " e , " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_NAME_ID
                + " and n." + AppConstant.EXPENSE_NAME_NAME + " like ? ||'%'  order by " + AppConstant.EXPENSE_TIME + " desc limit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{queryText});
            if (cursor.moveToFirst()) {
                do {
                    expenseIncome = new ExpenseIncome();
                    expenseIncome.setId(cursor.getLong(0));
                    expenseIncome.setDate(cursor.getString(1));
                    expenseIncome.setAmount(cursor.getDouble(2));
                    expenseIncome.setNameID(cursor.getLong(3));
                    expenseIncome.setRemark(cursor.getString(4));
                    expenseIncome.setDay(cursor.getString(5));
                    expenseIncome.setMonth(cursor.getString(6));
                    expenseIncome.setYear(cursor.getString(7));
                    expenseIncome.setName(cursor.getString(8));

                    if (cursor.getString(9).equalsIgnoreCase(ExpenseIncome.Type.Expense.toString())) {
                        expenseIncome.setType(ExpenseIncome.Type.Expense);
                    } else {
                        expenseIncome.setType(ExpenseIncome.Type.Income);
                    }


                    expenseIncomeList.add(expenseIncome);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return expenseIncomeList;
    }

    public List<ExpenseIncome> getAllExpensesSearch(int startLimit, int endLimit, String queryText) {
        expenseIncomeList = new ArrayList<>();
        query = "select e." + AppConstant.EXPENSE_ID + ", e." + AppConstant.EXPENSE_DATE + ", e."
                + AppConstant.EXPENSE_AMOUNT + ", e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + ", e." + AppConstant.EXPENSE_REMARK + ", e." + AppConstant.EXPENSE_DAY + ", e."
                + AppConstant.EXPENSE_MONTH + ", e." + AppConstant.EXPENSE_YEAR + ", n." + AppConstant.EXPENSE_NAME_NAME + ", n." + AppConstant.EXPENSE_NAME_TYPE + " from " +
                AppConstant.EXPENSE_TABLE_NAME + " e, " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where  n." + AppConstant.EXPENSE_NAME_TYPE + "=? and e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_NAME_ID
                + " and n." + AppConstant.EXPENSE_NAME_NAME + " like ? ||'%'  order by " + AppConstant.EXPENSE_TIME + " desclimit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{queryText});
            if (cursor.moveToFirst()) {
                do {
                    expenseIncome = new ExpenseIncome();
                    expenseIncome.setId(cursor.getLong(0));
                    expenseIncome.setDate(cursor.getString(1));
                    expenseIncome.setAmount(cursor.getDouble(2));
                    expenseIncome.setNameID(cursor.getLong(3));
                    expenseIncome.setRemark(cursor.getString(4));
                    expenseIncome.setDay(cursor.getString(5));
                    expenseIncome.setMonth(cursor.getString(6));
                    expenseIncome.setYear(cursor.getString(7));
                    expenseIncome.setName(cursor.getString(8));

                    if (cursor.getString(9).equalsIgnoreCase(ExpenseIncome.Type.Expense.toString())) {
                        expenseIncome.setType(ExpenseIncome.Type.Expense);
                    } else {
                        expenseIncome.setType(ExpenseIncome.Type.Income);
                    }

                    expenseIncomeList.add(expenseIncome);
                } while (cursor.moveToNext());
            }


            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }

        Log.w("helo in database", expenseIncomeList.size() + " SS");

        return expenseIncomeList;
    }


    public List<ExpenseIncome> getAllExpenseSearch(int startLimit, int endLimit, String queryText) {
        expenseIncomeList = new ArrayList<>();
        query = "select e." + AppConstant.EXPENSE_ID + ", e." + AppConstant.EXPENSE_DATE + ", e."
                + AppConstant.EXPENSE_AMOUNT + ", e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + ", e." + AppConstant.EXPENSE_REMARK + ", e." + AppConstant.EXPENSE_DAY + ", e."
                + AppConstant.EXPENSE_MONTH + ", e." + AppConstant.EXPENSE_YEAR + ", n." + AppConstant.EXPENSE_NAME_NAME + ", n." + AppConstant.EXPENSE_NAME_TYPE + " from " +
                AppConstant.EXPENSE_TABLE_NAME + " e, " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where e." + AppConstant.EXPENSE_NAME_TYPE + "=? and e. " + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_NAME_ID
                + " and n." + AppConstant.EXPENSE_NAME_NAME + " like ? ||'%' order by " + AppConstant.EXPENSE_TIME + " desc limit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{ExpenseIncome.Type.Income.toString(), queryText});
            if (cursor.moveToFirst()) {
                do {
                    expenseIncome = new ExpenseIncome();
                    expenseIncome.setId(cursor.getLong(0));
                    expenseIncome.setDate(cursor.getString(1));
                    expenseIncome.setAmount(cursor.getDouble(2));
                    expenseIncome.setNameID(cursor.getLong(3));
                    expenseIncome.setRemark(cursor.getString(4));
                    expenseIncome.setDay(cursor.getString(5));
                    expenseIncome.setMonth(cursor.getString(6));
                    expenseIncome.setYear(cursor.getString(7));
                    expenseIncome.setName(cursor.getString(8));

                    if (cursor.getString(9).equalsIgnoreCase(ExpenseIncome.Type.Expense.toString())) {
                        expenseIncome.setType(ExpenseIncome.Type.Expense);
                    } else {
                        expenseIncome.setType(ExpenseIncome.Type.Income);
                    }


                    expenseIncomeList.add(expenseIncome);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return expenseIncomeList;
    }

    public ExpenseIncome getIncomeExpenseByID(Long id) {
        expenseIncome = new ExpenseIncome();
        query = "select e." + AppConstant.EXPENSE_ID + ", e." + AppConstant.EXPENSE_DATE + ", e."
                + AppConstant.EXPENSE_AMOUNT + ", e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + ", e." + AppConstant.EXPENSE_REMARK + ", e." + AppConstant.EXPENSE_DAY + ", e."
                + AppConstant.EXPENSE_MONTH + ", e." + AppConstant.EXPENSE_YEAR + ", n." + AppConstant.EXPENSE_NAME_NAME + ", n." + AppConstant.EXPENSE_NAME_TYPE + " from " +
                AppConstant.EXPENSE_TABLE_NAME + " e, " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where e." + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_NAME_ID
                + " and e." + AppConstant.EXPENSE_ID + "=" + id + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    expenseIncome.setId(cursor.getLong(0));
                    expenseIncome.setDate(cursor.getString(1));
                    expenseIncome.setAmount(cursor.getDouble(2));
                    expenseIncome.setNameID(cursor.getLong(3));
                    expenseIncome.setRemark(cursor.getString(4));
                    expenseIncome.setDay(cursor.getString(5));
                    expenseIncome.setMonth(cursor.getString(6));
                    expenseIncome.setYear(cursor.getString(7));
                    expenseIncome.setName(cursor.getString(8));
                    if (cursor.getString(9).equalsIgnoreCase(ExpenseIncome.Type.Expense.toString())) {
                        expenseIncome.setType(ExpenseIncome.Type.Expense);
                    } else {
                        expenseIncome.setType(ExpenseIncome.Type.Income);
                    }
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return expenseIncome;
    }


    public boolean deleteIncomeExpense(Long id) {
        databaseWriteTransaction();
        try {
            count = database.delete(AppConstant.EXPENSE_TABLE_NAME, AppConstant.EXPENSE_ID + " = ?", new String[]{id.toString()});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return count > 0;
    }


}
