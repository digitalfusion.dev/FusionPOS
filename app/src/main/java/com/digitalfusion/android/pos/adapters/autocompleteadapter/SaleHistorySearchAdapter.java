package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.model.SalesHistory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 9/14/16.
 */
public class SaleHistorySearchAdapter extends ArrayAdapter<SalesHistory> {


    List<SalesHistory> suggestion;

    List<SalesHistory> searchList;

    SalesManager salesManager;

    int lenght = 0;

    String queryText = "";

    SalesHistory stockAutoCompleteView;

    Context context;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {

            String str = ((SalesHistory) resultValue).getVoucherNo();


            stockAutoCompleteView = (SalesHistory) resultValue;

            return str;

        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            if (constraint != null && !constraint.equals("")) {

                searchList.clear();

                queryText = constraint.toString();

                lenght = constraint.length();

                constraint.toString().replace("#", "");
                if (constraint.toString().startsWith("#")) {
                    searchList = salesManager.getSalesOnSearchWithVoucherNoOrCustomer(0, 100, constraint.toString().substring(1, constraint.length()));
                } else {
                    searchList = salesManager.getSalesOnSearchWithVoucherNoOrCustomer(0, 100, constraint.toString());
                }

                FilterResults filterResults = new FilterResults();

                filterResults.values = searchList;

                filterResults.count = searchList.size();

                return filterResults;
            } else {

                return new FilterResults();

            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            List<SalesHistory> filterList = (ArrayList<SalesHistory>) results.values;

            suggestion = searchList;

            if (results != null && results.count > 0) {

                clear();

                for (SalesHistory salesHistory : filterList) {

                    add(salesHistory);

                    notifyDataSetChanged();

                }

            }

        }

    };

    public SaleHistorySearchAdapter(Context context, int textViewResourceId, SalesManager salesManager) {

        super(context, textViewResourceId);

        this.context = context;

        this.salesManager = salesManager;

        suggestion = new ArrayList<>();
        searchList = new ArrayList<>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.sale_history_search_suggest_view, parent, false);

        }

        viewHolder = new ViewHolder(convertView);

        if (suggestion.size() > 0 && !suggestion.isEmpty()) {

            if (suggestion.get(position).getVoucherNo().toLowerCase().startsWith(queryText.toLowerCase())) {

                Spannable spanText = new SpannableString("#" + suggestion.get(position).getVoucherNo());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 1, lenght + 1, 0);

                viewHolder.voucherNoTextView.setText(spanText);

            } else if (queryText.startsWith("#") && suggestion.get(position).getVoucherNo().toLowerCase().contains(queryText.substring(1, queryText.length()).toLowerCase())) {

                Spannable spanText = new SpannableString("#" + suggestion.get(position).getVoucherNo());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.voucherNoTextView.setText(spanText);
            } else {

                viewHolder.voucherNoTextView.setText("#" + suggestion.get(position).getVoucherNo());

            }
            // viewHolder.customerNameTextView.setText(salesHistoryViewList.get(position).getItemName());

            if (suggestion.get(position).getCustomer().toLowerCase().startsWith(queryText.toLowerCase())) {

                Spannable spanText = new SpannableString(suggestion.get(position).getCustomer());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.customerNameTextView.setText(spanText);

            } else {

                viewHolder.customerNameTextView.setText(suggestion.get(position).getCustomer());

            }
        }


        return convertView;

    }

    @Override
    public int getCount() {
        return suggestion.size();
    }

    public List<SalesHistory> getSuggestion() {

        return suggestion;

    }

    public void setSuggestion(List<SalesHistory> suggestion) {

        this.suggestion = suggestion;

    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    public SalesHistory getSelectedItem() {
        return stockAutoCompleteView;
    }

    static class ViewHolder {

        TextView voucherNoTextView;

        TextView customerNameTextView;

        public ViewHolder(View itemView) {

            this.customerNameTextView = (TextView) itemView.findViewById(R.id.voucher_no);

            this.voucherNoTextView = (TextView) itemView.findViewById(R.id.customer_name);

        }

    }

}
