package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Unit;

import java.util.List;

/**
 * Created by MD003 on 8/19/16.
 */
public class RVAdapterForUnitMD extends RecyclerView.Adapter<RVAdapterForUnitMD.UnitViewHolder> {

    private List<Unit> unitList;
    private OnItemClickListener mItemClickListener;


    public RVAdapterForUnitMD(List<Unit> unitList) {
        this.unitList = unitList;
    }

    @Override
    public UnitViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item_view_md, parent, false);

        return new UnitViewHolder(v);

    }

    @Override
    public void onBindViewHolder(UnitViewHolder holder, final int position) {


        holder.unitTextView.setText(unitList.get(position).getUnit());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.w("Click ", "aaa");
                if (mItemClickListener != null) {
                    Log.w("click ", "ateab");
                    mItemClickListener.onItemClick(v, position);

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return unitList.size();
    }

    public List<Unit> getUnitList() {
        return unitList;
    }

    public void setUnitList(List<Unit> unitList) {
        this.unitList = unitList;
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class UnitViewHolder extends RecyclerView.ViewHolder {
        TextView unitTextView;

        View view;

        public UnitViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            unitTextView = (TextView) itemView.findViewById(R.id.category_name_tv);


        }

    }
}
