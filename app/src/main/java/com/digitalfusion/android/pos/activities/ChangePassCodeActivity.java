package com.digitalfusion.android.pos.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.ApiManager;
import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.util.AppConstant;
import com.example.passcode.PassNoImage;

public class ChangePassCodeActivity extends AppCompatActivity {

    private PassNoImage passCodeView;
    private TextView textView;
    private boolean isOld;
    private boolean isNew;
    private String oldPass = "";
    private String newPass = "";
    private ApiManager apiManager;
    private TextView passcodeErrorTextView;
    private String passwordAndConfirmErrorText = "";

    private String passcodeErrorText = "";

    private boolean passcodeErrorFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.change_confirm_pass);

        passCodeView = findViewById(R.id.pass_code_view);

        apiManager = new ApiManager(this);

        passcodeErrorText = getTheme().obtainStyledAttributes(new int[]{R.attr.wrong_passcode}).getString(0);
        passcodeErrorTextView = passCodeView.findViewById(R.id.passcode_error_text_view);
        passwordAndConfirmErrorText = getTheme().obtainStyledAttributes(new int[]{R.attr.password_and_confirm_password_are_not_match}).getString(0);

        isOld = true;

        passCodeView.setPassCodeListener(new PassNoImage.PassCodeListener() {
            @Override
            public void onEndOfPassCode(String passcode) {
                if (isOld) {
                    if (apiManager.validatePasscode(apiManager.getOwnerId(), passcode)) {
                        isNew = true;
                        isOld = false;
                        passCodeView.setLable("Enter Your New Passcode");
                        passCodeView.clearInput();
                    } else {

                        passcodeErrorTextView.setText(passcodeErrorText);
                        //                    YoYo.with(Techniques.Tada)
                        //                            .duration(700)
                        //                            .playOn(passcodeView);


                        passCodeView.clearInput();
                        passcodeErrorFlag = true;
                        ;
                    }
                } else if (isNew) {
                    isNew = false;

                    newPass = passcode;
                    passCodeView.setLable("Confirm Your New Passcode");
                    passCodeView.clearInput();
                } else {
                    if (newPass.equals(passcode)) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                apiManager.changePasscode(apiManager.getOwnerId(), newPass);
                                passcodeErrorTextView.setText("Change Successfully");
                                passcodeErrorTextView.setTextColor(Color.parseColor("#FF095E0F"));
                                finish();
                            }
                        }, 300);
                    } else {
                        passcodeErrorFlag = true;
                        passcodeErrorTextView.setText(passwordAndConfirmErrorText);
                        passCodeView.setLable("Confirm Your New Passcode");
                        passCodeView.clearInput();
                    }
                }
            }

            @Override
            public void onStartOfPassCode() {
                if (passcodeErrorFlag) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            passcodeErrorTextView.setText("");
                        }
                    }, 100);
                }
            }
        });

    }
}
