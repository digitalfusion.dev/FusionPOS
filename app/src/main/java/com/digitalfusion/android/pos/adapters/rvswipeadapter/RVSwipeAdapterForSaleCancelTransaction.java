package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 11/4/16.
 */

public class RVSwipeAdapterForSaleCancelTransaction extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    public int currentDate;
    private List<SalesHistory> salesHistoryList;
    private ClickListener putBackClickListener;
    private ClickListener viewDetailClickListener;

    public RVSwipeAdapterForSaleCancelTransaction(List<SalesHistory> salesHistoryList) {

        this.salesHistoryList = salesHistoryList;

        Calendar now = Calendar.getInstance();

        currentDate = Integer.parseInt(DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(now.get(Calendar.DAY_OF_MONTH))));

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.delivery_cancel_item_view, parent, false);

        return new RVSwipeAdapterForSaleCancelTransaction.SaleHistoryViewHolder(v);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof RVSwipeAdapterForSaleCancelTransaction.SaleHistoryViewHolder) {

            final RVSwipeAdapterForSaleCancelTransaction.SaleHistoryViewHolder viewHolder = (RVSwipeAdapterForSaleCancelTransaction.SaleHistoryViewHolder) holder;

            POSUtil.makeZebraStrip(viewHolder.itemView, position);

            viewHolder.invoiceNoTextView.setText("#" + salesHistoryList.get(position).getVoucherNo());

            viewHolder.customerTextView.setText(salesHistoryList.get(position).getCustomer());

            //  String dayDes[]= DateUtility.dayDes(salesHistoryList.get(position).getDate());
            // String yearMonthDes=DateUtility.monthYearDes(salesHistoryList.get(position).getDate());

            viewHolder.viewDetailsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (viewDetailClickListener != null) {

                        viewDetailClickListener.onClick(holder.getLayoutPosition());

                    }

                }
            });


            int date = Integer.parseInt(salesHistoryList.get(position).getDate());

            if (date == currentDate) {

                viewHolder.dueDateTextView.setText(viewHolder.today.getString(0));

            } else if (date == currentDate - 1) {

                viewHolder.dueDateTextView.setText(viewHolder.yesterday.getString(0));

            } else {
                //  viewHolder.dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));
                viewHolder.dueDateTextView.setText(DateUtility.makeDateFormatWithSlash(salesHistoryList.get(position).getYear(),

                        salesHistoryList.get(position).getMonth(),

                        salesHistoryList.get(position).getDay()));

            }


            mItemManger.bindView(viewHolder.view, position);

        }


    }

    @Override
    public int getItemCount() {
        return salesHistoryList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public ClickListener getViewDetailClickListener() {
        return viewDetailClickListener;
    }

    public void setViewDetailClickListener(ClickListener viewDetailClickListener) {

        this.viewDetailClickListener = viewDetailClickListener;

    }

    public List<SalesHistory> getSalesHistoryList() {
        return salesHistoryList;
    }

    public void setSalesHistoryList(List<SalesHistory> salesHistoryList) {

        this.salesHistoryList = salesHistoryList;

    }

    public ClickListener getPutBackClickListener() {
        return putBackClickListener;
    }

    public void setPutBackClickListener(ClickListener putBackClickListener) {

        this.putBackClickListener = putBackClickListener;

    }

    public class SaleHistoryViewHolder extends RecyclerView.ViewHolder {

        TypedArray today;

        TypedArray yesterday;

        TextView invoiceNoTextView;

        TextView dueDateTextView;


        TextView customerTextView;

        ImageButton viewDetailsBtn;

        View view;

        public SaleHistoryViewHolder(View itemView) {

            super(itemView);

            this.view = itemView;

            today = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.today});

            yesterday = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday});

            viewDetailsBtn = (ImageButton) itemView.findViewById(R.id.view_detail);

            customerTextView = (TextView) itemView.findViewById(R.id.customer_in_sale_transaction_view);

            viewDetailsBtn = (ImageButton) itemView.findViewById(R.id.view_detail);

            invoiceNoTextView = (TextView) itemView.findViewById(R.id.voucher_no);

            dueDateTextView = (TextView) itemView.findViewById(R.id.due_date);


            customerTextView = (TextView) itemView.findViewById(R.id.customer_name);

        }

    }
}