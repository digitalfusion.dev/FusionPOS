package com.digitalfusion.android.pos.fragments.inventoryfragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.LostSearchAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockNameOrCodeAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockSearchAdapterForLost;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterforStockListMaterialDialog;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForLostItemList;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.AuthorizationManager;
import com.digitalfusion.android.pos.database.business.LostManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.business.UserManager;
import com.digitalfusion.android.pos.database.model.LostItem;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.example.searchview.MaterialSearchView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 11/15/16.
 */

public class LostListFragmentWithAddDialog extends Fragment {

    TypedArray addLostItem;
    TypedArray editLostItem;
    TypedArray areUSureWantToDelete;
    private View mainLayoutView;
    private FloatingActionButton addNewLostFab;
    //For lost list
    private RecyclerView recyclerView;
    private LostManager lostManager;
    private Context context;
    private MaterialSearchView searchView;
    private UserManager userManager;
    private List<String> filterList;
    private TextView filterTextView;
    private RVAdapterForFilter rvAdapterForFilter;
    private DatePickerDialog customeDatePickerDialog;
    private MaterialDialog filterDialog;
    private String startDate;
    private String endDate;

    //View from add Item Dialog
    private Calendar calendar;
    private MaterialDialog addItemMaterialDialog;
    private TextView noTransactionTextView;
    private AutoCompleteTextView itemCodeEditText;
    private AutoCompleteTextView itemNameEditText;
    private EditText itemQtyEditText;
    private Button cancelAddItemLostDialogButton;
    private Button addItemLostDialogButton;
    private AppCompatImageButton qtyPlusButton;
    private AppCompatImageButton qtyMinusButton;
    private AppCompatImageButton stockListButton;
    private EditText remarkEditText;
    private RVSwipeAdapterForLostItemList rvSwipeAdapterForLostItemList;
    private List<LostItem> lostItemList;
    private StockNameOrCodeAutoCompleteAdapter stockNameAutoCompleteAdapter;
    private StockNameOrCodeAutoCompleteAdapter stockCodeAutoCompleteAdapter;
    private LostItem lostItemInDetailView;
    private int qty;
    private StockManager stockManager;
    private MaterialDialog stockListMaterialDialog;
    private RVAdapterforStockListMaterialDialog rvAdapterforStockListMaterialDialog;
    private List<StockItem> stockItemList;
    private boolean isEdit = false;
    private int editPosition;
    private DatePickerDialog datePickerDialog;
    private int day;
    private int month;
    private int lostYear;
    private String date;
    private Calendar now;
    private LinearLayout dateLinearLayout;
    private TextView dateTextView;
    private boolean isSearch = false;
    private boolean shouldLoad = true;
    private String searchText = "";
    private LostSearchAdapter lostSearchAdapter;
    private DatePickerDialog editDatePickerDialog;
    private DatePickerDialog startDatePickerDialog;
    private MaterialDialog customRangeDialog;
    private TextView startDateTextView, endDateTextView;
    private String customStartDate,
            customEndDate;
    private TextView traceDate;
    private int oldPos = 0;
    private int current = 0;
    private Button customRangeOkBtn, customRangeCancelBtn;
    private MaterialDialog deleteAlertDialog;
    private Button yesDeleteMdButton;
    private TextView searchedResultTxt;
    private String allTrans;
    private String thisWeek;
    private String lastWeek;
    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;
    private String customRange;
    private String customDate;
    private Long stockID;
    private StockSearchAdapterForLost stockSearchAdapterForLost;
    private boolean darkmode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.lost_list_fragment, null);

        context = getContext();
        stockID = 0l;
        isSearch = false;

        addLostItem = mainLayoutView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.add_lost_item});
        editLostItem = mainLayoutView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.edit_lost_item});
        areUSureWantToDelete = mainLayoutView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete});

        ///((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Lost Transactions");

        darkmode = POSUtil.isNightMode(context);

        now = Calendar.getInstance();

        day = now.get(Calendar.DAY_OF_MONTH);

        month = now.get(Calendar.MONTH) + 1;

        lostYear = now.get(Calendar.YEAR);

        lostManager = new LostManager(context);

        stockManager = new StockManager(context);

        stockItemList = stockManager.getAllStocks(0, 100);

        setHasOptionsMenu(true);

        //userManager=new UserManager(context);

        calendar = Calendar.getInstance();

        // MainActivity.setCurrentFragment(this);

        lostItemList = new ArrayList<>();

        ///  lostItemList=lostManager.getAllLosts(0,10);

        rvSwipeAdapterForLostItemList = new RVSwipeAdapterForLostItemList(lostItemList);

        loadAddItemDialog();

        stockCodeAutoCompleteAdapter = new StockNameOrCodeAutoCompleteAdapter(context, stockManager);

        stockNameAutoCompleteAdapter = new StockNameOrCodeAutoCompleteAdapter(context, stockManager);

        initializeDateFilter();

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();

        loadUI();

        configRecyclerView();

        loadStockListDialog();

        buildingCustomRangeDialog();

        deleteAlertDialog();

        listeners();

        return mainLayoutView;
    }

    private void listeners() {
        yesDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lostManager.deleteLost(lostItemList.get(editPosition).getId());

                deleteAlertDialog.dismiss();

                lostItemList.remove(editPosition);

                rvSwipeAdapterForLostItemList.setLostItemViewList(lostItemList);

                if (editPosition != 0) {

                    rvSwipeAdapterForLostItemList.notifyItemRemoved(editPosition);

                    rvSwipeAdapterForLostItemList.notifyItemRangeChanged(editPosition, lostItemList.size());
                } else {
                    rvSwipeAdapterForLostItemList.notifyDataSetChanged();
                }
            }
        });

        rvSwipeAdapterForLostItemList.setViewDetailClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Intent detailIntent = new Intent(context, DetailActivity.class);

                Bundle bundle = new Bundle();

                bundle.putSerializable(LostDetailNewFragment.KEY, lostItemList.get(postion));

                detailIntent.putExtras(bundle);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.LOST_DETAIL);

                startActivity(detailIntent);
            }
        });

        recyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {

                Log.w("V X", velocityX + " S");

                Log.w("V Y", velocityY + " S");

                if (velocityY > 100) {
                    addNewLostFab.hide();
                } else if (velocityY < -100) {
                    addNewLostFab.show();
                }

                return false;
            }
        });
    }

    private void initializeDateFilter() {
        allTrans = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);

        thisMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month}).getString(0);

        lastMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month}).getString(0);

        thisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year}).getString(0);

        lastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString(0);

        customRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_range}).getString(0);

        customDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_date}).getString(0);

        thisWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_week}).getString(0);

        lastWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week}).getString(0);

        filterList = new ArrayList<>();

        filterList.add(allTrans);

        filterList.add(thisWeek);

        filterList.add(lastWeek);

        filterList.add(thisMonth);

        filterList.add(lastMonth);

        filterList.add(thisYear);

        filterList.add(lastYear);

        filterList.add(customRange);

        filterList.add(customDate);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(rvSwipeAdapterForLostItemList);

        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                //searchedResultTxt.setVisibility(View.INVISIBLE);

                shouldLoad = true;
                if (!isSearch) {
                    stockID = 0l;
                }
                recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
                    @Override
                    public void onScrollUp() {

                    }

                    @Override
                    public void onScrollDown() {

                    }

                    @Override
                    public void onLoadMore() {

                        Log.w("Load more", "load more");

                        if (shouldLoad) {

                            rvSwipeAdapterForLostItemList.setShowLoader(true);
                            //                rvSwipeAdapterForSaleTransactions.notifyDataSetChanged();
                            loadmore();
                        }
                    }
                });
                if (filterList.get(position).equalsIgnoreCase(allTrans)) {

                    setFilterTextView(filterList.get(position));

                    startDate = "0000000";
                    endDate = "999999999999999";

                    refreshLostList(0, 10);
                } else if (filterList.get(position).equalsIgnoreCase(thisMonth)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisMonthStartDate();

                    endDate = DateUtility.getThisMonthEndDate();

                    refreshLostList(0, 10);
                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    refreshLostList(0, 10);
                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    refreshLostList(0, 10);
                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    refreshLostList(0, 10);
                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {

                    //setFilterTextView(filterList.get(position));

                    customRangeDialog.show();
                } else if (filterList.get(position).equalsIgnoreCase(customDate)) {

                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "customeDate");
                } else if (filterList.get(position).equalsIgnoreCase(thisWeek)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getStartDateOfWeekString();

                    endDate = DateUtility.getEndDateOfWeekString();

                    refreshLostList(0, 10);
                } else if (filterList.get(position).equalsIgnoreCase(lastWeek)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getStartDateOfLastWeekString();

                    endDate = DateUtility.getEndDateOfLastWeekString();

                    refreshLostList(0, 10);
                }

                filterDialog.dismiss();
            }
        });

        // lostSearchAdapter=new LostSearchAdapter(context, nameFilter);
        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDate = customStartDate;

                endDate = customEndDate;

                //String startDayDes[]= DateUtility.dayDes(startDate);

                //String startYearMonthDes= DateUtility.monthYearDes(startDate);

                //String endDayDes[]= DateUtility.dayDes(endDate);

                //  String endYearMonthDes= DateUtility.monthYearDes(endDate);

                //filterTextView.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));
                filterTextView.setText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));

                refreshLostList(0, 10);

                customRangeDialog.dismiss();
            }
        });

        customeDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        String date = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        filterTextView.setText(DateUtility.makeDateFormatWithSlash(date));

                        refreshLostList(0, 10);
                    }
                },
                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)
        );
        if (darkmode)
            customeDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        customeDatePickerDialog.setThemeDark(darkmode);

        addNewLostFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lostItemInDetailView = new LostItem();

                clearAddDialog();

                clearErrorAddDialog();

                addItemMaterialDialog.setTitle(addLostItem.getString(0));
                addItemMaterialDialog.show();
            }
        });

        searchViewListeners();

        addItemLostDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEdit) {

                    if (checkVaildation()) {

                        lostManager.updateLost(lostItemList.get(editPosition).getId(), lostItemInDetailView.getStockID(), lostItemInDetailView.getStockQty(),

                                lostItemInDetailView.getRemark(), 1l, Integer.toString(day), Integer.toString(month), Integer.toString(lostYear));

                        lostItemList.get(editPosition).setDay(String.valueOf(day));

                        lostItemList.get(editPosition).setMonth(String.valueOf(month));

                        lostItemList.get(editPosition).setYear(String.valueOf(lostYear));

                        rvSwipeAdapterForLostItemList.notifyItemChanged(editPosition);

                        addItemMaterialDialog.dismiss();
                    }
                } else {

                    if (checkVaildation()) {
                        AccessLogManager     accessLogManager     = new AccessLogManager(context);
                        AuthorizationManager authorizationManager = new AuthorizationManager(context);
                        Long                 deviceId             = authorizationManager.getDeviceId(Build.SERIAL);

                        lostManager.addNewLost(lostItemInDetailView.getStockID(), lostItemInDetailView.getStockQty(),
                                lostItemInDetailView.getRemark(), Integer.toString(day), Integer.toString(month), Integer.toString(lostYear), accessLogManager.getCurrentlyLoggedInUserId(deviceId));

                        lostItemList.add(lostItemInDetailView);

                        refreshLostList(0, 10);

                        addItemMaterialDialog.dismiss();
                    }
                }
            }
        });

        itemNameEditText.setThreshold(1);

        itemNameEditText.setAdapter(stockNameAutoCompleteAdapter);

        itemNameEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                itemCodeEditText.setText(stockNameAutoCompleteAdapter.getSuggestionList().get(position).getCodeNo());

                itemQtyEditText.setText("1");

                lostItemInDetailView.setStockID(stockNameAutoCompleteAdapter.getSuggestionList().get(position).getId());

                lostItemInDetailView.setStockCodeNo(stockNameAutoCompleteAdapter.getSuggestionList().get(position).getCodeNo());

                lostItemInDetailView.setUnitID(stockNameAutoCompleteAdapter.getSuggestionList().get(position).getUnitID());

                lostItemInDetailView.setStockName(stockNameAutoCompleteAdapter.getSuggestionList().get(position).getName());

                //lostItemInDetailView.set(stockNameAutoCompleteAdapter.getSuggestionList().get(position).getPurchasePrice());

                lostItemInDetailView.setStockQty(1);

                lostItemInDetailView.setTotalValue(0.0);

                qty = 1;
            }
        });

        itemCodeEditText.setAdapter(stockCodeAutoCompleteAdapter);

        itemCodeEditText.setThreshold(1);

        itemCodeEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                itemNameEditText.setText(stockCodeAutoCompleteAdapter.getSuggestionList().get(position).getName());

                itemQtyEditText.setText("1");

                lostItemInDetailView.setStockID(stockCodeAutoCompleteAdapter.getSuggestionList().get(position).getId());

                lostItemInDetailView.setStockCodeNo(stockCodeAutoCompleteAdapter.getSuggestionList().get(position).getCodeNo());

                lostItemInDetailView.setUnitID(stockCodeAutoCompleteAdapter.getSuggestionList().get(position).getUnitID());

                lostItemInDetailView.setStockName(stockCodeAutoCompleteAdapter.getSuggestionList().get(position).getName());

                // lostItemInDetailView.set(stockCodeAutoCompleteAdapter.getSuggestionList().get(position).getPurchasePrice());

                lostItemInDetailView.setStockQty(1);
                lostItemInDetailView.setTotalValue(0.0);
                qty = 1;
            }
        });

        itemCodeEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    itemQtyEditText.requestFocus();
                }
                return false;
            }
        });

        itemNameEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    itemCodeEditText.requestFocus();
                }
                return false;
            }
        });

        qtyPlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (itemQtyEditText != null && itemQtyEditText.getText().toString().trim().length() > 0) {

                    qty = Integer.parseInt(itemQtyEditText.getText().toString());

                    lostItemInDetailView.setStockQty(qty);
                }

                if (qty >= 0) {

                    qty++;

                    itemQtyEditText.setText(Integer.toString(qty));

                    lostItemInDetailView.setStockQty(qty);

                    itemQtyEditText.setSelection(itemQtyEditText.getText().length());
                }
            }
        });

        qtyMinusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (itemQtyEditText != null) {

                    qty = Integer.parseInt(itemQtyEditText.getText().toString());

                    lostItemInDetailView.setStockQty(qty);
                }

                if (qty > 0) {

                    qty--;

                    itemQtyEditText.setText(Integer.toString(qty));

                    lostItemInDetailView.setStockQty(qty);

                    itemQtyEditText.setSelection(itemQtyEditText.getText().length());
                }
            }
        });

        itemQtyEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0)
                    lostItemInDetailView.setStockQty(Integer.parseInt(s.toString()));
            }
        });

        rvAdapterforStockListMaterialDialog.setmItemClickListener(new RVAdapterforStockListMaterialDialog.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                itemCodeEditText.setText(stockItemList.get(position).getCodeNo());

                itemNameEditText.setText(stockItemList.get(position).getName());

                itemQtyEditText.setText("1");

                lostItemInDetailView.setStockID(stockItemList.get(position).getId());

                lostItemInDetailView.setStockCodeNo(stockItemList.get(position).getCodeNo());

                lostItemInDetailView.setUnitID(stockItemList.get(position).getUnitID());

                lostItemInDetailView.setStockName(stockItemList.get(position).getName());

                // lostItemInDetailView.setPurchasePrice(stockItemList.get(position).getPurchasePrice());

                qty = 1;

                lostItemInDetailView.setStockQty(1);

                lostItemInDetailView.setTotalValue(0.0);

                stockListMaterialDialog.dismiss();

                itemNameEditText.clearFocus();

                itemQtyEditText.requestFocus();
            }
        });

        stockListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stockListMaterialDialog.show();
            }
        });

        rvSwipeAdapterForLostItemList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                isEdit = true;

                editPosition = postion;

                setDataAddLostItemDialog(lostItemList.get(postion));

                day = Integer.parseInt(lostItemList.get(postion).getDay());

                month = Integer.parseInt(lostItemList.get(postion).getMonth());

                lostYear = Integer.parseInt(lostItemList.get(postion).getYear());

                buildEditDatePickerDialog();

                addItemMaterialDialog.setTitle(editLostItem.getString(0));
                addItemMaterialDialog.show();
            }
        });
        cancelAddItemLostDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItemMaterialDialog.dismiss();
            }
        });

        rvSwipeAdapterForLostItemList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                editPosition = postion;
                deleteAlertDialog.show();
            }
        });

        addItemMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                qty = 0;
                isEdit = false;
            }
        });

        buildDatePickerDialog();

        dateLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEdit) {
                    Log.w("edit", "show");
                    editDatePickerDialog.show(getActivity().getFragmentManager(), "show");
                } else {
                    datePickerDialog.show(getActivity().getFragmentManager(), "show");
                }
            }
        });

        rvAdapterForFilter.setCurrentPos(3);

        stockSearchAdapterForLost = new StockSearchAdapterForLost(context);

        //lostSearchAdapter.setFilter(nameFilter);

        searchView.setAdapter(stockSearchAdapterForLost);

        recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                Log.w("Load more", "load more");

                if (shouldLoad) {

                    rvSwipeAdapterForLostItemList.setShowLoader(true);
                    //                rvSwipeAdapterForSaleTransactions.notifyDataSetChanged();
                    loadmore();
                }
            }
        });
    }

    private void searchViewListeners() {
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                shouldLoad = true;
                //isSearch=true;
                // searchText=query;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Log.e("on", "item clikck");
                // shouldLoad=false;

                //lostItemList = new ArrayList<>();

                // stockID = stockSearchAdapterForLost .getSuggestionList().get(position).getId();

                // Log.e("id s", stockID + " d");

                isSearch = true;
                //  rvAdapterForFilter.setCurrentPos(0);
                //loadMore();

                //refreshRecyclerView();

                searchView.closeSearch();
                Intent detailIntent = new Intent(context, DetailActivity.class);

                Bundle bundle = new Bundle();

                Log.e("search_list_pos", position + " ");
                bundle.putSerializable(LostDetailNewFragment.KEY, lostItemList.get(position));

                detailIntent.putExtras(bundle);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.LOST_DETAIL);

                startActivity(detailIntent);
                //filterTextView.setText("-");

                // searchedResultTxt.setVisibility(View.VISIBLE);
            }
        });
    }

    public void loadmore() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("is sea", isSearch + "oo0");

                if (stockID != 0l) {
                    Log.e("is search", "true");
                    lostItemList.addAll(loadMore(lostItemList.size(), 9, stockID));
                } else {
                    lostItemList.addAll(loadMore(lostItemList.size(), 9));
                }

                //lostItemList.addAll(loadMore(lostItemList.size(),9));

                rvSwipeAdapterForLostItemList.setShowLoader(false);

                refreshList();
            }
        }, 500);
    }

    private void refreshLostList() {
        refreshList();
    }

    public void buildDatePickerDialog() {

        datePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        day = dayOfMonth;

                        month = monthOfYear + 1;

                        lostYear = year;

                        configDateUI();
                    }
                },

                now.get(Calendar.YEAR),

                now.get(Calendar.MONTH),

                now.get(Calendar.DAY_OF_MONTH)
        );
        configDateUI();

        if (darkmode)
            datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        datePickerDialog.setThemeDark(darkmode);
    }

    public void buildEditDatePickerDialog() {

        editDatePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        day = dayOfMonth;

                        month = monthOfYear + 1;

                        lostYear = year;

                        configDateUI();
                    }
                },

                lostYear,

                month - 1,

                day
        );
        configDateUI();

        if (darkmode)
            editDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        editDatePickerDialog.setThemeDark(darkmode);
    }

    private void configDateUI() {

        date = DateUtility.makeDate(Integer.toString(lostYear), Integer.toString(month), Integer.toString(day));

        //String dayDes[]= DateUtility.dayDes(date);

        // String yearMonthDes= DateUtility.monthYearDes(date);

        dateTextView.setText(DateUtility.makeDateFormatWithSlash(date));

        // dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

    }

    public void setDataAddLostItemDialog(LostItem dataAddLostItemDialog) {

        lostItemInDetailView = dataAddLostItemDialog;

        itemCodeEditText.setText(lostItemInDetailView.getStockCodeNo());

        itemNameEditText.setText(lostItemInDetailView.getStockName());

        itemQtyEditText.setText(Integer.toString(lostItemInDetailView.getStockQty()));

        dateTextView.setText(DateUtility.makeDateFormatWithSlash(lostItemInDetailView.getDate()));
    }

    private boolean checkVaildation() {

        TypedArray pleaseEnterItemCode = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_itemcode_or_choose});
        TypedArray pleaseEnterItemName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_name_or_choose});
        TypedArray pleaseEnterItemQty  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_qty_greater_than_0});

        boolean status = true;

        if (itemCodeEditText.getText().toString().trim().length() < 1) {
            status = false;
            itemCodeEditText.setError(pleaseEnterItemCode.getString(0));
        }

        if (itemNameEditText.getText().toString().trim().length() < 1) {
            status = false;
            itemNameEditText.setError(pleaseEnterItemName.getString(0));
        }

        if (itemQtyEditText.getText().toString().trim().length() < 1 || Integer.parseInt(itemQtyEditText.getText().toString().trim()) < 1) {
            status = false;
            itemQtyEditText.setError(pleaseEnterItemQty.getString(0));
        }

        lostItemInDetailView.setRemark(remarkEditText.getText().toString().trim());

        //Log.w("lost item reamkar",lostItemInDetailView.getRemark()+" SS");

        return status;
    }

    private void deleteAlertDialog() {

        //TypedArray no=context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        //TypedArray yes=context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();

        TextView textView = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(areUSureWantToDelete.getString(0));

        yesDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });
    }

    public void loadStockListDialog() {

        rvAdapterforStockListMaterialDialog = new RVAdapterforStockListMaterialDialog(stockItemList);

        stockListMaterialDialog = new MaterialDialog.Builder(context).

                adapter(rvAdapterforStockListMaterialDialog, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    private void clearAddDialog() {

        itemCodeEditText.setText(null);

        itemNameEditText.setText(null);

        itemQtyEditText.setText(null);
    }

    private void clearErrorAddDialog() {

        itemCodeEditText.setError(null);

        itemNameEditText.setError(null);

        itemQtyEditText.setError(null);
    }

    private void buildDateFilterDialog() {
        TypedArray filterByDate = mainLayoutView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date});

        filterDialog = new MaterialDialog.Builder(context).

                title(filterByDate.getString(0))

                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    public void loadAddItemDialog() {

        //TypedArray save=mainLayoutView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel=mainLayoutView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        addItemMaterialDialog = new MaterialDialog.Builder(context).

                customView(R.layout.add_damage_stock_dialog, true).title(addLostItem.getString(0)).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").

                //   positiveText(save.getString(0)).

                //  negativeText(cancel.getString(0)).

                        build();
    }

    private void setFilterTextView(String msg) {

        filterTextView.setText(msg);
    }

    public void configRecyclerView() {
        //     lostItemList=lostManager.getAllLosts(0,100);

        rvSwipeAdapterForLostItemList = new RVSwipeAdapterForLostItemList(lostItemList);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(rvSwipeAdapterForLostItemList);
    }

    public void refreshLostList(int startLimit, int endLimit) {

        Log.e("id", stockID + "diof");
        if (stockID == 0l) {
            lostItemList = lostManager.getLostListByDateRange(startDate, endDate, startLimit, endLimit);
        } else {
            lostItemList = lostManager.getLostByDateRangeOnSearch(startDate, endDate, startLimit, endLimit, stockID);
        }
        refreshList();
    }

    public List<LostItem> loadMore(int startLimit, int endLimit) {

        return lostManager.getLostListByDateRange(startDate, endDate, startLimit, endLimit);
    }

    public List<LostItem> loadMore(int startLimit, int endLimit, Long stockID) {
        Log.e(startLimit + "startLimit", endLimit + "");

        return lostManager.getLostByDateRangeOnSearch(startDate, endDate, startLimit, endLimit, stockID);
    }

    private void refreshList() {

        if (lostItemList.size() < 1) {

            recyclerView.setVisibility(View.GONE);

            noTransactionTextView.setVisibility(View.VISIBLE);
        } else {

            recyclerView.setVisibility(View.VISIBLE);

            noTransactionTextView.setVisibility(View.GONE);

            rvSwipeAdapterForLostItemList.setLostItemViewList(lostItemList);

            rvSwipeAdapterForLostItemList.notifyDataSetChanged();
        }
    }

    private void loadUIFromToolbar() {
        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        searchView.setCursorDrawable(attributeResourceId);
    }

    public void loadUI() {

        noTransactionTextView = (TextView) mainLayoutView.findViewById(R.id.no_transaction);
        filterTextView = (TextView) mainLayoutView.findViewById(R.id.filter_one);
        searchedResultTxt = (TextView) mainLayoutView.findViewById(R.id.searched_result_txt);
        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.lost_list_rv);
        addNewLostFab = (FloatingActionButton) mainLayoutView.findViewById(R.id.add_new_lost);
        loadUIFromToolbar();
        loadUIFromAdditemDialog();
    }

    public void loadUIFromAdditemDialog() {

        dateLinearLayout = (LinearLayout) addItemMaterialDialog.findViewById(R.id.date_ll);

        dateTextView = (TextView) addItemMaterialDialog.findViewById(R.id.date);

        itemCodeEditText = (AutoCompleteTextView) addItemMaterialDialog.findViewById(R.id.item_code_in_add_damage_item_dialog);

        itemNameEditText = (AutoCompleteTextView) addItemMaterialDialog.findViewById(R.id.item_name_in_add_damage_item_dialog);

        itemQtyEditText = (EditText) addItemMaterialDialog.findViewById(R.id.item_qty_in_add_damage_item_dialog);

        cancelAddItemLostDialogButton = (Button) addItemMaterialDialog.findViewById(R.id.cancel);

        addItemLostDialogButton = (Button) addItemMaterialDialog.findViewById(R.id.save);

        qtyPlusButton = (AppCompatImageButton) addItemMaterialDialog.findViewById(R.id.plus_btn);

        stockListButton = (AppCompatImageButton) addItemMaterialDialog.findViewById(R.id.stock_list_code_in_add_damage_item_dialog);

        qtyMinusButton = (AppCompatImageButton) addItemMaterialDialog.findViewById(R.id.minus_btn);

        remarkEditText = (EditText) addItemMaterialDialog.findViewById(R.id.remark_et);
    }

    public void buildingCustomRangeDialog() {

        TypedArray dateRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range});
        //TypedArray cancel=context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});
        //TypedArray ok=context.getTheme().obtainStyledAttributes(new int[]{R.attr.ok});

        buildingCustomRangeDatePickerDialog();
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(dateRange.getString(0))
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.negativeText(cancel.getString(0))
                // .positiveText(ok.getString(0))
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");
            }
        });

        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");
            }
        });

        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });
    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));

        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });

        if (darkmode)
            startDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        startDatePickerDialog.setThemeDark(darkmode);
    }
}
