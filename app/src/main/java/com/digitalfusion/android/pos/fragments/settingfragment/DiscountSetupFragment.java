package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForDiscountList;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.Discount;
import com.digitalfusion.android.pos.views.FusionToast;

import java.util.List;

/**
 * Created by MD002 on 6/20/17.
 */
public class DiscountSetupFragment extends Fragment {
    private View mainLayoutView;

    private RecyclerView recyclerView;

    private FloatingActionButton addDiscountFab;

    private SettingManager settingManager;

    //Add tax dialog
    //private MaterialDialog taxMaterialDialog;

    //private MDButton addDiscountMdButton;

    /*private EditText discountNameTextInputEditText;

    private EditText discountAmtTextInputEditText;

    private EditText discountDescTextInputEditText;

    private RadioGroup discountPercentRadioGroup;

    private RadioButton amountRadioButton;

    private RadioButton percentRadioButton;

    private CheckBox useDefaultCheckBox;*/


    //Edit tax dialog
    //private MaterialDialog editDiscountMaterialDialog;

    /*private MDButton editAddDiscountMdButton;

    private EditText editDiscountNameTextInputEditText;

    private EditText editDiscountAmtTextInputEditText;

    private EditText editDiscountDescTextInputEditText;

    private RadioGroup editDiscountTypeRadioGroup;

    private RadioButton editInclusive;

    private CheckBox editUseDefaultCheckBox;*/

    //For Discount list
    private RVSwipeAdapterForDiscountList rvSwipeAdapterForDiscountList;

    private List<Discount> discountList;

    private boolean isInclusive = true;

    private boolean isDefault = false;

    //For value
    private String discountName;

    private Double discountAmt;

    private String discountDesc;

    private String disountType;

    private Integer useDefault;

    //For edit value
    private int editPosition;

    private String editDiscountName;

    private Double editDiscountAmt;

    private String editDiscountDesc;

    private String editDiscountType;

    private Integer editUseDefault;

    private boolean editIsInclusive = true;

    private boolean editIsDefault = false;

    private Context context;

    private MaterialDialog deleteAlertDialog;

    private Button yesDeleteMdButton;

    private int deletePos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.fragment_discount_setup, null);

        context = getContext();

        settingManager = new SettingManager(context);

        String discount = context.getTheme().obtainStyledAttributes(new int[]{R.attr.discount}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(discount);

        // buildingEditDiscountDialog();

        //buildingAddDiscountDialog();

        loadUI();

        buildDeleteAlertDialog();

        configUI();
        MainActivity.setCurrentFragment(this);
        addDiscountFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // clearInput();

                // taxMaterialDialog.show();


                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_DISCOUNT);

                startActivity(addCurrencyIntent);

            }
        });

        /*discountPercentRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                isInclusive = amountRadioButton.getId() == checkedId;

            }
        });

        editDiscountTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                editIsInclusive = editInclusive.getId() == checkedId;

            }
        });

        editUseDefaultCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                editIsDefault = isChecked;

            }
        });


        useDefaultCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                isDefault = isChecked;

            }
        });*/

        /*addDiscountMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // settingManager.addNewDiscount();

                if (checkValidation()) {

                    takeValueFromView();

                    settingManager.addNewDiscount(discountName, discountAmt, disountType, discountDesc, useDefault);

                    taxMaterialDialog.dismiss();

                    refreshDiscountList();

                }

            }
        });*/

        rvSwipeAdapterForDiscountList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

            /*    editPosition=postion;

                editDiscountNameTextInputEditText.setText(discountList.get(postion).getName());

                editDiscountAmtTextInputEditText.setText(discountList.get(postion).getRate().toString());

                editDiscountDescTextInputEditText.setText(discountList.get(postion).getDescription());

                if(discountList.get(postion).getFeedbackType().equalsIgnoreCase("Inclusive")){

                    editDiscountTypeRadioGroup.check(editInclusive.getId());

                }else {

                    editDiscountTypeRadioGroup.check(editExclusive.getId());

                }
                if(discountList.get(postion).getIsDefault()>0){

                    editUseDefaultCheckBox.setChecked(true);

                }else {

                    editUseDefaultCheckBox.setChecked(false);

                }

                editDiscountMaterialDialog.show();*/

                Bundle bundle = new Bundle();

                bundle.putSerializable(AddEdiDiscountFragment.KEY, discountList.get(postion));

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtras(bundle);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_DISCOUNT);

                startActivity(addCurrencyIntent);

            }
        });

        /*editAddDiscountMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkEditValidation()) {

                    takeValueFromEditView();

                    discountList.get(editPosition).setIsDefault(editUseDefault);

                    discountList.get(editPosition).setName(editDiscountName);

                    discountList.get(editPosition).setIsPercent(editDiscountType);

                    discountList.get(editPosition).setDescription(editDiscountDesc);

                    discountList.get(editPosition).setAmount(editDiscountAmt);

                    settingManager.updateDiscount(editDiscountName, editDiscountAmt, editDiscountType, editDiscountDesc, editUseDefault, discountList.get(editPosition).getId());

                    rvSwipeAdapterForDiscountList.notifyItemChanged(editPosition);

                    //editDiscountMaterialDialog.dismiss();

                }

            }
        });*/


        rvSwipeAdapterForDiscountList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                deletePos = postion;
                deleteAlertDialog.show();
            }
        });

        yesDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                settingManager.deleteDiscount(discountList.get(deletePos).getId());
                Log.e("dele", deletePos + "fa");

                rvSwipeAdapterForDiscountList.deletePos(deletePos);

                deleteAlertDialog.dismiss();
                FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
            }
        });


        return mainLayoutView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshDiscountList();
    }

    /*private boolean checkValidation() {

        boolean status = true;

        if (discountNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;
            String taxName= context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_tax_name}).getString(0);

            discountNameTextInputEditText.setError(taxName);

        }

        if (discountAmtTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;
            String taxRate= context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_tax_rate}).getString(0);

            discountAmtTextInputEditText.setError(taxRate);

        }

        return status;

    }*/

    private boolean checkEditValidation() {

        boolean status = true;

        /*if (editDiscountNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;
            String taxName= context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_tax_name}).getString(0);

            editDiscountNameTextInputEditText.setError(taxName);

        }

        if (editDiscountAmtTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;
            String taxRate= context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_tax_rate}).getString(0);

            editDiscountAmtTextInputEditText.setError(taxRate);

        }*/

        return status;

    }

    /*private void takeValueFromView() {
        String inclusive= context.getTheme().obtainStyledAttributes(new int[]{R.attr.inclusive}).getString(0);

        String exclusive= context.getTheme().obtainStyledAttributes(new int[]{R.attr.exclusive}).getString(0);

        discountName = discountNameTextInputEditText.getText().toString().trim();

        discountAmt = Double.parseDouble(discountAmtTextInputEditText.getText().toString().trim());

        discountDesc = discountDescTextInputEditText.getText().toString().trim();

        if (isInclusive) {

            disountType = inclusive;

        } else {

            disountType = exclusive;

        }

        if (isDefault) {

            useDefault = 1;

        } else {

            useDefault = 0;

        }

    }

    private void takeValueFromEditView() {
        String inclusive= context.getTheme().obtainStyledAttributes(new int[]{R.attr.inclusive}).getString(0);

        String exclusive= context.getTheme().obtainStyledAttributes(new int[]{R.attr.exclusive}).getString(0);

        /*editDiscountName = editDiscountNameTextInputEditText.getText().toString().trim();

        editDiscountAmt = Double.parseDouble(editDiscountAmtTextInputEditText.getText().toString().trim());

        editDiscountDesc = editDiscountDescTextInputEditText.getText().toString().trim();

        if (editIsInclusive) {

            editDiscountType = inclusive;

        } else {

            editDiscountType = exclusive;

        }

        if (editIsDefault) {

            editUseDefault = 1;

        } else {

            editUseDefault = 0;

        }

    }

    private void clearInput() {

        discountNameTextInputEditText.setError(null);

        discountNameTextInputEditText.setText(null);

        discountAmtTextInputEditText.setText(null);

        discountAmtTextInputEditText.setError(null);

        discountDescTextInputEditText.setText(null);

        discountPercentRadioGroup.check(amountRadioButton.getId());

    }*/

    public void refreshDiscountList() {

        discountList = settingManager.getAllDiscounts();

        rvSwipeAdapterForDiscountList.setDiscountList(discountList);

        rvSwipeAdapterForDiscountList.notifyDataSetChanged();

    }

    public void configUI() {

        discountList = settingManager.getAllDiscounts();

        rvSwipeAdapterForDiscountList = new RVSwipeAdapterForDiscountList(discountList);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(rvSwipeAdapterForDiscountList);

    }


    /*private void buildingAddDiscountDialog() {

        taxMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.add_tax, true).negativeText("Cancel").positiveText("Save").title("Add Discount").build();

    }

    private void buildingEditDiscountDialog() {

        editDiscountMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.add_tax, true).negativeText("Cancel").positiveText("Save").title("Edit Discount").build();

    }*/


    public void loadUI() {

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.discount_list_rv);

        addDiscountFab = (FloatingActionButton) mainLayoutView.findViewById(R.id.add_new_discount);

        /*discountNameTextInputEditText = (EditText) taxMaterialDialog.findViewById(R.id.tax_name_in_add_tax_et);

        discountAmtTextInputEditText = (EditText) taxMaterialDialog.findViewById(R.id.tax_rate_in_add_tax_et);

        discountDescTextInputEditText = (EditText) taxMaterialDialog.findViewById(R.id.tax_desc_in_add_tax_et);

        addDiscountMdButton = taxMaterialDialog.getActionButton(DialogAction.POSITIVE);

        discountPercentRadioGroup = (RadioGroup) taxMaterialDialog.findViewById(R.id.tax_type_in_add_tax_rg);

        amountRadioButton = (RadioButton) taxMaterialDialog.findViewById(R.id.inclusive);

        exclusive = (RadioButton) taxMaterialDialog.findViewById(R.id.exclusive);

        useDefaultCheckBox = (CheckBox) taxMaterialDialog.findViewById(R.id.use_default_in_add_tax);

        editDiscountNameTextInputEditText = (EditText) editDiscountMaterialDialog.findViewById(R.id.tax_name_in_add_tax_et);

        editDiscountAmtTextInputEditText = (EditText) editDiscountMaterialDialog.findViewById(R.id.tax_rate_in_add_tax_et);

        editDiscountDescTextInputEditText = (EditText) editDiscountMaterialDialog.findViewById(R.id.tax_desc_in_add_tax_et);

        editAddDiscountMdButton = editDiscountMaterialDialog.getActionButton(DialogAction.POSITIVE);

        editDiscountTypeRadioGroup = (RadioGroup) editDiscountMaterialDialog.findViewById(R.id.tax_type_in_add_tax_rg);

        editInclusive = (RadioButton) editDiscountMaterialDialog.findViewById(R.id.inclusive);

        editExclusive = (RadioButton) editDiscountMaterialDialog.findViewById(R.id.exclusive);

        editUseDefaultCheckBox = (CheckBox) editDiscountMaterialDialog.findViewById(R.id.use_default_in_add_tax);*/

    }

    private void buildDeleteAlertDialog() {

        //TypedArray no = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        //TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        String   sureToDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete}).getString(0);
        TextView textView     = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(sureToDelete);

        yesDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });

    }


}
