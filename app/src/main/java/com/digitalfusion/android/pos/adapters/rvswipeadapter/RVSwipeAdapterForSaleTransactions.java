package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.AuthorizationManager;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 8/29/16.
 */
public class RVSwipeAdapterForSaleTransactions extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    public int currentDate;
    protected boolean showLoader = false;
    private List<SalesHistory> salesHistoryList;
    private ClickListener editClickListener;
    private ClickListener viewDetailsClickLister;
    private ClickListener viewPaymentsClickLister;
    private ClickListener mCancelClickLister;
    private ClickListener deleteClickLister;
    private LoaderViewHolder loaderViewHolder;
    private int editPos;

    public RVSwipeAdapterForSaleTransactions(List<SalesHistory> salesHistoryList) {

        Calendar now = Calendar.getInstance();

        currentDate = Integer.parseInt(DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(now.get(Calendar.DAY_OF_MONTH))));

        this.salesHistoryList = salesHistoryList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEWTYPE_LOADER) {

            // Your Loader XML view here
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            // Your LoaderViewHolder class
            return new LoaderViewHolder(view);
        } else {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sale_transaction_item_view, parent, false);

            return new SaleHistoryViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof LoaderViewHolder) {

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) holder;

            this.loaderViewHolder = loaderViewHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);
            return;
        } else if (holder instanceof SaleHistoryViewHolder) {

            POSUtil.makeZebraStrip(((SaleHistoryViewHolder) holder).itemView, position);

            final SaleHistoryViewHolder viewHolder = (SaleHistoryViewHolder) holder;

            viewHolder.saleIdTextView.setText("#" + salesHistoryList.get(position).getVoucherNo());

            if (salesHistoryList.get(position).getCustomer().equalsIgnoreCase(AppConstant.DEFAULT_CUSTOMER)) {

                viewHolder.customerTextView.setText(salesHistoryList.get(position).getCustomer());
            } else {
                viewHolder.customerTextView.setText(salesHistoryList.get(position).getCustomer());
            }

            viewHolder.totalAmountTextView.setText(POSUtil.NumberFormat(salesHistoryList.get(position).getTotalAmount()));



            // String dayDes[]= DateUtility.dayDes(salesHistoryList.get(position).getDate());

            //String yearMonthDes= DateUtility.monthYearDes(salesHistoryList.get(position).getDate());

            if (salesHistoryList.get(position).getType().equalsIgnoreCase(SalesManager.SaleType.Delivery.toString())) {

                viewHolder.deliveryIconView.setVisibility(View.VISIBLE);

                //   viewHolder.deleteButton.setVisibility(View.VISIBLE);

            } else if (salesHistoryList.get(position).getType().equalsIgnoreCase(SalesManager.SaleType.Sales.toString())) {

                viewHolder.deliveryIconView.setVisibility(View.GONE);

                //    viewHolder.deleteButton.setVisibility(View.GONE);

            }

            int date = Integer.parseInt(salesHistoryList.get(position).getDate());

            if (date == currentDate) {
                viewHolder.dateTextView.setText(viewHolder.today);
            } else if (date == currentDate - 1) {
                viewHolder.dateTextView.setText(viewHolder.yesterday);
            } else {
                //  viewHolder.dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

                viewHolder.dateTextView.setText(DateUtility.makeDateFormatWithSlash(salesHistoryList.get(position).getYear(),
                        salesHistoryList.get(position).getMonth(),
                        salesHistoryList.get(position).getDay()));
            }

            viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
                @Override
                public void onStartOpen(SwipeLayout layout) {

                }

                @Override
                public void onOpen(SwipeLayout layout) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            viewHolder.swipeLayout.close();
                        }
                    }, 2000);
                }

                @Override
                public void onStartClose(SwipeLayout layout) {

                }

                @Override
                public void onClose(SwipeLayout layout) {

                }

                @Override
                public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

                }

                @Override
                public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

                }
            });

          /*  viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(mCancelClickLister !=null){

                        mCancelClickLister.onClick(position);

                    }

                }
            });*/

            viewHolder.viewDetailButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewDetailsClickLister != null) {

                        viewDetailsClickLister.onClick(position);
                    }
                }
            });

            viewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    editPos = position;
                    if (editClickListener != null) {

                        editClickListener.onClick(position);
                    }
                }
            });

            viewHolder.viewPayments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewPaymentsClickLister != null) {
                        viewPaymentsClickLister.onClick(position);
                    }
                }
            });

            viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteClickLister != null) {

                        Log.w("delie pos", holder.getLayoutPosition() + " sss");

                        deleteClickLister.onClick(holder.getLayoutPosition());
                    }
                }
            });
            if (salesHistoryList.get(position).isHold()) {
                viewHolder.viewDetailButton.setVisibility(View.GONE);
                viewHolder.viewPayments.setVisibility(View.GONE);
            }
            if (salesHistoryList.get(position).isHold()) {
                viewHolder.editButton.setVisibility(View.VISIBLE);
            }
            mItemManger.bindView(viewHolder.itemView, holder.getLayoutPosition());
        }
    }

    public ClickListener getDeleteClickLister() {
        return deleteClickLister;
    }

    public void setDeleteClickLister(ClickListener deleteClickLister) {
        this.deleteClickLister = deleteClickLister;
    }

    public ClickListener getViewPaymentsClickLister() {
        return viewPaymentsClickLister;
    }

    public void setViewPaymentsClickLister(ClickListener viewPaymentsClickLister) {
        this.viewPaymentsClickLister = viewPaymentsClickLister;
    }

    public void updateItem(SalesHistory salesHistory) {

        Log.w("here updateRegistration", salesHistory.getId() + "");

        // Log.w("position",purchaseHistoryViewList.indexOf(purchaseHistoryView)+" S");

        //Log.w("position",purchaseHistoryViewList.get(purchaseHistoryViewList.indexOf(purchaseHistoryView)).getSupplierName()+" S");

        // Log.w("position",purchaseHistoryView.getSupplierName()+" S");

        salesHistoryList.set(editPos, salesHistory);

        notifyItemChanged(editPos, salesHistory);

        //  notifyDataSetChanged();

    }

    public void setShowLoader(boolean showLoader) {

        this.showLoader = showLoader;

        if (loaderViewHolder != null) {

            if (showLoader) {

                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {

                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    public ClickListener getmCancelClickLister() {
        return mCancelClickLister;
    }

    public void setmCancelClickLister(ClickListener mCancelClickLister) {

        this.mCancelClickLister = mCancelClickLister;

    }

    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            if (salesHistoryList != null && salesHistoryList.size() != 0) {
                return VIEWTYPE_LOADER;
            } else {
                return VIEWTYPE_ITEM;
            }
        }

        return VIEWTYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        if (salesHistoryList == null || salesHistoryList.size() == 0) {
            return 0;
        } else {
            return salesHistoryList.size() + 1;
        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public ClickListener getViewDetailsClickLister() {
        return viewDetailsClickLister;
    }

    public void setViewDetailsClickLister(ClickListener viewDetailsClickLister) {

        this.viewDetailsClickLister = viewDetailsClickLister;
    }

    public List<SalesHistory> getSalesHistoryList() {
        return salesHistoryList;
    }

    public void setSalesHistoryList(List<SalesHistory> salesHistoryList) {

        this.salesHistoryList = salesHistoryList;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {

        this.editClickListener = editClickListener;
    }

    public class SaleHistoryViewHolder extends RecyclerView.ViewHolder {

        TextView saleIdTextView;
        ImageView deliveryIconView;
        TextView customerTextView;
        TextView dateTextView;
        TextView totalAmountTextView;
        SwipeLayout swipeLayout;
        LinearLayout linearLayout;
        ImageButton editButton;
        ImageButton deleteButton;
        ImageButton viewDetailButton;

        TypedArray customer;
        String today;
        String yesterday;
        ImageButton viewPayments;

        public SaleHistoryViewHolder(View itemView) {
            super(itemView);
            Context context = itemView.getContext();

            deleteButton = (ImageButton) itemView.findViewById(R.id.delete);
            customer = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.customer});
            dateTextView = (TextView) itemView.findViewById(R.id.date);
            //deleteButton=(ImageButton)itemView.findViewById(R.id.delete_sale_trans);
            viewPayments = (ImageButton) itemView.findViewById(R.id.view_payments);
            viewDetailButton = (ImageButton) itemView.findViewById(R.id.view_detail);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            editButton = (ImageButton) itemView.findViewById(R.id.edit_sale_trans);
            saleIdTextView = (TextView) itemView.findViewById(R.id.sale_id_in_sale_transaction_view);
            deliveryIconView = (ImageView) itemView.findViewById(R.id.deliver_icon_in_sale_transaction_view);
            customerTextView = (TextView) itemView.findViewById(R.id.customer_in_sale_transaction_view);
            dateTextView = (TextView) itemView.findViewById(R.id.date_in_sale_transaction_view);
            totalAmountTextView = (TextView) itemView.findViewById(R.id.total_amount_in_sale_transaction_view);



            today = ThemeUtil.getString(context, R.attr.today);
            yesterday = ThemeUtil.getString(context, R.attr.yesterday);

            AccessLogManager     accessLogManager     = new AccessLogManager(context);
            AuthorizationManager authorizationManager = new AuthorizationManager(context);
            Long                 deviceId             = authorizationManager.getDeviceId(Build.SERIAL);
            User                 currentUser          = accessLogManager.getCurrentlyLoggedInUser(deviceId);

            if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Admin.toString())) {
            } else if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Manager.toString())) {
            } else if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Supervisor.toString())) {
                deleteButton.setVisibility(View.GONE);
            } else if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Sale.toString())) {
                editButton.setVisibility(View.GONE);
                deleteButton.setVisibility(View.GONE);
            }
        }
    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar mProgressBar;

        public View mView;

        public LoaderViewHolder(View itemView) {

            super(itemView);

            mView = itemView;

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);
        }
    }
}