package com.digitalfusion.android.pos.util;

import android.util.Log;

import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.database.model.StockItem;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.apache.poi.ss.usermodel.CellType.NUMERIC;
import static org.apache.poi.ss.usermodel.CellType.STRING;

/**
 * Created by hnin on 4/4/16.
 */

public class ExportImportUtil {

    public static void exportStocks(List<StockItem> stockItemList, String path) {
        Log.e("LJO", stockItemList.size()+"");
        String titles[] = new String[]{"Item Name", "Item Code", "Category", "Unit", "Quantity", "Reorder Level", "Purchase Price", "Retail Price", "Wholesale Price", "Description", "Barcode"};
        //New Workbook
        Workbook wb = new XSSFWorkbook();

        //New Sheet
        Sheet sheet1;
        sheet1 = wb.createSheet("Stock");

        //Generate column
        Row row = sheet1.createRow(0);

        //Config Column Width and Heigh
        short s = 300;

        sheet1.setDefaultRowHeight(s);
        sheet1.setColumnWidth(0, (30 * 500));
        sheet1.setColumnWidth(1, (10 * 500));
        sheet1.setColumnWidth(2, (10 * 500));
        sheet1.setColumnWidth(3, (10 * 500));
        sheet1.setColumnWidth(4, (10 * 500));
        sheet1.setColumnWidth(5, (10 * 500));
        sheet1.setColumnWidth(6, (10 * 500));
        sheet1.setColumnWidth(7, (20 * 500));
        sheet1.setColumnWidth(8, (10 * 500));
        sheet1.setColumnWidth(9, (10 * 500));
        sheet1.setColumnWidth(10, (10 * 500));
        sheet1.setColumnWidth(11, (10 * 500));
        sheet1.setColumnWidth(12, (10 * 500));
        Cell c;

        //Generate Header Cell
        for (int i = 0; i < titles.length; i++) {

            c = row.createCell(i);
            c.setCellValue(titles[i]);

        }

        int rowPos = 1;

        for (StockItem stockItem : stockItemList) {
            //create row as long as there is object
            row = sheet1.createRow(rowPos);

            c = row.createCell(0);
            c.setCellType(STRING);
            c.setCellValue(stockItem.getName());

            c = row.createCell(1);
            c.setCellType(STRING);
            c.setCellValue(stockItem.getCodeNo());

            c = row.createCell(2);
            c.setCellType(STRING);
            c.setCellValue(stockItem.getCategoryName());

            c = row.createCell(3);
            c.setCellType(STRING);
            c.setCellValue(stockItem.getUnitName());

            c = row.createCell(4);
            c.setCellType(CellType.NUMERIC);
            c.setCellValue(stockItem.getInventoryQty());


            c = row.createCell(5);
            c.setCellType(CellType.NUMERIC);
            c.setCellValue(stockItem.getReorderLevel());

            c = row.createCell(6);
            c.setCellType(CellType.NUMERIC);
            c.setCellValue(stockItem.getPurchasePrice());


            c = row.createCell(7);

            c.setCellType(CellType.NUMERIC);
            c.setCellValue(stockItem.getRetailPrice());

            c = row.createCell(8);
            c.setCellType(CellType.NUMERIC);
            c.setCellValue(stockItem.getWholesalePrice());

            c = row.createCell(9);
            c.setCellType(STRING);
            c.setCellValue(stockItem.getDescription());

            c = row.createCell(10);
            c.setCellType(STRING);
            c.setCellValue(stockItem.getBarcode());

            rowPos++;
        }

        File             file = new File(path, "stock.xls");

        try (FileOutputStream os = new FileOutputStream(file)){
            wb.write(os);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
/*
    public List<StockItem> importStocksFromExcel(String importDir){
        List<StockItem> stockViews=new ArrayList<>();
        try {

            FileInputStream inputStream = new FileInputStream(new File(importDir));

            Workbook workbook = new XSSFWorkbook(inputStream);

            Sheet firstSheet = workbook.getSheetAt(0);

            Iterator<Row> iterator = firstSheet.iterator();

            //skip header row
            iterator.next();

            while (iterator.hasNext()){

                Row nextRow = iterator.next();

               // Iterator<Cell> cellIterator = nextRow.cellIterator();


                Cell cell;

                StockItem stockView=new StockItem();

                try{
                    cell=nextRow.getCell(0,Row.RETURN_BLANK_AS_NULL);
                    cell.setCellType(STRING);
                    stockView.setName(cell.getStringCellValue());

                    cell=nextRow.getCell(1);
                    cell.setCellType(STRING);
                    stockView.setCodeNo(cell.getStringCellValue());

                    cell=nextRow.getCell(2);
                    cell.setCellType(STRING);
                    stockView.setCategoryName(cell.getStringCellValue());

                    cell=nextRow.getCell(3);
                    cell.setCellType(STRING);
                    stockView.setUnitName(cell.getStringCellValue());

                    cell=nextRow.getCell(4);
                    stockView.setInventoryQty((int) cell.getNumericCellValue());

                    cell=nextRow.getCell(5);
                    stockView.setReorderLevel((int) cell.getNumericCellValue());

                    cell=nextRow.getCell(6);
                    stockView.setPurchasePrice(Double.valueOf(cell.getNumericCellValue()));

                    cell=nextRow.getCell(7);
                    stockView.setRetailPrice(Double.valueOf(cell.getNumericCellValue()));

                    cell=nextRow.getCell(8);
                    stockView.setWholesalePrice(Double.valueOf(cell.getNumericCellValue()));

                    cell=nextRow.getCell(9);
                    cell.setCellType(STRING);
                    stockView.setDescription(cell.getStringCellValue());

                    cell=nextRow.getCell(10);
                    if (cell != null) {
                      if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                        stockView.setBarcode(String.format("%.0f", cell.getNumericCellValue()));
                      } else if (cell.getCellTypeEnum() == STRING) {
                        stockView.setBarcode(cell.getStringCellValue());
                      }
                    }
//                    Log.w("import column 11 ",cell.getStringCellValue()+" value");
                }catch (NumberFormatException e){
                    e.printStackTrace();
                    //error occurs and skip this row
                    Log.e("Cell", "Error 1");

                    continue;
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    continue;
                }

                if(stockView.isVaild()){
                    stockViews.add(stockView);
                }
            }

            Log.w("stock size",stockViews.size()+"SS");


        } catch (FileNotFoundException e) {
            Log.e("Cell", "Error 3");
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("Cell", "Error 4");
            e.printStackTrace();
        }

        return stockViews;

    }

 */

    //check empty row
    private static boolean checkEmptyRow(Row row) {
        if (row == null) {
            return true;
        }
        if (row.getLastCellNum() <= 0) {
            return true;
        }
        for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
            Cell cell = row.getCell(cellNum);
            if (cell != null && cell.getCellTypeEnum() != CellType.BLANK && !cell.toString().trim().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public static List<StockItem> importStocksFromExcel(String importDir) {
        List<StockItem> stockItems = new ArrayList<>();
        try {

            FileInputStream inputStream = new FileInputStream(new File(importDir));

            Workbook workbook = new XSSFWorkbook(inputStream);

            Sheet firstSheet = workbook.getSheetAt(0);

            Iterator<Row> iterator = firstSheet.iterator();

            //skip header row
            iterator.next();

            while (iterator.hasNext()) {

                Row nextRow = iterator.next();

                // Iterator<Cell> cellIterator = nextRow.cellIterator();

                if (checkEmptyRow(nextRow)) break;

                Cell cell;

                StockItem stockItem = new StockItem();

                try {
                    //read item name
                    cell = nextRow.getCell(0, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
                    if (cell != null) {
                        if (cell.getCellTypeEnum() == CellType.NUMERIC) {

                            stockItem.setName(String.format("%.0f", cell.getNumericCellValue()));
                        } else if (cell.getCellTypeEnum() == CellType.STRING) {

                            stockItem.setName(cell.getStringCellValue());
                        } else {
                            stockItem.setName("");
                        }
                    }

                    //read item code
                    cell = nextRow.getCell(1, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
                    if (cell != null) {
                        if (cell.getCellTypeEnum() == CellType.NUMERIC) {

                            stockItem.setCodeNo(String.format("%.0f", cell.getNumericCellValue()));
                        } else if (cell.getCellTypeEnum() == CellType.STRING) {

                            stockItem.setCodeNo(cell.getStringCellValue());
                        } else {
                            stockItem.setCodeNo("");
                        }
                    }

                    //read item category
                    cell = nextRow.getCell(2, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
                    if (cell != null) {
                        if (cell.getCellTypeEnum() == CellType.NUMERIC) {

                            stockItem.setCategoryName(String.format("%.0f", cell.getNumericCellValue()));
                        } else if (cell.getCellTypeEnum() == CellType.STRING) {

                            stockItem.setCategoryName(cell.getStringCellValue());
                        } else {
                            stockItem.setCategoryName("");
                        }
                    }

                    //read unit name
                    cell = nextRow.getCell(3, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
                    if (cell != null) {
                        if (cell.getCellTypeEnum() == CellType.NUMERIC) {

                            stockItem.setUnitName(String.format("%.0f", cell.getNumericCellValue()));
                        } else if (cell.getCellTypeEnum() == CellType.STRING) {

                            stockItem.setUnitName(cell.getStringCellValue());
                        } else {
                            stockItem.setUnitName("");
                        }
                    }

                    //read inventory qty
                    cell = nextRow.getCell(4, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
                    if (cell != null) {
                        if (cell.getCellTypeEnum() == CellType.NUMERIC) {

                            stockItem.setInventoryQty((int) cell.getNumericCellValue());
                        } else if (cell.getCellTypeEnum() == CellType.STRING) {

                            try {
                                int val = Integer.parseInt(cell.getStringCellValue());
                                stockItem.setInventoryQty(val);
                            } catch (NumberFormatException nfe) {
                                stockItem.setInventoryQty(0);
                            }

                        } else {
                            stockItem.setInventoryQty(0);
                        }
                    }

                    //read reorder level
                    cell = nextRow.getCell(5, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
                    if (cell != null) {
                        if (cell.getCellTypeEnum() == CellType.NUMERIC) {

                            stockItem.setReorderLevel((int) cell.getNumericCellValue());
                        } else if (cell.getCellTypeEnum() == CellType.STRING) {

                            try {
                                int val = Integer.parseInt(cell.getStringCellValue());
                                stockItem.setReorderLevel(val);
                            } catch (NumberFormatException nfe) {
                                stockItem.setReorderLevel(0);
                            }

                        } else {
                            stockItem.setReorderLevel(0);
                        }
                    }

                    //read purchase price
                    cell = nextRow.getCell(6, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
                    if (cell != null) {
                        if (cell.getCellTypeEnum() == CellType.NUMERIC) {

                            stockItem.setPurchasePrice(Double.valueOf(cell.getNumericCellValue()));
                        } else if (cell.getCellTypeEnum() == CellType.STRING) {

                            try {
                                double val = Double.parseDouble(cell.getStringCellValue());
                                stockItem.setPurchasePrice(val);
                            } catch (NumberFormatException nfe) {
                                stockItem.setPurchasePrice(0.0);
                            }

                        } else {
                            stockItem.setPurchasePrice(0.0);
                        }
                    }

                    //read retail price
                    cell = nextRow.getCell(7, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
                    if (cell != null) {
                        if (cell.getCellTypeEnum() == CellType.NUMERIC) {

                            stockItem.setRetailPrice(Double.valueOf(cell.getNumericCellValue()));
                        } else if (cell.getCellTypeEnum() == CellType.STRING) {

                            try {
                                double val = Double.parseDouble(cell.getStringCellValue());
                                stockItem.setRetailPrice(val);
                            } catch (NumberFormatException nfe) {
                                stockItem.setRetailPrice(0.0);
                            }

                        } else {
                            stockItem.setRetailPrice(0.0);
                        }
                    }

                    //read wholesale price
                    cell = nextRow.getCell(8, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
                    if (cell != null) {
                        if (cell.getCellTypeEnum() == CellType.NUMERIC) {

                            stockItem.setWholesalePrice(Double.valueOf(cell.getNumericCellValue()));
                        } else if (cell.getCellTypeEnum() == CellType.STRING) {

                            try {
                                double val = Double.parseDouble(cell.getStringCellValue());
                                stockItem.setWholesalePrice(val);
                            } catch (NumberFormatException nfe) {
                                stockItem.setWholesalePrice(0.0);
                            }

                        } else {
                            stockItem.setWholesalePrice(0.0);
                        }
                    }

                    //read desc
                    cell = nextRow.getCell(9, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
                    if (cell != null) {
                        if (cell.getCellTypeEnum() == CellType.NUMERIC) {

                            stockItem.setDescription(String.format("%.0f", cell.getNumericCellValue()));
                        } else if (cell.getCellTypeEnum() == CellType.STRING) {

                            stockItem.setDescription(cell.getStringCellValue());
                        } else {
                            stockItem.setDescription("");
                        }
                    }

                    //read barcode
                    cell = nextRow.getCell(10, Row.MissingCellPolicy.RETURN_NULL_AND_BLANK);
                    if (cell != null) {

                        if (cell.getCellTypeEnum() == CellType.NUMERIC) {

                            stockItem.setBarcode(String.format("%.0f", cell.getNumericCellValue()));
                        } else if (cell.getCellTypeEnum() == CellType.STRING) {

                            stockItem.setBarcode(cell.getStringCellValue());
                        } else {
                            stockItem.setBarcode("");
                        }
                    }
                    //                    Log.w("import column 11 ",cell.getStringCellValue()+" value");
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    //error occurs and skip this row

                    continue;
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    //continue;
                }

                if (stockItem.isVaild()) {
                    stockItems.add(stockItem);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return stockItems;

    }

    public String getCellValue(Cell cell) {

        String value = "";
        switch (cell.getCellTypeEnum()) {

            case STRING:
                value = cell.getStringCellValue();
                break;

            case NUMERIC:
                value = String.valueOf(cell.getNumericCellValue());
                break;
        }
        return value;
    }

    public Double getCellValueDouble(Cell cell) {

        String value = getCellValue(cell);

        if (value == null || value.equalsIgnoreCase("")) {
            return 0.0;
        } else {
            return Double.valueOf(value);
        }
    }

    interface CellValueListener {
        void numeric(Double number);

        void string(String s);
    }

    // Rename this method to a more suitable one
    public static void exportTwoFiltersReports(String reportName,
                                               String[] titles,
                                               List<ReportItem> reportItemList,
                                               Double totalAmt,
                                               String startDate,
                                               String endDate,
                                               String path,
                                               String filterOne) {

        //New Workbook
        Workbook wb = new XSSFWorkbook();

        //New Sheet
        Sheet sheet1;
        sheet1 = wb.createSheet(reportName);

        int rowPos = 0;

        //Config Column Width and Heigh
        short s = 300;

        sheet1.setDefaultRowHeight(s);
        sheet1.setDefaultColumnWidth(3000);
        Cell c;

        Row row = sheet1.createRow(rowPos++);
        sheet1.setColumnWidth(0, (20 * 500));
        sheet1.setColumnWidth(1, (10 * 500));
        sheet1.setColumnWidth(2, (10 * 500));
        sheet1.setColumnWidth(3, (10 * 500));
        sheet1.setColumnWidth(4, (10 * 500));

        c =row.createCell(0);
        c.setCellType(STRING);
        c.setCellValue(DateUtility.makeDateFormatWithSlash(startDate) + (endDate.equalsIgnoreCase(startDate) ? "" : " - " + DateUtility.makeDateFormatWithSlash(endDate)));

        if (filterOne != null && filterOne.length() != 0) {
            row = sheet1.createRow(rowPos++);
            c = row.createCell(0);
            c.setCellType(STRING);
            c.setCellValue(filterOne);
        }

        rowPos++;

        row = sheet1.createRow(rowPos++);
        //Generate Header Cell
        for (int i = 0; i < titles.length - 1; i++) {
            c = row.createCell(i);
            c.setCellValue(titles[i]);
        }


        for (ReportItem reportItem : reportItemList) {
            //create row as long as there is object
            row = sheet1.createRow(rowPos);

            c = row.createCell(0);
            c.setCellType(STRING);
            c.setCellValue(reportItem.getName());

            c = row.createCell(1);
            c.setCellType(NUMERIC);
            c.setCellValue(reportItem.getTotalQty());

            c = row.createCell(2);
            c.setCellType(NUMERIC);
            c.setCellValue(POSUtil.doubleToString(reportItem.getTotalAmt()));

            rowPos++;
        }

        row = sheet1.createRow(rowPos);
        c = row.createCell(1);
        c.setCellType(STRING);
        c.setCellValue(titles[titles.length - 1]);

        c = row.createCell(2);
        c.setCellType(NUMERIC);
        c.setCellValue(POSUtil.doubleToString(totalAmt));

        // ReportName_StartDate_EndDate.xls
        File file = new File(path, reportName.replaceAll("\\s", "") +"_" + startDate +
                (startDate.equals(endDate) ? ".xls" : "_" + endDate + ".xls"));

        try (FileOutputStream os = new FileOutputStream(file)) {
            wb.write(os);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void exportSalePurchaseItemReport(String reportName,
                                          String[] titles,
                                          List<ReportItem> reportItemList,
                                          Double totalAmt,
                                          String startDate,
                                          String endDate,
                                          String path) {

        //New Workbook
        Workbook wb = new XSSFWorkbook();

        //New Sheet
        Sheet sheet1;
        sheet1 = wb.createSheet(reportName);

        int rowPos = 0;

        //Config Column Width and Heigh
        short s = 300;

        sheet1.setDefaultRowHeight(s);
        sheet1.setDefaultColumnWidth(3000);
        Cell c;

        Row row = sheet1.createRow(rowPos++);
        sheet1.setColumnWidth(0, (20 * 500));
        sheet1.setColumnWidth(1, (10 * 500));
        sheet1.setColumnWidth(2, (10 * 500));
        sheet1.setColumnWidth(3, (10 * 500));
        sheet1.setColumnWidth(4, (10 * 500));
        
        c = row.createCell(0);
        c.setCellType(STRING);
        c.setCellValue(DateUtility.makeDateFormatWithSlash(startDate) + (endDate.equalsIgnoreCase(startDate) ? "" : " - " + DateUtility.makeDateFormatWithSlash(endDate)));

        row = sheet1.createRow(rowPos++);
        //Generate Header Cell
        for (int i = 0; i < titles.length - 1; i++) {
            c = row.createCell(i);
            c.setCellValue(titles[i]);
        }


        for (ReportItem reportItem : reportItemList) {
            //create row as long as there is object
            row = sheet1.createRow(rowPos);

            c = row.createCell(0);
            c.setCellType(STRING);
            c.setCellValue(reportItem.getName());

            c = row.createCell(1);
            c.setCellType(NUMERIC);
            c.setCellValue(reportItem.getQty());

            c = row.createCell(2);
            c.setCellType(NUMERIC);
            c.setCellValue(POSUtil.doubleToString(reportItem.getAmount()));

            rowPos++;
        }

        row = sheet1.createRow(rowPos);
        c = row.createCell(1);
        c.setCellType(STRING);
        c.setCellValue(titles[titles.length - 1]);

        c = row.createCell(2);
        c.setCellType(NUMERIC);
        c.setCellValue(POSUtil.doubleToString(totalAmt));

        File file = new File(path, reportName + "_" + startDate +
                (startDate.equals(endDate) ? ".xls" : "_" + endDate + ".xls"));

        try (FileOutputStream os = new FileOutputStream(file)) {
            wb.write(os);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
