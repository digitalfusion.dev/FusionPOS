package com.digitalfusion.android.pos.fragments.outstanding;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.ReceivableSearchAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.SaleHistorySearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVAdapterForSaleRecievableList;
import com.digitalfusion.android.pos.database.business.CustomerOutstandingManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.FusionToast;
import com.example.searchview.MaterialSearchView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 10/18/16.
 */

public class ReceivableFragment extends Fragment {
    private Context context;

    private View mainLayout;
    private Button customRangeOkBtn, customRangeCancelBtn;
    private RecyclerView receivableListRecyclerView;
    private TextView noTransactionTextView;
    private MaterialSearchView searchView;
    private MaterialDialog filterDialog;
    private TextView filterTextView;
    private DatePickerDialog customeDatePickerDialog;
    private MaterialDialog addPaymentMaterialDialog;
    private Button saveBtnMd;
    private Button cancelBtnMd;
    private EditText paymentDateTxtMd;
    private EditText paymentEditTxtMd;
    private ImageButton calenderBtnMd;
    private TextView paymentId;
    private TextView customerName;
    private DatePickerDialog datePickerDialog;
    private DatePickerDialog startDatePickerDialog;
    private MaterialDialog customRangeDialog;
    private TextView startDateTextView, endDateTextView;
    private TextView traceDate;

    private List<SalesHistory> receivableList;
    private CustomerOutstandingManager customerOutstandingManager;
    private RVAdapterForSaleRecievableList rvAdapterForSaleRecievableList;
    private RVAdapterForFilter rvAdapterForFilter;
    private List<String> filterList;
    private Calendar calendar;
    private SaleHistorySearchAdapter saleHistorySearchAdapter;
    private Customer customer;
    private ReceivableSearchAdapter receivableSearchAdapter;

    private String startDate;
    private String endDate;
    private Long customerIDTemp;
    private Long saleIdTemp;
    private int paymentDay;
    private int paymentMonth;
    private int paymentYear;
    private String date;
    private int payPos;
    private String customStartDate, customEndDate;
    private int oldPos = 0;
    private int current = 0;
    private boolean isSearch = false;
    private boolean shouldLoad = true;
    private String searchText = "";

    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {

            String str = ((SalesHistory) resultValue).getVoucherNo();

            return str;

        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            Log.w("filtering", "filtering");


            if (constraint != null && !constraint.equals("")) {

                if (constraint.toString().startsWith("#")) {
                    receivableSearchAdapter.setSuggestion(customerOutstandingManager.getCustomerOutstandingListOnSearch(0, 10, "00000000000", "99999999999999", constraint.toString().substring(1, constraint.length())));
                } else {
                    receivableSearchAdapter.setSuggestion(customerOutstandingManager.getCustomerOutstandingListOnSearch(0, 10, "00000000000", "99999999999999", constraint.toString()));
                }


                FilterResults filterResults         = new FilterResults();
                receivableSearchAdapter.lenght      = constraint.length();
                receivableSearchAdapter.queryText   = constraint.toString();
                filterResults.values                = receivableSearchAdapter.getSuggestion();
                filterResults.count                 = receivableSearchAdapter.getSuggestion().size();

                return filterResults;
            } else {

                return new FilterResults();

            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null && results.count > 0) {
                receivableSearchAdapter.notifyDataSetChanged();

            }

        }

    };

    private TextView totalReceivableTextView;
    private TextView searchedResultTxt;

    private String allTrans;
    private String thisWeek;
    private String lastWeek;
    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;
    private String customRange;
    private String customDate;
    private boolean darkmode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout     = inflater.inflate(R.layout.receivable_list_fragment, null);
        receivableList = new ArrayList<>();
        context        = getContext();
        calendar       = Calendar.getInstance();
        darkmode       = POSUtil.isNightMode(context);

        buildDatePickerDialog();

        paymentDay   = calendar.get(Calendar.DAY_OF_MONTH);
        paymentMonth = calendar.get(Calendar.MONTH) + 1;
        paymentYear  = calendar.get(Calendar.YEAR        );

        if (getArguments() != null) {
            customer = (Customer) getArguments().getSerializable("customer");
        }

        allTrans    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);
        thisMonth   = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month}).getString(0);
        lastMonth   = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month}).getString(0);
        thisYear    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year}).getString (0);
        lastYear    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString (0);
        customRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_range}).getString(0);
        customDate  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_date}).getString (0);
        thisWeek    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_week}).getString (0);
        lastWeek    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week}).getString (0);

        filterList = new ArrayList<>();
        filterList.add(allTrans);
        filterList.add(thisWeek);
        filterList.add(lastWeek);
        filterList.add(thisMonth);
        filterList.add(lastMonth);
        filterList.add(thisYear);
        filterList.add(lastYear);
        filterList.add(customRange);
        filterList.add(customDate);

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        customeDatePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        startDate   = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                        endDate     = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                        filterTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));

                        loadReceivableList(0, 10);
                        refreshRecyclerView();

                    }
                },

                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );

        if (darkmode)
            customeDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        customeDatePickerDialog.setThemeDark(darkmode);

        buildAddPaymentDialog();
        loadUI();
        buildDateFilterDialog();
        buildingCustomRangeDialog();
        MainActivity.setCurrentFragment(this);

        return mainLayout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        //TypedArray allTransArr=context.getTheme().obtainStyledAttributes(new int[]{R.attr.pickerAccentColor});
        //customeDatePickerDialog.setAccentColor(allTransArr.getColor(0,0));
        //customeDatePickerDialog.setThemeDark(true);

        startDate                   = "000000000000";
        endDate                     = "9999999999999999";
        customerOutstandingManager = new CustomerOutstandingManager(context);

        loadReceivableList(0, 10);
        configRecycler();

        receivableSearchAdapter = new ReceivableSearchAdapter(context, nameFilter);
        searchView.setAdapter(receivableSearchAdapter);

        paymentEditTxtMd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (paymentEditTxtMd.getText().toString().trim().length() > 0) {

                    Double payAmount = Double.valueOf(paymentEditTxtMd.getText().toString().trim());
                    if (payAmount > receivableList.get(payPos).getBalance()) {
                        String greaterthanleft = context.getTheme().obtainStyledAttributes(new int[]{R.attr.payment_amount_is_greater_than_left_amount}).getString(0);
                        paymentEditTxtMd.setError(greaterthanleft);
                    }

                }

            }
        });


        searchView.showSuggestions();
        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                shouldLoad     = false;
                receivableList = new ArrayList<SalesHistory>();

                receivableList.add(receivableSearchAdapter.getSuggestion().get(position));

                refreshRecyclerView();

                searchView.closeSearch();
                filterTextView.setText("-");

                //                searchedResultTxt.setVisibility(View.VISIBLE);


            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                shouldLoad = true;
                isSearch = true;
                searchText = query;

                loadReceivableList(0, 10, query);
                refreshRecyclerView();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                return false;
            }
        });


        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });

        setFilterTextView(filterList.get(0));

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                //                searchedResultTxt.setVisibility(View.INVISIBLE);

                shouldLoad = true;
                isSearch   = false;

                if (filterList.get(position).equalsIgnoreCase(allTrans)) {
                    setFilterTextView(filterList.get(position));
                    startDate = "000000000000";
                    endDate = "9999999999999999";

                    loadReceivableList(0, 10);
                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(thisMonth)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisMonthStartDate();
                    endDate   = DateUtility.getThisMonthEndDate  ();

                    loadReceivableList(0, 10);
                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastMonthStartDate();
                    endDate   = DateUtility.getLastMonthEndDate  ();

                    loadReceivableList(0, 10);
                    refreshRecyclerView();
                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisYearStartDate();
                    endDate   = DateUtility.getThisYearEndDate  ();

                    loadReceivableList(0, 10);
                    refreshRecyclerView();
                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastYearStartDate();
                    endDate   = DateUtility.getLastYearEndDate  ();

                    loadReceivableList(0, 10);
                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {
                    customRangeDialog.show();

                } else if (filterList.get(position).equalsIgnoreCase(customDate)) {

                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "customeDate");

                } else if (filterList.get(position).equalsIgnoreCase(thisWeek)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getStartDateOfWeekString();
                    endDate   = DateUtility.getEndDateOfWeekString();

                    loadReceivableList(0, 10);
                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(lastWeek)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getStartDateOfLastWeekString();
                    endDate   = DateUtility.getEndDateOfLastWeekString();

                    loadReceivableList(0, 10);
                    refreshRecyclerView();

                }
                totalReceivableTextView.setText(POSUtil.NumberFormat(customerOutstandingManager.getTotalReceivableAmt(startDate, endDate)));
                filterDialog.dismiss();
            }
        });

        rvAdapterForSaleRecievableList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                Log.e("delete", "ok");
            }
        });

        rvAdapterForSaleRecievableList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle = new Bundle();

                bundle.putSerializable("saleheader", receivableList.get(postion));

                // SaleEditFragments editSaleFragment=new SaleEditFragments();

                //  editSaleFragment.setArguments(bundle);

                // MainActivity.replacingFragment(editSaleFragment);

            }
        });

        rvAdapterForSaleRecievableList.setViewDetailListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                Bundle bundle = new Bundle();
                bundle.putLong(ReceivableDetailFragment.KEY, receivableList.get(postion).getId());

                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.RECEIVABLE_DETAIL_VOUCHER);
                detailIntent.putExtras(bundle);
                startActivity(detailIntent);
            }
        });

        rvAdapterForSaleRecievableList.setViewPaymentListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("saleheader", receivableList.get(postion));

                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.RECEIVABLE_PAYMENT_DETAIL);
                detailIntent.putExtras(bundle);
                startActivity(detailIntent);
            }
        });

        rvAdapterForSaleRecievableList.setAddPaymentListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                payPos         = postion;
                saleIdTemp     = receivableList.get(postion).getId();
                customerIDTemp = receivableList.get(postion).getCustomerID();

                paymentId.setText("#" + customerOutstandingManager.getCustomerOutstandingPaymentInvoiceNo());
                customerName.setText(receivableList.get(postion).getCustomer());
                paymentEditTxtMd.setText(POSUtil.doubleToString(receivableList.get(postion).getBalance()));
                paymentEditTxtMd.setError(null);

                configDateUI();

                addPaymentMaterialDialog.show();
            }
        });

        /*rvAdapterForSaleRecievableList.setViewDetailClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle=new Bundle();

                bundle.putSerializable("saleheader", receivableList.get(postion));

                SaleDetailFragment saleDetailFragment =new SaleDetailFragment();

                saleDetailFragment.setArguments(bundle);

                MainActivity.replacingFragment(saleDetailFragment);

            }

        });*/

        saveBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!paymentEditTxtMd.getText().toString().isEmpty() && Double.valueOf(paymentEditTxtMd.getText().toString().trim()) <= receivableList.get(payPos).getBalance()) {

                    customerOutstandingManager.addCustomerOutstandingPayment(customerOutstandingManager.getCustomerOutstandingPaymentInvoiceNo(),

                            customerIDTemp, saleIdTemp, Double.valueOf(paymentEditTxtMd.getText().toString()),

                            Integer.toString(paymentDay), Integer.toString(paymentMonth), Integer.toString(paymentYear));

                    receivableList.get(payPos).setBalance(receivableList.get(payPos).getBalance() - Double.valueOf(paymentEditTxtMd.getText().toString()));

                    if (receivableList.get(payPos).getBalance() > 0) {

                        rvAdapterForSaleRecievableList.notifyItemChanged(payPos);

                        addPaymentMaterialDialog.dismiss();
                    } else if (payPos != 0) {

                        receivableList.remove(payPos);

                        rvAdapterForSaleRecievableList.setReceivableList(receivableList);

                        rvAdapterForSaleRecievableList.notifyItemRemoved(payPos);

                        rvAdapterForSaleRecievableList.notifyItemChanged(payPos, receivableList.size());

                        addPaymentMaterialDialog.dismiss();

                    } else {
                        receivableList.remove(payPos);

                        rvAdapterForSaleRecievableList.setReceivableList(receivableList);

                        addPaymentMaterialDialog.dismiss();

                        rvAdapterForSaleRecievableList.notifyDataSetChanged();
                    }
                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                }
            }
        });

        cancelBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPaymentMaterialDialog.dismiss();
            }
        });

        paymentDateTxtMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog.show(getActivity().getFragmentManager(), "Date Pick");

            }
        });

        calenderBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog.show(getActivity().getFragmentManager(), "Date Pick");

            }
        });

        receivableListRecyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

            }
        });

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDate = customStartDate;

                endDate = customEndDate;


                filterTextView.setText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));

                //  filterTextView.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));

                loadReceivableList(0, 10);

                refreshRecyclerView();


                customRangeDialog.dismiss();
            }
        });

        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRangeDialog.dismiss();
            }
        });

        receivableListRecyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                if (shouldLoad) {

                    rvAdapterForSaleRecievableList.setShowLoader(true);

                    loadmore();

                }
            }
        });

        rvAdapterForFilter.setCurrentPos(3);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void loadmore() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isSearch) {

                    receivableList.addAll(loadMore(receivableList.size(), 9, searchText));

                } else {

                    receivableList.addAll(loadMore(receivableList.size(), 9));

                }

                receivableList.addAll(loadMore(receivableList.size(), 9));

                rvAdapterForSaleRecievableList.setShowLoader(false);

                refreshRecyclerView();

            }
        }, 500);
    }

    public List<SalesHistory> loadMore(int startLimit, int endLimit) {

        return customerOutstandingManager.getCustomerOutstandingList(startLimit, endLimit, startDate, endDate);


    }

    public List<SalesHistory> loadMore(int startLimit, int endLimit, String query) {

        return customerOutstandingManager.getCustomerOutstandingListOnSearch(startLimit, endLimit, startDate, endDate, query);

    }

    private void refreshRecyclerView() {

        if (receivableList.size() < 1) {

            noTransactionTextView.setVisibility(View.VISIBLE);

            receivableListRecyclerView.setVisibility(View.GONE);

        } else {

            noTransactionTextView.setVisibility(View.GONE);

            receivableListRecyclerView.setVisibility(View.VISIBLE);

            rvAdapterForSaleRecievableList.setReceivableList(receivableList);

            rvAdapterForSaleRecievableList.notifyDataSetChanged();
        }

    }

    public void buildAddPaymentDialog() {

        String addPayment = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_payment}).getString(0);

        addPaymentMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.receivable_add_payment_md, true)
                .title(addPayment).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.positiveText("Save").negativeText("Cancel")
                .build();

        saveBtnMd = (Button) addPaymentMaterialDialog.findViewById(R.id.save);

        cancelBtnMd = (Button) addPaymentMaterialDialog.findViewById(R.id.cancel);

        paymentEditTxtMd = (EditText) addPaymentMaterialDialog.findViewById(R.id.add_payment_et);

        paymentDateTxtMd = (EditText) addPaymentMaterialDialog.findViewById(R.id.calender_tv);

        calenderBtnMd = (ImageButton) addPaymentMaterialDialog.findViewById(R.id.calender_btn);

        paymentId = (TextView) addPaymentMaterialDialog.findViewById(R.id.payment_id);

        customerName = (TextView) addPaymentMaterialDialog.findViewById(R.id.customer_name);
    }

    private void loadReceivableList(int startLimit, int endLimit) {

        if (customer != null) {

            receivableList = customerOutstandingManager.getCustomerOutstandingListByCustomer(startLimit, endLimit, startDate, endDate, customer.getId());

        } else {

            receivableList = customerOutstandingManager.getCustomerOutstandingList(startLimit, endLimit, startDate, endDate);
        }

    }

    public void buildingCustomRangeDialog() {

        TypedArray dateRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});
        //TypedArray ok = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ok});

        buildingCustomRangeDatePickerDialog();
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(dateRange.getString(0))
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.negativeText(cancel.getString(0))
                //.positiveText(ok.getString(0))
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    private void loadReceivableList(int startLimit, int endLimit, String query) {

        // receivableList =customerOutstandingManager.get(startLimit,endLimit,startDate,endDate,query);

    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });

        if (darkmode)
            startDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        startDatePickerDialog.setThemeDark(darkmode);

    }

    private void setFilterTextView(String msg) {

        filterTextView.setText(msg);

    }

    private void buildDateFilterDialog() {

        String filterByDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);

        filterDialog = new MaterialDialog.Builder(context).

                title(filterByDate)

                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    public void configRecycler() {

        rvAdapterForSaleRecievableList = new RVAdapterForSaleRecievableList(receivableList);

        receivableListRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        receivableListRecyclerView.setAdapter(rvAdapterForSaleRecievableList);

        refreshRecyclerView();

    }

    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        searchView.setCursorDrawable(attributeResourceId);

    }

    public void loadUI() {

        totalReceivableTextView = (TextView) mainLayout.findViewById(R.id.total_receivable);

        noTransactionTextView = (TextView) mainLayout.findViewById(R.id.no_transaction);

        searchedResultTxt = (TextView) mainLayout.findViewById(R.id.searched_result_txt);

        receivableListRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.rv);

        filterTextView = (TextView) mainLayout.findViewById(R.id.filter_one);


        loadUIFromToolbar();

    }

    public void configDateUI() {

        date = DateUtility.makeDate(Integer.toString(paymentYear), Integer.toString(paymentMonth), Integer.toString(paymentDay));

        String dayDes[] = DateUtility.dayDes(date);

        String yearMonthDes = DateUtility.monthYearDes(date);

        //paymentDateTxtMd.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

        paymentDateTxtMd.setText(DateUtility.makeDateFormatWithSlash(date));


    }

    public void buildDatePickerDialog() {

        datePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        paymentDay = dayOfMonth;

                        paymentMonth = monthOfYear + 1;

                        paymentYear = year;

                        date = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear), Integer.toString(dayOfMonth));

                        String dayDes[] = DateUtility.dayDes(date);

                        String yearMonthDes = DateUtility.monthYearDes(date);

                        //paymentDateTxtMd.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

                        paymentDateTxtMd.setText(DateUtility.makeDateFormatWithSlash(date));

                    }
                },

                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)

        );

        if (darkmode)
            datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        datePickerDialog.setThemeDark(darkmode);

    }

    @Override
    public void onDetach() {
        searchView.closeSearch();
        searchView.hideKeyboard(searchView);
        super.onDetach();
    }

}