package com.digitalfusion.android.pos.adapters;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;

import java.util.List;

/**
 * Created by MD003 on 6/6/16.
 */
public class BluetoothDeviceViewAdapter extends ArrayAdapter<BluetoothDevice> {

    List<BluetoothDevice> deviceInfoArrayList;
    Context context;

    public BluetoothDeviceViewAdapter(Context context, int resource, int textViewResourceId, List<BluetoothDevice> objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        deviceInfoArrayList = objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.device_name, parent, false);

        }
        viewHolder = new ViewHolder();

        viewHolder.deviceName = (TextView) convertView.findViewById(R.id.device_name);
        viewHolder.deviceAddress = (TextView) convertView.findViewById(R.id.device_address);
        //viewHolder.owerNameTextView.setText(VehicleInfoObjList.get(position).getOwerName());
        viewHolder.deviceName.setText(deviceInfoArrayList.get(position).getName());
        viewHolder.deviceAddress.setText(deviceInfoArrayList.get(position).getAddress());


        return convertView;
    }


    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;

    }


}
