package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.User;

import java.util.List;

/**
 * Created by MD002 on 2/9/18.
 */

public class RVAdapterForUserListPasscode extends RecyclerView.Adapter<RVAdapterForUserListPasscode.LoginViewHolder> {
    List<User> userList;
    private LoginVRadapterEventHandler loginItemClickHandler;

    //    private int lastPos;

    public RVAdapterForUserListPasscode(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public LoginViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_login, parent, false);
        return new LoginViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LoginViewHolder holder, final int position) {
        //        holder.radioButton.setChecked(lastPos == position);
        User currentUser = userList.get(position);

        holder.username.setText(currentUser.getUserName());
        holder.role.setText(currentUser.getRole());
        holder.description.setVisibility(View.GONE);
        //TODO: Uncommented lines for displaying picture of users
        //        if (currentUser.getPicture() != null) {
        //            Bitmap image = POSUtil.getBitmapFromByteArray(currentUser.getPicture());
        //            holder.userPicture.setImageBitmap(image);
        //        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //                setSelectedPosition(position);
                loginItemClickHandler.onItemClick(position);
            }

        });
        if (position == userList.size() - 1) {
            holder.separator.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void setLoginItemClickHandler(LoginVRadapterEventHandler loginItemClickHandler) {
        this.loginItemClickHandler = loginItemClickHandler;
    }

    //    public void setSelectedPosition(int position) {
    //        lastPos = position;
    //        notifyDataSetChanged();
    //    }


    public interface LoginVRadapterEventHandler {
        void onItemClick(int position);
    }

    class LoginViewHolder extends RecyclerView.ViewHolder {
        TextView username;
        TextView role;
        TextView description;
        ImageView userPicture;
        View separator;

        LoginViewHolder(View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.user_name);
            role = itemView.findViewById(R.id.device_no);
            description = itemView.findViewById(R.id.user_description);
            userPicture = itemView.findViewById(R.id.userPicture);
            separator = itemView.findViewById(R.id.separator);
        }
    }
}
