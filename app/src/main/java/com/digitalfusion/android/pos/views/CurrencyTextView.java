package com.digitalfusion.android.pos.views;

import android.content.Context;
import android.util.AttributeSet;

import com.digitalfusion.android.pos.util.AppConstant;
import com.innovattic.font.FontTextView;

/**
 * Created by MD003 on 11/30/16.
 */

public class CurrencyTextView extends FontTextView {


    public CurrencyTextView(Context context) {
        super(context);
        setText(AppConstant.CURRENCY);
    }

    public CurrencyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setText(AppConstant.CURRENCY);
    }

    public CurrencyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setText(AppConstant.CURRENCY);
    }


}
