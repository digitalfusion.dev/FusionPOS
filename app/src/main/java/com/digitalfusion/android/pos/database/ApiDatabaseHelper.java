package com.digitalfusion.android.pos.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.util.AppConstant;

import java.text.SimpleDateFormat;

/**
 * Created by MD002 on 1/17/18.
 *
 * EDIT: OK. I have literally no idea why this class was named ApiManager or the database was named "FusionApi"
 *       But I guess this database is solely for app data, not the data which will be stored by user.
 */

public class ApiDatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "FusionApi.sqlite";
    public static final int DATABASE_VERSION = 4;

    private static ApiDatabaseHelper helperInstance;

    private ApiDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private void createAuthorizedDeviceListTable(@NonNull SQLiteDatabase db) {
        /*
        CREATE TABLE IF NOT EXISTS authorized_devices (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            serial_no TEXT,
            name TEXT NOT NULL,
            permission text
        );
         */
        db.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.TABLE_AUTHORIZED_DEVICES + " (" +
                AppConstant.COLUMN_DEVICE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                AppConstant.COLUMN_DEVICE_SERIAL_NO + " TEXT ," +
                AppConstant.COLUMN_DEVICE_NAME  + " TEXT NOT NULL," +
                AppConstant.COLUMN_DEVICE_PERMISSION + " TEXT);");
    }

    private void createAccessLogTable(@NonNull SQLiteDatabase db) {
        /*
        CREATE TABLE IF NOT EXISTS access_logs (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            user_id INTEGER REFERENCES userRoles(id) ON UPDATE CASCADE ON DELETE CASCADE,
            device_id TEXT REFERENCES authorized_devices(_id) ON UPDATE CASCADE ON DELETE CASCADE,
            time INTEGER,
            date TEXT,
            event TEXT
        );
         */
        db.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.TABLE_ACCESS_LOGS + " (" +
                " " + AppConstant.COLUMN_ACCESS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                " " + AppConstant.COLUMN_ACCESS_USER_ID + " INTEGER REFERENCES userRoles(" + AppConstant.COLUMN_USER_ID + ") ON UPDATE CASCADE ON DELETE CASCADE," +
                " " + AppConstant.COLUMN_ACCESS_DEVICE_ID + " TEXT REFERENCES authorized_devices(" + AppConstant.COLUMN_DEVICE_ID + ") ON UPDATE CASCADE ON DELETE CASCADE," +
                " " + AppConstant.COLUMN_ACCESS_DATE_TIME + " TEXT," +
                " " + AppConstant.COLUMN_ACCESS_EVENT + " TEXT);");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table if not exists " + AppConstant.LICENSE_INFO_TABLE_NAME + "(" + AppConstant.LICENSE_INFO_DURATION + " text, " + AppConstant.LICENSE_INFO_START_DATE +
                " date, " + AppConstant.LICENSE_INFO_END_DATE + " date)");

        db.execSQL("create table if not exists " + AppConstant.REGISTRATION_TABLE_NAME + "(" + AppConstant.REGISTRATION_USERNAME + " text, " + AppConstant.REGISTRATION_USER_ID + " text, " +
                AppConstant.REGISTRATION_BUSINESS_NAME + " text, " + AppConstant.REGISTRATION_USER_STATUS + " text )");

        addLicense(db);

        /*
        CREATE TABLE IF NOT EXISTS userRoles (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            passcode TEXT,
            role TEXT NOT NULL,
            description TEXT,
            picture BLOB,
            lastLogin TEXT
         );
         */
        db.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.TABLE_USER_ROLES + " (" +
                AppConstant.COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                AppConstant.COLUMN_USER_NAME + " TEXT NOT NULL," +
                AppConstant.COLUMN_USER_PASSCODE + " TEXT," +
                AppConstant.COLUMN_USER_ROLE + " TEXT NOT NULL, " +
                AppConstant.COLUMN_USER_DESCRIPTION + " TEXT, " +
                AppConstant.COLUMN_USER_PICTURE + " BLOB, " +
                AppConstant.COLUMN_USER_LAST_LOGIN + " TEXT);");

        addOwner(db);

        createAuthorizedDeviceListTable(db);
        createAccessLogTable(db);

        AppConstant.clearDataBase=true;
    }

    public static ApiDatabaseHelper getHelperInstance(Context context) {
        if (helperInstance == null){
            helperInstance = new ApiDatabaseHelper(context);
        }
        return helperInstance;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // we want all updates, so no break statement here
        switch (oldVersion) {
            case 2:
                upgradeToV3(db);
            case 3:
                // Version 4 creates 2 new tables
                createAuthorizedDeviceListTable(db);
                createAccessLogTable(db);
        }
    }

    private void addOwner(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(AppConstant.COLUMN_USER_NAME, User.ROLE.Owner.toString());
        values.put(AppConstant.COLUMN_USER_PASSCODE, "");
        values.put(AppConstant.COLUMN_USER_ROLE, User.ROLE.Owner.toString());

        db.insert(AppConstant.TABLE_USER_ROLES, null, values);
    }

    private boolean addLicense(SQLiteDatabase db){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String query = "insert into " + AppConstant.LICENSE_INFO_TABLE_NAME + "(" +
                AppConstant.LICENSE_INFO_DURATION + ", " +
                AppConstant.LICENSE_INFO_START_DATE + ", " +
                AppConstant.LICENSE_INFO_END_DATE + ") values (?, ?, ?)";
        db.execSQL(query, new String[]{"0", "000000", "0000"});


        String query1= "insert into " + AppConstant.REGISTRATION_TABLE_NAME + "(" + AppConstant.REGISTRATION_USERNAME + ", " + AppConstant.REGISTRATION_USER_ID + ", " +
                AppConstant.REGISTRATION_BUSINESS_NAME + ", " + AppConstant.REGISTRATION_USER_STATUS + ") values (?, ?, ?, ?)";
        db.execSQL(query1, new String[]{"no", "no", "no", "no"});


        return true;
    }

    /**
     * Upgrade Db version from v2 -> v3
     */
    private void upgradeToV3(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE TempTable (" +
                AppConstant.COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                AppConstant.COLUMN_USER_NAME + " TEXT NOT NULL," +
                AppConstant.COLUMN_USER_PASSCODE + " TEXT," +
                AppConstant.COLUMN_USER_ROLE + " TEXT NOT NULL, " +
                AppConstant.COLUMN_USER_DESCRIPTION + " TEXT, " +
                AppConstant.COLUMN_USER_PICTURE + " BLOB, " +
                AppConstant.COLUMN_USER_LAST_LOGIN + " TEXT);");
        db.execSQL("INSERT INTO TempTable SELECT " +
                AppConstant.COLUMN_USER_ID + "," + AppConstant.COLUMN_USER_NAME + "," +
                AppConstant.COLUMN_USER_PASSCODE + "," + AppConstant.COLUMN_USER_ROLE + "," +
                AppConstant.COLUMN_USER_DESCRIPTION + "," + AppConstant.COLUMN_USER_PICTURE + "," +
                AppConstant.COLUMN_USER_LAST_LOGIN + " FROM " + AppConstant.TABLE_USER_ROLES +";");
        db.execSQL("DROP TABLE IF EXISTS " + AppConstant.TABLE_USER_ROLES);
        db.execSQL("ALTER TABLE TempTable RENAME TO " + AppConstant.TABLE_USER_ROLES +";");
    }

}
