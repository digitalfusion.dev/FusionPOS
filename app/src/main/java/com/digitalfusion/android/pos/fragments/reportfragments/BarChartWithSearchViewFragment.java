package com.digitalfusion.android.pos.fragments.reportfragments;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForMonthAmtReport;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.DefaultYAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class BarChartWithSearchViewFragment extends Fragment implements Serializable {

    public static int[] colorArr = new int[]{
            ColorTemplate.rgb("#03A9F4"),
            ColorTemplate.rgb("#CDDC39"),
            ColorTemplate.rgb("#FF9800"),
            ColorTemplate.rgb("#e040fb"),
            ColorTemplate.rgb("#80DEEA"),
            ColorTemplate.rgb("#3F51B5"),
            ColorTemplate.rgb("#FFEB3B"),
            ColorTemplate.rgb("#00BFA5"),
            ColorTemplate.rgb("#757575"),
            ColorTemplate.rgb("#F44336"),
            ColorTemplate.rgb("#2E7D32"),
            ColorTemplate.rgb("#795548")};
    protected String[] mMonths = new String[]{
            " Jan", " Feb", " Mar", " Apr", " May", " Jun", " Jul", " Aug", " Sep", " Oct", " Nov", " Dec"
    };
    private TextView dateFilterTextView;
    private RVAdapterForFilter rvAdapterForDateFilter;
    private ParentRVAdapterForReports rvAdapterForReport;
    private MaterialDialog dateFilterDialog;
    private List<String> filterList;
    private View mainLayoutView;
    private Context context;
    private List<ReportItem> reportItemList;
    private ReportManager reportManager;
    private RecyclerView recyclerView;
    private BarChart barChart;
    private String reportType;
    private String startDate;
    private String endDate;
    private String thisYear;
    private String lastYear;
    private String last12Month;
    private String[] dateFilterArr;
    private boolean isTwelve;


    //private MaterialSearchView searchView;
    private String orderby;

    //private StockSearchAdapter stockSearchAdapter;

    //private StockManager stockBusiness;
    private Long stockID;
    private TextView chartDescTextView;
    private String chartDesc;
    private List<Double> yValList;
    private ArrayList<String> xValues;
    private List<BarEntry> yValues;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_bar_chart_with_search_view, container, false);
        //        setHasOptionsMenu(true);
        context = getContext();
        loadIngUI();
        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            reportType = getArguments().getString("reportType");
            stockID = getArguments().getLong("stockID");
            chartDesc = getArguments().getString("stockName");
        }
        Log.e("report type " + stockID, reportType);
        if (reportType.equalsIgnoreCase("monthly gross profit by product")) {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.monthly_sales_profit_gross_by_product}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        }
        initializeVariables();
        setDatesAndLimits();

        configFilter();

        buildDateFilterDialog();

        setDateFilterTextView(thisYear);

        clickListeners();

        new LoadProgressDialog().execute("");
        //        searchView.showSearch(false);
        //boolean bool = chartDescTextView.performClick();
        //Log.e("boo", bool + "ifj ij kjf");
    }

    @Override
    public void onResume() {
        super.onResume();
        //        searchView.showSearch(false);
    }

    private void configFilter() {
        TypedArray mthisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year});

        TypedArray mlastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year});

        TypedArray mlast12Month = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_12_month});


        thisYear = mthisYear.getString(0);

        lastYear = mlastYear.getString(0);


        last12Month = mlast12Month.getString(0);


        filterList = new ArrayList<>();

        filterList.add(thisYear);

        filterList.add(lastYear);

        filterList.add(last12Month);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    private void clickListeners() {
        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                dateFilterDialog.dismiss();


                if (filterList.get(position).equalsIgnoreCase(thisYear)) {
                    orderby = "asc";
                    isTwelve = false;

                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    int year = Calendar.getInstance().get(Calendar.YEAR);
                    for (int i = 0; i < 12; i++) {
                        dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
                    }

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {
                    orderby = "asc";
                    isTwelve = false;

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();
                    int year = Calendar.getInstance().get(Calendar.YEAR) - 1;
                    dateFilterArr = new String[12];
                    for (int i = 0; i < 12; i++) {
                        dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
                    }

                } else if (filterList.get(position).equalsIgnoreCase(last12Month)) {
                    orderby = "asc";
                    isTwelve = true;

                    startDate = DateUtility.getLast12MonthStartDate();

                    endDate = DateUtility.getLast12MotnEndDate();

                    Log.e("1 ", startDate + "  " + endDate);

                    dateFilterArr = new String[12];
                    int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
                    int year  = Calendar.getInstance().get(Calendar.YEAR);
                    for (int i = 0; i < 12; i++) {
                        if (month < 1) {
                            year -= 1;
                            month = 12;
                        }
                        dateFilterArr[i] = "(" + (month) + "," + year + ")";
                        month -= 1;
                    }

                }

                new LoadProgressDialog().execute("");

                setDateFilterTextView(filterList.get(position));

            }
        });

        //        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        //            @Override
        //            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //
        //                stockID = stockSearchAdapter.getSuggestionList().get(position).getId();
        //
        //                chartDesc = stockSearchAdapter.getSuggestionList().get(position).getName();
        //
        //                searchView.closeSearch();
        //
        //                new CreateReportTask().execute("");
        //
        //            }
        //        });
        //
        //        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
        //            @Override
        //            public boolean onQueryTextSubmit(String query) {
        //                return false;
        //            }
        //
        //            @Override
        //            public boolean onQueryTextChange(String newText) {
        //
        //                return false;
        //            }
        //        });
        //
        //        chartDescTextView.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //                searchView.showSearch();
        //                searchView.showKeyboard(chartDescTextView);
        //            }
        //        });
    }

    private void setDateFilterTextView(String msg) {

        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {

        String filter = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);
        dateFilterDialog = new MaterialDialog.Builder(context).

                title(filter)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))

                .build();

    }

    private void loadIngUI() {
        dateFilterTextView = (TextView) mainLayoutView.findViewById(R.id.date_filter);

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.bar_chart_recycler_view);
        barChart = (BarChart) mainLayoutView.findViewById(R.id.bar_chart);
        //        searchView=(MaterialSearchView)(this.getActivity()).findViewById(R.id.search_view);
        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        //        searchView.setCursorDrawable(attributeResourceId);
        chartDescTextView = (TextView) mainLayoutView.findViewById(R.id.chart_desc_text_view);
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);
        isTwelve = false;
        dateFilterArr = new String[12];
        orderby = "asc";
        //stockID = 0l;//
        startDate = DateUtility.getThisYearStartDate();
        endDate = DateUtility.getThisYearEndDate();

        //        stockBusiness = new StockManager(context);
        //        stockSearchAdapter = new StockSearchAdapter(context, stockBusiness);
        //        searchView.setAdapter(stockSearchAdapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void initializeList() {
        Log.e("initialize", "list");
        if (reportType.equalsIgnoreCase("monthly gross profit by product")) {
            Log.e(startDate, endDate + "d");
            reportManager.createPurchasePriceTempTable(startDate, endDate);
            reportItemList = reportManager.monthlyGrossProfitByProduct(stockID, startDate, endDate, dateFilterArr, orderby);
            reportManager.dropPurchasePriceTempTable();

        } /*else if (reportType.equalsIgnoreCase("monthly gross profit")){
            reportManager.createPurchasePriceTempTable(startDate, endDate);
            reportItemList = reportManager.monthlyGrossProfit(startDate, endDate, dateFilterArr, orderby);
            // Log.e("size", reportItemList.size()+ " ida");
        } else if (reportType.equalsIgnoreCase("monthly purchase transactions")){
            reportItemList = reportManager.monthlyPurchaseTransactions(startDate, endDate, dateFilterArr, orderby);
        }*/
    }

    private void setDatesAndLimits() {
        startDate = Calendar.getInstance().get(Calendar.YEAR) + "0101";
        //Log.e("start", startDate );
        endDate = Calendar.getInstance().get(Calendar.YEAR) + "1231";
        Log.e("start", endDate);
            /*Calendar c = Calendar.getInstance();
            c.set(2016,0,1,0,0,0);
            startTime = c.getTimeInMillis();
            c.set(2017,0,1,0,0,0);
            endTime = c.getTimeInMillis();*/

        int year = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 0; i < 12; i++) {
            dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
        }
        //startLimit = 0;
        //endLimit = 10;
    }

    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        rvAdapterForReport = new RVAdapterForMonthAmtReport(reportItemList);
        recyclerView.setAdapter(rvAdapterForReport);
    }

    public void settingBarChart() {

        // barChart.setDescriptionPosition(Float.parseFloat(XAxis.XAxisPosition.TOP.toString()),Float.parseFloat(YAxis.AxisDependency.RIGHT.toString()));
        barChart.setDrawBorders(false);
        barChart.setPinchZoom(true);
        barChart.setPivotY(10);
        barChart.setDrawBarShadow(false);
        barChart.setMaxVisibleValueCount(30);
        barChart.setDescription("");
        barChart.setDrawGridBackground(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setTouchEnabled(false);
        barChart.animateXY(1400, 1400, Easing.EasingOption.EaseInBack, Easing.EasingOption.EaseInBack);
        barChart.getLegend().setEnabled(false);
        if (POSUtil.isLabelWhite(context)) {
            barChart.getAxisLeft().setTextColor(Color.WHITE);
            barChart.getXAxis().setTextColor(Color.WHITE);
        }
        Legend l = barChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setYOffset(2f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f);
        l.setXOffset(3f);

        configXAxis();

        configYAxis();

        setBarDataToChart();

        barChart.invalidate();
    }

    private void setBarDataToChart() {
        xValues = new ArrayList<>();
        yValList = new ArrayList<>();

        xValues = new ArrayList<String>();
        if (isTwelve) {
            /*int cal=Calendar.getInstance().get(Calendar.MONTH);
            for (int i=0; i<12; i++){
                xValues.add(mMonths[cal]);
                cal-=1;
                if (cal<0){
                    cal=11;
                }

            }*/
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -11);
            int cal = calendar.get(Calendar.MONTH);
            for (int i = 0; i < 12; i++) {
                xValues.add(mMonths[cal]);
                cal += 1;
                if (cal > 11) {
                    cal = 0;
                }

            }
        } else {
            for (int i = 0; i < 12; i++) {
                //Log.e("ccccc",cal+"");
                xValues.add(mMonths[i]);
            }
        }

        for (ReportItem reportItem : reportItemList) {
            //Log.e("ccccc",cal+"");
            yValList.add(reportItem.getBalance());
        }
        //}


        Log.e("x " + xValues.size(), "y " + yValList.size());
        yValues = getYAxisBarEntries(yValList);


        BarData data = configBarData(yValues, xValues);

        barChart.setData(data);
        for (IBarDataSet set : barChart.getData().getDataSets())
            set.setDrawValues(!set.isDrawValuesEnabled());

    }

    private void configYAxis() {
        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setValueFormatter(new DefaultYAxisValueFormatter(1));
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(10f);
        leftAxis.setAxisMinValue(0);
        leftAxis.setLabelCount(5, true);
        barChart.getAxisRight().setEnabled(false);
    }

    private void configXAxis() {
        XAxis xl = barChart.getXAxis();

        xl.setLabelRotationAngle(270);
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setDrawGridLines(false);
        xl.setSpaceBetweenLabels(1);
        //xl.setDrawLabels(false);
    }

    /**
     * @param yValue is data want to add to bar entry.
     * @return bart entry.
     */
    @NonNull
    private List<BarEntry> getYAxisBarEntries(List<Double> yValue) {
        List<BarEntry> yVals1 = new ArrayList<BarEntry>();
        int            j      = 0;
        for (Double t : yValue) {
            Float f = Float.valueOf(t.toString());
            yVals1.add(new BarEntry(f, j));
            ++j;
        }
        return yVals1;
    }

    @NonNull
    private BarData configBarData(List<BarEntry> yVals, List<String> xVals) {
        BarDataSet set1 = new BarDataSet(yVals, "");
        set1.setColors(colorArr);
        List<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(xVals, dataSets);

        // add space between the dataset groups in percent of bar-width
        data.setGroupSpace(80f);

        //mChart.setDescription(calendar.get(Calendar.YEAR) + " " + xValues.get(0) + " - " + xValues.get(11));

        return data;
    }


    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String label = "";

            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {
            settingBarChart();
            configRecyclerView();
            chartDescTextView.setClickable(false);
            if (reportItemList.isEmpty()) {
                chartDescTextView.setText(context.getTheme().obtainStyledAttributes(new int[]{R.attr.there_is_no_searched_result_for_stock}).getString(0));
            } else {
                String gross = context.getTheme().obtainStyledAttributes(new int[]{R.attr.monthly_sales_profit_gross}).getString(0);
                chartDescTextView.setText(chartDesc + "(" + gross + ")");
            }

            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
    //    @Override
    //    public boolean onOptionsItemSelected(MenuItem item) {
    //        // Handle action bar item clicks here. The action bar will
    //        // automatically handle clicks on the Home/Up button, so long
    //        // as you specify a parent activity in AndroidManifest.xml.
    //        int id = item.getItemId();
    //
    //        //noinspection SimplifiableIfStatement
    //
    //        return super.onOptionsItemSelected(item);
    //    }
    //
    //    @Override
    //    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    //
    //        getActivity().getMenuInflater().inflate(R.menu.main, menu);
    //
    //        MenuItem item = menu.findItem(R.id.action_search);
    //
    ////        searchView.setMenuItem(item);
    //
    //        super.onCreateOptionsMenu(menu, inflater);
    //    }
}
