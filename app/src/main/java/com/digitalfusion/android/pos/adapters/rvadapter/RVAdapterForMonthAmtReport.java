package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;

import java.util.List;

/**
 * Created by MD002 on 12/3/16.
 */

public class RVAdapterForMonthAmtReport extends RecyclerView.Adapter<RVAdapterForMonthAmtReport.ReportItemViewHolder> {

    //private List<ReportItem> expenseReportItemViewList;
    protected String[] mMonths = new String[]{
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };
    private List<ReportItem> reportItemList;

    public RVAdapterForMonthAmtReport(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    @Override
    public ReportItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_month_amount, parent, false);

        return new ReportItemViewHolder(v);

    }


    @Override
    public void onBindViewHolder(ReportItemViewHolder holder, final int position) {
        holder.monthTextView.setText(mMonths[position]);

        Log.e("bal", reportItemList.get(position).getBalance() + "doajf");
        holder.amtTextView.setText(Double.toString(reportItemList.get(position).getBalance()));
        //holder.expenseTextView.setText(Double.toString(expenseReportItemViewList.get(position).getBalance()));
    }

    @Override
    public int getItemCount() {
        return reportItemList.size();
    }

    public List<ReportItem> getReportItemList() {
        return reportItemList;
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public class ReportItemViewHolder extends RecyclerView.ViewHolder {
        TextView monthTextView;
        TextView amtTextView;
        //TextView expenseTextView;

        View view;

        public ReportItemViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            monthTextView = (TextView) itemView.findViewById(R.id.month_text_view);

            amtTextView = (TextView) itemView.findViewById(R.id.amount_text_view);

            //expenseTextView = (TextView) itemView.findViewById(R.id.expense_text_view);

        }

    }

}
