package com.digitalfusion.android.pos.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MD002 on 1/23/18.
 */

public final class DateMapping {
    private static DateMapping instance;
    private HashMap<Character, Character> dateHashMap;
    private String inputString;

    private DateMapping() {
        dateHashMap = new HashMap<>();
        dateHashMap.put('h', '0');
        dateHashMap.put('n', '1');
        dateHashMap.put('i', '2');
        dateHashMap.put('s', '4');
        dateHashMap.put('u', '5');
        dateHashMap.put('a', '6');
        dateHashMap.put('t', '7');
        dateHashMap.put('r', '8');
        dateHashMap.put('k', '9');
        dateHashMap.put('y', '/');
        dateHashMap.put('w', '3');
    }

    public static DateMapping getInstance() {
        if (instance == null) {
            instance = new DateMapping();
        }
        return instance;
    }

    public String getKeys(String value) {
        String key = "";
        for (Character v : value.toCharArray()) {
            for (Map.Entry m : dateHashMap.entrySet()) {
                if (m.getValue() == v) {
                    key += m.getKey();
                    break;
                }
            }
        }
        return key;
    }

    public String getValue(String key) {
        String value = "";
        for (Character k : key.toCharArray()) {
            if (dateHashMap.containsKey(k)) {
                value += dateHashMap.get(k);
            } else {
                return "wrong";
            }
        }
        return value;
    }
}
