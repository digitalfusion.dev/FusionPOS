package com.digitalfusion.android.pos.fragments.inventoryfragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForCategoryList;
import com.digitalfusion.android.pos.database.business.CategoryManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.fragments.settingfragment.AddEditCategoryFragment;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.views.FusionToast;

import java.util.List;

/**
 * Created by MD003 on 8/24/16.
 */
public class CategoryFragmentDialog extends Fragment {

    TypedArray category;
    TypedArray categoryAddedSuccessfully;
    TypedArray cancel;
    TypedArray save;
    TypedArray addCategory;
    TypedArray editCategory_TA;
    TypedArray pleaseEnterName;
    TypedArray categoryAlreadyExist;
    TypedArray areUSureWantToDelete;
    private View mainLayoutView;
    private EditText categoryNameTextInputEditText;
    private EditText categoryDescTextInputEditText;
    private TextInputLayout categoryDescTextInputLayout;
    private MDButton categoryAddButton;
    private MDButton categoryCancelButton;
    private RecyclerView recyclerView;
    private MaterialDialog categoryMaterialDialog;
    private FloatingActionButton addCategoryFab;
    private EditText editCategoryNameTextInputEditText;
    private EditText editCategoryDescTextInputEditText;
    private TextInputLayout editCategoryDescTextInputLayout;
    private MDButton editCategoryAddButton;
    private MDButton editCategoryCancelButton;
    private MaterialDialog editCategoryMaterialDialog;
    private FloatingActionButton editSaveCategoryFab;
    private RVSwipeAdapterForCategoryList rvSwipeAdapterForCategoryList;
    private List<Category> categoryList;
    private Context context;
    private CategoryManager categoryManager;
    private String categoryName;
    private String categoryDesc;
    private Long editcategoryID;
    private Category editCategory;
    private MaterialDialog deleteAlertDialog;
    private Button yesDeleteMdButton;
    private int deletePos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.category, null);

        context = getContext();

        category = context.getTheme().obtainStyledAttributes(new int[]{R.attr.category});

        categoryAddedSuccessfully = context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_category_added_successfully});

        cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        addCategory = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_category});

        editCategory_TA = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_category});

        pleaseEnterName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_name});

        categoryAlreadyExist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.category_already_exist});

        areUSureWantToDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete});


        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(category.getString(0));

        categoryManager = new CategoryManager(context);
        categoryList = categoryManager.getAllCategories();
        buildingCategoryDialog();
        buildingEditCategoryDialog();
        loadUI();
        configRecyclerView();
        buildDeleteAlertDialog();
        MainActivity.setCurrentFragment(this);

        addCategoryFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_CATEGORY);

                startActivity(addCurrencyIntent);

                //clearInput();
                // categoryMaterialDialog.show();

            }
        });

        yesDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean status = categoryManager.deleteCategory(categoryList.get(deletePos).getId());

                if (status) {
                    categoryList.remove(deletePos);

                    rvSwipeAdapterForCategoryList.setCategoryList(categoryList);

                    rvSwipeAdapterForCategoryList.notifyItemRemoved(deletePos);

                    rvSwipeAdapterForCategoryList.notifyItemRangeChanged(deletePos, categoryList.size());

                    deleteAlertDialog.dismiss();

                }
            }
        });

        rvSwipeAdapterForCategoryList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
               /* editCategory=categoryList.get(postion);
                setInput();
                editCategoryMaterialDialog.show();*/

                Bundle bundle = new Bundle();

                bundle.putSerializable(AddEditCategoryFragment.KEY, categoryList.get(postion));

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtras(bundle);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_CATEGORY);

                startActivity(addCurrencyIntent);
            }
        });

        rvSwipeAdapterForCategoryList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                if (categoryList.get(postion).getId() != 1) {
                    deletePos = postion;
                    deleteAlertDialog.show();

                }
            }
        });

        categoryAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkCategoryValidation()) {
                    categoryManager.addNewCategory(categoryName, categoryDesc, null, 0);
                    refreshCategoryList();

                    categoryMaterialDialog.dismiss();
                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);

                }
            }
        });

        editCategoryAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if(checkEditCategoryValidation()){
                    if(categoryManager.updateCategory(categoryName,categoryDesc,editcategoryID)){
                        refreshCategoryList();
                        editCategoryMaterialDialog.dismiss();
                        POSUtil.showSnackBar(mainLayoutView,"Category Edited Successfully");
                    }

                }*/

            }
        });

        return mainLayoutView;
    }


    @Override
    public void onResume() {
        super.onResume();

        refreshCategoryList();
    }

    private void buildingCategoryDialog() {

        categoryMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.add_category, true)
                .negativeText(cancel.getString(0))
                .positiveText(save.getString(0))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .title(addCategory.getString(0)).build();

    }

    private void buildingEditCategoryDialog() {

        editCategoryMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.add_category, true)
                .negativeText(cancel.getString(0))
                .positiveText(save.getString(0))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .title(editCategory_TA.getString(0)).build();

    }

    public void refreshCategoryList() {
        categoryList = categoryManager.getAllCategories();
        rvSwipeAdapterForCategoryList.setCategoryList(categoryList);
        rvSwipeAdapterForCategoryList.notifyDataSetChanged();
    }


    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        rvSwipeAdapterForCategoryList = new RVSwipeAdapterForCategoryList(categoryList);
        recyclerView.setAdapter(rvSwipeAdapterForCategoryList);

    }

    private void clearInput() {
        categoryDescTextInputEditText.setText(null);
        categoryNameTextInputEditText.setText(null);

    }

    private void setInput() {
        editCategoryDescTextInputEditText.setText(editCategory.getDescription());
        editCategoryNameTextInputEditText.setText(editCategory.getName());
        editcategoryID = editCategory.getId();
        editCategoryNameTextInputEditText.setError(null);

    }

    private boolean checkCategoryValidation() {
        boolean status = true;

        if (categoryNameTextInputEditText.getText().toString().trim().length() < 1) {

            categoryNameTextInputEditText.setError(pleaseEnterName.getString(0));

            status = false;
        } else {

            categoryName = categoryNameTextInputEditText.getText().toString().trim();
            categoryDesc = categoryDescTextInputEditText.getText().toString().trim();
            if (categoryManager.checkNameExist(categoryName)) {
                status = false;
                categoryNameTextInputEditText.setError(categoryAlreadyExist.getString(0));
            }
        }

        return status;

    }

    private boolean checkEditCategoryValidation() {
        boolean status = true;
        if (editCategoryNameTextInputEditText.getText().toString().trim().length() < 1) {

            editCategoryNameTextInputEditText.setError(pleaseEnterName.getString(0));

            status = false;
        } else {

            categoryName = editCategoryNameTextInputEditText.getText().toString().trim();
            categoryDesc = editCategoryDescTextInputEditText.getText().toString().trim();
            if (categoryManager.checkNameExist(categoryName) && !editCategory.getName().trim().equals(categoryName)) {
                status = false;
                editCategoryNameTextInputEditText.setError(categoryAlreadyExist.getString(0));
            }
        }

        return status;
    }

    public void loadUI() {

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.category_list_rv);
        categoryNameTextInputEditText = (EditText) categoryMaterialDialog.findViewById(R.id.category_name_in_category_add_dg);
        categoryDescTextInputEditText = (EditText) categoryMaterialDialog.findViewById(R.id.category_desc_in_category_add_dg);
        categoryAddButton = categoryMaterialDialog.getActionButton(DialogAction.POSITIVE);
        categoryCancelButton = categoryMaterialDialog.getActionButton(DialogAction.NEGATIVE);
        addCategoryFab = (FloatingActionButton) mainLayoutView.findViewById(R.id.add_new_category);

        editCategoryNameTextInputEditText = (EditText) editCategoryMaterialDialog.findViewById(R.id.category_name_in_category_add_dg);
        editCategoryDescTextInputEditText = (EditText) editCategoryMaterialDialog.findViewById(R.id.category_desc_in_category_add_dg);
        editCategoryAddButton = editCategoryMaterialDialog.getActionButton(DialogAction.POSITIVE);
        editCategoryCancelButton = editCategoryMaterialDialog.getActionButton(DialogAction.NEGATIVE);
        editSaveCategoryFab = (FloatingActionButton) mainLayoutView.findViewById(R.id.add_new_category);

    }

    private void buildDeleteAlertDialog() {

        TypedArray no  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        TextView textView = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(areUSureWantToDelete.getString(0));

        yesDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });

    }


}