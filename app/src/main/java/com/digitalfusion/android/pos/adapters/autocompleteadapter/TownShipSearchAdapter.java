package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 1/17/18.
 */

public class TownShipSearchAdapter extends ArrayAdapter<String> {


    List<String> searchList;

    List<String> suggestion;

    List<String> templist;


    int lenght = 0;

    String queryText = "";


    Context context;

    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {

            String str = (String) resultValue;

            return str;

        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            Log.w("HERER", "Filtering");

            if (constraint != null && !constraint.equals("")) {

                templist = new ArrayList<>();

                queryText = constraint.toString();

                lenght = constraint.length();


                for (String s : searchList) {

                    if (s.startsWith(constraint.toString())) {
                        Log.w("HERER", "Equal");
                        templist.add(s);
                    }
                }

                FilterResults filterResults = new FilterResults();

                filterResults.values = templist;

                filterResults.count = templist.size();

                Log.w("here", "SIZE" + templist.size());

                return filterResults;
            } else {

                return new FilterResults();

            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            List<String> filterList = (ArrayList<String>) results.values;

            suggestion = templist;

            if (results != null && results.count > 0) {

                clear();

                for (String expenseIncomeView : filterList) {

                    add(expenseIncomeView);

                    notifyDataSetChanged();

                }

            }

        }

    };

    public TownShipSearchAdapter(Context context, List<String> searchList) {

        super(context, -1);

        this.context = context;

        suggestion = new ArrayList<>();

        this.searchList = searchList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.town_search_item_view, parent, false);

        }

        viewHolder = new ViewHolder(convertView);

        if (suggestion.size() > 0 && !suggestion.isEmpty()) {
            if (suggestion.get(position).toLowerCase().startsWith(queryText.toLowerCase())) {

                Spannable spanText = new SpannableString(suggestion.get(position));

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.townShipTextView.setText(spanText);

            } else {
                viewHolder.townShipTextView.setText(suggestion.get(position));
            }
            // viewHolder.customerNameTextView.setText(salesHistoryViewList.get(position).getItemName());
        }


        return convertView;

    }

    @Override
    public int getCount() {
        return suggestion.size();
    }

    public List<String> getSuggestion() {

        return suggestion;

    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }


    static class ViewHolder {

        TextView townShipTextView;


        public ViewHolder(View itemView) {

            this.townShipTextView = (TextView) itemView.findViewById(R.id.town_ship);

        }

    }

}