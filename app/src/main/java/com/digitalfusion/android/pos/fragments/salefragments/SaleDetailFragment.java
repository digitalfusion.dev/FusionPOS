package com.digitalfusion.android.pos.fragments.salefragments;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAPrintPreviewAdapter;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.business.SalesOrderManager;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.model.BusinessSetting;
import com.digitalfusion.android.pos.database.model.SaleDelivery;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.database.model.SalesHeader;
import com.digitalfusion.android.pos.util.AidlUtil;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.BluetoothUtil;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.GrantPermission;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.PrintImage;
import com.digitalfusion.android.pos.util.PrintInvoice;
import com.digitalfusion.android.pos.util.PrintViewSlip;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.util.TypefaceSpan;
import com.digitalfusion.android.pos.views.FolderChooserDialog;
import com.digitalfusion.android.pos.views.FusionToast;
import com.itextpdf.text.DocumentException;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.prologic.printapi.PrintCanvas;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.widget.Toast.LENGTH_SHORT;
import static com.digitalfusion.android.pos.util.AppConstant.DEFAULT_CUSTOMER;

/**
 * Created by MD003 on 9/7/16.
 */
public class SaleDetailFragment extends Fragment implements FolderChooserDialog.FolderSelectionCallback {

    public static final String KEY = "saleID";
    public static final String PRINTKEY = "Print";
    private final static char GS = 0x1D;
    private final static byte[] CUT_PAPER = new byte[]{GS, 0x56, 0x00};
    private static final int REQUEST_CONNECT_AND_PRINT_DEVICE = 1;
    private static final int REQUEST_CONNECT_DEVICE = 3;
    private static final int REQUEST_ENABLE_BT = 2;
    public static String EXTRA_DEVICE_ADDRESS = "device_address";
    private static BluetoothSocket mmSocket;
    private static BluetoothDevice device;
    private static String print;
    public TextView printDeviceNameTextView;
    OutputStream outputStream;
    KProgressHUD printHud;
    KProgressHUD connectHud;
    MaterialDialog fileOverrideMaterialDialog;
    KProgressHUD saveHud;
    private View mainLayout;
    //UI components from main
    private View deliverDashLine;
    private RecyclerView saleItemRecyclerView;
    private TextView saleIdTextView;
    private TextView dateTextView;
    private TextView discountTextView;
    private TextView subtotalTextView;
    private TextView totalTextView;
    private TextView deliveryAddress;
    private LinearLayout deliveryAddressLinearLayout;
    private LinearLayout customerPhoneLayout;
    private LinearLayout customerAddressLayout;
    private TextView deliverChargesTextView;
    private TextView customerNameTextView;
    private TextView customerPhoneTextView;
    private TextView customerAddressTextView;
    private TextView taxAmtTextView;
    private TextView taxRateTextView;
    private TextView taxTypeTextView;
    private TextView businessNameTextView;
    private TextView addressTextView;
    private TextView phoneTextView;
    private TextView paidAmtTextView;
    private TextView headerTextView;
    private TextView footerTextView;
    private TextView changeBalanceTextView; // change_balance textView

    //Business
    private SalesManager salesManager;
    //value
    private Context context;
    private RVAPrintPreviewAdapter rvAdapterForStockItemInSaleAndPurchaseDetail;
    private List<SalesAndPurchaseItem> salesAndPurchaseItemList;
    private BluetoothAdapter mBluetoothAdapter = null;
    //Values
    private String saleID;
    //  private Double totalAmount=0.0;
    // private Double voucherDiscount=0.0;
    //private Double voucherTax=0.0;
    private Double subtotal = 0.0;
    private Calendar now;
    private int saleDay;
    private int saleMonth;
    private int saleYear;
    private String date;

    // private DeliveryAndPickUpDialogFragment deliveryAndPickUpDialog;
    private Long saleId;
    private SaleDelivery saleDelivery;
    private SalesOrderManager salesOrderManager;
    private SalesHeader salesHeader;
    private Double deliveryCharges = 0.0;
    private BusinessSetting businessSetting;
    private SettingManager settingManager;
    private TextView statusTextView;
    private boolean isPrint = false;
    private BluetoothUtil bluetoothUtil;
    private PrintViewSlip printViewSlip;
    private FolderChooserDialog folderChooserDialog;
    private MaterialDialog fileNameMaterialDialog;
    private EditText fileNameEditText;
    private String headerText;
    private String footerText;
    private String header;
    private GrantPermission grantPermission;
    private boolean directPrint;
    private Button savePdfButton;
    private String path;
    private String fileName = "";
    private Double paidAmount;
    private  String customerPhNo;
    private String customerAddress;
    private String customerName;

    private static int colorToRGB(int alpha, int red, int green, int blue) {
        int newPixel = 0;
        newPixel += alpha;
        newPixel = newPixel << 8;
        newPixel += red;
        newPixel = newPixel << 8;
        newPixel += green;
        newPixel = newPixel << 8;
        newPixel += blue;

        return newPixel;
    }

    public static Bitmap grayScaleImage(Bitmap src) {
        // constant factors
        final double GS_RED = 0.299;
        final double GS_GREEN = 0.587;
        final double GS_BLUE = 0.114;

        // create output bitmap
        Bitmap bmOut = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
        // pixel information
        int A, R, G, B;
        int pixel;

        // get image size
        int width = src.getWidth();
        int height = src.getHeight();

        // scan through every single pixel
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                // get one pixel color
                pixel = src.getPixel(x, y);
                // retrieve color of all channels
                A = Color.alpha(pixel);
                R = Color.red(pixel);
                G = Color.green(pixel);
                B = Color.blue(pixel);
                // take conversion up to one single value
                R = G = B = (int) (GS_RED * R + GS_GREEN * G + GS_BLUE * B);
                // set new pixel color to output bitmap
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }

        // return final image
        return bmOut;
    }

    static public Bitmap createBitmapFromText(String printText, int textSize, int printWidth, Typeface typeface) {
        Paint paint = new Paint();
        Bitmap bitmap;
        Canvas canvas;
        paint.setTextSize(textSize);
        paint.setTypeface(typeface);
        paint.setColor(Color.BLACK);

        paint.getTextBounds(printText, 0, printText.length(), new Rect());

        TextPaint textPaint = new TextPaint(paint);
        android.text.StaticLayout staticLayout = new StaticLayout(printText, textPaint, printWidth, Layout.Alignment.ALIGN_NORMAL, 1, 0, false);

        // Create bitmap
        bitmap = Bitmap.createBitmap(staticLayout.getWidth(), staticLayout.getHeight(), Bitmap.Config.ARGB_8888);

        // Create canvas
        canvas = new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
        canvas.translate(0, 0);
        staticLayout.draw(canvas);

        return bitmap;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainLayout = inflater.inflate(R.layout.print_preview, null);
        salesAndPurchaseItemList = new ArrayList<>();

        setHasOptionsMenu(true);
        MainActivity.setCurrentFragment(this);

        context = getContext();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothUtil = BluetoothUtil.getInstance(this);
        settingManager = new SettingManager(context);
        businessSetting = settingManager.getBusinessSetting();

        TypedArray saleDetail = context.getTheme().obtainStyledAttributes(new int[]{R.attr.sale_detail});
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(saleDetail.getString(0));

        salesManager = new SalesManager(context);
        salesOrderManager = new SalesOrderManager(context);
        saleId = getArguments().getLong(KEY);
        directPrint = getArguments().getBoolean(PRINTKEY, false);
        now = Calendar.getInstance();
        grantPermission = new GrantPermission(this);

        initializeOldData();
        loadUI();
        buildAndConfigDialog();
        configUI();
        configRecyclerView();
        updateViewData();

        mainLayout.findViewById(R.id.blue_toothLL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isPrint = false;

                createBluetoothConnection();

            /*  isPrint=false;
                if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    getActivity().startActivityForResult(enableIntent, 2);
                    // Otherwise, setup the chat session
                }else {
                    Intent serverIntent = null;
                    serverIntent = new Intent(getContext(), BluetoothDeviceNearByList.class);
                    //startActivity(serverIntent);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                }*/
            }
        });

        //  print();

        bluetoothUtil.setBluetoothConnectionListener(new BluetoothUtil.JobListener() {

            @Override
            public void onPrepare() {

                connectHud.show();
            }

            @Override
            public void onSuccess() {

                Log.w("on Success", "on success");

                if (connectHud.isShowing()) {

                    connectHud.dismiss();

                }

                printDeviceNameTextView.setText(bluetoothUtil.getDevice().getName());


                if (bluetoothUtil.isConnected()) {

                    statusTextView.setTextColor(Color.parseColor("#2E7D32"));
                    String connected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.connected}).getString(0);

                    statusTextView.setText(connected);

                } else {

                    statusTextView.setTextColor(Color.parseColor("#DD2C00"));
                    String nonConnected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);

                    statusTextView.setText(nonConnected);

                }

                Log.e("is ori", isPrint + " dfa");
                if (isPrint) {

                    try {
                        //printViewSlip.printNow(bluetoothUtil.getOutputStream());
                        preparePrint();
                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }

            }

            @Override
            public void onFailure() {
                String noDevice = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_device}).getString(0);
                String notConnected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);
                String toast = context.getTheme().obtainStyledAttributes(new int[]{R.attr.make_sure_bluetooth_turn_on_both_devices_and_choose_printer_only}).getString(0);
                statusTextView.setTextColor(Color.parseColor("#DD2C00"));
                printDeviceNameTextView.setText(noDevice);

                statusTextView.setText(notConnected);

                FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.ERROR);

                if (connectHud.isShowing()) {

                    connectHud.dismiss();

                }

            }

            @Override
            public void onDisconnected() {

                String conLost = context.getTheme().obtainStyledAttributes(new int[]{R.attr.connection_lost}).getString(0);

                String noDevice = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_device}).getString(0);

                String notConnected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);

                //FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.ERROR);

                printDeviceNameTextView.setText(noDevice);

                statusTextView.setTextColor(Color.parseColor("#DD2C00"));

                statusTextView.setText(notConnected);


            }
        });


/*        printViewSlip.setPrintListener(new PrintViewSlip.PrintListener() {
            @Override
            public void onPrepare() {
                printHud.show();
            }

            @Override
            public void onSuccess() {

                if(printHud.isShowing()){

                    printHud.dismiss();

                }

            }

            @Override
            public void onFailure() {

                String toast= context.getTheme().obtainStyledAttributes(new int[]{R.attr.make_sure_bluetooth_turn_on_both_devices_and_choose_printer_only}).getString(0);
                DisplayToast(toast);

                if(printHud.isShowing()){

                    printHud.dismiss();

                }

            }
        });*/

        return mainLayout;
    }

    public void initializeOldData() {


        salesHeader = salesManager.getSalesHeaderView(saleId);

        saleID = salesHeader.getSaleVocherNo();

        //        Log.w("tota in details ",salesHeader.getTotal().toString());

        salesAndPurchaseItemList = salesManager.getSaleDetailsBySalesID(saleId);

        saleDay = Integer.parseInt(salesHeader.getDay());

        saleMonth = Integer.parseInt(salesHeader.getMonth());

        saleYear = Integer.parseInt(salesHeader.getYear());

        now.set(saleYear, saleMonth - 1, saleDay);

        if (salesHeader.getType().equalsIgnoreCase(SalesManager.SaleType.Delivery.toString())) {

            saleDelivery = salesManager.getDeliveryInfoBySalesID(salesHeader.getId(), new InsertedBooleanHolder());

            deliveryCharges = saleDelivery.getCharges();

        }

    }

    public void DisplayToast(String str) {
        if (getContext() != null) {


            SpannableString s = new SpannableString(str.toString());
            s.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            Toast toast = Toast.makeText(getContext(), s, LENGTH_SHORT);
            //����toast��ʾ��λ��
            toast.setGravity(Gravity.BOTTOM, 0, 100);
            //��ʾ��Toast
            toast.show();
        }

    }//

    public void buildAndConfigDialog() {


        String wait = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        String saving = context.getTheme().obtainStyledAttributes(new int[]{R.attr.saving_as_pdf}).getString(0);
        String file = context.getTheme().obtainStyledAttributes(new int[]{R.attr.file_name}).getString(0);

        View processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
        ImageView imageView = (ImageView) processDialog.findViewById(R.id.image_view);
        imageView.setBackgroundResource(R.drawable.spin_animation);
        AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
        drawable.start();

        //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        saveHud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setDetailsLabel(saving)
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);

        fileNameMaterialDialog = new MaterialDialog.Builder(getContext())
                .customView(R.layout.file_name, false)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .title(file)
                .build();

        fileNameEditText = (EditText) fileNameMaterialDialog.findViewById(R.id.file_name);

        savePdfButton = (Button) fileNameMaterialDialog.findViewById(R.id.save);

        savePdfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fileNameEditText.length() > 0) {
                    Log.e("file", fileNameEditText.getText().toString());
                    String npath = path + "/" + fileNameEditText.getText().toString() + ".pdf";

                    fileName = fileNameEditText.getText().toString();

                    Log.e("f", npath);
                    File file = new File(npath);
                    // If file does not exists, then create it
                    if (!file.exists()) {
                        new SavePDFProgress().execute(path, fileName);
                        fileNameMaterialDialog.dismiss();
                    } else {
                        fileNameMaterialDialog.dismiss();
                        fileOverrideMaterialDialog.show();
                    }
                } else {
                    String fillFName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_fill_file_name}).getString(0);
                    fileNameEditText.setError(fillFName);
                }


            }
        });

        fileNameMaterialDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileNameMaterialDialog.dismiss();
            }
        });

        fileOverrideMaterialDialog = new MaterialDialog.Builder(getContext())
                .customView(R.layout.confirm_dialog, false)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                // .title("File name already exist.Do you want to override it?")
                .build();

        String exist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.file_name_already_exist_do_u_want_to_override_it}).getString(0);
        ((TextView) fileOverrideMaterialDialog.findViewById(R.id.title)).setText(exist);

        fileOverrideMaterialDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileOverrideMaterialDialog.dismiss();
                fileNameMaterialDialog.show();
            }
        });

        fileOverrideMaterialDialog.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SavePDFProgress().execute(path);
                fileNameMaterialDialog.dismiss();
            }
        });


    }

    private void showFolderChooserDialog() {
        if (folderChooserDialog == null) {
            folderChooserDialog = new FolderChooserDialog();
            folderChooserDialog.setmCallback(this);
        }
        folderChooserDialog.show(getFragmentManager(), "folderChooser");
    }

    private void print() {

        printViewSlip = new PrintViewSlip();

        if (!businessSetting.isEmptyBusinessName()) {

            printViewSlip.addString(businessSetting.getBusinessName(), PrintViewSlip.Alignment.CENTER);

            printViewSlip.nextLine();

        }

        if (!businessSetting.isEmptyAddress()) {

            printViewSlip.addString(businessSetting.getAddress(), PrintViewSlip.Alignment.CENTER);

            printViewSlip.nextLine();
        }
        if (!businessSetting.isEmptyPhoneNo()) {

            printViewSlip.addString(businessSetting.getPhoneNo(), PrintViewSlip.Alignment.CENTER);

            printViewSlip.nextLine();

        }

        printViewSlip.nextLine();

        String sale = context.getTheme().obtainStyledAttributes(new int[]{R.attr.sale}).getString(0);
        printViewSlip.addString(PrintViewSlip.STYLE.BOLD, sale);

        printViewSlip.addSpace(5);

        printViewSlip.addFullColon();

        printViewSlip.addString(PrintViewSlip.STYLE.BOLD, "#");

        printViewSlip.addString(PrintViewSlip.STYLE.BOLD, salesHeader.getSaleVocherNo());

        printViewSlip.addString(DateUtility.makeDateFormatWithSlash(salesHeader.getSaleDate()), PrintViewSlip.Alignment.RIGHT);

        printViewSlip.nextLine();

        String customer = context.getTheme().obtainStyledAttributes(new int[]{R.attr.customer}).getString(0);
        printViewSlip.addString(customer + " ");

        printViewSlip.addFullColon();

        printViewSlip.addString(salesHeader.getCustomerName());

        printViewSlip.nextLine();

        if (salesHeader.getType().equalsIgnoreCase(SalesManager.SaleType.Delivery.toString())) {

            printViewSlip.addDividerLine();

            printViewSlip.nextLine();

            String shipTo = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ship_to}).getString(0);
            printViewSlip.addString(shipTo + " :");

            printViewSlip.addString(saleDelivery.getAddress());


        }


        printViewSlip.BuildTable(3);

        String no = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no}).getString(0);
        String itemName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.item_name}).getString(0);
        String price = context.getTheme().obtainStyledAttributes(new int[]{R.attr.price}).getString(0);

        printViewSlip.addNewColumn(no, 5, PrintViewSlip.Alignment.CENTER, 0);

        printViewSlip.addNewColumn(itemName, 30, PrintViewSlip.Alignment.LEFT, 5);

        // printViewSlip.addNewColumn("Price",10, PrintViewSlip.Alignment.RIGHT);

        printViewSlip.addNewColumn(price, 13, PrintViewSlip.Alignment.RIGHT, 35);

        printViewSlip.addHeader();

        int i = 1;

        for (SalesAndPurchaseItem s : salesAndPurchaseItemList) {
            printViewSlip.addRow(Integer.toString(i), s.getItemName() + " x " + s.getQty(), POSUtil.NumberFormat(s.getTotalPrice()));
            i++;
        }

        printViewSlip.addDividerLine();
        String subTotal = context.getTheme().obtainStyledAttributes(new int[]{R.attr.subtotal}).getString(0);
        String tax = context.getTheme().obtainStyledAttributes(new int[]{R.attr.tax}).getString(0);
        String discount = context.getTheme().obtainStyledAttributes(new int[]{R.attr.discount}).getString(0);
        String total = context.getTheme().obtainStyledAttributes(new int[]{R.attr.total}).getString(0);
        String paidAmt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.paid_amount}).getString(0);
        String thankU = context.getTheme().obtainStyledAttributes(new int[]{R.attr.thank_you}).getString(0);

        printViewSlip.addRow("", "                    " + subTotal + " :", POSUtil.NumberFormat(salesHeader.getSubtotal()));

        //don't use  in this version for upgrade version
        // if(salesHeader.getFeedbackType().equalsIgnoreCase(SalesManager.SaleType.Delivery.toString())){

        //  printViewSlip.addRow("","Delivery :", POSUtil.NumberFormat(salesHeader.getDiscountAmt()));

        //  }

        // printViewSlip.addRow("","","Delivery :",POSUtil.NumberFormat(salesHeader.getSubtotal()));

        printViewSlip.addRow("", "                    " + tax + "      :", POSUtil.NumberFormat(salesHeader.getPaidAmt()));

        printViewSlip.addRow("", "                    " + discount + " :", POSUtil.NumberFormat(salesHeader.getDiscountAmt()));

        printViewSlip.addDividerLine();

        printViewSlip.addRow(PrintViewSlip.STYLE.BIG, "", "                    " + total + "    :", POSUtil.NumberFormat(salesHeader.getTotal()));

        printViewSlip.addRow(PrintViewSlip.STYLE.BIG, "", "              " + paidAmt + "    :", POSUtil.NumberFormat(salesHeader

                .getPaidAmt()));

        printViewSlip.addDividerLine();

        printViewSlip.nextLine();

        printViewSlip.addStringBold(thankU, PrintViewSlip.Alignment.CENTER);

        printViewSlip.nextLine();

        printViewSlip.nextLine();

        printViewSlip.nextLine();

        printViewSlip.nextLine();

        printViewSlip.nextLine();


        //printViewSlip.addHeader("No.",);


    }

    public void configUI() {


        paidAmount = getpaidAmount(salesHeader.getTotal(), salesHeader.getChangeBalance());


        header = context.getTheme().obtainStyledAttributes(new int[]{R.attr.header_text}).getString(0);

        String temp_header = POSUtil.getHeaderTxt(getContext());
        String temp_footer = POSUtil.getFooterTxt(getContext());
        if (temp_header.equalsIgnoreCase(header)) {
            headerText = "";
            headerTextView.setVisibility(View.GONE);
        } else {
            headerText = temp_header;
            headerTextView.setText(temp_header);
            headerTextView.setVisibility(View.VISIBLE);
        }
        if (temp_footer.equalsIgnoreCase("-1")) {
            footerText = "";
            footerTextView.setText("");
        } else {
            footerText = temp_footer;
            footerTextView.setText(temp_footer);
        }

        if(salesHeader.getChangeBalance().equalsIgnoreCase("0"))
        {
            mainLayout.findViewById(R.id.change_money_layout).setVisibility(View.GONE);
        }
        if (salesHeader.getTaxAmt() == 0) {
            taxAmtTextView.setVisibility(View.GONE);
            mainLayout.findViewById(R.id.txt_).setVisibility(View.GONE);
            mainLayout.findViewById(R.id.txt_label).setVisibility(View.GONE);
        }
        if(salesHeader.getDiscountAmt() == 0)
        {
            discountTextView.setVisibility(View.GONE);
            mainLayout.findViewById(R.id.discount_label).setVisibility(View.GONE);
            mainLayout.findViewById(R.id.txt_discount).setVisibility(View.GONE);
        }

        if (bluetoothUtil.isConnected()) {
            // printDeviceNameTextView.setTextColor(Color.parseColor("#2E7D32"));
            printDeviceNameTextView.setText(bluetoothUtil.getDevice().getName());
            statusTextView.setTextColor(Color.parseColor("#2E7D32"));
            statusTextView.setText("Connected");

        } else {
            // printDeviceNameTextView.setTextColor(Color.parseColor("#DD2C00"));
            String notConnected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);
            String noDevice = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_device}).getString(0);

            printDeviceNameTextView.setText(noDevice);
            statusTextView.setTextColor(Color.parseColor("#DD2C00"));
            statusTextView.setText(notConnected);
        }


        if (businessSetting.isEmptyBusinessName()) {

            businessNameTextView.setVisibility(View.GONE);

        } else {

            businessNameTextView.setText(businessSetting.getBusinessName());

        }

        if (businessSetting.isEmptyAddress()) {

            addressTextView.setVisibility(View.GONE);

        } else {

            addressTextView.setText(businessSetting.getAddress());

        }
        if (businessSetting.isEmptyPhoneNo()) {

            phoneTextView.setVisibility(View.GONE);

        } else {

            phoneTextView.setText(businessSetting.getPhoneNo());

        }

        saleIdTextView.setText(saleID);

        taxAmtTextView.setText(POSUtil.NumberFormat(salesHeader.getTaxAmt()));

        taxRateTextView.setText(Double.toString(salesHeader.getTaxRate()));

        taxTypeTextView.setText(salesHeader.getTaxType());

        customerNameTextView.setText(salesHeader.getCustomerName());

        customerPhoneTextView.setText(customerPhNo);

        customerAddressTextView.setText(customerAddress);

        paidAmtTextView.setText(POSUtil.NumberFormat(paidAmount));

        changeBalanceTextView.setText(salesHeader.getChangeBalance());

        if (salesHeader.getType().equalsIgnoreCase(SalesManager.SaleType.Delivery.toString())) {

            deliveryAddressLinearLayout.setVisibility(View.VISIBLE);

            deliveryAddress.setText(saleDelivery.getAddress());


        } else {

            deliveryAddressLinearLayout.setVisibility(View.GONE);

            deliverDashLine.setVisibility(View.GONE);

        }

        configDateUI();
    }

    private Double getpaidAmount(Double total, String changeBalance) {
        Double paintAmt;

        if (total > salesHeader.getPaidAmt()) {
            paintAmt = salesHeader.getPaidAmt();
        } else {
            paintAmt = total + Double.parseDouble(changeBalance);
        }
        return paintAmt;
    }

    public void configDateUI() {

        date = DateUtility.makeDateFormatWithSlash(Integer.toString(saleYear), Integer.toString(saleMonth), Integer.toString(saleDay));
        dateTextView.setText(date);
    }

    public void configRecyclerView() {

        rvAdapterForStockItemInSaleAndPurchaseDetail = new RVAPrintPreviewAdapter(salesAndPurchaseItemList);

        saleItemRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        saleItemRecyclerView.setAdapter(rvAdapterForStockItemInSaleAndPurchaseDetail);
    }

    private void loadUI() {

        String pleaseWait = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        String printing = context.getTheme().obtainStyledAttributes(new int[]{R.attr.printing}).getString(0);
        String connecting = context.getTheme().obtainStyledAttributes(new int[]{R.attr.connecting}).getString(0);

        paidAmtTextView = (TextView) mainLayout.findViewById(R.id.paid_amt);

        changeBalanceTextView = (TextView) mainLayout.findViewById(R.id.paid_change_amt);

        SpannableString pleaseWaitTypeface = new SpannableString(pleaseWait.toString());
        pleaseWaitTypeface.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, pleaseWait.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        SpannableString printingTypeface = new SpannableString(printing.toString());
        printingTypeface.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, printing.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        SpannableString connectingTypeface = new SpannableString(connecting.toString());
        connectingTypeface.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, connecting.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        View processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
        ImageView imageView = (ImageView) processDialog.findViewById(R.id.image_view);
        imageView.setBackgroundResource(R.drawable.spin_animation);
        AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
        drawable.start();

        //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        printHud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);
        //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        connectHud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);

        statusTextView = (TextView) mainLayout.findViewById(R.id.bt_status);
        printDeviceNameTextView = (TextView) mainLayout.findViewById(R.id.device_name);
        deliverDashLine = mainLayout.findViewById(R.id.deliver_dash_line);
        businessNameTextView = (TextView) mainLayout.findViewById(R.id.business_name);
        addressTextView = (TextView) mainLayout.findViewById(R.id.address);
        phoneTextView = (TextView) mainLayout.findViewById(R.id.phone_no);

        customerPhoneLayout = (LinearLayout) mainLayout.findViewById(R.id.customer_phone_layout);
        customerAddressLayout = (LinearLayout) mainLayout.findViewById(R.id.customer_address_layout);
        customerNameTextView = (TextView) mainLayout.findViewById(R.id.customer_in_sale_detail_tv);
        customerPhoneTextView = (TextView) mainLayout.findViewById(R.id.customer_in_phoneNo);
        customerAddressTextView = (TextView) mainLayout.findViewById(R.id.customer_in_address);
        taxAmtTextView = (TextView) mainLayout.findViewById(R.id.tax_amt_in_sale_detail_tv);
        taxRateTextView = (TextView) mainLayout.findViewById(R.id.tax_rate_tv);
        taxTypeTextView = (TextView) mainLayout.findViewById(R.id.tax_type_tv);
        deliverChargesTextView = (TextView) mainLayout.findViewById(R.id.delivery_in_sale_detail_tv);
        deliveryAddress = (TextView) mainLayout.findViewById(R.id.delivery_address_tv);
        deliveryAddressLinearLayout = (LinearLayout) mainLayout.findViewById(R.id.ll_delivery_address);
        saleItemRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.sale_item_list_rv);
        saleIdTextView = (TextView) mainLayout.findViewById(R.id.sale_id_in_sale_detail_tv);
        dateTextView = (TextView) mainLayout.findViewById(R.id.sale_date_in_sale_detail_tv);
        discountTextView = (TextView) mainLayout.findViewById(R.id.discount_in_sale_detail_tv);
        subtotalTextView = (TextView) mainLayout.findViewById(R.id.subtotal_in_sale_detail_tv);
        totalTextView = (TextView) mainLayout.findViewById(R.id.total_in_sale_detail_tv);
        headerTextView = (TextView) mainLayout.findViewById(R.id.header_text);
        footerTextView = (TextView) mainLayout.findViewById(R.id.footer_text);

        customerName = salesHeader.getCustomerName();
        customerPhNo = salesHeader.getCustomerPhone();
        customerAddress = salesHeader.getCustomerAddress();


        if(customerName.equalsIgnoreCase(DEFAULT_CUSTOMER)) {
            customerPhNo = null;
            customerAddress = null;
            customerPhoneLayout.setVisibility(View.GONE);
            customerAddressLayout.setVisibility(View.GONE);
        }
        else {

            if (customerPhNo == null || customerPhNo.equals("")) {
                customerPhNo = null;
                customerPhoneLayout.setVisibility(View.GONE);


            }
            if (customerAddress == null || customerAddress.equals("")) {
                customerAddress = null;
                customerAddressLayout.setVisibility(View.GONE);

            }
        }
    }

    private void updateViewData() {
        discountTextView.setText(POSUtil.NumberFormat(salesHeader.getDiscountAmt()));
        subtotalTextView.setText(POSUtil.NumberFormat(salesHeader.getSubtotal()));
        totalTextView.setText(POSUtil.NumberFormat(salesHeader.getTotal()));
        deliverChargesTextView.setText(POSUtil.NumberFormat(deliveryCharges));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.print, menu);


        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.print) {

            preparePrint();

        } else if (item.getItemId() == R.id.save_pdf) {
            if (grantPermission.requestPermissions()) {
                showFolderChooserDialog();
            }
        }

        return true;
    }
    private void preparePrint() {
        isPrint = true;

        if (!bluetoothUtil.isConnected() && !POSUtil.isInternalPrinter(context)) {
            createBluetoothConnection();
        } else {
            String saleTxt = ThemeUtil.getString(context, R.attr.sale_invoice);
            String customerTxt = ThemeUtil.getString(context, R.attr.customer);
            String numberTxt = ThemeUtil.getString(context, R.attr.number);
            String itemNameTxt = ThemeUtil.getString(context, R.attr.item_name);
            String amountTxt = ThemeUtil.getString(context, R.attr.amount);
            String subtotalTxt = ThemeUtil.getString(context, R.attr.subtotal);
            String taxTxt = ThemeUtil.getString(context, R.attr.tax);
            String discountTxt = ThemeUtil.getString(context, R.attr.discount);
            String totalTxt = ThemeUtil.getString(context, R.attr.total);
            String paidAmountTxt = ThemeUtil.getString(context, R.attr.paid_amount);
            String changeAmountTxt = ThemeUtil.getString(context, R.attr.change);
            String cusPhoneNumber = ThemeUtil.getString(context,R.attr.phone_no);
            String cusAddressTxt = ThemeUtil.getString(context,R.attr.address);

            String thanksyouTxt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.thank_you}).getString(0);

            Typeface mTypeface = Typeface.createFromAsset(context.getApplicationContext()
                    .getAssets(), String.format("fonts/%s", "Zawgyi-One.ttf"));
            PrintCanvas printCanvas;

            // Paper type
            if (POSUtil.is80MMPrinter(getContext())) {
                printCanvas = new PrintCanvas(PrintCanvas.PAPERSIZE._80MM, mTypeface, 30);
            } else {
                printCanvas = new PrintCanvas(PrintCanvas.PAPERSIZE._58MM, mTypeface, 20);
            }

            // Header
            String headerTexts[] = headerText.split("\n");

            for (String headerTxt : headerTexts) {
                printCanvas.writeText(PrintCanvas.ALIGN.CENTER, headerTxt, PrintCanvas.ALIGN.CENTER, 60);
            }
            printCanvas.nextLine();

            // Sale:
            PrintCanvas.Row row = printCanvas.new Row();
            PrintCanvas.Column column = printCanvas.new Column(40, saleTxt + " : ", PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            // eg. #S0001
            column = printCanvas.new Column(30, "#" + salesHeader.getSaleVocherNo(), PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            // eg. 22/01/2017
            column = printCanvas.new Column(40, DateUtility.makeDateFormatWithSlash(salesHeader.getSaleDate()), PrintCanvas.ALIGN.RIGHT);
            row.addColumn(column);
            printCanvas.writeRow(row);

            // Customer:
            row = printCanvas.new Row();
            column = printCanvas.new Column(40, customerTxt + " : ", PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            // eg. John
            column = printCanvas.new Column(35, salesHeader.getCustomerName(), PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            // just some white space
            column = printCanvas.new Column(35, "", PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);
            printCanvas.writeRow(row);



            if(customerPhNo != null) {
                //PhoneNo
                row = printCanvas.new Row();
                column = printCanvas.new Column(40, cusPhoneNumber + " : ", PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);

                //09####

                column = printCanvas.new Column(60, customerPhNo, PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);



                printCanvas.writeRow(row);

            }

            if(customerAddress != null)
            {
                //Address
                row = printCanvas.new Row();
                column = printCanvas.new Column(40, cusAddressTxt + " : ", PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);

                //address

                column = printCanvas.new Column(60, customerAddress, PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);



                printCanvas.writeRow(row);
            }

            printCanvas.writeLine();

            // No
            row = printCanvas.new Row();
            column = printCanvas.new Column(15, numberTxt, PrintCanvas.ALIGN.CENTER);
            row.addColumn(column);

            // Item Name
            column = printCanvas.new Column(55, itemNameTxt, PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            // Amount
            column = printCanvas.new Column(40, amountTxt, PrintCanvas.ALIGN.RIGHT);
            row.addColumn(column);
            printCanvas.writeRow(row);
            printCanvas.writeLine();

            for (SalesAndPurchaseItem s : salesAndPurchaseItemList) {
                // eg. 1    Apple   300
                row = printCanvas.new Row();
                column = printCanvas.new Column(15, Integer.toString(salesAndPurchaseItemList.indexOf(s) + 1), PrintCanvas.ALIGN.CENTER);
                row.addColumn(column);

                column = printCanvas.new Column(60, s.getItemName(), PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);

                column = printCanvas.new Column(30, POSUtil.NumberFormat(s.getTotalPrice()), PrintCanvas.ALIGN.RIGHT);
                row.addColumn(column);
                printCanvas.writeRow(row);

                //new line
                // eg.     300 x 1
                row = printCanvas.new Row();
                column = printCanvas.new Column(15, "", PrintCanvas.ALIGN.CENTER);
                row.addColumn(column);

                column = printCanvas.new Column(60, s.getQty() + " x " + POSUtil.NumberFormat(s.getPrice()), PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);

                column = printCanvas.new Column(30, "", PrintCanvas.ALIGN.RIGHT);
                row.addColumn(column);
                printCanvas.writeRow(row);
            }
            printCanvas.nextLine();
            printCanvas.writeLine();

            // just some whitespaces
            row = printCanvas.new Row();
            column = printCanvas.new Column(20, "", PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            // Subtotal
            column = printCanvas.new Column(40, subtotalTxt, PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            // :
            column = printCanvas.new Column(15, ":", PrintCanvas.ALIGN.CENTER);
            row.addColumn(column);

            // eg. 300
            column = printCanvas.new Column(30, POSUtil.NumberFormat(salesHeader.getSubtotal()), PrintCanvas.ALIGN.RIGHT);
            row.addColumn(column);
            printCanvas.writeRow(row);

            if (salesHeader.getTaxAmt() != 0) {
                // white spaces
                row = printCanvas.new Row();
                column = printCanvas.new Column(20, "", PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);

                // Tax(5% Inclusive)
                column = printCanvas.new Column(40, taxTxt + "(" + salesHeader.getTaxRate() + "% " + salesHeader.getTaxType() + ")", PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);

                // :
                column = printCanvas.new Column(15, ":", PrintCanvas.ALIGN.CENTER);
                row.addColumn(column);

                // eg. 15
                column = printCanvas.new Column(30, POSUtil.NumberFormat(salesHeader.getTaxAmt()), PrintCanvas.ALIGN.RIGHT);
                row.addColumn(column);
                printCanvas.writeRow(row);
            }


            row = printCanvas.new Row();
            column = printCanvas.new Column(20, "", PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            // Discount
            if(salesHeader.getDiscountAmt() != 0) {
                column = printCanvas.new Column(40, discountTxt, PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);

                // :
                column = printCanvas.new Column(15, ":", PrintCanvas.ALIGN.CENTER);
                row.addColumn(column);

                // eg. 20
                column = printCanvas.new Column(30, POSUtil.NumberFormat(salesHeader.getDiscountAmt()), PrintCanvas.ALIGN.RIGHT);
                row.addColumn(column);
                printCanvas.writeRow(row);

            }
            printCanvas.writeLine();

            row = printCanvas.new Row();
            column = printCanvas.new Column(10, "", PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            // Total
            column = printCanvas.new Column(50, totalTxt + "(" + AppConstant.CURRENCY + ")", PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            // :
            column = printCanvas.new Column(15, ":", PrintCanvas.ALIGN.CENTER);
            row.addColumn(column);

            // eg. 144
            column = printCanvas.new Column(30, POSUtil.NumberFormat(salesHeader.getTotal()), PrintCanvas.ALIGN.RIGHT);
            row.addColumn(column);
            printCanvas.writeRow(row);

            row = printCanvas.new Row();
            column = printCanvas.new Column(10, "", PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            column = printCanvas.new Column(50, paidAmountTxt + "(" + AppConstant.CURRENCY + ")", PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            column = printCanvas.new Column(15, ":", PrintCanvas.ALIGN.CENTER);
            row.addColumn(column);

            column = printCanvas.new Column(30, POSUtil.NumberFormat(paidAmount), PrintCanvas.ALIGN.RIGHT);
            row.addColumn(column);
            printCanvas.writeRow(row);

            //Add Column For Change Amount
          if(!salesHeader.getChangeBalance().equalsIgnoreCase("0")) {
              row = printCanvas.new Row();
              column = printCanvas.new Column(10, "", PrintCanvas.ALIGN.LEFT);
              row.addColumn(column);

              //Change (MMK)
              column = printCanvas.new Column(50, changeAmountTxt + "(" + AppConstant.CURRENCY + ")", PrintCanvas.ALIGN.LEFT);
              row.addColumn(column);
              // :
              column = printCanvas.new Column(15, ":", PrintCanvas.ALIGN.CENTER);
              row.addColumn(column);

              //400
              column = printCanvas.new Column(30, POSUtil.NumberFormat(Double.parseDouble(salesHeader.getChangeBalance())), PrintCanvas.ALIGN.RIGHT);
              row.addColumn(column);
              printCanvas.writeRow(row);
          }
            printCanvas.writeLine();

            printCanvas.writeText(PrintCanvas.ALIGN.CENTER, footerText, PrintCanvas.ALIGN.CENTER, 60);

            //printCanvas.drawWaterMarkDiagonal();

            Bitmap bmpMonochrome = Bitmap.createBitmap(printCanvas.getBitmap().getWidth(), printCanvas.getBitmap().getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas1 = new Canvas(bmpMonochrome);
            Paint paint1 = new Paint();

            canvas1.drawColor(Color.WHITE);
            canvas1.drawBitmap(printCanvas.getBitmap(), 0, 0, paint1);

            PrintImage printImage = new PrintImage(bmpMonochrome);

            try {

                if (POSUtil.isInternalPrinter(getContext())) {

                    AidlUtil.getInstance().initPrinter();
                    // AidlUtil.getInstance().printText(printViewSlip.getPrintString(), 24, false, false,"Type monospace");
                    AidlUtil.getInstance().printBitmap(printCanvas.getBitmap());
                } else {
                    bluetoothUtil.getOutputStream().write(printImage.getPrintImageData());
                    bluetoothUtil.getOutputStream().write(CUT_PAPER);


                }

                ;
                //
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void createBluetoothConnection() {
        if (POSUtil.isTabetLand(context)) {
            bluetoothUtil.createConnection(true);
        } else {
            bluetoothUtil.createConnection(false);
        }

    }

    public void replacingFragment(Fragment fragment) {

        final FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        fragmentTransaction.replace(R.id.frame_replace, fragment);

        fragmentTransaction.commit();

    }

    @Override
    public void onFolderSelection(File folder) {
        path = folder.getAbsolutePath();

        fileNameEditText.setError(null);
        fileNameEditText.setText(null);

        fileNameMaterialDialog.show();
    }


    @Override
    public void onResume() {
        super.onResume();

        if (directPrint) {

            preparePrint();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.w("ACITVITY", "RESULT");

        bluetoothUtil.onActivityResultBluetooth(requestCode, resultCode, data);

       /* switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect

                if (resultCode == Activity.RESULT_OK) {

                    Log.w("CONE","HERE");
                    // Get the device MAC address
                    String address = data.getExtras()
                            .getString(EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    device = mBluetoothAdapter.getRemoteDevice(address);

                    new ConnectionProgress().execute();

                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    if(device==null||!mmSocket.isConnected()){
                        if(isPrint){
                            Log.w("PRINT","CONNECT AND PRINT");
                            isPrint=false;
                            Intent serverIntent = null;
                            serverIntent = new Intent(getContext(), BluetoothDeviceNearByList.class);
                            startActivityForResult(serverIntent, REQUEST_CONNECT_AND_PRINT_DEVICE);
                        }else {
                            Log.w("PRINT","CONNECT");

                            Intent serverIntent = null;
                            serverIntent = new Intent(getContext(), BluetoothDeviceNearByList.class);
                            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                        }

                    }else {
                        if (mmSocket.isConnected()) {
                            print();
                        } else {
                            //DisplayToast("Make sure bluetooth turn on both devices and choose printer only!!!");
                        }

                    }

                } else {
                    // User did not enable Bluetooth or an error occured
                    Log.d("DD", "BT not enabled");
                    //Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    // finish();
                   // DisplayToast("Bluetooth must be enable to print");
                }
                break;
            case REQUEST_CONNECT_AND_PRINT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getExtras()
                            .getString(EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    device = mBluetoothAdapter.getRemoteDevice(address);

                    new ConnectionAndPrintingProgress().execute();

                }

        }*/

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //recreate();
            //reload my activity with requestPermissions granted or use the features what required the requestPermissions
            showFolderChooserDialog();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            grantPermission.askAgainPermission();
        } else {
            grantPermission.forcePermissionSetting();
            //}
        }
    }

    class SavePDFProgress extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {

            Log.w("HELO", params[0]);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            PrintInvoice printInvoice = new PrintInvoice(getContext(), params[0]);

            //             Print_pdf print_pdf=new Print_pdf();
            try {
                //print_pdf.createPDF("TEST",getContext());

                salesHeader.setHeaderText(headerText);
                salesHeader.setFooterText(footerText);
                printInvoice.createPDF(params[1], salesHeader, salesAndPurchaseItemList, businessSetting);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Log.w("Saving", "Background");


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!saveHud.isShowing()) {
                        saveHud.show();
                    }


                }
            });


        }

        @Override
        protected void onPostExecute(String a) {
            if (saveHud.isShowing()) {
                saveHud.dismiss();

                String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save_successfully}).getString(0);
                FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);

            }
            fileOverrideMaterialDialog.dismiss();
        }
    }
}