package com.digitalfusion.android.pos.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.widget.Button;

/**
 * Created by MD002 on 1/17/18.
 */

public class GrantPermissionFromActivity {

    public static final int STORAGE_PERMISSION_RC = 200;
    private Activity activity;

    public GrantPermissionFromActivity(Activity activity) {
        this.activity = activity;
    }

    public boolean permission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED) {
                activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_RC);
            }
            return ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED;

        }
        return true;
    }

    public void askAgainPermission() {
        AlertDialog alertDialog;
        alertDialog = new AlertDialog.Builder(activity)
                .setTitle(Html.fromHtml("<font color='#424242'>Inform and request</font>"))
                .setMessage(Html.fromHtml("<font color='#424242'>Please enable storage requestPermissions to perform backup and restore functions!</font>")).setNegativeButton(Html.fromHtml("<font color='#000000'>No</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(Html.fromHtml("<font color='#424242'>Ok</font>"), new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.requestPermissions.WRITE_EXTERNAL_STORAGE}, RC_PERMISSION_WRITE_EXTERNAL_STORAGE);
                        //   ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.requestPermissions.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_RC);
                        activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 200);
                    }
                })

                .show();
        Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        nbutton.setTextColor(Color.parseColor("#424242"));
    }

    public void forcePermissionSetting() {
        AlertDialog alertDialog;
        alertDialog = new AlertDialog.Builder(activity)
                .setTitle(Html.fromHtml("<font color='#424242'>Inform and request</font>"))
                .setMessage(Html.fromHtml("<font color='#424242'>Please enable storage requestPermissions from settings to get access to the storage!</font>"))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 200);
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                        intent.setData(uri);
                        activity.startActivity(intent);
                    }
                })
                .show();
        Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        nbutton.setTextColor(Color.parseColor("#424242"));
    }

}
