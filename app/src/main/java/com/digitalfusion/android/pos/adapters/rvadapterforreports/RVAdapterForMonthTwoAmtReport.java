package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD002 on 12/3/16.
 */

public class RVAdapterForMonthTwoAmtReport extends ParentRVAdapterForReports {

    private final int HEADER_TYPE = 10000;

    protected String[] mMonths = new String[]{
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    };
    private List<ReportItem> incomeReportItemList;
    private List<ReportItem> expenseReportItemList;

    public RVAdapterForMonthTwoAmtReport(List<ReportItem> incomeReportItemList, List<ReportItem> expenseReportItemList) {
        this.incomeReportItemList = incomeReportItemList;
        this.expenseReportItemList = expenseReportItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_month_two_amount_header, parent, false);

            return new HeaderItemView(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_month_two_amount, parent, false);

            return new ReportItemViewHolder(v);
        }

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ReportItemViewHolder) {

            ReportItemViewHolder rholder = (ReportItemViewHolder) holder;
            rholder.monthTextView.setText(mMonths[incomeReportItemList.get(position - 1).getMonth() - 1]);

            Log.e("bal", incomeReportItemList.get(position - 1).getBalance() + "doajf");
            rholder.incomeTextView.setText(POSUtil.NumberFormat(incomeReportItemList.get(position - 1).getBalance()));
            rholder.expenseTextView.setText(POSUtil.NumberFormat(expenseReportItemList.get(position - 1).getBalance()));
        }
    }

    @Override
    public int getItemCount() {
        return incomeReportItemList.size() + 1;
    }

    public List<ReportItem> getIncomeReportItemList() {
        return incomeReportItemList;
    }

    public void setIncomeReportItemList(List<ReportItem> incomeReportItemList) {
        this.incomeReportItemList = incomeReportItemList;
    }

    public List<ReportItem> getExpenseReportItemList() {
        return expenseReportItemList;
    }

    public void setExpenseReportItemList(List<ReportItem> expenseReportItemList) {
        this.expenseReportItemList = expenseReportItemList;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_TYPE;
        } else {
            return 1;
        }
    }


    public class ReportItemViewHolder extends RecyclerView.ViewHolder {
        TextView monthTextView;
        TextView incomeTextView;
        TextView expenseTextView;

        View view;

        public ReportItemViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            monthTextView = (TextView) itemView.findViewById(R.id.month_text_view);

            incomeTextView = (TextView) itemView.findViewById(R.id.income_text_view);

            expenseTextView = (TextView) itemView.findViewById(R.id.expense_text_view);

        }

    }

    public class HeaderItemView extends RecyclerView.ViewHolder {

        View view;

        public HeaderItemView(View itemView) {
            super(itemView);

            this.view = itemView;


        }

    }

}
