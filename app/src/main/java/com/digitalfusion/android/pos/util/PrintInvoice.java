package com.digitalfusion.android.pos.util;

import android.content.Context;
import android.os.Environment;

import com.digitalfusion.android.pos.database.model.BusinessSetting;
import com.digitalfusion.android.pos.database.model.PurchaseHeader;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.database.model.SalesHeader;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by M0002 on 3/4/17.
 */
public class PrintInvoice {
    PdfWriter docWriter = null;
    Paragraph paragraph;
    PdfPTable table;
    PdfPCell cell;
    String savePath;

    SalesHeader salesHeader;
    PurchaseHeader purchaseHeader;

    List<SalesAndPurchaseItem> salesAndPurchaseItems;

    List<ReportItem> reportItems;

    BusinessSetting businessSetting;


    String invoice = "";

    String custSupName = "";

    Font font1;
    //DetailUtility detailUtility;

    Context context;

    public PrintInvoice(Context context, String path) {
        this.savePath = path;
        this.context = context;
        OutputStream outputStream = null;
        try {
            InputStream inputStream = context.getAssets().open("Zawgyi-One.ttf");
            outputStream = new FileOutputStream(Environment.getExternalStorageDirectory() + "/font.ttf");
            byte[] buffer = new byte[1024];
            int    length;
            while ((length = inputStream.read(buffer)) > 0) {

                outputStream.write(buffer, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        font1 = FontFactory.getFont(Environment.getExternalStorageDirectory() + "/font.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

        //detailUtility=new DetailUtility();
    }

    public void createPDF(String pdfFilename, SalesHeader salesHeader, List<SalesAndPurchaseItem> salesAndPurchaseItems, BusinessSetting businessSetting) throws IOException, DocumentException {
        Document doc = new Document();

        this.salesHeader = salesHeader;
        this.salesAndPurchaseItems = salesAndPurchaseItems;
        this.businessSetting = businessSetting;

        invoice = salesHeader.getSaleVocherNo();
        custSupName = salesHeader.getCustomerName();

        try {
            //file path
            String path = savePath + "/" + pdfFilename + ".pdf";


            File file = new File(path);
            // If file does not exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(file.getAbsoluteFile()));

            //document header attributes
            doc.addAuthor("FusionPOS's Note");
            doc.addCreationDate();
            doc.addProducer();
            doc.addCreator("Digitalfusion");
            doc.addTitle("Invoices");
            //   doc.setPageSize(PageSize.A4);

            //open document
            doc.open();
            PdfContentByte cb = docWriter.getDirectContent();
            createBillSale(doc);
        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    public void createSalePDFWithPaymentHistory(String pdfFilename, List<ReportItem> reportItems, SalesHeader salesHeader, List<SalesAndPurchaseItem> salesAndPurchaseItems, BusinessSetting businessSetting) throws IOException, DocumentException {
        Document doc = new Document();

        this.salesHeader = salesHeader;
        this.salesAndPurchaseItems = salesAndPurchaseItems;
        this.businessSetting = businessSetting;

        this.reportItems = reportItems;

        invoice = salesHeader.getSaleVocherNo();
        custSupName = salesHeader.getCustomerName();

        try {
            //file path
            String path = savePath + "/" + pdfFilename + ".pdf";


            File file = new File(path);
            // If file does not exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(file.getAbsoluteFile()));

            //document header attributes
            doc.addAuthor("FusionPOS's Note");
            doc.addCreationDate();
            doc.addProducer();
            doc.addCreator("DigitalFusion");
            doc.addTitle("Invoices");
            //   doc.setPageSize(PageSize.A4);

            //open document
            doc.open();
            PdfContentByte cb = docWriter.getDirectContent();
            createBillSaleWithPaymentHistory(doc);
        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    public void createPurchasePDFWithPaymentHistory(String pdfFilename, List<ReportItem> reportItems, PurchaseHeader purchaseHeader, List<SalesAndPurchaseItem> salesAndPurchaseItems, BusinessSetting businessSetting) throws IOException, DocumentException {
        Document doc = new Document();

        this.purchaseHeader = purchaseHeader;
        this.salesAndPurchaseItems = salesAndPurchaseItems;
        this.businessSetting = businessSetting;

        this.reportItems = reportItems;

        invoice = purchaseHeader.getPurchaseVocherNo();
        custSupName = purchaseHeader.getSupplierName();

        try {
            //file path
            String path = savePath + "/" + pdfFilename + ".pdf";


            File file = new File(path);
            // If file does not exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(file.getAbsoluteFile()));

            //document header attributes
            //document header attributes
            doc.addAuthor("FusionPOS's Note");
            doc.addCreationDate();
            doc.addProducer();
            doc.addCreator("DigitalFusion");
            doc.addTitle("Invoices");
            //   doc.setPageSize(PageSize.A4);

            //open document
            doc.open();
            PdfContentByte cb = docWriter.getDirectContent();
            createBillPurWithPaymentHistory(doc);
        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    public void createPDF(String pdfFilename, PurchaseHeader purchaseHeader, List<SalesAndPurchaseItem> salesAndPurchaseItems, BusinessSetting businessSetting) throws IOException, DocumentException {
        Document doc = new Document();

        this.purchaseHeader = purchaseHeader;
        this.salesAndPurchaseItems = salesAndPurchaseItems;
        this.businessSetting = businessSetting;

        invoice = purchaseHeader.getPurchaseVocherNo();
        custSupName = purchaseHeader.getSupplierName();


        try {
            //file path
            String path = savePath + "/" + pdfFilename + ".pdf";


            File file = new File(path);
            // If file does not exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(file.getAbsoluteFile()));

            //document header attributes
            doc.addAuthor("POS's Note");

            doc.addCreationDate();

            doc.addProducer();

            doc.addCreator("Prologic.com");

            doc.addTitle("Invoices");

            //   doc.setPageSize(PageSize.A4);

            //open document
            doc.open();

            PdfContentByte cb = docWriter.getDirectContent();

            createBillPurchase(doc);


        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    private void createBillSale(Document document) throws DocumentException {


        DottedLineSeparator separator = new DottedLineSeparator();
        separator.setPercentage(59500f / 523f);
        //Chunk linebreak = new Chunk(separator);
        //document.add(linebreak);

        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        if (salesHeader.getHeaderText() != null || salesHeader.getHeaderText() != "") {
            paragraph = new Paragraph(salesHeader.getHeaderText(), font1);
            //paragraph.setFont(font1);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setFirstLineIndent(1);
            document.add(paragraph);
        }

        paragraph = new Paragraph("Sales : #" + salesHeader.getSaleVocherNo());
        paragraph.setAlignment(Element.ALIGN_LEFT);

        document.add(paragraph);

        paragraph = new Paragraph("Customer : " + salesHeader.getCustomerName());
        paragraph.setAlignment(Element.ALIGN_LEFT);
        document.add(paragraph);

        float[] columnWidths = {1f, 10f, 3f};
        table = new PdfPTable(columnWidths);

        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);

        cell = new PdfPCell(new Phrase("No"));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);


        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Item Name"));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);

        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Amount"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);

        table.setHeaderRows(1);

        Long total = 0l;
        int  i     = 0;
        for (SalesAndPurchaseItem item : salesAndPurchaseItems) {
            cell = new PdfPCell(new Phrase(String.valueOf(i + 1)));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(item.getItemName() + " x " + item.getQty()));

            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(item.getTotalPrice())));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setMinimumHeight(25f);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            i++;
            //c.moveToNext();
        }


        //sqlcon.close();


        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("SubTotal"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(salesHeader.getSubtotal())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Tax(" + salesHeader.getTaxRate() + "% " + salesHeader.getTaxType() + ")"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(salesHeader.getTaxAmt())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Discount"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(salesHeader.getDiscountAmt())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        cell = new PdfPCell(new Phrase("Total(" + AppConstant.CURRENCY + ")", font1));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(salesHeader.getTotal()), font1));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        cell = new PdfPCell(new Phrase("Paid Amount(" + AppConstant.CURRENCY + ")", font1));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(salesHeader.getPaidAmt()), font1));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);


        table.setSpacingBefore(15f);
        table.setSpacingAfter(1f);
        document.add(table);


        paragraph = new Paragraph(salesHeader.getFooterText());
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);

       /* paragraph = new Paragraph("Part");
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setSpacingBefore(15f);
        paragraph.setSpacingAfter(5f);
        document.add(paragraph);
        document.add(table);
        */


        //  document.add(linebreak1);
        //  paragraph = new Paragraph("-------------------THANK YOU--------------------");
        // paragraph.setAlignment(Element.ALIGN_JUSTIFIED_ALL);
        // paragraph.setSpacingBefore(0f);
        //document.add(paragraph);

    }

    private void createBillSaleWithPaymentHistory(Document document) throws DocumentException {

        Double totalAmount  = salesHeader.getTotal();
        Double totalPaidAmt = 0.0;
        for (ReportItem r : reportItems) {
            totalPaidAmt += r.getAmount();
        }


        //totalReceivableTextView.setText(POSUtil.NumberFormat(totalAmount - totalPaidAmt));
        totalAmount -= totalPaidAmt;


        DottedLineSeparator separator = new DottedLineSeparator();
        separator.setPercentage(59500f / 523f);
        //Chunk linebreak = new Chunk(separator);
        //document.add(linebreak);


        font1.setSize(18);
        font1.setStyle(Font.BOLD);


        font1.setStyle(Font.BOLD);

        if (salesHeader.getHeaderText() != null || salesHeader.getHeaderText() != "") {
            paragraph = new Paragraph(salesHeader.getHeaderText(), font1);
            //paragraph.setFont(font1);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setFirstLineIndent(1);
            document.add(paragraph);
        }

        paragraph = new Paragraph("Sale     : #" + salesHeader.getSaleVocherNo());
        paragraph.setAlignment(Element.ALIGN_LEFT);

        document.add(paragraph);

        paragraph = new Paragraph("Customer : " + salesHeader.getCustomerName());
        paragraph.setAlignment(Element.ALIGN_LEFT);
        document.add(paragraph);

        float[] columnWidths = {1f, 10f, 3f};
        table = new PdfPTable(columnWidths);


        table.setSpacingBefore(1f);
        table.setSpacingAfter(1f);
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(5f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(5f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(5f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("No"));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);


        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Item Name"));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);

        table.addCell(cell);

       /* cell = new PdfPCell(new Phrase("Amount"));
        cell.setFixedHeight(30f);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);*/

        cell = new PdfPCell(new Phrase("Amount"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);

        table.setHeaderRows(1);

        /*sqlcon.open();
        Cursor c = sqlcon.readOrderedItems("");
        int rows = c.getCount();
        c.moveToFirst();*/

        Long total = 0l;
        int  i     = 0;
        for (SalesAndPurchaseItem item : salesAndPurchaseItems) {
            cell = new PdfPCell(new Phrase(String.valueOf(i + 1)));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(item.getItemName()));

            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);

        /*    cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(item.getPrice())));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);*/

            // total += item.getCharges();

            cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(item.getTotalPrice())));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setMinimumHeight(25f);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            i++;
            //c.moveToNext();
        }


        //sqlcon.close();


        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);


        cell = new PdfPCell(new Phrase("SubTotal"));
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(salesHeader.getSubtotal())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);


        cell = new PdfPCell(new Phrase("Tax(" + salesHeader.getTaxRate() + "% " + salesHeader.getTaxType() + ")"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(salesHeader.getTaxAmt())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Discount"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(salesHeader.getDiscountAmt())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);


        cell = new PdfPCell(new Phrase("Total (" + AppConstant.CURRENCY + ")"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(salesHeader.getTotal())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);


        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        cell = new PdfPCell(new Phrase("Total Paid"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(totalPaidAmt)));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        cell = new PdfPCell(new Phrase("Receivable ", font1));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(totalAmount), font1));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        //table.writeSelectedRows(0, -1, document.leftMargin(), 650,
        // docWriter.getDirectContent());

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(3f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(3f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(3f);
        table.addCell(cell);

        document.add(table);

        font1.setStyle(Font.BOLD);

        paragraph = new Paragraph("Payment History", font1);
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setSpacingAfter(20f);
        document.add(paragraph);


        float[] columnWidths1 = {1f, 10f, 3f, 3f};
        table = new PdfPTable(columnWidths1);

        table.setSpacingBefore(1f);
        table.setSpacingAfter(1f);
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);

        cell = new PdfPCell(new Phrase("No"));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);


        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Date"));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);

        table.addCell(cell);

       /* cell = new PdfPCell(new Phrase("Amount"));
        cell.setFixedHeight(30f);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);*/

        cell = new PdfPCell(new Phrase("Voucher"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Paid Amount"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);

        table.setHeaderRows(1);

        int j = 0;

        Double totalPaid = 0.0;
        for (ReportItem reportItem : reportItems) {
            cell = new PdfPCell(new Phrase(String.valueOf(j + 1)));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(reportItem.getDate()));

            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);

        /*    cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(item.getPrice())));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);*/

            // total += item.getCharges();

            cell = new PdfPCell(new Phrase(reportItem.getName()));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setMinimumHeight(25f);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);


            cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(reportItem.getAmount())));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setMinimumHeight(25f);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            totalPaid += reportItem.getAmount();

            j++;
        }

        cell = new PdfPCell(new Phrase(String.valueOf("")));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setMinimumHeight(25f);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setMinimumHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);


        cell = new PdfPCell(new Phrase("Total Paid"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setMinimumHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(totalPaid)));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setMinimumHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
        document.add(table);

        paragraph = new Paragraph(salesHeader.getFooterText());
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);


    }

    private void createBillPurWithPaymentHistory(Document document) throws DocumentException {


        Double totalAmount  = purchaseHeader.getTotal();
        Double totalPaidAmt = 0.0;
        for (ReportItem r : reportItems) {
            totalPaidAmt += r.getAmount();
        }


        //totalReceivableTextView.setText(POSUtil.NumberFormat(totalAmount - totalPaidAmt));
        totalAmount -= totalPaidAmt;


        DottedLineSeparator separator = new DottedLineSeparator();
        separator.setPercentage(59500f / 523f);
        //Chunk linebreak = new Chunk(separator);
        //document.add(linebreak);


        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        font1.setStyle(Font.BOLD);

        if (purchaseHeader.getHeaderText() != null || purchaseHeader.getHeaderText() != "") {
            paragraph = new Paragraph(purchaseHeader.getHeaderText(), font1);
            //paragraph.setFont(font1);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setFirstLineIndent(1);
            document.add(paragraph);
        }

        paragraph = new Paragraph("Purchase     : #" + purchaseHeader.getPurchaseVocherNo());
        paragraph.setAlignment(Element.ALIGN_LEFT);

        document.add(paragraph);

        paragraph = new Paragraph("Supplier : " + purchaseHeader.getSupplierName());
        paragraph.setAlignment(Element.ALIGN_LEFT);
        document.add(paragraph);

        float[] columnWidths = {1f, 10f, 3f};
        table = new PdfPTable(columnWidths);


        table.setSpacingBefore(1f);
        table.setSpacingAfter(1f);
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(5f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(5f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(5f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("No"));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);


        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Item Name"));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);

        table.addCell(cell);

       /* cell = new PdfPCell(new Phrase("Amount"));
        cell.setFixedHeight(30f);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);*/

        cell = new PdfPCell(new Phrase("Amount"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);

        table.setHeaderRows(1);

        /*sqlcon.open();
        Cursor c = sqlcon.readOrderedItems("");
        int rows = c.getCount();
        c.moveToFirst();*/

        Long total = 0l;
        int  i     = 0;
        for (SalesAndPurchaseItem item : salesAndPurchaseItems) {
            cell = new PdfPCell(new Phrase(String.valueOf(i + 1)));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(item.getItemName()));

            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);

        /*    cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(item.getPrice())));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);*/

            // total += item.getCharges();

            cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(item.getTotalPrice())));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setMinimumHeight(25f);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            i++;
            //c.moveToNext();
        }


        //sqlcon.close();


        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);


        cell = new PdfPCell(new Phrase("SubTotal"));
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(purchaseHeader.getSubtotal())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);


        cell = new PdfPCell(new Phrase("Tax(" + purchaseHeader.getTaxRate() + "% " + purchaseHeader.getTaxType() + ")"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(purchaseHeader.getTaxAmt())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Discount"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(purchaseHeader.getDiscountAmt())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);


        cell = new PdfPCell(new Phrase("Total (" + AppConstant.CURRENCY + ")"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(purchaseHeader.getTotal())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);


        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        cell = new PdfPCell(new Phrase("Total Paid"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(totalPaidAmt)));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        cell = new PdfPCell(new Phrase("Receivable ", font1));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(totalAmount), font1));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        //table.writeSelectedRows(0, -1, document.leftMargin(), 650,
        // docWriter.getDirectContent());

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(3f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(3f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(3f);
        table.addCell(cell);

        document.add(table);

        font1.setStyle(Font.BOLD);

        paragraph = new Paragraph("Payment History", font1);
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setSpacingAfter(20f);
        document.add(paragraph);


        float[] columnWidths1 = {1f, 10f, 3f, 3f};
        table = new PdfPTable(columnWidths1);

        table.setSpacingBefore(1f);
        table.setSpacingAfter(1f);
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);

        cell = new PdfPCell(new Phrase("No"));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);


        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Date"));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);

        table.addCell(cell);

       /* cell = new PdfPCell(new Phrase("Amount"));
        cell.setFixedHeight(30f);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);*/

        cell = new PdfPCell(new Phrase("Voucher"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Paid Amount"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);

        table.setHeaderRows(1);

        int j = 0;

        Double totalPaid = 0.0;
        for (ReportItem reportItem : reportItems) {
            cell = new PdfPCell(new Phrase(String.valueOf(j + 1)));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(reportItem.getDate()));

            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);

        /*    cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(item.getPrice())));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);*/

            // total += item.getCharges();

            cell = new PdfPCell(new Phrase(reportItem.getName()));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setMinimumHeight(25f);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);


            cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(reportItem.getAmount())));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setMinimumHeight(25f);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            totalPaid += reportItem.getAmount();

            j++;
        }

        cell = new PdfPCell(new Phrase(String.valueOf("")));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setMinimumHeight(25f);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setMinimumHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);


        cell = new PdfPCell(new Phrase("Total Paid"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setMinimumHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(totalPaid)));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setMinimumHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
        document.add(table);

        paragraph = new Paragraph(purchaseHeader.getFooterText());
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);


    }

    private void createBillPurchase(Document document) throws DocumentException {

        DottedLineSeparator separator = new DottedLineSeparator();
        separator.setPercentage(59500f / 523f);
        //Chunk linebreak = new Chunk(separator);
        //document.add(linebreak);

        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        if (purchaseHeader.getHeaderText() != null || purchaseHeader.getHeaderText().trim() != "") {
            paragraph = new Paragraph(purchaseHeader.getHeaderText(), font1);
            //paragraph.setFont(font1);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setFirstLineIndent(1);
            document.add(paragraph);
        }

        paragraph = new Paragraph("Purchase : #" + purchaseHeader.getPurchaseVocherNo());
        paragraph.setAlignment(Element.ALIGN_LEFT);

        document.add(paragraph);

        paragraph = new Paragraph("Supplier : " + purchaseHeader.getSupplierName());
        paragraph.setAlignment(Element.ALIGN_LEFT);
        document.add(paragraph);

        float[] columnWidths = {1f, 10f, 3f};
        table = new PdfPTable(columnWidths);

        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);

        cell = new PdfPCell(new Phrase("No"));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);


        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Item Name"));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);

        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Amount"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(30f);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedCell());
        table.addCell(cell);

        table.setHeaderRows(1);

        Long total = 0l;
        int  i     = 0;
        for (SalesAndPurchaseItem item : salesAndPurchaseItems) {
            cell = new PdfPCell(new Phrase(String.valueOf(i + 1)));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(item.getItemName() + " x " + item.getQty()));

            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setMinimumHeight(25f);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(item.getTotalPrice())));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setMinimumHeight(25f);
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            i++;
            //c.moveToNext();
        }


        //sqlcon.close();


        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("SubTotal"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(purchaseHeader.getSubtotal())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setCellEvent(new DottedNoBottomCell());
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Tax(" + purchaseHeader.getTaxRate() + "% " + purchaseHeader.getTaxType() + ")"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(purchaseHeader.getTaxAmt())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Discount"));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(purchaseHeader.getDiscountAmt())));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        cell = new PdfPCell(new Phrase("Total(" + AppConstant.CURRENCY + ")", font1));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(purchaseHeader.getTotal()), font1));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);

        font1.setSize(18);
        font1.setStyle(Font.BOLD);

        cell = new PdfPCell(new Phrase("Paid Amount(" + AppConstant.CURRENCY + ")", font1));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setFixedHeight(25f);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(POSUtil.NumberFormat(purchaseHeader.getPaidAmt()), font1));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setCellEvent(new DottedCell());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(25f);
        table.addCell(cell);


        table.setSpacingBefore(15f);
        table.setSpacingAfter(1f);
        document.add(table);


        paragraph = new Paragraph(purchaseHeader.getFooterText());
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);

       /* paragraph = new Paragraph("Part");
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setSpacingBefore(15f);
        paragraph.setSpacingAfter(5f);
        document.add(paragraph);
        document.add(table);
        */


        //  document.add(linebreak1);
        //  paragraph = new Paragraph("-------------------THANK YOU--------------------");
        // paragraph.setAlignment(Element.ALIGN_JUSTIFIED_ALL);
        // paragraph.setSpacingBefore(0f);
        //document.add(paragraph);


    }

    class DottedCell implements PdfPCellEvent {
        public void cellLayout(PdfPCell cell, Rectangle position,
                               PdfContentByte[] canvases) {
            PdfContentByte canvas = canvases[PdfPTable.LINECANVAS];
            canvas.setLineDash(3f, 3f);
            canvas.moveTo(position.getLeft(), position.getTop());
            canvas.lineTo(position.getRight(), position.getTop());
            canvas.moveTo(position.getLeft(), position.getBottom());
            canvas.lineTo(position.getRight(), position.getBottom());
            canvas.stroke();
        }
    }

    class DottedNoBottomCell implements PdfPCellEvent {
        public void cellLayout(PdfPCell cell, Rectangle position,
                               PdfContentByte[] canvases) {
            PdfContentByte canvas = canvases[PdfPTable.LINECANVAS];
            canvas.setLineDash(3f, 3f);
            canvas.moveTo(position.getLeft(), position.getTop());
            canvas.lineTo(position.getRight(), position.getTop());
            //  canvas.add(new PdfContentByte("DDD"));
            canvas.stroke();
        }
    }

}
