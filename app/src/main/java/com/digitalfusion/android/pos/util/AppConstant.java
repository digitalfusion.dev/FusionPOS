package com.digitalfusion.android.pos.util;

/**
 * Created by MD002 on 8/17/16.
 */
public class AppConstant {
    public static final String[] MONTHS = new String[]{ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

    public static final String REGDATE = "REGDATE";
    public static boolean isAlreadyShowExpire=false;

    public static  boolean clearDataBase = false;
    public static  String url = "";
    public static  String token_name = "";
    public static  String token = "";
    public static  String user_name = "";
    public static  String password = "";


    public static final String PURCHASE_HOLD_DETAIL_TABLE_NAME = "PurchaseHoldDetail";
    public static final String PURCHASE_HOLD_TABLE_NAME = "PurchaseHold";
    public static final String VOUCHERSETTING = "voucherSetting";
    public static final String COUNT = "COUNT";
    public static final String ISNOEMBEDED = "isembended";
    public static final String ISACTIVATED = "isActivated";
    public static final String ENDDATE = "endDate";
    public static final String ISREGISTERED = "isRegistered";
    public static final String ISUNEXPECTEDCHANGE = "isUnExpectedChange";

    public static final String DEFAULT_SUPPLIER = "Default";
    public static final String DEFAULT_CUSTOMER = "Default";
    public static final String IS_MYANMAR="MYANMAR_KEY";
    public static final String THEME = "THEME";
    public static final String PRINTER = "PRINTER";
    public static final String _80MM = "80MM";
    public static final String _58MM = "58MM";
    public static final String _Internal = "Internal";
    public static final String NIGHT_MODE = "NIGHT_MODE";
    public static final String THEME_NAME = "THEME_NAME";
    public static final String DEFAULT_THEME_NO_ACTION_BAR = "DEFAULT_THEME_NO_ACTION_BAR";
    public static final String DEFAULT_THEME_ACTION_BAR = "DEFAULT_THEME_ACTION_BAR";
    public static final String DAMAGE_INVOICE_NO = "Damage";
    public static final String LOST_INVOICE_NO = "Lost";
    public static final String SALE_INVOICE_NO = "Sales";
    public static final String PURCHASE_INVOICE_NO = "Purchase";
    public static final String CUSTOMER_OUTSTANDING_INVOICE_NO = "Customer Outstanding";
    public static final String SUPPLIER_OUTSTANDING_INVOICE_NO = "Supplier Outstanding";

    public static final String SALE_MENU_NAME = "Sale";
    public static final String PURCHASE_MENU_NAME = "Purchase";
    public static final String DAMAGE_MENU_NAME = "Damage";
    public static final String LOST_MENU_NAME = "Lost";
    public static final String CREATED_DATE = "createdDate";

    public static final String ID_GENERATOR_TABLE_NAME = "IdGenerator";
    public static final String ID_GENERATOR_ID = "id";
    public static final String ID_GENERATOR_TABLE_NAME_DESCRIPTION = "tableName";
    public static final String ID_GENERATOR_VALUE = "value";
    public static final String CURRENCY_TABLE_NAME = "Currency";
    public static final String CURRENCY_ID = "id";
    public static final String CURRENCY_NAME = "name";
    public static final String CURRENCY_SYMBOL = "symbol";
    public static final String CURRENCY_IS_DEFAULT = "isDefault";
    public static final String CURRENCY_DESCRIPTION = "description";
    public static final String CURRENCY_CUSTOM_FIELD_1 = "customField1";
    public static final String CURRENCY_CUSTOM_FIELD_2 = "customField2";
    public static final String CURRENCY_CUSTOM_FIELD_3 = "customField3";
    public static final String CURRENCY_CUSTOM_FIELD_4 = "customField4";
    public static final String CURRENCY_CUSTOM_FIELD_5 = "customField5";

    public static final String BUSINESS_SETTING_TABLE_NAME = "BusinessSetting";
    public static final String BUSINESS_SETTING_ID = "id";
    public static final String BUSINESS_SETTING_BUSINESS_NAME = "businessName";
    public static final String BUSINESS_SETTING_PHONE_NUM = "phoneNo";
    public static final String BUSINESS_SETTING_EMAIL = "email";
    public static final String BUSINESS_SETTING_WEBSITE = "website";
    //public static final String BUSINESS_SETTING_ADDRESS = "address";
    public static final String BUSINESS_SETTING_STREET = "street";
    public static final String BUSINESS_SETTING_TOWNSHIP = "township";
    public static final String BUSINESS_SETTING_STATE = "state";
    public static final String BUSINESS_SETTING_CITY = "city";
    public static final String BUSINESS_SETTING_VALUATION_METHOD = "valuationMethod";
    public static final String BUSINESS_SETTING_CURRENCY_ID = "currencyID";
    public static final String BUSINESS_SETTING_LOGO = "logo";
    public static final String BUSINESS_SETTING_CUSTOM_FIELD_1 = "customField1";
    public static final String BUSINESS_SETTING_CUSTOM_FIELD_2 = "customField2";
    public static final String BUSINESS_SETTING_CUSTOM_FIELD_3 = "customField3";
    public static final String BUSINESS_SETTING_CUSTOM_FIELD_4 = "customField4";
    public static final String BUSINESS_SETTING_CUSTOM_FIELD_5 = "customField5";

    public static final String MENU_TABLE_NAME = "Menu";
    public static final String MENU_ID = "id";
    public static final String MENU_NAME = "name";
    public static final String MENU_CUSTOM_FIELD_1 = "customField1";
    public static final String MENU_CUSTOM_FIELD_2 = "customField2";
    public static final String MENU_CUSTOM_FIELD_3 = "customField3";
    public static final String MENU_CUSTOM_FIELD_4 = "customField4";
    public static final String MENU_CUSTOM_FIELD_5 = "customField5";

    public static final String DOCUMENT_NUMBER_SETTING_TABLE_NAME = "DocumentNumberSetting";
    public static final String DOCUMENT_NUMBER_SETTING_ID = "id";
    public static final String DOCUMENT_NUMBER_SETTING_MENU_ID = "menuID";
    //public static final String DOCUMENT_NUMBER_SETTING_DATE_STATUS = "dateStatus";
    public static final String DOCUMENT_NUMBER_SETTING_PREFIX = "prefix";
    public static final String DOCUMENT_NUMBER_SETTING_MIN_DIGIT = "minDigit";
    public static final String DOCUMENT_NUMBER_SETTING_SUFFIX = "suffix";
    public static final String DOCUMENT_NUMBER_SETTING_NEXT_NUMBER = "nextNumber";
    public static final String DOCUMENT_NUMBER_SETTING_CUSTOM_FIELD_1 = "customField1";
    public static final String DOCUMENT_NUMBER_SETTING_CUSTOM_FIELD_2 = "customField2";
    public static final String DOCUMENT_NUMBER_SETTING_CUSTOM_FIELD_3 = "customField3";
    public static final String DOCUMENT_NUMBER_SETTING_CUSTOM_FIELD_4 = "customField4";
    public static final String DOCUMENT_NUMBER_SETTING_CUSTOM_FIELD_5 = "customField5";

    public static final String CUSTOMER_TABLE_NAME = "Customer";
    public static final String CUSTOMER_ID = "id";
    public static final String CUSTOMER_NAME = "name";
    public static final String CUSTOMER_ADDRESS = "address";
    public static final String CUSTOMER_PHONE_NUM = "phoneNo";
    public static final String CUSTOMER_BUSINESS_NAME = "businessName";
    public static final String CUSTOMER_BALANCE = "balance";
    public static final String CUSTOMER_IS_DELETED = "isDeleted";
    public static final String CUSTOMER_CUSTOM_FIELD_1 = "customField1";
    public static final String CUSTOMER_CUSTOM_FIELD_2 = "customField2";
    public static final String CUSTOMER_CUSTOM_FIELD_3 = "customField3";
    public static final String CUSTOMER_CUSTOM_FIELD_4 = "customField4";
    public static final String CUSTOMER_CUSTOM_FIELD_5 = "customField5";

    public static final String CATEGORY_TABLE_NAME = "Category";
    public static final String CATEGORY_ID = "id";
    public static final String CATEGORY_NAME = "name";
    public static final String CATEGORY_DESCRIPTION = "description";
    public static final String CATEGORY_PARENT_ID = "parentID";
    public static final String CATEGORY_LEVEL = "level";
    public static final String CATEGORY_CUSTOM_FIELD_1 = "customField1";
    public static final String CATEGORY_CUSTOM_FIELD_2 = "customField2";
    public static final String CATEGORY_CUSTOM_FIELD_3 = "customField3";
    public static final String CATEGORY_CUSTOM_FIELD_4 = "customField4";
    public static final String CATEGORY_CUSTOM_FIELD_5 = "customField5";

    public static final String UNIT_TABLE_NAME = "Unit";
    public static final String UNIT_ID = "id";
    public static final String UNIT_UNIT = "unit";
    public static final String UNIT_DESCRIPTION = "description";
    public static final String UNIT_CUSTOM_FIELD_1 = "customField1";
    public static final String UNIT_CUSTOM_FIELD_2 = "customField2";
    public static final String UNIT_CUSTOM_FIELD_3 = "customField3";
    public static final String UNIT_CUSTOM_FIELD_4 = "customField4";
    public static final String UNIT_CUSTOM_FIELD_5 = "customField5";

    public static final String TAX_TABLE_NAME = "Tax";
    public static final String TAX_ID = "id";
    public static final String TAX_NAME = "name";
    public static final String TAX_RATE = "rate";
    public static final String TAX_TYPE = "type";
    public static final String TAX_DESCRIPTION = "description";
    public static final String TAX_IS_DEFAULT = "isDefault";
    public static final String TAX_CUSTOM_FIELD_1 = "customField1";
    public static final String TAX_CUSTOM_FIELD_2 = "customField2";
    public static final String TAX_CUSTOM_FIELD_3 = "customField3";
    public static final String TAX_CUSTOM_FIELD_4 = "customField4";
    public static final String TAX_CUSTOM_FIELD_5 = "customField5";

    public static final String STOCK_TABLE_NAME = "Stock";
    public static final String STOCK_ID = "id";
    public static final String STOCK_CODE_NUM = "codeNo";
    public static final String STOCK_BARCODE = "barcode";
    public static final String STOCK_NAME = "name";
    public static final String STOCK_CATEGORY_ID = "categoryID";
    public static final String STOCK_UNIT_ID = "unitID";
    public static final String STOCK_INVENTORY_QTY = "inventoryQty";
    public static final String STOCK_REORDER_QTY = "reorderQty";
    public static final String STOCK_PURCHASE_PRICE = "purchasePrice";
    public static final String STOCK_WHOLESALE_PRICE = "wholesalePrice";
    public static final String STOCK_NORMAL_PRICE = "normalPrice";
    public static final String STOCK_RETAIL_PRICE = "retailPrice";
    //public static final String STOCK_IMAGE = "image";
    public static final String STOCK_IS_DELETED = "isDeleted";
    public static final String STOCK_DESCRIPTION = "description";
    public static final String STOCK_BRAND = "brand";
    public static final String STOCK_COLOR = "color";
    public static final String STOCK_SIZE = "size";
    public static final String STOCK_OPENING_DATE = "openingDate";
    public static final String STOCK_OPENING_QTY = "openingQty";
    public static final String STOCK_OPENING_PRICE = "openingPrice";
    public static final String STOCK_OPENING_TIME = "openingTime";
    public static final String STOCK_CUSTOM_FIELD_1 = "customField1";
    public static final String STOCK_CUSTOM_FIELD_2 = "customField2";
    public static final String STOCK_CUSTOM_FIELD_3 = "customField3";
    public static final String STOCK_CUSTOM_FIELD_4 = "customField4";
    public static final String STOCK_CUSTOM_FIELD_5 = "customField5";

    public static final String ADJUSTMENT_TABLE_NAME = "Adjustment";
    public static final String ADJUSTMENT_STOCK_ID = "stockID";
    public static final String ADJUSTMENT_DATE = "date";
    public static final String ADJUSTMENT_DAY = "day";
    public static final String ADJUSTMENT_MONTH = "month";
    public static final String ADJUSTMENT_YEAR = "year";
    public static final String ADJUSTMENT_QTY = "qty";
    public static final String ADJUSTMENT_PRICE = "price";
    public static final String ADJUSTMENT_TIME = "time";

    public static final String USER_TABLE_NAME = "UserRole";
    public static final String USER_ID = "id";
    public static final String USER_USER_NAME = "username";
    public static final String USER_PASSWORD = "password";
    public static final String USER_ROLE = "role";
    public static final String USER_DESCRIPTION = "description";
    public static final String USER_IS_DELETED = "isDeleted";
    public static final String USER_CUSTOM_FIELD_1 = "customField1";
    public static final String USER_CUSTOM_FIELD_2 = "customField2";
    public static final String USER_CUSTOM_FIELD_3 = "customField3";
    public static final String USER_CUSTOM_FIELD_4 = "customField4";
    public static final String USER_CUSTOM_FIELD_5 = "customField5";

    public static final String DAMAGE_TABLE_NAME = "Damage";
    public static final String DAMAGE_ID = "id";
    //public static final String DAMAGE_INVOICE_NUM = "invoiceNo";
    public static final String DAMAGE_DATE = "date";
    public static final String DAMAGE_DAY = "day";
    public static final String DAMAGE_MONTH = "month";
    public static final String DAMAGE_YEAR = "year";
    //public static final String DAMAGE_APPROVE_BY = "approveBy";
    //public static final String DAMAGE_TOTAL_VALUE = "totalValue";
    public static final String DAMAGE_REMARK = "remark";
    public static final String DAMAGE_USER_ID = "userID";
    public static final String DAMAGE_STOCK_ID = "stockID";
    public static final String DAMAGE_STOCK_QTY = "stockQty";
    public static final String DAMAGE_TIME = "time";
    public static final String DAMAGE_CUSTOM_FIELD_1 = "customField1";
    public static final String DAMAGE_CUSTOM_FIELD_2 = "customField2";
    public static final String DAMAGE_CUSTOM_FIELD_3 = "customField3";
    public static final String DAMAGE_CUSTOM_FIELD_4 = "customField4";
    public static final String DAMAGE_CUSTOM_FIELD_5 = "customField5";

    public static final String LOST_TABLE_NAME = "Lost";
    public static final String LOST_ID = "id";
    //public static final String LOST_INVOICE_NUM = "invoiceNo";
    public static final String LOST_DATE = "date";
    public static final String LOST_DAY = "day";

    /*public static final String DAMAGE_DETAIL_TABLE_NAME = "DamageDetail";
    public static final String DAMAGE_DETAIL_ID = "id";
    public static final String DAMAGE_DETAIL_DAMAGE_ID = "damageID";
    public static final String DAMAGE_DETAIL_STOCK_ID = "stockID";
    public static final String DAMAGE_DETAIL_QTY = "qty";
    public static final String DAMAGE_DETAIL_PURCHASE_PRICE = "purchasePrice";
    public static final String DAMAGE_DETAIL_TOTAL = "total";
    //public static final String DAMAGE_DETAIL_UNIT_ID = "unitID";
    public static final String DAMAGE_DETAIL_REMARK = "remark";
    public static final String DAMAGE_DETAIL_CUSTOM_FIELD_1 = "customField1";
    public static final String DAMAGE_DETAIL_CUSTOM_FIELD_2 = "customField2";
    public static final String DAMAGE_DETAIL_CUSTOM_FIELD_3 = "customField3";
    public static final String DAMAGE_DETAIL_CUSTOM_FIELD_4 = "customField4";
    public static final String DAMAGE_DETAIL_CUSTOM_FIELD_5 = "customField5";*/
    public static final String LOST_MONTH = "month";
    public static final String LOST_YEAR = "year";
    //public static final String LOST_APPROVE_BY = "approveBy";
    //public static final String LOST_TOTAL_VALUE = "totalValue";
    public static final String LOST_REMARK = "remark";
    public static final String LOST_USER_ID = "userID";
    public static final String LOST_STOCK_ID = "stockID";
    public static final String LOST_STOCK_QTY = "stockQty";
    public static final String LOST_TIME = "time";
    public static final String LOST_CUSTOM_FIELD_1 = "customField1";
    public static final String LOST_CUSTOM_FIELD_2 = "customField2";
    public static final String LOST_CUSTOM_FIELD_3 = "customField3";
    public static final String LOST_CUSTOM_FIELD_4 = "customField4";
    public static final String LOST_CUSTOM_FIELD_5 = "customField5";
    public static final String SALES_TABLE_NAME = "Sales";
    public static final String SALES_HOLD_TABLE_NAME = "Hold";

    public static final String SALES_ID = "id";
    public static final String SALES_VOUCHER_NUM = "voucherNo";
    public static final String SALES_DATE = "date";

    /*public static final String LOST_DETAIL_TABLE_NAME = "LostDetail";
    public static final String LOST_DETAIL_ID = "id";
    public static final String LOST_DETAIL_LOST_ID = "lostID";
    public static final String LOST_DETAIL_STOCK_ID = "stockID";
    public static final String LOST_DETAIL_QTY = "qty";
    public static final String LOST_DETAIL_PURCHASE_PRICE = "purchasePrice";
    public static final String LOST_DETAIL_TOTAL = "total";
    //public static final String LOST_DETAIL_UNIT_ID = "unitID";
    public static final String LOST_DETAIL_REMARK = "remark";
    public static final String LOST_DETAIL_CUSTOM_FIELD_1 = "customField1";
    public static final String LOST_DETAIL_CUSTOM_FIELD_2 = "customField2";
    public static final String LOST_DETAIL_CUSTOM_FIELD_3 = "customField3";
    public static final String LOST_DETAIL_CUSTOM_FIELD_4 = "customField4";
    public static final String LOST_DETAIL_CUSTOM_FIELD_5 = "customField5";*/
    public static final String SALES_DAY = "day";
    public static final String SALES_MONTH = "month";
    public static final String SALES_YEAR = "year";
    public static final String SALES_CUSTOMER_ID = "customerID";
    public static final String SALES_TOTAL_AMOUNT = "totalAmount";
    public static final String SALES_DISCOUNT = "discount";
    public static final String SALES_TAX_ID = "taxID";
    public static final String SALES_TAX_RATE = "taxRate";
    public static final String SALES_SUB_TOTAL = "subTotal";
    public static final String SALES_TYPE = "type";
    public static final String SALES_DISCOUNT_ID = "discountID";
    public static final String SALES_DISCOUNT_AMOUNT = "discountAmt";
    public static final String SALES_REMARK = "remark";
    public static final String SALES_PAID_AMOUNT = "paidAmt";
    public static final String SALES_BALANCE = "balance";
    public static final String SALES_TAX_AMOUNT = "taxAmt";
    public static final String SALES_TIME = "time";
    public static final String SALES_TAX_TYPE = "taxType";
    public static final String SALES_CUSTOM_FIELD_1 = "customField1";
    public static final String SALES_USER_ID = "customField2";
    public static final String SALES_CHANGE_CASH = "customField3";
    public static final String SALES_CUSTOM_FIELD_4 = "customField4";
    public static final String SALES_CUSTOM_FIELD_5 = "customField5";
    public static final String SALES_DETAIL_TABLE_NAME = "SalesDetail";
    public static final String SALES_HOLD_DETAIL_TABLE_NAME = "SaleHoldDetails";
    public static final String SALES_DETAIL_ID = "id";
    public static final String SALES_DETAIL_SALES_ID = "salesID";
    public static final String SALES_DETAIL_STOCK_ID = "stockID";
    public static final String SALES_DETAIL_QTY = "qty";
    public static final String SALES_DETAIL_PRICE = "price";
    public static final String SALES_DETAIL_DISCOUNT = "discount";
    public static final String SALES_DETAIL_DISCOUNT_AMT = "discountAmt";
    public static final String SALES_DETAIL_TAX_ID = "taxID";
    public static final String SALES_DETAIL_TAX_RATE = "taxRate";
    public static final String SALES_DETAIL_TOTAL = "total";
    public static final String SALES_DETAIL_TAX_AMOUNT = "taxAmt";
    public static final String SALES_DETAIL_TAX_TYPE = "taxType";
    public static final String SALES_DETAIL_CUSTOM_FIELD_1 = "customField1";
    public static final String SALES_DETAIL_CUSTOM_FIELD_2 = "customField2";
    public static final String SALES_DETAIL_CUSTOM_FIELD_3 = "customField3";
    public static final String SALES_DETAIL_CUSTOM_FIELD_4 = "customField4";
    public static final String SALES_DETAIL_CUSTOM_FIELD_5 = "customField5";

    public static final String PICKUP_TABLE_NAME = "Pickup";
    public static final String PICKUP_ID = "id";
    public static final String PICKUP_DAY = "day";
    public static final String PICKUP_MONTH = "month";
    public static final String PICKUP_YEAR = "year";
    public static final String PICKUP_DATE = "date";
    public static final String PICKUP_PHONE_NUM = "phoneNo";
    public static final String PICKUP_SALES_ID = "salesID";
    public static final String PICKUP_STATUS = "status";
    public static final String PICKUP_CUSTOM_FIELD_1 = "customField1";
    public static final String PICKUP_CUSTOM_FIELD_2 = "customField2";
    public static final String PICKUP_CUSTOM_FIELD_3 = "customField3";
    public static final String PICKUP_CUSTOM_FIELD_4 = "customField4";
    public static final String PICKUP_CUSTOM_FIELD_5 = "customField5";

    public static final String DELIVERY_TABLE_NAME = "Delivery";
    public static final String DELIVERY_ID = "id";
    public static final String DELIVERY_DATE = "date";
    public static final String DELIVERY_DAY = "day";
    public static final String DELIVERY_MONTH = "month";
    public static final String DELIVERY_YEAR = "year";
    public static final String DELIVERY_PHONE_NUM = "phoneNo";
    public static final String DELIVERY_ADDRESS = "address";
    public static final String DELIVERY_AGENT = "agent";
    public static final String DELIVERY_CHARGES = "charges";
    public static final String DELIVERY_IS_INCLUSIVE = "isInclusive";
    public static final String DELIVERY_SALES_ID = "salesID";
    public static final String DELIVERY_STATUS = "status";
    public static final String DELIVERY_TIME = "time";
    public static final String DELIVERY_CUSTOM_FIELD_1 = "customField1";
    public static final String DELIVERY_CUSTOM_FIELD_2 = "customField2";
    public static final String DELIVERY_CUSTOM_FIELD_3 = "customField3";
    public static final String DELIVERY_CUSTOM_FIELD_4 = "customField4";
    public static final String DELIVERY_CUSTOM_FIELD_5 = "customField5";

    public static final String SUPPLIER_TABLE_NAME = "Supplier";
    public static final String SUPPLIER_ID = "id";
    public static final String SUPPLIER_NAME = "name";
    public static final String SUPPLIER_BUSINESS_NAME = "businessName";
    public static final String SUPPLIER_ADDRESS = "address";
    public static final String SUPPLIER_PHONE_NUM = "phoneNo";
    public static final String SUPPLIER_BALANCE = "balance";
    public static final String SUPPLIER_IS_DELETED = "isDeleted";
    public static final String SUPPLIER_CUSTOM_FIELD_1 = "customField1";
    public static final String SUPPLIER_CUSTOM_FIELD_2 = "customField2";
    public static final String SUPPLIER_CUSTOM_FIELD_3 = "customField3";
    public static final String SUPPLIER_CUSTOM_FIELD_4 = "customField4";
    public static final String SUPPLIER_CUSTOM_FIELD_5 = "customField5";

    public static final String PURCHASE_TABLE_NAME = "Purchase";
    public static final String PURCHASE_ID = "id";
    public static final String PURCHASE_VOUCHER_NUM = "voucherNo";
    public static final String PURCHASE_SUPPLIER_ID = "supplierID";
    public static final String PURCHASE_TAX_ID = "taxID";
    public static final String PURCHASE_DISCOUNT = "discount";
    public static final String PURCHASE_DISCOUNT_ID = "discountID";
    public static final String PURCHASE_SUB_TOTAL = "subTotal";
    public static final String PURCHASE_TOTAL_AMOUNT = "total";
    public static final String PURCHASE_PAID_AMOUNT = "paidAmt";
    public static final String PURCHASE_TAX_RATE = "taxRate";
    public static final String PURCHASE_TAX_AMOUNT = "taxAmt";
    public static final String PURCHASE_DAY = "day";
    public static final String PURCHASE_MONTH = "month";
    public static final String PURCHASE_YEAR = "year";
    public static final String PURCHASE_DATE = "date";
    public static final String PURCHASE_REMARK = "remark";
    public static final String PURCHASE_BALANCE = "balance";
    public static final String PURCHASE_DISCOUNT_AMOUNT = "discountAmt";
    public static final String PURCHASE_TIME = "time";
    public static final String PURCHASE_TAX_TYPE = "taxType";
    public static final String CURRENT_USER_ID = "customField_1";
    public static final String PURCHASE_CUSTOM_FIELD_2 = "customField_2";
    public static final String PURCHASE_CUSTOM_FIELD_3 = "customField_3";
    public static final String PURCHASE_CUSTOM_FIELD_4 = "customField_4";
    public static final String PURCHASE_CUSTOM_FIELD_5 = "customField_5";

    public static final String PURCHASE_DETAIL_TABLE_NAME = "PurchaseDetail";
    public static final String PURCHASE_DETAIL_ID = "id";
    public static final String PURCHASE_DETAIL_PURCHASE_ID = "purchaseID";
    public static final String PURCHASE_DETAIL_STOCK_ID = "stockID";
    public static final String PURCHASE_DETAIL_PRICE = "price";
    public static final String PURCHASE_DETAIL_QTY = "qty";
    public static final String PURCHASE_DETAIL_DISCOUNT = "discount";
    public static final String PURCHASE_DETAIL_TAX_ID = "taxID";
    public static final String PURCHASE_DETAIL_TAX_RATE = "taxRate";
    public static final String PURCHASE_DETAIL_TOTAL = "total";
    public static final String PURCHASE_DETAIL_TAX_AMT = "taxAmt";
    public static final String PURCHASE_DETAIL_DISCOUNT_AMT = "discountAmt";
    public static final String PURCHASE_DETAIL_TAX_TYPE = "taxType";
    public static final String PURCHASE_DETAIL_CUSTOM_FIELD_1 = "customField1";
    public static final String PURCHASE_DETAIL_CUSTOM_FIELD_2 = "customField2";
    public static final String PURCHASE_DETAIL_CUSTOM_FIELD_3 = "customField3";
    public static final String PURCHASE_DETAIL_CUSTOM_FIELD_4 = "customField4";
    public static final String PURCHASE_DETAIL_CUSTOM_FIELD_5 = "customField5";

    public static final String EXPENSE_NAME_TABLE_NAME = "ExpenseName";
    public static final String EXPENSE_NAME_ID = "id";
    public static final String EXPENSE_NAME_NAME = "name";
    public static final String EXPENSE_NAME_TYPE = "type";
    public static final String EXPENSE_NAME_CUSTOM_FIELD_1 = "customField1";
    public static final String EXPENSE_NAME_CUSTOM_FIELD_2 = "customField2";
    public static final String EXPENSE_NAME_CUSTOM_FIELD_3 = "customField3";
    public static final String EXPENSE_NAME_CUSTOM_FIELD_4 = "customField4";
    public static final String EXPENSE_NAME_CUSTOM_FIELD_5 = "customField5";
    public static final String EXPENSE_TABLE_NAME = "Expense";
    public static final String EXPENSE_ID = "id";
    public static final String EXPENSE_DATE = "date";
    public static final String EXPENSE_AMOUNT = "amount";
    public static final String EXPENSE_EXPENSE_NAME_ID = "expenseNameID";
    public static final String EXPENSE_REMARK = "remark";
    public static final String EXPENSE_DAY = "day";
    public static final String EXPENSE_MONTH = "month";
    public static final String EXPENSE_YEAR = "year";
    public static final String EXPENSE_REFUND_ID = "refundID";
    public static final String EXPENSE_TIME = "time";
    public static final String EXPENSE_CUSTOM_FIELD_1 = "customField1";
    public static final String EXPENSE_CUSTOM_FIELD_2 = "customField2";
    public static final String EXPENSE_CUSTOM_FIELD_3 = "customField3";
    public static final String EXPENSE_CUSTOM_FIELD_4 = "customField4";
    public static final String EXPENSE_CUSTOM_FIELD_5 = "customField5";

    public static final String CUSTOMER_OUTSTANDING_TABLE_NAME = "CustomerOutstanding";
    public static final String CUSTOMER_OUTSTANDING_ID = "id";
    public static final String CUSTOMER_OUTSTANDING_INVOICE_NUM = "invoiceNo";
    public static final String CUSTOMER_OUTSTANDING_SALES_ID = "salesID";
    public static final String CUSTOMER_OUTSTANDING_PAID_AMOUNT = "paidAmt";
    public static final String CUSTOMER_OUTSTANDING_CUSTOMER_ID = "customerID";
    //public static final String CUSTOMER_OUTSTANDING_CURRENCY_ID = "currencyID";
    public static final String CUSTOMER_OUTSTANDING_DAY = "day";
    public static final String CUSTOMER_OUTSTANDING_MONTH = "month";
    public static final String CUSTOMER_OUTSTANDING_YEAR = "year";
    public static final String CUSTOMER_OUTSTANDING_DATE = "date";
    public static final String CUSTOMER_OUTSTANDING_TIME = "time";
    public static final String CUSTOMER_OUTSTANDING_CUSTOM_FIELD_1 = "customField1";
    public static final String CUSTOMER_OUTSTANDING_CUSTOM_FIELD_2 = "customField2";
    public static final String CUSTOMER_OUTSTANDING_CUSTOM_FIELD_3 = "customField3";
    public static final String CUSTOMER_OUTSTANDING_CUSTOM_FIELD_4 = "customField4";
    public static final String CUSTOMER_OUTSTANDING_CUSTOM_FIELD_5 = "customField5";

    public static final String SUPPLIER_OUTSTANDING_TABLE_NAME = "SupplierOutstanding";
    public static final String SUPPLIER_OUTSTANDING_ID = "id";
    public static final String SUPPLIER_OUTSTANDING_INVOICE_NUM = "invoiceNo";
    public static final String SUPPLIER_OUTSTANDING_PURCHASE_ID = "purchaseID";
    public static final String SUPPLIER_OUTSTANDING_PAID_AMOUNT = "paidAmt";
    public static final String SUPPLIER_OUTSTANDING_SUPPLIER_ID = "supplierID";
    //public static final String SUPPLIER_OUTSTANDING_CURRENCY_ID = "currencyID";
    public static final String SUPPLIER_OUTSTANDING_DAY = "day";
    public static final String SUPPLIER_OUTSTANDING_MONTH = "month";
    public static final String SUPPLIER_OUTSTANDING_YEAR = "year";
    public static final String SUPPLIER_OUTSTANDING_DATE = "date";
    public static final String SUPPLIER_OUTSTANDING_TIME = "time";
    public static final String SUPPLIER_OUTSTANDING_CUSTOM_FIELD_1 = "customField1";
    public static final String SUPPLIER_OUTSTANDING_CUSTOM_FIELD_2 = "customField2";
    public static final String SUPPLIER_OUTSTANDING_CUSTOM_FIELD_3 = "customField3";
    public static final String SUPPLIER_OUTSTANDING_CUSTOM_FIELD_4 = "customField4";
    public static final String SUPPLIER_OUTSTANDING_CUSTOM_FIELD_5 = "customField5";

    public static final String STOCK_IMAGE_TABLE_NAME = "StockImage";
    public static final String STOCK_IMAGE_ID = "id";
    public static final String STOCK_IMAGE_STOCK_ID = "stockID";
    public static final String STOCK_IMAGE_IMG = "img";
    public static final String STOCK_IMAGE_CUSTOM_FIELD_1 = "customField1";
    public static final String STOCK_IMAGE_CUSTOM_FIELD_2 = "customField2";
    public static final String STOCK_IMAGE_CUSTOM_FIELD_3 = "customField3";
    public static final String STOCK_IMAGE_CUSTOM_FIELD_4 = "customField4";
    public static final String STOCK_IMAGE_CUSTOM_FIELD_5 = "customField5";
    public static final String REFUND_TABLE_NAME = "Refund";
    public static final String REFUND_ID = "id";
    public static final String REFUND_VOUCHER_NO = "voucherNo";
    public static final String REFUND_DATE = "date";

    //public static final String PURCHASE_PRICE_TEMP_TABLE_NAME = "PurchasePriceTemp";
    //public static final String PURCHASE_PRICE_STOCK_ID = "stockID";
    //public static final String PURCHASE_PRICE_PRICE = "price";
    //public static final String PURCHASE_PRICE_STOCK_NAME = "name";
    // public static final String PURCHASE_PRICE_STOCK_CODE_NUM = "codeNo";
    public static final String REFUND_DAY = "day";
    public static final String REFUND_MONTH = "month";
    public static final String REFUND_YEAR = "year";
    public static final String REFUND_SALES_ID = "salesID";
    public static final String REFUND_CUSTOMER_ID = "customerID";
    public static final String REFUND_AMOUNT = "amount";
    public static final String REFUND_REMARK = "remark";
    public static final String REFUND_TIME = "time";
    public static final String REFUND_CUSTOM_FIELD_1 = "customField1";
    public static final String REFUND_CUSTOM_FIELD_2 = "customField2";
    public static final String REFUND_CUSTOM_FIELD_3 = "customField3";
    public static final String REFUND_CUSTOM_FIELD_4 = "customField4";
    public static final String REFUND_CUSTOM_FIELD_5 = "customField5";

    public static final String MONTH_TEMP_TABLE_NAME = "MonthTemp";
    public static final String MONTH_TEMP_MONTH = "month";
    public static final String MONTH_TEMP_YEAR = "year";
    public static final String PURCHASE_TEMP_TABLE_NAME = "PurchaseTemp";
    public static final String PURCHASE_TEMP_ID = "id";
    public static final String PURCHASE_TEMP_QTY = "qty";
    public static final String PURCHASE_TEMP_PRICE = "price";
    //public static final String PURCHASE_TEMP_DETAIL_ID = "detailID";
    public static final String PURCHASE_TEMP_TIME = "time";
    public static final String PURCHASE_TEMP_STOCK_ID = "stockID";

    public static final String SALES_TEMP_TABLE_NAME = "SalesTemp";
    public static final String SALES_TEMP_ID = "id";
    public static final String SALES_TEMP_DETAIL_ID = "detailID";
    public static final String SALES_TEMP_STOCK_ID = "stockID";
    public static final String SALES_TEMP_PRICE = "price";
    public static final String SALES_TEMP_QTY = "salesTempQty";
    public static final String SALES_TEMP_TIME = "time";
    public static final String SALES_TEMP_TYPE = "type";

    public static final String SALES_PURCHASE_TEMP_TABLE_NAME = "SalesPurchaseTemp";
    public static final String SALES_PURCHASE_SALES_TEMP_ID = "salesTempID";
    public static final String SALES_PURCHASE_PURCHASE_TEMP_ID = "purchaseTempID";
    //public static final String SALES_PURCHASE_SALES_TEMP_QTY = "salesTempQty";
    public static final String SALES_PURCHASE_PURCHASE_TEMP_QTY = "purchaseTempQty";
    public static final String SALES_PURCHASE_PURCHASE_TEMP_PRICE = "purchaseTempPrice";
    public static final String SALES_PURCHASE_TEMP_STOCK_ID = "stockID";
    public static final String SALES_PURCHASE_SALES_TEMP_TYPE = "salesTempType";
    public static final String SALES_PURCHASE_TEMP_DETAIL_ID = "detailID";

    public static String VALUATION_METHOD;
    public static Long currentUserId = 1l;
    public static String roleOfCurrentUser = "Admin";
    public static String CURRENCY = "MK";
    public static String SHARED_PREFERENCE_DB = "BusinessSuite.sqlite";
    public static final String DATABASE_NAME = "BusinessSuite.sqlite";
    public static final String SAMPLE_DATABASE_NAME = "SampleBusinessSuite.sqlite";

    public static final String DISCOUNT_TABLE_NAME = "Discount";
    public static final String DISCOUNT_ID = "id";
    public static final String DISCOUNT_NAME = "name";
    public static final String DISCOUNT_AMOUNT = "amount";
    public static final String DISCOUNT_IS_PERCENT = "isPercent";
    public static final String DISCOUNT_DESCRIPTION = "description";
    public static final String DISCOUNT_IS_DEFAULT = "isDefault";
    public static final String DISCOUNT_CUSTOM_FIELD_1 = "customField1";
    public static final String DISCOUNT_CUSTOM_FIELD_2 = "customField2";
    public static final String DISCOUNT_CUSTOM_FIELD_3 = "customField3";
    public static final String DISCOUNT_CUSTOM_FIELD_4 = "customField4";
    public static final String DISCOUNT_CUSTOM_FIELD_5 = "customField5";


    public static final String LICENSE_INFO_TABLE_NAME = "LicenseInfo";
    public static final String LICENSE_INFO_START_DATE = "startDate";
    public static final String LICENSE_INFO_END_DATE = "endDate";
    public static final String LICENSE_INFO_DURATION = "duration";

    public static final String REGISTRATION_TABLE_NAME = "Registration";
    public static final String REGISTRATION_USER_ID = "userID";
    public static final String REGISTRATION_USER_STATUS = "userStatus";
    public static final String REGISTRATION_USERNAME = "userName";
    public static final String REGISTRATION_BUSINESS_NAME = "businessName";

    public static final String PASSCODE_TABLE_NAME = "Passcode";
    public static final String PASSCODE_ID = "id";
    public static final String PASSCODE_PASSCODE = "passcode";

    public static final String API_DATABASE = "FusionApi.sqlite";


    /*
    User roles table
     */
    public static final String TABLE_USER_ROLES   = "userRoles";
    public static final String COLUMN_USER_ID = "id";
    public static final String COLUMN_USER_NAME = "name";
    public static final String COLUMN_USER_PASSCODE = "passcode";
    public static final String COLUMN_USER_ROLE = "role";
    public static final String COLUMN_USER_DESCRIPTION = "description";
    public static final String COLUMN_USER_PICTURE = "picture";
    public static final String COLUMN_USER_LAST_LOGIN = "lastLogin";


    /*
    Authorized devices table
     */
    public static final String TABLE_AUTHORIZED_DEVICES = "authorized_devices";
    public static final String COLUMN_DEVICE_ID = "id";
    public static final String COLUMN_DEVICE_SERIAL_NO = "serial_no";
    public static final String COLUMN_DEVICE_NAME = "device_name";
    public static final String COLUMN_DEVICE_PERMISSION = "permission";
    public static final String PERMISSION_ALLOWED = "Allowed";
    public static final String PERMISSION_BLOCKED = "Blocked";

    /*
    Access logs table
     */
    public static final String TABLE_ACCESS_LOGS = "access_logs";
    public static final String COLUMN_ACCESS_ID = "id";
    public static final String COLUMN_ACCESS_USER_ID = "user_id";
    public static final String COLUMN_ACCESS_DEVICE_ID = "device_id";
    public static final String COLUMN_ACCESS_DATE_TIME = "date_time";
    public static final String COLUMN_ACCESS_EVENT = "event";

    public static final String EVENT_IN = "in";
    public static final String EVENT_OUT = "out";

    /*
    Response codes
    UMM. THE ACTUAL CODES AIN'T IMPORTANT HERE
     */
    public static final int RESPONSE_ALLOWED = 200;
    public static final int RESPONSE_BLOCKED = 403;
    public static final int RESPONSE_FULL = 423;
    public static final int RESPONSE_NOT_FOUND = 404;

    /*
    Shared preference for last logged in user info
     */
    public static final String PREF_LAST_LOGIN_INFO = "LastLoginInfo";
    public static final String KEY_USER_ID          = "user_id";
    public static final String KEY_USER_NAME        = "user_name";
    public static final String KEY_USER_ROLE        = "user_role";
    public static final String KEY_LOGIN_TIME       = "login_time";
}

