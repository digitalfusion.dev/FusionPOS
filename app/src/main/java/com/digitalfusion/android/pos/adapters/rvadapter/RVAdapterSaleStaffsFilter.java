package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.User;

import java.util.List;

/**
 * Created by MD003 on 9/9/16.
 */
public class RVAdapterSaleStaffsFilter extends RecyclerView.Adapter<RVAdapterSaleStaffsFilter.FilterViewHolder> {
    List<User> userList;

    private OnItemClickListener mItemClickListener;
    private RadioButton oldRb;
    private int currentPos;


    public RVAdapterSaleStaffsFilter(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_item_view, parent, false);

        return new FilterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FilterViewHolder holder, final int position) {

        holder.name.setText(userList.get(position).getUserName());
        if (position == currentPos && currentPos != -1) {
            oldRb = holder.rb;
            holder.rb.setChecked(true);
        } else {
            holder.rb.setChecked(false);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {

                    if (oldRb != null) {
                        oldRb.setChecked(false);
                        oldRb = holder.rb;
                        holder.rb.setChecked(true);
                    }

                    mItemClickListener.onItemClick(v, position);
                }
            }

        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setCurrentPos(int currentPos) {
        if (currentPos < 0 && currentPos > userList.size() - 1) {
            //throw new Exception("Current Position can't be greater than array size or smaller than 0");
        } else {
            this.currentPos = currentPos;
            notifyDataSetChanged();

            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(null, currentPos);
            }
        }

    }

    public void setFilterNameList(List<User> userList) {
        this.userList = userList;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class FilterViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        RadioButton rb;

        FilterViewHolder(View itemView) {
            super(itemView);
            rb   = (RadioButton) itemView.findViewById(R.id.rb  );
            name = (TextView   ) itemView.findViewById(R.id.name);
        }
    }
}
