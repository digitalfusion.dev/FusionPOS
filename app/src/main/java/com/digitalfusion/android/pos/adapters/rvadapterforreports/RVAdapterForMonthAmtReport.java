package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD002 on 12/3/16.
 */

public class RVAdapterForMonthAmtReport extends ParentRVAdapterForReports {

    private final int HEADER_TYPE = 10000;
    //private List<ReportItem> expenseReportItemViewList;
    protected String[] mMonths = new String[]{
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    };
    private List<ReportItem> reportItemList;

    public RVAdapterForMonthAmtReport(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_month_amount_header, parent, false);

            return new HeaderItemView(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_month_amount, parent, false);

            return new ReportItemViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ReportItemViewHolder) {
            Log.w("month", reportItemList.get(position - 1).getMonth() + " ss");

            ReportItemViewHolder rholder = (ReportItemViewHolder) holder;

            rholder.monthTextView.setText(mMonths[reportItemList.get(position - 1).getMonth() - 1]);

            rholder.amtTextView.setText(POSUtil.NumberFormat(reportItemList.get(position - 1).getBalance()));
            //holder.expenseTextView.setText(Double.toString(expenseReportItemViewList.get(position).getBalance()));
        }
    }

    @Override
    public int getItemCount() {
        return reportItemList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_TYPE;
        } else {
            return 1;
        }
    }

    public List<ReportItem> getReportItemList() {
        return reportItemList;
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public class ReportItemViewHolder extends RecyclerView.ViewHolder {
        TextView monthTextView;
        TextView amtTextView;
        //TextView expenseTextView;

        View view;

        public ReportItemViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            monthTextView = (TextView) itemView.findViewById(R.id.month_text_view);

            amtTextView = (TextView) itemView.findViewById(R.id.amount_text_view);

            //expenseTextView = (TextView) itemView.findViewById(R.id.expense_text_view);

        }

    }

    public class HeaderItemView extends RecyclerView.ViewHolder {

        View view;

        public HeaderItemView(View itemView) {
            super(itemView);

            this.view = itemView;


        }

    }

}
