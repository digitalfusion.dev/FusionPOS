package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.database.dao.LostDAO;
import com.digitalfusion.android.pos.database.dao.LostDetailDAO;
import com.digitalfusion.android.pos.database.model.LostItem;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InvoiceNoGenerator;

import java.util.List;

/**
 * Created by MD002 on 8/19/16.
 */
public class LostManager {
    private Context context;
    private LostDAO lostDAO;
    private LostDetailDAO lostDetailDAO;
    private InvoiceNoGenerator invoiceNoGenerator;

    public LostManager(Context context) {
        this.context = context;
        lostDAO = LostDAO.getLostDaoInstance(context);
        lostDetailDAO = LostDetailDAO.getLostDetailDaoInstance(context);
        invoiceNoGenerator = new InvoiceNoGenerator(context);
    }

    public boolean addNewLost(Long stockID, int stockQty, String remark, String day, String month, String year, Long userId) {
        boolean flag = lostDAO.addNewLost(stockID, DateUtility.makeDate(year, month, day), stockQty, remark, day, month, year, userId);

        return flag;
    }

    public String getLostInvoiceNo() {
        return invoiceNoGenerator.getInvoiceNo(AppConstant.LOST_INVOICE_NO);
    }

    public List<LostItem> getAllLosts(int startLimit, int endLimit) {
        return lostDAO.getAllLosts(startLimit, endLimit);
    }

    public List<LostItem> getLostListByDateRange(String startDate, String endDate, int startLimit, int endLimit) {
        return lostDAO.getAllLostsByDateRange(startDate, endDate, startLimit, endLimit);
    }

    /*public List<LostItemInDetail> getLostDetailByLostID(Long lostID){
        return lostDetailDAO.getAllLostDetailByLostID(lostID);
    }*/

    public boolean updateLost(Long id, Long stockID, int stockQty, String remark, Long userID,
                              String day, String month, String year) {
        return lostDAO.updateLost(id, DateUtility.makeDate(year, month, day), stockID, stockQty, remark, userID, day, month, year);
    }

    public boolean deleteLost(Long id) {
        return lostDAO.deleteLost(id);
    }


   /* public List<LostItem> getAllLostsByDateRangeOnSearch(String startDate, String endDate, int startLimit, int endLimit, String searchStr){
        return lostDAO.getAllLostsByDateRangeOnSearch(startDate, endDate, startLimit, endLimit, searchStr);
    }*/

    public List<LostItem> getLostByDateRangeOnSearch(String startDate, String endDate, int startLimit, int endLimit, Long id) {
        return lostDAO.getLostByDateRangeOnSearch(startDate, endDate, startLimit, endLimit, id);
    }
}
