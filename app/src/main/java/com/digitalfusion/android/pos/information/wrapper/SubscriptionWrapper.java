package com.digitalfusion.android.pos.information.wrapper;

import com.digitalfusion.android.pos.information.AppInfo;
import com.digitalfusion.android.pos.information.DeviceInfo;
import com.digitalfusion.android.pos.information.PaymentInfo;

public class SubscriptionWrapper {
    private String userId;
    private long days;
    private PaymentInfo paymentInfo;
    private AppInfo appInfo;
    private DeviceInfo deviceInfo;

    public SubscriptionWrapper(String userId, long days, PaymentInfo paymentInfo) {
        this.userId = userId;
        this.days = days;
        this.paymentInfo = paymentInfo;
    }

    public SubscriptionWrapper() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getDays() {
        return days;
    }

    public void setDays(long days) {
        this.days = days;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public AppInfo getAppInfo() {
        return appInfo;
    }

    public void setAppInfo(AppInfo appInfo) {
        this.appInfo = appInfo;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    @Override
    public String toString() {
        return "SubscriptionWrapper{" +
                "userId='" + userId + '\'' +
                ", days=" + days +
                ", paymentInfo=" + paymentInfo +
                '}';
    }
}
