package com.digitalfusion.android.pos.adapters.rvadapter;

import android.content.Context;
import android.os.Vibrator;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.StockImage;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD003 on 11/30/16.
 */

public class RVAdapterForImageList extends RecyclerView.Adapter<RVAdapterForImageList.StockImageView> implements View.OnTouchListener {


    List<StockImage> stockImageList;

    private OnItemClickListener deletClickListener;

    private OnItemClickListener mClickListener;

    private boolean editMode;

    private boolean longClickEditModeOn;

    private RadioButton oldRb;


    public RVAdapterForImageList(List<StockImage> stockImageList) {
        this.stockImageList = stockImageList;
    }

    @Override
    public StockImageView onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_image_view, parent, false);

        return new StockImageView(view);
    }

    @Override
    public void onBindViewHolder(final StockImageView holder, final int position) {

        holder.imageView.setImageBitmap(POSUtil.getBitmapFromByteArray(stockImageList.get(position).getImage()));

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (longClickEditModeOn) {
                    editModeOff();
                }
            }
        });

        holder.imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if (longClickEditModeOn) {
                    Vibrator b = (Vibrator) v.getContext().getSystemService(Context.VIBRATOR_SERVICE);
                    b.vibrate(100);
                    editModeOn();
                }

                return false;
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(position);
                }
            }
        });

        if (editMode) {
            holder.deleteButton.setVisibility(View.VISIBLE);
        } else {
            holder.deleteButton.setVisibility(View.GONE);
        }
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (deletClickListener != null) {
                    deletClickListener.onItemClick(position);
                }
            }
        });

    }

    public OnItemClickListener getmClickListener() {
        return mClickListener;
    }

    public void setmClickListener(OnItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    @Override
    public int getItemCount() {

        return stockImageList.size();

    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        Log.w("on touch", "touch");

        if (longClickEditModeOn) {
            editModeOff();
        }

        return false;
    }

    public List<StockImage> getStockImageList() {
        return stockImageList;
    }

    public void setStockImageList(List<StockImage> stockImageList) {
        this.stockImageList = stockImageList;
    }

    public OnItemClickListener getDeletClickListener() {
        return deletClickListener;
    }

    public void setDeletClickListener(OnItemClickListener deletClickListener) {
        this.deletClickListener = deletClickListener;
    }

    private void editModeOn() {
        editMode = true;
        notifyDataSetChanged();
    }

    private void editModeOff() {
        editMode = false;
        notifyDataSetChanged();
    }

    public boolean isLongClickEditModeOn() {
        return longClickEditModeOn;
    }

    public void setLongClickEditModeOn(boolean longClickEditModeOn) {
        this.longClickEditModeOn = longClickEditModeOn;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public class StockImageView extends RecyclerView.ViewHolder {

        ImageView imageView;

        ImageButton deleteButton;

        View itemView;

        public StockImageView(View itemView) {

            super(itemView);

            this.itemView = itemView;

            imageView = (ImageView) itemView.findViewById(R.id.img);

            deleteButton = (ImageButton) itemView.findViewById(R.id.delete_img_btn);

        }
    }
}
