package com.digitalfusion.android.pos.database.model;

import android.util.Log;

import java.io.Serializable;

/**
 * Created by MD002 on 6/19/17.
 */

public class Discount implements Serializable {
    private Long id;
    private String name;
    private Double amount;
    private int isPercent;
    private String description;
    private int isDefault;

    public Discount() {
        isPercent = 1;
        amount = 0.0;
    }

    public Discount(Long id, String name, Double amount) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        Log.e("AMOUNT IN COUNT", amount + " ANT");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public int getIsPercent() {
        return isPercent;
    }

    public void setIsPercent(int isPercent) {
        this.isPercent = isPercent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }

    public boolean isActive() {
        return isDefault == 1;
    }

    public boolean isNull() {
        return this.getId() == null;
    }
}
