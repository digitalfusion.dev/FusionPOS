package com.digitalfusion.android.pos.information;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by MD003 on 8/15/17.
 */

public class PaymentInfo implements Serializable {

    private String agentName;
    private String bank;
    private Long amount;
    private Date date;

    public PaymentInfo(String agentName, String bank, Long amount, Date date) {
        this.agentName = agentName;
        this.bank = bank;
        this.amount = amount;
        this.date = date;
    }

    public PaymentInfo() {
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "PaymentInfo{" +
                "agentName='" + agentName + '\'' +
                ", bank='" + bank + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                '}';
    }
}
