package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.model.Unit;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/17/16.
 */
public class UnitDAO extends ParentDAO {
    private static ParentDAO unitDaoInstance;
    private Context context;
    private IdGeneratorDAO idGeneratorDAO;
    private Unit unit;
    private List<Unit> unitList;

    private UnitDAO(Context context) {
        super(context);
        this.context = context;
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        unit = new Unit();
    }

    public static UnitDAO getUnitDaoInstance(Context context) {
        if (unitDaoInstance == null) {
            unitDaoInstance = new UnitDAO(context);
        }
        return (UnitDAO) unitDaoInstance;
    }

    public Long addNewUnit(String unit, String description, InsertedBooleanHolder flag) {
        id = checkUnitAlreadyExist(unit);
        if (id == null) {
            genID = idGeneratorDAO.getLastIdValue("Unit");
            genID += 1;
            query = "insert into " + AppConstant.UNIT_TABLE_NAME + "(" + AppConstant.UNIT_ID + ", " + AppConstant.UNIT_UNIT + ", " + AppConstant.UNIT_DESCRIPTION
                    + ", " + AppConstant.CREATED_DATE + ") values (?,?,?,?)";
            databaseWriteTransaction(flag);
            try {
                Log.e("id", genID + "idoa");
                statement = database.compileStatement(query);
                statement.bindLong(1, genID);
                statement.bindString(2, unit);
                if (description == null) {
                    statement.bindNull(3);
                } else {
                    statement.bindString(3, description);
                }
                statement.bindString(4, DateUtility.getTodayDate());
                statement.execute();
                statement.clearBindings();
                database.setTransactionSuccessful();
                id = genID;
            } catch (SQLiteException e) {
                e.printStackTrace();
            } finally {
                database.endTransaction();
                databaseHelper.close();
            }
        }
        Log.e("ini", flag.isInserted() + "rfasdfas");
        return id;
    }

    public boolean deleteUnit(Long id) {
        query = "delete from " + AppConstant.UNIT_TABLE_NAME + " where " + AppConstant.UNIT_ID + " = " + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public Long checkUnitAlreadyExist(String unit) {
        query = " select " + AppConstant.UNIT_ID + " from " + AppConstant.UNIT_TABLE_NAME + " where " + AppConstant.UNIT_UNIT + " = ?;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{unit});

            if (cursor.moveToFirst()) {
                return cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            cursor.close();
        }
        databaseHelper.close();
        return null;
    }

    public List<Unit> getAllUnits() {
        unitList = new ArrayList<>();
        query = "select " + AppConstant.UNIT_ID + ", " + AppConstant.UNIT_UNIT + ", " + AppConstant.UNIT_DESCRIPTION + " from " + AppConstant.UNIT_TABLE_NAME;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    unit = new Unit();
                    unit.setId(cursor.getLong(0));
                    unit.setUnit(cursor.getString(1));
                    unit.setDescription(cursor.getString(2));
                    unitList.add(unit);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            cursor.close();
        }
        databaseHelper.close();
        return unitList;
    }

    public boolean updateUnit(String unit, String description, Long id) {
        query = "update " + AppConstant.UNIT_TABLE_NAME + " set " + AppConstant.UNIT_UNIT + " = ?, " + AppConstant.UNIT_DESCRIPTION + " = ? where " + AppConstant.UNIT_ID + " = " + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{unit, description});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            databaseHelper.close();
        }
        return flag.isInserted();
    }
}
