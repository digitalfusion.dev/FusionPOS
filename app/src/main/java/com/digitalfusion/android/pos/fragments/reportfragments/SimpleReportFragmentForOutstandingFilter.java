package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForOutstandingHeaderReport;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SimpleReportFragmentForOutstandingFilter extends Fragment implements Serializable {
    private String reportName;
    private View mainLayoutView;
    private Context context;
    private RecyclerView recyclerView;
    private ReportManager reportManager;
    private List<ReportItem> reportItemList;
    private ParentRVAdapterForReports rvAdapterForReport;
    private List<String> outstandingFilterList;
    private MaterialDialog filterMaterialDialog;
    private RVAdapterForFilter rvAdapterForFilter;
    private String startDate;
    private String endDate;
    private TextView filterTextView;
    private int token = -1;

    private int startLimit;
    private int endLimit;
    private boolean shouldLoad = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_simple_report, container, false);
        if (getArguments() != null) {
            reportName = getArguments().getString("reportType");
        }
        if (reportName.equalsIgnoreCase("receivable header")) {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.receivable}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        } else if (reportName.equalsIgnoreCase("payable header")) {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.payable}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        }

        loadIngUI();

        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        initializeVariables();

        new LoadProgressDialog().execute("");

    }

    private void loadIngUI() {
        recyclerView = mainLayoutView.findViewById(R.id.simple_report_recycler_view);
        filterTextView = mainLayoutView.findViewById(R.id.filter_text_view);
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);
        outstandingFilterList = new ArrayList<>();
        final String all = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);
        filterTextView.setText(all);
        outstandingFilterList.add(all);
        final String receivable = context.getTheme().obtainStyledAttributes(new int[]{R.attr.clear_outstanding}).getString(0);
        outstandingFilterList.add(receivable);
        final String no_receivable = context.getTheme().obtainStyledAttributes(new int[]{R.attr.unclear_outstanding}).getString(0);
        outstandingFilterList.add(no_receivable);
        rvAdapterForFilter = new RVAdapterForFilter(outstandingFilterList);
        filterMaterialDialog = new MaterialDialog.Builder(context)
                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
        setDatesAndLimits();
        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterMaterialDialog.show();
            }
        });
        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (outstandingFilterList.get(position).equalsIgnoreCase(all)) {
                    token = -1;
                    filterTextView.setText(all);
                } else if (outstandingFilterList.get(position).equalsIgnoreCase(receivable)) {
                    token = 0;
                    filterTextView.setText(receivable);
                } else if (outstandingFilterList.get(position).equalsIgnoreCase(no_receivable)) {
                    token = 1;
                    filterTextView.setText(no_receivable);
                }
                filterMaterialDialog.dismiss();
                recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
                    @Override
                    public void onScrollUp() {

                    }

                    @Override
                    public void onScrollDown() {

                    }

                    @Override
                    public void onLoadMore() {


                        Log.e("IN SRF ", "Load more");
                        if (shouldLoad) {

                            rvAdapterForReport.setShowLoader(true);

                            loadmore();

                        }

                    }
                });
                new LoadProgressDialog().execute("");
            }
        });
        recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {


                Log.e("IN SRF ", "Load more");
                if (shouldLoad) {

                    rvAdapterForReport.setShowLoader(true);

                    loadmore();

                }

            }
        });

    }

    private void initializeList() {
        if (reportName.equalsIgnoreCase("receivable header")) {
            reportItemList = reportManager.receivableHeaderList(startLimit, endLimit, token);
        } else if (reportName.equalsIgnoreCase("payable header")) {
            reportItemList = reportManager.payableHeaderList(startLimit, endLimit, token);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void setDatesAndLimits() {
        //if (!isTwelve){
        startDate = Calendar.getInstance().get(Calendar.YEAR) + "0101";
        //Log.e("start", startDate );
        endDate = Calendar.getInstance().get(Calendar.YEAR) + "1231";
        Log.e("start", endDate);
        //}
        startLimit = 0;
        endLimit = 10;
    }

    private void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        if (reportName.equalsIgnoreCase("receivable header")) {
            Log.e("fragment", "header");
            rvAdapterForReport = new RVAdapterForOutstandingHeaderReport(reportItemList);
            ((RVAdapterForOutstandingHeaderReport) rvAdapterForReport).setPayable(false);
            ((RVAdapterForOutstandingHeaderReport) rvAdapterForReport).setRvAdapterForOutstandingHeaderClickListener(new RVAdapterForOutstandingHeaderReport.ClickListener() {
                @Override
                public void onClick(ReportItem reportItem) {
                    Bundle                              bundle                              = new Bundle();
                    PayableReceivableDetailViewFragment payableReceivableDetailViewFragment = new PayableReceivableDetailViewFragment();
                    bundle.putString("reportType", "receivable");
                    bundle.putSerializable("reportItem", reportItem);
                    //    MainActivity.replacingFragment(payableReceivableDetailViewFragment);
                    payableReceivableDetailViewFragment.setArguments(bundle);
                    // MainActivity.replacingFragment(simpleReportFragment);

                    bundle.putSerializable("frag", payableReceivableDetailViewFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                }
            });
            recyclerView.setAdapter(rvAdapterForReport);
        } else if (reportName.equalsIgnoreCase("payable header")) {
            rvAdapterForReport = new RVAdapterForOutstandingHeaderReport(reportItemList);
            ((RVAdapterForOutstandingHeaderReport) rvAdapterForReport).setPayable(true);
            ((RVAdapterForOutstandingHeaderReport) rvAdapterForReport).setRvAdapterForOutstandingHeaderClickListener(new RVAdapterForOutstandingHeaderReport.ClickListener() {
                @Override
                public void onClick(ReportItem reportItem) {
                    Bundle                              bundle                              = new Bundle();
                    PayableReceivableDetailViewFragment payableReceivableDetailViewFragment = new PayableReceivableDetailViewFragment();
                    bundle.putString("reportType", "payable");
                    bundle.putSerializable("reportItem", reportItem);
                    //  payableReceivableDetailViewFragment.setArguments(bundle);
                    // MainActivity.replacingFragment(payableReceivableDetailViewFragment);
                    payableReceivableDetailViewFragment.setArguments(bundle);
                    // MainActivity.replacingFragment(simpleReportFragment);

                    bundle.putSerializable("frag", payableReceivableDetailViewFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                }
            });
            recyclerView.setAdapter(rvAdapterForReport);
        }
        //recyclerView.setAdapter(rvAdapterForValuation);
    }

    public void loadmore() {
        Log.e("load", "more");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                reportItemList.addAll(loadMore(reportItemList.size(), 9));

                Log.w("HErer size", reportItemList.size() + " Ss");

                rvAdapterForReport.setShowLoader(false);

                configRecyclerView();
            }
        }, 500);
    }

    public List<ReportItem> loadMore(int startLimit, int endLimit) {
        List<ReportItem> reportItemList = new ArrayList<>();
        if (reportName.equalsIgnoreCase("receivable header")) {
            reportItemList = reportManager.receivableHeaderList(startLimit, endLimit, token);
        } else if (reportName.equalsIgnoreCase("payable header")) {
            reportItemList = reportManager.payableHeaderList(startLimit, endLimit, token);
        }
        return reportItemList;
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String label = "";

            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {
            configRecyclerView();
            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
