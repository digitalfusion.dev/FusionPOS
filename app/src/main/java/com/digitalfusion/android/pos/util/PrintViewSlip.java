/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitalfusion.android.pos.util;

import android.os.AsyncTask;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;

import com.digitalfusion.android.pos.activities.MainActivity;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author HtetAung
 */

public class PrintViewSlip {

    HashMap<Integer, byte[]> hm = new HashMap<Integer, byte[]>();
    private String printString = "\n";
    private List<String> printStringList = new ArrayList<>();
    private PrintListener printListener;
    private TableBuilder tableBuilder;

    public PrintViewSlip() {

        nextLine();
    }

    private byte[] boldFormat() {

        byte[] arrayOfByte2 = {27, 33, 0};

        byte[] big = {27, 33, 0};

        big[2] = ((byte) (0x8 | arrayOfByte2[2]));

        return big;
    }

    private byte[] bigFormat() {

        byte[] arrayOfByte2 = {27, 33, 0};

        byte[] big = {27, 33, 0};

        big[2] = ((byte) (0x10 | arrayOfByte2[2]));

        return big;
    }

    private byte[] normalFormat() {

        return new byte[]{27, 33, 0};
    }

    public void nextLine() {
        printString += "\n";
        printStringList.add("\n");
    }

    public void addSpace(int count) {
        String temp = "";
        for (int i = 0; i < count; i++) {
            temp += " ";
        }

        printString += temp;

        printStringList.add(temp);
    }

    public void addTabSpace() {
        printString += "   ";
        printStringList.add("   ");

    }

    /*
    add string at after current string
    */
    public void addString(String string) {

        printString += string;

        printStringList.add(string);
    }

    public void addString(STYLE style, String string) {

        switch (style) {
            case BOLD: {
                addStringBold(string);
            }
            break;
            case BIG: {
                addStringBig(string);
            }
            break;
            default: {
                addString(string);
            }
            break;
        }

    }

    public void addStringBold(String string) {

        printString += string;

        hm.put(printStringList.size(), boldFormat());

        printStringList.add(string);

    }

    public void addStringBold(String string, Alignment alignment) {

        printString += string;

        hm.put(printStringList.size(), boldFormat());

        if (alignment == Alignment.LEFT) {
            addString(string);
        } else if (alignment == Alignment.CENTER) {
            addStringAtCenter(string);
        } else if (alignment == Alignment.RIGHT) {
            addStringAtRightMost(string);
        }
    }

    public void addStringBig(String string) {

        printString += string;

        hm.put(printStringList.size(), bigFormat());

        printStringList.add(string);

    }

    public void addStringBig(String string, Alignment alignment) {

        printString += string;

        hm.put(printStringList.size(), bigFormat());

        if (alignment == Alignment.LEFT) {
            addString(string);
        } else if (alignment == Alignment.CENTER) {
            addStringAtCenter(string);
        } else if (alignment == Alignment.RIGHT) {
            addStringAtRightMost(string);
        }
    }

    public void addString(String string, Alignment alignment) {
        if (alignment == Alignment.LEFT) {
            addString(string);
        } else if (alignment == Alignment.CENTER) {
            addStringAtCenter(string);
        } else if (alignment == Alignment.RIGHT) {
            addStringAtRightMost(string);
        }
    }

    public void addFullColon() {
        printString += ":";
        printStringList.add(":");
    }

    public void addDividerLine() {
        printString += "------------------------------------------------";

        printStringList.add("------------------------------------------------");

    }

    public void addStringAtLeftMost(String s) {

        int    offset = countCharacterAfterNextLine();
        String temp   = "";
        for (int i = 0; i < 48 - s.length() - offset; i++) {

            temp += " ";

        }
        printString += temp + s;
        printStringList.add(temp + s);


    }

    public void addStringAtRightMost(String s) {

        int offset = countCharacterAfterNextLine();

        String temp = "";

        for (int i = 0; i < 48 - s.length() - offset; i++) {

            temp += " ";

        }

        printString += temp + s;

        printStringList.add(temp + s);


    }

    public void addStringAtCenter(String s) {

        int offset = (48 - s.length()) / 2;

        String temp = "";

        for (int i = 0; i < offset; i++) {

            temp += " ";

        }

        printString += temp + s + temp;
        printStringList.add(temp + s + temp);


    }

    private int findLatestNextLine() {
        return printString.lastIndexOf("\n");
    }

    private int countCharacterAfterNextLine() {


        return printString.length() - findLatestNextLine() - 1;

    }

    public void BuildTable(int columnCounts) {
        tableBuilder = new TableBuilder(columnCounts);
    }

    public void addNewColumn(String name, int width, Alignment alignment) {
        tableBuilder.addColumn(new Column(name, width, alignment));
    }

    public void addNewColumn(String name, int width, Alignment alignment, int start) {
        tableBuilder.addColumn(new Column(name, width, alignment, start));
    }

    public void addNewColumn(String name, int width) {
        tableBuilder.addColumn(new Column(name, width));
    }

    public void addRow(STYLE style, String... s) {
        tableBuilder.printRow(style, s);
    }

    public void addRow(String... s) {
        tableBuilder.printRow(STYLE.DEFAULT, s);
    }

    public void addHeader() {
        tableBuilder.addHeader();
    }

    public String getPrintString() {
        return printString;
    }

    public void show() {
        // System.out.println(printString);

        for (String s : printStringList) {
            System.out.print(s);
        }


    }

    public void printNow(OutputStream outputStream) {
        Log.e("print", "now");
        new PrintingProgress().execute(outputStream);
    }

    public void setPrintListener(PrintListener printListener) {
        this.printListener = printListener;
    }

    public enum Alignment {
        CENTER, RIGHT, LEFT
    }


    public enum STYLE {
        BOLD, BIG, DEFAULT
    }

    public interface PrintListener {
        void onPrepare();

        void onSuccess();

        void onFailuare();

    }

    public class TableBuilder {

        int columnCount;
        List<Column> columnList = new ArrayList<>();


        public TableBuilder(int columnCount) {
            this.columnCount = columnCount;
        }

        public void addColumn(Column column) {
            if (columnList.size() > columnCount) {
                System.out.println("You can't add column more than columncount");
            } else {
                columnList.add(column);
            }
            checkValidColum();
        }

        public void checkValidColum() {

            int totalColumnCount = 0;

            for (Column c : columnList) {
                totalColumnCount += c.columnWidth;
            }

            if (totalColumnCount > 48) {
                System.out.println("Column count is greater than slip can print");
            }
        }

        public void printRow(STYLE style, String... s) {

            nextLine();

            switch (style) {
                case BOLD: {
                    for (int i = 0; i < columnCount; i++) {
                        //if(s[i].length()<=columnList.get(i).columnWidth){
                        //   printString+=s[i];
                        // }

                        columnList.get(i).addStringBold(s[i]);

                    }
                }
                break;
                case BIG: {
                    for (int i = 0; i < columnCount; i++) {
                        //if(s[i].length()<=columnList.get(i).columnWidth){
                        //   printString+=s[i];
                        // }

                        columnList.get(i).addStringBig(s[i]);

                    }
                }
                break;
                default: {
                    for (int i = 0; i < columnCount; i++) {
                        //if(s[i].length()<=columnList.get(i).columnWidth){
                        //   printString+=s[i];
                        // }

                        columnList.get(i).addString(s[i]);

                    }
                }
                break;
            }

            nextLine();


        }

        public void addHeader() {

            //add table header border
            nextLine();
            addDividerLine();
            nextLine();

            for (int i = 0; i < columnCount; i++) {
                //if(s[i].length()<=columnList.get(i).columnWidth){
                //   printString+=s[i];
                // }

                columnList.get(i).addString(columnList.get(i).columnName);

            }

            //add underline table header border
            nextLine();
            addDividerLine();


        }

    }

    public class Column {

        String columnName;

        int start;

        int columnWidth;

        Alignment alignment;

        public Column(String columnName, int columnWidth) {
            this.columnName = columnName;
            this.columnWidth = columnWidth;
            this.alignment = Alignment.RIGHT;
        }

        public Column(String columnName, int columnWidth, int start) {
            this.columnName = columnName;
            this.columnWidth = columnWidth;
            this.alignment = Alignment.RIGHT;
            this.start = start;
        }

        public Column(String columnName, int columnWidth, Alignment alignment) {
            this.columnName = columnName;
            this.columnWidth = columnWidth;
            this.alignment = alignment;
        }

        public Column(String columnName, int columnWidth, Alignment alignment, int start) {
            this.columnName = columnName;
            this.columnWidth = columnWidth;
            this.alignment = alignment;
            this.start = start;
        }

        public void addString(String s) {
            if (alignment == Alignment.RIGHT) {
                addStringAtRight(s);
            } else if (alignment == Alignment.LEFT) {
                addStringAtLeft(s);
            } else {
                addStringAtCenter(s);
            }
        }

        public void addStringBold(String s) {

            hm.put(printStringList.size(), boldFormat());

            if (alignment == Alignment.RIGHT) {
                addStringAtRight(s);
            } else if (alignment == Alignment.LEFT) {
                addStringAtLeft(s);
            } else {
                addStringAtCenter(s);
            }
        }

        public void addStringBig(String s) {

            hm.put(printStringList.size(), bigFormat());

            if (alignment == Alignment.RIGHT) {
                addStringAtRight(s);
            } else if (alignment == Alignment.LEFT) {
                addStringAtLeft(s);
            } else {
                addStringAtCenter(s);
            }
        }


        public void addStringAtLeft(String s) {

            String temp = "";

            makeStringWrapWithinColumnWidth(s, true);

          /*  if(s.length()>columnWidth){



            }else {
                for (int i = 0; i < columnWidth - s.length(); i++) {
                    temp += " ";
                }

                printString += s + temp;
                printStringList.add(s + temp);
            }*/


        }


        public void makeStringWrapWithinColumnWidth(String s, boolean firstTime) {

            Log.e("START", start + " S");

            String temp = "";
            if (s.length() > columnWidth) {

                String first = s.substring(0, columnWidth);

                String last = s.substring(columnWidth);

                if (!firstTime) {
                    for (int i = 0; i < start; i++) {
                        temp += " ";
                        Log.w("here", "makein space");
                    }
                }
                printStringList.add(temp + first + "\n");

                makeStringWrapWithinColumnWidth(last, false);

            } else {

                if (!firstTime) {
                    for (int i = 0; i < start; i++) {
                        temp += " ";
                        Log.w("here", "makein space in last time");
                    }
                }

                //add fore space
                printString += temp + s;
                printStringList.add(temp + s);
                temp = "";
                //make fill needed last spaces
                for (int i = 0; i < columnWidth - s.length(); i++) {
                    temp += " ";
                }
                printStringList.add(temp);


            }

        }

        public void addStringAtRight(String s) {

            String temp = "";

            for (int i = 0; i < columnWidth - s.length(); i++) {
                temp += " ";
            }

            printString += temp + s;
            printStringList.add(temp + s);


        }

        public void addStringAtCenter(String s) {

            int offset = (columnWidth - s.length()) / 2;

            String temp = "";

            for (int i = 0; i < offset; i++) {
                temp += " ";
            }

            printString += temp + s + temp;
            printStringList.add(temp + s + temp);


        }

    }

    class PrintingProgress extends AsyncTask<OutputStream, String, String> {
        @Override
        protected String doInBackground(OutputStream... params) {

            try {

                Log.e("dklajoidf", "A joidfjaoif");
                //Thread.sleep(1000);

                OutputStream os = params[0];

                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(params[0], "UTF-8");

                byte[] printformat = {0x1B, 0x21, 00};

                os.write(printformat);

                for (int i = 0; i < printStringList.size(); i++) {

                    if (hm.get(i) != null) {


                        SpannableString s = new SpannableString(printStringList.get(i).toString());
                        s.setSpan(new TypefaceSpan(MainActivity.context, "Zawgyi-One.ttf"), 0, s.length(),
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        os.write(hm.get(i));
                        os.write(s.toString().getBytes());
                        os.write(normalFormat());

                    } else {
                        os.write(printStringList.get(i).getBytes());
                    }
                }
            } catch (IOException e) {
                if (printListener != null) {
                    printListener.onFailuare();
                }
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (printListener != null) {
                printListener.onPrepare();
            }
        }

        @Override
        protected void onPostExecute(String a) {

            if (printListener != null) {
                printListener.onSuccess();
            }
        }
    }
}
