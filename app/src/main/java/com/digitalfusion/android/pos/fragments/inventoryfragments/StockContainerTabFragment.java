package com.digitalfusion.android.pos.fragments.inventoryfragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.util.TabUtil;
import com.digitalfusion.android.pos.adapters.DamageAndLostViewPagerAdapter;
import com.example.searchview.MaterialSearchView;

/**
 * Created by MD003 on 10/18/16.
 */

public class StockContainerTabFragment extends Fragment {

    private View mainLayoutView;

    //Tab
    private TabLayout tabLayout;

    private DamageAndLostViewPagerAdapter damageAndLostViewPagerAdapter;
    private DamageListFragmentWithAddDailog damageListFragment;
    private LostListFragmentWithAddDialog lostListFragment;
    private StockListViewFragment stockListViewFragment;
    private ReorderListFragment reorderListFragment;


    private MaterialSearchView searchView;
    private StockManager stockManager;

    private int reorderCount;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainLayoutView = inflater.inflate(R.layout.damage_and_lost_tab_fragment, null);

        setHasOptionsMenu(true);

        stockManager = new StockManager(getContext());

        reorderCount = stockManager.getReorderCount();

        context = getContext();

        TypedArray inventory = context.getTheme().obtainStyledAttributes(new int[]{R.attr.inventory});
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(inventory.getString(0));

        loadUI();
        MainActivity.setCurrentFragment(this);
        setupTabLayout();

        MainActivity.replacingTabFragment(new StockListViewFragment());

        return mainLayoutView;

    }


    private void setupTabLayout() {

        damageListFragment = new DamageListFragmentWithAddDailog();
        lostListFragment = new LostListFragmentWithAddDialog();
        stockListViewFragment = new StockListViewFragment();
        reorderListFragment = new ReorderListFragment();

        //  tabLayout.addTab(tabLayout.newTab().setText("Stock"),true);
        // tabLayout.addTab(tabLayout.newTab().setText("Reorder"));
        //  tabLayout.addTab(tabLayout.newTab().setText("Damage"));
        //  tabLayout.addTab(tabLayout.newTab().setText("Lost"));

        TypedArray stock   = context.getTheme().obtainStyledAttributes(new int[]{R.attr.stock});
        TypedArray reorder = context.getTheme().obtainStyledAttributes(new int[]{R.attr.reorder});
        TypedArray damage  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.damaged});
        TypedArray lost    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.lost});

        tabLayout.addTab(tabLayout.newTab().setCustomView(TabUtil.renderTabView(mainLayoutView.getContext(), stock.getString(0), 0)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(TabUtil.renderTabView(mainLayoutView.getContext(), reorder.getString(0), reorderCount)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(TabUtil.renderTabView(mainLayoutView.getContext(), damage.getString(0), 0)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(TabUtil.renderTabView(mainLayoutView.getContext(), lost.getString(0), 0)));


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setCurrentTabFragment(int tabPostion) {
        switch (tabPostion) {
            case 0:
                MainActivity.replacingTabFragment(stockListViewFragment);
                break;
            case 1:
                MainActivity.replacingTabFragment(reorderListFragment);
                break;
            case 2:
                MainActivity.replacingTabFragment(damageListFragment);
                break;
            case 3:
                MainActivity.replacingTabFragment(lostListFragment);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.main, menu);

        MenuItem item = menu.findItem(R.id.action_search);


        searchView.setMenuItem(item);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void loadUIFromToolbar() {
        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        searchView.setCursorDrawable(attributeResourceId);
    }

    public void loadUI() {
        loadUIFromToolbar();
        tabLayout = (TabLayout) mainLayoutView.findViewById(R.id.tablayout);

    }


}