package com.digitalfusion.android.pos.activities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.AuthorizationManager;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.business.UserManager;
import com.digitalfusion.android.pos.database.model.AccessLog;
import com.digitalfusion.android.pos.database.model.Device;
import com.digitalfusion.android.pos.fragments.CustomerListFragment;
import com.digitalfusion.android.pos.fragments.ExpenseManager;
import com.digitalfusion.android.pos.fragments.SplashScreenFragment;
import com.digitalfusion.android.pos.fragments.SupplierListFragment;
import com.digitalfusion.android.pos.fragments.dashboardfragments.DashBoardWithFilter;
import com.digitalfusion.android.pos.fragments.inventoryfragments.CategoryFragment;
import com.digitalfusion.android.pos.fragments.inventoryfragments.DamageListFragmentWithAddDailog;
import com.digitalfusion.android.pos.fragments.inventoryfragments.LostListFragmentWithAddDialog;
import com.digitalfusion.android.pos.fragments.inventoryfragments.ReorderListFragment;
import com.digitalfusion.android.pos.fragments.inventoryfragments.StockContainerTabFragment;
import com.digitalfusion.android.pos.fragments.inventoryfragments.StockFormFragment;
import com.digitalfusion.android.pos.fragments.inventoryfragments.StockListViewFragment;
import com.digitalfusion.android.pos.fragments.inventoryfragments.UnitFragment;
import com.digitalfusion.android.pos.fragments.outstanding.PayableAndReceivableTabFragment;
import com.digitalfusion.android.pos.fragments.purchasefragments.AddEditNewPurchaseFragment;
import com.digitalfusion.android.pos.fragments.purchasefragments.PurchaseDetailFragment;
import com.digitalfusion.android.pos.fragments.purchasefragments.PurchaseListTabFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.BarChartFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.BarChartWithSearchViewFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.BarChartWithTwoBarsFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.BarChartWithYearViewFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.LineChartForPriceTrendFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.LineChartFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.LineChartWithSearchViewFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.OutstandingPaymentDetailFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.PayableReceivableDetailViewFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.PieChartFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.PurchaseOutstandingPaymentDetailFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.ReportTitleListFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.SalesOutstandingPaymentDetailFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.SimpleReportFragment;
import com.digitalfusion.android.pos.fragments.reportfragments.SimpleReportFragmentWithDateFilter;
import com.digitalfusion.android.pos.fragments.reportfragments.SimpleReportFragmentWithDateFilterSearch;
import com.digitalfusion.android.pos.fragments.reportfragments.SimpleReportFragmentWithSearchView;
import com.digitalfusion.android.pos.fragments.salefragments.AddEditNewSaleFragment;
import com.digitalfusion.android.pos.fragments.salefragments.SaleDetailFragment;
import com.digitalfusion.android.pos.fragments.salefragments.SaleListHistoryTabFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.BussinessSettingFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.CustomerSupplierTabFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.DataManagementFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.DocumentSettingFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.SettingFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.TaxSetupFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.ThemeSettingListViewFragment;
import com.digitalfusion.android.pos.fragments.settingfragment.UserRoleSettingFragment;
import com.digitalfusion.android.pos.database.model.BusinessSetting;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.BlurNavigationDrawer.BlurActionBarDrawerToggle;
import com.digitalfusion.android.pos.views.FusionToast;
import com.example.searchview.MaterialSearchView;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.StringHolder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import java.io.OutputStream;

//import com.digitalfusion.android.pos.fragments.reportfragments.ReportTitleListFragment;

public class MainActivity extends ParentActivity implements DrawerLayout.DrawerListener {

    private static final int ACCOUNT_SETTING = 200000000;
    private static final int LOG_OUT = 9000001;
    public static DrawerLayout drawerLayout;
    public static MainActivity instance;
    public static Fragment oldFragment;

    public static Fragment currentFragment;

    public static IDrawerItem selectedDrawerItem;

    public static Drawer result;
    public static Context context;
    public static View splView;
    public static View view;
    public static boolean close = false;
    private static MaterialSearchView searchView;
    public boolean navi = false;
    public Bundle savedInstanceState;
    public View rootView;
    private Toolbar toolbar;
    boolean doubleBackToExitPressedOnce = false;
    private String dashboardStr;
    private String saleStr;
    private String purchaseStr;
    private String inventoryStr;
    private String outstandingStr;
    private String contactsStr;
    private String expenseManagerStr;
    private String reportsStr;
    private String settingStr;
    private String accountSettingStr;
    private String subscribeStr;
    private String feedbackStr;
    private String logOutStr;
    private int theme;
    private BusinessSetting businessSetting;

    // ExpandableDrawerItem inventory =  new ExpandableDrawerItem().withName("Inventory").withSubItems(stockMenu.withLevel(2),catogoryMenu.withLevel(2),unitMenu.withLevel(2),damageAndLostMenu.withLevel(2),reorder.withLevel(2));
    private SettingManager settingManager;
    private PrimaryDrawerItem outstanding;
    private PrimaryDrawerItem deliveryMenu;
    private PrimaryDrawerItem expenseManagerMenu;
    private PrimaryDrawerItem reprotMenu;
    private PrimaryDrawerItem settingMenu;
    private PrimaryDrawerItem dashBoardMenu;
    private PrimaryDrawerItem contactsMenu;
    private TypedArray allTrans;
    private TypedArray allTrans2;
    private TypedArray allTrans3;
    private PrimaryDrawerItem saleMenu;
    private PrimaryDrawerItem purchaseMenu;
    private PrimaryDrawerItem stockMenu;
    private IDrawerItem drawerItems[];
    private User user;
    // ExpandableDrawerItem oustanding =  new ExpandableDrawerItem().withName("Oustanding").withSubItems(receivableMenu.withLevel(2),payableMenu.withLevel(2));
    private UserManager userManager;
    private ProfileDrawerItem drawerHeader;
    private ProfileSettingDrawerItem accountSetting;
    private ProfileSettingDrawerItem subscribeSetting;
    private ProfileSettingDrawerItem complainSetting;
    private MaterialDialog editAccountSetting;
    private EditText usernameAccountSetting;
    private EditText passwordAccountSetting;
    private EditText oldPasswordAccountSetting;
    private EditText confirmPasswordAccountSetting;
    private ImageButton showPasswordAccountSetting;
    private ImageButton showOldPasswordAccountSetting;
    private ImageButton showConfirmPasswordAccountSetting;
    private AppCompatImageButton changeUserButton;
    private TextView userNameView;
    private TextView userRoleView;
    private Button saveAccountSetting;
    private Button cancelAccountSetting;


    public static MainActivity getInstance() {
        return instance;
    }

    public static void upgradeBadgeInventory(int badge) {

        result.updateBadge(3, new StringHolder(Integer.toString(badge)));

    }

    public static void replacingFragment(Fragment fragment) {


        splView.setVisibility(View.GONE);
        view.setVisibility(View.VISIBLE);

        oldFragment = currentFragment;

        currentFragment = fragment;

        if (searchView != null) {
            searchView.closeSearch();

            searchView.hideKeyboard(searchView);
        }


        final FragmentTransaction fragmentTransaction = instance.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        fragmentTransaction.replace(R.id.frame_replace, fragment);

        fragmentTransaction.addToBackStack("back");

        fragmentTransaction.commit();

    }

    public static void replacingTabFragment(Fragment fragment) {

        oldFragment = currentFragment;

        currentFragment = fragment;

        if (searchView != null) {

            searchView.closeSearch();

            searchView.hideKeyboard(searchView);
        }

        final FragmentTransaction fragmentTransaction = instance.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        fragmentTransaction.replace(R.id.tab_frame_replace, fragment);

        fragmentTransaction.commit();

    }

    public static void replacingTabFragmentWithoutSearchView(Fragment fragment) {

        oldFragment = currentFragment;

        currentFragment = fragment;

        final FragmentTransaction fragmentTransaction = instance.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        fragmentTransaction.replace(R.id.tab_frame_replace, fragment);

        fragmentTransaction.commit();

    }

    public static void setCurrentFragment(Fragment fragment) {

        oldFragment = currentFragment;
        currentFragment = fragment;


    }

    public static void addNotification() {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(instance)
                        .setContentTitle("Notifications Example")
                        .setContentText("This is a test notification");

        Intent notificationIntent = new Intent(instance, MainActivity.class);

        //notificationIntent.pu
        PendingIntent contentIntent = PendingIntent.getActivity(instance, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) instance.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

    public static void end() {
        instance.finish();
    }

    static View appBarlayout;
    static View frameReplace;
    static View frameSplash;

    public static void replacingSplashFragment() {

        appBarlayout.setVisibility(View.GONE);
        frameReplace.setVisibility(View.GONE);
        frameSplash.setVisibility(View.VISIBLE);

        final FragmentTransaction fragmentTransaction = instance.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        fragmentTransaction.replace(R.id.frame_splash, new SplashScreenFragment());

        fragmentTransaction.commit();


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        theme = POSUtil.getDefaultThemeNoActionBar(this);


       /* user = new User();

        AccessLogManager     accessLogManager     = new AccessLogManager(this);
        AuthorizationManager authorizationManager = new AuthorizationManager(this);
        Long                 deviceId             = authorizationManager.getDeviceId(Build.SERIAL);
         user          = accessLogManager.getCurrentlyLoggedInUser(deviceId);*/


        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setTheme(theme);
        setContentView(R.layout.app_bar_main);
        appBarlayout = findViewById(R.id.app_bar_layout);

        frameReplace = findViewById(R.id.frame_replace);
        frameSplash = findViewById(R.id.frame_splash);


        rootView = findViewById(R.id.root);

        splView = findViewById(R.id.frame_splash);
        view = findViewById(R.id.frame_replace);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        context = this;

        long t1 = System.currentTimeMillis();

        Log.w("t3 in main", t1 + "SS");

        instance = this;
        Log.e("Close ", close + " ");
        // close=getIntent().getBooleanExtra("navi",false);

        if (close) {
            Log.e("Close ", close + " ");
            finish();
            System.exit(1);
        }

        if (currentFragment instanceof ThemeSettingListViewFragment) {

            Intent intent = new Intent(this, DetailActivity.class);

            intent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.THEME_SETTING);

            startActivity(intent);

        } else if (currentFragment instanceof SettingFragment) {

            replacingFragment(new SettingFragment());

        } else if (currentFragment instanceof PurchaseListTabFragment) {

            replacingFragment(new PurchaseListTabFragment());//

        } else if (currentFragment instanceof StockContainerTabFragment || currentFragment instanceof StockListViewFragment || currentFragment instanceof LostListFragmentWithAddDialog
                || currentFragment instanceof ReorderListFragment || currentFragment instanceof DamageListFragmentWithAddDailog) {

            replacingFragment(new StockContainerTabFragment());//

        } else if (currentFragment instanceof PayableAndReceivableTabFragment) {

            replacingFragment(new PayableAndReceivableTabFragment());

        } else if (currentFragment instanceof ExpenseManager) {

            replacingFragment(new ExpenseManager());

        } else if (currentFragment instanceof CustomerSupplierTabFragment) {

            replacingFragment(new CustomerSupplierTabFragment());

        } else if (currentFragment instanceof ReportTitleListFragment) {

            replacingFragment(new ReportTitleListFragment());

        } else if (currentFragment instanceof SaleListHistoryTabFragment) {

            replacingFragment(new SaleListHistoryTabFragment());//

//        } else if (currentFragment instanceof SplashScreenFragment){
//            replaceFragment(new SplashScreenFragment());
        } else {


            replacingSplashFragment();
        }

        navi = getIntent().getBooleanExtra("navi", false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
//        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {

        super.onResume();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.getBoolean("firstrun", true)) {
            prefs.edit().putBoolean("firstrun", false).apply();

            Device device = new Device();
            device.setDeviceName(Build.DEVICE);
            device.setSerialNo(Build.SERIAL);
            device.setPermission(AppConstant.PERMISSION_ALLOWED);

            AuthorizationManager authorizationManager = new AuthorizationManager(this);
            authorizationManager.insertDevice(device);
        }

        Log.e("Navi ", navi + " ");

        if (navi) {
            StockManager stockManager = new StockManager(this);
            int reorderCount = stockManager.getReorderCount();

            userManager = new UserManager(this);
            AccessLogManager accessLogManager = new AccessLogManager(this);
            AuthorizationManager authorizationManager = new AuthorizationManager(context);
            Long deviceId = authorizationManager.getDeviceId(Build.SERIAL);

            user = accessLogManager.getCurrentlyLoggedInUser(deviceId);
            if (user == null) {
                POSUtil.noUserIsLoggedIn(this);
                finish();
            }

            allTrans = this.getTheme().obtainStyledAttributes(new int[]{R.attr.materialDrawerItemselectedText});

            allTrans2 = this.getTheme().obtainStyledAttributes(new int[]{R.attr.materialDrawerItemselectedBackground});

            allTrans3 = this.getTheme().obtainStyledAttributes(new int[]{R.attr.materialDrawerHeaderTxt});

            TypedArray dashboardIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_dashboard});
            TypedArray feedbackIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_feedback});
            TypedArray oustandIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_outstanding});
            TypedArray expenseManagerIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_expense});
            TypedArray reportIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_report});
            TypedArray contactsIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_contact});
            TypedArray settingIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_settings});
            TypedArray logoutIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_logout});
            TypedArray accountSettingIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_account_settings});
            TypedArray saleIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_sale});
            TypedArray purchaseIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_purchase});
            TypedArray inventoryIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_inventory});
            TypedArray subscribeIcon = this.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_inventory});


            dashboardStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.dashboard}).getString(0);
            saleStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.sale}).getString(0);
            purchaseStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.purchase}).getString(0);
            inventoryStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.inventory}).getString(0);
            outstandingStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.outstanding}).getString(0);
            contactsStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.contacts}).getString(0);
            expenseManagerStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.expense_manager}).getString(0);
            reportsStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.reports}).getString(0);
            settingStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.setting}).getString(0);
            accountSettingStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.account_setting}).getString(0);
            subscribeStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.lincense}).getString(0);
            logOutStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.log_out}).getString(0);
            feedbackStr = this.getTheme().obtainStyledAttributes(new int[]{R.attr.feedback}).getString(0);

            saleMenu = new PrimaryDrawerItem().withIdentifier(1).withName(saleStr).withIcon(saleIcon.getDrawable(0)).withSelectedTextColor(allTrans.getColor(0, 0)).withSelectedColor(allTrans2.getColor(0, 0));
            purchaseMenu = new PrimaryDrawerItem().withIdentifier(2).withName(purchaseStr).withIcon(purchaseIcon.getDrawable(0)).withSelectedTextColor(allTrans.getColor(0, 0)).withSelectedColor(allTrans2.getColor(0, 0));

            if (reorderCount > 0) {

                stockMenu = new PrimaryDrawerItem().withIdentifier(3).withName(inventoryStr).withBadge(
                        new StringHolder(Integer.toString(reorderCount))).withIcon(inventoryIcon.getDrawable(0)).withSelectedTextColor(allTrans.getColor(0, 0)).withSelectedColor(allTrans2.getColor(0, 0));

            } else {
                stockMenu = new PrimaryDrawerItem().withIdentifier(3).withName(inventoryStr).withIcon(inventoryIcon.getDrawable(0)).withSelectedTextColor(allTrans.getColor(0, 0)).withSelectedColor(allTrans2.getColor(0, 0));

            }

//        final Drawable drawable = ContextCompat.getDrawable(this, drawableResId);

            //    Drawable drawable=getResources().getDrawable(drawableResId);


            outstanding = new PrimaryDrawerItem().withIdentifier(4).withName(outstandingStr).withIcon(oustandIcon.getDrawable(0)).withSelectedTextColor(allTrans.getColor(0, 0)).withSelectedColor(allTrans2.getColor(0, 0));
            // deliveryMenu = new PrimaryDrawerItem().withIdentifier(11).withName("Delivery").withIcon(R.drawable.ic_delivery).withSelectedTextColor(allTrans.getColor(0, 0)).withSelectedColor(allTrans2.getColor(0, 0));
            expenseManagerMenu = new PrimaryDrawerItem().withIdentifier(5).withName(expenseManagerStr).withIcon(expenseManagerIcon.getDrawable(0)).withSelectedTextColor(allTrans.getColor(0, 0)).withSelectedColor(allTrans2.getColor(0, 0));
            reprotMenu = new PrimaryDrawerItem().withIdentifier(6).withName(reportsStr).withIcon(reportIcon.getDrawable(0)).withSelectedTextColor(allTrans.getColor(0, 0)).withSelectedColor(allTrans2.getColor(0, 0));
            settingMenu = new PrimaryDrawerItem().withIdentifier(7).withName(settingStr).withIcon(settingIcon.getDrawable(0)).withSelectedTextColor(allTrans.getColor(0, 0)).withSelectedColor(allTrans2.getColor(0, 0));
            dashBoardMenu = new PrimaryDrawerItem().withIdentifier(8).withName(dashboardStr).withIcon(dashboardIcon.getDrawable(0)).withSelectedTextColor(allTrans.getColor(0, 0)).withSelectedColor(allTrans2.getColor(0, 0));
            contactsMenu = new PrimaryDrawerItem().withIdentifier(9).withName(contactsStr).withIcon(contactsIcon.getDrawable(0)).withSelectedTextColor(allTrans.getColor(0, 0)).withSelectedColor(allTrans2.getColor(0, 0));


            // ExpandableDrawerItem inventory =  new ExpandableDrawerItem().withName("Inventory").withSubItems(stockMenu.withLevel(2),catogoryMenu.withLevel(2),unitMenu.withLevel(2),damageAndLostMenu.withLevel(2),reorder.withLevel(2));

            // ExpandableDrawerItem oustanding =  new ExpandableDrawerItem().withName("Oustanding").withSubItems(receivableMenu.withLevel(2),payableMenu.withLevel(2));

            TypedArray logo = this.getTheme().obtainStyledAttributes(new int[]{R.attr.logo});
            drawerHeader = new ProfileDrawerItem().withEmail("Inventory").withName("Prologic").withIcon(logo.getDrawable(0));
//            logout = new ProfileSettingDrawerItem().withName(logOutStr).withIcon(logoutIcon.getDrawable(0)).withIdentifier(LOG_OUT);
            accountSetting = new ProfileSettingDrawerItem().withName(accountSettingStr).withIcon(accountSettingIcon.getDrawable(0)).withIdentifier(ACCOUNT_SETTING);
            subscribeSetting = new ProfileSettingDrawerItem().withName(subscribeStr).withIcon(subscribeIcon.getDrawable(0)).withIdentifier(100);
            complainSetting = new ProfileSettingDrawerItem().withName(feedbackStr).withIcon(feedbackIcon.getDrawable(0)).withIdentifier(200);
            settingManager = new SettingManager(this);


            businessSetting = settingManager.getBusinessSetting();

            AppConstant.CURRENCY = businessSetting.getSign();


            toolbar.setTitleTextColor(000000);

            searchView = (MaterialSearchView) findViewById(R.id.search_view);


            this.savedInstanceState = savedInstanceState;

            buildingMaterialDrawer();
            if (isFinishing()) return;

            drawerLayout.addDrawerListener(this);
//addNotification();


            loadEditAccountSettingDialog();

            loadUI();


            passwordAccountSetting.setInputType(129);

            showPasswordAccountSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (showPasswordAccountSetting.isSelected()) {
                        passwordAccountSetting.setInputType(129);
                        showPasswordAccountSetting.setSelected(false);
                    } else {
                        passwordAccountSetting.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        showPasswordAccountSetting.setSelected(true);
                    }
                    passwordAccountSetting.setSelection(passwordAccountSetting.getText().length());
                }
            });

            oldPasswordAccountSetting.setInputType(129);

            showOldPasswordAccountSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (showOldPasswordAccountSetting.isSelected()) {
                        oldPasswordAccountSetting.setInputType(129);
                        showOldPasswordAccountSetting.setSelected(false);
                    } else {
                        oldPasswordAccountSetting.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        showOldPasswordAccountSetting.setSelected(true);
                    }
                    oldPasswordAccountSetting.setSelection(passwordAccountSetting.getText().length());
                }
            });

            confirmPasswordAccountSetting.setInputType(129);

            showConfirmPasswordAccountSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (showConfirmPasswordAccountSetting.isSelected()) {
                        confirmPasswordAccountSetting.setInputType(129);
                        showConfirmPasswordAccountSetting.setSelected(false);
                    } else {
                        confirmPasswordAccountSetting.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        showConfirmPasswordAccountSetting.setSelected(true);
                    }
                    confirmPasswordAccountSetting.setSelection(passwordAccountSetting.getText().length());
                }
            });

            saveAccountSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (checkVaildation()) {

                        FusionToast.toast(context, FusionToast.TOAST_TYPE.SUCCESS);
                        editAccountSetting.dismiss();
                    }


                }
            });

            cancelAccountSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editAccountSetting.dismiss();
                }
            });

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        if (searchView != null) {

            if (searchView.isSearchOpen()) {
                searchView.closeSearch();
                searchView.hideKeyboard(searchView);
            }
        }

        if (result != null) {

            if (result.isDrawerOpen()) {
                result.closeDrawer();
            }
        }
//        if (currentFragment instanceof AddEditNewSaleFragment || currentFragment instanceof AddEditNewPurchaseFragment || currentFragment instanceof StockFormFragment
//                || currentFragment instanceof StockFormFragment
//                || currentFragment instanceof BussinessSettingFragment || currentFragment instanceof BussinessSettingFragment || currentFragment instanceof DocumentSettingFragment
//                || currentFragment instanceof UserRoleSettingFragment || currentFragment instanceof TaxSetupFragment || currentFragment instanceof UnitFragment
//                || currentFragment instanceof CategoryFragment || currentFragment instanceof CustomerListFragment || currentFragment instanceof SupplierListFragment
//                || currentFragment instanceof DataManagementFragment || currentFragment instanceof SaleDetailFragment || currentFragment instanceof PurchaseDetailFragment
//                || currentFragment instanceof BarChartFragment || currentFragment instanceof BarChartWithSearchViewFragment || currentFragment instanceof BarChartWithTwoBarsFragment
//                || currentFragment instanceof BarChartWithYearViewFragment || currentFragment instanceof LineChartForPriceTrendFragment || currentFragment instanceof LineChartFragment
//                || currentFragment instanceof LineChartWithSearchViewFragment || currentFragment instanceof PayableReceivableDetailViewFragment || currentFragment instanceof OutstandingPaymentDetailFragment
//                || currentFragment instanceof PieChartFragment || currentFragment instanceof PurchaseOutstandingPaymentDetailFragment || currentFragment instanceof SalesOutstandingPaymentDetailFragment
//                || currentFragment instanceof SimpleReportFragment || currentFragment instanceof SimpleReportFragmentWithDateFilter || currentFragment instanceof SimpleReportFragmentWithDateFilterSearch
//                || currentFragment instanceof SimpleReportFragmentWithSearchView) {
//            super.onBackPressed();
//
//        } else {
//
//            if (doubleBackToExitPressedOnce) {
//
//                currentFragment=null;
//                finish();
//                //System.exit(1);
//            }
//            this.doubleBackToExitPressedOnce = true;
//            POSUtil.showSnackBar(rootView, "Please click BACK again to exit");
//
//            new Handler().postDelayed(new Runnable() {
//
//                @Override
//                public void run() {
//                    doubleBackToExitPressedOnce = false;
//                }
//            }, 2000);
//
//        }

        if (currentFragment instanceof AddEditNewSaleFragment || currentFragment instanceof AddEditNewPurchaseFragment || currentFragment instanceof StockFormFragment
                //|| currentFragment instanceof DashboardFragmentV1
                || currentFragment instanceof BussinessSettingFragment || currentFragment instanceof BussinessSettingFragment || currentFragment instanceof DocumentSettingFragment
                || currentFragment instanceof UserRoleSettingFragment || currentFragment instanceof TaxSetupFragment || currentFragment instanceof UnitFragment
                || currentFragment instanceof CategoryFragment || currentFragment instanceof CustomerListFragment || currentFragment instanceof SupplierListFragment
                || currentFragment instanceof DataManagementFragment || currentFragment instanceof SaleDetailFragment || currentFragment instanceof PurchaseDetailFragment
                || currentFragment instanceof BarChartFragment || currentFragment instanceof BarChartWithSearchViewFragment || currentFragment instanceof BarChartWithTwoBarsFragment
                || currentFragment instanceof BarChartWithYearViewFragment || currentFragment instanceof LineChartForPriceTrendFragment || currentFragment instanceof LineChartFragment
                || currentFragment instanceof LineChartWithSearchViewFragment || currentFragment instanceof PayableReceivableDetailViewFragment || currentFragment instanceof OutstandingPaymentDetailFragment
                || currentFragment instanceof PieChartFragment || currentFragment instanceof PurchaseOutstandingPaymentDetailFragment || currentFragment instanceof SalesOutstandingPaymentDetailFragment
                || currentFragment instanceof SimpleReportFragment || currentFragment instanceof SimpleReportFragmentWithDateFilter || currentFragment instanceof SimpleReportFragmentWithDateFilterSearch
                || currentFragment instanceof SimpleReportFragmentWithSearchView //|| currentFragment instanceof ThemeSettingListViewFragment || currentFragment instanceof SettingFragment
                ) {
            Log.e("parent", "main activity");
            super.onBackPressed();
            currentFragment = null;

        } else {
            if (doubleBackToExitPressedOnce) {
                Log.e("exit", " after ");
                oldFragment = currentFragment;
                currentFragment = null;
                Log.e("finish", "hey");
                finishAffinity();
                System.exit(1);
                //}
            }
            Log.e("jkj", "kjoijikjioj");
            this.doubleBackToExitPressedOnce = true;
            POSUtil.showSnackBar(rootView, "Please click BACK again to exit");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e("make", "false");
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("on act", "result");
    }

//    public void loadMenu() {
//
//       /* if (!AppConstant.currentUser.isVailid()) {
//            Intent mainActivity = new Intent(MainActivity.this, LoginActivity.class);
//
//            startActivity(mainActivity);
//
//            finish();
//        } else {*/
//       User currentUser = ApiDAO.getApiDAOInstance(this).getUser(AppConstant.currentUserId);
//       AppConstant.roleOfCurrentUser = currentUser.getRole();
//
////        Log.w("Herer", currentUser.toString());
//
//        if (AppConstant.roleOfCurrentUser.equalsIgnoreCase(User.ROLE.Admin.toString())) {
//            drawerItems = new IDrawerItem[]{dashBoardMenu, saleMenu, purchaseMenu, stockMenu, outstanding, contactsMenu, expenseManagerMenu, reprotMenu, settingMenu};
//        } else if (AppConstant.roleOfCurrentUser.equalsIgnoreCase(User.ROLE.Sale.toString())) {
//            Log.d("UserRole mainactivity", "Sale");
//            drawerItems = new IDrawerItem[]{saleMenu};
//        } else if (AppConstant.roleOfCurrentUser.equalsIgnoreCase(User.ROLE.Purchase.toString())) {
//            drawerItems = new IDrawerItem[]{purchaseMenu};
//        }
//        //  if (AppConstant.currentUs
//        // er.getRole().equalsIgnoreCase(User.ROLE.Admin.toString())) {
//
////        drawerItems = new IDrawerItem[]{dashBoardMenu, saleMenu, purchaseMenu, stockMenu, outstanding, contactsMenu, expenseManagerMenu, reprotMenu, settingMenu};
//
//           /* } else if (AppConstant.currentUser.getRole().equalsIgnoreCase(User.ROLE.Sale.toString())) {
//
//                drawerItems = new IDrawerItem[]{dashBoardMenu, saleMenu, purchaseMenu, stockMenu, outstanding, contactsMenu, expenseManagerMenu, reprotMenu, settingMenu};
//
//            } else if (AppConstant.currentUser.getRole().equalsIgnoreCase(User.ROLE.Purchase.toString())) {
//
//                drawerItems = new IDrawerItem[]{saleMenu, purchaseMenu, stockMenu, contactsMenu, settingMenu};
//
//            } else if (AppConstant.currentUser.getRole().equalsIgnoreCase(User.ROLE.Staff.toString())) {
//
//                drawerItems = new IDrawerItem[]{saleMenu, purchaseMenu, stockMenu, contactsMenu};
//
//            }
//        }*/
//
//
//    }

    public void buildingMaterialDrawer() {

        // final PrimaryDrawerItem customerMenu = new PrimaryDrawerItem().withIdentifier(7).withName("Customer");
        //  final PrimaryDrawerItem supplierMenu = new PrimaryDrawerItem().withIdentifier(8).withName("Supplier");
        final AccessLogManager accessLogManager = new AccessLogManager(this);
        AuthorizationManager authorizationManager = new AuthorizationManager(context);
        Long deviceId = authorizationManager.getDeviceId(Build.SERIAL);
        User currentUser = accessLogManager.getCurrentlyLoggedInUser(deviceId);
        if (currentUser == null) {
            POSUtil.noUserIsLoggedIn(this);
            finish();
            return;
        }

        IProfile profileDrawerItems[] = new IProfile[]{drawerHeader, accountSetting};


        if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Admin.toString())) {
            drawerItems = new IDrawerItem[]{dashBoardMenu, saleMenu, purchaseMenu, stockMenu, outstanding, contactsMenu, expenseManagerMenu, reprotMenu, settingMenu};
            profileDrawerItems = new IProfile[]{drawerHeader, accountSetting, subscribeSetting, complainSetting};

        } else if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Sale.toString())) {
            drawerItems = new IDrawerItem[]{saleMenu};

        } else if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Purchase.toString())) {
            drawerItems = new IDrawerItem[]{purchaseMenu};

        } else {
            drawerItems = new IDrawerItem[]{dashBoardMenu, saleMenu, purchaseMenu, stockMenu, outstanding, contactsMenu, expenseManagerMenu, reprotMenu, settingMenu};
            profileDrawerItems = new IProfile[]{drawerHeader, accountSetting, subscribeSetting, complainSetting};
        }

        View v = LayoutInflater.from(this).inflate(R.layout.material_drawer_header, null, false);
        userNameView = (TextView) v.findViewById(R.id.userNameView);
        userNameView.setText(currentUser.getUserName());

        TextView textView=v.findViewById(R.id.remain_day);
        //TODO: Don't forget to activate subscription check
        int day=POSUtil.getRemainDay(this);
        textView.setText(day+" days remaining");


        AccountHeader accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withSelectionListEnabledForSingleProfile(true)
                .withCurrentProfileHiddenInList(true)
                .withOnlyMainProfileImageVisible(true)
                .withAlternativeProfileHeaderSwitching(false)
                .withAccountHeader(v)
                .addProfiles(profileDrawerItems).withTextColor(allTrans3.getColor(0, 0))
                .withTranslucentStatusBar(false)
                .withSavedInstance(savedInstanceState)
                .withOnAccountHeaderListener((view, profile, current) -> {
                     if (profile.getIdentifier() == ACCOUNT_SETTING) {

                        Intent intent = new Intent(MainActivity.this, ChangePassCodeActivity.class);
                        startActivity(intent);
                        //replaceFragment(new ChangePassFragment());


                    } else if (profile.getIdentifier() == complainSetting.getIdentifier()) {

                        Intent intent = new Intent(MainActivity.this, ComplainFormActivity.class);
                        startActivity(intent);

                    } else if (profile.getIdentifier() == subscribeSetting.getIdentifier()) {

                        Intent intent = new Intent(MainActivity.this, SubscriptionHistoryActivity.class);
                        startActivity(intent);

                    }
                    return false;
                })
                .build();

        //Log.w("testing", (selectedDrawerItem != null ? selectedDrawerItem.getIdentifier() : stockMenu.getIdentifier()) + " sss");
        ViewGroup drawerFooter = (ViewGroup) LayoutInflater.from(this).inflate(R.layout.drawer_footer, null);
        ((TextView) drawerFooter.findViewById(R.id.username_text_view)).setText(currentUser.getUserName());
        drawerFooter.findViewById(R.id.logout_button).setOnClickListener(view -> {
            Long userId = accessLogManager.getCurrentlyLoggedInUserId(deviceId);
            AccessLog accessLog = new AccessLog();
            accessLog.setUserId(userId);
            accessLog.setDeviceId(deviceId);
            accessLog.setEvent(AppConstant.EVENT_OUT);
            accessLog.setDatetime(DateUtility.getCurrentDateTime());
            accessLogManager.insertAccessLog(accessLog);

            Intent mainActivity = new Intent(MainActivity.this, PasscodeActivity.class);
            startActivity(mainActivity);

            finish();
        });

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(accountHeader)
                .withSelectedItem(selectedDrawerItem != null ? selectedDrawerItem.getIdentifier() : saleMenu.getIdentifier())
                .withStickyFooter(drawerFooter)
                .addDrawerItems(
                        drawerItems
                )
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    // do something with the clicked item :D

                    Log.w("on drawer click", "main activity");

                    if (drawerItem.getIdentifier() == saleMenu.getIdentifier()) {
                        selectedDrawerItem = drawerItem;

                        replacingFragment(new SaleListHistoryTabFragment());

                    } else if (drawerItem.getIdentifier() == purchaseMenu.getIdentifier()) {
                        selectedDrawerItem = drawerItem;
                        replacingFragment(new PurchaseListTabFragment());

                    } else if (drawerItem.getIdentifier() == stockMenu.getIdentifier()) {
                        selectedDrawerItem = drawerItem;
                        replacingFragment(new StockContainerTabFragment());

                    } else if (drawerItem.getIdentifier() == expenseManagerMenu.getIdentifier()) {
                        selectedDrawerItem = drawerItem;
                        replacingFragment(new ExpenseManager());

                    } else if (drawerItem.getIdentifier() == reprotMenu.getIdentifier()) {
                        selectedDrawerItem = drawerItem;
                        replacingFragment(new ReportTitleListFragment());

                    }/* else if (drawerItem.getIdentifier() == deliveryMenu.getIdentifier()) {
                        selectedDrawerItem = drawerItem;
                        replaceFragment(new DeliveryTabFragment());

                    }*/ else if (drawerItem.getIdentifier() == settingMenu.getIdentifier()) {
                        selectedDrawerItem = drawerItem;
                        replacingFragment(new SettingFragment());

                    } else if (drawerItem.getIdentifier() == outstanding.getIdentifier()) {
                        selectedDrawerItem = drawerItem;
                        replacingFragment(new PayableAndReceivableTabFragment());

                    } else if (drawerItem.getIdentifier() == contactsMenu.getIdentifier()) {
                        selectedDrawerItem = drawerItem;
                        replacingFragment(new CustomerSupplierTabFragment());
                    } else if (drawerItem.getIdentifier() == dashBoardMenu.getIdentifier()) {
                        selectedDrawerItem = drawerItem;
                        replacingFragment(new DashBoardWithFilter());

                    }

                    return false;

                })
                .build();


        //  upgradeBadgeInventory(reorderCount);

        drawerLayout = result.getDrawerLayout();


        BlurActionBarDrawerToggle mDrawerToggle = new BlurActionBarDrawerToggle(

                this,                    /* m,  Activity */

                drawerLayout,                    /* DrawerLayout object */

                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */

                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */

        );

        mDrawerToggle.setRadius(15);

        mDrawerToggle.setDownScaleFactor(3.0f);

        drawerLayout.setScrimColor(Color.TRANSPARENT);

        drawerLayout.setDrawerListener(mDrawerToggle);


    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

        searchView.closeSearch();

        searchView.hideKeyboard(searchView);

    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    private boolean checkVaildation() {

        boolean status = true;

//        if (oldPasswordAccountSetting.getText().toString().equalsIgnoreCase(user.getPasscode_id())) {
//            oldPasswordAccountSetting.setError(getTheme().obtainStyledAttributes(new int[]{R.attr.old_password_is_incorrect_and_changes_cannot_be_updated}).getString(0));
//            status = false;
//            //return status;
//        }

        if (usernameAccountSetting.getText().toString().trim().length() < 1) {

            usernameAccountSetting.setError(getTheme().obtainStyledAttributes(new int[]{R.attr.enter_user_name}).getString(0));

            status = false;

            //return status;
        }


        if (passwordAccountSetting.getText().toString().trim().length() < 4) {

            passwordAccountSetting.setError(getTheme().obtainStyledAttributes(new int[]{R.attr.password_must_be_more_than_4_characters}).getString(0));

            status = false;

            //return status;
        }

        if (!passwordAccountSetting.getText().toString().equalsIgnoreCase(confirmPasswordAccountSetting.getText().toString())) {
            confirmPasswordAccountSetting.setError(getTheme().obtainStyledAttributes(new int[]{R.attr.password_and_confirm_password_are_not_match}).getString(0));

            status = false;
        }

        return status;

    }

    private void loadUI() {
        usernameAccountSetting = (EditText) editAccountSetting.findViewById(R.id.username_in_accountsetting_et);
        passwordAccountSetting = (EditText) editAccountSetting.findViewById(R.id.password_in_accountsetting_et);
        oldPasswordAccountSetting = (EditText) editAccountSetting.findViewById(R.id.old_password_in_accountsetting_et);
        confirmPasswordAccountSetting = (EditText) editAccountSetting.findViewById(R.id.confirm_password_in_account_setting_et);
        showPasswordAccountSetting = (ImageButton) editAccountSetting.findViewById(R.id.showpassword_in_accountsetting_et);
        showOldPasswordAccountSetting = (ImageButton) editAccountSetting.findViewById(R.id.show_old_password_in_accountsetting_et);
        showConfirmPasswordAccountSetting = (ImageButton) editAccountSetting.findViewById(R.id.show_confirm_password_in_accountsetting_et);
        saveAccountSetting = (Button) editAccountSetting.findViewById(R.id.save);
        cancelAccountSetting = (Button) editAccountSetting.findViewById(R.id.cancel);

    }

    public void loadEditAccountSettingDialog() {

        //TypedArray save = this.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel = this.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        editAccountSetting = new MaterialDialog.Builder(this).

                customView(R.layout.account_setting, true).title("Edit Account Setting").

                //positiveText(save.getString(0)).

                //negativeText(cancel.getString(0)).

                        build();

    }

    public interface BackPress {
        void onBackPress();
    }

    class Loading extends AsyncTask<OutputStream, String, String> {
        @Override
        protected String doInBackground(OutputStream... params) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (currentFragment instanceof ThemeSettingListViewFragment) {
                        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                        intent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.THEME_SETTING);
                        startActivity(intent);

                    } else if (currentFragment instanceof SettingFragment) {
                        replacingFragment(new SettingFragment());

                    } else {
                        replacingFragment(new DashBoardWithFilter());//

                    }
                }
            });

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String a) {

        }
    }
}