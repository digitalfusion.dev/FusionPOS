package com.digitalfusion.android.pos.fragments.outstanding;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVAdapterForStockItemInSaleAndPurchaseDetail;
import com.digitalfusion.android.pos.database.business.PurchaseManager;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.business.SupplierOutstandingManager;
import com.digitalfusion.android.pos.database.model.BusinessSetting;
import com.digitalfusion.android.pos.database.model.PurchaseHeader;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.widget.Toast.LENGTH_SHORT;

/**
 * Created by MD003 on 3/7/17.
 */

public class PayableDetailFragment extends Fragment {

    // private DeliveryAndPickUpDialogFragment deliveryAndPickUpDialog;
    public static final String KEY = "purchaseId";
    private static final int REQUEST_CONNECT_AND_PRINT_DEVICE = 1;
    private static final int REQUEST_CONNECT_DEVICE = 3;
    private static final int REQUEST_ENABLE_BT = 2;
    public static String EXTRA_DEVICE_ADDRESS = "device_address";
    private static BluetoothSocket mmSocket;
    private static BluetoothDevice device;
    private static String print;
    OutputStream outputStream;
    private View mainLayout;
    //UI components from main
    private View deliverDashLine;
    private RecyclerView purchaseItemRecyclerView;
    private TextView purchaseIdTextView;
    private TextView dateTextView;
    private TextView discountTextView;
    private TextView subtotalTextView;
    private TextView totalTextView;
    private TextView deliveryAddress;

    //Business
    private LinearLayout deliveryAddressLinearLayout;
    private TextView deliverChargesTextView;
    private TextView customerNameTextView;
    private TextView taxRateTextView;
    private TextView businessNameTextView;
    private TextView addressTextView;
    private TextView phoneTextView;
    private TextView headerText;
    private TextView footerText;
    private PurchaseManager purchaseBussiness;
    //values
    private Context context;
    private RVAdapterForStockItemInSaleAndPurchaseDetail rvAdapterForStockItemInSaleAndPurchaseDetail;

    //Values
    private List<SalesAndPurchaseItem> purchaseItemViewList;
    private BluetoothAdapter mBluetoothAdapter = null;
    private Long purchaseId;
    private Double totalAmount = 0.0;
    private Double voucherDiscount = 0.0;
    private Double voucherTax = 0.0;
    private Double subtotal = 0.0;
    private Calendar now;
    private int purchaseDay;
    private int purchaseMonth;
    private int purchaseYear;
    private String date;
    private String purchaseID;
    // private SaleDelivery saleDeliveryView;
    //  private SalesOrderManager salesOrderBusiness;
    private PurchaseHeader purchaseHeader;
    private Double deliveryCharges = 0.0;
    private BusinessSetting businessSetting;
    private SettingManager settingManager;
    private TextView paidAmountTextView;
    //  public TextView printDeviceNameTextView;
    //  private TextView statusTextView;
    //  private boolean isPrint=false;
    // KProgressHUD printHud;
    // KProgressHUD connectHud;
    // private BluetoothUtil bluetoothUtil;
    //  private PrintViewSlip printViewSlip;
    private SupplierOutstandingManager supplierOutstandingManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainLayout = inflater.inflate(R.layout.payable_detail, null);
        purchaseItemViewList = new ArrayList<>();

        // setHasOptionsMenu(true);

        MainActivity.setCurrentFragment(this);

        context = getContext();
        supplierOutstandingManager = new SupplierOutstandingManager(context);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //bluetoothUtil=BluetoothUtil.getInstance(this);
        settingManager = new SettingManager(context);
        businessSetting = settingManager.getBusinessSetting();
        String payableDetail = context.getTheme().obtainStyledAttributes(new int[]{R.attr.payable_detail}).getString(0);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(payableDetail);
        purchaseBussiness = new PurchaseManager(context);
        // salesOrderBusiness=new SalesOrderManager(context);
        purchaseId = getArguments().getLong(KEY);
        now = Calendar.getInstance();

        initializeOldData();
        loadUI();
        configUI();
        configRecyclerView();
        updateViewData();

        paidAmountTextView.setText(POSUtil.NumberFormat(supplierOutstandingManager.getPaidAmtByPurchaseID(purchaseId)));


     /*   mainLayout.findViewById(R.id.blue_toothLL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //  isPrint=false;

                //bluetoothUtil.createConnection();

              isPrint=false;
                if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    getActivity().startActivityForResult(enableIntent, 2);
                    // Otherwise, setup the chat session
                }else {
                    Intent serverIntent = null;
                    serverIntent = new Intent(getContext(), BluetoothDeviceNearByList.class);
                    //startActivity(serverIntent);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                }
            }
        });
*/
        //print();

      /*  bluetoothUtil.setBluetoothConnectionListener(new BluetoothUtil.JobListener() {

            @Override
            public void onPrepare(){
                connectHud.show();
            }

            @Override
            public void onSuccess() {

                Log.w("on Success","on success");

                if(connectHud.isShowing()){

                    connectHud.dismiss();

                }

                printDeviceNameTextView.setText(bluetoothUtil.getDevice().getName());


                if(bluetoothUtil.isConnected()){

                    statusTextView.setTextColor(Color.parseColor("#2E7D32"));

                    statusTextView.setText("Connected");

                }else {

                    statusTextView.setTextColor(Color.parseColor("#DD2C00"));

                    statusTextView.setText("Not Connected");

                }

                if(isPrint){
                    try {

                        printViewSlip.printNow(bluetoothUtil.getOutputStream());

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }

            }

            @Override
            public void onFailure() {

                printDeviceNameTextView.setText("No Device");

                statusTextView.setText("Not Connected");

                DisplayToast("Make sure bluetooth turn on both devices and choose printer only!!!");

                if(connectHud.isShowing()){

                    connectHud.dismiss();

                }

            }
            @Override
            public void onDisconnected() {

                DisplayToast("Connection Lost!!!");

                printDeviceNameTextView.setText("No Device");

                statusTextView.setTextColor(Color.parseColor("#DD2C00"));

                statusTextView.setText("Not Connected");


            }
        });


        printViewSlip.setPrintListener(new PrintViewSlip.PrintListener() {
            @Override
            public void onPrepare() {
                printHud.show();
            }

            @Override
            public void onSuccess() {

                if(printHud.isShowing()){

                    printHud.dismiss();

                }

            }

            @Override
            public void onFailure() {

                DisplayToast("Make sure bluetooth turn on both devices and choose printer only!!!");

                if(printHud.isShowing()){

                    printHud.dismiss();

                }

            }
        });*/

        return mainLayout;
    }

    public void initializeOldData() {

        purchaseHeader = purchaseBussiness.getPurchaseHeaderByPurchaseID(purchaseId, new InsertedBooleanHolder());

        purchaseID = purchaseHeader.getPurchaseVocherNo();

        //        Log.w("tota in details ", purchaseHeader.getTotal().toString());

        purchaseItemViewList = purchaseBussiness.getPurchaseDetailsByPurchaseID(purchaseId);

        voucherDiscount = purchaseHeader.getDiscountAmt();

        purchaseDay = Integer.parseInt(purchaseHeader.getDay());

        purchaseMonth = Integer.parseInt(purchaseHeader.getMonth());

        purchaseYear = Integer.parseInt(purchaseHeader.getYear());

        now.set(purchaseYear, purchaseMonth - 1, purchaseDay);

      /*  if(purchaseHeader.getFeedbackType().equalsIgnoreCase(SalesManager.SaleType.Delivery.toString())){

            saleDeliveryView= purchaseBussiness.getDeliveryInfoBySalesID(purchaseHeader.getId(),new InsertedBooleanHolder());

            deliveryCharges=saleDeliveryView.getCharges();

        }*/

    }

    public void DisplayToast(String str) {
        if (getContext() != null) {
            Toast toast = Toast.makeText(getContext(), str, LENGTH_SHORT);
            //����toast��ʾ��λ��
            toast.setGravity(Gravity.BOTTOM, 0, 100);
            //��ʾ��Toast
            toast.show();
        }

    }//
 /*   private void print(){

        printViewSlip=new PrintViewSlip();

        if(!businessSetting.isEmptyBusinessName()){

            printViewSlip.addString(businessSetting.getBusinessName(), PrintViewSlip.Alignment.CENTER);

            printViewSlip.nextLine();

        }

        if(!businessSetting.isEmptyAddress()){

            printViewSlip.addString(businessSetting.getAddress(), PrintViewSlip.Alignment.CENTER);

            printViewSlip.nextLine();
        }
        if (!businessSetting.isEmptyPhoneNo()){

            printViewSlip.addString(businessSetting.getPhoneNo(), PrintViewSlip.Alignment.CENTER);

            printViewSlip.nextLine();

        }

        printViewSlip.nextLine();

        printViewSlip.addString(PrintViewSlip.STYLE.BOLD,"Sale");

        printViewSlip.addSpace(5);

        printViewSlip.addFullColon();

        printViewSlip.addString(PrintViewSlip.STYLE.BOLD,"#");

        printViewSlip.addString(PrintViewSlip.STYLE.BOLD,purchaseHeader.getSaleVocherNo());

        printViewSlip.addString(DateUtility.makeDateFormatWithSlash(purchaseHeader.getSaleDate()), PrintViewSlip.Alignment.RIGHT);

        printViewSlip.nextLine();

        printViewSlip.addString("Customer ");

        printViewSlip.addFullColon();

        printViewSlip.addString(purchaseHeader.getCustomerName());

        printViewSlip.nextLine();

        if(purchaseHeader.getFeedbackType().equalsIgnoreCase(SalesManager.SaleType.Delivery.toString())){

            printViewSlip.addDividerLine();

            printViewSlip.nextLine();

            printViewSlip.addString("Ship To :");

            printViewSlip.addString(saleDeliveryView.getAddress());


        }


        printViewSlip.BuildTable(4);

        printViewSlip.addNewColumn("No.",5, PrintViewSlip.Alignment.CENTER);

        printViewSlip.addNewColumn("Item Name",20, PrintViewSlip.Alignment.LEFT);

        printViewSlip.addNewColumn("Price",10, PrintViewSlip.Alignment.RIGHT);

        printViewSlip.addNewColumn("SubTotal",13, PrintViewSlip.Alignment.RIGHT);

        printViewSlip.addHeader();

        int i=1;

        for (SalesAndPurchaseItem s:purchaseItemViewList){
            printViewSlip.addRow(Integer.toString(i),s.getItemName()+" x "+s.getQty(), POSUtil.NumberFormat(s.getPrice()),POSUtil.NumberFormat(s.getTotalPrice()));
            i++;
        }

        printViewSlip.addDividerLine();

        printViewSlip.addRow("","","SubTotal :",POSUtil.NumberFormat(purchaseHeader.getSubtotal()));

        if(purchaseHeader.getFeedbackType().equalsIgnoreCase(SalesManager.SaleType.Delivery.toString())){

            printViewSlip.addRow("","","Delivery :",POSUtil.NumberFormat(purchaseHeader.getDiscountAmt()));

        }

        // printViewSlip.addRow("","","Delivery :",POSUtil.NumberFormat(purchaseHeader.getSubtotal()));

        printViewSlip.addRow("","","Tax      :",POSUtil.NumberFormat(purchaseHeader.getPaidAmt()));

        printViewSlip.addRow("","","Discount :",POSUtil.NumberFormat(purchaseHeader.getDiscountAmt()));

        printViewSlip.addDividerLine();

        printViewSlip.addRow(PrintViewSlip.STYLE.BIG,"","","Total    :",POSUtil.NumberFormat(purchaseHeader.getTotal()));

        printViewSlip.addDividerLine();

        printViewSlip.nextLine();

        printViewSlip.addStringBold("~~~~~Thank You~~~~~", PrintViewSlip.Alignment.CENTER);

        printViewSlip.nextLine();

        printViewSlip.nextLine();

        printViewSlip.nextLine();

        printViewSlip.nextLine();

        printViewSlip.nextLine();


        //printViewSlip.addHeader("No.",);


    }*/

    public void configUI() {

        String headerTxt   = context.getTheme().obtainStyledAttributes(new int[]{R.attr.header_text}).getString(0);
        String temp_header = POSUtil.getHeaderTxt(context);
        String temp_footer = POSUtil.getFooterTxt(context);

        if (temp_header.equalsIgnoreCase(headerTxt)) {
            headerText.setVisibility(View.GONE);
        } else {
            headerText.setText(temp_header);
            headerText.setVisibility(View.VISIBLE);
        }
        footerText.setText(temp_footer);

      /*  if(bluetoothUtil.isConnected()){
            // printDeviceNameTextView.setTextColor(Color.parseColor("#2E7D32"));

            printDeviceNameTextView.setText(bluetoothUtil.getDevice().getName());

            statusTextView.setTextColor(Color.parseColor("#2E7D32"));

            statusTextView.setText("Connected");

        }else {

            // printDeviceNameTextView.setTextColor(Color.parseColor("#DD2C00"));
            printDeviceNameTextView.setText("No Device");

            statusTextView.setTextColor(Color.parseColor("#DD2C00"));

            statusTextView.setText("Not Connected");
        }*/

        if (businessSetting.isEmptyBusinessName()) {
            businessNameTextView.setVisibility(View.GONE);
        } else {
            businessNameTextView.setText(businessSetting.getBusinessName());
        }

        if (businessSetting.isEmptyAddress()) {
            addressTextView.setVisibility(View.GONE);
        } else {
            addressTextView.setText(businessSetting.getAddress());
        }

        if (businessSetting.isEmptyPhoneNo()) {
            phoneTextView.setVisibility(View.GONE);
        } else {
            phoneTextView.setText(businessSetting.getPhoneNo());
        }

        purchaseIdTextView.setText(purchaseID);
        taxRateTextView.setText(Double.toString(purchaseHeader.getTaxRate()));
        customerNameTextView.setText(purchaseHeader.getSupplierName());

    /*    if(purchaseHeader.getFeedbackType().equalsIgnoreCase(SalesManager.SaleType.Delivery.toString())){

            deliveryAddressLinearLayout.setVisibility(View.VISIBLE);

            deliveryAddress.setText(saleDeliveryView.getAddress());




        }else {

            deliveryAddressLinearLayout.setVisibility(View.GONE);

            deliverDashLine.setVisibility(View.GONE);

        }*/

        configDateUI();
    }

    public void configDateUI() {

        date = DateUtility.makeDateFormatWithSlash(Integer.toString(purchaseYear), Integer.toString(purchaseMonth), Integer.toString(purchaseDay));
        dateTextView.setText(date);
    }


    public void configRecyclerView() {

        rvAdapterForStockItemInSaleAndPurchaseDetail = new RVAdapterForStockItemInSaleAndPurchaseDetail(purchaseItemViewList);

        purchaseItemRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        purchaseItemRecyclerView.setAdapter(rvAdapterForStockItemInSaleAndPurchaseDetail);
    }

    private void loadUI() {
/*
        printHud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setDimAmount(0.5f)
                .setDetailsLabel("Printing....")
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);

        connectHud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setDimAmount(0.5f)
                .setDetailsLabel("Connecting....")
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);

*/
        // statusTextView=(TextView)mainLayout.findViewById(R.id.bt_status);
        //   printDeviceNameTextView= (TextView) mainLayout.findViewById(R.id.device_name);
        paidAmountTextView          = (TextView    ) mainLayout.findViewById(R.id.paid_amt                   );
        deliverDashLine             =                mainLayout.findViewById(R.id.deliver_dash_line          );
        businessNameTextView        = (TextView    ) mainLayout.findViewById(R.id.business_name              );
        addressTextView             = (TextView    ) mainLayout.findViewById(R.id.address                    );
        phoneTextView               = (TextView    ) mainLayout.findViewById(R.id.phone_no                   );
        customerNameTextView        = (TextView    ) mainLayout.findViewById(R.id.customer_in_sale_detail_tv );
        taxRateTextView             = (TextView    ) mainLayout.findViewById(R.id.tax_rate_in_sale_detail_tv );
        deliverChargesTextView      = (TextView    ) mainLayout.findViewById(R.id.delivery_in_sale_detail_tv );
        deliveryAddress             = (TextView    ) mainLayout.findViewById(R.id.delivery_address_tv        );
        deliveryAddressLinearLayout = (LinearLayout) mainLayout.findViewById(R.id.ll_delivery_address        );
        purchaseItemRecyclerView    = (RecyclerView) mainLayout.findViewById(R.id.sale_item_list_rv          );
        purchaseIdTextView          = (TextView    ) mainLayout.findViewById(R.id.sale_id_in_sale_detail_tv  );
        dateTextView                = (TextView    ) mainLayout.findViewById(R.id.sale_date_in_sale_detail_tv);
        discountTextView            = (TextView    ) mainLayout.findViewById(R.id.discount_in_sale_detail_tv );
        subtotalTextView            = (TextView    ) mainLayout.findViewById(R.id.subtotal_in_sale_detail_tv );
        totalTextView               = (TextView    ) mainLayout.findViewById(R.id.total_in_sale_detail_tv    );
        headerText                  = (TextView    ) mainLayout.findViewById(R.id.header_text                );
        footerText                  = (TextView    ) mainLayout.findViewById(R.id.footer_text                );

    }

    private void updateViewData() {

        calculateValues();

        discountTextView.setText(POSUtil.NumberFormat(voucherDiscount));
        subtotalTextView.setText(POSUtil.NumberFormat(subtotal));
        totalTextView.setText(POSUtil.NumberFormat(purchaseHeader.getTotal()));
        deliverChargesTextView.setText(POSUtil.NumberFormat(deliveryCharges));
    }

    private void calculateValues() {
        subtotal = 0.0;
        for (SalesAndPurchaseItem s : purchaseItemViewList) {
            subtotal += s.getTotalPrice();
        }

        totalAmount = subtotal + voucherTax - voucherDiscount + deliveryCharges;

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.print, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.print) {

       /*     isPrint=true;

            if (!bluetoothUtil.isConnected()) {

                bluetoothUtil.createConnection();

            }else {
                try {
                    printViewSlip.printNow(bluetoothUtil.getOutputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/
        }

        return true;
    }

    public void replacingFragment(Fragment fragment) {

        final FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        fragmentTransaction.replace(R.id.frame_replace, fragment);

        fragmentTransaction.commit();

    }

  /*  class PrintingProgress extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... params) {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Log.w("printing","Background");

            Log.w("SOcket","COnnect : "+mmSocket.isConnected());

            print();

            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!printHud.isShowing()){
                        printHud.show();
                    }

                }
            });



        }

        @Override
        protected void onPostExecute(String a) {
            if (printHud.isShowing()){
                printHud.dismiss();
//
            }
        }
    }
*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.w("ACITVITY", "RESULT");

        // bluetoothUtil.onActivityResultBluetooth(requestCode,resultCode,data);

       /* switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect

                if (resultCode == Activity.RESULT_OK) {

                    Log.w("CONE","HERE");
                    // Get the device MAC address
                    String address = data.getExtras()
                            .getString(EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    device = mBluetoothAdapter.getRemoteDevice(address);

                    new ConnectionProgress().execute();

                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    if(device==null||!mmSocket.isConnected()){
                        if(isPrint){
                            Log.w("PRINT","CONNECT AND PRINT");
                            isPrint=false;
                            Intent serverIntent = null;
                            serverIntent = new Intent(getContext(), BluetoothDeviceNearByList.class);
                            startActivityForResult(serverIntent, REQUEST_CONNECT_AND_PRINT_DEVICE);
                        }else {
                            Log.w("PRINT","CONNECT");

                            Intent serverIntent = null;
                            serverIntent = new Intent(getContext(), BluetoothDeviceNearByList.class);
                            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                        }

                    }else {
                        if (mmSocket.isConnected()) {
                            print();
                        } else {
                            //DisplayToast("Make sure bluetooth turn on both devices and choose printer only!!!");
                        }

                    }

                } else {
                    // User did not enable Bluetooth or an error occured
                    Log.d("DD", "BT not enabled");
                    //Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    // finish();
                   // DisplayToast("Bluetooth must be enable to print");
                }
                break;
            case REQUEST_CONNECT_AND_PRINT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getExtras()
                            .getString(EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    device = mBluetoothAdapter.getRemoteDevice(address);

                    new ConnectionAndPrintingProgress().execute();

                }

        }*/

    }

}
