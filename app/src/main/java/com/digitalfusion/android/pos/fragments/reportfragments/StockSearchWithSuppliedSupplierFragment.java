package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockSearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForStockSearchSuppliers;
import com.digitalfusion.android.pos.database.business.CategoryManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.example.searchview.MaterialSearchView;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StockSearchWithSuppliedSupplierFragment extends Fragment implements Serializable {
    private View mainLayoutView;
    //for stock recycler view
    private RecyclerView recyclerView;
    private List<StockItem> stockItemList;
    private Context context;
    //FAB add new stock
    private StockManager stockManager;
    private MaterialSearchView searchView;
    private List<String> filterList;
    private MaterialDialog filterDialog;
    private RVAdapterForFilter rvAdapterForFilter;
    private List<Category> categoryList;
    private CategoryManager categoryManager;
    private TextView filterTextView;
    private TextView searchedResultTxt;
    private LinearLayoutManager linearLayoutManager;
    private StockSearchAdapter stockSearchAdapter;
    private boolean isFilter = false;
    private Long choseCategoryId = 0l;
    private TextView noTransactionTextView;
    private boolean isAll = true;
    private boolean isSearch = true;
    private RVAdapterForStockSearchSuppliers rvAdapterForStockSearchSuppliers;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_stock_search_with_supplied_supplier, container, false);
        context = getContext();
        setHasOptionsMenu(true);


        String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.product_by_supplier}).getString(0);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);


        stockManager = new StockManager(context);

        categoryManager = new CategoryManager(context);

        linearLayoutManager = new LinearLayoutManager(context);

        categoryList = categoryManager.getAllCategories();

        filterList = new ArrayList<>();

        loadIngUI();

        String all = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);

        filterList.add(all);

        for (Category c : categoryList) {
            filterList.add(c.getName());
        }

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();

        rvAdapterForStockSearchSuppliers = new RVAdapterForStockSearchSuppliers(stockItemList);

        handleOnClick();

        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setAdapter(rvAdapterForStockSearchSuppliers);

        setListeners();

        new LoadProgressDialog().execute();


        return mainLayoutView;
    }

    private void setListeners() {
        scrollListener();

        rvAdapterForStockSearchSuppliers.setClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                nextProcess(postion, stockItemList);
            }
        });
        mainLayoutView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Log.w("here", "toch");

                //                simpleOnGestureListener.onTouchEvent(event);

                return false;
            }
        });
    }

    private void scrollListener() {
        recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                rvAdapterForStockSearchSuppliers.setShowLoader(true);

                loadMore(stockItemList.size(), 10);

            }
        });
    }

    private void nextProcess(int postion, List<StockItem> stockItemList) {
        Bundle                             bundle                             = new Bundle();
        SimpleReportFragmentWithSearchView simpleReportFragmentWithSearchView = new SimpleReportFragmentWithSearchView();
        bundle.putString("reportType", "supplier by product");
        bundle.putLong("stockID", stockItemList.get(postion).getId());
        bundle.putString("stockName", stockItemList.get(postion).getName());
        //Log.e("header", stockItemList.get(postion).getId() + " ");
        simpleReportFragmentWithSearchView.setArguments(bundle);
        // MainActivity.replacingFragment(simpleReportFragmentWithSearchView);

        bundle.putSerializable("frag", (Serializable) simpleReportFragmentWithSearchView);

        Intent detailIntent = new Intent(context, DetailActivity.class);

        detailIntent.putExtras(bundle);

        startActivity(detailIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
        //new CreateReportTask().execute();
    }

    private void handleOnClick() {
        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                filterTextView.setText(filterList.get(position));
                isSearch = false;
                searchedResultTxt.setVisibility(View.INVISIBLE);
                isFilter = true;
                if (position != 0) {

                    isAll = false;

                    choseCategoryId = categoryList.get(position - 1).getId();
                    refreshStockList(0, 10);

                } else {
                    isAll = true;

                    refreshStockList(0, 10);
                }

                filterDialog.dismiss();
                scrollListener();
            }
        });

        stockSearchAdapter = new StockSearchAdapter(context, stockManager);

        searchView.setAdapter(stockSearchAdapter);

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                nextProcess(position, stockSearchAdapter.getSuggestion());
                //                stockItemList = new ArrayList<>();
                //
                //                Log.w("here","hello In Search");
                //
                //                stockItemList = stockManager.getAllStocksWithNoOfSuppliersByStockID(stockSearchAdapter.getSuggestionList().get(position).getId());
                //
                //                isSearch=true;
                //
                //                isAll = false;
                //                isFilter = false;
                //
                //                refreshRecyclerView();
                //
                //                searchView.closeSearch();
                //
                //                filterTextView.setText("-");
                //
                //                searchedResultTxt.setVisibility(View.VISIBLE);

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });

        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });


    }


    private void buildDateFilterDialog() {
        TypedArray filterByCategory = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_category});

        filterDialog = new MaterialDialog.Builder(context).
                title(filterByCategory.getString(0))

                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }


    public void initializingStockViewList() {

        stockItemList = stockManager.getAllStocksWithNoOfSuppliers(0, 10);

    }

    public void loadMore(int startLimit, int endLimit) {
        if (isAll) {
            Log.w("Here ", "IN ALL");
            stockItemList.addAll(stockManager.getAllStocksWithNoOfSuppliers(startLimit, endLimit));
        } else if (isSearch) {
            //donothing
            Log.w("Here ", "IN Search");
        } else if (isFilter) {
            Log.w("Here ", "IN filter");
            stockItemList.addAll(stockManager.getAllStocksWithNoOfSuppliersByCategoryID(startLimit, endLimit, choseCategoryId));
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                refreshRecyclerView();

                rvAdapterForStockSearchSuppliers.setShowLoader(false);

            }
        }, 500);
    }

    public void refreshStockList(int startLimit, int endLimit) {

        if (isAll && isFilter) {

            stockItemList = stockManager.getAllStocksWithNoOfSuppliers(startLimit, endLimit);

        } else if (!isAll && isFilter) {

            //reoder category filter

            stockItemList = stockManager.getAllStocksWithNoOfSuppliersByCategoryID(startLimit, endLimit, choseCategoryId);

        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshRecyclerView();
                rvAdapterForStockSearchSuppliers.setShowLoader(false);
            }
        }, 500);

    }

    private void refreshRecyclerView() {

        if (stockItemList.size() > 0) {

            recyclerView.setVisibility(View.VISIBLE);

            noTransactionTextView.setVisibility(View.GONE);


            rvAdapterForStockSearchSuppliers.setStockItemList(stockItemList);

            rvAdapterForStockSearchSuppliers.notifyDataSetChanged();

        } else {

            recyclerView.setVisibility(View.GONE);

            noTransactionTextView.setVisibility(View.VISIBLE);


        }

    }


    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});

        int attributeResourceId = a.getResourceId(0, 0);

        searchView.setCursorDrawable(attributeResourceId);

    }

    public void loadIngUI() {

        noTransactionTextView = (TextView) mainLayoutView.findViewById(R.id.no_transaction);

        filterTextView = (TextView) mainLayoutView.findViewById(R.id.filter_one);

        searchedResultTxt = (TextView) mainLayoutView.findViewById(R.id.searched_result_txt);

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.stock_list_rv);

        loadUIFromToolbar();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.main, menu);

        MenuItem item = menu.findItem(R.id.action_search);

        searchView.setMenuItem(item);

        super.onCreateOptionsMenu(menu, inflater);
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializingStockViewList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String label = "";

            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {

            refreshRecyclerView();

            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
