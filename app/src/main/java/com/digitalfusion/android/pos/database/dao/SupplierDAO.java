package com.digitalfusion.android.pos.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteException;

import com.digitalfusion.android.pos.database.model.Supplier;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 9/5/16.
 */
public class SupplierDAO extends ParentDAO {

    private static ParentDAO supplierDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private List<Supplier> supplierList;
    private Supplier supplier;
    private boolean status;

    private SupplierDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        supplier = new Supplier();
    }

    public static SupplierDAO getSupplierDapInstance(Context context) {
        if (supplierDaoInstance == null) {
            supplierDaoInstance = new SupplierDAO(context);
        }
        return (SupplierDAO) supplierDaoInstance;
    }

    public Long addNewSupplier(String name, String businessName, String address, String phoneNo, InsertedBooleanHolder flag) {
        genID = idGeneratorDAO.getLastIdValue("Supplier");
        genID++;
        databaseWriteTransaction(flag);
        try {
            contentValues = new ContentValues();
            contentValues.put(AppConstant.SUPPLIER_ID, genID);
            contentValues.put(AppConstant.SUPPLIER_NAME, name);
            contentValues.put(AppConstant.SUPPLIER_BUSINESS_NAME, businessName);
            contentValues.put(AppConstant.SUPPLIER_ADDRESS, address);
            contentValues.put(AppConstant.SUPPLIER_PHONE_NUM, phoneNo);
            contentValues.put(AppConstant.SUPPLIER_BALANCE, 0.0);
            database.insert(AppConstant.SUPPLIER_TABLE_NAME, null, contentValues);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return genID;
    }

    public List<Supplier> getAllSuppliers(int startLimit, int endLimit, int isDeleted) {

        supplierList = new ArrayList<>();

        databaseReadTransaction(flag);
        try {
            query = "select " + AppConstant.SUPPLIER_ID + ", " + AppConstant.SUPPLIER_NAME + ", " + AppConstant.SUPPLIER_BUSINESS_NAME + ", " + AppConstant.SUPPLIER_ADDRESS + ", " +
                    AppConstant.SUPPLIER_PHONE_NUM + ", " + AppConstant.SUPPLIER_BALANCE + " from " + AppConstant.SUPPLIER_TABLE_NAME + " where " + AppConstant.SUPPLIER_ID + " <> 1 and " + AppConstant.SUPPLIER_IS_DELETED + " <> " + isDeleted + " limit " + startLimit + ", " + endLimit;
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    supplier = new Supplier();
                    supplier.setId(cursor.getLong(0));
                    supplier.setName(cursor.getString(1));
                    supplier.setBusinessName(cursor.getString(2));
                    supplier.setAddress(cursor.getString(3));
                    supplier.setPhoneNo(cursor.getString(4));
                    supplier.setBalance(cursor.getDouble(5));
                    supplierList.add(supplier);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return supplierList;
    }

    public Supplier getSupplierById(Long id) {


        Supplier supplier = new Supplier();
        databaseReadTransaction(flag);
        try {
            query = "select " + AppConstant.SUPPLIER_ID + ", " + AppConstant.SUPPLIER_NAME + ", " + AppConstant.SUPPLIER_BUSINESS_NAME + ", " + AppConstant.SUPPLIER_ADDRESS + ", " +
                    AppConstant.SUPPLIER_PHONE_NUM + ", " + AppConstant.SUPPLIER_BALANCE + " from " + AppConstant.SUPPLIER_TABLE_NAME + " where " + AppConstant.SUPPLIER_ID + " = " + id;
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {

                    supplier.setId(cursor.getLong(0));
                    supplier.setName(cursor.getString(1));
                    supplier.setBusinessName(cursor.getString(2));
                    supplier.setAddress(cursor.getString(3));
                    supplier.setPhoneNo(cursor.getString(4));
                    supplier.setBalance(cursor.getDouble(5));

                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return supplier;
    }

    public boolean checkSupplierBalance(Long id) {
        boolean success = false;
        databaseReadTransaction(flag);
        try {
            query = "select " + AppConstant.SUPPLIER_BALANCE + " from " + AppConstant.SUPPLIER_TABLE_NAME + " where " + AppConstant.SUPPLIER_ID + " = " + id;
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                if (cursor.getDouble(0) > 0) {
                    success = false;
                } else {
                    success = true;
                }
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return success;
    }

    public boolean deleteSupplier(Long id) {
        databaseWriteTransaction(flag);
        query = "update " + AppConstant.SUPPLIER_TABLE_NAME + " set " + AppConstant.SUPPLIER_IS_DELETED + " = 1 where " + AppConstant.SUPPLIER_ID + " = " + id;
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public List<Supplier> getAllSuppliersOutstand() {
        supplierList = new ArrayList<>();
        databaseReadTransaction(flag);
        try {
            query = "select " + AppConstant.SUPPLIER_ID + ", " + AppConstant.SUPPLIER_NAME + ", " + AppConstant.SUPPLIER_BUSINESS_NAME + ", " + AppConstant.SUPPLIER_ADDRESS + ", " +
                    AppConstant.SUPPLIER_PHONE_NUM + ", " + AppConstant.SUPPLIER_BALANCE + " from " + AppConstant.SUPPLIER_TABLE_NAME + " where " + AppConstant.SUPPLIER_BALANCE + " >0";
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    supplier = new Supplier();
                    supplier.setId(cursor.getLong(0));
                    supplier.setName(cursor.getString(1));
                    supplier.setBusinessName(cursor.getString(2));
                    supplier.setAddress(cursor.getString(3));
                    supplier.setPhoneNo(cursor.getString(4));
                    supplier.setBalance(cursor.getDouble(5));
                    supplierList.add(supplier);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return supplierList;
    }

    public ArrayList<Supplier> getAllSuppliersArrayList() {
        ArrayList supplierVOList = new ArrayList<>();
        databaseReadTransaction(flag);
        try {
            query = "select " + AppConstant.SUPPLIER_ID + ", " + AppConstant.SUPPLIER_NAME + ", " + AppConstant.SUPPLIER_BUSINESS_NAME + ", " + AppConstant.SUPPLIER_ADDRESS + ", " +
                    AppConstant.SUPPLIER_PHONE_NUM + ", " + AppConstant.SUPPLIER_BALANCE + " from " + AppConstant.SUPPLIER_TABLE_NAME + " where " + AppConstant.SUPPLIER_IS_DELETED + " <> 1";
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    supplier = new Supplier();
                    supplier.setId(cursor.getLong(0));
                    supplier.setName(cursor.getString(1));
                    supplier.setBusinessName(cursor.getString(2));
                    supplier.setAddress(cursor.getString(3));
                    supplier.setPhoneNo(cursor.getString(4));
                    supplier.setBalance(cursor.getDouble(5));
                    supplierVOList.add(supplier);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return supplierVOList;
    }

    public boolean checkSupplierAlreadyExists(String name) {
        status = false;
        query = "select " + AppConstant.SUPPLIER_ID + " from " + AppConstant.SUPPLIER_TABLE_NAME + " where " + AppConstant.SUPPLIER_NAME + " = ? ";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{name});
            if (cursor.moveToFirst()) {
                status = true;
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return status;
    }

    public Long findIDByNameAndBusinessName(String name, String businessName) {
        id = null;
        query = "select " + AppConstant.SUPPLIER_ID + " from " + AppConstant.SUPPLIER_TABLE_NAME + " where " + AppConstant.SUPPLIER_NAME + " = ? and " + AppConstant.SUPPLIER_BUSINESS_NAME + "=?";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{name, businessName});
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }

    public Long findIDByBusinessName(String businessName) {
        id = null;
        query = "select " + AppConstant.SUPPLIER_ID + " from " + AppConstant.SUPPLIER_TABLE_NAME + " where " + AppConstant.SUPPLIER_BUSINESS_NAME + "=?";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{businessName});
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }

    public Long findIDByName(String name) {
        id = null;
        query = "select " + AppConstant.SUPPLIER_ID + " from " + AppConstant.SUPPLIER_TABLE_NAME + " where " + AppConstant.SUPPLIER_NAME + "=?";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{name});
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }

    public boolean updateSupplierByID(String name, String businessName, String address, String phoneNo, Long id) {
        query = "update " + AppConstant.SUPPLIER_TABLE_NAME + " set " + AppConstant.SUPPLIER_NAME + "=?, " + AppConstant.SUPPLIER_BUSINESS_NAME + "=?, " + AppConstant.SUPPLIER_ADDRESS +
                "=?, " + AppConstant.SUPPLIER_PHONE_NUM + "=? where " + AppConstant.SUPPLIER_ID + "=" + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{name, businessName, address, phoneNo});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean updateSupplierByID(String phoneNo,String supplierAddress, Long id) {
        query = "update " + AppConstant.SUPPLIER_TABLE_NAME + " set " + AppConstant.SUPPLIER_PHONE_NUM + "=? , " + AppConstant.SUPPLIER_ADDRESS + "=?  where " + AppConstant.SUPPLIER_ID + "=" + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{phoneNo,supplierAddress});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public List<Supplier> getAllSuppliersByNameOnSearch(int startLimit, int endLimit, String queryStr, int isDeleted) {
        supplierList = new ArrayList<>();
        databaseReadTransaction(flag);
        try {
            query = "select " + AppConstant.SUPPLIER_ID + ", " + AppConstant.SUPPLIER_NAME + ", " + AppConstant.SUPPLIER_BUSINESS_NAME + ", " + AppConstant.SUPPLIER_ADDRESS + ", " +
                    AppConstant.SUPPLIER_PHONE_NUM + ", " + AppConstant.SUPPLIER_BALANCE + " from " + AppConstant.SUPPLIER_TABLE_NAME + " where " + AppConstant.SUPPLIER_NAME + " like ? || '%' and " + AppConstant.SUPPLIER_IS_DELETED + " <> " +
                    isDeleted + " limit " + startLimit + ", " + endLimit;
            cursor = database.rawQuery(query, new String[]{queryStr});
            if (cursor.moveToFirst()) {
                do {
                    supplier = new Supplier();
                    supplier.setId(cursor.getLong(0));
                    supplier.setName(cursor.getString(1));
                    supplier.setBusinessName(cursor.getString(2));
                    supplier.setAddress(cursor.getString(3));
                    supplier.setPhoneNo(cursor.getString(4));
                    supplier.setBalance(cursor.getDouble(5));
                    supplierList.add(supplier);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return supplierList;
    }

}
