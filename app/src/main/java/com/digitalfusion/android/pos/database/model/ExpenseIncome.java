package com.digitalfusion.android.pos.database.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 9/6/16.
 */
public class ExpenseIncome implements Serializable {

    private Long id;
    private String name;
    private Type type;
    private String date;
    private String day;
    private String month;
    private String year;
    private Double amount;
    private Long nameID;
    private String remark;

    public ExpenseIncome() {
    }

    public static List<String> getTypeFilterList() {

        List<String> filterList = new ArrayList<>();

        filterList.add(Type.All.toString());

        filterList.add(Type.Income.toString());

        filterList.add(Type.Expense.toString());

        return filterList;

    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getNameID() {
        return nameID;
    }

    public void setNameID(Long nameID) {
        this.nameID = nameID;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public boolean isEmptyRemark() {

        if (remark != null && remark.trim().length() > 0) {
            return remark.isEmpty();
        }
        return true;
    }

    public boolean isIncome() {
        return (type == Type.Income);
    }

    public enum Type {Income, Expense, All}

}
