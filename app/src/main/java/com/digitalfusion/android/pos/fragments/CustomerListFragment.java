package com.digitalfusion.android.pos.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.CustomerSearchAdapter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwiperAdapterForCustomerList;
import com.digitalfusion.android.pos.database.business.CustomerManager;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.views.FusionToast;
import com.example.searchview.MaterialSearchView;

import java.util.List;

/**
 * Created by MD003 on 8/29/16.
 */
public class CustomerListFragment extends Fragment {

    private View mainLayout;

    private FloatingActionButton addNewCustomerFab;

    private RecyclerView customerListRecyclerView;

    private CustomerManager customerManager;

    private Context context;

    private List<Customer> customerList;

    private RVSwiperAdapterForCustomerList rvSwiperAdapterForCustomerList;

    private MaterialDialog addNewCustomerMaterialDialog;

    //UI component for addNewCustomerDialog

    private EditText customerNameTextInputEditTextMd;

    private EditText customerPhoneTextInputEditTextMd;

    private EditText customerAddressTextInputEditTextMd;

    private MDButton saveBtnMd;


    private MaterialDialog editCustomerMaterialDialog;

    //UI component for EditCustomerDialog

    private EditText editCustomerNameTextInputEditTextMd;

    private EditText editCustomerPhoneTextInputEditTextMd;

    private EditText editCustomerAddressTextInputEditTextMd;

    private MDButton editSaveBtnMd;


    //Values
    private String customerName;

    private String customerPhone;

    private String customerAddress;

    private int editPosition;

    private MaterialSearchView searchView;

    private boolean isEdit;


    private boolean isSearch = false;

    private boolean shouldLoad = true;

    private String searchText = "";

    private CustomerSearchAdapter customerSearchAdapter;

    private TextView noTransactionTextView;

    private TextView searchedResultTxt;


    private MaterialDialog deleteAlertDialog;

    private MaterialDialog deniedDeleteAlertDialog;

    private Button yesSaleDeleteMdButton;

    private int deletepos;

    private boolean isAdd;

    private Customer customer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.customer, null);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Customer List");

        context = getContext();

        customerManager = new CustomerManager(context);

        buildEditCustomerDialog();

        buildAddNewCustomerDialog();
        buildOrderCancelAlertDialog();
        loadUI();
        MainActivity.setCurrentFragment(this);
        configRecyclerView();

        addNewCustomerFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* clearDialog();

                SpannableString content = new SpannableString("New Customer");
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                addNewCustomerMaterialDialog.setTitle(content);

                addNewCustomerMaterialDialog.show();*/


                isAdd = true;

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_CUSTOMER);

                startActivity(addCurrencyIntent);

            }
        });

        rvSwiperAdapterForCustomerList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

              /*  isEdit=true;

                editPosition = postion;

                setDataEditDialog();

                SpannableString content = new SpannableString("Edit Customer");
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

                addNewCustomerMaterialDialog.setTitle(content);

                addNewCustomerMaterialDialog.show();*/

                customer = customerList.get(postion);
                Bundle bundle = new Bundle();

                isAdd = false;


                bundle.putSerializable(AddEditCustomerFragment.KEY, customerList.get(postion));

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtras(bundle);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_CUSTOMER);

                startActivity(addCurrencyIntent);

            }
        });

        addNewCustomerMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                isEdit = false;
            }
        });

        rvSwiperAdapterForCustomerList.setViewDetailClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Log.w("here ", "detail click");
                Bundle bundle = new Bundle();

                bundle.putSerializable("customer", customerList.get(postion));

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.CUSTOMER_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);

            }
        });

        rvSwiperAdapterForCustomerList.setCallClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {


                Intent intent = new Intent(Intent.ACTION_DIAL);


                String ph[] = customerList.get(postion).getPhoneNo().split(",");

                String temp = "tel:" + ph[0];
                intent.setData(Uri.parse(temp));

                startActivity(intent);
            }
        });

        rvSwiperAdapterForCustomerList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                deletepos = postion;

                deleteAlertDialog.show();
                Log.w("hrerer", " show");


            }
        });

        yesSaleDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // supplierBusiness.(salesHistoryViewList.get(deletepos).getId());


                //                Log.w("hello delete",customerList.get(deletepos).getName()+"DDD");


                //rvSwipeAdapterForSaleTransactions.setSalesHistoryList(salesHistoryViewList);

                if (customerManager.checkCustomerBalance(customerList.get(deletepos).getId())) {

                    if (customerList.get(deletepos).getId() != 1) {

                        Log.w("CUstomer id", customerList.get(deletepos).getId() + " SSS");

                        customerManager.deleteCustomer(customerList.get(deletepos).getId());

                        if (customerList.size() == 1) {
                            customerList.remove(deletepos);
                            rvSwiperAdapterForCustomerList.setCustomerList(customerList);
                            rvSwiperAdapterForCustomerList.notifyDataSetChanged();
                        } else {

                            customerList.remove(deletepos);
                            rvSwiperAdapterForCustomerList.notifyItemRemoved(deletepos);
                            rvSwiperAdapterForCustomerList.notifyItemRangeChanged(deletepos, customerList.size());

                        }
                    }


                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);

                    deleteAlertDialog.dismiss();
                } else {
                    deleteAlertDialog.dismiss();
                    deniedDeleteAlertDialog.show();
                }

            }
        });


        saveBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEdit) {
                    if (checkValidation()) {

                        getValuesFromView();

                        boolean status = customerManager.updateCustomer(customerName, customerAddress, customerPhone, customerList.get(editPosition).getId());

                        if (status) {
                            String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.customer_updated_successfully}).getString(0);

                            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);


                            addNewCustomerMaterialDialog.dismiss();

                            refreshCustomerList();

                        }

                    }
                } else {
                    if (checkValidation()) {

                        getValuesFromView();

                        InsertedBooleanHolder status = new InsertedBooleanHolder();

                        customerManager.addNewCustomer(customerName, customerAddress, customerPhone, 0.0, status);

                        if (status.isInserted()) {
                            String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_customer_added_successfully}).getString(0);

                            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);


                            addNewCustomerMaterialDialog.dismiss();

                            refreshCustomerList();

                        }

                    }
                }


            }
        });

        customerListRecyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                if (shouldLoad) {

                    rvSwiperAdapterForCustomerList.setShowLoader(true);

                    loadmore();

                }

            }
        });

        customerSearchAdapter = new CustomerSearchAdapter(context, customerManager);

        searchView.setAdapter(customerSearchAdapter);

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // shouldLoad = false;

                // customerList = new ArrayList<Customer>();

                // customerList.add(customerSearchAdapter.getSuggestionList().get(position));

                //  refreshRecyclerView();

                searchView.closeSearch();

                Log.w("here ", "detail click");
                Bundle bundle = new Bundle();

                bundle.putSerializable("customer", customerSearchAdapter.getSuggestion().get(position));

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.CUSTOMER_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);

                //                searchedResultTxt.setVisibility(View.VISIBLE);
            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                shouldLoad = true;

                isSearch = true;

                searchText = query;

                customerSearch(0, 10, query);

                refreshRecyclerView();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                shouldLoad = true;

                isSearch = false;

                return false;

            }
        });

        refreshRecyclerView();

        customerListRecyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {

                Log.w("V X", velocityX + " S");

                Log.w("V Y", velocityY + " S");

                if (velocityY > 100) {
                    addNewCustomerFab.hide();
                } else if (velocityY < -100) {
                    addNewCustomerFab.show();
                }


                return false;
            }
        });

        return mainLayout;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (customer != null) {

            customer = customerManager.getCustomerByID(customer.getId());

            rvSwiperAdapterForCustomerList.updateItem(customer);

            customer = null;
        }

        if (isAdd) {

            isAdd = false;

            startLoad();

        }


    }

    public void buildAddNewCustomerDialog() {


        SpannableString content = new SpannableString("New Customer");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

        addNewCustomerMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.new_customer_md, true).title(content).positiveText("Save").negativeText("Cancel").build();

    }

    private void buildOrderCancelAlertDialog() {

        //TypedArray no = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        //TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        String   sureToDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete}).getString(0);
        TextView textView     = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(sureToDelete);


        yesSaleDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });


        deniedDeleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.alert_dialog_with_only_ok_button, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        String   titleText     = context.getTheme().obtainStyledAttributes(new int[]{R.attr.denied_customer_delete}).getString(0);
        TextView titleTextView = (TextView) deniedDeleteAlertDialog.findViewById(R.id.title);
        titleTextView.setText(titleText);


        deniedDeleteAlertDialog.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deniedDeleteAlertDialog.dismiss();
            }
        });

    }


    public void buildEditCustomerDialog() {

        editCustomerMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.new_customer_md, true).title("Edit Customer").positiveText("Save").negativeText("Cancel").build();

    }

    public boolean checkValidation() {

        boolean status = true;

        if (customerNameTextInputEditTextMd.getText().toString().trim().length() < 1) {
            String enterCName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_customer_name}).getString(0);

            customerNameTextInputEditTextMd.setError(enterCName);

            status = false;

        }

        return status;

    }

    public void clearDialog() {

        customerNameTextInputEditTextMd.setText(null);

        customerPhoneTextInputEditTextMd.setText(null);

        customerAddressTextInputEditTextMd.setText(null);

    }

    public void setDataEditDialog() {

        customerNameTextInputEditTextMd.setText(customerList.get(editPosition).getName());

        customerPhoneTextInputEditTextMd.setText(customerList.get(editPosition).getPhoneNo());

        customerAddressTextInputEditTextMd.setText(customerList.get(editPosition).getAddress());

    }


    public void getValuesFromView() {

        customerName = customerNameTextInputEditTextMd.getText().toString().trim();

        customerPhone = customerPhoneTextInputEditTextMd.getText().toString().trim();

        customerAddress = customerAddressTextInputEditTextMd.getText().toString().trim();

    }

    public void refreshCustomerList() {

        customerList = customerManager.getAllCustomers(0, 10);

        refreshRecyclerView();

    }

    private void refreshRecyclerView() {

        if (customerList.size() > 0) {

            customerListRecyclerView.setVisibility(View.VISIBLE);

            noTransactionTextView.setVisibility(View.GONE);

            rvSwiperAdapterForCustomerList.setCustomerList(customerList);

            rvSwiperAdapterForCustomerList.notifyDataSetChanged();


        } else {

            customerListRecyclerView.setVisibility(View.GONE);

            noTransactionTextView.setVisibility(View.VISIBLE);
        }

    }

    public void configRecyclerView() {

        customerList = customerManager.getAllCustomers(0, 10);


        rvSwiperAdapterForCustomerList = new RVSwiperAdapterForCustomerList(customerList);

        customerListRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        customerListRecyclerView.setAdapter(rvSwiperAdapterForCustomerList);


    }

    public void startLoad() {
        customerList = customerManager.getAllCustomers(0, 10);

        Log.w("cust size", customerList.size() + " SSS");
        refreshRecyclerView();
        customerListRecyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                if (shouldLoad) {

                    rvSwiperAdapterForCustomerList.setShowLoader(true);

                    loadmore();

                }

            }
        });

        rvSwiperAdapterForCustomerList.setCustomerList(customerList);
        rvSwiperAdapterForCustomerList.notifyDataSetChanged();

    }

    public void loadmore() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isSearch) {
                    customerList.addAll(loadMore(customerList.size(), 9, searchText));
                } else {
                    customerList.addAll(loadMore(customerList.size(), 9));
                }

                customerList.addAll(loadMore(customerList.size(), 9));

                rvSwiperAdapterForCustomerList.setShowLoader(false);

                refreshRecyclerView();
            }
        }, 500);
    }

    public List<Customer> loadMore(int startLimit, int endLimit) {

        return customerManager.getAllCustomers(startLimit, endLimit);


    }

    public List<Customer> loadMore(int startLimit, int endLimit, String query) {

        return customerManager.getAllCustomersByNameOnSearch(startLimit, endLimit, query);


    }

    public void customerSearch(int startLimit, int endLimit, String query) {

        customerList = customerManager.getAllCustomersByNameOnSearch(startLimit, endLimit, query);


    }

    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        searchView.setCursorDrawable(attributeResourceId);

    }

    public void loadUI() {

        loadUIFromToolbar();

        noTransactionTextView = (TextView) mainLayout.findViewById(R.id.no_transaction);

        searchedResultTxt = (TextView) mainLayout.findViewById(R.id.searched_result_txt);

        customerListRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.customer_list_rv);

        addNewCustomerFab = (FloatingActionButton) mainLayout.findViewById(R.id.add_new_customer);

        saveBtnMd = addNewCustomerMaterialDialog.getActionButton(DialogAction.POSITIVE);

        customerNameTextInputEditTextMd = (EditText) addNewCustomerMaterialDialog.findViewById(R.id.cust_name_in_add_customer_TIET);

        customerPhoneTextInputEditTextMd = (EditText) addNewCustomerMaterialDialog.findViewById(R.id.cust_phone_in_add_customer_TIET);

        customerAddressTextInputEditTextMd = (EditText) addNewCustomerMaterialDialog.findViewById(R.id.cust_address_in_add_customer_TIET);


        editSaveBtnMd = editCustomerMaterialDialog.getActionButton(DialogAction.POSITIVE);

        editCustomerNameTextInputEditTextMd = (EditText) editCustomerMaterialDialog.findViewById(R.id.cust_name_in_add_customer_TIET);

        editCustomerPhoneTextInputEditTextMd = (EditText) editCustomerMaterialDialog.findViewById(R.id.cust_phone_in_add_customer_TIET);

        editCustomerAddressTextInputEditTextMd = (EditText) editCustomerMaterialDialog.findViewById(R.id.cust_address_in_add_customer_TIET);


    }


}
