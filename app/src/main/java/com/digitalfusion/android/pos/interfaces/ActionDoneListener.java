package com.digitalfusion.android.pos.interfaces;

import com.digitalfusion.android.pos.database.model.Discount;

/**
 * Created by MD002 on 1/8/18.
 */

public interface ActionDoneListener {
    public void onActionDone(Discount discount);
}
