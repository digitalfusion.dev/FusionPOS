package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.model.StockItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 6/8/17.
 */

public class StockNameOrCodeAutoCompleteAdapter extends ArrayAdapter<StockItem> {

    Context context;
    StockManager stockManager;
    StockItem stockAutoCompleteView;

    List<StockItem> suggestionList;
    List<StockItem> searchList;
    String queryTxt = "";
    int lenght = 0;
    boolean flag = false;

    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((StockItem) resultValue).getName();
            stockAutoCompleteView = (StockItem) resultValue;

            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                searchList = new ArrayList<>();
                queryTxt = constraint.toString().trim();
                searchList = stockManager.getActiveStocksByNameOrCode(0, 10, queryTxt);

              /*  if(suggestionList.size()==1){

                    if(callback!=null){

                        callback.onOneResut(suggestionList.get(0));
                        return new FilterResults();

                    }else {


                        lenght=constraint.length();

                        FilterResults filterResults = new FilterResults();
                        filterResults.values = suggestionList;
                        filterResults.count = suggestionList.size();
                        return filterResults;
                    }
*/

                //    }else {

                FilterResults filterResults = new FilterResults();
                filterResults.values = searchList;
                filterResults.count = searchList.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<StockItem> filterList = (ArrayList<StockItem>) results.values;

            if (filterList == null) {
                suggestionList = new ArrayList<>();
            } else {
                Log.w("helre", filterList.size() + " reslutle sixe");
                setSuggestionList(filterList);
            }
            notifyDataSetChanged();

        }
    };

    public StockNameOrCodeAutoCompleteAdapter(Context context, StockManager stockManager) {
        super(context, -1);

        this.context = context;
        this.stockManager = stockManager;

        suggestionList = new ArrayList<>();
        searchList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return suggestionList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.stock_item_auto_complete_view1, parent, false);
        }

        viewHolder = new ViewHolder(convertView);

        if (!suggestionList.isEmpty() && suggestionList.size() > 0) {
            String suggestionName = suggestionList.get(position).getName();
            if (queryTxt.equalsIgnoreCase(suggestionName)) {
                Spannable spanText = highLightSuggestion(suggestionName);

                viewHolder.nameTextView.setText(spanText);
            } else {
                viewHolder.nameTextView.setText(suggestionList.get(position).getName());
            }

            // viewHolder.customerNameTextView.setText(salesHistoryViewList.get(position).getItemName());

            String suggestionCode = suggestionList.get(position).getCodeNo();
            if (queryTxt.equalsIgnoreCase(suggestionCode)) {
                Spannable spanText = highLightSuggestion(suggestionCode);

                viewHolder.codeTextView.setText(spanText);
            } else {
                viewHolder.codeTextView.setText(suggestionList.get(position).getCodeNo());
            }

            viewHolder.qtyTextView.setText(suggestionList.get(position).getInventoryQty() + "");
            viewHolder.unitTextView.setText(suggestionList.get(position).getUnitName());
        }

        return convertView;

    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    @Nullable
    @Override
    public StockItem getItem(int position) {
        return suggestionList.get(position);
    }

    public List<StockItem> getSuggestionList() {
        return suggestionList;
    }

    public void setSuggestionList(List<StockItem> suggestionList) {
        this.suggestionList = suggestionList;
    }

    private Spannable highLightSuggestion(String suggestion) {
        Spannable spanText = new SpannableString(suggestion);

        spanText.setSpan(new ForegroundColorSpan(getContext().getResources()
                .getColor(R.color.accent)), 0, queryTxt.length(), 0);

        return spanText;
    }

    static class ViewHolder {
        TextView codeTextView;
        TextView nameTextView;
        TextView qtyTextView;
        TextView unitTextView;

        public ViewHolder(View itemView) {
            this.nameTextView = (TextView) itemView.findViewById(R.id.name);
            this.codeTextView = (TextView) itemView.findViewById(R.id.code);
            qtyTextView = itemView.findViewById(R.id.qty);
            unitTextView = itemView.findViewById(R.id.unit);
        }

    }


}
