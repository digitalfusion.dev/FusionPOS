package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD001 on 11/3/16.
 */

public class RVSwipeAdapterForUserRoleList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private List<User> userList;
    private ClickListener editClickListener;
    private ClickListener deleteClickListener;


    public RVSwipeAdapterForUserRoleList(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_role_item_view, parent, false);

        return new UserRoleViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof UserRoleViewHolder) {

            final UserRoleViewHolder userRoleViewHolder = (UserRoleViewHolder) holder;

            POSUtil.makeZebraStrip(userRoleViewHolder.itemView, position);

            if (position == 0) {
                userRoleViewHolder.deleteButton.setVisibility(View.GONE);
            }
            userRoleViewHolder.userNameTextView.setText(userList.get(position).getUserName());
            userRoleViewHolder.userRoleTextView.setText(userList.get(position).getRole());
            userRoleViewHolder.userDescriptionTextView.setText(userList.get(position).getPasscode());

            //
            //            if (userList.get(position).getDescription() != null && !userList.get(position).getDescription().equalsIgnoreCase("")) {
            //
            //                userRoleViewHolder.userDescriptionTextView.setText(userList.get(position).getDescription());
            //            } else {
            //                userRoleViewHolder.userDescriptionTextView.setText(null);
            //                userRoleViewHolder.userDescriptionTextView.setHint(userRoleViewHolder.nodes);
            //            }

            userRoleViewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (editClickListener != null) {
                        userRoleViewHolder.swipeLayout.close(true);

                        editClickListener.onClick(position);
                    }
                }
            });

            userRoleViewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteClickListener != null) {
                        userRoleViewHolder.swipeLayout.close(true);
                        deleteClickListener.onClick(position);
                    }
                }
            });

            mItemManger.bindView(userRoleViewHolder.itemView, position);

        }

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    public class UserRoleViewHolder extends RecyclerView.ViewHolder {

        TextView userNameTextView;
        TextView userRoleTextView;
        TextView userDescriptionTextView;
        ImageButton editButton;
        ImageButton deleteButton;
        SwipeLayout swipeLayout;

        //        String nodes;

        public UserRoleViewHolder(View itemView) {
            super(itemView);

            userNameTextView = (TextView) itemView.findViewById(R.id.user_name);
            userRoleTextView = (TextView) itemView.findViewById(R.id.device_no);
            userDescriptionTextView = (TextView) itemView.findViewById(R.id.user_description);

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            editButton = (ImageButton) itemView.findViewById(R.id.edit);
            deleteButton = (ImageButton) itemView.findViewById(R.id.delete);


            //            nodes= itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.no_description}).getString(0);
        }

    }
}

