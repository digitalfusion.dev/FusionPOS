package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.model.Stock;
import com.digitalfusion.android.pos.database.model.StockAutoComplete;
import com.digitalfusion.android.pos.database.model.StockImage;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/17/16.
 */
public class StockDAO extends ParentDAO {

    private static ParentDAO stockDaoInstance;
    private Context context;
    private IdGeneratorDAO idGeneratorDAO;
    private StockItem stockItem;
    private List<StockItem> stockItemList;
    private Stock stock;
    private List<StockAutoComplete> stockAutoCompleteList;
    private StockAutoComplete stockAutoComplete;

    private StockDAO(Context context) {
        super(context);
        this.context = context;
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        stockItem = new StockItem();
        stock = new Stock();
    }

    public static StockDAO getStockDaoInstance(Context context) {
        if (stockDaoInstance == null) {
            stockDaoInstance = new StockDAO(context);
        }
        return (StockDAO) stockDaoInstance;
    }

    public void addNewStock(String codeNo, String barcode, String name, Long categoryID, Long unitID, int inventoryQty, int reorderLevel,
                            Double purchasePrice, Double wholesalePrice, Double normalPrice, Double retailPrice, List<StockImage> imageList, String description,
                            InsertedBooleanHolder isInserted) {
        genID = idGeneratorDAO.getLastIdValue("Stock");
        id = genID + 1;
        Log.w("hello", id + " SS");
        databaseWriteTransaction(isInserted);
        query = "insert into " + AppConstant.STOCK_TABLE_NAME + "(" + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_CODE_NUM + ", " + AppConstant.STOCK_BARCODE + ", " +
                AppConstant.STOCK_NAME + ", " + AppConstant.STOCK_CATEGORY_ID + ", " + AppConstant.STOCK_UNIT_ID + ", " + AppConstant.STOCK_INVENTORY_QTY + ", " + AppConstant.STOCK_REORDER_QTY
                + ", " + AppConstant.STOCK_PURCHASE_PRICE + ", " + AppConstant.STOCK_WHOLESALE_PRICE + ", " + AppConstant.STOCK_NORMAL_PRICE + ", " + AppConstant.STOCK_RETAIL_PRICE + //", " + AppConstant.STOCK_IMAGE +
                ", " + AppConstant.STOCK_DESCRIPTION + ", " + AppConstant.STOCK_OPENING_DATE + ", " + AppConstant.STOCK_OPENING_QTY + ", " + AppConstant.STOCK_OPENING_PRICE + ", " +
                AppConstant.CREATED_DATE + ", " + AppConstant.STOCK_OPENING_TIME + ") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, strftime('%s', 'now', 'localtime'));";
        try {
            statement = database.compileStatement(query);
            statement.bindLong(1, id);
            statement.bindString(2, codeNo);
            if (barcode == null) {
                statement.bindNull(3);
            } else {
                statement.bindString(3, barcode);
            }
            statement.bindString(4, name);
            statement.bindLong(5, categoryID);
            if (unitID == null) {
                statement.bindNull(6);
            } else {
                statement.bindLong(6, unitID);
            }
            statement.bindString(7, Integer.toString(inventoryQty));
            statement.bindString(8, Integer.toString(reorderLevel));
            statement.bindDouble(9, purchasePrice);
            statement.bindDouble(10, wholesalePrice);
            statement.bindDouble(11, normalPrice);
            statement.bindDouble(12, retailPrice);
            /*if (image == null){
                statement.bindNull(13);
            }else {
                statement.bindBlob(13, image);
            }*/
            if (description != null) {
                statement.bindString(13, description);
            } else {
                statement.bindNull(13);
            }
            statement.bindString(14, DateUtility.getTodayDate());
            statement.bindLong(15, inventoryQty);
            statement.bindDouble(16, purchasePrice);
            statement.bindString(17, DateUtility.getTodayDate());
            statement.execute();
            statement.clearBindings();
            if (!imageList.isEmpty()) {
                addImage(imageList);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
    }

    public Long addQuickStockSetup(String name, String stockCode, Double retailPrice, Double purchasePrice) {
        genID = idGeneratorDAO.getLastIdValue("Stock");
        id = genID + 1;
        query = "insert into " + AppConstant.STOCK_TABLE_NAME + "(" + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_CODE_NUM + ", " + AppConstant.STOCK_NAME + ", " + AppConstant.STOCK_CATEGORY_ID +
                ", " + AppConstant.STOCK_INVENTORY_QTY + "," + AppConstant.STOCK_RETAIL_PRICE
                + "," + AppConstant.STOCK_INVENTORY_QTY + ", " + AppConstant.STOCK_OPENING_DATE + ", " + AppConstant.STOCK_OPENING_QTY + ", " + AppConstant.STOCK_OPENING_PRICE + ", "
                + AppConstant.STOCK_PURCHASE_PRICE + ", " + AppConstant.STOCK_OPENING_TIME + ") values (" + id + ", ?, ?, " + 1 + ", " + 0 + "," + retailPrice + "," + 0 +
                ", " + DateUtility.getTodayDate() + " , " + 1 + ", " + purchasePrice + ", " + purchasePrice + ", strftime('%s', 'now', 'localtime'));";
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{stockCode, name});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return id;
    }

    public Long addQuickStockSetupPurchase(String name, String stockCode, Double purchasePrice, Double retailPrice) {
        genID = idGeneratorDAO.getLastIdValue("Stock");
        id = genID + 1;
        query = "insert into " + AppConstant.STOCK_TABLE_NAME + "(" + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_CODE_NUM + ", " + AppConstant.STOCK_NAME + ", " + AppConstant.STOCK_CATEGORY_ID +
                ", " + AppConstant.STOCK_INVENTORY_QTY + "," + AppConstant.STOCK_PURCHASE_PRICE + "," + AppConstant.STOCK_RETAIL_PRICE + "," + AppConstant.STOCK_REORDER_QTY + ", " + AppConstant.STOCK_OPENING_PRICE + " , " +
                AppConstant.STOCK_OPENING_QTY + ", " + AppConstant.STOCK_OPENING_DATE + AppConstant.STOCK_OPENING_TIME + ") values (" + id + ", ?, ?, " + 1 + ", " + 0 + "," + purchasePrice + ","
                + retailPrice + "," + 0 + ", " + purchasePrice + " , " + DateUtility.getTodayDate() + ", strftime('%s', 'now', 'localtime'));";
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{stockCode, name});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return id;
    }

    public boolean deleteStock(Long id) {
        databaseWriteTransaction(flag);
        query = "update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_IS_DELETED + " = 1 where " + AppConstant.STOCK_ID + " = " + id;
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    private void addImage(List<StockImage> imageList) {
        query = "insert into " + AppConstant.STOCK_IMAGE_TABLE_NAME + "(" + AppConstant.STOCK_IMAGE_ID + ", " + AppConstant.STOCK_IMAGE_STOCK_ID + ", " + AppConstant.STOCK_IMAGE_IMG
                + ") values (?, ?, ?);";
        statement = database.compileStatement(query);
        Log.w("stock id in add img", id + "SS before loop");
        for (StockImage img : imageList) {
            genID = idGeneratorDAO.getLastIdValue("StockImage");
            genID += 1;

            Log.w("stock id in add img", id + "SS");

            statement.bindLong(1, genID);
            statement.bindLong(2, id);

            if (img == null) {
                statement.bindNull(3);
            } else {
                statement.bindBlob(3, img.getImage());
            }
            statement.execute();
            statement.clearBindings();
        }
    }

    public boolean checkBarcodeAlreadyExists(String barcode) {
        database = databaseHelper.getWritableDatabase();
        query = "select " + AppConstant.STOCK_ID + " from " + AppConstant.STOCK_TABLE_NAME + " where " + AppConstant.STOCK_BARCODE + " = ?";
        cursor = database.rawQuery(query, new String[]{barcode});
        if (cursor.moveToFirst()) {
            return true;
        }
        cursor.close();
        return false;
    }

    public boolean checkStockCodeAlreadyExists(String stockCode) {
        database = databaseHelper.getWritableDatabase();
        query = "select " + AppConstant.STOCK_ID + " from " + AppConstant.STOCK_TABLE_NAME + " where " + AppConstant.STOCK_CODE_NUM + " = ?";
        cursor = database.rawQuery(query, new String[]{stockCode});
        if (cursor.moveToFirst()) {
            return true;
        }
        if (cursor != null) {
            cursor.close();
        }
        return false;
    }

    public Long getStockIDByStockCode(String stockCode) {
        long stockId = 0l;
        database = databaseHelper.getWritableDatabase();
        query = "select " + AppConstant.STOCK_ID + " from " + AppConstant.STOCK_TABLE_NAME + " where " + AppConstant.STOCK_CODE_NUM + " = ?";
        cursor = database.rawQuery(query, new String[]{stockCode});
        if (cursor.moveToFirst()) {
            stockId = cursor.getLong(0);
        }
        if (cursor != null) {
            cursor.close();
        }
        return stockId;
    }

    public List<StockItem> getAllStocks(int startLimit, int endLimit, int isDeleted) {
        stockItemList = new ArrayList<>();
        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_BARCODE + ", c." + AppConstant.CATEGORY_NAME +
                ", u." + AppConstant.UNIT_UNIT + ", s." + AppConstant.STOCK_INVENTORY_QTY + ", s." + AppConstant.STOCK_REORDER_QTY + ", s." + AppConstant.STOCK_PURCHASE_PRICE +
                ", s." + AppConstant.STOCK_WHOLESALE_PRICE + ", s." + AppConstant.STOCK_NORMAL_PRICE + ", s." + AppConstant.STOCK_RETAIL_PRICE + //", s." + AppConstant.STOCK_IMAGE +
                ", s." + AppConstant.STOCK_DESCRIPTION + ", s." + AppConstant.STOCK_NAME + ", u." + AppConstant.UNIT_ID + ", s." + AppConstant.STOCK_CATEGORY_ID + " from " + AppConstant.STOCK_TABLE_NAME + " s left outer join "
                + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + ", " + AppConstant.CATEGORY_TABLE_NAME
                + " c " + " where s."
                + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and " + AppConstant.STOCK_IS_DELETED + " <> " + isDeleted + " order by s." +
                AppConstant.STOCK_NAME + " asc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setBarcode(cursor.getString(2));
                    stockItem.setCategoryName(cursor.getString(3));
                    stockItem.setUnitName(cursor.getString(4));
                    stockItem.setInventoryQty(cursor.getInt(5));
                    stockItem.setReorderLevel(cursor.getInt(6));
                    stockItem.setPurchasePrice(cursor.getDouble(7));
                    stockItem.setWholesalePrice(cursor.getDouble(8));
                    stockItem.setNormalPrice(cursor.getDouble(9));
                    stockItem.setRetailPrice(cursor.getDouble(10));
                    //stockItem.setImageList(cursor.getBlob(11));
                    stockItem.setDescription(cursor.getString(11));
                    stockItem.setName(cursor.getString(12));

                    if (cursor.getLong(13) != 0) {
                        stockItem.setUnitID(cursor.getLong(13));
                    }

                    stockItem.setCategoryID(cursor.getLong(14));
                    stockItemList.add(stockItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockItemList;
    }

    public List<StockItem> getAllStocks(int isDeleted) {
        stockItemList = new ArrayList<>();
        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_BARCODE + ", c." + AppConstant.CATEGORY_NAME +
                ", u." + AppConstant.UNIT_UNIT + ", s." + AppConstant.STOCK_INVENTORY_QTY + ", s." + AppConstant.STOCK_REORDER_QTY + ", s." + AppConstant.STOCK_PURCHASE_PRICE +
                ", s." + AppConstant.STOCK_WHOLESALE_PRICE + ", s." + AppConstant.STOCK_NORMAL_PRICE + ", s." + AppConstant.STOCK_RETAIL_PRICE + //", s." + AppConstant.STOCK_IMAGE +
                ", s." + AppConstant.STOCK_DESCRIPTION + ", s." + AppConstant.STOCK_NAME + ", u." + AppConstant.UNIT_ID + ", s." + AppConstant.STOCK_CATEGORY_ID + " from " + AppConstant.STOCK_TABLE_NAME + " s left outer join "
                + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + ", " + AppConstant.CATEGORY_TABLE_NAME
                + " c " + " where s."
                + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and " + AppConstant.STOCK_IS_DELETED + " <> " + isDeleted + " order by s." +
                AppConstant.STOCK_NAME + " asc ";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setBarcode(cursor.getString(2));
                    stockItem.setCategoryName(cursor.getString(3));
                    stockItem.setUnitName(cursor.getString(4));
                    stockItem.setInventoryQty(cursor.getInt(5));
                    stockItem.setReorderLevel(cursor.getInt(6));
                    stockItem.setPurchasePrice(cursor.getDouble(7));
                    stockItem.setWholesalePrice(cursor.getDouble(8));
                    stockItem.setNormalPrice(cursor.getDouble(9));
                    stockItem.setRetailPrice(cursor.getDouble(10));
                    //stockItem.setImageList(cursor.getBlob(11));
                    stockItem.setDescription(cursor.getString(11));
                    stockItem.setName(cursor.getString(12));

                    if (cursor.getLong(13) != 0) {
                        stockItem.setUnitID(cursor.getLong(13));
                    }

                    stockItem.setCategoryID(cursor.getLong(14));
                    stockItemList.add(stockItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockItemList;
    }

    public StockItem getStockByID(Long id) {
        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_BARCODE + ", c." + AppConstant.CATEGORY_NAME +
                ", u." + AppConstant.UNIT_UNIT + ", s." + AppConstant.STOCK_INVENTORY_QTY + ", s." + AppConstant.STOCK_REORDER_QTY + ", s." + AppConstant.STOCK_PURCHASE_PRICE +
                ", s." + AppConstant.STOCK_WHOLESALE_PRICE + ", s." + AppConstant.STOCK_NORMAL_PRICE + ", s." + AppConstant.STOCK_RETAIL_PRICE + //", s." + AppConstant.STOCK_IMAGE +
                ", s." + AppConstant.STOCK_DESCRIPTION + ", s." + AppConstant.STOCK_NAME + ", u." + AppConstant.UNIT_ID + ", s." + AppConstant.STOCK_CATEGORY_ID + " from " + AppConstant.STOCK_TABLE_NAME + " s left outer join "
                + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + ", " + AppConstant.CATEGORY_TABLE_NAME
                + " c " + " where s."
                + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and s." + AppConstant.STOCK_ID + " = " + id;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setBarcode(cursor.getString(2));
                    stockItem.setCategoryName(cursor.getString(3));
                    stockItem.setUnitName(cursor.getString(4));
                    stockItem.setInventoryQty(cursor.getInt(5));
                    stockItem.setReorderLevel(cursor.getInt(6));
                    stockItem.setPurchasePrice(cursor.getDouble(7));
                    stockItem.setWholesalePrice(cursor.getDouble(8));
                    stockItem.setNormalPrice(cursor.getDouble(9));
                    stockItem.setRetailPrice(cursor.getDouble(10));
                    //stockItem.setImageList(cursor.getBlob(11));
                    stockItem.setDescription(cursor.getString(11));
                    stockItem.setName(cursor.getString(12));

                    if (cursor.getLong(13) != 0) {
                        stockItem.setUnitID(cursor.getLong(13));
                    }

                    stockItem.setCategoryID(cursor.getLong(14));

                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockItem;
    }

    public List<StockItem> getAllStocksWithNoOfSuppliers(int startLimit, int endLimit) {
        stockItemList = new ArrayList<>();
        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_BARCODE + ", c." + AppConstant.CATEGORY_NAME +
                ", count(distinct " + AppConstant.PURCHASE_SUPPLIER_ID + "), s." + AppConstant.STOCK_NAME + " from " + AppConstant.STOCK_TABLE_NAME + " s left outer join " + AppConstant.PURCHASE_TABLE_NAME + " p on " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID + ", " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, "
                + AppConstant.CATEGORY_TABLE_NAME + " c " + " where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and " +
                " s." + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " group by s." + AppConstant.STOCK_ID + " order by s." +
                AppConstant.STOCK_NAME + " asc limit " + startLimit + ", " + endLimit;
        Log.e("query", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setBarcode(cursor.getString(2));
                    stockItem.setCategoryName(cursor.getString(3));
                    stockItem.setInventoryQty(cursor.getInt(4));
                    stockItem.setName(cursor.getString(5));
                    stockItemList.add(stockItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockItemList;
    }

    public List<StockItem> getAllStocksWithNoOfSuppliersByStockID(Long stockID) {
        stockItemList = new ArrayList<>();
        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_BARCODE + ", c." + AppConstant.CATEGORY_NAME +
                ", count(distinct " + AppConstant.PURCHASE_SUPPLIER_ID + "), s." + AppConstant.STOCK_NAME + " from " + AppConstant.STOCK_TABLE_NAME + " s left outer join " + AppConstant.PURCHASE_TABLE_NAME + " p on " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID + ", " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, "
                + AppConstant.CATEGORY_TABLE_NAME + " c " + " where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and " +
                " s." + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and s." + AppConstant.STOCK_ID + " = " + stockID + " group by s." + AppConstant.STOCK_ID;
        Log.e("query", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setBarcode(cursor.getString(2));
                    stockItem.setCategoryName(cursor.getString(3));
                    stockItem.setInventoryQty(cursor.getInt(4));
                    stockItem.setName(cursor.getString(5));
                    stockItemList.add(stockItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockItemList;
    }

    public List<StockItem> getAllStocksWithNoOfSuppliersByCategoryID(int startLimit, int endLimit, Long categoryID) {
        stockItemList = new ArrayList<>();
        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_BARCODE + ", c." + AppConstant.CATEGORY_NAME +
                ", count(distinct " + AppConstant.PURCHASE_SUPPLIER_ID + "), s." + AppConstant.STOCK_NAME + " from " + AppConstant.STOCK_TABLE_NAME + " s left outer join " + AppConstant.PURCHASE_TABLE_NAME + " p on " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID + ", " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, "
                + AppConstant.CATEGORY_TABLE_NAME + " c " + " where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and " +
                " s." + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and s." + AppConstant.STOCK_CATEGORY_ID + " = " + categoryID + " group by s." + AppConstant.STOCK_ID + " order by s." +
                AppConstant.STOCK_NAME + " asc limit " + startLimit + ", " + endLimit;
        Log.e("query", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setBarcode(cursor.getString(2));
                    stockItem.setCategoryName(cursor.getString(3));
                    stockItem.setInventoryQty(cursor.getInt(4));
                    stockItem.setName(cursor.getString(5));
                    stockItemList.add(stockItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockItemList;
    }

    public List<StockItem> getAllStocksSearchByNameOrCode(int startLimit, int endLimit, String nameOrCode, int isDeleted) {
        stockItemList = new ArrayList<>();
        String query = "select distinct (s." + AppConstant.STOCK_ID + "), s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_BARCODE + ", c." + AppConstant.CATEGORY_NAME +
                ", u." + AppConstant.UNIT_UNIT + ", s." + AppConstant.STOCK_INVENTORY_QTY + ", s." + AppConstant.STOCK_REORDER_QTY + ", s." + AppConstant.STOCK_PURCHASE_PRICE +
                ", s." + AppConstant.STOCK_WHOLESALE_PRICE + ", s." + AppConstant.STOCK_NORMAL_PRICE + ", s." + AppConstant.STOCK_RETAIL_PRICE + //", s." + AppConstant.STOCK_IMAGE +
                ", s." + AppConstant.STOCK_DESCRIPTION + ", s." + AppConstant.STOCK_NAME + ", u." + AppConstant.UNIT_ID + ", s." + AppConstant.STOCK_CATEGORY_ID + " from " + AppConstant.STOCK_TABLE_NAME + " s left outer join "
                + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + ", " + AppConstant.CATEGORY_TABLE_NAME
                + " c " + " where s."
                + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and (s." + AppConstant.STOCK_NAME + " like ? ||'%' or s." + AppConstant.STOCK_CODE_NUM + " like ? || '%'or s." + AppConstant.STOCK_BARCODE + " like ?  || '%' ) and " + AppConstant.STOCK_IS_DELETED + " <> " + isDeleted +
                " order by case when s." + AppConstant.STOCK_CODE_NUM + " like ? then s." + AppConstant.STOCK_CODE_NUM + " end asc," +
                " case when  s." + AppConstant.STOCK_BARCODE + " LIKE ? then s." + AppConstant.STOCK_BARCODE + " end asc," +
                " case when s." + AppConstant.STOCK_NAME + " like ? ||'%' then s." + AppConstant.STOCK_NAME + " end asc" +
                " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{nameOrCode, nameOrCode, nameOrCode});
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setBarcode(cursor.getString(2));
                    stockItem.setCategoryName(cursor.getString(3));
                    stockItem.setUnitName(cursor.getString(4));
                    stockItem.setInventoryQty(cursor.getInt(5));
                    stockItem.setReorderLevel(cursor.getInt(6));
                    stockItem.setPurchasePrice(cursor.getDouble(7));
                    stockItem.setWholesalePrice(cursor.getDouble(8));
                    stockItem.setNormalPrice(cursor.getDouble(9));
                    stockItem.setRetailPrice(cursor.getDouble(10));
                    //stockItem.setImageList(cursor.getBlob(11));
                    stockItem.setDescription(cursor.getString(11));
                    stockItem.setName(cursor.getString(12));

                    if (cursor.getLong(13) != 0) {
                        stockItem.setUnitID(cursor.getLong(13));
                    }

                    stockItem.setCategoryID(cursor.getLong(14));
                    stockItemList.add(stockItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }

        return stockItemList;
    }

    public List<StockItem> getAllStocksSearchByNameOrCodeNotDelete(int startLimit, int endLimit, String nameOrCode) {
        stockItemList = new ArrayList<>();
        String query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_BARCODE + ", c." + AppConstant.CATEGORY_NAME +
                ", u." + AppConstant.UNIT_UNIT + ", s." + AppConstant.STOCK_INVENTORY_QTY + ", s." + AppConstant.STOCK_REORDER_QTY + ", s." + AppConstant.STOCK_PURCHASE_PRICE +
                ", s." + AppConstant.STOCK_WHOLESALE_PRICE + ", s." + AppConstant.STOCK_NORMAL_PRICE + ", s." + AppConstant.STOCK_RETAIL_PRICE + //", s." + AppConstant.STOCK_IMAGE +
                ", s." + AppConstant.STOCK_DESCRIPTION + ", s." + AppConstant.STOCK_NAME + ", u." + AppConstant.UNIT_ID + ", s." + AppConstant.STOCK_CATEGORY_ID + " from " +
                AppConstant.STOCK_TABLE_NAME + " s left outer join " + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + ", " +
                AppConstant.CATEGORY_TABLE_NAME + " c " + " where s." + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and (s." + AppConstant.STOCK_NAME +
                " like ? ||'%' or s." + AppConstant.STOCK_CODE_NUM + " like ? || '%') order by s." + AppConstant.STOCK_NAME + " asc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{nameOrCode, nameOrCode});
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setBarcode(cursor.getString(2));
                    stockItem.setCategoryName(cursor.getString(3));
                    stockItem.setUnitName(cursor.getString(4));
                    stockItem.setInventoryQty(cursor.getInt(5));
                    stockItem.setReorderLevel(cursor.getInt(6));
                    stockItem.setPurchasePrice(cursor.getDouble(7));
                    stockItem.setWholesalePrice(cursor.getDouble(8));
                    stockItem.setNormalPrice(cursor.getDouble(9));
                    stockItem.setRetailPrice(cursor.getDouble(10));
                    //stockItem.setImageList(cursor.getBlob(11));
                    stockItem.setDescription(cursor.getString(11));
                    stockItem.setName(cursor.getString(12));

                    if (cursor.getLong(13) != 0) {
                        stockItem.setUnitID(cursor.getLong(13));
                    }

                    stockItem.setCategoryID(cursor.getLong(14));
                    stockItemList.add(stockItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockItemList;
    }

    public List<StockItem> getStockListByCategory(int startLimit, int endLimit, Long categoryId) {
        stockItemList = new ArrayList<>();
        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_BARCODE + ", c." + AppConstant.CATEGORY_NAME +
                ", u." + AppConstant.UNIT_UNIT + ", s." + AppConstant.STOCK_INVENTORY_QTY + ", s." + AppConstant.STOCK_REORDER_QTY + ", s." + AppConstant.STOCK_PURCHASE_PRICE +
                ", s." + AppConstant.STOCK_WHOLESALE_PRICE + ", s." + AppConstant.STOCK_NORMAL_PRICE + ", s." + AppConstant.STOCK_RETAIL_PRICE + //", s." + AppConstant.STOCK_IMAGE +
                ", s." + AppConstant.STOCK_DESCRIPTION + ", s." + AppConstant.STOCK_NAME + ", u." + AppConstant.UNIT_ID + ", s." + AppConstant.STOCK_CATEGORY_ID + " from " + AppConstant.STOCK_TABLE_NAME + " s left outer join "
                + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + ", " + AppConstant.CATEGORY_TABLE_NAME
                + " c " + " where s."
                + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID +
                " and  " + AppConstant.STOCK_IS_DELETED + " <> 1 " +
                " and s." + AppConstant.STOCK_CATEGORY_ID + "= ?  order by s." +
                AppConstant.STOCK_NAME + " asc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{Long.toString(categoryId)});
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setBarcode(cursor.getString(2));
                    stockItem.setCategoryName(cursor.getString(3));
                    stockItem.setUnitName(cursor.getString(4));
                    stockItem.setInventoryQty(cursor.getInt(5));
                    stockItem.setReorderLevel(cursor.getInt(6));
                    stockItem.setPurchasePrice(cursor.getDouble(7));
                    stockItem.setWholesalePrice(cursor.getDouble(8));
                    stockItem.setNormalPrice(cursor.getDouble(9));
                    stockItem.setRetailPrice(cursor.getDouble(10));
                    //stockItem.setImageList(cursor.getBlob(11));
                    stockItem.setDescription(cursor.getString(11));
                    stockItem.setName(cursor.getString(12));
                    stockItem.setUnitID(cursor.getLong(13));
                    stockItem.setCategoryID(cursor.getLong(14));
                    stockItemList.add(stockItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockItemList;
    }

    public List<StockItem> getAllReorderStocks(int startLimit, int endLimit) {
        stockItemList = new ArrayList<>();
        query = "select s." + AppConstant.STOCK_ID + ", " +
                    "s." + AppConstant.STOCK_CODE_NUM + ", " +
                    "s." + AppConstant.STOCK_BARCODE + ", " +
                    "c." + AppConstant.CATEGORY_NAME + ", " +
                    "u." + AppConstant.UNIT_UNIT + ", " +
                    "s." + AppConstant.STOCK_INVENTORY_QTY + ", " +
                    "s." + AppConstant.STOCK_REORDER_QTY + ", " +
                    "s." + AppConstant.STOCK_PURCHASE_PRICE + ", " +
                    "s." + AppConstant.STOCK_WHOLESALE_PRICE + ", " +
                    "s." + AppConstant.STOCK_NORMAL_PRICE + ", " +
                    "s." + AppConstant.STOCK_RETAIL_PRICE + ", " +
                    //", s." + AppConstant.STOCK_IMAGE +
                    "s." + AppConstant.STOCK_DESCRIPTION + ", " +
                    "s." + AppConstant.STOCK_NAME + ", " +
                    "u." + AppConstant.UNIT_ID + ", " +
                    "s." + AppConstant.STOCK_CATEGORY_ID +
                " from " + AppConstant.STOCK_TABLE_NAME + " s " +
                "left outer join " + AppConstant.UNIT_TABLE_NAME + " u " +
                    "on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + ", " + AppConstant.CATEGORY_TABLE_NAME + " c " + " " +
                "where s." +
                    AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID +
                    " and s." + AppConstant.STOCK_REORDER_QTY + " >= s." + AppConstant.STOCK_INVENTORY_QTY +
                    " and " + AppConstant.STOCK_IS_DELETED + " <> 1 limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setBarcode(cursor.getString(2));
                    stockItem.setCategoryName(cursor.getString(3));
                    stockItem.setUnitName(cursor.getString(4));
                    stockItem.setInventoryQty(cursor.getInt(5));
                    stockItem.setReorderLevel(cursor.getInt(6));
                    stockItem.setPurchasePrice(cursor.getDouble(7));
                    stockItem.setWholesalePrice(cursor.getDouble(8));
                    stockItem.setNormalPrice(cursor.getDouble(9));
                    stockItem.setRetailPrice(cursor.getDouble(10));
                    //stockItem.setImageList(cursor.getBlob(11));
                    stockItem.setDescription(cursor.getString(11));
                    stockItem.setName(cursor.getString(12));
                    stockItem.setUnitID(cursor.getLong(13));
                    stockItem.setCategoryID(cursor.getLong(14));
                    stockItemList.add(stockItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockItemList;
    }

    public List<StockItem> getAllReorderStocks(int startLimit, int endLimit, Long categoryId) {
        stockItemList = new ArrayList<>();
        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_BARCODE + ", c." + AppConstant.CATEGORY_NAME +
                ", u." + AppConstant.UNIT_UNIT + ", s." + AppConstant.STOCK_INVENTORY_QTY + ", s." + AppConstant.STOCK_REORDER_QTY + ", s." + AppConstant.STOCK_PURCHASE_PRICE +
                ", s." + AppConstant.STOCK_WHOLESALE_PRICE + ", s." + AppConstant.STOCK_NORMAL_PRICE + ", s." + AppConstant.STOCK_RETAIL_PRICE + //", s." + AppConstant.STOCK_IMAGE +
                ", s." + AppConstant.STOCK_DESCRIPTION + ", s." + AppConstant.STOCK_NAME + ", u." + AppConstant.UNIT_ID + ", s." + AppConstant.STOCK_CATEGORY_ID + " from " + AppConstant.STOCK_TABLE_NAME + " s left outer join "
                + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + ", " + AppConstant.CATEGORY_TABLE_NAME
                + " c " + " where s."
                + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and s." + AppConstant.STOCK_REORDER_QTY + ">= s." + AppConstant.STOCK_INVENTORY_QTY
                + " and s." + AppConstant.STOCK_CATEGORY_ID + "=c." + AppConstant.CATEGORY_ID + " and s." + AppConstant.STOCK_CATEGORY_ID + " = ? and " + AppConstant.STOCK_IS_DELETED
                + " <> 1 limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{Long.toString(categoryId)});
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setBarcode(cursor.getString(2));
                    stockItem.setCategoryName(cursor.getString(3));
                    stockItem.setUnitName(cursor.getString(4));
                    stockItem.setInventoryQty(cursor.getInt(5));
                    stockItem.setReorderLevel(cursor.getInt(6));
                    stockItem.setPurchasePrice(cursor.getDouble(7));
                    stockItem.setWholesalePrice(cursor.getDouble(8));
                    stockItem.setNormalPrice(cursor.getDouble(9));
                    stockItem.setRetailPrice(cursor.getDouble(10));
                    //stockItem.setImageList(cursor.getBlob(11));
                    stockItem.setDescription(cursor.getString(11));
                    stockItem.setName(cursor.getString(12));
                    stockItem.setUnitID(cursor.getLong(13));
                    stockItem.setCategoryID(cursor.getLong(14));
                    stockItemList.add(stockItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockItemList;
    }

    public int allStockCounts() {
        databaseReadTransaction(flag);
        try {
            query = "select count(" + AppConstant.STOCK_ID + ") from " + AppConstant.STOCK_TABLE_NAME;
            cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            count = cursor.getInt(0);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return count;
    }

    public int allStockReorderCounts() {
        databaseReadTransaction(flag);
        try {
            query = "select count(" + AppConstant.STOCK_ID + ") from " + AppConstant.STOCK_TABLE_NAME + " where " + AppConstant.STOCK_REORDER_QTY + " >= " + AppConstant.STOCK_INVENTORY_QTY
                    + " and " + AppConstant.STOCK_IS_DELETED + " <> 1";
            cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            count = cursor.getInt(0);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return count;
    }

    public int allStockCountsByCategory(Long categoryID) {
        databaseReadTransaction(flag);
        try {
            query = "select count(" + AppConstant.STOCK_ID + ") from " + AppConstant.STOCK_TABLE_NAME + " where " + AppConstant.STOCK_CATEGORY_ID + " = ?";
            cursor = database.rawQuery(query, new String[]{categoryID.toString()});
            cursor.moveToFirst();
            count = cursor.getInt(0);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return count;
    }

    public Stock findStockByStockCode(String codeNo) {
        String query = "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_CODE_NUM + ", " + AppConstant.STOCK_BARCODE + ", " +
                AppConstant.STOCK_INVENTORY_QTY + ", " + AppConstant.STOCK_REORDER_QTY + ", " + AppConstant.STOCK_PURCHASE_PRICE +
                ", " + AppConstant.STOCK_WHOLESALE_PRICE + ", " + AppConstant.STOCK_NORMAL_PRICE + ", " + AppConstant.STOCK_RETAIL_PRICE
                //+ ", " + AppConstant.STOCK_IMAGE
                + ", " + AppConstant.STOCK_DESCRIPTION + ", " + AppConstant.STOCK_NAME + ", " + AppConstant.STOCK_CATEGORY_ID
                + " from " + AppConstant.STOCK_TABLE_NAME + " where " + AppConstant.STOCK_CODE_NUM + "=?";
        databaseReadTransaction(flag);
        stock = new Stock();
        try {
            cursor = database.rawQuery(query, new String[]{codeNo});
            if (cursor.moveToFirst()) {
                stock.setId(cursor.getLong(0));
                stock.setStockCode(cursor.getString(1));
                stock.setBarCode(cursor.getString(2));
                stock.setInventoryQty(cursor.getInt(3));
                stock.setReorderLevel(cursor.getInt(4));
                stock.setPurchasePrice(cursor.getDouble(5));
                stock.setWholeSalePrice(cursor.getDouble(6));
                stock.setNormalSalePrice(cursor.getDouble(7));
                stock.setRetailPrice(cursor.getDouble(8));
                //stock.setImageList(cursor.getBlob(9));
                stock.setDescription(cursor.getString(9));
                stock.setItemName(cursor.getString(10));
                stock.setCategoryID(cursor.getLong(11));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stock;
    }

    public Stock findStockByBarCode(String codeNo) {
        query = "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_CODE_NUM + ", " + AppConstant.STOCK_BARCODE + ", " +
                AppConstant.STOCK_INVENTORY_QTY + ", " + AppConstant.STOCK_REORDER_QTY + ", " + AppConstant.STOCK_PURCHASE_PRICE +
                ", " + AppConstant.STOCK_WHOLESALE_PRICE + ", " + AppConstant.STOCK_NORMAL_PRICE + ", " + AppConstant.STOCK_RETAIL_PRICE + //", " + AppConstant.STOCK_IMAGE +
                ", " + AppConstant.STOCK_DESCRIPTION + ", " + AppConstant.STOCK_NAME + ", " + AppConstant.STOCK_CATEGORY_ID + " from " + AppConstant.STOCK_TABLE_NAME + " where " +
                AppConstant.STOCK_BARCODE + "=?";
        databaseReadTransaction(flag);
        stock = new Stock();
        try {
            cursor = database.rawQuery(query, new String[]{codeNo});
            if (cursor.moveToFirst()) {
                stock.setId(cursor.getLong(0));
                stock.setStockCode(cursor.getString(1));
                stock.setBarCode(cursor.getString(2));
                stock.setInventoryQty(cursor.getInt(3));
                stock.setReorderLevel(cursor.getInt(4));
                stock.setPurchasePrice(cursor.getDouble(5));
                stock.setWholeSalePrice(cursor.getDouble(6));
                stock.setNormalSalePrice(cursor.getDouble(7));
                stock.setRetailPrice(cursor.getDouble(8));
                //stock.setImageList(cursor.getBlob(9));
                stock.setDescription(cursor.getString(9));
                stock.setItemName(cursor.getString(10));
                stock.setCategoryID(cursor.getLong(11));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stock;
    }

    public Stock findStockByName(String name) {
        String query = "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_CODE_NUM + ", " + AppConstant.STOCK_BARCODE + ", " +
                AppConstant.STOCK_INVENTORY_QTY + ", " + AppConstant.STOCK_REORDER_QTY + ", " + AppConstant.STOCK_PURCHASE_PRICE +
                ", " + AppConstant.STOCK_WHOLESALE_PRICE + ", " + AppConstant.STOCK_NORMAL_PRICE + ", " + AppConstant.STOCK_RETAIL_PRICE +// ", " + AppConstant.STOCK_IMAGE +
                ", " + AppConstant.STOCK_DESCRIPTION + ", " + AppConstant.STOCK_NAME + ", " + AppConstant.STOCK_CATEGORY_ID + " from " + AppConstant.STOCK_TABLE_NAME + " where " +
                AppConstant.STOCK_NAME + "=?";
        databaseReadTransaction(flag);
        stock = new Stock();
        try {
            cursor = database.rawQuery(query, new String[]{name});
            if (cursor.moveToFirst()) {
                stock.setId(cursor.getLong(0));
                stock.setStockCode(cursor.getString(1));
                stock.setBarCode(cursor.getString(2));
                stock.setInventoryQty(cursor.getInt(3));
                stock.setReorderLevel(cursor.getInt(4));
                stock.setPurchasePrice(cursor.getDouble(5));
                stock.setWholeSalePrice(cursor.getDouble(6));
                stock.setNormalSalePrice(cursor.getDouble(7));
                stock.setRetailPrice(cursor.getDouble(8));
                //stock.setImageList(cursor.getBlob(9));
                stock.setDescription(cursor.getString(9));
                stock.setItemName(cursor.getString(10));
                stock.setCategoryID(cursor.getLong(11));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stock;
    }

    public Stock findStockByID(Long id) {
        query = "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_CODE_NUM + ", " + AppConstant.STOCK_BARCODE + ", " +
                AppConstant.STOCK_INVENTORY_QTY + ", " + AppConstant.STOCK_REORDER_QTY + ", " + AppConstant.STOCK_PURCHASE_PRICE +
                ", " + AppConstant.STOCK_WHOLESALE_PRICE + ", " + AppConstant.STOCK_NORMAL_PRICE + ", " + AppConstant.STOCK_RETAIL_PRICE + //", " + AppConstant.STOCK_IMAGE +
                ", " + AppConstant.STOCK_DESCRIPTION + ", " + AppConstant.STOCK_NAME + ", " + AppConstant.STOCK_CATEGORY_ID + " from " + AppConstant.STOCK_TABLE_NAME + " where " +
                AppConstant.STOCK_ID + "=" + id;
        databaseReadTransaction(flag);
        stock = new Stock();
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                stock.setId(cursor.getLong(0));
                stock.setStockCode(cursor.getString(1));
                stock.setBarCode(cursor.getString(2));
                stock.setInventoryQty(cursor.getInt(3));
                stock.setReorderLevel(cursor.getInt(4));
                stock.setPurchasePrice(cursor.getDouble(5));
                stock.setWholeSalePrice(cursor.getDouble(6));
                stock.setNormalSalePrice(cursor.getDouble(7));
                stock.setRetailPrice(cursor.getDouble(8));
                //stock.setImageList(cursor.getBlob(9));
                stock.setDescription(cursor.getString(9));
                stock.setItemName(cursor.getString(10));
                stock.setCategoryID(cursor.getLong(11));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stock;
    }

    public boolean updateStock(String codeNo, String barcode, String name, Long categoryID, Long unitID, int inventoryQty, int reorderLevel,
                               Double purchasePrice, Double wholesalePrice, Double normalPrice, Double retailPrice, List<Long> deletedImageIdList, List<StockImage> insertImgList,
                               String description, Long id) {
        int invQty = 0;

        query = "update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_CODE_NUM + " =?, " + AppConstant.STOCK_BARCODE + " =?, " +
                AppConstant.STOCK_NAME + " =?, " + AppConstant.STOCK_CATEGORY_ID + " = " + categoryID + ", " + AppConstant.STOCK_UNIT_ID + "=" + unitID + ", " + AppConstant.STOCK_INVENTORY_QTY
                + "=" + inventoryQty + ", " + AppConstant.STOCK_REORDER_QTY + "=" + reorderLevel + ",  " + AppConstant.STOCK_PURCHASE_PRICE + "=" + purchasePrice + ", " + AppConstant.STOCK_WHOLESALE_PRICE
                + "=" + wholesalePrice + ", " + AppConstant.STOCK_NORMAL_PRICE + "=" + normalPrice + ", " + AppConstant.STOCK_RETAIL_PRICE + "=" + retailPrice + ", " +
                //AppConstant.STOCK_IMAGE + "=" + image + ", " +
                AppConstant.STOCK_DESCRIPTION + "=? where " + AppConstant.STOCK_ID + "=" + id;
        this.id = id;
        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery("select " + AppConstant.STOCK_INVENTORY_QTY + " from " + AppConstant.STOCK_TABLE_NAME + " where " + AppConstant.STOCK_ID + " = " + id, null);
            if (cursor.moveToFirst()) {
                invQty = cursor.getInt(0);
            }

            Cursor priceCursor = database.rawQuery("select " + AppConstant.STOCK_PURCHASE_PRICE + " from " + AppConstant.STOCK_TABLE_NAME + " where " + AppConstant.STOCK_ID + " = " + id,
                    null);
            Double oldPurPrice = 0.0;
            if (priceCursor.moveToFirst()) {
                oldPurPrice = priceCursor.getDouble(0);
            }
            priceCursor.close();

            //            Log.e("ads a " + oldPurPrice.toString(), purchasePrice.toString());
            database.execSQL(query, new String[]{codeNo, barcode, name, description});

            //            Log.e("trere", (oldPurPrice == purchasePrice) + " dkfj");
            if (oldPurPrice.doubleValue() != purchasePrice.doubleValue()) {
                database.execSQL("update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_OPENING_PRICE + " = " + purchasePrice + " where " + AppConstant.STOCK_ID + " = " +
                        id);

                //                Log.e("updated", "it");
            }
            invQty -= inventoryQty;

            Log.e("invqty", insertImgList.isEmpty() + "");
            if (invQty != 0) {
                database.execSQL(" insert into " + AppConstant.ADJUSTMENT_TABLE_NAME + "(" + AppConstant.ADJUSTMENT_STOCK_ID + ", " + AppConstant.ADJUSTMENT_QTY + ", " + AppConstant.ADJUSTMENT_DAY + ", " +
                        AppConstant.ADJUSTMENT_MONTH + ", " + AppConstant.ADJUSTMENT_YEAR + ", " + AppConstant.ADJUSTMENT_DATE + ", " + AppConstant.ADJUSTMENT_TIME + ", " + AppConstant.ADJUSTMENT_PRICE +
                        ") values (" + id + ", " + invQty + " , strftime('%d', 'now'), strftime('%m', 'now'), strftime('%Y', 'now'), strftime('%Y%m%d', 'now'), strftime('%s', 'now', 'localtime') " + ", "
                        + purchasePrice + " );");


            }
            if (!insertImgList.isEmpty()) {
                addImage(insertImgList);
            }
            if (!deletedImageIdList.isEmpty()) {
                deleteImage(deletedImageIdList);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            cursor.close();
        }
        return
                flag.isInserted();

    }

    public boolean updateStockRetailPrice(Double retailPrice, Long id) {
        query = "update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_RETAIL_PRICE + "=" + retailPrice + " where " + AppConstant.STOCK_ID + "=" + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{});

            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();

    }

    public boolean updateStockPurchasePrice(Double pruchasePrice, Long id) {
        query = "update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_RETAIL_PRICE + "=" + pruchasePrice + ", " + "where " + AppConstant.STOCK_ID + "=" + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{});

            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();

    }

    private void deleteImage(List<Long> deletedImageIdList) {
        for (Long deletedID : deletedImageIdList) {
            database.delete(AppConstant.STOCK_IMAGE_TABLE_NAME, AppConstant.STOCK_IMAGE_ID + " = ? ", new String[]{deletedID.toString()});
        }
    }

    public List<StockAutoComplete> findStockByBarcodeForAutoComplete(String barcode) {
        stockAutoCompleteList = new ArrayList<>();
        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_BARCODE + ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_NAME + ", s." + AppConstant.STOCK_WHOLESALE_PRICE +
                ", s." + AppConstant.STOCK_RETAIL_PRICE + ", s." + AppConstant.STOCK_NORMAL_PRICE + ", s." + AppConstant.STOCK_UNIT_ID + ", u." + AppConstant.UNIT_UNIT + " from " + AppConstant.STOCK_TABLE_NAME +
                " s left outer join " + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + " where s." + AppConstant.STOCK_BARCODE + " like ? || '%'";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{barcode});
            if (cursor.moveToFirst()) {
                do {
                    stockAutoComplete = new StockAutoComplete();
                    stockAutoComplete.setId(cursor.getLong(0));
                    stockAutoComplete.setBarcode(cursor.getString(1));
                    stockAutoComplete.setCodeNo(cursor.getString(2));
                    stockAutoComplete.setItemName(cursor.getString(3));
                    stockAutoComplete.setWholesalePrice(cursor.getDouble(4));
                    stockAutoComplete.setRetailPrice(cursor.getDouble(5));
                    stockAutoComplete.setNormalPrice(cursor.getDouble(6));
                    stockAutoComplete.setUnitID(cursor.getLong(7));
                    stockAutoComplete.setUnitName(cursor.getString(8));
                    stockAutoCompleteList.add(stockAutoComplete);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockAutoCompleteList;
    }

    public List<StockAutoComplete> findStockByCodeNoForAutoComplete(String codeNo) {
        stockAutoCompleteList = new ArrayList<>();
        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_BARCODE + ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_NAME + ", s." + AppConstant.STOCK_WHOLESALE_PRICE +
                ", s." + AppConstant.STOCK_RETAIL_PRICE + ", s." + AppConstant.STOCK_NORMAL_PRICE + ", s." + AppConstant.STOCK_UNIT_ID + ", u." + AppConstant.UNIT_UNIT + " ,s." + AppConstant.STOCK_PURCHASE_PRICE + " from " + AppConstant.STOCK_TABLE_NAME +
                " s left outer join " + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + " where s." + AppConstant.STOCK_CODE_NUM + " like ? || '%' and " + AppConstant.STOCK_IS_DELETED + " <> 1 limit 0, 10";
        databaseReadTransaction(flag);
        try {
            //            Log.e("auto cio","iaonr");
            cursor = database.rawQuery(query, new String[]{codeNo});
            /*cursor = database.query(AppConstant.STOCK_TABLE_NAME + " as s " + "left outer join " + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID ,
                    new String[]{"s." + AppConstant.STOCK_ID  , "s." + AppConstant.STOCK_BARCODE , "s." + AppConstant.STOCK_CODE_NUM, "s." + AppConstant.STOCK_NAME, "s." + AppConstant.STOCK_WHOLESALE_PRICE,
                    "s." + AppConstant.STOCK_RETAIL_PRICE, "s." + AppConstant.STOCK_NORMAL_PRICE, "s." + AppConstant.STOCK_UNIT_ID, "u." + AppConstant.UNIT_UNIT },
                     " s." + AppConstant.STOCK_CODE_NUM + " like '?'",
                    new String[]{codeNo + "%"}, null, null, null);*/
            if (cursor.moveToFirst()) {
                do {
                    stockAutoComplete = new StockAutoComplete();
                    stockAutoComplete.setId(cursor.getLong(0));
                    stockAutoComplete.setBarcode(cursor.getString(1));
                    stockAutoComplete.setCodeNo(cursor.getString(2));
                    stockAutoComplete.setItemName(cursor.getString(3));
                    stockAutoComplete.setWholesalePrice(cursor.getDouble(4));
                    stockAutoComplete.setRetailPrice(cursor.getDouble(5));
                    stockAutoComplete.setNormalPrice(cursor.getDouble(6));
                    stockAutoComplete.setUnitID(cursor.getLong(7));
                    stockAutoComplete.setUnitName(cursor.getString(8));
                    stockAutoComplete.setPurchasePrice(cursor.getDouble(9));
                    stockAutoCompleteList.add(stockAutoComplete);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        Log.e(stockAutoCompleteList.size() + " ifao", " iodahf");
        return stockAutoCompleteList;
    }

    public List<StockImage> getImagesByStockID(Long stockID) {
        List<StockImage> stockImageList = new ArrayList<>();
        StockImage       stockImage;
        query = "select " + AppConstant.STOCK_IMAGE_ID + ", " + AppConstant.STOCK_IMAGE_IMG + ", " + AppConstant.STOCK_IMAGE_STOCK_ID + " from " + AppConstant.STOCK_IMAGE_TABLE_NAME + " where " +
                AppConstant.STOCK_IMAGE_STOCK_ID + " = " + stockID;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    stockImage = new StockImage();
                    stockImage.setId(cursor.getLong(0));
                    stockImage.setImage(cursor.getBlob(1));
                    stockImage.setStockID(cursor.getLong(2));

                    Log.w("hello stock id", cursor.getLong(2) + " d");

                    stockImageList.add(stockImage);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockImageList;
    }

    public List<StockAutoComplete> findStockByNameForAutoComplete(String name) {
        stockAutoCompleteList = new ArrayList<>();
        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_BARCODE + ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_NAME + ", s." + AppConstant.STOCK_WHOLESALE_PRICE +
                ", s." + AppConstant.STOCK_RETAIL_PRICE + ", s." + AppConstant.STOCK_NORMAL_PRICE + ", s." + AppConstant.STOCK_UNIT_ID + ", u." + AppConstant.UNIT_UNIT + ", " + AppConstant.STOCK_PURCHASE_PRICE + " from " + AppConstant.STOCK_TABLE_NAME +
                " s left outer join " + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + " where s." + AppConstant.STOCK_NAME + " like ? || '%' and " + AppConstant.STOCK_IS_DELETED + " <> 1 limit 0,10";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{name});
            if (cursor.moveToFirst()) {
                do {
                    stockAutoComplete = new StockAutoComplete();
                    stockAutoComplete.setId(cursor.getLong(0));
                    stockAutoComplete.setBarcode(cursor.getString(1));
                    stockAutoComplete.setCodeNo(cursor.getString(2));
                    stockAutoComplete.setItemName(cursor.getString(3));
                    stockAutoComplete.setWholesalePrice(cursor.getDouble(4));
                    stockAutoComplete.setRetailPrice(cursor.getDouble(5));
                    stockAutoComplete.setNormalPrice(cursor.getDouble(6));
                    stockAutoComplete.setUnitID(cursor.getLong(7));
                    stockAutoComplete.setUnitName(cursor.getString(8));
                    stockAutoComplete.setPurchasePrice(cursor.getDouble(9));
                    stockAutoCompleteList.add(stockAutoComplete);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return stockAutoCompleteList;
    }

    public boolean putOrderCancelIncreaseStockQty(Long stockID, int saleDetailQty) {
        query = " update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_INVENTORY_QTY + " = (select " + AppConstant.STOCK_INVENTORY_QTY + " from " + AppConstant.STOCK_TABLE_NAME +
                " where " + AppConstant.STOCK_ID + " = " + stockID + ") + " + saleDetailQty + " where " + AppConstant.STOCK_ID + " = " + stockID + ";";
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query);
        return true;
    }

    public boolean putBackOrderNormalDecreaseStockQty(Long stockID, int saleDetailQty) {
        query = " update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_INVENTORY_QTY + " = (select " + AppConstant.STOCK_INVENTORY_QTY + " from " + AppConstant.STOCK_TABLE_NAME +
                " where " + AppConstant.STOCK_ID + " = " + stockID + ") - " + saleDetailQty + " where " + AppConstant.STOCK_ID + " = " + stockID + ";";
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query);
        return true;
    }


    public int inProcessQty(Long id) {
        int qty = 0;
        query = "select ifnull(sum(qty),0) as inProcessQty from Sales s, SalesDetail sd, Delivery d where d.salesID = s.id and sd.salesID = s.id and status = 0 and stockID = " + id;
        databaseReadTransaction(flag);
        stock = new Stock();
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                qty = cursor.getInt(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return qty;
    }

    public List<StockItem> getDamagesForSearch(int startLimit, int endLimit, String searchStr) {
        stockItemList = new ArrayList<>();
        //        Log.e(startLimit + "o", endLimit + "f");
        query = "select distinct s." + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_CODE_NUM + ", " + AppConstant.STOCK_NAME + " from " + AppConstant.DAMAGE_TABLE_NAME + " d, " + AppConstant.STOCK_TABLE_NAME +
                " s where " + AppConstant.DAMAGE_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " +
                "(s." + AppConstant.STOCK_NAME + " like ? || '%' or s." + AppConstant.STOCK_CODE_NUM + " like ? || '%' ) " + " order by d." + AppConstant.DAMAGE_TIME + " desc limit " +
                startLimit + ", " + endLimit + ";";
        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setName(cursor.getString(2));
                    stockItemList.add(stockItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        //Log.e("list", stockItemList.size()  + " id");
        return stockItemList;
    }

    public List<StockItem> getLostForSearch(int startLimit, int endLimit, String searchStr) {
        stockItemList = new ArrayList<>();
        Log.e(startLimit + "o", endLimit + "f");
        query = "select distinct s." + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_CODE_NUM + ", " + AppConstant.STOCK_NAME + " from " + AppConstant.LOST_TABLE_NAME + " d, " + AppConstant.STOCK_TABLE_NAME +
                " s where " + AppConstant.LOST_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " +
                "(s." + AppConstant.STOCK_NAME + " like ? || '%' or s." + AppConstant.STOCK_CODE_NUM + " like ? || '%' ) " + " order by d." + AppConstant.LOST_TIME + " desc limit " +
                startLimit + ", " + endLimit + ";";
        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    stockItem = new StockItem();
                    stockItem.setId(cursor.getLong(0));
                    stockItem.setCodeNo(cursor.getString(1));
                    stockItem.setName(cursor.getString(2));
                    stockItemList.add(stockItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        //Log.e("list", stockItemList.size()  + " id");
        return stockItemList;
    }

}
