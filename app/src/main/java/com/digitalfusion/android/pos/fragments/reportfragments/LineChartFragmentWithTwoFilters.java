package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterSaleStaffsFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForTwoColumnsReports;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.views.Spinner;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class LineChartFragmentWithTwoFilters extends Fragment implements Serializable {

    private ArrayList<Entry> e1;
    private ArrayList<String> xVals;
    private List<ReportItem> reportItemList;
    private List<String> filterList;
    private List<User> staffList;

    private Context context;
    private ReportManager reportManager;
    private SalesManager salesManager;
    private RVAdapterForTwoColumnsReports rvAdapterForReport;
    private RVAdapterForFilter rvAdapterForDateFilter;
    private RVAdapterSaleStaffsFilter staffFilterAdapter;

    private RecyclerView recyclerView;
    private View mainLayoutView;
    private MaterialDialog dateFilterDialog;
    private MaterialDialog staffFilterDialog;
    private TextView dateFilterTextView;
    private LineChart lineChart;
    private TextView staffFilterTextView;

    private String reportType;
    private String startDate;
    private String endDate;
    private String thisYear;
    private String lastYear;
    private String last12Month;
    private User selectedUser;

    private boolean isTwelve;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_line_chart_with_two_filters, container, false);
        context = getContext();
        loadIngUI();
        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            reportType = getArguments().getString("reportType");
        }
        String title = ThemeUtil.getString(context, R.attr.monthly_sales_by_sale_staff);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);

        initializeVariables();

        configRecyclerView();
        rvAdapterForReport.sortMonths(false);
        new LoadProgressDialog().execute("");

        configFilter();
        buildDateFilterDialog();
        buildStaffFilterDialog();
        staffFilterTextView.setText(selectedUser.getUserName());
        dateFilterTextView.setText(thisYear);
        clickListeners();
    }

    private void configFilter() {
        thisYear = ThemeUtil.getString(context, R.attr.this_year);
        lastYear = ThemeUtil.getString(context, R.attr.last_year);
        last12Month = ThemeUtil.getString(context, R.attr.last_12_month);

        filterList = new ArrayList<>();
        filterList.add(thisYear);
        filterList.add(lastYear);
        filterList.add(last12Month);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
        staffFilterAdapter = new RVAdapterSaleStaffsFilter(staffList);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    private void clickListeners() {
        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        staffFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                staffFilterDialog.show();
            }
        });
        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                dateFilterDialog.dismiss();

                if (filterList.get(position).equalsIgnoreCase(thisYear)) {
                    rvAdapterForReport.sortMonths(false);
                    isTwelve = false;

                    startDate = DateUtility.getThisYearStartDate();
                    endDate = DateUtility.getThisYearEndDate();

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {
                    rvAdapterForReport.sortMonths(false);
                    isTwelve = false;

                    startDate = DateUtility.getLastYearStartDate();
                    endDate = DateUtility.getLastYearEndDate();

                } else if (filterList.get(position).equalsIgnoreCase(last12Month)) {
                    rvAdapterForReport.sortMonths(true);
                    isTwelve = true;

                    startDate = DateUtility.getLast12MonthStartDate();
                    endDate = DateUtility.getLast12MotnEndDate();

                }

                new LoadProgressDialog().execute("");

                dateFilterTextView.setText((filterList.get(position)));
            }
        });

        staffFilterAdapter.setmItemClickListener(new RVAdapterSaleStaffsFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                staffFilterDialog.dismiss();
                selectedUser = staffList.get(position);
                new LoadProgressDialog().execute("");
                staffFilterTextView.setText(staffList.get(position).getUserName());
            }
        });
    }

    private void buildDateFilterDialog() {
        String filter = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);
        dateFilterDialog = new MaterialDialog.Builder(context)
                .title(filter)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))
                .build();
    }

    private void buildStaffFilterDialog() {
        String filter = ThemeUtil.getString(context, R.attr.sale_staff);
        staffFilterDialog = new MaterialDialog.Builder(context)
                .title(filter)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(staffFilterAdapter, new LinearLayoutManager(context))
                .build();
    }

    private void loadIngUI() {
        dateFilterTextView  = (TextView    ) mainLayoutView.findViewById(R.id.date_filter             );
        staffFilterTextView = (TextView    ) mainLayoutView.findViewById(R.id.staff_filter            );
        recyclerView        = (RecyclerView) mainLayoutView.findViewById(R.id.line_chart_recycler_view);
        lineChart           = (LineChart   ) mainLayoutView.findViewById(R.id.line_chart              );
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);
        salesManager = new SalesManager(context);
        isTwelve       = false;
        startDate      = Calendar.getInstance().get(Calendar.YEAR) + "0101";
        endDate        = Calendar.getInstance().get(Calendar.YEAR) + "1231";
        staffList      = ApiDAO.getApiDAOInstance(context).getAllUserRoles();
        selectedUser   = staffList.get(0);
    }

    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        rvAdapterForReport = new RVAdapterForTwoColumnsReports(reportItemList, RVAdapterForTwoColumnsReports.Type.MONTH_AMOUNT, context);
        rvAdapterForReport.sortMonths(false);
        recyclerView.setAdapter(rvAdapterForReport);
    }

    public void setLineChartConfiguration() {
        lineChart.setDrawGridBackground(false);
        lineChart.animateX(1400, Easing.EasingOption.EaseInOutQuart);
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelRotationAngle(270);
        xAxis.setYOffset(10f);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);
        xAxis.setSpaceBetweenLabels(1);

        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);

        // change the position of the y-labels

        YAxis leftAxis = lineChart.getAxisLeft();
        //leftAxis.setValueFormatter(new MyYAxisValueFormatter());
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        //leftAxis.setLabelCount(1, false);
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)
        lineChart.getAxisRight().setEnabled(false);
        //leftAxis.setSpaceTop(15f);

        //leftAxis.setLabelCount(5, true);        // no description text
        lineChart.setDescription("");
        String need = ThemeUtil.getString(context, R.attr.u_need_to_provide_data_for_chart);
        lineChart.setNoDataTextDescription(need);

        // enable touch gestures
        lineChart.setTouchEnabled(true);

        // set the marker to the chart
        // enable scaling and dragging
        lineChart.setDragEnabled(false);
        lineChart.setScaleEnabled(false);
        lineChart.setDrawBorders(false);
        lineChart.getLegend().setEnabled(false);

        if (POSUtil.isLabelWhite(context)) {
            lineChart.getAxisLeft().setTextColor(Color.WHITE);
            lineChart.getXAxis().setTextColor(Color.WHITE);
        }

        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(false);
        lineChart.setDrawGridBackground(false);

        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setLabelCount(5, false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)

        // set data
        // do not forget to refresh the chart
        // holder.chart.invalidate();
        //lineChart.setDescriptionPosition(Float.parseFloat(XAxis.XAxisPosition.TOP.toString()),Float.parseFloat(YAxis.AxisDependency.RIGHT.toString()));

        setLineDataToChart();

        lineChart.invalidate();
    }

    private void setLineDataToChart() {

        initializingDataForLineChart();
        LineDataSet d1 = new LineDataSet(e1, "");
        d1.setLineWidth(2.5f);
        d1.setCircleRadius(4.5f);
        d1.setHighLightColor(Color.rgb(244, 117, 117));
        d1.setDrawValues(false);
        d1.setColor(Color.parseColor("#4dd0e1"));
        d1.setCircleColor(Color.parseColor("#4dd0e1"));
        d1.setDrawFilled(true);
        // fill drawable only supported on api level 18 and above
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.fade_blue);
        d1.setFillDrawable(drawable);

        ArrayList<ILineDataSet> sets = new ArrayList<ILineDataSet>();
        sets.add(d1);
        LineData cd = new LineData(xVals, sets);
        lineChart.setData(cd);
    }

    public void initializingDataForLineChart() {
        e1    = new ArrayList<>();
        xVals = new ArrayList<>();

        if (isTwelve) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -11);
            int cal = calendar.get(Calendar.MONTH);
            for (int i = 0; i < 12; i++) {
                xVals.add(AppConstant.MONTHS[cal]);
                cal += 1;
                if (cal > 11) {
                    cal = 0;
                }
            }
        } else {
            xVals.addAll(Arrays.asList(AppConstant.MONTHS).subList(0, 12));
        }

        for (int i = 0; i < 12; i++) {
            e1.add(new Entry((float)getTotalAmount(i), i));
        }
    }

    private double getTotalAmount(int position) {
        for (ReportItem reportItem : reportItemList) {
            if (AppConstant.MONTHS[reportItem.getMonth() - 1].equals(xVals.get(position))) {
                return reportItem.getTotalAmt();
            }
        }
        return 0;
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private Spinner spinner = new Spinner(context);

        @Override
        protected String doInBackground(String... params) {
            reportItemList = reportManager.monthlySalesBySaleStaff(startDate, endDate, selectedUser.getId());

            return params[0];
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.show();
        }

        @Override
        protected void onPostExecute(String a) {
            setLineChartConfiguration();
            rvAdapterForReport.setReportItemList(reportItemList);
            rvAdapterForReport.notifyDataSetChanged();
            recyclerView.invalidate();

            spinner.dismiss();
        }
    }
}
