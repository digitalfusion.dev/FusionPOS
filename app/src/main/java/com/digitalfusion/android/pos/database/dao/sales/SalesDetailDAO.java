package com.digitalfusion.android.pos.database.dao.sales;

import android.content.Context;
import android.util.Log;

import com.digitalfusion.android.pos.database.dao.ParentDAO;
import com.digitalfusion.android.pos.database.dao.IdGeneratorDAO;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.database.model.SalesStock;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/29/16.
 */
public class SalesDetailDAO extends ParentDAO {
    private static ParentDAO salesDetailDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private List<SalesAndPurchaseItem> salesAndPurchaseItemList;
    private SalesAndPurchaseItem salesAndPurchaseItem;
    private List<SalesStock> salesStockList;
    private SalesStock salesStock;

    private SalesDetailDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
    }

    public static SalesDetailDAO getSalesDetailDaoInstance(Context context) {
        if (salesDetailDaoInstance == null) {
            salesDetailDaoInstance = new SalesDetailDAO(context);
        }
        return (SalesDetailDAO) salesDetailDaoInstance;
    }

    public Long addNewSalesDetail(Long salesID, Long stockID, int qty, double price, String discount, Long taxID, double total, double taxAmt, double discountAmt, double taxRate, String taxType) {
        genID = idGeneratorDAO.getLastIdValue("SalesDetail");
        genID++;
        Log.e(salesID + " " + stockID + " ", taxID + " ion");
        query = "insert into " + AppConstant.SALES_DETAIL_TABLE_NAME + "(" + AppConstant.SALES_DETAIL_ID + ", " + AppConstant.SALES_DETAIL_SALES_ID +
                ", " + AppConstant.SALES_DETAIL_STOCK_ID + ", " + AppConstant.SALES_DETAIL_QTY + ", " + AppConstant.SALES_DETAIL_PRICE + ", "
                + AppConstant.SALES_DETAIL_DISCOUNT + ", " + AppConstant.SALES_DETAIL_TAX_ID + ", " + AppConstant.SALES_DETAIL_TOTAL
                + ", " + AppConstant.SALES_DETAIL_TAX_AMOUNT + ", " + AppConstant.SALES_DETAIL_DISCOUNT_AMT + "," + AppConstant.CREATED_DATE
                + ", " + AppConstant.SALES_DETAIL_TAX_RATE + ", " + AppConstant.SALES_DETAIL_TAX_TYPE + ") values(" + genID + ", " + salesID + ", " + stockID +
                "," + qty + ", " + price + ", ?, " + taxID + ", " + total + ", " + taxAmt + ", " + discountAmt + ",?, " + taxRate + ", ?)";
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query, new String[]{discount, DateUtility.getTodayDate(), taxType});
        return genID;
    }

    public List<SalesAndPurchaseItem> getSaleDetailsBySalesID(Long id) {
        query = "select sd." + AppConstant.SALES_DETAIL_ID + ", sd." + AppConstant.SALES_DETAIL_STOCK_ID + ", st." + AppConstant.STOCK_NAME + ", sd." + AppConstant.SALES_DETAIL_QTY
                + ", sd." + AppConstant.SALES_DETAIL_PRICE + ", sd." + AppConstant.SALES_DETAIL_TOTAL + ", sd." + AppConstant.SALES_DETAIL_DISCOUNT + ", sd." + AppConstant.SALES_DETAIL_DISCOUNT_AMT
                + ", st." + AppConstant.STOCK_CODE_NUM + ", st." + AppConstant.STOCK_BARCODE
                + " from " + AppConstant.SALES_DETAIL_TABLE_NAME + " sd, " + AppConstant.STOCK_TABLE_NAME + " st where sd." + AppConstant.SALES_DETAIL_STOCK_ID + "=st." + AppConstant.STOCK_ID
                + " and sd." + AppConstant.SALES_DETAIL_SALES_ID + " = " + id;
        database = databaseHelper.getReadableDatabase();
        cursor = database.rawQuery(query, null);
        salesAndPurchaseItemList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                salesAndPurchaseItem = new SalesAndPurchaseItem();
                salesAndPurchaseItem.setId(cursor.getLong(0));
                salesAndPurchaseItem.setStockID(cursor.getLong(1));
                salesAndPurchaseItem.setItemName(cursor.getString(2));
                salesAndPurchaseItem.setQty(cursor.getInt(3));
                salesAndPurchaseItem.setPrice(cursor.getDouble(4));
                salesAndPurchaseItem.setTotalPrice(cursor.getDouble(5));
                //  salesAndPurchaseItem.setDiscountPercent(cursor.getDouble(6));

                salesAndPurchaseItem.setDiscountAmount(cursor.getDouble(7));

                Log.w("In DB", "hello" + salesAndPurchaseItem.getDiscountAmount());

                salesAndPurchaseItem.setStockCode(cursor.getString(8));
                salesAndPurchaseItem.setBarcode(cursor.getString(9));
                salesAndPurchaseItemList.add(salesAndPurchaseItem);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return salesAndPurchaseItemList;
    }

    public boolean updateSaleDetailByID(Long id, Long stockID, int qty, double price, String discount, Long taxID, double total, double taxAmt, double discountAmt, double taxRate) {
        query = "update " + AppConstant.SALES_DETAIL_TABLE_NAME + " set " + AppConstant.SALES_DETAIL_STOCK_ID + "=" + stockID + ", " + AppConstant.SALES_DETAIL_QTY + "= " + qty + ", "
                + AppConstant.SALES_DETAIL_PRICE + "=" + price + ", " + AppConstant.SALES_DETAIL_DISCOUNT + "=?, " + AppConstant.SALES_DETAIL_TAX_ID + "=" + taxID + ", " + AppConstant.SALES_DETAIL_TOTAL
                + "=" + total + ", " + AppConstant.SALES_DETAIL_TAX_AMOUNT + "=" + taxAmt + ", " + AppConstant.SALES_DETAIL_DISCOUNT_AMT + "=" + discountAmt + ", "
                + AppConstant.SALES_DETAIL_TAX_RATE + "=" + taxRate + " where " + AppConstant.SALES_DETAIL_ID + "=" + id;
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query, new String[]{discount});
        return flag.isInserted();
    }

    public boolean deleteSaleDetailByID(Long id) {
        query = "delete from " + AppConstant.SALES_DETAIL_TABLE_NAME + " where " + AppConstant.SALES_DETAIL_ID + "=" + id;
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query);
        return true;
    }

    public List<SalesStock> getSaleDetailStocksBySalesID(Long id) {
        query = "select " + AppConstant.SALES_DETAIL_STOCK_ID + ", " + AppConstant.SALES_DETAIL_QTY + " from " + AppConstant.SALES_DETAIL_TABLE_NAME + "  where " + AppConstant.SALES_DETAIL_SALES_ID + " = " + id;
        database = databaseHelper.getReadableDatabase();
        cursor = database.rawQuery(query, null);
        salesStockList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                salesStock = new SalesStock();
                salesStock.setStockID(cursor.getLong(0));
                salesStock.setStockQty(cursor.getInt(1));
                salesStockList.add(salesStock);
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        return salesStockList;
    }

}
