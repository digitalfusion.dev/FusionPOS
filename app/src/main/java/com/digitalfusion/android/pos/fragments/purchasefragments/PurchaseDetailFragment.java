package com.digitalfusion.android.pos.fragments.purchasefragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAPrintPreviewAdapter;
import com.digitalfusion.android.pos.database.business.PurchaseManager;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.database.model.BusinessSetting;
import com.digitalfusion.android.pos.database.model.PurchaseHeader;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.util.AidlUtil;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.BluetoothUtil;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.GrantPermission;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.PrintImage;
import com.digitalfusion.android.pos.util.PrintInvoice;
import com.digitalfusion.android.pos.util.PrintViewSlip;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.util.TypefaceSpan;
import com.digitalfusion.android.pos.views.FolderChooserDialog;
import com.digitalfusion.android.pos.views.FusionToast;
import com.itextpdf.text.DocumentException;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.prologic.printapi.PrintCanvas;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.digitalfusion.android.pos.util.AppConstant.DEFAULT_SUPPLIER;

/**
 * Created by MD003 on 9/7/16.
 */
public class PurchaseDetailFragment extends Fragment implements FolderChooserDialog.FolderSelectionCallback {

    public static final String KEY = "purchaseId";
    public static final String PRINTKEY = "Print";
    private final static char ESC_CHAR = 0x1B;
    private final static char GS = 0x1D;
    private final static byte[] LINE_FEED = new byte[]{0x0A};
    private final static byte[] CUT_PAPER = new byte[]{GS, 0x56, 0x00};
    private final static byte[] INIT_PRINTER = new byte[]{ESC_CHAR, 0x40};
    private final static byte[] SET_PRINT_MODE = new byte[]{ESC_CHAR, 0x21, 0x00};
    private final static byte[] UNDERLINED_MODE = new byte[]{ESC_CHAR, 0x2D, 0x32};
    private final static byte[] EMPHASIZED_MODE_ON = new byte[]{ESC_CHAR, 0x45, 0x01};
    private final static byte[] EMPHASIZED_MODE_OFF = new byte[]{ESC_CHAR, 0x45, 0x00};
    private final static byte[] DSTRIKE_MODE_ON = new byte[]{ESC_CHAR, 0x47, 0x01};
    private final static byte[] DSTRIKE_MODE_OFF = new byte[]{ESC_CHAR, 0x47, 0x00};
    private final static byte[] ALIGN_LEFT = new byte[]{ESC_CHAR, 0x61, 0x30};
    private final static byte[] ALIGN_CENTER = new byte[]{ESC_CHAR, 0x61, 0x31};
    private final static byte[] ALIGN_RIGHT = new byte[]{ESC_CHAR, 0x61, 0x32};
    private final static byte[] UPSIDE_ON = new byte[]{ESC_CHAR, 0x7B, 0x01};
    private final static byte[] UPSIDE_OFF = new byte[]{ESC_CHAR, 0x7B, 0x00};
    private final static byte[] SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33};
    private final static byte[] SET_LINE_SPACE_24 = new byte[]{ESC_CHAR, 0x33, 24};
    private final static byte[] SET_LINE_SPACE_30 = new byte[]{ESC_CHAR, 0x33, 30};
    private final static byte FONT_POINT = 0x11;
    private final static String DEFAULT_TYPE = "SERIAL", SERIAL_TYPE = "SERIAL", USB_TYPE = "USB";
    private final static String[] PRINTER = {"ESC/POS Epson Printers"};
    public TextView printDeviceNameTextView;
    KProgressHUD printHud;
    KProgressHUD connectHud;
    MaterialDialog fileOverrideMaterialDialog;
    KProgressHUD saveHud;
    private View mainLayout;
    //UI components from main
    private TextView taxAmtTextView;
    private TextView taxRateTextView;
    private TextView taxTypeTextView;
    private TextView supplierNameTextView;
    private TextView supplierPhoneTextView;
    private TextView supplierAddressTextView;
    private LinearLayout supplierPhoneLayout;
    private LinearLayout supplierAddressLayout;
    private RecyclerView purchaseItemRecyclerView;
    private TextView purchaseIdTextView;
    private TextView dateTextView;
    private TextView discountTextView;
    private TextView subtotalTextView;
    private TextView totalTextView;

    //values
    private TextView businessNameTextView;
    private TextView addressTextView;
    private TextView phoneTextView;


    //Values
    private TextView paidAmtTextView;
    private TextView headerTextView;
    private TextView footerTextView;
    //Business
    private PurchaseManager purchaseManager;
    private Context context;
    private RVAPrintPreviewAdapter rvAdapterForStockItemInSaleAndPurchaseDetail;
    private List<SalesAndPurchaseItem> purchaseItemViewInSaleList;
    private String supplierName = "";
    private String purchaseInvoiceNo;
    private int qty = 0;
    private Boolean foc = false;
    private Double deliveryCharges = 0.0;
    private Double voucherTax = 0.0;
    private Double subtotal = 0.0;
    private Double paidAmount = 0.0;
    private int editposition;
    private Calendar now;
    private int purchaseDay;
    private int purchaseMonth;
    private int purchaseYear;
    private DatePickerDialog datePickerDialog;
    private String date;
    private Long purchaseId;
    private PurchaseHeader purchaseHeader;
    private BusinessSetting businessSetting;
    private SettingManager settingManager;
    private PrintViewSlip printViewSlip;
    private BluetoothUtil bluetoothUtil;
    private TextView statusTextView;
    private boolean isPrint = false;
    private FolderChooserDialog folderChooserDialog;
    private MaterialDialog fileNameMaterialDialog;
    private EditText fileNameEditText;
    private Button savePdfButton;
    private String path;
    private String fileName = "";
    private GrantPermission grantPermission;

    private String header;

    private String headerText;

    private String footer;

    private String footerText;
    private boolean directPrint;
    private String supplierPhNo;
    private String supplierAddress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.purchase_print_preview, null);

        purchaseItemViewInSaleList = new ArrayList<>();

        context = getContext();

        setHasOptionsMenu(true);

        bluetoothUtil = BluetoothUtil.getInstance(this);

        directPrint = getArguments().getBoolean(PRINTKEY, false);

        TypedArray purchaseDetail = context.getTheme().obtainStyledAttributes(new int[]{R.attr.purchase_detail});

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(purchaseDetail.getString(0));

        purchaseManager = new PurchaseManager(context);

        settingManager = new SettingManager(context);

        businessSetting = settingManager.getBusinessSetting();

        now = Calendar.getInstance();

        purchaseId = getArguments().getLong(KEY);

        grantPermission = new GrantPermission(this);

        initializeOldData();

        loadUI();

        configUI();

        configRecyclerView();

        buildAndConfigDialog();

        MainActivity.setCurrentFragment(this);

        //preparePrintView();

        mainLayout.findViewById(R.id.blue_toothLL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isPrint = false;

                createBluetoothConnection();


            }
        });

/*        printViewSlip.setPrintListener(new PrintViewSlip.PrintListener() {
            @Override
            public void onPrepare() {
                printHud.show();
            }

            @Override
            public void onSuccess() {

                if(printHud.isShowing()){

                    printHud.dismiss();

                }


            }

            @Override
            public void onFailure() {

                String makeSure=context.getTheme().obtainStyledAttributes(new int[]{R.attr.make_sure_bluetooth_turn_on_both_devices_and_choose_printer_only}).getString(0);
                DisplayToast(makeSure);

                if(printHud.isShowing()){

                    printHud.dismiss();

                }

            }
        });

*/
        bluetoothUtil.setBluetoothConnectionListener(new BluetoothUtil.JobListener() {

            @Override
            public void onPrepare() {
                Log.w("on prepare", "onprep pare");

                connectHud.show();

            }

            @Override
            public void onSuccess() {

                Log.w("on Success", "on success");

                if (connectHud.isShowing()) {

                    connectHud.dismiss();

                }

                printDeviceNameTextView.setText(bluetoothUtil.getDevice().getName());


                if (bluetoothUtil.isConnected()) {

                    statusTextView.setTextColor(Color.parseColor("#2E7D32"));
                    String connected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.connected}).getString(0);

                    statusTextView.setText(connected);

                } else {

                    statusTextView.setTextColor(Color.parseColor("#DD2C00"));
                    String notConnected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);

                    statusTextView.setText(notConnected);

                }

                if (isPrint) {

                    try {

                        // printViewSlip.printNow(bluetoothUtil.getOutputStream());
                        preparePrint();
                    } catch (Exception e) {
                        String errorOccur = context.getTheme().obtainStyledAttributes(new int[]{R.attr.error_occur_please_try_again}).getString(0);

                        FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.ERROR);
                        e.printStackTrace();

                    }
                }

            }

            @Override
            public void onFailure() {
                String noDevice = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_device}).getString(0);

                printDeviceNameTextView.setText(noDevice);

                statusTextView.setTextColor(Color.parseColor("#DD2C00"));
                String notConnected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);

                statusTextView.setText(notConnected);
                String makeSure = context.getTheme().obtainStyledAttributes(new int[]{R.attr.make_sure_bluetooth_turn_on_both_devices_and_choose_printer_only}).getString(0);

                FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.ERROR);

                if (connectHud.isShowing()) {

                    connectHud.dismiss();

                }

            }

            @Override
            public void onDisconnected() {
                String connectionLost = context.getTheme().obtainStyledAttributes(new int[]{R.attr.connection_lost}).getString(0);
                String noDevice = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_device}).getString(0);
                String notConnected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.connected}).getString(0);


                //DisplayToast(connectionLost);

                printDeviceNameTextView.setText(noDevice);

                statusTextView.setTextColor(Color.parseColor("#DD2C00"));

                statusTextView.setText(notConnected);


            }
        });

        return mainLayout;
    }

    public void buildAndConfigDialog() {

        final String fileNamestr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.file_name}).getString(0);


        String pleaseWait = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);

        String savingAsPdf = context.getTheme().obtainStyledAttributes(new int[]{R.attr.saving_as_pdf}).getString(0);

        SpannableString pleaseWaitTypeface = new SpannableString(pleaseWait.toString());
        pleaseWaitTypeface.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, pleaseWait.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        SpannableString savingAsPdfTypeface = new SpannableString(savingAsPdf.toString());
        savingAsPdfTypeface.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, savingAsPdf.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        View processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
        ImageView imageView = (ImageView) processDialog.findViewById(R.id.image_view);
        imageView.setBackgroundResource(R.drawable.spin_animation);
        AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
        drawable.start();

        //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        saveHud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setDetailsLabel(savingAsPdfTypeface.toString())
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);

        fileNameMaterialDialog = new MaterialDialog.Builder(getContext())
                .customView(R.layout.file_name, false)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .title(fileNamestr)
                .build();

        fileNameEditText = (EditText) fileNameMaterialDialog.findViewById(R.id.file_name);

        savePdfButton = (Button) fileNameMaterialDialog.findViewById(R.id.save);

        savePdfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fileNameEditText.length() > 0) {
                    String npath = path + "/" + fileNameEditText.getText().toString() + ".pdf";

                    fileName = fileNameEditText.getText().toString();

                    File file = new File(npath);
                    // If file does not exists, then create it
                    if (!file.exists()) {
                        new SavePDFProgress().execute(path);
                        fileNameMaterialDialog.dismiss();
                    } else {
                        fileNameMaterialDialog.dismiss();
                        fileOverrideMaterialDialog.show();
                    }
                } else {

                    String fillFileName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_fill_file_name}).getString(0);
                    fileNameEditText.setError(fillFileName);
                }


            }
        });

        fileNameMaterialDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileNameMaterialDialog.dismiss();
            }
        });

        fileOverrideMaterialDialog = new MaterialDialog.Builder(getContext())
                .customView(R.layout.confirm_dialog, false)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                // .title("File name already exist.Do you want to override it?")
                .build();

        String wantToOverride = context.getTheme().obtainStyledAttributes(new int[]{R.attr.file_name_already_exist_do_u_want_to_override_it}).getString(0);
        ((TextView) fileOverrideMaterialDialog.findViewById(R.id.title)).setText(wantToOverride);

        fileOverrideMaterialDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileOverrideMaterialDialog.dismiss();
                fileNameMaterialDialog.show();
            }
        });

        fileOverrideMaterialDialog.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fileOverrideMaterialDialog.isShowing()) {
                    fileOverrideMaterialDialog.dismiss();
                }
                new SavePDFProgress().execute(path);
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();

        if (directPrint) {

            preparePrint();

        }
    }

    private void showFolderChooserDialog() {
        if (folderChooserDialog == null) {
            folderChooserDialog = new FolderChooserDialog();
            folderChooserDialog.setmCallback(this);
        }
        folderChooserDialog.show(getFragmentManager(), "folderChooser");
    }

    private void createBluetoothConnection() {
        if (POSUtil.isTabetLand(context)) {
            bluetoothUtil.createConnection(true);
        } else {
            bluetoothUtil.createConnection(false);
        }

    }

    public void initializeOldData() {


        purchaseItemViewInSaleList = purchaseManager.getPurchaseDetailsByPurchaseID(purchaseId);

        purchaseHeader = purchaseManager.getPurchaseHeaderByPurchaseID(purchaseId, new InsertedBooleanHolder());

        purchaseInvoiceNo = purchaseHeader.getPurchaseVocherNo();

        supplierName = purchaseHeader.getSupplierName();

        purchaseDay = Integer.parseInt(purchaseHeader.getDay());

        purchaseMonth = Integer.parseInt(purchaseHeader.getMonth());

        purchaseYear = Integer.parseInt(purchaseHeader.getYear());

        now.set(purchaseYear, purchaseMonth - 1, purchaseDay);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.print, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.print) {

            preparePrint();
        } else if (item.getItemId() == R.id.edit) {

            Bundle bundle = new Bundle();

            bundle.putSerializable("purchaseheader", purchaseManager.getPurchase(purchaseId));

            //  AddEditNewSaleFragment saleEditFragments = new AddEditNewSaleFragment();

            // saleEditFragments.setArguments(bundle);

            //  MainActivity.replaceFragment(saleEditFragments);


            // bundle.putSerializable(AddEditNewSaleFragment.KEY, salesHistoryViewList.get(postion));

            Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

            addCurrencyIntent.putExtras(bundle);

            addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_PURCHASE);

            startActivity(addCurrencyIntent);

        } else if (item.getItemId() == R.id.save_pdf) {
            if (grantPermission.requestPermissions()) {
                showFolderChooserDialog();
            }

        }

        return true;
    }

    private void preparePrint() {
        isPrint = true;

        if (!bluetoothUtil.isConnected() && !POSUtil.isInternalPrinter(context)) {

            createBluetoothConnection();

        } else {
            String saleTxt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.sale}).getString(0);
            String supplierTxt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.supplier}).getString(0);
            String numberTxt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.number}).getString(0);
            String itemNameTxt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.item_name}).getString(0);
            String amountTxt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.amount}).getString(0);
            String subtotalTxt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.subtotal}).getString(0);
            String taxTxt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.tax}).getString(0);
            String discountTxt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.discount}).getString(0);
            String totalTxt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.total}).getString(0);
            String paidAmountTxt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.paid_amount}).getString(0);
            String supPhoneNumber = ThemeUtil.getString(context, R.attr.phone_no);
            String supAddressTxt = ThemeUtil.getString(context, R.attr.address);

            String thanksyouTxt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.thank_you}).getString(0);

            Typeface mTypeface = Typeface.createFromAsset(context.getApplicationContext()
                    .getAssets(), String.format("fonts/%s", "Zawgyi-One.ttf"));
            PrintCanvas printCanvas;
            if (POSUtil.is80MMPrinter(getContext())) {
                printCanvas = new PrintCanvas(PrintCanvas.PAPERSIZE._80MM, mTypeface, 30);
            } else {
                printCanvas = new PrintCanvas(PrintCanvas.PAPERSIZE._58MM, mTypeface, 20);
            }

            printCanvas.writeText(PrintCanvas.ALIGN.CENTER, headerText, PrintCanvas.ALIGN.CENTER, 60);
            printCanvas.nextLine();
            PrintCanvas.Row row = printCanvas.new Row();

            PrintCanvas.Column column = printCanvas.new Column(40, saleTxt + " : ", PrintCanvas.ALIGN.LEFT);

            row.addColumn(column);

            column = printCanvas.new Column(35, "#" + purchaseHeader.getPurchaseVocherNo(), PrintCanvas.ALIGN.LEFT);

            row.addColumn(column);

            column = printCanvas.new Column(35, DateUtility.makeDateFormatWithSlash(purchaseHeader.getSaleDate()), PrintCanvas.ALIGN.RIGHT);

            row.addColumn(column);

            printCanvas.writeRow(row);

            row = printCanvas.new Row();

            column = printCanvas.new Column(40, supplierTxt + " : ", PrintCanvas.ALIGN.LEFT);

            row.addColumn(column);

            column = printCanvas.new Column(35, purchaseHeader.getSupplierName(), PrintCanvas.ALIGN.LEFT);

            row.addColumn(column);

            column = printCanvas.new Column(35, "", PrintCanvas.ALIGN.LEFT);

            row.addColumn(column);

            printCanvas.writeRow(row);

            if (supplierPhNo != null) {
                //PhoneNo
                row = printCanvas.new Row();
                column = printCanvas.new Column(40, supPhoneNumber + " : ", PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);

                //09####

                column = printCanvas.new Column(60, supplierPhNo, PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);
                printCanvas.writeRow(row);

            }

            if (supplierAddress != null) {
                //Address
                row = printCanvas.new Row();
                column = printCanvas.new Column(40, supAddressTxt + " : ", PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);

                //address
                column = printCanvas.new Column(70, supplierAddress, PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);
                printCanvas.writeRow(row);
            }

            printCanvas.writeLine();

            row = printCanvas.new Row();

            column = printCanvas.new Column(15, numberTxt, PrintCanvas.ALIGN.CENTER);

            row.addColumn(column);

            column = printCanvas.new Column(55, itemNameTxt, PrintCanvas.ALIGN.LEFT);

            row.addColumn(column);

            column = printCanvas.new Column(30, amountTxt, PrintCanvas.ALIGN.RIGHT);

            row.addColumn(column);

            printCanvas.writeRow(row);

            printCanvas.writeLine();


            for (SalesAndPurchaseItem s : purchaseItemViewInSaleList) {

                row = printCanvas.new Row();

                column = printCanvas.new Column(15, Integer.toString(purchaseItemViewInSaleList.indexOf(s) + 1), PrintCanvas.ALIGN.CENTER);

                row.addColumn(column);

                column = printCanvas.new Column(45, s.getItemName(), PrintCanvas.ALIGN.LEFT);

                row.addColumn(column);

                column = printCanvas.new Column(15, "x " + s.getQty(), PrintCanvas.ALIGN.CENTER);

                row.addColumn(column);

                column = printCanvas.new Column(30, POSUtil.NumberFormat(s.getTotalPrice()), PrintCanvas.ALIGN.RIGHT);

                row.addColumn(column);

                printCanvas.writeRow(row);
            }
            printCanvas.nextLine();
            printCanvas.writeLine();

            row = printCanvas.new Row();

            column = printCanvas.new Column(20, "", PrintCanvas.ALIGN.LEFT);

            row.addColumn(column);

            column = printCanvas.new Column(40, subtotalTxt, PrintCanvas.ALIGN.LEFT);

            row.addColumn(column);

            column = printCanvas.new Column(15, ":", PrintCanvas.ALIGN.CENTER);

            row.addColumn(column);

            column = printCanvas.new Column(30, POSUtil.NumberFormat(purchaseHeader.getSubtotal()), PrintCanvas.ALIGN.RIGHT);

            row.addColumn(column);

            printCanvas.writeRow(row);

            if (purchaseHeader.getTaxAmt() != 0) {
                row = printCanvas.new Row();

                column = printCanvas.new Column(20, "", PrintCanvas.ALIGN.LEFT);

                row.addColumn(column);

                column = printCanvas.new Column(40, taxTxt + "(" + purchaseHeader.getTaxRate() + "% " + purchaseHeader.getTaxType() + ")", PrintCanvas.ALIGN.LEFT);

                row.addColumn(column);

                column = printCanvas.new Column(15, ":", PrintCanvas.ALIGN.CENTER);

                row.addColumn(column);

                column = printCanvas.new Column(30, POSUtil.NumberFormat(purchaseHeader.getTaxAmt()), PrintCanvas.ALIGN.RIGHT);

                row.addColumn(column);

                printCanvas.writeRow(row);
            }

            if (purchaseHeader.getDiscountAmt() != 0) {
                row = printCanvas.new Row();

                column = printCanvas.new Column(20, "", PrintCanvas.ALIGN.LEFT);

                row.addColumn(column);

                column = printCanvas.new Column(40, discountTxt, PrintCanvas.ALIGN.LEFT);

                row.addColumn(column);

                column = printCanvas.new Column(15, ":", PrintCanvas.ALIGN.CENTER);

                row.addColumn(column);

                column = printCanvas.new Column(30, POSUtil.NumberFormat(purchaseHeader.getDiscountAmt()), PrintCanvas.ALIGN.RIGHT);

                row.addColumn(column);

                printCanvas.writeRow(row);
            }
            printCanvas.writeLine();

            row = printCanvas.new Row();

            column = printCanvas.new Column(10, "", PrintCanvas.ALIGN.LEFT);

            row.addColumn(column);

            column = printCanvas.new Column(50, totalTxt + "(" + AppConstant.CURRENCY + ")", PrintCanvas.ALIGN.LEFT);

            row.addColumn(column);

            column = printCanvas.new Column(15, ":", PrintCanvas.ALIGN.CENTER);

            row.addColumn(column);

            column = printCanvas.new Column(30, POSUtil.NumberFormat(purchaseHeader.getTotal()), PrintCanvas.ALIGN.RIGHT);

            row.addColumn(column);

            printCanvas.writeRow(row);

            row = printCanvas.new Row();

            column = printCanvas.new Column(10, "", PrintCanvas.ALIGN.LEFT);

            row.addColumn(column);

            column = printCanvas.new Column(50, paidAmountTxt + "(" + AppConstant.CURRENCY + ")", PrintCanvas.ALIGN.LEFT);

            row.addColumn(column);

            column = printCanvas.new Column(15, ":", PrintCanvas.ALIGN.CENTER);

            row.addColumn(column);

            column = printCanvas.new Column(30, POSUtil.NumberFormat(purchaseHeader.getPaidAmt()), PrintCanvas.ALIGN.RIGHT);

            row.addColumn(column);

            printCanvas.writeRow(row);

            printCanvas.writeLine();

            printCanvas.writeText(PrintCanvas.ALIGN.CENTER, footerText, PrintCanvas.ALIGN.CENTER, 60);

            //printCanvas.drawWaterMarkDiagonal();

            // imageView.setImageBitmap(printCanvas.getBitmap());

            // mainLayout.findViewById(R.id.aa).setVisibility(View.GONE);

            Bitmap bmpMonochrome = Bitmap.createBitmap(printCanvas.getBitmap().getWidth(), printCanvas.getBitmap().getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas1 = new Canvas(bmpMonochrome);
            Paint paint1 = new Paint();

            canvas1.drawColor(Color.WHITE);
            canvas1.drawBitmap(printCanvas.getBitmap(), 0, 0, paint1);

            PrintImage printImage = new PrintImage(bmpMonochrome);


            try {
                if (POSUtil.isInternalPrinter(getContext())) {

                    AidlUtil.getInstance().initPrinter();
                    // AidlUtil.getInstance().printText(printViewSlip.getPrintString(), 24, false, false,"Type monospace");
                    AidlUtil.getInstance().printBitmap(printCanvas.getBitmap());
                } else {
                    bluetoothUtil.getOutputStream().write(printImage.getPrintImageData());
                    bluetoothUtil.getOutputStream().write(CUT_PAPER);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void DisplayToast(String str) {

        FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.ERROR);


    }//

    /**
     * Gets the pixels stored in an image.
     * TODO very slow, could be improved (use different class, cache result, etc.)
     *
     * @param image image to get pixels from.
     * @return 2D array of pixels of the image (RGB, row major order)
     */
    private int[][] getPixelsSlow(Bitmap image) {
        int width = image.getWidth();
        int height = image.getHeight();
        int[][] result = new int[height][width];
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                result[row][col] = image.getPixel(col, row);
            }
        }

        return result;
    }

    /**
     * Defines if a color should be printed (burned).
     *
     * @param color RGB color.
     * @return true if should be printed/burned (black), false otherwise (white).
     */
    private boolean shouldPrintColor(int color) {
        final int threshold = 127;
        int a, r, g, b, luminance;
        a = (color >> 24) & 0xff;
        if (a != 0xff) { // ignore pixels with alpha channel
            return false;
        }
        r = (color >> 16) & 0xff;
        g = (color >> 8) & 0xff;
        b = color & 0xff;

        luminance = (int) (0.299 * r + 0.587 * g + 0.114 * b);

        return luminance < threshold;
    }

    private byte[] collectSlice(int y, int x, int[][] img) {
        byte[] slices = new byte[]{0, 0, 0};
        for (int yy = y, i = 0; yy < y + 24 && i < 3; yy += 8, i++) {// va a hacer 3 ciclos
            byte slice = 0;
            for (int b = 0; b < 8; b++) {
                int yyy = yy + b;
                if (yyy >= img.length) {
                    continue;
                }
                int col = img[yyy][x];
                boolean v = shouldPrintColor(col);
                slice |= (byte) ((v ? 1 : 0) << (7 - b));
            }
            slices[i] = slice;
        }

        return slices;
    }

    private void preparePrintView() {

        printViewSlip = new PrintViewSlip();

        if (!businessSetting.isEmptyBusinessName()) {

            printViewSlip.addString(businessSetting.getBusinessName(), PrintViewSlip.Alignment.CENTER);

            printViewSlip.nextLine();

        }

        if (!businessSetting.isEmptyAddress()) {

            printViewSlip.addString(businessSetting.getAddress(), PrintViewSlip.Alignment.CENTER);

            printViewSlip.nextLine();
        }
        if (!businessSetting.isEmptyPhoneNo()) {

            printViewSlip.addString(businessSetting.getPhoneNo(), PrintViewSlip.Alignment.CENTER);

            printViewSlip.nextLine();

        }

        printViewSlip.nextLine();

        printViewSlip.addString(PrintViewSlip.STYLE.BOLD, "Purchase");

        printViewSlip.addSpace(5);

        printViewSlip.addFullColon();

        printViewSlip.addString(PrintViewSlip.STYLE.BOLD, "#");

        printViewSlip.addString(PrintViewSlip.STYLE.BOLD, purchaseHeader.getPurchaseVocherNo());

        printViewSlip.addString(DateUtility.makeDateFormatWithSlash(purchaseHeader.getSaleDate()), PrintViewSlip.Alignment.RIGHT);

        printViewSlip.nextLine();
        String supplierstr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.supplier}).getString(0);

        printViewSlip.addString(supplierstr);

        printViewSlip.addFullColon();

        printViewSlip.addString(purchaseHeader.getSupplierName());

        printViewSlip.nextLine();


        printViewSlip.BuildTable(3);
        String nostr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no}).getString(0);

        printViewSlip.addNewColumn(nostr, 5, PrintViewSlip.Alignment.CENTER, 0);
        String itemnamestr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.item_name}).getString(0);

        printViewSlip.addNewColumn(itemnamestr, 30, PrintViewSlip.Alignment.LEFT, 5);

        // printViewSlip.addNewColumn("Price",10, PrintViewSlip.Alignment.RIGHT);
        String totalstr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.total}).getString(0);

        printViewSlip.addNewColumn(totalstr, 13, PrintViewSlip.Alignment.RIGHT, 35);

        printViewSlip.addHeader();

        int i = 1;

        for (SalesAndPurchaseItem s : purchaseItemViewInSaleList) {
            printViewSlip.addRow(Integer.toString(i), s.getItemName() + " x " + s.getQty(), POSUtil.NumberFormat(s.getTotalPrice()));
            i++;
        }

        printViewSlip.addDividerLine();

        String subtotalstr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.subtotal}).getString(0);

        printViewSlip.addRow("", "                    " + subtotalstr + " :", POSUtil.NumberFormat(purchaseHeader.getSubtotal()));


        // printViewSlip.addRow("","","Delivery :",POSUtil.NumberFormat(salesHeaderView.getSubtotal()));

        String taxstr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.tax}).getString(0);

        printViewSlip.addRow("", "                    " + taxstr + "      :", POSUtil.NumberFormat(purchaseHeader.getPaidAmt()));

        String discountstr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.discount}).getString(0);

        printViewSlip.addRow("", "                    " + discountstr + " :", POSUtil.NumberFormat(purchaseHeader.getDiscountAmt()));

        printViewSlip.addDividerLine();


        printViewSlip.addRow(PrintViewSlip.STYLE.BIG, "", "                    " + totalstr + "    :", POSUtil.NumberFormat(purchaseHeader.getTotal()));

        String paidamtstr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.paid_amount}).getString(0);

        printViewSlip.addRow(PrintViewSlip.STYLE.BIG, "", "                    " + paidamtstr + "    :", POSUtil.NumberFormat(purchaseHeader.getTotal()));

        printViewSlip.addDividerLine();

        printViewSlip.nextLine();
        String thankU = context.getTheme().obtainStyledAttributes(new int[]{R.attr.thank_you}).getString(0);

        printViewSlip.addStringBold(thankU, PrintViewSlip.Alignment.CENTER);

        printViewSlip.nextLine();

        printViewSlip.nextLine();

        printViewSlip.nextLine();

        printViewSlip.nextLine();

        printViewSlip.nextLine();


        //printViewSlip.addHeader("No.",);


    }

    public void configUI() {
        header = context.getTheme().obtainStyledAttributes(new int[]{R.attr.header_text}).getString(0);
        String temp_header = POSUtil.getHeaderTxt(getContext());
        footer = POSUtil.getFooterTxt(getContext());
        if (temp_header.equalsIgnoreCase("") || temp_header == null || temp_header.equalsIgnoreCase(header)) {
            headerText = "";
            headerTextView.setVisibility(View.GONE);
        } else {
            headerText = temp_header;
            headerTextView.setText(temp_header);
            headerTextView.setVisibility(View.VISIBLE);
        }
        if (footer.equalsIgnoreCase("") || footer.equalsIgnoreCase("-1")) {
            footerText = "";
            footerTextView.setVisibility(View.GONE);
        } else {
            footerText = footer;
            footerTextView.setText(footer);
            footerTextView.setVisibility(View.VISIBLE);
        }
        if (purchaseHeader.getTaxAmt() == 0) {
            taxAmtTextView.setVisibility(View.GONE);
            mainLayout.findViewById(R.id.txt_).setVisibility(View.GONE);
            mainLayout.findViewById(R.id.txt_label).setVisibility(View.GONE);
        }
        if (purchaseHeader.getDiscountAmt() == 0) {
            discountTextView.setVisibility(View.GONE);
            mainLayout.findViewById(R.id.discount_lbl).setVisibility(View.GONE);
            mainLayout.findViewById(R.id.discount_txt).setVisibility(View.GONE);
        }
        if (bluetoothUtil.isConnected()) {
            // printDeviceNameTextView.setTextColor(Color.parseColor("#2E7D32"));
            String connected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.connected}).getString(0);

            printDeviceNameTextView.setText(bluetoothUtil.getDevice().getName());

            statusTextView.setTextColor(Color.parseColor("#2E7D32"));

            statusTextView.setText(connected);

        } else {
            String noDevice = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_device}).getString(0);
            String notConnected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);

            // printDeviceNameTextView.setTextColor(Color.parseColor("#DD2C00"));
            printDeviceNameTextView.setText(noDevice);

            statusTextView.setTextColor(Color.parseColor("#DD2C00"));

            statusTextView.setText(notConnected);
        }


        if (businessSetting.isEmptyBusinessName()) {

            businessNameTextView.setVisibility(View.GONE);

        } else {

            businessNameTextView.setText(businessSetting.getBusinessName());

        }

        if (businessSetting.isEmptyAddress()) {
            Log.w("address", "empty");

            addressTextView.setVisibility(View.GONE);

        } else {

            addressTextView.setText(businessSetting.getAddress());

        }
        if (businessSetting.isEmptyPhoneNo()) {
            Log.w("phone", "empty");
            phoneTextView.setVisibility(View.GONE);

        } else {

            phoneTextView.setText(businessSetting.getPhoneNo());

        }

        paidAmtTextView.setText(purchaseHeader.getPaidAmtString());

        supplierNameTextView.setText(supplierName);

        supplierPhoneTextView.setText(supplierPhNo);

        supplierAddressTextView.setText(supplierAddress);

        purchaseIdTextView.setText(purchaseInvoiceNo);

        taxAmtTextView.setText(POSUtil.NumberFormat(purchaseHeader.getTaxAmt()));

        taxRateTextView.setText(Double.toString(purchaseHeader.getTaxRate()));

        taxTypeTextView.setText(purchaseHeader.getTaxType());

        discountTextView.setText(Double.toString(purchaseHeader.getDiscountAmt()));

        configDateUI();

        updateViewData();
    }

    public void configDateUI() {

        date = DateUtility.makeDateFormatWithSlash(Integer.toString(purchaseYear), Integer.toString(purchaseMonth), Integer.toString(purchaseDay));

        dateTextView.setText(date);
    }


    public void configRecyclerView() {

        rvAdapterForStockItemInSaleAndPurchaseDetail = new RVAPrintPreviewAdapter(purchaseItemViewInSaleList);

        purchaseItemRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        purchaseItemRecyclerView.setAdapter(rvAdapterForStockItemInSaleAndPurchaseDetail);

    }


    private void loadUI() {
        paidAmtTextView = (TextView) mainLayout.findViewById(R.id.paid_amt);
        String pleasewait = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        String printing = context.getTheme().obtainStyledAttributes(new int[]{R.attr.printing}).getString(0);
        String connecting = context.getTheme().obtainStyledAttributes(new int[]{R.attr.connecting}).getString(0);

        SpannableString pleaseWaitTypeface = new SpannableString(pleasewait.toString());
        pleaseWaitTypeface.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, pleasewait.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        SpannableString printingTypeface = new SpannableString(printing.toString());
        printingTypeface.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, printing.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        SpannableString connectingTypeface = new SpannableString(connecting.toString());
        connectingTypeface.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, connecting.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        View processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
        ImageView imageView = (ImageView) processDialog.findViewById(R.id.image_view);
        imageView.setBackgroundResource(R.drawable.spin_animation);
        AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
        drawable.start();

        //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        printHud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);

        //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        connectHud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);

        statusTextView = (TextView) mainLayout.findViewById(R.id.bt_status);

        printDeviceNameTextView = (TextView) mainLayout.findViewById(R.id.device_name);

        businessNameTextView = (TextView) mainLayout.findViewById(R.id.business_name);

        addressTextView = (TextView) mainLayout.findViewById(R.id.address);

        phoneTextView = (TextView) mainLayout.findViewById(R.id.phone_no);

        supplierNameTextView = (TextView) mainLayout.findViewById(R.id.purchase_customer_in_purcahse_detail_tv);

        supplierPhoneTextView = (TextView) mainLayout.findViewById(R.id.supplier_in_phoneNo);

        supplierAddressTextView = (TextView) mainLayout.findViewById(R.id.supplier_in_address);

        supplierPhoneLayout = (LinearLayout) mainLayout.findViewById(R.id.supplier_phone_layout);

        supplierAddressLayout = (LinearLayout) mainLayout.findViewById(R.id.supplier_address_layout);

        purchaseItemRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.purchase_item_list_rv);

        purchaseIdTextView = (TextView) mainLayout.findViewById(R.id.purchase_id_in_purcahse_detail_tv);

        dateTextView = (TextView) mainLayout.findViewById(R.id.purchase_date_in_purcahse_detail_tv);
        //supplierTextInputEditText =(TextInputEditText) mainLayout.findViewById(R.id.supplier_name_in_new_purchase_TIET);

        taxAmtTextView = (TextView) mainLayout.findViewById(R.id.tax_amt_in_purchase_detail_tv);

        taxRateTextView = (TextView) mainLayout.findViewById(R.id.tax_rate_tv);

        taxTypeTextView = (TextView) mainLayout.findViewById(R.id.tax_type_tv);

        discountTextView = (TextView) mainLayout.findViewById(R.id.discount_in_purchase_detail_tv);

        subtotalTextView = (TextView) mainLayout.findViewById(R.id.subtotal_in_purchase_detail_tv);

        totalTextView = (TextView) mainLayout.findViewById(R.id.total_in_purchase_detail_tv);


        headerTextView = (TextView) mainLayout.findViewById(R.id.header_text);

        footerTextView = (TextView) mainLayout.findViewById(R.id.footer_text);


        supplierName = purchaseHeader.getSupplierName();
        supplierPhNo = purchaseHeader.getSupplierPhone();
        supplierAddress = purchaseHeader.getSupplierAddress();

        if (supplierName.equalsIgnoreCase(DEFAULT_SUPPLIER)) {
            supplierPhNo = null;
            supplierPhoneLayout.setVisibility(View.GONE);
            supplierAddress = null;
            supplierAddressLayout.setVisibility(View.GONE);
        } else {
            if (supplierPhNo == null || supplierPhNo.equals("")) {
                supplierPhNo = null;
                supplierPhoneLayout.setVisibility(View.GONE);


            }
            if (supplierAddress == null || supplierAddress.equals("")) {
                supplierAddress = null;
                supplierAddressLayout.setVisibility(View.GONE);

            }
        }
    }


    private void updateViewData() {


        calculateValues();


        discountTextView.setText(POSUtil.NumberFormat(purchaseHeader.getDiscountAmt()));
        subtotalTextView.setText(POSUtil.NumberFormat(subtotal));
        totalTextView.setText(POSUtil.NumberFormat(purchaseHeader.getTotal()));

    }

    private void calculateValues() {
        subtotal = 0.0;
        for (SalesAndPurchaseItem s : purchaseItemViewInSaleList) {
            subtotal += s.getTotalPrice();
        }
        //    totalAmount=subtotal+voucherTax+deliveryCharges-voucherDiscount;

    }


    public void replacingFragment(Fragment fragment) {
        final FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.replace(R.id.frame_replace, fragment);
        fragmentTransaction.commit();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.w("ACITVITY", "RESULT");

        bluetoothUtil.onActivityResultBluetooth(requestCode, resultCode, data);

       /* switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect

                if (resultCode == Activity.RESULT_OK) {

                    Log.w("CONE","HERE");
                    // Get the device MAC address
                    String address = data.getExtras()
                            .getString(EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    device = mBluetoothAdapter.getRemoteDevice(address);

                    new ConnectionProgress().execute();

                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    if(device==null||!mmSocket.isConnected()){
                        if(isPrint){
                            Log.w("PRINT","CONNECT AND PRINT");
                            isPrint=false;
                            Intent serverIntent = null;
                            serverIntent = new Intent(getContext(), BluetoothDeviceNearByList.class);
                            startActivityForResult(serverIntent, REQUEST_CONNECT_AND_PRINT_DEVICE);
                        }else {
                            Log.w("PRINT","CONNECT");

                            Intent serverIntent = null;
                            serverIntent = new Intent(getContext(), BluetoothDeviceNearByList.class);
                            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                        }

                    }else {
                        if (mmSocket.isConnected()) {
                            print();
                        } else {
                            //DisplayToast("Make sure bluetooth turn on both devices and choose printer only!!!");
                        }

                    }

                } else {
                    // User did not enable Bluetooth or an error occured
                    Log.d("DD", "BT not enabled");
                    //Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    // finish();
                   // DisplayToast("Bluetooth must be enable to print");
                }
                break;
            case REQUEST_CONNECT_AND_PRINT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getExtras()
                            .getString(EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    device = mBluetoothAdapter.getRemoteDevice(address);

                    new ConnectionAndPrintingProgress().execute();

                }

        }*/

    }

    @Override
    public void onFolderSelection(File folder) {
        path = folder.getAbsolutePath();

        fileNameEditText.setError(null);
        fileNameEditText.setText(null);

        fileNameMaterialDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //recreate();
            //reload my activity with requestPermissions granted or use the features what required the requestPermissions
            showFolderChooserDialog();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            grantPermission.askAgainPermission();
        } else {
            grantPermission.forcePermissionSetting();
            //}
        }
    }

    class SavePDFProgress extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {

            Log.w("HELO", params[0]);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            PrintInvoice printInvoice = new PrintInvoice(getContext(), params[0]);

            // Print_pdf print_pdf=new Print_pdf();
            try {
                //print_pdf.createPDF("TEST",getContext());
                if (headerText == header) {
                    headerText = "";
                }
                if (footerText == "-1") {
                    footerText = "";
                }

                purchaseHeader.setHeaderText(headerText);
                purchaseHeader.setFooterText(footerText);
                printInvoice.createPDF(fileName, purchaseHeader, purchaseItemViewInSaleList, businessSetting);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            Log.w("Saving", "Background");


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            if (!saveHud.isShowing()) {
                saveHud.show();
            }


        }

        @Override
        protected void onPostExecute(String a) {

            if (saveHud.isShowing()) {

                saveHud.dismiss();
                String saveSuccessfully = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save_successfully}).getString(0);

                FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);

            }
            fileOverrideMaterialDialog.dismiss();
        }
    }

}