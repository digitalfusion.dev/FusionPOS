package com.digitalfusion.android.pos.database.business;

import android.content.Context;
import android.util.Log;

import com.digitalfusion.android.pos.database.dao.CategoryDAO;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 10/20/17.
 */

public class CategoryManager {
    private Context context;
    private CategoryDAO categoryDAO;
    private List<Category> categoryList;
    private Long id;

    public CategoryManager(Context context) {
        this.context = context;
        categoryDAO = CategoryDAO.getCategoryDaoInstance(context);
        categoryList = new ArrayList<>();
    }

    public boolean addNewCategory(String name, String description, Long parentId, Integer level) {
        InsertedBooleanHolder isInserted = new InsertedBooleanHolder();
        id = categoryDAO.addNewCategory(name, description, parentId, level, isInserted);
        return isInserted.isInserted();
    }

    public Long addNewCategoryFromStock(String name, String description, Long parentId, int level, InsertedBooleanHolder isInserted) {
        id = categoryDAO.addNewCategory(name, description, parentId, level, isInserted);
        return id;
    }

    public Long addNewCategoryFromStock(String name, String description, Long parentId, int level) {
        Long id = categoryDAO.addNewCategory(name, description, parentId, level, new InsertedBooleanHolder());
        Log.w("Cateid in database", id + " heehe");
        return id;
    }

    public boolean checkNameExist(String cname) {
        id = categoryDAO.checkCategoryAlreadyExist(cname);
        return id != null;
    }

    public Long checkNameExistLong(String cname) {
        id = 0l;
        id = categoryDAO.checkCategoryAlreadyExist(cname);
        if (id == null) {
            return 0l;
        } else {
            return id;
        }
    }

    public List<Category> getAllCategories() {
        categoryList = categoryDAO.getAllCategories();
        return categoryList;
    }


    public boolean updateCategory(String name, String description, Long id) {
        return categoryDAO.updateCategory(name, description, id);
    }

    public int getAllCategoryCounts() {
        return categoryDAO.categoryCounts();
    }

    public List<String> getAllDistinctCategoryNames() {
        return categoryDAO.getAllDistinctCategoryNames();
    }

    public Long findIdByName(String name) {
        return categoryDAO.findIdByName(name);
    }

    public boolean deleteCategory(Long id) {
        return categoryDAO.deleteCategory(id);
    }
}
