package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.fragments.reportfragments.PieChartFragment;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.ColorCircleDrawable;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by hnin on 11/20/16.
 */

public class RVAdapterForPieChart extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int HEADER_TYPE = 10000;
    private List<ReportItem> reportItemList;

    public RVAdapterForPieChart(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pie_chart_report_item_view_header, parent, false);

            return new HeaderItemViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pie_chart_report_item_view, parent, false);

            return new ReportItemViewHolder(v);
        }


    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_TYPE;
        } else {
            return 1;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ReportItemViewHolder) {
            ReportItemViewHolder reportItemViewHolder = (ReportItemViewHolder) holder;
            reportItemViewHolder.nameTextView.setText(reportItemList.get(position - 1).getName());

            reportItemViewHolder.percentTextView.setText(reportItemList.get(position - 1).getPercent() + "%");

            reportItemViewHolder.totalAmtTextView.setText(POSUtil.NumberFormat(reportItemList.get(position - 1).getBalance()));

            if (Build.VERSION.SDK_INT >= 16) {
                reportItemViewHolder.colorView.setBackground(new ColorCircleDrawable(PieChartFragment.colorArr[position - 1]));
            } else {
                reportItemViewHolder.colorView.setBackgroundDrawable(new ColorCircleDrawable(PieChartFragment.colorArr[position - 1]));
            }
        }
    }

    @Override
    public int getItemCount() {
        return reportItemList.size() + 1;
    }

    public List<ReportItem> getReportItemList() {
        return reportItemList;
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public class ReportItemViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView;
        TextView percentTextView;
        ImageView colorView;
        TextView totalAmtTextView;

        View view;

        public ReportItemViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);

            percentTextView = (TextView) itemView.findViewById(R.id.percent_text_view);

            colorView = (ImageView) itemView.findViewById(R.id.color_image_view);

            totalAmtTextView = (TextView) itemView.findViewById(R.id.total_amt_text_view);
        }

    }

    public class HeaderItemViewHolder extends RecyclerView.ViewHolder {
        /*TextView voucherNoLabelTextView;
        TextView percentTextView;
        ImageView colorView;
        TextView dateTextView;

        View view;*/

        public HeaderItemViewHolder(View itemView) {
            super(itemView);

            /*this.view=itemView;

            voucherNoLabelTextView =(TextView)itemView.findViewById(R.id.name_text_view);

            percentTextView = (TextView) itemView.findViewById(R.id.percent_text_view);

            colorView = (ImageView) itemView.findViewById(R.id.color_image_view);

            dateTextView = (TextView) itemView.findViewById(R.id.total_amt_text_view);*/
        }

    }

}
