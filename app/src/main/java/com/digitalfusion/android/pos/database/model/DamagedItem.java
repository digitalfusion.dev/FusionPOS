package com.digitalfusion.android.pos.database.model;

import com.digitalfusion.android.pos.util.POSUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/18/16.
 */
public class DamagedItem implements Serializable {
    private Long id;
    private String stockName;
    private int stockQty;
    private String date;
    private Double totalValue;
    private String remark;
    private Long userID;
    private String userName;
    private String role;
    private String day;
    private String month;
    private String year;
    private Double value;
    private String categoryName;
    private List<StockValue> stockValueList;


    private Long UnitId;
    private String stockUnit;
    private String stockCode;
    private Long stockId;

    public DamagedItem() {
        stockValueList = new ArrayList<>();
    }

    public String getStockUnit() {
        return stockUnit;
    }

    public void setStockUnit(String stockUnit) {
        this.stockUnit = stockUnit;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUnitId() {
        return UnitId;
    }

    public void setUnitId(Long unitId) {
        UnitId = unitId;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public int getStockQty() {
        return stockQty;
    }

    public void setStockQty(int stockQty) {
        this.stockQty = stockQty;
        if (totalValue != null & value != null) {

            totalValue = stockQty * value;

        }
    }

    public String getStockQtyString() {
        return POSUtil.NumberFormat(stockQty);
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDay() {

        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
        if (totalValue != null & value != null) {

            totalValue = stockQty * value;

        }
    }

    public List<StockValue> getStockValueList() {
        return stockValueList;
    }

    public void setStockValueList(List<StockValue> stockValueList) {
        this.stockValueList = stockValueList;
    }

    public String getTotalValueString() {

        Double total = 0.0;

        for (StockValue stockValue : stockValueList) {

            total += stockValue.getValue() * stockValue.getQty();

        }

        return POSUtil.NumberFormat(total);
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
