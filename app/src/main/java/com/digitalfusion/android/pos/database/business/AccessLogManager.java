package com.digitalfusion.android.pos.database.business;

import android.content.Context;
import android.util.Log;

import com.digitalfusion.android.pos.database.dao.AccessLogDAO;
import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.database.model.AccessLog;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.util.AppConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lyx on 5/23/18.
 */
public class AccessLogManager {
    private Context context;
    private AccessLogDAO accessLogDAO;

    public AccessLogManager(Context context) {
        this.context = context;
        accessLogDAO = AccessLogDAO.getInstance(context);
    }

    public Long insertAccessLog(AccessLog accessLog) {
        if (accessLog == null) {
            return null;
        }

        return accessLogDAO.insert(accessLog);
    }

    /**
     * Find and return id of user who is currently logged in to provided device id.
     *
     * @return
     */
    public Long getCurrentlyLoggedInUserId(Long deviceId) {
        Long currentUserId = accessLogDAO.getLastLoggedInUserId(deviceId);
        if (currentUserId == null) {
            return null;
        }

        if (!isUserLoggedIn(currentUserId)) {
            return null;
        }

        return currentUserId;
    }

    public User getCurrentlyLoggedInUser(Long deviceId) {
        Long currentUserId = getCurrentlyLoggedInUserId(deviceId);

        if (currentUserId == null) {
            return null;
        }

        ApiDAO apiDAO = ApiDAO.getApiDAOInstance(context);

        return apiDAO.getUser(getCurrentlyLoggedInUserId(deviceId));
    }

    public Boolean isUserLoggedInWithDevice(Long deviceId, Long userId) {
        AccessLog accessLog = accessLogDAO.getLatestUserAccessLog(userId);
        return accessLog.getDeviceId().equals(deviceId);
    }

    public Boolean isUserLoggedIn(Long userId) {
        AccessLog accessLog = accessLogDAO.getLatestUserAccessLog(userId);
        return accessLog != null && accessLog.getEvent().equalsIgnoreCase(AppConstant.EVENT_IN);
    }


    public List<User> getOnlineUsers() {
        UserManager userManager = new UserManager(context);
        AuthorizationManager authorizationManager = new AuthorizationManager(context);
        List<User> userList = userManager.getAllUserRoles();

        List<User> onlineUserList = new ArrayList<>();
        for (User user : userList) {
            if (isUserLoggedIn(user.getId())) {
                AccessLog accessLog = accessLogDAO.getLatestUserAccessLog(user.getId());
                user.setDeviceSerialNo(authorizationManager.getDeviceSerialNo(accessLog.getDeviceId()));
                onlineUserList.add(user);
            }
        }

        return onlineUserList;
    }
}
