package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD002 on 2/10/17.
 */

public class RVAdapterForProfitAndLossExpMgr extends ParentRVAdapterForReports {

    private final int HEADER_TYPE = 10000;
    protected boolean showLoader = false;
    private List<ReportItem> reportItemList;
    private int viewType;

    /**
     * 1 for stock, category, amount, qty, unit
     * 2 for category, amount
     */
    public RVAdapterForProfitAndLossExpMgr(List<ReportItem> reportItemList) {
        super();
        this.reportItemList = reportItemList;
        Log.w("hello", reportItemList.size() + " ss");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.profit_and_loss_report_item_view, parent, false);

        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        Log.e("vl", "daf");
        if (viewHolder instanceof ViewHolder) {
            ViewHolder reorderViewHolder = (ViewHolder) viewHolder;
            reorderViewHolder.nameTextView.setText(reportItemList.get(position).getName());
            reorderViewHolder.amountTextView.setText(POSUtil.NumberFormat(reportItemList.get(position).getAmount()));
        }
    }

    @Override
    public int getItemCount() {

        return reportItemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_TYPE;
        } else {
            return viewType;
        }
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView;
        TextView amountTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            amountTextView = (TextView) itemView.findViewById(R.id.amount_text_view);

        }
    }

}
