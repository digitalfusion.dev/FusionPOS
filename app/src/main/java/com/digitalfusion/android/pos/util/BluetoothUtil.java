package com.digitalfusion.android.pos.util;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.digitalfusion.android.pos.BluetoothDeviceNearByList;
import com.digitalfusion.android.pos.views.BluetoothDeviceNearByListDialog;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by MD003 on 1/3/17.
 */

public class BluetoothUtil implements BluetoothDeviceNearByListDialog.DeviceConnectListener {

    private static final int REQUEST_CONNECT_DEVICE = 20;
    private static final int REQUEST_ENABLE_BT = 17;
    public static String EXTRA_DEVICE_ADDRESS = "device_address";
    private static BluetoothSocket mmSocket;
    private static BluetoothDevice device;
    private static BluetoothDeviceNearByListDialog nearByListDialog;
    private static BluetoothUtil bluetoothUtil;
    private boolean isDialog;
    private Fragment activity;
    private JobListener bluetoothConnectionListener;
//    public BroadcastReceiver mReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//
//            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
//                Log.w("DEVICE", "FOUND"); //Device found
//            } else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
//                Log.w("DEVICE", "Connceted"); //Device is now connected
//
//            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
//                Log.w("DEVICE", "Searching"); //Done searching
//            } else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
//                Log.w("DEVICE", "Disconnected"); //Device is about to disconnect
//            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
//                Log.w("DEVICE", "Disc"); //Device has disconnected
//                mmSocket = null;
//                if (activity != null && activity.getActivity() != null) {
//                    activity.getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            if (bluetoothConnectionListener != null && activity.isVisible()) {
//                                bluetoothConnectionListener.onDisconnected();
//                            }
//
//                        }
//                    });
//                }
//
//            } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
//                Log.w(BluetoothAdapter.getDefaultAdapter().isEnabled() + "", "Bluetooth Turn off or ON");
//                if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
//                    Log.w("DEVICE", "Bluetooth Turn off"); //Device has disconnected
//                    mmSocket = null;
//
//                    if (activity != null && activity.getActivity() != null) {
//                        activity.getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                if (bluetoothConnectionListener != null && activity.isVisible()) {
//                                    bluetoothConnectionListener.onDisconnected();
//                                }
//                            }
//                        });
//                    }
//                }
//
//            }
//        }
//    };

    private BluetoothAdapter mBluetoothAdapter;

    private BluetoothUtil(@NonNull Fragment activity) {

        this.activity = activity;
        nearByListDialog = new BluetoothDeviceNearByListDialog();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        nearByListDialog.setDeviceConnectListener(this);
        IntentFilter filter1 = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        IntentFilter filter2 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        IntentFilter filter3 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        IntentFilter filter4 = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);


        //        try{
        //
        //            activity.getActivity().registerReceiver(mReceiver, filter1);
        //            activity.getActivity().registerReceiver(mReceiver, filter2);
        //            activity.getActivity().registerReceiver(mReceiver, filter3);
        //            activity.getActivity().registerReceiver(mReceiver, filter4);
        //        }catch (Exception e){
        //
        //        }


    }

    public static BluetoothUtil getInstance(@NonNull Fragment activity) {

        if (bluetoothUtil != null) {
            bluetoothUtil.activity = activity;
            return bluetoothUtil;
        }

        return new BluetoothUtil(activity);
    }

    public OutputStream getOutputStream() throws IOException {
        return mmSocket.getOutputStream();
    }

    public InputStream getInputStream() throws IOException {
        return mmSocket.getInputStream();
    }

    public void createConnection(boolean dialogMode) {

        isDialog = dialogMode;

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (dialogMode) {
                nearByListDialog.show(activity.getFragmentManager(), "dg");
            } else {

                Intent serverIntent = null;
                serverIntent = new Intent(activity.getContext(), BluetoothDeviceNearByList.class);
                //startActivity(serverIntent);
                activity.startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            }
        }
    }


    public void onActivityResultBluetooth(int requestCode, int resultCode, Intent data) {
        Log.w("herer", "blue result");
        switch (requestCode) {

            case REQUEST_CONNECT_DEVICE:

                // When DeviceListActivity returns with a device to connect

                if (resultCode == Activity.RESULT_OK) {

                    Log.w("CONE", "HERE");
                    // Get the device MAC address
                    String address = data.getExtras()
                            .getString(EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    device = mBluetoothAdapter.getRemoteDevice(address);

                    new ConnectionProgress().execute();

                }
                break;
            case REQUEST_ENABLE_BT:

                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {

                    if (isDialog) {

                        nearByListDialog.show(activity.getFragmentManager(), "dg");

                    } else {
                        // Bluetooth is now enabled, so set up a chat session

                        Intent serverIntent = null;
                        serverIntent = new Intent(activity.getContext(), BluetoothDeviceNearByList.class);
                        activity.startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);


                    }

                }
                break;

        }

    }


    private synchronized void connectBluetoothSocket() throws IOException {

        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
        if (mmSocket != null) {

            mmSocket.close();

        }


        Log.w("Make", "new Socket");
        mmSocket = device.createRfcommSocketToServiceRecord(uuid);

        if (!mmSocket.isConnected()) {
            mmSocket.connect();
        }

    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public boolean isConnected() {
        if (mmSocket != null) {

            return mmSocket.isConnected();

        } else {
            return false;
        }
    }

    public void setBluetoothConnectionListener(JobListener bluetoothConnectionListener) {
        this.bluetoothConnectionListener = bluetoothConnectionListener;
    }

    @Override
    public void onDeviceConnect(String address) {
        // Get the BLuetoothDevice object
        device = mBluetoothAdapter.getRemoteDevice(address);

        new ConnectionProgress().execute();

        if (nearByListDialog.isVisible()) {
            nearByListDialog.dismiss();
        }
    }

    public interface JobListener {
        void onPrepare();

        void onSuccess();

        void onFailure();

        void onDisconnected();

    }

    private class ConnectionProgress extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {

            try {
                connectBluetoothSocket();

                if (device != null) {
                    if (activity != null && activity.getActivity() != null) {
                        activity.getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

//                                if (bluetoothConnectionListener != null && activity.isVisible()) {
//                                    bluetoothConnectionListener.onSuccess();
//                                }
                                if (bluetoothConnectionListener != null) {
                                    bluetoothConnectionListener.onSuccess();
                                }
                            }
                        });
                    }
                } else {

                    if (activity != null && activity.getActivity() != null) {
                        activity.getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (bluetoothConnectionListener != null) {
                                    bluetoothConnectionListener.onFailure();
                                    closeSocket();
                                }
                            }
                        });
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();

                Log.w("here", "exvception");
                if (activity != null && activity.getActivity() != null) {
                    activity.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (bluetoothConnectionListener != null) {
                                bluetoothConnectionListener.onFailure();
                                closeSocket();
                            }

                        }
                    });
                }

            }


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (activity != null && activity.getActivity() != null) {
                activity.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (bluetoothConnectionListener != null) {
                            bluetoothConnectionListener.onPrepare();
                        }
                    }
                });
            }
        }

        @Override
        protected void onPostExecute(String a) { }
    }

    private void closeSocket() {
        try {
            mmSocket.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        mmSocket = null;
    }
}
