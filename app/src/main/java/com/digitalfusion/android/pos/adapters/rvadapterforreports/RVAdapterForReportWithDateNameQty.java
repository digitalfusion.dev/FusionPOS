package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;

import java.util.List;

/**
 * Created by MD002 on 12/3/16.
 */

public class RVAdapterForReportWithDateNameQty extends ParentRVAdapterForReports {
    private static final int VIEWTYPE_ITEM = 2;
    private static final int VIEWTYPE_LOADER = 3;
    private final int HEADER_TYPE = 10000;
    private List<ReportItem> reportItemList;


    public RVAdapterForReportWithDateNameQty(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_date_name_qty_header, parent, false);
            Log.e("header", reportItemList.size() + " dkfl");

            return new HeaderViewHolder(v);
        } else if (viewType == VIEWTYPE_LOADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);
            Log.e("loader", reportItemList.size() + " dkfl");
            return new LoaderViewHolder(v);
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_date_name_qty, parent, false);
        Log.e("holder", reportItemList.size() + " dkfl");

        return new ReportItemViewHolder(v);


    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof LoaderViewHolder) {
            Log.e("loader", "loader");

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) holder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);

        } else if (holder instanceof ReportItemViewHolder) {

            ReportItemViewHolder mholder = (ReportItemViewHolder) holder;

            mholder.nameTextView.setText(reportItemList.get(position - 1).getName());

            mholder.qtyTextView.setText(Integer.toString(reportItemList.get(position - 1).getQty()));

            mholder.dateTextView.setText(reportItemList.get(position - 1).getDate());
        } else {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            if (!reportItemList.isEmpty()) {
                headerViewHolder.noDataTextView.setVisibility(View.GONE);
                headerViewHolder.headerLayout.setVisibility(View.VISIBLE);
                headerViewHolder.line.setVisibility(View.VISIBLE);
            } else {
                headerViewHolder.noDataTextView.setVisibility(View.VISIBLE);
                headerViewHolder.headerLayout.setVisibility(View.GONE);
                headerViewHolder.line.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position != 0 && position == getItemCount() - 1) {

            if (reportItemList != null) {

                return VIEWTYPE_LOADER;

            }

        }
        if (position == 0) {
            return HEADER_TYPE;
        }
        return VIEWTYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return reportItemList.size() + 2;
    }

    public List<ReportItem> getReportItemList() {
        return reportItemList;
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public class ReportItemViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView;
        TextView qtyTextView;
        TextView dateTextView;
        View view;

        public ReportItemViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);

            qtyTextView = (TextView) itemView.findViewById(R.id.qty_text_view);

            dateTextView = (TextView) itemView.findViewById(R.id.date_text_view);
        }

    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {


        View view;
        LinearLayout headerLayout;
        TextView noDataTextView;
        View line;

        public HeaderViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            headerLayout = (LinearLayout) itemView.findViewById(R.id.header_layout);
            noDataTextView = (TextView) itemView.findViewById(R.id.no_data_text_view);
            line = itemView.findViewById(R.id.line);
        }

    }

}
