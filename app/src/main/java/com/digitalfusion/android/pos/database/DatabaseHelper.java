package com.digitalfusion.android.pos.database;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteTransactionListener;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.model.Currency;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by MD002 on 8/17/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    //private String a;
    private static final int DATABASE_VERSION = 2;
    private static final String DB_PATH_SUFFIX = "/databases/";
    private static final String DB_PATH = "/mnt/sdcard/android/data/";
    private static Context context;
    private static DatabaseHelper helperInstance;
    private SQLiteDatabase database;
    private Cursor cursor;
    private Long id;
    private List<Long> idList;

    private DatabaseHelper(Context context) {
        super(context, AppConstant.SHARED_PREFERENCE_DB, null, DATABASE_VERSION);
        // Log.e("create", "dhelp");
        //super(context, DB_PATH + context.getPackageName() + "/" + AppConstant.DATABASE_NAME, null, DATABASE_VERSION);
        // Log.e("dab", AppConstant.SHARED_PREFERENCE_DB);
        //super(context, "SampleDB", null, 1);
        DatabaseHelper.context = context;

    }

    private DatabaseHelper(Context context, String dbName) {
        super(context, dbName, null, DATABASE_VERSION);
        Log.e("dir", dbName);
        Log.e("db", "open");
        DatabaseHelper.context = context;
       /* database = getWritableDatabase();
        database.execSQL(" drop trigger BeforeDeleteCustomerChangeFKsToDefault");
        database.execSQL("CREATE TRIGGER BeforeDeleteCustomerChangeFKsToDefault before delete on Customer when old.id <> 1 \n" +
                "begin  \n" +
                "update Sales set customerID = 1 where customerID = old.id; \n" +
                "end");*/
    }

    public static synchronized DatabaseHelper getHelperInstance(Context context) {
        //Log.e("helper","instance" + helperInstance);
        if (helperInstance == null) {
            helperInstance = new DatabaseHelper(context.getApplicationContext());
        }


        //        Log.e("fjaoifj", "path" + context.getDatabasePath(DATABASE_NAME).getParent());
        return helperInstance;
    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys = ON;");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.database = db;
        if (oldVersion < 2) {
            database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.PURCHASE_HOLD_TABLE_NAME + "(" + AppConstant.PURCHASE_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.PURCHASE_VOUCHER_NUM
                    + " TEXT, " + AppConstant.PURCHASE_DATE + " TEXT, " + AppConstant.PURCHASE_SUPPLIER_ID + " INTEGER, " + AppConstant.PURCHASE_TOTAL_AMOUNT + " REAL, " + AppConstant.PURCHASE_DISCOUNT + " TEXT, " + AppConstant.PURCHASE_DISCOUNT_ID + " INTEGER, " +
                    AppConstant.PURCHASE_TAX_ID + " INTEGER, " + AppConstant.PURCHASE_SUB_TOTAL + " REAL, " + AppConstant.PURCHASE_DISCOUNT_AMOUNT + " REAL, "
                    + AppConstant.PURCHASE_REMARK + " TEXT, " + AppConstant.PURCHASE_PAID_AMOUNT + " REAL, " + AppConstant.PURCHASE_BALANCE + " REAL, " + AppConstant.PURCHASE_TAX_AMOUNT + " REAL, " +
                    AppConstant.CREATED_DATE + " TEXT, " + AppConstant.PURCHASE_DAY + " TEXT, " + AppConstant.PURCHASE_MONTH + " TEXT, " + AppConstant.PURCHASE_YEAR
                    + " TEXT, " + AppConstant.PURCHASE_TAX_RATE + " REAL, " + AppConstant.PURCHASE_TAX_TYPE + " TEXT, " +
                    AppConstant.PURCHASE_TIME + " INTEGER, " + AppConstant.CURRENT_USER_ID + " TEXT, " + AppConstant.PURCHASE_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.PURCHASE_CUSTOM_FIELD_3 + " TEXT, "
                    + AppConstant.PURCHASE_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.PURCHASE_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.PURCHASE_SUPPLIER_ID +
                    ") REFERENCES " + AppConstant.SUPPLIER_TABLE_NAME + "(" + AppConstant.SUPPLIER_ID + "), FOREIGN KEY (" + AppConstant.PURCHASE_TAX_ID + ") REFERENCES " + AppConstant.TAX_TABLE_NAME +
                    "(" + AppConstant.TAX_ID + "), FOREIGN KEY(" + AppConstant.PURCHASE_DISCOUNT_ID + ") REFERENCES " + AppConstant.DISCOUNT_TABLE_NAME + "(" + AppConstant.DISCOUNT_ID + "));");

            database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.PURCHASE_HOLD_DETAIL_TABLE_NAME + "(" + AppConstant.PURCHASE_DETAIL_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID +
                    " INTEGER, " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " INTEGER, " + AppConstant.PURCHASE_DETAIL_QTY + " INTEGER, " + AppConstant.PURCHASE_DETAIL_PRICE + " REAL, " + AppConstant.PURCHASE_DETAIL_DISCOUNT
                    + " TEXT, " + AppConstant.PURCHASE_DETAIL_TAX_ID + " INTEGER, " + AppConstant.PURCHASE_DETAIL_TOTAL + " REAL, " + AppConstant.PURCHASE_DETAIL_TAX_AMT + " REAL, " + AppConstant.PURCHASE_DETAIL_DISCOUNT_AMT + " REAL, "
                    + AppConstant.PURCHASE_DETAIL_TAX_RATE + " REAL, " + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.PURCHASE_DETAIL_TAX_TYPE + " TEXT, " + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_1
                    + " TEXT," + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_2 + " TEXT," + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_3 + " TEXT," + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_4 + " TEXT,"
                    + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.PURCHASE_DETAIL_STOCK_ID + ") REFERENCES " + AppConstant.STOCK_TABLE_NAME + "(" + AppConstant.STOCK_ID + "), FOREIGN KEY (" +
                    AppConstant.PURCHASE_DETAIL_PURCHASE_ID + ") REFERENCES " + AppConstant.PURCHASE_HOLD_TABLE_NAME + "(" + AppConstant.PURCHASE_ID + "), FOREIGN KEY(" + AppConstant.PURCHASE_DETAIL_TAX_ID + ") REFERENCES "
                    + AppConstant.TAX_TABLE_NAME + "( " + AppConstant.TAX_ID + "));");
            database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.SALES_HOLD_TABLE_NAME + "(" + AppConstant.SALES_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.SALES_VOUCHER_NUM
                    + " TEXT, " + AppConstant.SALES_DATE + " TEXT, " + AppConstant.SALES_CUSTOMER_ID + " INTEGER, " + AppConstant.SALES_TOTAL_AMOUNT + " REAL, " + AppConstant.SALES_DISCOUNT + " TEXT, " + AppConstant.SALES_DISCOUNT_ID + " INTEGER, " +
                    AppConstant.SALES_TAX_ID + " INTEGER, " + AppConstant.SALES_SUB_TOTAL + " REAL, " + AppConstant.SALES_TYPE + " TEXT, " + AppConstant.SALES_DISCOUNT_AMOUNT + " REAL, "
                    + AppConstant.SALES_REMARK + " TEXT, " + AppConstant.SALES_PAID_AMOUNT + " REAL, " + AppConstant.SALES_BALANCE + " REAL, " + AppConstant.SALES_TAX_AMOUNT + " REAL, " +
                    AppConstant.CREATED_DATE + " TEXT, " + AppConstant.SALES_DAY + " TEXT, " + AppConstant.SALES_MONTH + " TEXT, " + AppConstant.SALES_YEAR + " TEXT, " + AppConstant.SALES_TAX_RATE +
                    " REAL, " + AppConstant.SALES_TIME + " INTEGER, " + AppConstant.SALES_TAX_TYPE + " TEXT, " +
                    AppConstant.SALES_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.SALES_USER_ID + " TEXT, " + AppConstant.SALES_CHANGE_CASH + " TEXT, "
                    + AppConstant.SALES_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.SALES_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.SALES_CUSTOMER_ID +
                    ") REFERENCES " + AppConstant.CUSTOMER_TABLE_NAME + "(" + AppConstant.CUSTOMER_ID + "), FOREIGN KEY (" + AppConstant.SALES_TAX_ID + ") REFERENCES " + AppConstant.TAX_TABLE_NAME +
                    "(" + AppConstant.TAX_ID + "), FOREIGN KEY(" + AppConstant.SALES_DISCOUNT_ID + ") REFERENCES " + AppConstant.DISCOUNT_TABLE_NAME + "(" + AppConstant.DISCOUNT_ID + "));");

            database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.SALES_HOLD_DETAIL_TABLE_NAME + "(" + AppConstant.SALES_DETAIL_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.SALES_DETAIL_SALES_ID +
                    " INTEGER, " + AppConstant.SALES_DETAIL_STOCK_ID + " INTEGER NOT NULL, " + AppConstant.SALES_DETAIL_QTY + " INTEGER, " + AppConstant.SALES_DETAIL_PRICE + " REAL, " + AppConstant.SALES_DETAIL_DISCOUNT
                    + " TEXT, " + AppConstant.SALES_DETAIL_TAX_ID + " INTEGER, " + AppConstant.SALES_DETAIL_TOTAL + " REAL, " + AppConstant.SALES_DETAIL_TAX_AMOUNT + " REAL, " + AppConstant.SALES_DETAIL_DISCOUNT_AMT + " REAL, "
                    + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.SALES_DETAIL_TAX_RATE + " REAL, " + AppConstant.SALES_DETAIL_TAX_TYPE + " TEXT, " + AppConstant.SALES_DETAIL_CUSTOM_FIELD_1
                    + " TEXT," + AppConstant.SALES_DETAIL_CUSTOM_FIELD_2 + " TEXT," + AppConstant.SALES_DETAIL_CUSTOM_FIELD_3 + " TEXT," + AppConstant.SALES_DETAIL_CUSTOM_FIELD_4 + " TEXT,"
                    + AppConstant.SALES_DETAIL_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.SALES_DETAIL_STOCK_ID + ") REFERENCES " + AppConstant.STOCK_TABLE_NAME + "(" + AppConstant.STOCK_ID + "), FOREIGN KEY (" +
                    AppConstant.SALES_DETAIL_SALES_ID + ") REFERENCES " + AppConstant.SALES_HOLD_TABLE_NAME + "(" + AppConstant.SALES_ID + "));");

            database.execSQL("create trigger if not exists AfterInsertHoldSalesUpdateIdGen after insert on " + AppConstant.SALES_HOLD_TABLE_NAME + " begin " +
                    " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                    AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Hold';" + " end");

            database.execSQL("create trigger if not exists AfterInsertHoldSalesDetailUpdateIdGen after insert on " + AppConstant.SALES_HOLD_DETAIL_TABLE_NAME + " begin " +
                    " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                    AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'SaleHoldDetails';" + " end");
            database.execSQL("create trigger if not exists AfterInsertPurchaseHoldUpdateIdGen after insert on " + AppConstant.PURCHASE_HOLD_TABLE_NAME + " begin " +
                    " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                    AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'PurchaseHold';" + " end");

            database.execSQL("create trigger if not exists AfterInsertPurchaseHoldDetailUpdateIdGen after insert on " + AppConstant.PURCHASE_HOLD_DETAIL_TABLE_NAME + " begin " +
                    " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                    AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'PurchaseHoldDetail';" + " end");
            addNewID(AppConstant.SALES_HOLD_TABLE_NAME, 0);
            addNewID(AppConstant.SALES_HOLD_DETAIL_TABLE_NAME, 0);
            addNewID(AppConstant.PURCHASE_HOLD_TABLE_NAME, 0);
            addNewID(AppConstant.PURCHASE_HOLD_DETAIL_TABLE_NAME, 0);
        }
    }

    public synchronized void close() {
        if (database != null)
            database.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.database = db;
        Log.e("db on create", ":D");
        createTables();

        createTriggers();

        initializeToIdGenerator();

        createSampleDatabase();

        // AppConstant.SHARED_PREFERENCE_DB = AppConstant.SAMPLE_DATABASE_NAME;

        helperInstance = null;
    }

    public void createAfterClear() {

    }

    private void createTables() {
        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.ID_GENERATOR_TABLE_NAME + "(" + AppConstant.ID_GENERATOR_ID
                + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " TEXT, " +
                AppConstant.ID_GENERATOR_VALUE + " INTEGER, " + AppConstant.CREATED_DATE + " TEXT);");


        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.CATEGORY_TABLE_NAME + "(" + AppConstant.CATEGORY_ID + " INTEGER NOT NULL PRIMARY KEY, "
                + AppConstant.CATEGORY_NAME + " TEXT, " + AppConstant.CATEGORY_DESCRIPTION + " TEXT, " + AppConstant.CATEGORY_PARENT_ID +
                " INTEGER, " + AppConstant.CATEGORY_LEVEL + " INTEGER, " + AppConstant.CATEGORY_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.CATEGORY_CUSTOM_FIELD_2 + " TEXT, "
                + AppConstant.CATEGORY_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.CATEGORY_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.CATEGORY_CUSTOM_FIELD_5 + " TEXT, " +
                AppConstant.CREATED_DATE + " TEXT);");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.UNIT_TABLE_NAME + "(" + AppConstant.UNIT_ID + " INTEGER NOT NULL PRIMARY KEY, "
                + AppConstant.UNIT_UNIT + " TEXT, " + AppConstant.UNIT_DESCRIPTION + " TEXT, " + AppConstant.UNIT_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.UNIT_CUSTOM_FIELD_2 + " TEXT, "
                + AppConstant.UNIT_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.UNIT_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.UNIT_CUSTOM_FIELD_5 + " TEXT, " + AppConstant.CREATED_DATE +
                " TEXT);");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.STOCK_TABLE_NAME + "(" + AppConstant.STOCK_ID + " INTEGER NOT NULL PRIMARY KEY , "
                + AppConstant.STOCK_CODE_NUM + " TEXT, " + AppConstant.STOCK_BARCODE + " TEXT, " + AppConstant.STOCK_NAME + " TEXT, " + AppConstant.STOCK_CATEGORY_ID
                + " INTEGER, " + AppConstant.STOCK_UNIT_ID + " INTEGER, " + AppConstant.STOCK_INVENTORY_QTY + " INTEGER, " + AppConstant.STOCK_REORDER_QTY +
                " INTEGER, " + AppConstant.STOCK_PURCHASE_PRICE + " REAL DEFAULT 0.0, " + AppConstant.STOCK_WHOLESALE_PRICE + " REAL DEFAULT 0.0, " + AppConstant.STOCK_NORMAL_PRICE +
                " REAL DEFAULT 0.0, " + AppConstant.STOCK_RETAIL_PRICE + " REAL DEFAULT 0.0, " + //+ AppConstant.STOCK_IMAGE + " BLOB, " +
                AppConstant.STOCK_IS_DELETED + " INTEGER DEFAULT 0, " + AppConstant.STOCK_DESCRIPTION + " TEXT, " + AppConstant.STOCK_OPENING_DATE + " TEXT, " + AppConstant.STOCK_OPENING_QTY + " INTEGER, "
                + AppConstant.STOCK_OPENING_PRICE + " REAL DEFAULT 0.0, " + AppConstant.STOCK_OPENING_TIME + " INTEGER, "
                //+ AppConstant.STOCK_BRAND + " TEXT, " + AppConstant.STOCK_SIZE + " TEXT, " + AppConstant.STOCK_COLOR + "TEXT, "
                + AppConstant.STOCK_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.STOCK_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.STOCK_CUSTOM_FIELD_3 + " TEXT, " +
                AppConstant.STOCK_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.STOCK_CUSTOM_FIELD_5 + " TEXT, " + AppConstant.CREATED_DATE + " TEXT, FOREIGN KEY(" +
                AppConstant.STOCK_CATEGORY_ID + ") REFERENCES " + AppConstant.CATEGORY_TABLE_NAME + "(" + AppConstant.CATEGORY_ID + "), FOREIGN KEY(" +
                AppConstant.STOCK_UNIT_ID + ") REFERENCES " + AppConstant.UNIT_TABLE_NAME + "(" + AppConstant.UNIT_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.ADJUSTMENT_TABLE_NAME + "(" + AppConstant.ADJUSTMENT_STOCK_ID + " INTEGER, " + AppConstant.ADJUSTMENT_DATE + " TEXT, " +
                AppConstant.ADJUSTMENT_TIME + " INTEGER, " + AppConstant.ADJUSTMENT_QTY + " INTEGER, " + AppConstant.ADJUSTMENT_PRICE + " REAL, " + AppConstant.ADJUSTMENT_DAY + " TEXT, " +
                AppConstant.ADJUSTMENT_MONTH + " TEXT, " + AppConstant.ADJUSTMENT_YEAR +
                " TEXT, FOREIGN KEY (" + AppConstant.ADJUSTMENT_STOCK_ID + ") REFERENCES " + AppConstant.STOCK_TABLE_NAME
                + "(" + AppConstant.STOCK_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.STOCK_IMAGE_TABLE_NAME + "(" + AppConstant.STOCK_IMAGE_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.STOCK_IMAGE_STOCK_ID +
                " INTEGER, " + AppConstant.STOCK_IMAGE_IMG + " BLOB, FOREIGN KEY(" + AppConstant.STOCK_IMAGE_STOCK_ID + ") REFERENCES " + AppConstant.STOCK_TABLE_NAME + "(" + AppConstant.STOCK_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.USER_TABLE_NAME + "(" + AppConstant.USER_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.USER_USER_NAME +
                " TEXT, " + AppConstant.USER_PASSWORD + " TEXT, " + AppConstant.USER_ROLE + " TEXT, " + AppConstant.USER_DESCRIPTION + " TEXT, " + AppConstant.USER_IS_DELETED + " INTEGER DEFAULT 0, " + AppConstant.USER_CUSTOM_FIELD_1 + " TEXT, " +
                AppConstant.USER_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.USER_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.USER_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.USER_CUSTOM_FIELD_5
                + " TEXT, " + AppConstant.CREATED_DATE + " TEXT);");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.DAMAGE_TABLE_NAME + "(" + AppConstant.DAMAGE_ID + " INTEGER NOT NULL PRIMARY KEY, " +// AppConstant.DAMAGE_INVOICE_NUM +
                AppConstant.DAMAGE_DATE + " TEXT, " +// AppConstant.DAMAGE_APPROVE_BY + " TEXT, " + AppConstant.DAMAGE_TOTAL_VALUE + " REAL, " +
                AppConstant.DAMAGE_REMARK + " TEXT, " +
                AppConstant.DAMAGE_USER_ID + " INTEGER, " + AppConstant.DAMAGE_DAY + " TEXT, " + AppConstant.DAMAGE_MONTH + " TEXT, " + AppConstant.DAMAGE_YEAR + " TEXT, " +
                AppConstant.DAMAGE_STOCK_ID + " INTEGER, " + AppConstant.DAMAGE_STOCK_QTY + " INTEGER, " + AppConstant.DAMAGE_TIME + " INTEGER, "
                + AppConstant.DAMAGE_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.DAMAGE_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.DAMAGE_CUSTOM_FIELD_3 + " TEXT, " +
                AppConstant.DAMAGE_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.DAMAGE_CUSTOM_FIELD_5 + " TEXT, " + AppConstant.CREATED_DATE + " TEXT, FOREIGN KEY (" + AppConstant.DAMAGE_USER_ID
                + ") REFERENCES " + AppConstant.USER_TABLE_NAME + "(" + AppConstant.USER_ID + "), FOREIGN KEY(" + AppConstant.DAMAGE_STOCK_ID + ") REFERENCES " + AppConstant.STOCK_TABLE_NAME
                + "(" + AppConstant.STOCK_ID + "));");

        /*database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.DAMAGE_DETAIL_TABLE_NAME + "(" + AppConstant.DAMAGE_DETAIL_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.DAMAGE_DETAIL_DAMAGE_ID +
                " INTEGER, " + AppConstant.DAMAGE_DETAIL_STOCK_ID + " INTEGER, " + AppConstant.DAMAGE_DETAIL_QTY + " INTEGER, " + AppConstant.DAMAGE_DETAIL_PURCHASE_PRICE + " REAL, " +
                AppConstant.DAMAGE_DETAIL_TOTAL + " REAL, "
                //+ AppConstant.DAMAGE_DETAIL_UNIT_ID + " INTEGER, "
                + AppConstant.DAMAGE_DETAIL_REMARK + " TEXT, " + AppConstant.DAMAGE_DETAIL_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.DAMAGE_DETAIL_CUSTOM_FIELD_2
                + " TEXT, " + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.DAMAGE_DETAIL_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.DAMAGE_DETAIL_CUSTOM_FIELD_4 + " TEXT, "
                + AppConstant.DAMAGE_DETAIL_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY( " + AppConstant.DAMAGE_DETAIL_DAMAGE_ID +
                ") REFERENCES " + AppConstant.DAMAGE_TABLE_NAME + "(" + AppConstant.DAMAGE_ID + "), FOREIGN KEY(" + AppConstant.DAMAGE_DETAIL_STOCK_ID + ") REFERENCES " + AppConstant.STOCK_TABLE_NAME + "(" +
                AppConstant.STOCK_ID + /*"), FOREIGN KEY (" + AppConstant.DAMAGE_DETAIL_UNIT_ID + ") REFERENCES " + AppConstant.UNIT_TABLE_NAME + "(" + AppConstant.UNIT_ID +*/ //"));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.LOST_TABLE_NAME + "(" + AppConstant.LOST_ID + " INTEGER NOT NULL PRIMARY KEY, " + //AppConstant.LOST_INVOICE_NUM + AppConstant.LOST_APPROVE_BY + " TEXT, " +
                AppConstant.LOST_DATE + " TEXT, " +// AppConstant.LOST_TOTAL_VALUE + " REAL, " +
                AppConstant.LOST_REMARK + " TEXT, " +
                AppConstant.LOST_USER_ID + " INTEGER, " + AppConstant.LOST_DAY + " TEXT, " + AppConstant.LOST_MONTH + " TEXT, " + AppConstant.LOST_YEAR + " TEXT, " + AppConstant.LOST_STOCK_ID + " INTEGER, " +
                AppConstant.LOST_STOCK_QTY + " INTEGER, " + AppConstant.LOST_TIME + " INTEGER, " + AppConstant.LOST_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.LOST_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.LOST_CUSTOM_FIELD_3 + " TEXT, " +
                AppConstant.LOST_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.LOST_CUSTOM_FIELD_5 + " TEXT, " + AppConstant.CREATED_DATE + " TEXT, FOREIGN KEY (" +
                AppConstant.LOST_USER_ID + ") REFERENCES " + AppConstant.USER_TABLE_NAME + "(" + AppConstant.USER_ID + ")FOREIGN KEY(" + AppConstant.LOST_STOCK_ID + ") REFERENCES " + AppConstant.STOCK_TABLE_NAME
                + "(" + AppConstant.STOCK_ID + "));");

        /*database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.LOST_DETAIL_TABLE_NAME + "(" + AppConstant.LOST_DETAIL_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.LOST_DETAIL_LOST_ID +
                " INTEGER, " + AppConstant.LOST_DETAIL_STOCK_ID + " INTEGER, " + AppConstant.LOST_DETAIL_QTY + " INTEGER, " + AppConstant.LOST_DETAIL_PURCHASE_PRICE + " REAL, " +
                AppConstant.LOST_DETAIL_TOTAL + " REAL, "+ AppConstant.LOST_DETAIL_REMARK + " TEXT, " + AppConstant.LOST_DETAIL_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.LOST_DETAIL_CUSTOM_FIELD_2
                + " TEXT, " + AppConstant.LOST_DETAIL_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.LOST_DETAIL_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.LOST_DETAIL_CUSTOM_FIELD_5 + " TEXT, " + AppConstant.CREATED_DATE +
                " TEXT, FOREIGN KEY( " + AppConstant.LOST_DETAIL_LOST_ID + ") REFERENCES " + AppConstant.LOST_TABLE_NAME + "(" + AppConstant.LOST_ID + "), FOREIGN KEY(" +
                AppConstant.LOST_DETAIL_STOCK_ID + ") REFERENCES " + AppConstant.STOCK_TABLE_NAME + "(" +
                AppConstant.STOCK_ID + /*"), FOREIGN KEY (" + AppConstant.LOST_DETAIL_UNIT_ID + ") REFERENCES " + AppConstant.UNIT_TABLE_NAME + "(" + AppConstant.UNIT_ID + "));");*/

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.MENU_TABLE_NAME + "(" + AppConstant.MENU_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.MENU_NAME
                + " TEXT, " + AppConstant.MENU_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.MENU_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.MENU_CUSTOM_FIELD_3 + " TEXT, "
                + AppConstant.MENU_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.MENU_CUSTOM_FIELD_5 + " TEXT, " + AppConstant.CREATED_DATE + " TEXT); ");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.DOCUMENT_NUMBER_SETTING_TABLE_NAME + "(" + AppConstant.DOCUMENT_NUMBER_SETTING_ID + " INTEGER NOT NULL PRIMARY KEY, "
                + AppConstant.DOCUMENT_NUMBER_SETTING_MENU_ID + " INTEGER, "
                //+ AppConstant.DOCUMENT_NUMBER_SETTING_DATE_STATUS + " TEXT, "
                + AppConstant.DOCUMENT_NUMBER_SETTING_PREFIX + " TEXT, " + AppConstant.DOCUMENT_NUMBER_SETTING_MIN_DIGIT + " INTEGER, " + AppConstant.DOCUMENT_NUMBER_SETTING_SUFFIX
                + " TEXT, " + AppConstant.DOCUMENT_NUMBER_SETTING_NEXT_NUMBER + " INTEGER, " + AppConstant.DOCUMENT_NUMBER_SETTING_CUSTOM_FIELD_1 + " TEXT, "
                + AppConstant.DOCUMENT_NUMBER_SETTING_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.DOCUMENT_NUMBER_SETTING_CUSTOM_FIELD_3 + " TEXT, "
                + AppConstant.DOCUMENT_NUMBER_SETTING_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.DOCUMENT_NUMBER_SETTING_CUSTOM_FIELD_5 + " TEXT, " + AppConstant.CREATED_DATE +
                " TEXT, FOREIGN KEY (" + AppConstant.DOCUMENT_NUMBER_SETTING_MENU_ID + ") REFERENCES " + AppConstant.MENU_TABLE_NAME + "(" + AppConstant.MENU_ID + ")) ");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.TAX_TABLE_NAME + "(" + AppConstant.TAX_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.TAX_NAME + " TEXT, " +
                AppConstant.TAX_RATE + " REAL, " + AppConstant.TAX_TYPE + " TEXT, " + AppConstant.TAX_DESCRIPTION + " TEXT, " + AppConstant.TAX_IS_DEFAULT + " INTEGER, " + AppConstant.TAX_CUSTOM_FIELD_1
                + " TEXT, " + AppConstant.TAX_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.TAX_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.TAX_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.TAX_CUSTOM_FIELD_5
                + " TEXT, " + AppConstant.CREATED_DATE + " TEXT);");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.DISCOUNT_TABLE_NAME + "(" + AppConstant.DISCOUNT_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.DISCOUNT_NAME + " TEXT, " +
                AppConstant.DISCOUNT_AMOUNT + " REAL, " + AppConstant.DISCOUNT_IS_PERCENT + " Integer, " + AppConstant.DISCOUNT_DESCRIPTION + " TEXT, " + AppConstant.DISCOUNT_IS_DEFAULT + " INTEGER, " + AppConstant.DISCOUNT_CUSTOM_FIELD_1
                + " TEXT, " + AppConstant.DISCOUNT_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.DISCOUNT_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.DISCOUNT_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.DISCOUNT_CUSTOM_FIELD_5
                + " TEXT, " + AppConstant.CREATED_DATE + " TEXT);");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.CURRENCY_TABLE_NAME + "(" + AppConstant.CURRENCY_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.CURRENCY_NAME +
                " TEXT, " + AppConstant.CURRENCY_SYMBOL + " TEXT, " + AppConstant.CURRENCY_DESCRIPTION + " TEXT, " + AppConstant.CURRENCY_IS_DEFAULT + " INTEGER, " + AppConstant.CURRENCY_CUSTOM_FIELD_1 +
                " TEXT, " + AppConstant.CURRENCY_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.CURRENCY_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.CURRENCY_CUSTOM_FIELD_4 + " TEXT, " +
                AppConstant.CURRENCY_CUSTOM_FIELD_5 + " TEXT);");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.BUSINESS_SETTING_TABLE_NAME + "(" + AppConstant.BUSINESS_SETTING_ID + " INTEGER NOT NULL PRIMARY KEY, " +
                AppConstant.BUSINESS_SETTING_BUSINESS_NAME + " TEXT, " + AppConstant.BUSINESS_SETTING_PHONE_NUM + " TEXT, " + AppConstant.BUSINESS_SETTING_EMAIL + " TEXT, " +
                AppConstant.BUSINESS_SETTING_WEBSITE + " TEXT, " + AppConstant.BUSINESS_SETTING_STREET + " TEXT, " + AppConstant.BUSINESS_SETTING_CITY + " TEXT, " +
                AppConstant.BUSINESS_SETTING_STATE + " TEXT, " + AppConstant.BUSINESS_SETTING_TOWNSHIP + " TEXT, " + AppConstant.BUSINESS_SETTING_VALUATION_METHOD + " TEXT, " +
                AppConstant.BUSINESS_SETTING_CURRENCY_ID + " INTEGER, " + AppConstant.BUSINESS_SETTING_LOGO + " BLOB, " + AppConstant.BUSINESS_SETTING_CUSTOM_FIELD_1 + " TEXT, "
                + AppConstant.BUSINESS_SETTING_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.BUSINESS_SETTING_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.BUSINESS_SETTING_CUSTOM_FIELD_4 +
                " TEXT, " + AppConstant.BUSINESS_SETTING_CUSTOM_FIELD_5 + " TEXT, " + AppConstant.CREATED_DATE + " TEXT, FOREIGN KEY (" + AppConstant.BUSINESS_SETTING_CURRENCY_ID + ") REFERENCES " + AppConstant.CURRENCY_TABLE_NAME +
                "(" + AppConstant.CURRENCY_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.CUSTOMER_TABLE_NAME + "(" + AppConstant.CUSTOMER_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.CUSTOMER_NAME +
                " TEXT, " + AppConstant.CUSTOMER_ADDRESS + " TEXT, " + AppConstant.CUSTOMER_PHONE_NUM + " TEXT, " + AppConstant.CUSTOMER_BUSINESS_NAME + " TEXT, " + AppConstant.CUSTOMER_BALANCE +
                " REAL, " + AppConstant.CUSTOMER_IS_DELETED + " integer default 0, " + AppConstant.CUSTOMER_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.CUSTOMER_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.CUSTOMER_CUSTOM_FIELD_3 + " TEXT, "
                + AppConstant.CUSTOMER_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.CUSTOMER_CUSTOM_FIELD_5 + " TEXT)");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.SALES_TABLE_NAME + "(" + AppConstant.SALES_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.SALES_VOUCHER_NUM
                + " TEXT, " + AppConstant.SALES_DATE + " TEXT, " + AppConstant.SALES_CUSTOMER_ID + " INTEGER, " + AppConstant.SALES_TOTAL_AMOUNT + " REAL, " + AppConstant.SALES_DISCOUNT + " TEXT, " + AppConstant.SALES_DISCOUNT_ID + " INTEGER, " +
                AppConstant.SALES_TAX_ID + " INTEGER, " + AppConstant.SALES_SUB_TOTAL + " REAL, " + AppConstant.SALES_TYPE + " TEXT, " + AppConstant.SALES_DISCOUNT_AMOUNT + " REAL, "
                + AppConstant.SALES_REMARK + " TEXT, " + AppConstant.SALES_PAID_AMOUNT + " REAL, " + AppConstant.SALES_BALANCE + " REAL, " + AppConstant.SALES_TAX_AMOUNT + " REAL, " +
                AppConstant.CREATED_DATE + " TEXT, " + AppConstant.SALES_DAY + " TEXT, " + AppConstant.SALES_MONTH + " TEXT, " + AppConstant.SALES_YEAR + " TEXT, " + AppConstant.SALES_TAX_RATE +
                " REAL, " + AppConstant.SALES_TIME + " INTEGER, " + AppConstant.SALES_TAX_TYPE + " TEXT, " +
                AppConstant.SALES_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.SALES_USER_ID + " TEXT, " + AppConstant.SALES_CHANGE_CASH + " TEXT, "
                + AppConstant.SALES_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.SALES_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.SALES_CUSTOMER_ID +
                ") REFERENCES " + AppConstant.CUSTOMER_TABLE_NAME + "(" + AppConstant.CUSTOMER_ID + "), FOREIGN KEY (" + AppConstant.SALES_TAX_ID + ") REFERENCES " + AppConstant.TAX_TABLE_NAME +
                "(" + AppConstant.TAX_ID + "), FOREIGN KEY(" + AppConstant.SALES_DISCOUNT_ID + ") REFERENCES " + AppConstant.DISCOUNT_TABLE_NAME + "(" + AppConstant.DISCOUNT_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.SALES_HOLD_TABLE_NAME + "(" + AppConstant.SALES_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.SALES_VOUCHER_NUM
                + " TEXT, " + AppConstant.SALES_DATE + " TEXT, " + AppConstant.SALES_CUSTOMER_ID + " INTEGER, " + AppConstant.SALES_TOTAL_AMOUNT + " REAL, " + AppConstant.SALES_DISCOUNT + " TEXT, " + AppConstant.SALES_DISCOUNT_ID + " INTEGER, " +
                AppConstant.SALES_TAX_ID + " INTEGER, " + AppConstant.SALES_SUB_TOTAL + " REAL, " + AppConstant.SALES_TYPE + " TEXT, " + AppConstant.SALES_DISCOUNT_AMOUNT + " REAL, "
                + AppConstant.SALES_REMARK + " TEXT, " + AppConstant.SALES_PAID_AMOUNT + " REAL, " + AppConstant.SALES_BALANCE + " REAL, " + AppConstant.SALES_TAX_AMOUNT + " REAL, " +
                AppConstant.CREATED_DATE + " TEXT, " + AppConstant.SALES_DAY + " TEXT, " + AppConstant.SALES_MONTH + " TEXT, " + AppConstant.SALES_YEAR + " TEXT, " + AppConstant.SALES_TAX_RATE +
                " REAL, " + AppConstant.SALES_TIME + " INTEGER, " + AppConstant.SALES_TAX_TYPE + " TEXT, " +
                AppConstant.SALES_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.SALES_USER_ID + " TEXT, " + AppConstant.SALES_CHANGE_CASH + " TEXT, "
                + AppConstant.SALES_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.SALES_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.SALES_CUSTOMER_ID +
                ") REFERENCES " + AppConstant.CUSTOMER_TABLE_NAME + "(" + AppConstant.CUSTOMER_ID + "), FOREIGN KEY (" + AppConstant.SALES_TAX_ID + ") REFERENCES " + AppConstant.TAX_TABLE_NAME +
                "(" + AppConstant.TAX_ID + "), FOREIGN KEY(" + AppConstant.SALES_DISCOUNT_ID + ") REFERENCES " + AppConstant.DISCOUNT_TABLE_NAME + "(" + AppConstant.DISCOUNT_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.SALES_HOLD_DETAIL_TABLE_NAME + "(" + AppConstant.SALES_DETAIL_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.SALES_DETAIL_SALES_ID +
                " INTEGER, " + AppConstant.SALES_DETAIL_STOCK_ID + " INTEGER NOT NULL, " + AppConstant.SALES_DETAIL_QTY + " INTEGER, " + AppConstant.SALES_DETAIL_PRICE + " REAL, " + AppConstant.SALES_DETAIL_DISCOUNT
                + " TEXT, " + AppConstant.SALES_DETAIL_TAX_ID + " INTEGER, " + AppConstant.SALES_DETAIL_TOTAL + " REAL, " + AppConstant.SALES_DETAIL_TAX_AMOUNT + " REAL, " + AppConstant.SALES_DETAIL_DISCOUNT_AMT + " REAL, "
                + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.SALES_DETAIL_TAX_RATE + " REAL, " + AppConstant.SALES_DETAIL_TAX_TYPE + " TEXT, " + AppConstant.SALES_DETAIL_CUSTOM_FIELD_1
                + " TEXT," + AppConstant.SALES_DETAIL_CUSTOM_FIELD_2 + " TEXT," + AppConstant.SALES_DETAIL_CUSTOM_FIELD_3 + " TEXT," + AppConstant.SALES_DETAIL_CUSTOM_FIELD_4 + " TEXT,"
                + AppConstant.SALES_DETAIL_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.SALES_DETAIL_STOCK_ID + ") REFERENCES " + AppConstant.STOCK_TABLE_NAME + "(" + AppConstant.STOCK_ID + "), FOREIGN KEY (" +
                AppConstant.SALES_DETAIL_SALES_ID + ") REFERENCES " + AppConstant.SALES_HOLD_TABLE_NAME + "(" + AppConstant.SALES_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.SALES_DETAIL_TABLE_NAME + "(" + AppConstant.SALES_DETAIL_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.SALES_DETAIL_SALES_ID +
                " INTEGER, " + AppConstant.SALES_DETAIL_STOCK_ID + " INTEGER NOT NULL, " + AppConstant.SALES_DETAIL_QTY + " INTEGER, " + AppConstant.SALES_DETAIL_PRICE + " REAL, " + AppConstant.SALES_DETAIL_DISCOUNT
                + " TEXT, " + AppConstant.SALES_DETAIL_TAX_ID + " INTEGER, " + AppConstant.SALES_DETAIL_TOTAL + " REAL, " + AppConstant.SALES_DETAIL_TAX_AMOUNT + " REAL, " + AppConstant.SALES_DETAIL_DISCOUNT_AMT + " REAL, "
                + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.SALES_DETAIL_TAX_RATE + " REAL, " + AppConstant.SALES_DETAIL_TAX_TYPE + " TEXT, " + AppConstant.SALES_DETAIL_CUSTOM_FIELD_1
                + " TEXT," + AppConstant.SALES_DETAIL_CUSTOM_FIELD_2 + " TEXT," + AppConstant.SALES_DETAIL_CUSTOM_FIELD_3 + " TEXT," + AppConstant.SALES_DETAIL_CUSTOM_FIELD_4 + " TEXT,"
                + AppConstant.SALES_DETAIL_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.SALES_DETAIL_STOCK_ID + ") REFERENCES " + AppConstant.STOCK_TABLE_NAME + "(" + AppConstant.STOCK_ID + "), FOREIGN KEY (" +
                AppConstant.SALES_DETAIL_SALES_ID + ") REFERENCES " + AppConstant.SALES_TABLE_NAME + "(" + AppConstant.SALES_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.DELIVERY_TABLE_NAME + "(" + AppConstant.DELIVERY_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.DELIVERY_DATE +
                " TEXT, " + AppConstant.DELIVERY_PHONE_NUM + " TEXT, " + AppConstant.DELIVERY_ADDRESS + " TEXT, " + AppConstant.DELIVERY_AGENT + " TEXT, " + AppConstant.DELIVERY_CHARGES + " REAL, " +
                AppConstant.DELIVERY_IS_INCLUSIVE + " INTEGER, " + AppConstant.DELIVERY_SALES_ID + " INTEGER, " + AppConstant.DELIVERY_STATUS + " INTEGER, "
                + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.DELIVERY_DAY + " TEXT, " + AppConstant.DELIVERY_MONTH + " TEXT, " + AppConstant.DELIVERY_YEAR + " TEXT, " +
                AppConstant.DELIVERY_TIME + " INTEGER, " + AppConstant.DELIVERY_CUSTOM_FIELD_1 +
                " TEXT, " + AppConstant.DELIVERY_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.DELIVERY_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.DELIVERY_CUSTOM_FIELD_4 + " TEXT, " +
                AppConstant.DELIVERY_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY (" + AppConstant.DELIVERY_SALES_ID + ") REFERENCES " + AppConstant.SALES_TABLE_NAME + "(" + AppConstant.SALES_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.PICKUP_TABLE_NAME + "(" + AppConstant.PICKUP_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.PICKUP_DATE +
                " TEXT, " + AppConstant.PICKUP_PHONE_NUM + " TEXT, " + AppConstant.PICKUP_SALES_ID + " INTEGER, " + AppConstant.PICKUP_STATUS + " INTEGER, "
                + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.PICKUP_DAY + " TEXT, " + AppConstant.PICKUP_MONTH + " TEXT, " + AppConstant.PICKUP_YEAR + " TEXT, " + AppConstant.PICKUP_CUSTOM_FIELD_1 + " TEXT, "
                + AppConstant.PICKUP_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.PICKUP_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.PICKUP_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.PICKUP_CUSTOM_FIELD_5
                + " TEXT, FOREIGN KEY(" + AppConstant.PICKUP_SALES_ID + ") REFERENCES " + AppConstant.SALES_TABLE_NAME + "(" + AppConstant.SALES_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.SUPPLIER_TABLE_NAME + "(" + AppConstant.SUPPLIER_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.SUPPLIER_NAME +
                " TEXT, " + AppConstant.SUPPLIER_BUSINESS_NAME + " TEXT, " + AppConstant.SUPPLIER_ADDRESS + " TEXT, " + AppConstant.SUPPLIER_PHONE_NUM + " TEXT, "
                + AppConstant.SUPPLIER_BALANCE + " REAL, " + AppConstant.SUPPLIER_IS_DELETED + " integer default 0, " + AppConstant.SUPPLIER_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.SUPPLIER_CUSTOM_FIELD_2 + " TEXT, "
                + AppConstant.SUPPLIER_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.SUPPLIER_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.SUPPLIER_CUSTOM_FIELD_5 + " TEXT)");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.PURCHASE_TABLE_NAME + "(" + AppConstant.PURCHASE_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.PURCHASE_VOUCHER_NUM
                + " TEXT, " + AppConstant.PURCHASE_DATE + " TEXT, " + AppConstant.PURCHASE_SUPPLIER_ID + " INTEGER, " + AppConstant.PURCHASE_TOTAL_AMOUNT + " REAL, " + AppConstant.PURCHASE_DISCOUNT + " TEXT, " + AppConstant.PURCHASE_DISCOUNT_ID + " INTEGER, " +
                AppConstant.PURCHASE_TAX_ID + " INTEGER, " + AppConstant.PURCHASE_SUB_TOTAL + " REAL, " + AppConstant.PURCHASE_DISCOUNT_AMOUNT + " REAL, "
                + AppConstant.PURCHASE_REMARK + " TEXT, " + AppConstant.PURCHASE_PAID_AMOUNT + " REAL, " + AppConstant.PURCHASE_BALANCE + " REAL, " + AppConstant.PURCHASE_TAX_AMOUNT + " REAL, " +
                AppConstant.CREATED_DATE + " TEXT, " + AppConstant.PURCHASE_DAY + " TEXT, " + AppConstant.PURCHASE_MONTH + " TEXT, " + AppConstant.PURCHASE_YEAR
                + " TEXT, " + AppConstant.PURCHASE_TAX_RATE + " REAL, " + AppConstant.PURCHASE_TAX_TYPE + " TEXT, " +
                AppConstant.PURCHASE_TIME + " INTEGER, " + AppConstant.CURRENT_USER_ID + " TEXT, " + AppConstant.PURCHASE_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.PURCHASE_CUSTOM_FIELD_3 + " TEXT, "
                + AppConstant.PURCHASE_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.PURCHASE_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.PURCHASE_SUPPLIER_ID +
                ") REFERENCES " + AppConstant.SUPPLIER_TABLE_NAME + "(" + AppConstant.SUPPLIER_ID + "), FOREIGN KEY (" + AppConstant.PURCHASE_TAX_ID + ") REFERENCES " + AppConstant.TAX_TABLE_NAME +
                "(" + AppConstant.TAX_ID + "), FOREIGN KEY(" + AppConstant.PURCHASE_DISCOUNT_ID + ") REFERENCES " + AppConstant.DISCOUNT_TABLE_NAME + "(" + AppConstant.DISCOUNT_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + "(" + AppConstant.PURCHASE_DETAIL_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID +
                " INTEGER, " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " INTEGER, " + AppConstant.PURCHASE_DETAIL_QTY + " INTEGER, " + AppConstant.PURCHASE_DETAIL_PRICE + " REAL, " + AppConstant.PURCHASE_DETAIL_DISCOUNT
                + " TEXT, " + AppConstant.PURCHASE_DETAIL_TAX_ID + " INTEGER, " + AppConstant.PURCHASE_DETAIL_TOTAL + " REAL, " + AppConstant.PURCHASE_DETAIL_TAX_AMT + " REAL, " + AppConstant.PURCHASE_DETAIL_DISCOUNT_AMT + " REAL, "
                + AppConstant.PURCHASE_DETAIL_TAX_RATE + " REAL, " + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.PURCHASE_DETAIL_TAX_TYPE + " TEXT, " + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_1
                + " TEXT," + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_2 + " TEXT," + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_3 + " TEXT," + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_4 + " TEXT,"
                + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.PURCHASE_DETAIL_STOCK_ID + ") REFERENCES " + AppConstant.STOCK_TABLE_NAME + "(" + AppConstant.STOCK_ID + "), FOREIGN KEY (" +
                AppConstant.PURCHASE_DETAIL_PURCHASE_ID + ") REFERENCES " + AppConstant.PURCHASE_TABLE_NAME + "(" + AppConstant.PURCHASE_ID + "), FOREIGN KEY(" + AppConstant.PURCHASE_DETAIL_TAX_ID + ") REFERENCES "
                + AppConstant.TAX_TABLE_NAME + "( " + AppConstant.TAX_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.PURCHASE_HOLD_TABLE_NAME + "(" + AppConstant.PURCHASE_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.PURCHASE_VOUCHER_NUM
                + " TEXT, " + AppConstant.PURCHASE_DATE + " TEXT, " + AppConstant.PURCHASE_SUPPLIER_ID + " INTEGER, " + AppConstant.PURCHASE_TOTAL_AMOUNT + " REAL, " + AppConstant.PURCHASE_DISCOUNT + " TEXT, " + AppConstant.PURCHASE_DISCOUNT_ID + " INTEGER, " +
                AppConstant.PURCHASE_TAX_ID + " INTEGER, " + AppConstant.PURCHASE_SUB_TOTAL + " REAL, " + AppConstant.PURCHASE_DISCOUNT_AMOUNT + " REAL, "
                + AppConstant.PURCHASE_REMARK + " TEXT, " + AppConstant.PURCHASE_PAID_AMOUNT + " REAL, " + AppConstant.PURCHASE_BALANCE + " REAL, " + AppConstant.PURCHASE_TAX_AMOUNT + " REAL, " +
                AppConstant.CREATED_DATE + " TEXT, " + AppConstant.PURCHASE_DAY + " TEXT, " + AppConstant.PURCHASE_MONTH + " TEXT, " + AppConstant.PURCHASE_YEAR
                + " TEXT, " + AppConstant.PURCHASE_TAX_RATE + " REAL, " + AppConstant.PURCHASE_TAX_TYPE + " TEXT, " +
                AppConstant.PURCHASE_TIME + " INTEGER, " + AppConstant.CURRENT_USER_ID + " TEXT, " + AppConstant.PURCHASE_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.PURCHASE_CUSTOM_FIELD_3 + " TEXT, "
                + AppConstant.PURCHASE_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.PURCHASE_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.PURCHASE_SUPPLIER_ID +
                ") REFERENCES " + AppConstant.SUPPLIER_TABLE_NAME + "(" + AppConstant.SUPPLIER_ID + "), FOREIGN KEY (" + AppConstant.PURCHASE_TAX_ID + ") REFERENCES " + AppConstant.TAX_TABLE_NAME +
                "(" + AppConstant.TAX_ID + "), FOREIGN KEY(" + AppConstant.PURCHASE_DISCOUNT_ID + ") REFERENCES " + AppConstant.DISCOUNT_TABLE_NAME + "(" + AppConstant.DISCOUNT_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.PURCHASE_HOLD_DETAIL_TABLE_NAME + "(" + AppConstant.PURCHASE_DETAIL_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID +
                " INTEGER, " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " INTEGER, " + AppConstant.PURCHASE_DETAIL_QTY + " INTEGER, " + AppConstant.PURCHASE_DETAIL_PRICE + " REAL, " + AppConstant.PURCHASE_DETAIL_DISCOUNT
                + " TEXT, " + AppConstant.PURCHASE_DETAIL_TAX_ID + " INTEGER, " + AppConstant.PURCHASE_DETAIL_TOTAL + " REAL, " + AppConstant.PURCHASE_DETAIL_TAX_AMT + " REAL, " + AppConstant.PURCHASE_DETAIL_DISCOUNT_AMT + " REAL, "
                + AppConstant.PURCHASE_DETAIL_TAX_RATE + " REAL, " + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.PURCHASE_DETAIL_TAX_TYPE + " TEXT, " + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_1
                + " TEXT," + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_2 + " TEXT," + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_3 + " TEXT," + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_4 + " TEXT,"
                + AppConstant.PURCHASE_DETAIL_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.PURCHASE_DETAIL_STOCK_ID + ") REFERENCES " + AppConstant.STOCK_TABLE_NAME + "(" + AppConstant.STOCK_ID + "), FOREIGN KEY (" +
                AppConstant.PURCHASE_DETAIL_PURCHASE_ID + ") REFERENCES " + AppConstant.PURCHASE_HOLD_TABLE_NAME + "(" + AppConstant.PURCHASE_ID + "), FOREIGN KEY(" + AppConstant.PURCHASE_DETAIL_TAX_ID + ") REFERENCES "
                + AppConstant.TAX_TABLE_NAME + "( " + AppConstant.TAX_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.EXPENSE_NAME_TABLE_NAME + "(" + AppConstant.EXPENSE_NAME_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.EXPENSE_NAME_NAME + " TEXT, "
                + AppConstant.EXPENSE_NAME_TYPE + " TEXT, " + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.EXPENSE_NAME_CUSTOM_FIELD_1
                + " TEXT," + AppConstant.EXPENSE_NAME_CUSTOM_FIELD_2 + " TEXT," + AppConstant.EXPENSE_NAME_CUSTOM_FIELD_3 + " TEXT," + AppConstant.EXPENSE_NAME_CUSTOM_FIELD_4 + " TEXT,"
                + AppConstant.EXPENSE_NAME_CUSTOM_FIELD_5 + " TEXT);");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.EXPENSE_TABLE_NAME + "(" + AppConstant.EXPENSE_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.EXPENSE_DATE + " TEXT, "
                + AppConstant.EXPENSE_AMOUNT + " REAL, " + AppConstant.EXPENSE_EXPENSE_NAME_ID + " INTEGER, " + AppConstant.EXPENSE_REMARK + " TEXT, " + AppConstant.EXPENSE_DAY + " TEXT, "
                + AppConstant.EXPENSE_MONTH + " TEXT, " + AppConstant.EXPENSE_YEAR + " TEXT, " + AppConstant.CREATED_DATE + " TEXT, " + AppConstant.EXPENSE_REFUND_ID + " INTEGER, " +
                AppConstant.EXPENSE_TIME + " INTEGER, " + AppConstant.EXPENSE_CUSTOM_FIELD_1
                + " TEXT," + AppConstant.EXPENSE_CUSTOM_FIELD_2 + " TEXT," + AppConstant.EXPENSE_CUSTOM_FIELD_3 + " TEXT," + AppConstant.EXPENSE_CUSTOM_FIELD_4 + " TEXT,"
                + AppConstant.EXPENSE_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.EXPENSE_EXPENSE_NAME_ID + ") REFERENCES " + AppConstant.EXPENSE_NAME_TABLE_NAME + " (" +
                AppConstant.EXPENSE_NAME_ID + "), FOREIGN KEY(" + AppConstant.EXPENSE_REFUND_ID + ") REFERENCES " + AppConstant.REFUND_TABLE_NAME + " (" +
                AppConstant.REFUND_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + "(" + AppConstant.CUSTOMER_OUTSTANDING_ID +
                " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " + AppConstant.CUSTOMER_OUTSTANDING_INVOICE_NUM + " TEXT, " + AppConstant.CUSTOMER_OUTSTANDING_CUSTOMER_ID +
                " INTEGER, " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " INTEGER, " + AppConstant.CREATED_DATE + " TEXT," +// AppConstant.CUSTOMER_OUTSTANDING_CURRENCY_ID + " INTEGER, " +
                AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT + " REAL, " + AppConstant.CUSTOMER_OUTSTANDING_DAY + " TEXT, " + AppConstant.CUSTOMER_OUTSTANDING_MONTH + " TEXT, " +
                AppConstant.CUSTOMER_OUTSTANDING_YEAR + " TEXT, " + AppConstant.CUSTOMER_OUTSTANDING_DATE + " TEXT, " + AppConstant.CUSTOMER_OUTSTANDING_TIME + " INTEGER, " + AppConstant.CUSTOMER_OUTSTANDING_CUSTOM_FIELD_1 + " TEXT, "
                + AppConstant.CUSTOMER_OUTSTANDING_CUSTOM_FIELD_2 + " TEXT, " + AppConstant.CUSTOMER_OUTSTANDING_CUSTOM_FIELD_3 + " TEXT, "
                + AppConstant.CUSTOMER_OUTSTANDING_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.CUSTOMER_OUTSTANDING_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" +
                AppConstant.CUSTOMER_OUTSTANDING_CUSTOMER_ID + ") REFERENCES " + AppConstant.CUSTOMER_TABLE_NAME + "(" + AppConstant.CUSTOMER_ID +
                "), FOREIGN KEY (" + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + ") REFERENCES " + AppConstant.SALES_TABLE_NAME
                + "(" + AppConstant.SALES_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + "(" + AppConstant.SUPPLIER_OUTSTANDING_ID +
                " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " + AppConstant.SUPPLIER_OUTSTANDING_INVOICE_NUM + " TEXT, " + AppConstant.SUPPLIER_OUTSTANDING_SUPPLIER_ID +
                " INTEGER, " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " INTEGER, " + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                " REAL, " + AppConstant.CREATED_DATE + " TEXT," + AppConstant.SUPPLIER_OUTSTANDING_DAY + " TEXT, " + AppConstant.SUPPLIER_OUTSTANDING_MONTH + " TEXT, " +
                AppConstant.SUPPLIER_OUTSTANDING_YEAR + " TEXT, " + AppConstant.SUPPLIER_OUTSTANDING_DATE + " TEXT, " + AppConstant.SUPPLIER_OUTSTANDING_TIME + " INTEGER, " +
                AppConstant.SUPPLIER_OUTSTANDING_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.SUPPLIER_OUTSTANDING_CUSTOM_FIELD_2 + " TEXT, "
                + AppConstant.SUPPLIER_OUTSTANDING_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.SUPPLIER_OUTSTANDING_CUSTOM_FIELD_4 + " TEXT, "
                + AppConstant.SUPPLIER_OUTSTANDING_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY (" + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID +
                ") REFERENCES " + AppConstant.PURCHASE_TABLE_NAME + "(" + AppConstant.PURCHASE_ID + "), FOREIGN KEY (" + AppConstant.SUPPLIER_OUTSTANDING_SUPPLIER_ID +
                ") REFERENCES " + AppConstant.SUPPLIER_TABLE_NAME + "(" + AppConstant.SUPPLIER_ID + "));");

        database.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.REFUND_TABLE_NAME + "(" + AppConstant.REFUND_ID + " INTEGER NOT NULL PRIMARY KEY, " + AppConstant.REFUND_VOUCHER_NO + " TEXT, " +
                AppConstant.REFUND_SALES_ID + " INTEGER, " + AppConstant.REFUND_CUSTOMER_ID + " INTEGER, " + AppConstant.REFUND_DATE + " TEXT, " + AppConstant.REFUND_DAY + " TEXT, " + AppConstant.REFUND_MONTH +
                " TEXT, " + AppConstant.REFUND_YEAR + " TEXT, " + AppConstant.REFUND_AMOUNT + " REAL, " + AppConstant.REFUND_REMARK + " TEXT, " + AppConstant.CREATED_DATE + " TEXT, "
                + AppConstant.REFUND_TIME + " INTEGER, " + AppConstant.REFUND_CUSTOM_FIELD_1 + " TEXT, " + AppConstant.REFUND_CUSTOM_FIELD_2 + " TEXT, "
                + AppConstant.REFUND_CUSTOM_FIELD_3 + " TEXT, " + AppConstant.REFUND_CUSTOM_FIELD_4 + " TEXT, " + AppConstant.REFUND_CUSTOM_FIELD_5 + " TEXT, FOREIGN KEY(" + AppConstant.REFUND_SALES_ID + ") REFERENCES " +
                AppConstant.SALES_TABLE_NAME + "(" + AppConstant.SALES_ID + "), FOREIGN KEY(" + AppConstant.REFUND_CUSTOMER_ID + ") REFERENCES " + AppConstant.CUSTOMER_TABLE_NAME + " (" + AppConstant.CUSTOMER_ID + "));");
    }

    private void createTriggers() {
        database.execSQL("create trigger if not exists BeforeDeleteDefaultCurrencyUpdateDefaultToOne before delete on Currency when old.isDefault = 1 begin \n" +
                "update Currency set isDefault = 1 where id = 1;\n" +
                "end;");
        database.execSQL("create trigger if not exists AfterInsertStockUpdateIdGen after insert on " + AppConstant.STOCK_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Stock';" + " end");

        database.execSQL("create trigger if not exists AfterInsertDiscountUpdateIdGen after insert on " + AppConstant.DISCOUNT_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Discount';" + " end");

        database.execSQL("create trigger if not exists AfterInsertCategoryUpdateIdGen after insert on " + AppConstant.CATEGORY_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Category';" + " end");

        database.execSQL("create trigger if not exists AfterInsertUnitUpdateIdGen after insert on " + AppConstant.UNIT_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Unit';" + " end");

        database.execSQL("create trigger if not exists AfterInsertUserRoleUpdateIdGen after insert on " + AppConstant.USER_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'UserRole';" + " end");

        database.execSQL("create trigger if not exists AfterInsertLostUpdateIdGen after insert on " + AppConstant.LOST_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Lost';" + " end");

        /*database.execSQL("create trigger if not exists AfterInsertLostDetailUpdateIdGen after insert on " + AppConstant.LOST_DETAIL_TABLE_NAME+ " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME +" set "+ AppConstant.ID_GENERATOR_VALUE+ " = (select "+ AppConstant.ID_GENERATOR_VALUE + "+1 from "
                + AppConstant.ID_GENERATOR_TABLE_NAME +" where " + AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION +" = 'LostDetail') where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION +" = 'LostDetail';" + " end");*/

        database.execSQL("create trigger if not exists AfterInsertDamageUpdateIdGen after insert on " + AppConstant.DAMAGE_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Damage';"
                /* + " update " + AppConstant.DOCUMENT_NUMBER_SETTING_TABLE_NAME + " set " + AppConstant.DOCUMENT_NUMBER_SETTING_NEXT_NUMBER + "= (select " +
                AppConstant.DOCUMENT_NUMBER_SETTING_NEXT_NUMBER +" + 1 from " + AppConstant.DOCUMENT_NUMBER_SETTING_TABLE_NAME + " where " + AppConstant.DOCUMENT_NUMBER_SETTING_MENU_ID +
                "=1) where " + AppConstant.DOCUMENT_NUMBER_SETTING_MENU_ID + "=1;"*/
                + " end");

        /*database.execSQL("create trigger if not exists AfterInsertDamageDetailUpdateIdGen after insert on " + AppConstant.DAMAGE_DETAIL_TABLE_NAME+ " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME +" set "+ AppConstant.ID_GENERATOR_VALUE+ " = (select "+ AppConstant.ID_GENERATOR_VALUE + "+1 from "
                + AppConstant.ID_GENERATOR_TABLE_NAME +" where " + AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION +" = 'DamageDetail') where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION +" = 'DamageDetail';" + " end");*/

        database.execSQL("create trigger if not exists AfterInsertBusinessSettingUpdateIdGen after insert on " + AppConstant.BUSINESS_SETTING_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'BusinessSetting';" + " end");

        database.execSQL("create trigger if not exists AfterInsertCustomerUpdateIdGen after insert on " + AppConstant.CUSTOMER_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Customer';" + " end");

        database.execSQL("create trigger if not exists AfterInsertSalesUpdateIdGen after insert on " + AppConstant.SALES_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Sales';" + " end");

        database.execSQL("create trigger if not exists AfterInsertSalesDetailUpdateIdGen after insert on " + AppConstant.SALES_DETAIL_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'SalesDetail';" + " end");
        database.execSQL("create trigger if not exists AfterInsertHoldSalesUpdateIdGen after insert on " + AppConstant.SALES_HOLD_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Hold';" + " end");

        database.execSQL("create trigger if not exists AfterInsertHoldSalesDetailUpdateIdGen after insert on " + AppConstant.SALES_HOLD_DETAIL_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'SaleHoldDetails';" + " end");

        database.execSQL("create trigger if not exists AfterInsertDeliveryUpdateIdGen after insert on " + AppConstant.DELIVERY_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Delivery';" + " end");

        database.execSQL("create trigger if not exists AfterInsertPickupUpdateIdGen after insert on " + AppConstant.PICKUP_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Pickup';" + " end");

        database.execSQL("create trigger if not exists AfterInsertSupplierUpdateIdGen after insert on " + AppConstant.SUPPLIER_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Supplier';" + " end");

        database.execSQL("create trigger if not exists AfterInsertPurchaseUpdateIdGen after insert on " + AppConstant.PURCHASE_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Purchase';" + " end");

        database.execSQL("create trigger if not exists AfterInsertPurchaseDetailUpdateIdGen after insert on " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'PurchaseDetail';" + " end");

        database.execSQL("create trigger if not exists AfterInsertPurchaseHoldUpdateIdGen after insert on " + AppConstant.PURCHASE_HOLD_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'PurchaseHold';" + " end");

        database.execSQL("create trigger if not exists AfterInsertPurchaseHoldDetailUpdateIdGen after insert on " + AppConstant.PURCHASE_HOLD_DETAIL_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'PurchaseHoldDetail';" + " end");

        database.execSQL("create trigger if not exists AfterInsertExpenseNameUpdateIdGen after insert on " + AppConstant.EXPENSE_NAME_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'ExpenseName';" + " end");

        database.execSQL("create trigger if not exists AfterInsertExpenseUpdateIdGen after insert on " + AppConstant.EXPENSE_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Expense';" + " end");

        database.execSQL("create trigger if not exists AfterInsertCustomerOutstandingUpdateIdGen after insert on " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'CustomerOutstanding';" + " end");

        database.execSQL("create trigger if not exists AfterInsertSupplierOutstandingUpdateIdGen after insert on " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'SupplierOutstanding';" + " end");

        database.execSQL("create trigger if not exists AfterInsertStockImageUpdateIdGen after insert on " + AppConstant.STOCK_IMAGE_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'StockImage';" + " end");

        database.execSQL("create trigger if not exists AfterInsertCurrencyUpdateIdGen after insert on " + AppConstant.CURRENCY_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Currency';" + " end");


        database.execSQL("create trigger if not exists AfterInsertRefuncUpdateIdGen after insert on " + AppConstant.REFUND_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Refund';" + " end");


        //tax
        database.execSQL("create trigger if not exists AfterInsertTaxUpdateIdGen after insert on " + AppConstant.TAX_TABLE_NAME + " begin " +
                " update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = " + AppConstant.ID_GENERATOR_VALUE + "+1 where " +
                AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Tax';" + " end");

        database.execSQL("create trigger if not exists AfterInsertTaxUpdateOtherTaxDefault after insert on " + AppConstant.TAX_TABLE_NAME
                + " when (select count( " + AppConstant.TAX_ID + ") from " + AppConstant.TAX_TABLE_NAME + ") > 1 and new." + AppConstant.TAX_IS_DEFAULT + "=1"
                + " begin "
                + " update " + AppConstant.TAX_TABLE_NAME + " set " + AppConstant.TAX_IS_DEFAULT + "= 0 where " + AppConstant.TAX_IS_DEFAULT + "=1 and "
                + AppConstant.TAX_ID + " <> new." + AppConstant.TAX_ID + " ;"
                + " end;");

        database.execSQL("create trigger if not exists AfterUpdateTaxDefaultUpdateOtherTaxDefault after update on " + AppConstant.TAX_TABLE_NAME
                + " when (select count( " + AppConstant.TAX_ID + ") from " + AppConstant.TAX_TABLE_NAME + ") > 1 and new." + AppConstant.TAX_IS_DEFAULT + "=1"
                + " begin "
                + " update " + AppConstant.TAX_TABLE_NAME + " set " + AppConstant.TAX_IS_DEFAULT + "= 0 where " + AppConstant.TAX_IS_DEFAULT + "=1 and "
                + AppConstant.TAX_ID + " <> new." + AppConstant.TAX_ID + " ;"
                + " end;");

        database.execSQL("create trigger if not exists AfterInsertDiscountUpdateOtherDiscountDefault after insert on " + AppConstant.DISCOUNT_TABLE_NAME
                + " when (select count( " + AppConstant.DISCOUNT_ID + ") from " + AppConstant.DISCOUNT_TABLE_NAME + ") > 1 and new." + AppConstant.DISCOUNT_IS_DEFAULT + "=1"
                + " begin "
                + " update " + AppConstant.DISCOUNT_TABLE_NAME + " set " + AppConstant.DISCOUNT_IS_DEFAULT + "= 0 where " + AppConstant.DISCOUNT_IS_DEFAULT + "=1 and "
                + AppConstant.DISCOUNT_ID + " <> new." + AppConstant.DISCOUNT_ID + " ;"
                + " end;");

        database.execSQL("create trigger if not exists AfterUpdateDiscountDefaultUpdateOtherDiscountDefault after update on " + AppConstant.DISCOUNT_TABLE_NAME
                + " when (select count( " + AppConstant.DISCOUNT_ID + ") from " + AppConstant.DISCOUNT_TABLE_NAME + ") > 1 and new." + AppConstant.DISCOUNT_IS_DEFAULT + "=1"
                + " begin "
                + " update " + AppConstant.DISCOUNT_TABLE_NAME + " set " + AppConstant.DISCOUNT_IS_DEFAULT + "= 0 where " + AppConstant.DISCOUNT_IS_DEFAULT + " = 1 and "
                + AppConstant.DISCOUNT_ID + " <> new." + AppConstant.DISCOUNT_ID + " ;"
                + " end;");

        database.execSQL("create trigger if not exists BeforeDeleteTaxChangeFKtoNull before delete on " + AppConstant.TAX_TABLE_NAME + " when old." + AppConstant.TAX_ID + " <> " + 1 +
                " begin" +
                " update " + AppConstant.SALES_TABLE_NAME + " set " + AppConstant.SALES_TAX_ID + " = null where " + AppConstant.SALES_TAX_ID + " = old." + AppConstant.TAX_ID + ";" +
                " update " + AppConstant.SALES_DETAIL_TABLE_NAME + " set " + AppConstant.SALES_DETAIL_TAX_ID + " = null where " + AppConstant.SALES_DETAIL_TAX_ID + " = old." + AppConstant.TAX_ID + ";" +
                " update " + AppConstant.PURCHASE_TABLE_NAME + " set " + AppConstant.PURCHASE_TAX_ID + " = null where " + AppConstant.PURCHASE_TAX_ID + " = old." + AppConstant.TAX_ID + ";" +
                " update " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " set " + AppConstant.PURCHASE_DETAIL_TAX_ID + " = null where " + AppConstant.PURCHASE_DETAIL_TAX_ID + " = old." + AppConstant.TAX_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists BeforeDeleteDiscountChangeFKtoNull before delete on " + AppConstant.DISCOUNT_TABLE_NAME +
                " begin" +
                " update " + AppConstant.SALES_TABLE_NAME + " set " + AppConstant.SALES_DISCOUNT_ID + " = null where " + AppConstant.SALES_DISCOUNT_ID + " = old." + AppConstant.DISCOUNT_ID + ";" +
                " update " + AppConstant.PURCHASE_TABLE_NAME + " set " + AppConstant.PURCHASE_DISCOUNT_ID + " = null where " + AppConstant.PURCHASE_DISCOUNT_ID + " = old." + AppConstant.DISCOUNT_ID + ";" +
                " end;");

        //damage
        database.execSQL("create trigger if not exists AfterInsertDamageUpdateStockQty after insert on " + AppConstant.DAMAGE_TABLE_NAME +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " - new." + AppConstant.DAMAGE_STOCK_QTY +
                " where " + AppConstant.STOCK_ID + " = new." + AppConstant.DAMAGE_STOCK_ID + "; " +
                " end;");

        database.execSQL("create trigger if not exists AfterDeleteDamageUpdateStockQty after delete on " + AppConstant.DAMAGE_TABLE_NAME +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " + old." + AppConstant.DAMAGE_STOCK_QTY +
                " where " + AppConstant.STOCK_ID + " = old." + AppConstant.DAMAGE_STOCK_ID + "; " +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateDamageUpdateStockQtyWithNotEquSID after update on " + AppConstant.DAMAGE_TABLE_NAME +
                " when new." + AppConstant.DAMAGE_STOCK_ID + " <> old." + AppConstant.DAMAGE_STOCK_ID +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY
                + " + old." + AppConstant.DAMAGE_STOCK_QTY +
                " where " + AppConstant.STOCK_ID + " = old." + AppConstant.DAMAGE_STOCK_ID + "; " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " - new." + AppConstant.DAMAGE_STOCK_QTY +
                " where " + AppConstant.STOCK_ID + " = new." + AppConstant.DAMAGE_STOCK_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateDamageUpdateStockQtyWithEquSID after update on " + AppConstant.DAMAGE_TABLE_NAME +
                " when new." + AppConstant.DAMAGE_STOCK_ID + " = old." + AppConstant.DAMAGE_STOCK_ID +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + " + old." + AppConstant.DAMAGE_STOCK_QTY +
                " - new." + AppConstant.DAMAGE_STOCK_QTY +
                " where " + AppConstant.STOCK_ID + " = old." + AppConstant.DAMAGE_STOCK_ID + "; " +
                " end;");

        // lost
        database.execSQL("create trigger if not exists AfterInsertLostDetailUpdateStockQty after insert on " + AppConstant.LOST_TABLE_NAME +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " - new." + AppConstant.LOST_STOCK_QTY +
                " where " + AppConstant.STOCK_ID + " = new." + AppConstant.LOST_STOCK_ID + "; " +
                " end;");

        database.execSQL("create trigger if not exists AfterDeleteLostUpdateStockQty after delete on " + AppConstant.LOST_TABLE_NAME +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " + old." + AppConstant.LOST_STOCK_QTY +
                " where " + AppConstant.STOCK_ID + " = old." + AppConstant.LOST_STOCK_ID + "; " +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateLostUpdateStockQtyWithNotEquSID after update on " + AppConstant.LOST_TABLE_NAME +
                " when new." + AppConstant.LOST_STOCK_ID + " <> old." + AppConstant.LOST_STOCK_ID +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " + old." + AppConstant.LOST_STOCK_QTY +
                " where " + AppConstant.STOCK_ID + " = old." + AppConstant.LOST_STOCK_ID + "; " +

                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + "=  " + AppConstant.STOCK_INVENTORY_QTY + " - new." + AppConstant.LOST_STOCK_QTY +
                " where " + AppConstant.STOCK_ID + " = new." + AppConstant.LOST_STOCK_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateLostDetailUpdateStockQtyWithEquSID after update on " + AppConstant.LOST_TABLE_NAME +
                " when new." + AppConstant.LOST_STOCK_ID + " = old." + AppConstant.LOST_STOCK_ID +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " + old." + AppConstant.LOST_STOCK_QTY +
                " - new." + AppConstant.LOST_STOCK_QTY +
                " where " + AppConstant.STOCK_ID + " = old." + AppConstant.LOST_STOCK_ID + "; " +
                " end;");

        //sale
        database.execSQL("create trigger if not exists AfterInsertSalesCheckBalForUpdateCustBal after insert on " + AppConstant.SALES_TABLE_NAME + " when new." + AppConstant.SALES_BALANCE + " > 0 " +
                " begin " +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " + new." + AppConstant.SALES_BALANCE +
                " where " + AppConstant.CUSTOMER_ID + " = new." + AppConstant.CUSTOMER_OUTSTANDING_CUSTOMER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateSalesUpdateCustBalForEquCust after update on " + AppConstant.SALES_TABLE_NAME + " when new." + AppConstant.SALES_CUSTOMER_ID + " = old." + AppConstant.SALES_CUSTOMER_ID +
                " begin " +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " - old." + AppConstant.SALES_BALANCE + " + new." +
                AppConstant.SALES_BALANCE + " where " + AppConstant.CUSTOMER_ID + " = new." + AppConstant.SALES_CUSTOMER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateSalesUpdateCustBalForUnequCust after update on " + AppConstant.SALES_TABLE_NAME + " when new." + AppConstant.SALES_CUSTOMER_ID + " <> old." + AppConstant.SALES_CUSTOMER_ID +
                " begin " +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " - old." + AppConstant.SALES_BALANCE
                + " where " + AppConstant.CUSTOMER_ID + " = old." + AppConstant.SALES_CUSTOMER_ID + ";" +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " + new." +
                AppConstant.SALES_BALANCE + " where " + AppConstant.CUSTOMER_ID + " = new." + AppConstant.SALES_CUSTOMER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterDeleteSalesUpdateCustBal after delete on " + AppConstant.SALES_TABLE_NAME + "" +
                " begin " +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " - old." + AppConstant.SALES_BALANCE + " where " + AppConstant.CUSTOMER_ID + " = old." + AppConstant.SALES_CUSTOMER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterDeletePurchaseUpdateSupBal after delete on " + AppConstant.PURCHASE_TABLE_NAME + "" +
                " begin " +
                " update " + AppConstant.SUPPLIER_TABLE_NAME + " set " + AppConstant.SUPPLIER_BALANCE + " = " + AppConstant.SUPPLIER_BALANCE + " - old." + AppConstant.PURCHASE_BALANCE + " where " + AppConstant.SUPPLIER_ID + " = old." + AppConstant.PURCHASE_SUPPLIER_ID + ";" +
                " end;");

        //saleDetail
        database.execSQL("create trigger if not exists AfterInsertSaleDetailUpdateStockQty after insert on " + AppConstant.SALES_DETAIL_TABLE_NAME +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " - new." + AppConstant.SALES_DETAIL_QTY + " where " + AppConstant.STOCK_ID +
                " = new." + AppConstant.SALES_DETAIL_STOCK_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterDeleteSalesDetailUpdateStockQty after delete on " + AppConstant.SALES_DETAIL_TABLE_NAME +
                " when ((select 1 from  " + AppConstant.SALES_TABLE_NAME + " where " + AppConstant.SALES_TYPE + " not like '%Cancel' and " + AppConstant.SALES_ID + " = old." + AppConstant.SALES_DETAIL_SALES_ID + ") > 0)" +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " + old." + AppConstant.SALES_DETAIL_QTY +
                " where " + AppConstant.STOCK_ID + " = old." + AppConstant.SALES_DETAIL_STOCK_ID + "; " +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateSalesDetailUpdateStockQtyWithNotEquStockID after update on " + AppConstant.SALES_DETAIL_TABLE_NAME +
                " when new." + AppConstant.SALES_DETAIL_STOCK_ID + " <> old." + AppConstant.SALES_DETAIL_STOCK_ID +
                " and (select " + AppConstant.SALES_TYPE + " from " + AppConstant.SALES_TABLE_NAME + " where " + AppConstant.SALES_ID + " = new." + AppConstant.SALES_DETAIL_SALES_ID + ") not like '%Cancel'" +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " + old." + AppConstant.SALES_DETAIL_QTY +
                " where " + AppConstant.STOCK_ID + " = old." + AppConstant.SALES_DETAIL_STOCK_ID + "; " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + "= " + AppConstant.STOCK_INVENTORY_QTY + " - new." + AppConstant.SALES_DETAIL_QTY +
                " where " + AppConstant.STOCK_ID + " = new." + AppConstant.SALES_DETAIL_STOCK_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateSalesDetailUpdateStockQtyWithEquStockID after update on " + AppConstant.SALES_DETAIL_TABLE_NAME +
                " when new." + AppConstant.SALES_DETAIL_STOCK_ID + " = old." + AppConstant.SALES_DETAIL_STOCK_ID +
                " and (select " + AppConstant.SALES_TYPE + " from " + AppConstant.SALES_TABLE_NAME + " where " + AppConstant.SALES_ID + " = new." + AppConstant.SALES_DETAIL_SALES_ID + ") not like '%Cancel'" +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " + old." + AppConstant.SALES_DETAIL_QTY +
                " - new." + AppConstant.SALES_DETAIL_QTY +
                " where " + AppConstant.STOCK_ID + " = old." + AppConstant.SALES_DETAIL_STOCK_ID + "; " +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateSaleTypeUpdatePickup after update on " + AppConstant.SALES_TABLE_NAME
                + " when old." + AppConstant.SALES_TYPE + " <> new." + AppConstant.SALES_TYPE + " and old." + AppConstant.SALES_TYPE + "= '" + SalesManager.SaleType.Pickup.toString()
                + "' and new." + AppConstant.SALES_TYPE + " <> '" + SalesManager.SaleType.PickupCancel.toString() + "'"
                + " begin"
                + " delete from " + AppConstant.PICKUP_TABLE_NAME + " where " + AppConstant.PICKUP_SALES_ID + " = old." + AppConstant.SALES_ID + ";"
                + "end;");

        database.execSQL("create trigger if not exists AfterUpdateSaleTypeUpdateDelivery after update on " + AppConstant.SALES_TABLE_NAME
                + " when old." + AppConstant.SALES_TYPE + " <> new." + AppConstant.SALES_TYPE + " and old." + AppConstant.SALES_TYPE + "= '" + SalesManager.SaleType.Delivery.toString()
                + "' and new." + AppConstant.SALES_TYPE + " <> '" + SalesManager.SaleType.DeliveryCancel.toString() + "'"
                + " begin"
                + " delete from " + AppConstant.DELIVERY_TABLE_NAME + " where " + AppConstant.DELIVERY_SALES_ID + " = old." + AppConstant.SALES_ID + ";"
                + "end;");

        /*database.execSQL("CREATE TRIGGER AfterPutOrderCancelIncreaseStockQty " + " after update on " + AppConstant.SALES_DETAIL_TABLE_NAME + " when (select " + AppConstant.SALES_TYPE+ " from " +
                AppConstant.SALES_TABLE_NAME + " where " + AppConstant.SALES_ID + " = new." + AppConstant.SALES_DETAIL_SALES_ID + ") like '%Cancel'" +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_INVENTORY_QTY + " = (select " + AppConstant.STOCK_INVENTORY_QTY + " from " + AppConstant.STOCK_TABLE_NAME +
                " where " + AppConstant.STOCK_ID + " = new." + AppConstant.SALES_DETAIL_STOCK_ID + ") + new." + AppConstant.SALES_DETAIL_QTY + " where " + AppConstant.STOCK_ID + " = new." +
                AppConstant.SALES_DETAIL_STOCK_ID + ";" +
                " end");

        database.execSQL("CREATE TRIGGER AfterPutBackOrderNormalDecreaseStockQty " + " after update on " + AppConstant.SALES_DETAIL_TABLE_NAME + " when (select " + AppConstant.SALES_TYPE+ " from " +
                AppConstant.SALES_TABLE_NAME + " where " + AppConstant.SALES_ID + " = new." + AppConstant.SALES_DETAIL_SALES_ID + ") not like '%Cancel'" +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_INVENTORY_QTY + " = (select " + AppConstant.STOCK_INVENTORY_QTY + " from " + AppConstant.STOCK_TABLE_NAME +
                " where " + AppConstant.STOCK_ID + " = new." + AppConstant.SALES_DETAIL_STOCK_ID + ") - new." + AppConstant.SALES_DETAIL_QTY + " where " + AppConstant.STOCK_ID + " = new." +
                AppConstant.SALES_DETAIL_STOCK_ID + ";" +
                " end");*/

        //purchase
        database.execSQL("create trigger if not exists AfterInsertPurchaseDetailUpdateStockQty after insert on " + AppConstant.PURCHASE_DETAIL_TABLE_NAME +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY +
                " + new." + AppConstant.PURCHASE_DETAIL_QTY + " where " + AppConstant.STOCK_ID +
                " = new." + AppConstant.PURCHASE_DETAIL_STOCK_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterDeletePurchaseDetailUpdateStockQty after delete on " + AppConstant.PURCHASE_DETAIL_TABLE_NAME +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " - old." + AppConstant.PURCHASE_DETAIL_QTY +
                " where " + AppConstant.STOCK_ID + " = old." + AppConstant.PURCHASE_DETAIL_STOCK_ID + "; " +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdatePurchaseDetailUpdateStockQtyWithNotEquSID after update on " + AppConstant.PURCHASE_DETAIL_TABLE_NAME +
                " when new." + AppConstant.PURCHASE_DETAIL_STOCK_ID + " <> old." + AppConstant.PURCHASE_DETAIL_STOCK_ID +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " - old." + AppConstant.PURCHASE_DETAIL_QTY +
                " where " + AppConstant.STOCK_ID + " = old." + AppConstant.PURCHASE_DETAIL_STOCK_ID + "; " +

                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + "=" + AppConstant.STOCK_INVENTORY_QTY + " + new." + AppConstant.PURCHASE_DETAIL_QTY +
                " where " + AppConstant.STOCK_ID + " = new." + AppConstant.PURCHASE_DETAIL_STOCK_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdatePurchaseDetailUpdateStockQtyWithEquSID after update on " + AppConstant.PURCHASE_DETAIL_TABLE_NAME +
                " when new." + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = old." + AppConstant.PURCHASE_DETAIL_STOCK_ID +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME +
                " set " + AppConstant.STOCK_INVENTORY_QTY + " = " + AppConstant.STOCK_INVENTORY_QTY + " - old." + AppConstant.PURCHASE_DETAIL_QTY +
                " + new." + AppConstant.PURCHASE_DETAIL_QTY +
                " where " + AppConstant.STOCK_ID + " = old." + AppConstant.PURCHASE_DETAIL_STOCK_ID + "; " +
                " end;");

        //category
        database.execSQL("create trigger if not exists BeforeDeleteCategoryChangeFKsToDefault before delete on " + AppConstant.CATEGORY_TABLE_NAME +
                " when old" +
                "." + AppConstant.CATEGORY_ID + " <> " + 1 +
                " begin " +
                " update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_CATEGORY_ID + " = " + 1 +
                " where " + AppConstant.STOCK_CATEGORY_ID + " = old." + AppConstant.CATEGORY_ID + ";" +
                " end;");

        //customer
        database.execSQL("create trigger if not exists BeforeDeleteCustomerChangeFKsToDefault before delete on " + AppConstant.CUSTOMER_TABLE_NAME + " when old." + AppConstant.CUSTOMER_ID + " <> " + 1 +
                " begin " +
                " update " + AppConstant.SALES_TABLE_NAME + " set " + AppConstant.SALES_CUSTOMER_ID + " = " + 1 + " where " +
                AppConstant.SALES_CUSTOMER_ID + " = old." + AppConstant.CUSTOMER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists BeforeDeleteSupplierChangeFKsToDefault before delete on " + AppConstant.SUPPLIER_TABLE_NAME + " when old." + AppConstant.SUPPLIER_ID + " <> " + 1 +
                " begin " +
                " update " + AppConstant.PURCHASE_TABLE_NAME + " set " + AppConstant.PURCHASE_SUPPLIER_ID + " = " + 1 + " where " +
                AppConstant.PURCHASE_SUPPLIER_ID + " = old." + AppConstant.SUPPLIER_ID + ";" +
                " end;");

        //customer outstanding
        database.execSQL("create trigger if not exists AfterInsertCustOutstandPaymentUpdateCustBal after insert on " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME +
                " begin " +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " - new." + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                " where " + AppConstant.CUSTOMER_ID + " = new." + AppConstant.CUSTOMER_OUTSTANDING_CUSTOMER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateCustOutstandPaymentUpdateCustBal after update on " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME +
                " begin" +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " + old." + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT
                + " - new." + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT + " where " + AppConstant.CUSTOMER_ID + " = new." + AppConstant.CUSTOMER_OUTSTANDING_CUSTOMER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterDeleteCustOutstandPaymentUpdateCustBal after delete on " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME +
                " begin" +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " + old." + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                " where " + AppConstant.CUSTOMER_ID + " = old." + AppConstant.CUSTOMER_OUTSTANDING_CUSTOMER_ID + ";" +
                " end;");

        //supplier outstanding
        database.execSQL("create trigger if not exists AfterInsertSupOutstandPaymentUpdateSupBal after insert on " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME +
                " begin " +
                " update " + AppConstant.SUPPLIER_TABLE_NAME + " set " + AppConstant.SUPPLIER_BALANCE + " = " + AppConstant.SUPPLIER_BALANCE + " - new." + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                " where " + AppConstant.SUPPLIER_ID + " = new." + AppConstant.SUPPLIER_OUTSTANDING_SUPPLIER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateSupOutstandPaymentUpdateSupBal after update on " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME +
                " begin" +
                " update " + AppConstant.SUPPLIER_TABLE_NAME + " set " + AppConstant.SUPPLIER_BALANCE + " = " + AppConstant.SUPPLIER_BALANCE + " + old." + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT
                + " - new." + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT + " where " + AppConstant.SUPPLIER_ID + " = new." + AppConstant.SUPPLIER_OUTSTANDING_SUPPLIER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterDeleteSupOutstandPaymentUpdateSupBal after delete on " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME +
                " begin" +
                " update " + AppConstant.SUPPLIER_TABLE_NAME + " set " + AppConstant.SUPPLIER_BALANCE + " = " + AppConstant.SUPPLIER_BALANCE + " + old." + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                " where " + AppConstant.SUPPLIER_ID + " = old." + AppConstant.SUPPLIER_OUTSTANDING_SUPPLIER_ID + ";" +
                " end;");


        /////
        database.execSQL("create trigger if not exists AfterInsertPurchaseCheckBalForUpdateSupBal after insert on " + AppConstant.PURCHASE_TABLE_NAME + " when new." + AppConstant.PURCHASE_BALANCE + " > 0 " +
                " begin " +
                " update " + AppConstant.SUPPLIER_TABLE_NAME + " set " + AppConstant.SUPPLIER_BALANCE + " = " + AppConstant.SUPPLIER_BALANCE + " + new." + AppConstant.PURCHASE_BALANCE +
                " where " + AppConstant.SUPPLIER_ID + " = new." + AppConstant.SUPPLIER_OUTSTANDING_SUPPLIER_ID + ";" +
                " end;");

        /*database.execSQL("create trigger if not exists AfterUpdateSalesUpdateCustBalForEquCust after update on " + AppConstant.SALES_TABLE_NAME + " when new." + AppConstant.SALES_CUSTOMER_ID + " = old." + AppConstant.SALES_CUSTOMER_ID +
                " begin " +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " - old." + AppConstant.SALES_BALANCE + " + new." +
                AppConstant.SALES_BALANCE + " where " + AppConstant.CUSTOMER_ID + " = new." + AppConstant.SALES_CUSTOMER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateSalesUpdateCustBalForUnequCust after update on " + AppConstant.SALES_TABLE_NAME + " when new." + AppConstant.SALES_CUSTOMER_ID + " <> old." + AppConstant.SALES_CUSTOMER_ID +
                " begin " +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " - old." + AppConstant.SALES_BALANCE
                + " where " + AppConstant.CUSTOMER_ID + " = old." + AppConstant.SALES_CUSTOMER_ID + ";" +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " + new." +
                AppConstant.SALES_BALANCE + " where " + AppConstant.CUSTOMER_ID + " = new." + AppConstant.SALES_CUSTOMER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterDeleteSalesUpdateCustBal after delete on " + AppConstant.SALES_TABLE_NAME + "" +
                " begin " +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " - old." + AppConstant.SALES_BALANCE + " + new." +
                AppConstant.SALES_BALANCE + " where " + AppConstant.CUSTOMER_ID + " = new." + AppConstant.SALES_CUSTOMER_ID + ";" +
                " end;");*/

        database.execSQL("create trigger if not exists AfterUpdateSalesTypeCancelUpdateCustBal after update on " + AppConstant.SALES_TABLE_NAME + " when new." + AppConstant.SALES_TYPE + " like '%' || 'Cancel' " +
                "and old." + AppConstant.SALES_TYPE + " <> new." + AppConstant.SALES_TYPE + " and new." + AppConstant.SALES_BALANCE + " > 0.0" +
                " begin " +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " - new." + AppConstant.SALES_BALANCE
                + " where " + AppConstant.CUSTOMER_ID + " = new." + AppConstant.SALES_CUSTOMER_ID + " ;" +
                "end;"
        );

        database.execSQL("create trigger if not exists AfterUpdateSalesTypeNotCancelUpdateCustBal after update on " + AppConstant.SALES_TABLE_NAME + " when new." + AppConstant.SALES_TYPE + " not like '%' || 'Cancel' " +
                "and old." + AppConstant.SALES_TYPE + " <> new." + AppConstant.SALES_TYPE + " and new." + AppConstant.SALES_BALANCE + " > 0.0" +
                " begin " +
                " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " + new." + AppConstant.SALES_BALANCE +
                " where " + AppConstant.CUSTOMER_ID + " = new." + AppConstant.SALES_CUSTOMER_ID + ";" +
                "end;"
        );

        database.execSQL("create trigger if not exists AfterUpdatePurchaseUpdateSupBalForEquSup after update on " + AppConstant.PURCHASE_TABLE_NAME + " when new." + AppConstant.PURCHASE_SUPPLIER_ID + " = old." + AppConstant.PURCHASE_SUPPLIER_ID +
                " begin " +
                " update " + AppConstant.SUPPLIER_TABLE_NAME + " set " + AppConstant.SUPPLIER_BALANCE + " = " + AppConstant.SUPPLIER_BALANCE + " - old." + AppConstant.PURCHASE_BALANCE + " + new." +
                AppConstant.PURCHASE_BALANCE + " where " + AppConstant.SUPPLIER_ID + " = new." + AppConstant.PURCHASE_SUPPLIER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdatePurchaseUpdateSupBalForUnequSup after update on " + AppConstant.PURCHASE_TABLE_NAME + " when new." + AppConstant.PURCHASE_SUPPLIER_ID + " <> old." + AppConstant.PURCHASE_SUPPLIER_ID +
                " begin " +
                " update " + AppConstant.SUPPLIER_TABLE_NAME + " set " + AppConstant.SUPPLIER_BALANCE + " = " + AppConstant.SUPPLIER_BALANCE + " - old." + AppConstant.PURCHASE_BALANCE
                + " where " + AppConstant.SUPPLIER_ID + " = old." + AppConstant.PURCHASE_SUPPLIER_ID + ";" +
                " update " + AppConstant.SUPPLIER_TABLE_NAME + " set " + AppConstant.SUPPLIER_BALANCE + " = " + AppConstant.SUPPLIER_BALANCE + " + new." +
                AppConstant.PURCHASE_BALANCE + " where " + AppConstant.SUPPLIER_ID + " = new." + AppConstant.PURCHASE_SUPPLIER_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterInsertSaleDetailPriceUpdateStockRetailPrice after insert on " + AppConstant.SALES_DETAIL_TABLE_NAME + " when " +
                "new." + AppConstant.SALES_DETAIL_PRICE + " <> (select " + AppConstant.STOCK_RETAIL_PRICE + " from " + AppConstant.STOCK_TABLE_NAME + " where " + AppConstant.STOCK_ID + "= new." +
                AppConstant.SALES_DETAIL_STOCK_ID + ")" +
                " begin " +
                "update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_RETAIL_PRICE + " = new." + AppConstant.SALES_DETAIL_PRICE + " where " + AppConstant.STOCK_ID + "= new." +
                AppConstant.SALES_DETAIL_STOCK_ID + "; " +
                "end");

        database.execSQL("create trigger if not exists AfterUpdateSaleDetailPriceUpdateStockRetailPrice after update on " + AppConstant.SALES_DETAIL_TABLE_NAME + " when " +
                "new." + AppConstant.SALES_DETAIL_PRICE + " <> old." + AppConstant.SALES_DETAIL_PRICE +
                " begin " +
                "update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_RETAIL_PRICE + " = new." + AppConstant.SALES_DETAIL_PRICE + " where " + AppConstant.STOCK_ID + "= new." +
                AppConstant.SALES_DETAIL_STOCK_ID + "; " +
                "end");

        database.execSQL("create trigger if not exists AfterInsertPurchaseDetailPriceUpdateStockPurchasePrice after insert on " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " when " +
                "new." + AppConstant.PURCHASE_DETAIL_PRICE + " <> (select " + AppConstant.STOCK_PURCHASE_PRICE + " from " + AppConstant.STOCK_TABLE_NAME + " where " + AppConstant.STOCK_ID + "= new." +
                AppConstant.PURCHASE_DETAIL_STOCK_ID + ")" +
                " begin " +
                "update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_PURCHASE_PRICE + " = new." + AppConstant.PURCHASE_DETAIL_PRICE + " where " + AppConstant.STOCK_ID + "= new." +
                AppConstant.PURCHASE_DETAIL_STOCK_ID + "; " +
                "end");

        database.execSQL("create trigger if not exists AfterUpdatePurchaseDetailPriceUpdateStockPurchasePrice after update on " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " when " +
                "new." + AppConstant.PURCHASE_DETAIL_PRICE + " <> old." + AppConstant.PURCHASE_DETAIL_PRICE +
                " begin " +
                "update " + AppConstant.STOCK_TABLE_NAME + " set " + AppConstant.STOCK_RETAIL_PRICE + " = new." + AppConstant.PURCHASE_DETAIL_PRICE + " where " + AppConstant.STOCK_ID + "= new." +
                AppConstant.PURCHASE_DETAIL_STOCK_ID + "; " +
                "end");

        database.execSQL("create trigger if not exists AfterInsertBusinessSettingUpdateCurrency after insert on " + AppConstant.BUSINESS_SETTING_TABLE_NAME +
                " begin " +
                " update " + AppConstant.CURRENCY_TABLE_NAME + " set " + AppConstant.CURRENCY_IS_DEFAULT + " = 0 where " + AppConstant.CURRENCY_IS_DEFAULT + " = 1" + ";" +
                " update " + AppConstant.CURRENCY_TABLE_NAME + " set " + AppConstant.CURRENCY_IS_DEFAULT + " = 1 where " + AppConstant.CURRENCY_ID + " = new." + AppConstant.BUSINESS_SETTING_CURRENCY_ID + ";" +
                " end;");

        database.execSQL("create trigger if not exists AfterUpdateBusinessSettingNotEqualCurrencyDefault after update on " + AppConstant.BUSINESS_SETTING_TABLE_NAME + " when new."
                + AppConstant.BUSINESS_SETTING_CURRENCY_ID + " <> (select " + AppConstant.CURRENCY_ID + " from " + AppConstant.CURRENCY_TABLE_NAME + " where " + AppConstant.CURRENCY_IS_DEFAULT + " = 1)"
                + " begin "
                + "update " + AppConstant.CURRENCY_TABLE_NAME + " set " + AppConstant.CURRENCY_IS_DEFAULT + " = 0 where " + AppConstant.CURRENCY_IS_DEFAULT + " = 1;"
                + " update " + AppConstant.CURRENCY_TABLE_NAME + " set " + AppConstant.CURRENCY_IS_DEFAULT + " = 1 where " + AppConstant.CURRENCY_ID + " = new." + AppConstant.BUSINESS_SETTING_CURRENCY_ID + ";"
                + " end;");

        /*database.execSQL("create trigger if not exists AfterUpdateBusinessSettingNotEqualCurrencyDefault after update on " + AppConstant.BUSINESS_SETTING_TABLE_NAME + " when new."
                + AppConstant.BUSINESS_SETTING_CURRENCY_ID + " <> old." + AppConstant.BUSINESS_SETTING_CURRENCY_ID
                + " begin "
                + "update " + AppConstant.CURRENCY_TABLE_NAME + " set " + AppConstant.CURRENCY_IS_DEFAULT + " = 0 where " + AppConstant.CURRENCY_IS_DEFAULT + " = 1;"
                + " update " + AppConstant.CURRENCY_TABLE_NAME + " set " + AppConstant.CURRENCY_IS_DEFAULT + " = 1 where " + AppConstant.CURRENCY_ID + " <> new." + AppConstant.BUSINESS_SETTING_CURRENCY_ID + ";"
                + " end;");*/

        database.execSQL("create trigger if not exists AfterInsertCurrencyWhenActive after insert on " + AppConstant.CURRENCY_TABLE_NAME + " when new." + AppConstant.CURRENCY_IS_DEFAULT + " = 1 " +
                " begin " +
                " update " + AppConstant.CURRENCY_TABLE_NAME + " set " + AppConstant.CURRENCY_IS_DEFAULT + " = 0 where " + AppConstant.CURRENCY_IS_DEFAULT + " = 1 and " + AppConstant.CURRENCY_ID +
                " <> new." + AppConstant.CURRENCY_ID + ";" +
                "update " + AppConstant.BUSINESS_SETTING_TABLE_NAME + " set " + AppConstant.BUSINESS_SETTING_CURRENCY_ID + " = new." + AppConstant.CURRENCY_ID + ";"
                + " end;");

        database.execSQL("create trigger if not exists AfterUpdateCurrencyWhenActive after update on " + AppConstant.CURRENCY_TABLE_NAME + " when new." + AppConstant.CURRENCY_IS_DEFAULT + " = 1 " +
                " begin " +
                " update " + AppConstant.CURRENCY_TABLE_NAME + " set " + AppConstant.CURRENCY_IS_DEFAULT + " = 0 where " + AppConstant.CURRENCY_IS_DEFAULT + " = 1 and " + AppConstant.CURRENCY_ID +
                " <> new." + AppConstant.CURRENCY_ID + ";" +
                "update " + AppConstant.BUSINESS_SETTING_TABLE_NAME + " set " + AppConstant.BUSINESS_SETTING_CURRENCY_ID + " = new." + AppConstant.CURRENCY_ID + ";"
                + " end;");

        database.execSQL("create trigger if not exists AfterInsertRefundInsertExpenseMgr after insert on " + AppConstant.REFUND_TABLE_NAME + " when (select " + AppConstant.SALES_PAID_AMOUNT + " from " +
                AppConstant.SALES_TABLE_NAME + " where " + AppConstant.SALES_ID + " = new." + AppConstant.REFUND_SALES_ID + ") - new." + AppConstant.REFUND_AMOUNT + " > 0 " +
                " begin " +
                "insert into " + AppConstant.EXPENSE_TABLE_NAME + "(" + AppConstant.EXPENSE_ID + ", " + AppConstant.EXPENSE_EXPENSE_NAME_ID + ", " + AppConstant.EXPENSE_AMOUNT + ", "
                + AppConstant.EXPENSE_DATE + ", " + AppConstant.EXPENSE_DAY + ", " + AppConstant.EXPENSE_MONTH + ", " + AppConstant.EXPENSE_YEAR + ") values((select " + AppConstant.ID_GENERATOR_VALUE + "+1 from " +
                AppConstant.ID_GENERATOR_TABLE_NAME + " where " + AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = 'Expense'), 1, (select " + AppConstant.SALES_PAID_AMOUNT + " from " +
                AppConstant.SALES_TABLE_NAME + " where " + AppConstant.SALES_ID + " = new." + AppConstant.REFUND_SALES_ID + ") - new." + AppConstant.REFUND_AMOUNT + ", new." + AppConstant.REFUND_DATE + ", new." +
                AppConstant.REFUND_DAY + ", new." + AppConstant.REFUND_MONTH + ", new." + AppConstant.REFUND_YEAR + ");" +
                "end;");

        database.execSQL("create trigger if not exists AfterUpdatePurchaseUpdateTime after update on " + AppConstant.PURCHASE_TABLE_NAME + " when new." + AppConstant.PURCHASE_DATE + " <> old." + AppConstant.PURCHASE_DATE +
                " begin " +
                " update " + AppConstant.PURCHASE_TABLE_NAME + " set " + AppConstant.PURCHASE_TIME + " = " +
                "strftime('%s', (select new." + AppConstant.PURCHASE_YEAR + " || '-' || case when cast(new." + AppConstant.PURCHASE_MONTH + " as integer) < 10 then '0' || new." + AppConstant.PURCHASE_MONTH +
                " else new." + AppConstant.PURCHASE_MONTH + " end || '-' || case when cast(new." + AppConstant.PURCHASE_DAY + " as integer) < 10 then '0' || new." + AppConstant.PURCHASE_DAY + " else new." + AppConstant.PURCHASE_DAY +
                " end as date from " + AppConstant.PURCHASE_TABLE_NAME + " where " + AppConstant.PURCHASE_ID + " = old." + AppConstant.PURCHASE_ID + "), time('now', 'localtime' )) where " + AppConstant.PURCHASE_ID + " = old." + AppConstant.PURCHASE_ID + ";" +
                "end;");

        database.execSQL("create trigger if not exists AfterUpdateSalesUpdateTime after update on " + AppConstant.SALES_TABLE_NAME + " when new." + AppConstant.SALES_DATE + " <> old." + AppConstant.SALES_DATE +
                " begin " +
                " update " + AppConstant.SALES_TABLE_NAME + " set " + AppConstant.SALES_TIME + " = " +
                "strftime('%s', (select new." + AppConstant.SALES_YEAR + " || '-' || case when cast(new." + AppConstant.SALES_MONTH + " as integer) < 10 then '0' || new." + AppConstant.SALES_MONTH +
                " else new." + AppConstant.SALES_MONTH + " end || '-' || case when cast(new." + AppConstant.SALES_DAY + " as integer) < 10 then '0' || new." + AppConstant.SALES_DAY + " else new." + AppConstant.SALES_DAY +
                " end as date from " + AppConstant.SALES_TABLE_NAME + " where " + AppConstant.SALES_ID + " = old." + AppConstant.SALES_ID + "), time('now', 'localtime' )) where " + AppConstant.SALES_ID + " = old." + AppConstant.SALES_ID + ";" +
                "end;");

        database.execSQL("create trigger if not exists AfterUpdateDamageUpdateTime after update on " + AppConstant.DAMAGE_TABLE_NAME + " when new." + AppConstant.DAMAGE_DATE + " <> old." + AppConstant.DAMAGE_DATE +
                " begin " +
                " update " + AppConstant.DAMAGE_TABLE_NAME + " set " + AppConstant.DAMAGE_TIME + " = " +
                "strftime('%s', (select new." + AppConstant.DAMAGE_YEAR + " || '-' || case when cast(new." + AppConstant.DAMAGE_MONTH + " as integer) < 10 then '0' || new." + AppConstant.DAMAGE_MONTH +
                " else new." + AppConstant.DAMAGE_MONTH + " end || '-' || case when cast(new." + AppConstant.DAMAGE_DAY + " as integer) < 10 then '0' || new." + AppConstant.DAMAGE_DAY + " else new." + AppConstant.DAMAGE_DAY +
                " end as date from " + AppConstant.DAMAGE_TABLE_NAME + " where " + AppConstant.DAMAGE_ID + " = old." + AppConstant.DAMAGE_ID + "), time('now', 'localtime' )) where " + AppConstant.DAMAGE_ID + " = old." + AppConstant.DAMAGE_ID + ";" +
                "end;");

        database.execSQL("create trigger if not exists AfterUpdateLostUpdateTime after update on " + AppConstant.LOST_TABLE_NAME + " when new." + AppConstant.LOST_DATE + " <> old." + AppConstant.LOST_DATE +
                " begin " +
                " update " + AppConstant.LOST_TABLE_NAME + " set " + AppConstant.LOST_TIME + " = " +
                "strftime('%s', (select new." + AppConstant.LOST_YEAR + " || '-' || case when cast(new." + AppConstant.LOST_MONTH + " as integer) < 10 then '0' || new." + AppConstant.LOST_MONTH +
                " else new." + AppConstant.LOST_MONTH + " end || '-' || case when cast(new." + AppConstant.LOST_DAY + " as integer) < 10 then '0' || new." + AppConstant.LOST_DAY + " else new." + AppConstant.LOST_DAY +
                " end as date from " + AppConstant.LOST_TABLE_NAME + " where " + AppConstant.LOST_ID + " = old." + AppConstant.LOST_ID + "), time('now', 'localtime' )) where " + AppConstant.LOST_ID + " = old." + AppConstant.LOST_ID + ";" +
                "end;");

        database.execSQL("create trigger if not exists AfterUpdateDeliveryUpdateTime after update on " + AppConstant.DELIVERY_TABLE_NAME + " when new." + AppConstant.DELIVERY_DATE + " <> old." + AppConstant.DELIVERY_DATE +
                " begin " +
                " update " + AppConstant.DELIVERY_TABLE_NAME + " set " + AppConstant.DELIVERY_TIME + " = " +
                "strftime('%s', (select new." + AppConstant.DELIVERY_YEAR + " || '-' || case when cast(new." + AppConstant.DELIVERY_MONTH + " as integer) < 10 then '0' || new." + AppConstant.DELIVERY_MONTH +
                " else new." + AppConstant.DELIVERY_MONTH + " end || '-' || case when cast(new." + AppConstant.DELIVERY_DAY + " as integer) < 10 then '0' || new." + AppConstant.DELIVERY_DAY + " else new." + AppConstant.DELIVERY_DAY +
                " end as date from " + AppConstant.DELIVERY_TABLE_NAME + " where " + AppConstant.DELIVERY_ID + " = old." + AppConstant.DELIVERY_ID + "), time('now', 'localtime' )) where " + AppConstant.DELIVERY_ID + " = old." + AppConstant.DELIVERY_ID + ";" +
                "end;");

        database.execSQL("create trigger if not exists AfterUpdateExpenseUpdateTime after update on " + AppConstant.EXPENSE_TABLE_NAME + " when new." + AppConstant.EXPENSE_DATE + " <> old." + AppConstant.EXPENSE_DATE +
                " begin " +
                " update " + AppConstant.EXPENSE_TABLE_NAME + " set " + AppConstant.EXPENSE_TIME + " = " +
                "strftime('%s', (select new." + AppConstant.EXPENSE_YEAR + " || '-' || case when cast(new." + AppConstant.EXPENSE_MONTH + " as integer) < 10 then '0' || new." + AppConstant.EXPENSE_MONTH +
                " else new." + AppConstant.EXPENSE_MONTH + " end || '-' || case when cast(new." + AppConstant.EXPENSE_DAY + " as integer) < 10 then '0' || new." + AppConstant.EXPENSE_DAY + " else new." + AppConstant.EXPENSE_DAY +
                " end as date from " + AppConstant.EXPENSE_TABLE_NAME + " where " + AppConstant.EXPENSE_ID + " = old." + AppConstant.EXPENSE_ID + "), time('now', 'localtime' )) where " + AppConstant.EXPENSE_ID + " = old." + AppConstant.EXPENSE_ID + ";" +
                "end;");

        database.execSQL("create trigger if not exists AfterUpdateCustomerOutstandingUpdateTime after update on " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " when new." + AppConstant.CUSTOMER_OUTSTANDING_DATE + " <> old." + AppConstant.CUSTOMER_OUTSTANDING_DATE +
                " begin " +
                " update " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " set " + AppConstant.CUSTOMER_OUTSTANDING_TIME + " = " +
                "strftime('%s', (select new." + AppConstant.CUSTOMER_OUTSTANDING_YEAR + " || '-' || case when cast(new." + AppConstant.CUSTOMER_OUTSTANDING_MONTH + " as integer) < 10 then '0' || new." + AppConstant.CUSTOMER_OUTSTANDING_MONTH +
                " else new." + AppConstant.CUSTOMER_OUTSTANDING_MONTH + " end || '-' || case when cast(new." + AppConstant.CUSTOMER_OUTSTANDING_DAY + " as integer) < 10 then '0' || new." + AppConstant.CUSTOMER_OUTSTANDING_DAY + " else new." + AppConstant.CUSTOMER_OUTSTANDING_DAY +
                " end as date from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_ID + " = old." + AppConstant.CUSTOMER_OUTSTANDING_ID + "), time('now', 'localtime' )) where " + AppConstant.CUSTOMER_OUTSTANDING_ID + " = old." + AppConstant.CUSTOMER_OUTSTANDING_ID + ";" +
                "end;");

        database.execSQL("create trigger if not exists AfterUpdateSupplierOutstandingUpdateTime after update on " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " when new." + AppConstant.SUPPLIER_OUTSTANDING_DATE + " <> old." + AppConstant.SUPPLIER_OUTSTANDING_DATE +
                " begin " +
                " update " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " set " + AppConstant.SUPPLIER_OUTSTANDING_TIME + " = " +
                "strftime('%s', (select new." + AppConstant.SUPPLIER_OUTSTANDING_YEAR + " || '-' || case when cast(new." + AppConstant.SUPPLIER_OUTSTANDING_MONTH + " as integer) < 10 then '0' || new." + AppConstant.SUPPLIER_OUTSTANDING_MONTH +
                " else new." + AppConstant.SUPPLIER_OUTSTANDING_MONTH + " end || '-' || case when cast(new." + AppConstant.SUPPLIER_OUTSTANDING_DAY + " as integer) < 10 then '0' || new." + AppConstant.SUPPLIER_OUTSTANDING_DAY + " else new." + AppConstant.SUPPLIER_OUTSTANDING_DAY +
                " end as date from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_ID + " = old." + AppConstant.SUPPLIER_OUTSTANDING_ID + "), time('now', 'localtime' )) where " + AppConstant.SUPPLIER_OUTSTANDING_ID + " = old." + AppConstant.SUPPLIER_OUTSTANDING_ID + ";" +
                "end;");

        database.execSQL("create trigger if not exists AfterUpdateRefundUpdateTime after update on " + AppConstant.REFUND_TABLE_NAME + " when new." + AppConstant.REFUND_DATE + " <> old." + AppConstant.REFUND_DATE +
                " begin " +
                " update " + AppConstant.REFUND_TABLE_NAME + " set " + AppConstant.REFUND_TIME + " = " +
                "strftime('%s', (select new." + AppConstant.REFUND_YEAR + " || '-' || case when cast(new." + AppConstant.REFUND_MONTH + " as integer) < 10 then '0' || new." + AppConstant.REFUND_MONTH +
                " else new." + AppConstant.REFUND_MONTH + " end || '-' || case when cast(new." + AppConstant.REFUND_DAY + " as integer) < 10 then '0' || new." + AppConstant.REFUND_DAY + " else new." + AppConstant.REFUND_DAY +
                " end as date from " + AppConstant.REFUND_TABLE_NAME + " where " + AppConstant.REFUND_ID + " = old." + AppConstant.REFUND_ID + "), time('now', 'localtime' )) where " + AppConstant.REFUND_ID + " = old." + AppConstant.REFUND_ID + ";" +
                "end;");
        //database.execSQL("create trigger if not exists AfterInsertRefundInsertExpenseMgr after insert on " + AppConstant.REFUND_TABLE_NAME + " when (select " + AppConstant.SALES_TOTAL_AMOUNT + " from " +
        //AppConstant.SALES_TABLE_NAME + " ");

       /* database.execSQL("create trigger if not exists AfterUpdatePurchaseUpdateCustBal after update on " + AppConstant.PURCHASE_TABLE_NAME + " begin " +
        " update " + AppConstant.CUSTOMER_TABLE_NAME + " set " + AppConstant.CUSTOMER_BALANCE + " = " + AppConstant.CUSTOMER_BALANCE + " - new." + AppConstant.PU);*/

        /*database.execSQL("create trigger if not exists AfterUpdateStockInvQtyInsertAdjustment after update on " + AppConstant.STOCK_TABLE_NAME + " when old." + AppConstant.STOCK_INVENTORY_QTY + " <> new." +
                AppConstant.STOCK_INVENTORY_QTY + " begin " +
                " insert into " + AppConstant.ADJUSTMENT_TABLE_NAME + "(" + AppConstant.ADJUSTMENT_STOCK_ID + ", " + AppConstant.ADJUSTMENT_QTY + ", " + AppConstant.ADJUSTMENT_DAY + ", " +
                AppConstant.ADJUSTMENT_MONTH + ", " + AppConstant.ADJUSTMENT_YEAR + ", " + AppConstant.ADJUSTMENT_DATE + ", " + AppConstant.ADJUSTMENT_TIME + ") values (new." + AppConstant.STOCK_ID + ", old." +
                AppConstant.STOCK_INVENTORY_QTY + " - new." + AppConstant.STOCK_INVENTORY_QTY + " , strftime('%d', 'now'), strftime('%m', 'now'), strftime('%Y', 'now'), strftime('%Y%m%d', 'now'), strftime('%s', 'now', 'localtime'));"
                + " end;");*/
    }

    private void initializeToIdGenerator() {


        addNewID("BusinessSetting", 0);
        addNewID("Currency", 0);
        addNewID("CustomerOutstanding", 0);
        addNewID("Damage", 0);
        addNewID("Delivery", 0);
        addNewID("DocumentNumberSetting", 0);
        addNewID("Expense", 0);
        addNewID("ExpenseName", 0);
        addNewID("Lost", 0);
        addNewID("Pickup", 0);

        addNewID("Category", 0);
        addNewID("Unit", 0);
        addNewID("UserRole", 0);
        addNewID("Stock", 0);
        addNewID("StockImage", 0);
        addNewID("PurchaseDetail", 0);
        addNewID("PurchaseHold", 0);
        addNewID("PurchaseHoldDetail", 0);
        addNewID("Refund", 0);
        addNewID("SaleHoldDetails", 0);
        addNewID("SalesDetail", 0);
        addNewID("Sales", 0);
        addNewID("SupplierOutstanding", 0);
        addNewID("Purchase", 0);
        addNewID("Supplier", 0);
        addNewID("Hold", 0);
        addNewID("Discount", 0);

        //addNewID("LostDetail",0);

        //addNewID("DamageDetail",0);

        addNewID("Tax", 0);
        addNewID("Customer", 0);

        insertDefaultCategory();
        insertDefaultCustomer();
        insertDefaultSupplier();
        insertExpenseRefundType();
        insertDefaultTax();
        initializingMenus();
        insertDefaultDiscount();
        insertUnit();
        List<Currency> currencies = new ArrayList<>();

        currencies.add(new Currency(1l, "MMK", "Myanmar Kyat"));

        currencies.add(new Currency(2l, "$", "US Dollar"));

        insertCurrency(currencies);

        insertDefaultBusinessSetting();

        insertDefaultAccount();

        //insertSampleData();

    }

    private void insertCurrency(List<Currency> currencyList) {
        Long id = 1l;
        for (Currency currency : currencyList) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(AppConstant.CURRENCY_ID, id);
            contentValues.put(AppConstant.CURRENCY_NAME, currency.getDisplayName());
            contentValues.put(AppConstant.CURRENCY_IS_DEFAULT, currency.getStatus());
            contentValues.put(AppConstant.CURRENCY_SYMBOL, currency.getSign());
            database.insert(AppConstant.CURRENCY_TABLE_NAME, null, contentValues);
            id += 1;
        }
    }

    private void insertExpenseRefundType() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.EXPENSE_NAME_ID, 1);
        contentValues.put(AppConstant.EXPENSE_NAME_NAME, "Refund");
        contentValues.put(AppConstant.EXPENSE_NAME_TYPE, "Income");
        database.insert(AppConstant.EXPENSE_NAME_TABLE_NAME, null, contentValues);
    }

    private void insertDefaultCustomer() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.CUSTOMER_ID, 1);
        contentValues.put(AppConstant.CUSTOMER_NAME, "Default");
        contentValues.put(AppConstant.CUSTOMER_ADDRESS, "");
        contentValues.put(AppConstant.CUSTOMER_BALANCE, 0.0);
        database.insert(AppConstant.CUSTOMER_TABLE_NAME, null, contentValues);
    }

    private void insertDefaultTax() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.TAX_ID, 1);
        contentValues.put(AppConstant.TAX_NAME, "No Tax");
        contentValues.put(AppConstant.TAX_DESCRIPTION, "No Tax");
        contentValues.put(AppConstant.TAX_IS_DEFAULT, 1);
        contentValues.put(AppConstant.TAX_RATE, 0);
        contentValues.put(AppConstant.TAX_TYPE, "Inclusive");
        database.insert(AppConstant.TAX_TABLE_NAME, null, contentValues);
    }

    private void insertDefaultDiscount() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.DISCOUNT_ID, 1);
        contentValues.put(AppConstant.DISCOUNT_NAME, "No Discount");
        contentValues.put(AppConstant.DISCOUNT_IS_PERCENT, 1);
        contentValues.put(AppConstant.DISCOUNT_IS_DEFAULT, 1);
        contentValues.put(AppConstant.DISCOUNT_AMOUNT, 0);
        contentValues.put(AppConstant.DISCOUNT_DESCRIPTION, "No Discount");
        database.insert(AppConstant.DISCOUNT_TABLE_NAME, null, contentValues);
    }

    private void insertUnit() {
        String query = "insert into " + AppConstant.UNIT_TABLE_NAME + "(" + AppConstant.UNIT_ID + ", " + AppConstant.UNIT_UNIT + ", " + AppConstant.UNIT_DESCRIPTION
                + ", " + AppConstant.CREATED_DATE + ") values (1,'အိတ္','အိတ္',20180122), (2,'ထုပ္','ထုပ္',20180122), (3,'ပုလင္း','ပုလင္း',20180122), (4,'ခု','ခု',20180122), (5,'ကီလို','ကီလို',20180122), (6,'ဂရမ္','ဂရမ္',20180122)" +
                ", (7,'ဒါဇင္','ဒါဇင္',20180122), ( 8,'ေခ်ာင္း','ေခ်ာင္း',20180122), (9,'ဘူး','ဘူး',20180122), (10,'လီတာ','လီတာ',20180122), (11,'စံု','စံု',20180122), (12,'ေပါင္','ေပါင္',20180122),(13,'ကိုက္','ကိုက္',20180122);";
        database.execSQL(query);
    }

    private void insertDefaultAccount() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.USER_ID, 1);
        contentValues.put(AppConstant.USER_USER_NAME, "Admin");
        contentValues.put(AppConstant.USER_DESCRIPTION, "Admin");
        contentValues.put(AppConstant.USER_PASSWORD, "1236");
        contentValues.put(AppConstant.USER_ROLE, "Admin");
        database.insert(AppConstant.USER_TABLE_NAME, null, contentValues);
    }

    private void insertDefaultSupplier() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.SUPPLIER_ID, 1);
        contentValues.put(AppConstant.SUPPLIER_NAME, "Default");
        contentValues.put(AppConstant.SUPPLIER_ADDRESS, "");
        contentValues.put(AppConstant.SUPPLIER_BUSINESS_NAME, "Default");
        contentValues.put(AppConstant.SUPPLIER_BALANCE, 0.0);
        database.insert(AppConstant.SUPPLIER_TABLE_NAME, null, contentValues);
    }

    private void insertDefaultCategory() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.CATEGORY_ID, 1);
        contentValues.put(AppConstant.CATEGORY_NAME, "Default");
        contentValues.put(AppConstant.CATEGORY_DESCRIPTION, "for default");
        contentValues.put(AppConstant.CATEGORY_PARENT_ID, "");
        contentValues.put(AppConstant.CATEGORY_LEVEL, "");
        database.insert(AppConstant.CATEGORY_TABLE_NAME, null, contentValues);
    }

    private void insertDefaultBusinessSetting() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.BUSINESS_SETTING_ID, 1);
        contentValues.put(AppConstant.BUSINESS_SETTING_CURRENCY_ID, 1l);
        contentValues.put(AppConstant.BUSINESS_SETTING_VALUATION_METHOD, "AVG");
        database.insert(AppConstant.BUSINESS_SETTING_TABLE_NAME, null, contentValues);
    }

    private void initializingMenus() {
        Long id = addNewMenu("Damage");
        //addNewDocNum(id, "D", null, 4, 1);
        addNewDocNum(id, "D", null, 4, 1);
        id = addNewMenu("Lost");
        addNewDocNum(id, "L", null, 4, 1);
        //addNewDocNum(id, "L", null, 4, 1);
        id = addNewMenu("Sales");
        addNewDocNum(id, "S", null, 4, 1);
        //addNewDocNum(id, "S", null, 4, 1);
        id = addNewMenu("Purchase");
        addNewDocNum(id, "P", null, 4, 1);
        //addNewDocNum(id, "P", null, 4, 1);
        id = addNewMenu("Customer Outstanding");
        addNewDocNum(id, "CO", null, 4, 1);
        id = addNewMenu("Supplier Outstanding");
        addNewDocNum(id, "SO", null, 4, 1);
        //id = addNewMenu("Refund");
        //addNewDocNum(id, "R", null, 4, 1);
    }

    private boolean addNewID(String tableName, Integer value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION, tableName);
        contentValues.put(AppConstant.ID_GENERATOR_VALUE, value);
        database.insert(AppConstant.ID_GENERATOR_TABLE_NAME, null, contentValues);
        return true;
    }

    private Long addNewMenu(String name) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.MENU_NAME, name);
        return database.insert(AppConstant.MENU_TABLE_NAME, null, contentValues);
    }

    private boolean addNewDocNum(Long menuID, String prefix, String suffix, int minDigit, int nextNum) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.DOCUMENT_NUMBER_SETTING_MENU_ID, menuID);
        contentValues.put(AppConstant.DOCUMENT_NUMBER_SETTING_PREFIX, prefix);
        contentValues.put(AppConstant.DOCUMENT_NUMBER_SETTING_MIN_DIGIT, minDigit);
        contentValues.put(AppConstant.DOCUMENT_NUMBER_SETTING_SUFFIX, suffix);
        contentValues.put(AppConstant.DOCUMENT_NUMBER_SETTING_NEXT_NUMBER, nextNum);
        database.insert(AppConstant.DOCUMENT_NUMBER_SETTING_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean isTableExists(String tableName) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from sqlite_master where type='table' and tbl_name = '" + tableName + "'", null);
        if (cursor.moveToFirst()) {
            //Log.e("cursor",cursor.getCount()+" ");
            if (cursor.getCount() > 0) {
                cursor.close();
                //  db.close();
                //Log.e("TRUE",tableName+"TRUE");
                return true;
            }
            cursor.close();
            //db.close();
        }
        //db.close();
        //Log.e("False", tableName + "false");
        return false;
    }

//    public String restore(String inFile) throws IOException {
//
//        if (isExternalStorageReadable()) {
//            //String destination = "folder/source/";
//            String password = context.getString(R.string.title_name) + ";";
//            try {
//                ZipFile zipFile = new ZipFile(inFile);
//                if (zipFile.isEncrypted()) {
//                    zipFile.setPassword(password);
//                }
//                zipFile.extractAll(Environment.getExternalStorageDirectory().getPath());
//            } catch (ZipException e) {
//                e.printStackTrace();
//            }
//            inFile = inFile.replace(".zip", ".sqlite");
//            String backupDir = (new File(inFile)).getParent();
//            String inventoryDbName = backupDir + "/" + AppConstant.SHARED_PREFERENCE_DB;
//            String apiDbName = backupDir + "/" + AppConstant.API_DATABASE;
//
//            String inventoryDbOutFile = context.getFilesDir().getParent() + "/databases/" + AppConstant.SHARED_PREFERENCE_DB;
//            String apiDbOutFile = context.getFilesDir().getParent() + "/databases/" + AppConstant.API_DATABASE;
//            getHelperInstance(context);
//
//            FileInputStream fis;
//            try {
//                File dbFile = new File(inFile);
//                fileProcessor(Cipher.DECRYPT_MODE, context.getString(R.string.title_name), dbFile, dbFile);
//                fis = new FileInputStream(dbFile);
//                DatabaseHelper databaseHelper = new DatabaseHelper(context, inFile);
//                String[] tableNames = new String[]{AppConstant.ID_GENERATOR_TABLE_NAME, AppConstant.CATEGORY_TABLE_NAME, AppConstant.UNIT_TABLE_NAME, AppConstant.STOCK_TABLE_NAME,
//                        AppConstant.USER_TABLE_NAME, AppConstant.DAMAGE_TABLE_NAME,
//                        AppConstant.LOST_TABLE_NAME,
//                        //AppConstant.LOST_DETAIL_TABLE_NAME, AppConstant.DAMAGE_DETAIL_TABLE_NAME,
//                        AppConstant.MENU_TABLE_NAME,
//                        AppConstant.DOCUMENT_NUMBER_SETTING_TABLE_NAME, AppConstant.TAX_TABLE_NAME,
//                        AppConstant.BUSINESS_SETTING_TABLE_NAME, AppConstant.CUSTOMER_TABLE_NAME, AppConstant.SALES_TABLE_NAME,
//                        AppConstant.SALES_DETAIL_TABLE_NAME, AppConstant.DELIVERY_TABLE_NAME, AppConstant.PICKUP_TABLE_NAME,
//                        AppConstant.SUPPLIER_TABLE_NAME, AppConstant.PURCHASE_TABLE_NAME, AppConstant.PURCHASE_DETAIL_TABLE_NAME,
//                        AppConstant.EXPENSE_NAME_TABLE_NAME, AppConstant.EXPENSE_TABLE_NAME, AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME,
//                        AppConstant.SALES_HOLD_TABLE_NAME, AppConstant.SALES_HOLD_DETAIL_TABLE_NAME, AppConstant.PURCHASE_HOLD_TABLE_NAME, AppConstant.PURCHASE_HOLD_DETAIL_TABLE_NAME,
//                        AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME};
//
//                for (int i = 0; i < tableNames.length; i++) {
//                    if (!databaseHelper.isTableExists(tableNames[i])) {
//                        return "TableNotExists";
//                    }
//                }
//
//
//                // Open the empty db as the output stream
//                OutputStream output = new FileOutputStream(inventoryDbOutFile);
//
//                // Transfer bytes from the inputfile to the outputfile
//                byte[] buffer = new byte[1024];
//                int    length;
//                while ((length = fis.read(buffer)) > 0) {
//                    output.write(buffer, 0, length);
//                }
//                // Close the streams
//                output.flush();
//                output.close();
//                fis.close();
//                dbFile.delete();
//
//            } catch (FileNotFoundException e) {
//                return "FileNotFound";
//            }
//
//        }
//
//        Log.e("restore", "success");
//        return "success";
//    }


    /**
     * I kind of modified the original one instead of completely rewriting. So, this method still sucks.
     * But it works. So, ¯\_(ツ)_/¯ .
     */
    public String restore(String inFile) {
        if (isExternalStorageReadable()) {
            //String destination = "folder/source/";
            String password = context.getString(R.string.title_name) + ";";
            try {
                ZipFile zipFile = new ZipFile(inFile);
                if (zipFile.isEncrypted()) {
                    zipFile.setPassword(password);
                }
                zipFile.extractAll(Environment.getExternalStorageDirectory().getPath());
            } catch (ZipException e) {
                e.printStackTrace();
            }

            String backupDir = (new File(inFile)).getParent();

            String inventoryDbOutFile = context.getFilesDir().getParent() + "/databases/" + AppConstant.SHARED_PREFERENCE_DB;
            String apiDbOutFile = context.getFilesDir().getParent() + "/databases/" + AppConstant.API_DATABASE;
            getHelperInstance(context);

            FileInputStream fis;
            try {
                File inventoryDbFile = new File(Environment.getExternalStorageDirectory().getPath() + "/" + AppConstant.SHARED_PREFERENCE_DB);
                File apiDbFile = new File(Environment.getExternalStorageDirectory().getPath() + "/" + AppConstant.API_DATABASE);

                fileProcessor(Cipher.DECRYPT_MODE, context.getString(R.string.title_name), inventoryDbFile, inventoryDbFile);
                fileProcessor(Cipher.DECRYPT_MODE, context.getString(R.string.title_name), apiDbFile, apiDbFile);

                fis = new FileInputStream(inventoryDbFile);

                // Open the empty db as the output stream
                OutputStream output = new FileOutputStream(inventoryDbOutFile);

                // Transfer bytes from the inputfile to the outputfile
                byte[] buffer = new byte[1024];
                int length;
                while ((length = fis.read(buffer)) > 0) {
                    output.write(buffer, 0, length);
                }

                fis = new FileInputStream(apiDbFile);
                output = new FileOutputStream(apiDbOutFile);
                while ((length = fis.read(buffer)) > 0) {
                    output.write(buffer, 0, length);
                }
                // Close the streams
                output.flush();
                output.close();
                fis.close();
                inventoryDbFile.delete();
                apiDbFile.delete();
            } catch (FileNotFoundException e) {
                return "FileNotFound";
            } catch (IOException e) {
                e.printStackTrace();
                return "FileNotFound";
            }
        }

        return "success";
    }

    private void fileProcessor(int cipherMode, String key, File inputFile, File outputFile) {
        try {
            SecretKey secretKey = new SecretKeySpec(key.getBytes(), context.getString(R.string.algorithm));
            Cipher cipher = Cipher.getInstance(context.getString(R.string.algorithm));
            //            Log.e("secret key", new String(secretKey.getEncoded()));
            cipher.init(cipherMode, secretKey);


            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputStream.available()];
            inputStream.read(inputBytes);
            byte[] outputBytes = cipher.doFinal(inputBytes);

            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);
            inputStream.close();
            outputStream.close();

        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException e) {
            //            e.printStackTrace();
        }
    }


    public boolean clearAllData() {

        InsertedBooleanHolder flag = new InsertedBooleanHolder();
        databaseWriteTransaction(flag);
        try {
            //dropAllTriggers();

            dropTable(AppConstant.ADJUSTMENT_TABLE_NAME);
            dropTable(AppConstant.BUSINESS_SETTING_TABLE_NAME);
            dropTable(AppConstant.CURRENCY_TABLE_NAME);
            dropTable(AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME);
            dropTable(AppConstant.DAMAGE_TABLE_NAME);
            dropTable(AppConstant.DELIVERY_TABLE_NAME);
            dropTable(AppConstant.DOCUMENT_NUMBER_SETTING_TABLE_NAME);
            dropTable(AppConstant.EXPENSE_TABLE_NAME);
            dropTable(AppConstant.EXPENSE_NAME_TABLE_NAME);
            dropTable(AppConstant.ID_GENERATOR_TABLE_NAME);
            dropTable(AppConstant.LOST_TABLE_NAME);
            dropTable(AppConstant.PICKUP_TABLE_NAME);
            dropTable(AppConstant.MENU_TABLE_NAME);
            dropTable(AppConstant.PURCHASE_DETAIL_TABLE_NAME);
            dropTable(AppConstant.PURCHASE_HOLD_DETAIL_TABLE_NAME);
            dropTable(AppConstant.PURCHASE_HOLD_TABLE_NAME);
            dropTable(AppConstant.REFUND_TABLE_NAME);
            dropTable(AppConstant.SALES_HOLD_DETAIL_TABLE_NAME);
            dropTable(AppConstant.SALES_DETAIL_TABLE_NAME);
            dropTable(AppConstant.SALES_TABLE_NAME);
            dropTable(AppConstant.STOCK_TABLE_NAME);
            dropTable(AppConstant.STOCK_IMAGE_TABLE_NAME);
            dropTable(AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME);
            dropTable(AppConstant.UNIT_TABLE_NAME);
            dropTable(AppConstant.USER_TABLE_NAME);
            dropTable(AppConstant.PURCHASE_TABLE_NAME);
            dropTable(AppConstant.SUPPLIER_TABLE_NAME);
            dropTable(AppConstant.CATEGORY_TABLE_NAME);
            dropTable(AppConstant.SALES_HOLD_TABLE_NAME);
            dropTable(AppConstant.DISCOUNT_TABLE_NAME);
            dropTable(AppConstant.TAX_TABLE_NAME);
            dropTable(AppConstant.CUSTOMER_TABLE_NAME);
            database.setTransactionSuccessful();
        } catch (SQLiteException s) {
            s.printStackTrace();
        } finally {
            database.endTransaction();
            onCreate(database);
        }
        return flag.isInserted();
    }

    private void dropAllTriggers() throws SQLException {
        database = helperInstance.getWritableDatabase();
        database.beginTransaction();
        try {
            database.execSQL("DROP TRIGGER AfterDeleteCustOutstandPaymentUpdateCustBal;");
            database.execSQL("DROP TRIGGER AfterDeleteDamageUpdateStockQty;");
            database.execSQL("DROP TRIGGER AfterDeleteLostUpdateStockQty;");
            database.execSQL("DROP TRIGGER AfterDeletePurchaseDetailUpdateStockQty;");
            database.execSQL("DROP TRIGGER AfterDeletePurchaseUpdateSupBal;");
            database.execSQL("DROP TRIGGER AfterDeleteSalesDetailUpdateStockQty;");
            database.execSQL("DROP TRIGGER AfterDeleteSalesUpdateCustBal;");
            database.execSQL("DROP TRIGGER AfterDeleteSupOutstandPaymentUpdateSupBal;");
            database.execSQL("DROP TRIGGER AfterInsertBusinessSettingUpdateCurrency;");
            database.execSQL("DROP TRIGGER AfterInsertBusinessSettingUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertCategoryUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertCurrencyUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertCurrencyWhenActive;");
            database.execSQL("DROP TRIGGER AfterInsertCustOutstandPaymentUpdateCustBal;");
            database.execSQL("DROP TRIGGER AfterInsertCustomerOutstandingUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertCustomerUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertDamageUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertDamageUpdateStockQty;");
            database.execSQL("DROP TRIGGER AfterInsertDeliveryUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertDiscountUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertDiscountUpdateOtherDiscountDefault;");
            database.execSQL("DROP TRIGGER AfterInsertExpenseNameUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertHoldSalesDetailUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertHoldSalesUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertLostDetailUpdateStockQty;");
            database.execSQL("DROP TRIGGER AfterInsertPurchaseCheckBalForUpdateSupBal;");
            database.execSQL("DROP TRIGGER AfterInsertPurchaseDetailPriceUpdateStockPurchasePrice;");
            database.execSQL("DROP TRIGGER AfterInsertPurchaseDetailUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertPurchaseDetailUpdateStockQty;");
            database.execSQL("DROP TRIGGER AfterInsertPurchaseHoldDetailUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertPurchaseHoldUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertPurchaseUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertRefuncUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertRefundInsertExpenseMgr;");
            database.execSQL("DROP TRIGGER AfterInsertSaleDetailPriceUpdateStockRetailPrice;");
            database.execSQL("DROP TRIGGER AfterInsertSaleDetailUpdateStockQty;");
            database.execSQL("DROP TRIGGER AfterInsertSalesCheckBalForUpdateCustBal;");
            database.execSQL("DROP TRIGGER AfterInsertSalesDetailUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertSalesUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertStockImageUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertStockUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertSupOutstandPaymentUpdateSupBal;");
            database.execSQL("DROP TRIGGER AfterInsertSupplierOutstandingUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertSupplierUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertTaxUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertTaxUpdateOtherTaxDefault;");
            database.execSQL("DROP TRIGGER AfterInsertUnitUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterInsertUserRoleUpdateIdGen;");
            database.execSQL("DROP TRIGGER AfterUpdateBusinessSettingNotEqualCurrencyDefault;");
            database.execSQL("DROP TRIGGER AfterUpdateCurrencyWhenActive;");
            database.execSQL("DROP TRIGGER AfterUpdateCustOutstandPaymentUpdateCustBal;");
            database.execSQL("DROP TRIGGER AfterUpdateCustomerOutstandingUpdateTime;");
            database.execSQL("DROP TRIGGER AfterUpdateDamageUpdateStockQtyWithEquSID;");
            database.execSQL("DROP TRIGGER AfterUpdateDamageUpdateStockQtyWithNotEquSID;");
            database.execSQL("DROP TRIGGER AfterUpdateDamageUpdateTime;");
            database.execSQL("DROP TRIGGER AfterUpdateDeliveryUpdateTime;");
            database.execSQL("DROP TRIGGER AfterUpdateDiscountDefaultUpdateOtherDiscountDefault;");
            database.execSQL("DROP TRIGGER AfterUpdateExpenseUpdateTime;");
            database.execSQL("DROP TRIGGER AfterUpdateLostDetailUpdateStockQtyWithEquSID;");
            database.execSQL("DROP TRIGGER AfterUpdateLostUpdateStockQtyWithNotEquSID;");
            database.execSQL("DROP TRIGGER AfterUpdateLostUpdateTime;");
            database.execSQL("DROP TRIGGER AfterUpdatePurchaseDetailPriceUpdateStockPurchasePrice;");
            database.execSQL("DROP TRIGGER AfterUpdatePurchaseDetailUpdateStockQtyWithEquSID;");
            database.execSQL("DROP TRIGGER AfterUpdatePurchaseDetailUpdateStockQtyWithNotEquSID;");
            database.execSQL("DROP TRIGGER AfterUpdatePurchaseUpdateSupBalForEquSup;");
            database.execSQL("DROP TRIGGER AfterUpdatePurchaseUpdateSupBalForUnequSup;");
            database.execSQL("DROP TRIGGER AfterUpdatePurchaseUpdateTime;");
            database.execSQL("DROP TRIGGER AfterUpdateRefundUpdateTime;");
            database.execSQL("DROP TRIGGER AfterUpdateSaleDetailPriceUpdateStockRetailPrice;");
            database.execSQL("DROP TRIGGER AfterUpdateSaleTypeUpdateDelivery;");
            database.execSQL("DROP TRIGGER AfterUpdateSaleTypeUpdatePickup;");
            database.execSQL("DROP TRIGGER AfterUpdateSalesDetailUpdateStockQtyWithEquStockID;");
            database.execSQL("DROP TRIGGER AfterUpdateSalesDetailUpdateStockQtyWithNotEquStockID;");
            database.execSQL("DROP TRIGGER AfterUpdateSalesTypeCancelUpdateCustBal;");
            database.execSQL("DROP TRIGGER AfterUpdateSalesTypeNotCancelUpdateCustBal;");
            database.execSQL("DROP TRIGGER AfterUpdateSalesUpdateCustBalForEquCust;");
            database.execSQL("DROP TRIGGER AfterUpdateSalesUpdateCustBalForUnequCust;");
            database.execSQL("DROP TRIGGER AfterUpdateSalesUpdateTime;");
            database.execSQL("DROP TRIGGER AfterUpdateSupOutstandPaymentUpdateSupBal;");
            database.execSQL("DROP TRIGGER AfterUpdateTaxDefaultUpdateOtherTaxDefault;");
            database.execSQL("DROP TRIGGER BeforeDeleteCategoryChangeFKsToDefault;");
            database.execSQL("DROP TRIGGER BeforeDeleteCustomerChangeFKsToDefault;");
            database.execSQL("DROP TRIGGER BeforeDeleteDefaultCurrencyUpdateDefaultToOne;");
            database.execSQL("DROP TRIGGER BeforeDeleteDiscountChangeFKtoNull;");
            database.execSQL("DROP TRIGGER BeforeDeleteTaxChangeFKtoNull;");

            database.setTransactionSuccessful();
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            database.endTransaction();
        }
    }

    private boolean dropTable(String tableName) {
        String query = "DROP TABLE IF EXISTS " + tableName;
        database.execSQL(query);
        return true;
    }

    private boolean truncateData(String tableName) {
        String query = "delete from " + tableName;
        database.execSQL(query);
        query = "update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = 0 " + " where " + AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = ?";
        database.execSQL(query, new String[]{tableName});
        return true;
    }

    public void databaseWriteTransaction(final InsertedBooleanHolder status) {
        database = getWritableDatabase();
        database.beginTransactionWithListenerNonExclusive(new SQLiteTransactionListener() {
            @Override
            public void onBegin() {
                status.setInserted(false);
                Log.e("on", "begin");
            }

            @Override
            public void onCommit() {
                Log.e("on", "commit");
                status.setInserted(true);
            }

            @Override
            public void onRollback() {
                Log.e("on", "roll back");
                status.setInserted(false);
            }
        });
    }

    private void createSampleDatabase() {
        //        Log.e("fjaoifj", "path" + context.getDatabasePath(AppConstant.DATABASE_NAME).getParent());
        //        String dir = context.getDatabasePath(AppConstant.DATABASE_NAME).getParent();
        //        //String dir = DB_PATH + context.getPackageName();
        //        // Log.e("jj",outFile);
        //        //final String inFileName = context.getFilesDir().getParent() + "/databases/BusinessSuite.sqlite";
        //
        //        //final String inFileName = getDatabasePath()+"BusinessSuite.sqlite" ;
        //        //Log.e("iiii",inFileName);
        //        //File dbFile = new File(inFileName);
        //        //File sampleFile = new File(dir, AppConstant.SAMPLE_DATABASE_NAME);
        //        /*if (!dbFile.exists()) {
        //            Log.e("not", "exist");
        //            dbFile.mkdir();
        //        }*/
        //
        //        //Log.e("dir", sampleFile.getAbsolutePath());

        openDataBase();
        //        /*FileInputStream fis = new FileInputStream(dbFile);
        //        String state = Environment.getExternalStorageState();
        //
        //        FileOutputStream output;
        //        // Open the empty db as the output stream
        //
        //        try {
        //            output = new FileOutputStream(sampleFile);
        //            //Log.e("path",outFile);
        //        } catch (FileNotFoundException ex) {
        //            ex.printStackTrace();
        //
        //        }
        //        // Transfer bytes from the inputfile to the outputfile
        //        byte[] buffer = new byte[1024];
        //        int length;
        //        while ((length = fis.read(buffer)) > 0) {
        //            output.write(buffer, 0, length);
        //        }
        //        // Close the streams*/

    }

    public SQLiteDatabase openDataBase() throws SQLException {

        File dbFile = context.getDatabasePath(AppConstant.SAMPLE_DATABASE_NAME);
        //        Log.e("db path", context.getDatabasePath(AppConstant.DATABASE_NAME) + "");

        if (!dbFile.exists()) {
            Log.e("open", "open");
            try {
                CopyDataBaseFromAsset();
                //Log.e("database", "Copying sucess from Assets folder");
            } catch (IOException e) {
                // Log.e("database", "Copying fail from Assets folder");
                e.printStackTrace();
                throw new RuntimeException("Error creating source database", e);
            }
        }
        database = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY);
        return database;
    }

    public void CopyDataBaseFromAsset() throws IOException {
        //Log.e("in", "outfafa");
        InputStream myInput = context.getAssets().open("inventory_backup.sqlite");//inventory_backup copy
        //Log.e("in", "out");

        // Path to the just created empty db

        String outFileName = getDatabasePath();
        //String outFileName = DB_PATH  + context.getPackageName();


        //        String key = context.getString(R.string.title_name);
        //        try {
        //            Key secretKey = new SecretKeySpec(key.getBytes(), "RSA");
        //            Cipher cipher = Cipher.getInstance("RSA");
        //            cipher.init(Cipher.DECRYPT_MODE, secretKey);
        //
        ////            FileInputStream inputStream = new FileInputStream(inputFile);
        ////            byte[] inputBytes = new byte[(int) inputFile.length()];
        ////            inputStream.read(inputBytes);
        //
        //
        //            byte[] buffer = new byte[1024];
        //            int length;
        //            length = myInput.available();
        //
        //            byte[] inputBytes = new byte[length];
        //
        //            byte[] outputBytes = cipher.doFinal(inputBytes);
        //
        //
        //// if the path doesn't exist first, create it
        //            File f = new File(outFileName);
        //            if (!f.exists())
        //                f.mkdir();
        //
        //            Log.e("outfile", outFileName + " " + f.getAbsolutePath());
        //            // Open the empty db as the output stream
        //            OutputStream myOutput = new FileOutputStream(outFileName + AppConstant.SAMPLE_DATABASE_NAME);
        //
        //            // transfer bytes from the inputfile to the outputfile
        //
        //            myOutput.write(outputBytes);
        //
        //
        //            // Close the streams
        //            myOutput.flush();
        //            myOutput.close();
        //
        //        } catch (NoSuchPaddingException | NoSuchAlgorithmException
        //                | InvalidKeyException | BadPaddingException
        //                | IllegalBlockSizeException | IOException e) {
        //            e.printStackTrace();
        //        }
        //
        //        File f = new File(outFileName);
        //        if (!f.exists())
        //            f.mkdir();
        //
        //        Log.e("outfile", outFileName + " " + f.getAbsolutePath());


        // Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName + AppConstant.SAMPLE_DATABASE_NAME);

        // transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        File sampleDBFile = new File(outFileName + AppConstant.SAMPLE_DATABASE_NAME);
        //        if (sampleDBFile.exists()){
        //            Log.e("exists", "true" + sampleDBFile.getAbsolutePath());
        //        }
        fileProcessor(Cipher.DECRYPT_MODE, context.getString(R.string.title_name), sampleDBFile, sampleDBFile);

        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    private String getDatabasePath() {
        Log.e("db", "path");
        if (Build.VERSION.SDK_INT >= 17) {
            Log.e("oaeioa", context.getApplicationInfo().dataDir + DB_PATH_SUFFIX);
            return context.getApplicationInfo().dataDir + DB_PATH_SUFFIX;
        } else {
            return "/data/data/" + context.getPackageName() + DB_PATH_SUFFIX;
        }
    }

    public boolean shutDownHelper() {
        //Log.e("he", "lper");
        if (helperInstance != null) {
            //Log.e("hellp", "puppy");
            helperInstance = null;
            //helperInstance = new DatabaseHelper(context);
            //close();
        }
        return true;
    }

    public boolean createPurchasePriceTempTable(String startDate, String endDate) {
        //Log.e(startDate, endDate + " " + reportType);

        String query = "select " + AppConstant.BUSINESS_SETTING_VALUATION_METHOD + " from " + AppConstant.BUSINESS_SETTING_TABLE_NAME;
        Cursor cursor = null;
        final InsertedBooleanHolder flag = new InsertedBooleanHolder();
        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                String valuationMethod = cursor.getString(0);
                Log.e("value", valuationMethod);
                if (valuationMethod.equalsIgnoreCase("AVG")) {
                    createPurchaseTempTableForAVG(startDate, endDate);
                    createSalesTempTable();
                    insertSalesTempTableForAVG(startDate, endDate);
                } else {
                    createPurchaseTempTable();
                    if (valuationMethod.equalsIgnoreCase("LIFO")) {
                        insertPurchaseTempTableForLifo(startDate, endDate);
                    } else {
                        insertPurchaseTempTableForFIFO(startDate, endDate);
                    }
                    createSalesTempTable();
                    insertSalesTempTable(startDate, endDate);
                }
                createSalesPurchaseTempTable();
                insertSalePurchaseTemp();
            }
           /* cursor = database.rawQuery("select "
                    //+ AppConstant.SALES_PURCHASE_SALES_TEMP_ID + ", " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID + ", "
                    + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + ", " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
                    ", " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ", " + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " from " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME , null);
            if (cursor.moveToFirst()){
                do {
                    Log.e("type " + cursor.getString(0) + " qty " + cursor.getInt(1), "price " + cursor.getDouble(2) + " stockID " + cursor.getLong(3));
                }while (cursor.moveToNext());
                cursor.close();
            }*/
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return flag.isInserted();
    }

    public boolean createPurchasePriceTempTableForProfitAndLoss(String startDate, String endDate) {
        //Log.e("temp table " + startDate, endDate);
        String query = "select " + AppConstant.BUSINESS_SETTING_VALUATION_METHOD + " from " + AppConstant.BUSINESS_SETTING_TABLE_NAME;
        final InsertedBooleanHolder flag = new InsertedBooleanHolder();
        databaseWriteTransaction(flag);
        //Log.e("quer", query);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                String valuationMethod = cursor.getString(0);
                Log.e("value", valuationMethod);
                if (valuationMethod.equalsIgnoreCase("AVG")) {
                    createPurchaseTempTableForAVG(startDate, endDate);
                    //createSalesTempTable();
                    createTempTableForSalesForProfitLossForAVG(startDate, endDate);
                } else {
                    //                    database.execSQL("create temporary table " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " (" + AppConstant.PURCHASE_TEMP_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                    //                            + AppConstant.PURCHASE_TEMP_STOCK_ID + " integer, " + AppConstant.PURCHASE_TEMP_PRICE +
                    //                            " real, " + AppConstant.PURCHASE_TEMP_QTY + " integer, " + AppConstant.PURCHASE_TEMP_TIME + " integer);");
                    createPurchaseTempTable();
                    if (valuationMethod.equalsIgnoreCase("LIFO")) {
                        insertPurchaseTempTableForLifo(startDate, endDate);
                    } else {
                        insertPurchaseTempTableForFIFO(startDate, endDate);
                    }
                    createTempTableForSalesForProfitLoss(startDate, endDate);
                    //                    /*database.execSQL("create temporary table " + AppConstant.SALES_TEMP_TABLE_NAME + " as select * from ( select stockID, 0.0 price, (inventoryQty - sum(qty)) qty, 0 detailID, 0 time, 'a' as type from (" +
                    //                                    "select sum(qty) qty, stockID from Purchase p, PurchaseDetail where purchaseID = p.id and date >= ? and date <= ? group by stockID  union all  " +
                    //                                    "select -sum(qty) qty, stockID from Sales s, SalesDetail where salesID = s.id and date >= ? and date <= ?  group by stockID union all " +
                    //                                    "select -sum(stockQty) qty, stockID from Damage where date >= ? and date <= ?  group by stockID union all " +
                    //                                    "select -sum(stockQty) qty, stockID from Lost where date >= ? and date <= ?  group by stockID)" +
                    //                                    "as q, Stock s where " +
                    //                                    "s.id = q.stockID group by stockID  ) order by stockID asc;",
                    //                            new String []{startDate, endDate, startDate, endDate, startDate, endDate, startDate, endDate});*/
                }
                database.execSQL("create temp table " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " (" + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID +
                        " integer, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + " integer, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + " real " +
                        ", " + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " integer, " + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + " integer, " +
                        AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID + " integer," + AppConstant.SALES_TEMP_TYPE + " text );");

                database.execSQL("create temporary trigger AfterInsertOnSalePurPrice after insert on  " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME +
                        " begin " +
                        "update " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " set  " + AppConstant.PURCHASE_TEMP_QTY + " = " + AppConstant.PURCHASE_TEMP_QTY + " - new." +
                        AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + " where " + AppConstant.PURCHASE_TEMP_ID + " = new." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID + ";" +
                        "update " + AppConstant.SALES_TEMP_TABLE_NAME + " set  salesTempQty = salesTempQty - new." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
                        " where " + AppConstant.SALES_TEMP_ID + " = new." + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + ";" +
                        "end;");
                Log.e("diaj", "kdjfkadjf");
                insertSalePurchaseTempTableForProfitLoss();
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
            Log.e("temp", "commit");
        }
        return flag.isInserted();
    }

    public boolean createPurchasePriceTempTableForValuation(String endDate) {
        //Log.e(startDate, endDate + " " + reportType);

        String query = "select " + AppConstant.BUSINESS_SETTING_VALUATION_METHOD + " from " + AppConstant.BUSINESS_SETTING_TABLE_NAME;
        Cursor cursor = null;
        final InsertedBooleanHolder flag = new InsertedBooleanHolder();
        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                String valuationMethod = cursor.getString(0);
                //Log.e("value", valuationMethod);
                if (valuationMethod.equalsIgnoreCase("AVG")) {
                    createPurchaseTempTableForValuationAVG("0", endDate);
                    createSalesTempTable();
                    insertSalesTempTableValuationAVG(endDate);
                } else {
                    createPurchaseTempTable();
                    if (valuationMethod.equalsIgnoreCase("LIFO")) {
                        insertPurchaseTempTableForLifoValuation(endDate);
                    } else {
                        insertPurchaseTempTableForFIFOValuation(endDate);
                    }
                    createSalesTempTable();
                    insertSalesTempTableValuation(endDate);
                }
                createSalesPurchaseTempTableForValuation();
                insertSalePurchaseTemp();
            }
           /* cursor = database.rawQuery("select "
                    //+ AppConstant.SALES_PURCHASE_SALES_TEMP_ID + ", " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID + ", "
                    + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + ", " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
                    ", " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ", " + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " from " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME , null);
            if (cursor.moveToFirst()){
                do {
                    Log.e("type " + cursor.getString(0) + " qty " + cursor.getInt(1), "price " + cursor.getDouble(2) + " stockID " + cursor.getLong(3));
                }while (cursor.moveToNext());
                cursor.close();
            }*/
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return flag.isInserted();
    }

    private void createSalesPurchaseTempTable() {
        database.execSQL("create   table " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " (" + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + " integer, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID +
                " integer, " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " text, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + " integer, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + " real " +
                ", " + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " integer, " + AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID + " integer);");
        //                    Log.e("dkja ", "create temporary  table " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " (" + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + " integer, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID +
        //                            " integer, " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " text, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + " integer, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + " real " +
        //                            ", " + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " integer);")
        database.execSQL("create temporary trigger AfterInsertOnSalePurPrice after insert on  " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME +
                " begin " +
                "update " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " set  " + AppConstant.PURCHASE_TEMP_QTY + " = " + AppConstant.PURCHASE_TEMP_QTY + " - new." +
                AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + " where " + AppConstant.PURCHASE_TEMP_ID + " = new." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID + ";" +
                "update " + AppConstant.SALES_TEMP_TABLE_NAME + " set  " + AppConstant.SALES_TEMP_QTY + " = " + AppConstant.SALES_TEMP_QTY + " - new." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
                " where " + AppConstant.SALES_TEMP_ID + " = new." + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + ";" +
                "end;");
        //                    Log.e("kdjfakdf", "create temporary trigger AfterInsertOnSalePurPrice after insert on  " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME +
        //                            " begin " +
        //                            "update " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " set  " + AppConstant.PURCHASE_TEMP_QTY + " = " + AppConstant.PURCHASE_TEMP_QTY + " - new." +
        //                            AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + " where " + AppConstant.PURCHASE_TEMP_ID + " = new." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID + ";" +
        //                            "update " + AppConstant.SALES_TEMP_TABLE_NAME + " set  " + AppConstant.SALES_TEMP_QTY + " = " + AppConstant.SALES_TEMP_QTY + " - new." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
        //                            " where " + AppConstant.SALES_TEMP_ID + " = new." + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + ";" +
        //                            "end;");
    }

    private void createSalesPurchaseTempTableForValuation() {
        database.execSQL("create temporary  table " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " (" + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + " integer, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID +
                " integer, " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " text, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + " integer, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + " real " +
                ", " + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " integer, " + AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID + " integer);");
        //                    Log.e("dkja ", "create temporary  table " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " (" + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + " integer, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID +
        //                            " integer, " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " text, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + " integer, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + " real " +
        //                            ", " + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " integer);")
        database.execSQL("create temporary trigger AfterInsertOnSalePurPrice after insert on  " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME +
                " begin " +
                "update " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " set  " + AppConstant.PURCHASE_TEMP_QTY + " = ifnull(" + AppConstant.PURCHASE_TEMP_QTY + ", 0) - abs(new." +
                AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + ") where " + AppConstant.PURCHASE_TEMP_ID + " = new." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID + ";" +
                "update " + AppConstant.SALES_TEMP_TABLE_NAME + " set  " + AppConstant.SALES_TEMP_QTY + " = " + AppConstant.SALES_TEMP_QTY + " - abs(new." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
                ") where " + AppConstant.SALES_TEMP_ID + " = new." + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + ";" +
                "end;");
        //                    Log.e("kdjfakdf", "create temporary trigger AfterInsertOnSalePurPrice after insert on  " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME +
        //                            " begin " +
        //                            "update " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " set  " + AppConstant.PURCHASE_TEMP_QTY + " = " + AppConstant.PURCHASE_TEMP_QTY + " - new." +
        //                            AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + " where " + AppConstant.PURCHASE_TEMP_ID + " = new." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID + ";" +
        //                            "update " + AppConstant.SALES_TEMP_TABLE_NAME + " set  " + AppConstant.SALES_TEMP_QTY + " = " + AppConstant.SALES_TEMP_QTY + " - new." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
        //                            " where " + AppConstant.SALES_TEMP_ID + " = new." + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + ";" +
        //                            "end;");
    }

    private void insertPurchaseTempTableForFIFO(String startDate, String endDate) {
        database.execSQL("insert into " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " (" + AppConstant.PURCHASE_TEMP_STOCK_ID + ", " + AppConstant.PURCHASE_TEMP_PRICE + ", "
                + AppConstant.PURCHASE_TEMP_QTY + ", " + AppConstant.PURCHASE_TEMP_TIME + ")" + "select * from (select " +
                AppConstant.PURCHASE_DETAIL_STOCK_ID + ", " + AppConstant.PURCHASE_DETAIL_PRICE + ", " + AppConstant.PURCHASE_DETAIL_QTY + ", " +
                AppConstant.PURCHASE_TIME + " from " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID +
                " = p." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DATE + " >= ? and  " + AppConstant.PURCHASE_DATE + " <= ? " +
                " union all " +
                "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_OPENING_PRICE + ", " + AppConstant.STOCK_OPENING_QTY //+ ", strftime('%s', substr(" + AppConstant.STOCK_OPENING_DATE + ", 1,4) || '-' || " +
                //"substr(" + AppConstant.STOCK_OPENING_DATE + ",5, 2)  || '-' || substr(" + AppConstant.STOCK_OPENING_DATE + ", 7)  || ' 00:00:00')  as time" +
                + ", " + AppConstant.STOCK_OPENING_TIME + " from " + AppConstant.STOCK_TABLE_NAME
                + " union all select " + AppConstant.ADJUSTMENT_STOCK_ID + ", " + AppConstant.ADJUSTMENT_PRICE + ", -" + AppConstant.ADJUSTMENT_QTY +
                ", " + AppConstant.ADJUSTMENT_TIME + " from " + AppConstant.ADJUSTMENT_TABLE_NAME + " where " + AppConstant.ADJUSTMENT_DATE + " >= ? and " + AppConstant.ADJUSTMENT_DATE +
                " <= ? and " + AppConstant.ADJUSTMENT_QTY + " < 0 union all " +
                "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_PURCHASE_PRICE + ",  null, '' from " + AppConstant.STOCK_TABLE_NAME +
                ") t order by " +
                AppConstant.PURCHASE_TEMP_STOCK_ID + " asc, " + AppConstant.PURCHASE_TEMP_TIME + " asc", new String[]{startDate, endDate, startDate, endDate});
    }

    private void insertPurchaseTempTableForFIFOValuation(String endDate) {
        database.execSQL("insert into " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " (" + AppConstant.PURCHASE_TEMP_STOCK_ID + ", " + AppConstant.PURCHASE_TEMP_PRICE + ", "
                + AppConstant.PURCHASE_TEMP_QTY + ", " + AppConstant.PURCHASE_TEMP_TIME + ")" + "select * from (select " +
                AppConstant.PURCHASE_DETAIL_STOCK_ID + ", " + AppConstant.PURCHASE_DETAIL_PRICE + ", " + AppConstant.PURCHASE_DETAIL_QTY + ", " +
                AppConstant.PURCHASE_TIME + " from " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID +
                " = p." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DATE + " <= ? " +
                " union all select " + AppConstant.STOCK_ID + " , " + AppConstant.STOCK_OPENING_PRICE + ", " + AppConstant.STOCK_OPENING_QTY + ", " + AppConstant.STOCK_OPENING_TIME +
                " from " + AppConstant.STOCK_TABLE_NAME
                + " union all select " + AppConstant.ADJUSTMENT_STOCK_ID + ", " + AppConstant.ADJUSTMENT_PRICE + ", -" + AppConstant.ADJUSTMENT_QTY +
                ", " + AppConstant.ADJUSTMENT_TIME + " from " + AppConstant.ADJUSTMENT_TABLE_NAME + " where " + AppConstant.ADJUSTMENT_DATE +
                " <= ? and " + AppConstant.ADJUSTMENT_QTY + " < 0 union all " +
                "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_PURCHASE_PRICE + ",  null, '' from " + AppConstant.STOCK_TABLE_NAME +
                ") t order by " +
                AppConstant.PURCHASE_TEMP_STOCK_ID + " asc, " + AppConstant.PURCHASE_TEMP_TIME + " asc", new String[]{endDate, endDate});
    }

    private void createPurchaseTempTableForAVG(String startDate, String endDate) {
        //        String query = " CREATE TEMP TABLE if not exists " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " AS " +
        //                " select " + AppConstant.PURCHASE_DETAIL_STOCK_ID + ", round(sum(" + AppConstant.PURCHASE_DETAIL_QTY + " * " + AppConstant.PURCHASE_DETAIL_PRICE + ")/sum(" +
        //                AppConstant.PURCHASE_DETAIL_QTY + "), 2) as " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ", 'a' as " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + ", null as "
        //                + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + ", null as " + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + " from " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + ", " + AppConstant.PURCHASE_TABLE_NAME +
        //                " p where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID +
        //                " and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? group by " + AppConstant.PURCHASE_DETAIL_STOCK_ID +
        //                " union " +
        //                " select distinct " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_OPENING_PRICE + ", 'a', null, null from " + AppConstant.STOCK_TABLE_NAME + " s where not exists " +
        //                " (select * from " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, " + AppConstant.PURCHASE_TABLE_NAME + " p where " +
        //                "d." + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and d." + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID +
        //                " and " + AppConstant.PURCHASE_DATE + " >= ?  and " + AppConstant.PURCHASE_DATE + " <= ?);";
        //
        //        String query = " CREATE TEMP TABLE if not exists " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " AS " +"select stockID, round(sum(total_price)/sum(total_qty), 2) " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ", 'a' as salesTempType,null as purchaseTempQty, null as salesTempID, null as detailID, 1 sort_order from ( " +
        //                "select stockID, sum(qty * price) as total_price, sum(qty) total_qty from PurchaseDetail, Purchase p where purchaseID = p.id and date >= ? and  date  <= ? group by  " +
        //                "stockID " +
        //                "union select id, openingQty * openingPrice, openingQty from Stock where openingDate between ? and ? group by id " +
        //                "union select stockID, sum(price * abs(qty)), sum(abs(qty)) from Adjustment where date >= ? and  date  <= ? and qty < 0 group by stockID " +
        //                "union select stockID, -sum(price * qty), -sum(qty) from Adjustment where date >= ? and  date  <= ? and qty > 0 group by stockID) group by stockID " +
        //                "union  select distinct id, openingPrice, 'a', null, null, null, 2 from Stock s order by stockID, sort_order;";
        //        Log.e("query", query);
        createPurchaseTempTable();
        String query = "insert into PurchaseTemp (stockID, price, qty, time)  " +
                "select stockID, round(sum(total_price)/sum(total_qty), 2) price, sum(total_qty), 1 time from ( " +
                "select stockID, sum(qty * price) as total_price, sum(qty) total_qty from PurchaseDetail, Purchase p where purchaseID = p.id and date >= ? and  date  <= ? group by  " +
                "stockID  " +
                "union all select id, openingQty * openingPrice, openingQty from Stock  " +
                "union all select stockID, sum(price * abs(qty)), sum(abs(qty)) from Adjustment where date >= ? and  date  <= ? and qty < 0 group by stockID " +
                "union all select stockID, -sum(price * qty), -sum(qty) from Adjustment where date >= ? and  date  <= ? and qty > 0 group by stockID) group by stockID " +
                "union all select id,purchasePrice, null, 3 from Stock order by stockID, time;";
        database.execSQL(query, new String[]{startDate, endDate, startDate, endDate, startDate, endDate});
    }

    private void createPurchaseTempTableForValuationAVG(String startDate, String endDate) {
        //        String query = " CREATE TEMP TABLE if not exists " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " AS " +
        //                " select " + AppConstant.PURCHASE_DETAIL_STOCK_ID + ", round(sum(" + AppConstant.PURCHASE_DETAIL_QTY + " * " + AppConstant.PURCHASE_DETAIL_PRICE + ")/sum(" +
        //                AppConstant.PURCHASE_DETAIL_QTY + "), 2) as " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ", 'a' as " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + ", null as "
        //                + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + ", null as " + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + " from " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + ", " + AppConstant.PURCHASE_TABLE_NAME +
        //                " p where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID +
        //                " and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? group by " + AppConstant.PURCHASE_DETAIL_STOCK_ID +
        //                " union " +
        //                " select distinct " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_OPENING_PRICE + ", 'a', null, null from " + AppConstant.STOCK_TABLE_NAME + " s where not exists " +
        //                " (select * from " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, " + AppConstant.PURCHASE_TABLE_NAME + " p where " +
        //                "d." + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and d." + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID +
        //                " and " + AppConstant.PURCHASE_DATE + " >= ?  and " + AppConstant.PURCHASE_DATE + " <= ?);";
        //
        //        String query = " CREATE TEMP TABLE if not exists " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " AS " +"select stockID, round(sum(total_price)/sum(total_qty), 2) " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ", 'a' as salesTempType,null as purchaseTempQty, null as salesTempID, null as detailID, 1 sort_order from ( " +
        //                "select stockID, sum(qty * price) as total_price, sum(qty) total_qty from PurchaseDetail, Purchase p where purchaseID = p.id and date >= ? and  date  <= ? group by  " +
        //                "stockID " +
        //                "union select id, openingQty * openingPrice, openingQty from Stock where openingDate between ? and ? group by id " +
        //                "union select stockID, sum(price * abs(qty)), sum(abs(qty)) from Adjustment where date >= ? and  date  <= ? and qty < 0 group by stockID " +
        //                "union select stockID, -sum(price * qty), -sum(qty) from Adjustment where date >= ? and  date  <= ? and qty > 0 group by stockID) group by stockID " +
        //                "union  select distinct id, openingPrice, 'a', null, null, null, 2 from Stock s order by stockID, sort_order;";
        //        Log.e("query", query);
        createPurchaseTempTable();
        String query = "insert into PurchaseTemp (stockID, price, qty, time)  " +
                "select stockID, round(sum(total_price)/sum(total_qty), 2) price, sum(total_qty), 1 time from ( " +
                "select stockID, sum(qty * price) as total_price, sum(qty) total_qty from PurchaseDetail, Purchase p where purchaseID = p.id and date >= ? and  date  <= ? group by  " +
                "stockID  " +
                "union all select id, openingQty * openingPrice, openingQty from Stock where openingDate between ? and ? group by id " +
                "union all select stockID, sum(price * abs(qty)), sum(abs(qty)) from Adjustment where date >= ? and  date  <= ? and qty < 0 group by stockID " +
                "union all select stockID, -sum(price * qty), -sum(qty) from Adjustment where date >= ? and  date  <= ? and qty > 0 group by stockID) group by stockID " +
                "union all select id,purchasePrice, null, 2 from Stock order by stockID, time;";
        database.execSQL(query, new String[]{startDate, endDate, startDate, endDate, startDate, endDate, startDate, endDate});
    }

    private void insertPurchaseTempTableForLifo(String startDate, String endDate) {
        String a = "insert into " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " ( " + AppConstant.PURCHASE_TEMP_STOCK_ID + ", " + AppConstant.PURCHASE_TEMP_PRICE + ", " + AppConstant.PURCHASE_TEMP_QTY + ", " +
                AppConstant.PURCHASE_TEMP_TIME + ")select * from (select " + AppConstant.PURCHASE_DETAIL_STOCK_ID + ", " + AppConstant.PURCHASE_DETAIL_PRICE + ", " + AppConstant.PURCHASE_DETAIL_QTY + ", " +
                AppConstant.PURCHASE_TIME + " from " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME +
                " d where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DATE + " between ? and ? union all\n" +
                " select " + AppConstant.STOCK_ID + " , " + AppConstant.STOCK_OPENING_PRICE + ", " + AppConstant.STOCK_OPENING_QTY + ", " + AppConstant.STOCK_OPENING_TIME +
                " from " + AppConstant.STOCK_TABLE_NAME +
                " union all select " + AppConstant.ADJUSTMENT_STOCK_ID + ", " + AppConstant.ADJUSTMENT_PRICE + ", -" + AppConstant.ADJUSTMENT_QTY +
                ", " + AppConstant.ADJUSTMENT_TIME + " from " + AppConstant.ADJUSTMENT_TABLE_NAME + " where " + AppConstant.ADJUSTMENT_DATE + " >= ? and " + AppConstant.ADJUSTMENT_DATE +
                " <= ? and " + AppConstant.ADJUSTMENT_QTY + " < 0 " +
                " union all " + "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_PURCHASE_PRICE + ",  null, 0  as time from " + AppConstant.STOCK_TABLE_NAME +
                ") t order by " + AppConstant.PURCHASE_TEMP_STOCK_ID + " asc, time desc";

        database.execSQL(a, new String[]{startDate, endDate, startDate, endDate});


        //                        Log.e("lifo", "insert into " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " (" + AppConstant.PURCHASE_TEMP_STOCK_ID + ", " + AppConstant.PURCHASE_TEMP_PRICE + ", "
        //                                + AppConstant.PURCHASE_TEMP_QTY + ", " + AppConstant.PURCHASE_TEMP_TIME + ")" + "select * from (select " +
        //                                AppConstant.PURCHASE_DETAIL_STOCK_ID + ", " + AppConstant.PURCHASE_DETAIL_PRICE + ", " + AppConstant.PURCHASE_DETAIL_QTY + ", " +
        //                                AppConstant.PURCHASE_TIME + " from " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID +
        //                                " = p." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DATE + " >= ? and  " + AppConstant.PURCHASE_DATE + "  <= ? " +
        //                                " union " +
        //                                "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_OPENING_PRICE + ",  null, strftime('%s', substr(" + AppConstant.STOCK_OPENING_DATE + ", 1,4) || '-' || " +
        //                                "substr(" + AppConstant.STOCK_OPENING_DATE + ",5, 2)  || '-' || substr(" + AppConstant.STOCK_OPENING_DATE + ", 7)  || ' 00:00:00')  as time from " + AppConstant.STOCK_TABLE_NAME +
        //                                " union select " + AppConstant.ADJUSTMENT_STOCK_ID + ", " + AppConstant.ADJUSTMENT_PRICE + ", -" + AppConstant.ADJUSTMENT_QTY +
        //                                ", " + AppConstant.ADJUSTMENT_TIME + " from " + AppConstant.ADJUSTMENT_TABLE_NAME + " where " + AppConstant.ADJUSTMENT_DATE + " >= ? and " + AppConstant.ADJUSTMENT_DATE +
        //                                " <= ? and " + AppConstant.ADJUSTMENT_QTY + " < 0 " + " union " +
        //                                "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_PURCHASE_PRICE + ",  null, 0  as time from " + AppConstant.STOCK_TABLE_NAME +
        //                                ") t order by " +
        //                                AppConstant.PURCHASE_TEMP_STOCK_ID + " asc, " + AppConstant.PURCHASE_TEMP_TIME + " desc");
    }

    private void insertPurchaseTempTableForLifoValuation(String endDate) {
        String a = "insert into " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " ( " + AppConstant.PURCHASE_TEMP_STOCK_ID + ", " + AppConstant.PURCHASE_TEMP_PRICE + ", " + AppConstant.PURCHASE_TEMP_QTY + ", " +
                AppConstant.PURCHASE_TEMP_TIME + ")select * from (select " + AppConstant.PURCHASE_DETAIL_STOCK_ID + ", " + AppConstant.PURCHASE_DETAIL_PRICE + ", " + AppConstant.PURCHASE_DETAIL_QTY + ", " +
                AppConstant.PURCHASE_TIME + " from " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME +
                " d where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DATE + " <= ? union all \n" +
                " select " + AppConstant.STOCK_ID + " , " + AppConstant.STOCK_OPENING_PRICE + ", " + AppConstant.STOCK_OPENING_QTY + ", " + AppConstant.STOCK_OPENING_TIME +
                " from " + AppConstant.STOCK_TABLE_NAME +
                " union all select " + AppConstant.ADJUSTMENT_STOCK_ID + ", " + AppConstant.ADJUSTMENT_PRICE + ", -" + AppConstant.ADJUSTMENT_QTY +
                ", " + AppConstant.ADJUSTMENT_TIME + " from " + AppConstant.ADJUSTMENT_TABLE_NAME + " where " + AppConstant.ADJUSTMENT_DATE +
                " <= ? and " + AppConstant.ADJUSTMENT_QTY + " < 0 " +
                " union all " + "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_PURCHASE_PRICE + ",  null, 0  as time from " + AppConstant.STOCK_TABLE_NAME +
                ") t order by " + AppConstant.PURCHASE_TEMP_STOCK_ID + " asc, time desc";

        database.execSQL(a, new String[]{endDate, endDate});


        //                        Log.e("lifo", "insert into " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " (" + AppConstant.PURCHASE_TEMP_STOCK_ID + ", " + AppConstant.PURCHASE_TEMP_PRICE + ", "
        //                                + AppConstant.PURCHASE_TEMP_QTY + ", " + AppConstant.PURCHASE_TEMP_TIME + ")" + "select * from (select " +
        //                                AppConstant.PURCHASE_DETAIL_STOCK_ID + ", " + AppConstant.PURCHASE_DETAIL_PRICE + ", " + AppConstant.PURCHASE_DETAIL_QTY + ", " +
        //                                AppConstant.PURCHASE_TIME + " from " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID +
        //                                " = p." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DATE + " >= ? and  " + AppConstant.PURCHASE_DATE + "  <= ? " +
        //                                " union " +
        //                                "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_OPENING_PRICE + ",  null, strftime('%s', substr(" + AppConstant.STOCK_OPENING_DATE + ", 1,4) || '-' || " +
        //                                "substr(" + AppConstant.STOCK_OPENING_DATE + ",5, 2)  || '-' || substr(" + AppConstant.STOCK_OPENING_DATE + ", 7)  || ' 00:00:00')  as time from " + AppConstant.STOCK_TABLE_NAME +
        //                                " union select " + AppConstant.ADJUSTMENT_STOCK_ID + ", " + AppConstant.ADJUSTMENT_PRICE + ", -" + AppConstant.ADJUSTMENT_QTY +
        //                                ", " + AppConstant.ADJUSTMENT_TIME + " from " + AppConstant.ADJUSTMENT_TABLE_NAME + " where " + AppConstant.ADJUSTMENT_DATE + " >= ? and " + AppConstant.ADJUSTMENT_DATE +
        //                                " <= ? and " + AppConstant.ADJUSTMENT_QTY + " < 0 " + " union " +
        //                                "select " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_PURCHASE_PRICE + ",  null, 0  as time from " + AppConstant.STOCK_TABLE_NAME +
        //                                ") t order by " +
        //                                AppConstant.PURCHASE_TEMP_STOCK_ID + " asc, " + AppConstant.PURCHASE_TEMP_TIME + " desc");
    }

    private void createSalesTempTable() {
        database.execSQL("create temp table " + AppConstant.SALES_TEMP_TABLE_NAME + " (" + AppConstant.SALES_TEMP_ID + " integer not null primary key autoincrement, " + AppConstant.SALES_TEMP_QTY + " integer, " +
                AppConstant.SALES_TEMP_TIME + " integer, " + AppConstant.SALES_TEMP_STOCK_ID + " integer, " + AppConstant.SALES_TEMP_TYPE + " text, " + AppConstant.SALES_TEMP_DETAIL_ID + " integer);");
        //        Log.e("kda", "create temp table " + AppConstant.SALES_TEMP_TABLE_NAME + " (" + AppConstant.SALES_TEMP_ID + " integer not null primary key autoincrement, " + AppConstant.SALES_TEMP_QTY + " integer, " +
        //                AppConstant.SALES_TEMP_TIME + " integer, " + AppConstant.SALES_TEMP_STOCK_ID + " integer, " + AppConstant.SALES_TEMP_TYPE + " text);");
        //        Log.e("dkjfa", "insert into " + AppConstant.SALES_TEMP_TABLE_NAME + "(" + AppConstant.SALES_TEMP_QTY + ", time, stockID, type) select qty,  time, stockID, type from(" + " select " + AppConstant.SALES_DETAIL_QTY +
        //                ", " + AppConstant.SALES_TIME + ", d." + AppConstant.SALES_DETAIL_STOCK_ID + ", 's' " + AppConstant.SALES_TEMP_TYPE + " from " +
        //                AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.SALES_DETAIL_TABLE_NAME + " d where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE +
        //                " >= ? and " + AppConstant.SALES_DATE + " <= ? " //+ ") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc "
        //                + " union " + //"select qty,  time, stockID, type from(" +
        //                " select " + AppConstant.DAMAGE_STOCK_QTY + " as qty, "
        //                + AppConstant.DAMAGE_TIME + ", " + AppConstant.DAMAGE_STOCK_ID + ", 'd' as type from " + AppConstant.DAMAGE_TABLE_NAME + " where " + AppConstant.DAMAGE_DATE + " >= ? and " + AppConstant.DAMAGE_DATE + " <= ? " +
        //                //") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc " +
        //                " union " + //"select qty,  time, stockID, type from( " +
        //                "select " + AppConstant.LOST_STOCK_QTY + " as qty, " + AppConstant.LOST_TIME + ", " + AppConstant.LOST_STOCK_ID + ", 'l' as type from " + AppConstant.LOST_TABLE_NAME +
        //                " where " + AppConstant.LOST_DATE + " >= ? and " + AppConstant.LOST_DATE + " <= ?) order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc;"
        //                + " union " //+ "select qty,  time, stockID, type from( " +
        //                + " select " + AppConstant.STOCK_INVENTORY_QTY + " as qty, " + "0 as time, " + AppConstant.STOCK_ID + " as stockID" + ", 'st' as type from " +
        //                AppConstant.STOCK_TABLE_NAME + ") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, time asc ");
        //        if(reportType.equalsIgnoreCase(AppConstant.SALES_TABLE_NAME)){
        //            database.execSQL("insert into " + AppConstant.SALES_TEMP_TABLE_NAME + "(" + AppConstant.SALES_TEMP_QTY + ", time, stockID, type) select qty,  time, stockID, type from(" + " select " + AppConstant.SALES_DETAIL_QTY +
        //                             ", " + AppConstant.SALES_TIME + "d." + AppConstant.SALES_DETAIL_STOCK_ID + ", 's' " + AppConstant.SALES_TEMP_TYPE + " from " +
        //                            AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.SALES_DETAIL_TABLE_NAME + " d where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE +
        //                            " >= ? and " + AppConstant.SALES_DATE + " <= ?) order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc;",
        //                    new String []{startDate, endDate});
        //        } else if (reportType.equalsIgnoreCase(AppConstant.DAMAGE_TABLE_NAME)){
        //            database.execSQL("insert into " + AppConstant.SALES_TEMP_TABLE_NAME + "(" + AppConstant.SALES_TEMP_QTY + ", time, stockID, type) select qty,  time, stockID, type from( select " + AppConstant.DAMAGE_STOCK_QTY + " as qty, "
        //                            + AppConstant.DAMAGE_TIME+ AppConstant.DAMAGE_STOCK_ID + ", 'd' as type from " + AppConstant.DAMAGE_TABLE_NAME + " where " + AppConstant.DAMAGE_DATE + " >= ? and " + AppConstant.DAMAGE_DATE + " <= ? ) order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc;",
        //                    new String []{startDate, endDate});
        //        } else if (reportType.equalsIgnoreCase(AppConstant.LOST_TABLE_NAME)){
        //            database.execSQL("insert into " + AppConstant.SALES_TEMP_TABLE_NAME + "(" + AppConstant.SALES_TEMP_QTY + ", time, stockID, type) select qty,  time, stockID, type from( select "
        //                            + AppConstant.LOST_STOCK_QTY + " as qty, " + AppConstant.LOST_TIME + ", " + AppConstant.LOST_STOCK_ID + ", 'l' as type from " + AppConstant.LOST_TABLE_NAME +
        //                            " where " + AppConstant.LOST_DATE + " >= ? and " + AppConstant.LOST_DATE + " <= ?) order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc;",
        //                    new String []{startDate, endDate});
        //        } else if (reportType.equalsIgnoreCase(AppConstant.STOCK_TABLE_NAME)){
        //            database.execSQL("insert into " + AppConstant.SALES_TEMP_TABLE_NAME + "(" + AppConstant.SALES_TEMP_QTY + ", time, stockID, type) select qty,  time, stockID, type from( select " + AppConstant.STOCK_INVENTORY_QTY + " as qty, "
        //                    + "0 as time, " + AppConstant.STOCK_ID + " as stockID" + ", 'st' as type from " +
        //                    AppConstant.STOCK_TABLE_NAME + ") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc");
        //        }
    }

    private void insertSalesTempTable(String startDate, String endDate) {
        String query = "insert into " + AppConstant.SALES_TEMP_TABLE_NAME + "(" + AppConstant.SALES_TEMP_QTY + ", " + AppConstant.SALES_TEMP_TIME + ", " + AppConstant.SALES_TEMP_STOCK_ID + ", " +
                AppConstant.SALES_TEMP_TYPE + ", " + AppConstant.SALES_TEMP_DETAIL_ID + ") select qty,  time, stockID, type, id from(" + " select " + AppConstant.SALES_DETAIL_QTY +
                ", " + AppConstant.SALES_TIME + ", d." + AppConstant.SALES_DETAIL_STOCK_ID + ", 's' as type, d." + AppConstant.SALES_DETAIL_ID + " from " +
                AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.SALES_DETAIL_TABLE_NAME + " d where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE +
                " >= ? and " + AppConstant.SALES_DATE + " <= ? " //+ ") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc "
                + " union " + //"select qty,  time, stockID, type from(" +
                " select " + AppConstant.DAMAGE_STOCK_QTY + " as qty, "
                + AppConstant.DAMAGE_TIME + ", " + AppConstant.DAMAGE_STOCK_ID + ", 'd', " + AppConstant.DAMAGE_ID + " from " + AppConstant.DAMAGE_TABLE_NAME + " where " + AppConstant.DAMAGE_DATE + " >= ? and " + AppConstant.DAMAGE_DATE + " <= ? " +
                //") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc " +
                " union " + //"select qty,  time, stockID, type from( " +
                "select " + AppConstant.LOST_STOCK_QTY + ", " + AppConstant.LOST_TIME + ", " + AppConstant.LOST_STOCK_ID + ", 'l', " + AppConstant.LOST_ID + " from " + AppConstant.LOST_TABLE_NAME +
                " where " + AppConstant.LOST_DATE + " >= ? and " + AppConstant.LOST_DATE + " <= ? "
                + " union " //+ "select qty,  time, stockID, type from( " +
                + " select " + AppConstant.ADJUSTMENT_QTY + " as qty, " + AppConstant.ADJUSTMENT_TIME + ", " + AppConstant.ADJUSTMENT_STOCK_ID + ", 'aj', 0 as type from " +
                AppConstant.ADJUSTMENT_TABLE_NAME + " where " + AppConstant.ADJUSTMENT_DATE + " >= ? and " + AppConstant.ADJUSTMENT_DATE + " <= ? and " + AppConstant.ADJUSTMENT_QTY + " > 0) order by " +
                AppConstant.SALES_TEMP_TIME + " asc ";
        database.execSQL(query, new String[]{startDate, endDate, startDate, endDate, startDate, endDate, startDate, endDate});
    }

    private void insertSalesTempTableForAVG(String startDate, String endDate) {
        String query = "insert into " + AppConstant.SALES_TEMP_TABLE_NAME + "(" + AppConstant.SALES_TEMP_QTY + ", " + AppConstant.SALES_TEMP_TIME + ", " + AppConstant.SALES_TEMP_STOCK_ID + ", " +
                AppConstant.SALES_TEMP_TYPE + ", " + AppConstant.SALES_TEMP_DETAIL_ID + ") select qty,  time, stockID, type, id from(" + " select " + AppConstant.SALES_DETAIL_QTY +
                ", " + AppConstant.SALES_TIME + ", d." + AppConstant.SALES_DETAIL_STOCK_ID + ", 's' as type, d." + AppConstant.SALES_DETAIL_ID + " from " +
                AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.SALES_DETAIL_TABLE_NAME + " d where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE +
                " >= ? and " + AppConstant.SALES_DATE + " <= ? " //+ ") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc "
                + " union all " + //"select qty,  time, stockID, type from(" +
                " select " + AppConstant.DAMAGE_STOCK_QTY + " as qty, "
                + AppConstant.DAMAGE_TIME + ", " + AppConstant.DAMAGE_STOCK_ID + ", 'd', " + AppConstant.DAMAGE_ID + " from " + AppConstant.DAMAGE_TABLE_NAME + " where " + AppConstant.DAMAGE_DATE + " >= ? and " + AppConstant.DAMAGE_DATE + " <= ? " +
                //") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc " +
                " union all " + //"select qty,  time, stockID, type from( " +
                "select " + AppConstant.LOST_STOCK_QTY + ", " + AppConstant.LOST_TIME + ", " + AppConstant.LOST_STOCK_ID + ", 'l', " + AppConstant.LOST_ID + " from " + AppConstant.LOST_TABLE_NAME +
                " where " + AppConstant.LOST_DATE + " >= ? and " + AppConstant.LOST_DATE + " <= ? "
                + ") order by " +
                AppConstant.SALES_TEMP_TIME + " asc ";
        database.execSQL(query, new String[]{startDate, endDate, startDate, endDate, startDate, endDate});
    }

    private void insertSalesTempTableValuation(String endDate) {
        String query = "insert into " + AppConstant.SALES_TEMP_TABLE_NAME + "(" + AppConstant.SALES_TEMP_QTY + ", " + AppConstant.SALES_TEMP_TIME + ", " + AppConstant.SALES_TEMP_STOCK_ID + ", " +
                AppConstant.SALES_TEMP_TYPE + ", " + AppConstant.SALES_TEMP_DETAIL_ID + ") select qty,  time, stockID, type, id from(" + " select " + AppConstant.SALES_DETAIL_QTY +
                ", " + AppConstant.SALES_TIME + ", d." + AppConstant.SALES_DETAIL_STOCK_ID + ", 's' as type, d." + AppConstant.SALES_DETAIL_ID + " from " +
                AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.SALES_DETAIL_TABLE_NAME + " d where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE +
                " <= ? " //+ ") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc "
                + " union all " + //"select qty,  time, stockID, type from(" +
                " select " + AppConstant.DAMAGE_STOCK_QTY + " as qty, "
                + AppConstant.DAMAGE_TIME + ", " + AppConstant.DAMAGE_STOCK_ID + ", 'd', " + AppConstant.DAMAGE_ID + " from " + AppConstant.DAMAGE_TABLE_NAME + " where " + AppConstant.DAMAGE_DATE + " <= ? " +
                //") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc " +
                " union all " + //"select qty,  time, stockID, type from( " +
                "select " + AppConstant.LOST_STOCK_QTY + ", " + AppConstant.LOST_TIME + ", " + AppConstant.LOST_STOCK_ID + ", 'l', " + AppConstant.LOST_ID + " from " + AppConstant.LOST_TABLE_NAME +
                " where " + AppConstant.LOST_DATE + " <= ? "
                + " union all " //+ "select qty,  time, stockID, type from( " +
                + " select " + AppConstant.ADJUSTMENT_QTY + " as qty, " + AppConstant.ADJUSTMENT_TIME + ", " + AppConstant.ADJUSTMENT_STOCK_ID + ", 'aj', 0 as type from " +
                AppConstant.ADJUSTMENT_TABLE_NAME + " where " + AppConstant.ADJUSTMENT_DATE + " <= ? and " + AppConstant.ADJUSTMENT_QTY + " > 0) order by " +
                AppConstant.SALES_TEMP_TIME + " asc ";
        database.execSQL(query, new String[]{endDate, endDate, endDate, endDate});
    }

    private void insertSalesTempTableValuationAVG(String endDate) {
        String query = "insert into " + AppConstant.SALES_TEMP_TABLE_NAME + "(" + AppConstant.SALES_TEMP_QTY + ", " + AppConstant.SALES_TEMP_TIME + ", " + AppConstant.SALES_TEMP_STOCK_ID + ", " +
                AppConstant.SALES_TEMP_TYPE + ", " + AppConstant.SALES_TEMP_DETAIL_ID + ") select qty,  time, stockID, type, id from(" + " select " + AppConstant.SALES_DETAIL_QTY +
                ", " + AppConstant.SALES_TIME + ", d." + AppConstant.SALES_DETAIL_STOCK_ID + ", 's' as type, d." + AppConstant.SALES_DETAIL_ID + " from " +
                AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.SALES_DETAIL_TABLE_NAME + " d where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE +
                " <= ? " //+ ") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc "
                + " union all " + //"select qty,  time, stockID, type from(" +
                " select " + AppConstant.DAMAGE_STOCK_QTY + " as qty, "
                + AppConstant.DAMAGE_TIME + ", " + AppConstant.DAMAGE_STOCK_ID + ", 'd', " + AppConstant.DAMAGE_ID + " from " + AppConstant.DAMAGE_TABLE_NAME + " where " + AppConstant.DAMAGE_DATE + " <= ? " +
                //") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc " +
                " union all " + //"select qty,  time, stockID, type from( " +
                "select " + AppConstant.LOST_STOCK_QTY + ", " + AppConstant.LOST_TIME + ", " + AppConstant.LOST_STOCK_ID + ", 'l', " + AppConstant.LOST_ID + " from " + AppConstant.LOST_TABLE_NAME +
                " where " + AppConstant.LOST_DATE + " <= ? "
                + ") order by " +
                AppConstant.SALES_TEMP_TIME + " asc ";
        database.execSQL(query, new String[]{endDate, endDate, endDate});
    }

    private void insertSalePurchaseTemp() {
        String selection = "select " + AppConstant.SALES_TEMP_ID + " from " + AppConstant.SALES_TEMP_TABLE_NAME + " where " + AppConstant.SALES_TEMP_QTY + " <> 0 limit 2";
        String insertion = "insert   into " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME +
                " select s." + AppConstant.SALES_TEMP_ID + ", p." + AppConstant.PURCHASE_TEMP_ID + //", case when s." + AppConstant.SALES_TEMP_QTY + " < ifnull(p." +
                // AppConstant.PURCHASE_TEMP_QTY + ", s." + AppConstant.SALES_TEMP_QTY + "+1) then s." + AppConstant.SALES_TEMP_QTY + " else  p." + AppConstant.PURCHASE_TEMP_QTY + " end " + AppConstant.SALES_TEMP_QTY + "," +
                ", " + AppConstant.SALES_TEMP_TYPE + ", case when s." + AppConstant.SALES_TEMP_QTY + " < ifnull(abs(p." + AppConstant.PURCHASE_TEMP_QTY + "), s." + AppConstant.SALES_TEMP_QTY + "+1) then s." + AppConstant.SALES_TEMP_QTY +
                " else  p." + AppConstant.PURCHASE_TEMP_QTY + " end purQty, p." + AppConstant.PURCHASE_TEMP_PRICE + ", s." + AppConstant.SALES_TEMP_STOCK_ID + ", " + AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID +
                " from " + AppConstant.SALES_TEMP_TABLE_NAME + " s," + AppConstant.PURCHASE_TEMP_TABLE_NAME + " p where s." + AppConstant.SALES_TEMP_QTY + " <> 0 and (p." +
                AppConstant.PURCHASE_TEMP_QTY + " <> 0 or p." + AppConstant.PURCHASE_TEMP_QTY + "  is null)" +
                " and s." + AppConstant.SALES_TEMP_STOCK_ID + " = p." + AppConstant.PURCHASE_TEMP_STOCK_ID +
                 " order by s." + AppConstant.SALES_TEMP_ID + " asc, p." +
                AppConstant.PURCHASE_TEMP_ID + " asc limit 1";

        do {

            if (cursor != null)
                cursor.close();
            cursor = database.rawQuery(selection, null);
            //cursor = database.rawQuery("select id from CostOfGoodSoldQtyTemp where costOfGoodSoldQty <> 0 limit 1", null);
            //Log.e("s fadfaffaf", "6");
            // Log.e(cursor.getCount()+ " cursor", "idoa");
            if (cursor.moveToFirst()) {
                //Log.e("oei", "oier");
                database.execSQL(insertion);
            }
            //}
            // Log.e("count", cursor.getCount() + " oidfjaj f");
        } while (cursor.moveToFirst());

        if (cursor != null)
            cursor.close();
        Log.e("oidjfid", "ieojioa");
    }

    private void insertSalePurchaseTempTableForProfitLoss() {
        String b = "select id from " + AppConstant.SALES_TEMP_TABLE_NAME + " where " + AppConstant.SALES_TEMP_QTY + " <> 0 limit 2";
        //Log.e("s", b);
        String a = "insert into " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME +
                " select p." + AppConstant.PURCHASE_TEMP_ID +
                ", case when s." + AppConstant.SALES_TEMP_QTY + " < ifnull(p." + AppConstant.PURCHASE_TEMP_QTY + ", s." + AppConstant.SALES_TEMP_QTY + " + 1) then s." + AppConstant.SALES_TEMP_QTY + " else  p." +
                AppConstant.PURCHASE_TEMP_QTY + " end purQty, p." + AppConstant.PURCHASE_TEMP_PRICE + ", s." + AppConstant.SALES_TEMP_STOCK_ID + ", s." + AppConstant.SALES_TEMP_ID +
                ", " + AppConstant.SALES_TEMP_DETAIL_ID + ", s." + AppConstant.SALES_TEMP_TYPE + " from " + AppConstant.SALES_TEMP_TABLE_NAME + " s," + AppConstant.PURCHASE_TEMP_TABLE_NAME + " p where s." + AppConstant.SALES_TEMP_QTY + " <> 0 and (p." +
                AppConstant.PURCHASE_TEMP_QTY + " <> 0 or p." + AppConstant.PURCHASE_TEMP_QTY + "  is null)" +
                " and s." + AppConstant.SALES_TEMP_STOCK_ID + " = p." + AppConstant.PURCHASE_TEMP_STOCK_ID  + " order by s.id asc, p." +
                AppConstant.PURCHASE_TEMP_ID + " asc limit 1";
        //Log.e("kdjfka", a);
        do {
            if (cursor != null)
                cursor.close();
            cursor = database.rawQuery(b, null);
            if (cursor.moveToFirst()) {
                //  Log.e("cur", cursor.getLong(1) + "d");
                //Log.e("oei", "oier");
                database.execSQL(a);
            }
        } while (cursor.moveToFirst());
        if (cursor != null)
            cursor.close();
        //Log.e("after", "finish yay \n ****************************************************************");
    }

    private void createTempTableForSalesForProfitLoss(String startDate, String endDate) {
        //        /*String query = "create temp table OpeningTemp as\n" +
        //                "select sum(qty) opening, id from (\n" +
        //                "select openingQty as qty, id from Stock where openingDate <= ? union all\n" +
        //                "select sum(qty), stockID from Purchase p, PurchaseDetail where purchaseID = p.id and date < ? group by stockID union all\n" +
        //                "select -sum(qty), stockID from Sales s, SalesDetail where salesID = s.id and date < ? group by stockID union all\n" +
        //                "select -sum(stockQty), stockID from Damage where date < ? group by stockID union all\n" +
        //                "select -sum(stockQty), stockID from Lost where date < ? group by stockID union all\n" +
        //                "select sum(qty), stockID from Adjustment where date < ? group by stockID \n" +
        //                ") group by id";
        //
        //
        //        database.execSQL(query, new String[]{startDate, startDate, startDate, startDate, startDate, startDate});*/
        database.execSQL("create temp table " + AppConstant.SALES_TEMP_TABLE_NAME + " (" + AppConstant.SALES_TEMP_ID + " integer not null primary key autoincrement, " + AppConstant.SALES_TEMP_QTY + " integer, " +
                AppConstant.SALES_TEMP_TIME + " integer, " + AppConstant.SALES_TEMP_STOCK_ID + " integer, " + AppConstant.SALES_TEMP_DETAIL_ID + " integer, " + AppConstant.SALES_TEMP_TYPE + " text );");
        String query = "insert into " + AppConstant.SALES_TEMP_TABLE_NAME + " (" + AppConstant.SALES_TEMP_QTY + ", " + AppConstant.SALES_TEMP_TIME + " , " + AppConstant.SALES_TEMP_STOCK_ID +
                ", " + AppConstant.SALES_TEMP_DETAIL_ID +" , " + AppConstant.SALES_TEMP_TYPE + "  ) " +
                "select " + AppConstant.SALES_TEMP_QTY + ", " + AppConstant.SALES_TEMP_TIME + ", " + AppConstant.SALES_TEMP_STOCK_ID + ", id ," +  AppConstant.SALES_TEMP_TYPE + " from( " +
                "select qty as " + AppConstant.SALES_TEMP_QTY + ", stockID, time, d.id , 's' type from Sales s, SalesDetail d where salesID = s.id and date between ? and ? " +
                " union  all select stockQty, stockID , time , id , 'd'  from Damage where date between ? and ? group by stockID union all\n" +
                "select stockQty, stockID ,time , id , 'l' from Lost where date between ? and ? group by stockID union all\n"
                + " select " + AppConstant.ADJUSTMENT_QTY + ", " + AppConstant.ADJUSTMENT_STOCK_ID + ", " + AppConstant.ADJUSTMENT_TIME + ", 0 , 'aj' from " +
                AppConstant.ADJUSTMENT_TABLE_NAME + " where " + AppConstant.ADJUSTMENT_DATE + " >= ? and " + AppConstant.ADJUSTMENT_DATE + " <= ? and " + AppConstant.ADJUSTMENT_QTY +
                " >0 ) order by time asc ";
        //"select opening as qty, id from OpeningTemp union all\n" +
        //"select sum(qty), stockID from Purchase p, PurchaseDetail where purchaseID = p.id and p.date between ? and ? group by stockID union all\n" +
        //"select -opening, id from OpeningTemp union all\n" +
        //"select -sum(qty), stockID from Purchase p, PurchaseDetail where purchaseID = p.id and p.date between ? and ? group by stockID union all\n" +
        //"select -sum(qty), stockID from Adjustment where date between ? and ? group by stockID) group by id;"


        database.execSQL(query, new String[]{startDate, endDate, startDate, endDate , startDate, endDate ,startDate, endDate});//, startDate, endDate, startDate, endDate, startDate, endDate, startDate, endDate, startDate, endDate});

    }

    private void createTempTableForSalesForProfitLossForAVG(String startDate, String endDate) {
        //        /*String query = "create temp table OpeningTemp as\n" +
        //                "select sum(qty) opening, id from (\n" +
        //                "select openingQty as qty, id from Stock where openingDate <= ? union all\n" +
        //                "select sum(qty), stockID from Purchase p, PurchaseDetail where purchaseID = p.id and date < ? group by stockID union all\n" +
        //                "select -sum(qty), stockID from Sales s, SalesDetail where salesID = s.id and date < ? group by stockID union all\n" +
        //                "select -sum(stockQty), stockID from Damage where date < ? group by stockID union all\n" +
        //                "select -sum(stockQty), stockID from Lost where date < ? group by stockID union all\n" +
        //                "select sum(qty), stockID from Adjustment where date < ? group by stockID \n" +
        //                ") group by id";
        //
        //
        //        database.execSQL(query, new String[]{startDate, startDate, startDate, startDate, startDate, startDate});*/
        database.execSQL("create  table " + AppConstant.SALES_TEMP_TABLE_NAME + " (" + AppConstant.SALES_TEMP_ID + " integer not null primary key autoincrement, " + AppConstant.SALES_TEMP_QTY + " integer, " +
                AppConstant.SALES_TEMP_TIME + " integer, " + AppConstant.SALES_TEMP_STOCK_ID + " integer, " + AppConstant.SALES_TEMP_DETAIL_ID + " integer, " + AppConstant.SALES_TEMP_TYPE + " text );");

        String query = "insert into " + AppConstant.SALES_TEMP_TABLE_NAME + " (" +
                            AppConstant.SALES_TEMP_QTY + ", " +
                            AppConstant.SALES_TEMP_TIME + " , " +
                            AppConstant.SALES_TEMP_STOCK_ID + ", " +
                            AppConstant.SALES_TEMP_DETAIL_ID + "," +
                            AppConstant.SALES_TEMP_TYPE + " ) " +
                "select * from ( " +
                    "select qty as " + AppConstant.SALES_TEMP_QTY + ", time, stockID, d.id ,'s' type from Sales s, SalesDetail d where salesID = s.id and date between ? and ? " +
                    " union all select stockQty, time, stockID , id , 'd' type from Damage where date between ? and ? " +
                    " union  all select stockQty, time , stockID ,id , 'l' type from Lost where date between ? and ? ) " +
                " order by time asc";

        //"select -sum(qty), stockID from Adjustment where date between ? and ? group by stockID) group by id;"


        database.execSQL(query, new String[]{startDate, endDate, startDate, endDate, startDate, endDate});//, startDate, endDate, startDate, endDate, startDate, endDate, startDate, endDate, startDate, endDate});


    }

    public boolean dropPurchasePriceTempTable() {
        final InsertedBooleanHolder flag = new InsertedBooleanHolder();
        database = getWritableDatabase();
        database.beginTransactionWithListenerNonExclusive(new SQLiteTransactionListener() {
            @Override
            public void onBegin() {
                flag.setInserted(false);
                Log.e("on", "begin");
            }

            @Override
            public void onCommit() {
                Log.e("on", "commit");
                flag.setInserted(true);
            }

            @Override
            public void onRollback() {
                Log.e("on", "roll back");
                flag.setInserted(false);
            }
        });
        try {
            database.execSQL("drop table if exists " + AppConstant.SALES_TEMP_TABLE_NAME);
            database.execSQL("drop table if exists " + AppConstant.PURCHASE_TEMP_TABLE_NAME);
            database.execSQL("drop table if exists " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME);
            database.execSQL("drop trigger if exists AfterInsertOnSalePurPrice;");
            //database.execSQL("drop table if exists OpeningTemp");
            //database.execSQL("drop table if exists CostOfGoodSoldQtyTemp");
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean createPurchasePriceTempTableForStockBalance(String startDate, String endDate) {
        //Log.e(startDate, endDate + " " + reportType);

        String query = "select " + AppConstant.BUSINESS_SETTING_VALUATION_METHOD + " from " + AppConstant.BUSINESS_SETTING_TABLE_NAME;
        Cursor cursor = null;
        final InsertedBooleanHolder flag = new InsertedBooleanHolder();
        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                String valuationMethod = cursor.getString(0);
                Log.e("value", valuationMethod);
                if (valuationMethod.equalsIgnoreCase("AVG")) {
                    createPurchaseTempTableForAVG(startDate, endDate);
                } else {
                    createPurchaseTempTable();
                    if (valuationMethod.equalsIgnoreCase("LIFO")) {
                        insertPurchaseTempTableForLifo(startDate, endDate);
                    } else {
                        insertPurchaseTempTableForFIFO(startDate, endDate);
                    }
                    insertSalesTempTableForStockBal(startDate, endDate);
                    createSalesPurchaseTempTable();
                    insertSalePurchaseTemp();
                }
            }
           /* cursor = database.rawQuery("select "
                    //+ AppConstant.SALES_PURCHASE_SALES_TEMP_ID + ", " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_ID + ", "
                    + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + ", " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
                    ", " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ", " + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " from " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME , null);
            if (cursor.moveToFirst()){
                do {
                    Log.e("type " + cursor.getString(0) + " qty " + cursor.getInt(1), "price " + cursor.getDouble(2) + " stockID " + cursor.getLong(3));
                }while (cursor.moveToNext());
                cursor.close();
            }*/
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return flag.isInserted();
    }

    private void insertSalesTempTableForStockBal(String startDate, String endDate) {
        String query = "insert into " + AppConstant.SALES_TEMP_TABLE_NAME + "(" + AppConstant.SALES_TEMP_QTY + ", time, stockID, type) select qty,  time, stockID, type from(" + " select " + AppConstant.SALES_DETAIL_QTY +
                ", " + AppConstant.SALES_TIME + ", d." + AppConstant.SALES_DETAIL_STOCK_ID + ", 's' " + AppConstant.SALES_TEMP_TYPE + " from " +
                AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.SALES_DETAIL_TABLE_NAME + " d where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE +
                " >= ? and " + AppConstant.SALES_DATE + " <= ? " //+ ") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc "
                + " union " + //"select qty,  time, stockID, type from(" +
                " select " + AppConstant.DAMAGE_STOCK_QTY + " as qty, "
                + AppConstant.DAMAGE_TIME + ", " + AppConstant.DAMAGE_STOCK_ID + ", 'd' as type from " + AppConstant.DAMAGE_TABLE_NAME + " where " + AppConstant.DAMAGE_DATE + " >= ? and " + AppConstant.DAMAGE_DATE + " <= ? " +
                //") order by " + AppConstant.SALES_DETAIL_STOCK_ID + " asc, " + AppConstant.SALES_TIME + " asc " +
                " union " + //"select qty,  time, stockID, type from( " +
                "select " + AppConstant.LOST_STOCK_QTY + " as qty, " + AppConstant.LOST_TIME + ", " + AppConstant.LOST_STOCK_ID + ", 'l' as type from " + AppConstant.LOST_TABLE_NAME +
                " where " + AppConstant.LOST_DATE + " >= ? and " + AppConstant.LOST_DATE + " <= ? "
                + " union " //+ "select qty,  time, stockID, type from( " +
                + " select " + AppConstant.ADJUSTMENT_QTY + " as qty, " + AppConstant.ADJUSTMENT_TIME + ", " + AppConstant.ADJUSTMENT_STOCK_ID + ", 'aj' as type from " +
                AppConstant.ADJUSTMENT_TABLE_NAME + " where " + AppConstant.ADJUSTMENT_DATE + " >= ? and " + AppConstant.ADJUSTMENT_DATE + " <= ? and " + AppConstant.ADJUSTMENT_QTY + " > 0" +
                " union select " + AppConstant.STOCK_INVENTORY_QTY + " , '', " + AppConstant.STOCK_ID + ", 'st' from  " + AppConstant.STOCK_TABLE_NAME + ") order by " +
                AppConstant.SALES_DETAIL_STOCK_ID + " asc, time asc ";
        database.execSQL(query, new String[]{startDate, endDate});
    }

    private void createPurchaseTempTable() {
        database.execSQL("create temp table " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " (" + AppConstant.PURCHASE_TEMP_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                + AppConstant.PURCHASE_TEMP_STOCK_ID + " integer, " + AppConstant.PURCHASE_TEMP_PRICE +
                " real, " + AppConstant.PURCHASE_TEMP_QTY + " integer, " + AppConstant.PURCHASE_TEMP_TIME + " integer);");
    }

    // private void insertSampleData(){
    //        final InsertedBooleanHolder flag = new InsertedBooleanHolder();
    //        //database = getWritableDatabase();
    //        database.beginTransactionWithListenerNonExclusive(new SQLiteTransactionListener() {
    //            @Override
    //            public void onBegin() {
    //                flag.setInserted(false);
    //                Log.e("on","begin daf");
    //            }
    //
    //            @Override
    //            public void onCommit() {
    //                Log.e("on","commit adf");
    //                flag.setInserted(true);
    //            }
    //
    //            @Override
    //            public void onRollback() {
    //                Log.e("on","roll back adfa");
    //                flag.setInserted(false);
    //            }
    //        });
    //        try {
    //            database.execSQL("insert into Category(id, name, description) values(2, 'Perfumes', ''), (3, 'Skin Care', ''), (4, 'Face Makeup', ''), (5, 'Hair Care', ''), (6, 'Nail Care', ''), " +
    //                    "(7,'Eye Makeup', ''), (8,'Lips Makeup',''), (9, 'Speedo', ''), (10, 'Sneakers', '');");
    //            database.execSQL("insert into Unit(id, unit, description) values" +
    //                    "(1, 'pcs', 'pices')," +
    //                    "(2, 'btl', 'bottle');");
    //            database.execSQL("insert into Stock(id, codeNo, name, categoryID, unitID, inventoryQty, reorderQty, purchasePrice, wholesalePrice, retailPrice, description)" +
    //                    "values" +
    //                    "(1, 'st0001', 'Hunputa Multi Shape Sponges New', 4, 1, 20, 10, 4000, 4500, 5000, 'Comes with 9PCS Sponge with random Colors')," +
    //                    "(2, 'st0002', 'Hunputa Beauty Blender Water Droplets', 4, 1, 20, 10, 1000, 1200, 1500, '4xPro Beauty Flawless Makeup Blender Foundation Puff Multi Shape Sponges ')," +
    //                    "(3, 'st0003', 'Handmade Long False Black Eyelashes', 4, 1, 15, 5, 1500, 1800, 2100, 'A box of 5 to install')," +
    //                    "(4, 'st0004', 'Victoria''s Secret Bombshell Eau De Parfum', 2, 1, 10, 5, 50000, 52000, 55000, 'pink color')," +
    //                    "(5, 'st0005', 'Almond Milk By Carols Daughter Sulfate Shampoo', 5, 1, 10, 5, 45000, 47000, 49000, 'daily damage repair')," +
    //                    "(6, 'st0006', 'Hunputa Pro Makeup Vidvid Matte Lip Gloss Pencil Beauty ', 8, 1, 10, 5, 3000, 3300, 3700, 'Long Lasting Moisturizing Waterproof')," +
    //                    "(7, 'st0007', 'IPSA Eye Color Clear Eyes # A17 ', 7, 1, 10, 5, 13000, 15000, 18000, 'Urban Decay')," +
    //                    "(8, 'st0008', 'Laneige BB Cushion SPF50+ PA++' , 4, 1, 10, 5, 26000, 28000, 30000, 'Natural Beige 15gx2 ~ NIB')," +
    //                    "(9, 'st0009', 'Laneige BB Cushion Anti-aging SPF50+ PA+' , 4, 1, 10, 5, 43000, 47000, 49000, '15g(main+refill)')," +
    //                    "(10, 'st0010', 'Banila Co. Clean It Zero' , 3, 1, 10, 5, 22000, 25000, 29000, '150ml')," +
    //                    "(11, 'st0011', 'Banila Co. Clean It Zero Purity' , 3, 1, 10, 5, 17000, 19500, 23500, '100ml')," +
    //                    "(12, 'st0012', '3w Clinic Difference Point Up Eye Liner ' , 7, 1, 10, 5, 5000, 6500, 8500, '5g')," +
    //                    "(13, 'st0013', 'China Glaze Nail Polish Lacquer' , 6, 2, 10, 5, 2100, 2400, 2600, ' 0.5oz/15ml for all colors')," +
    //                    "(14, 'st0014', 'Nail Art Sticker Decal Joby' , 6, 1, 10, 5, 3400, 4000, 4300, '')," +
    //                    "(15, 'st0015', 'Clean+easy Zinc Oxide Ultra Sensitive Hair Wax ' , 5, 1, 10, 5, 8200, 8800, 9600, '14oz Soft Hair Remove Wax')," +
    //                    "(16, 'st0016', 'Voesh Deluxe Pedicure Callus Remover Gel' , 3, 1, 10, 5, 27000, 29000, 31000, '50pcs')," +
    //                    "(17, 'st0017', 'OPI Nail Files' , 6, 1, 10, 5, 9000, 10500, 12000, ' Crystal / Buffer /Shiner & More')," +
    //                    "(18, 'st0018', 'OPI Nail Lacquer' , 6, 2, 10, 5, 7000, 9000, 10500, 'Oh My Majesty! NL BA2 Full Size 0.5oz 15ml')," +
    //                    "(19, 'st0019', 'Speedo Sun Protection Zip Up Long Sleeve Sun Top' , 9, 1, 10, 5, 51000, 55000, 59000, '')," +
    //                    "(20, 'st0020', 'Speedo UV Sun Protection Men''s Swim Tee' , 9, 1, 10, 5, 25000, 28000, 30500, 'Longview Long Sleeve Rash guard Swim Tee XL Black')," +
    //                    "(21, 'st0021', 'Men Swimming Surfing Dive Long Sleeve ' , 9, 1, 10, 5, 22000, 24500, 27800, 'Sun UV Protection Swimwear')," +
    //                    "(22, 'st0022', 'Men''s Sport Fitness Swim Trunks' , 9, 1, 10, 5, 16000, 17800, 19200, 'Long Leg Swimming Tights Swimwear gift')," +
    //                    "(23, 'st0023', 'Sprint Goggles Clear' , 9, 1, 10, 5, 9000, 10500, 12000, 'Swim-Swimming Performance Racing Competition')," +
    //                    "(24, 'st0024', 'Speedo Solid Latex Swimming Swim Cap Black' , 9, 1, 10, 5, 7000, 8700, 9300, 'Unisex, Water UV Protection Flexible')," +
    //                    "(25, 'st0025', 'Swim Adult Recreation Dive Mask Snorkeling Swimming Goggles Blue' , 9, 1, 10, 5, 17000, 19000, 22500, '')," +
    //                    "(26, 'st0026', 'Original Swedish Swim-Swimming Goggles ' , 9, 1, 10, 5, 16000, 17000, 21500, '2-Pack Set Anti-Fog Coating ')," +
    //                    "(27, 'st0027', 'Men Sharkskin Swimming Trunk Swim Jammer ' , 9, 1, 10, 5, 10000, 12000, 13500, 'Racing Training Swimwear Fina ')," +
    //                    "(28, 'st0028', 'Opi Soak Off Gelcolor Polish Lacquer' , 6, 2, 10, 5, 15000, 17500, 19800, ' GC S86 Bubble Bath')," +
    //                    "(29, 'st0029', 'Divaderme Lash Extender Black Lashes in a Bottle' , 7, 2, 10, 5, 8000, 9500, 13800, '9 ml / 0.30 oz. ')," +
    //                    "(30, 'st0030', 'DivaDerme Lashes In A Bottle Eyelash Enhancer' , 7, 2, 10, 5, 20000, 23500, 26000, '')," +
    //                    "(31, 'st0031', 'Eyebrow Powder Eye Brow Palette Cosmetic Makeup Shading Kit ' , 7, 2, 10, 5, 2000, 3000, 4000, 'With Brush Mirror')," +
    //                    "(32, 'st0032', 'Travelmall 2 Box Gold Silver Mirror Powder Pigment Nail Glitter Nail Art ' , 6, 2, 10, 5, 10000, 12000, 14000, 'With Brush Mirror')," +
    //                    "(33, 'st0033', 'Essie Nail Lacquer Polish ' , 6, 2, 10, 5, 7000, 9500, 12800, ' Full Size .46 fl oz Set')," +
    //                    "(34, 'st0034', 'Shadow Lock Eyelid Primer Sheer 0.11 Fluid Ounce' , 7, 2, 10, 5, 8000, 10500, 13800, '')," +
    //                    "(35, 'st0035', 'Bulletproof Eyeliner Pencil ' , 7, 1, 10, 5, 14000, 16500, 18500, 'Delightful Beauty')," +
    //                    "(36, 'st0036', 'BareMinerals Round The Clock Waterproof Eye Liner' , 7, 2, 10, 5, 7000, 9200, 12500, '')," +
    //                    "(37, 'st0037', 'Anastasia Bevely Hills Brow Fix Clear Wax Pencil ' , 7, 1, 10, 5, 19000, 20800, 22900, 'Primer - Brow Fixer')," +
    //                    "(38, 'st0038', 'Chanel Rouge Allure Extrait De Gloss Lip Gloss' , 8, 1, 10, 5, 50000, 53000, 56500, ' 57 Insolence Full Sized ')," +
    //                    "(39, 'st0039', 'Chanel Lumiere D''ete Highlighting Bronzing Illuminating Powder ' , 4, 1, 10, 5, 55000, 57500, 59800, 'rare')," +
    //                    "(40, 'st0040', 'Cosmedicine Medi-Matte® Oil Control Lotion' , 4, 1, 10, 5, 10000, 12000, 15000, ' Protectant SPF 20~20ml/.68oz ')," +
    //                    "(41, 'st0041', 'Chanel Double Perfection Lumiere Long-Wear Powder' , 4, 1, 10, 5, 50000, 52000, 54500, ' Makeup Beige Rose')," +
    //                    "(42, 'st0042', 'Chanel Levres Scintillantes Glossimer Amour' , 8, 1, 10, 5, 20000, 22000, 24500, ' #166 Lip Gloss!')," +
    //                    "(43, 'st0043', 'Chanel Les 4 Ombres Quadra Powder Eyeshadow Quad Tisse Mademoiselle' , 7, 1, 10, 5, 50000, 52000, 55000, '')," +
    //                    "(44, 'st0044', 'Chanel Joues Contraste Powder Blush 270' , 7, 1, 10, 5, 23000, 24500, 26500, 'Vibration')," +
    //                    "(45, 'st0045', 'Pleasures Perfume by Estee Lauder' , 2, 2, 10, 5, 32000, 36000, 39000, ' 1.7 oz EDP Spray for Women')," +
    //                    "(46, 'st0046', 'Youth Dew Perfume By Estee Lauder for Women ' , 2, 2, 10, 5, 24000, 27000, 31000, ' 2.25 oz')," +
    //                    "(47, 'st0047', 'Taylor Swift Enchanted Wonderstruck Perfume for Women  ' , 2, 2, 10, 5, 19000, 22000, 26000, 'EDP 3.4 100 ml')," +
    //                    "(48, 'st0048', 'Modern Muse by Estee Lauder Eau de Parfum' , 2, 2, 10, 5, 54000, 58000, 63000, ' 3.4 oz 100 ml Spray')," +
    //                    "(49, 'st0049', 'Bvlgari Black by Bulgari Cologne ' , 2, 2, 10, 5, 28000, 31500, 34000, '2.5 oz for Men for Women')," +
    //                    "(50, 'st0050', 'Onika by Nicki Minaj 3.4 oz Eau De Parfum Spray' , 2, 2, 10, 5, 23000, 25500, 28500, ' for Women')," +
    //                    "(51, 'st0051', 'White Linen by Estee Lauder Eau De Parfum Spray' , 2, 2, 10, 5, 33000, 37000, 41000, ' 1 oz For Women')," +
    //                    "(52, 'st0052', 'Original Santal by Creed 2.5 oz (75ml) EDT spray' , 2, 2, 10, 5, 135000, 140000, 150000, ' For Women')," +
    //                    "(53, 'st0053', 'Apple iPhone 7 PLUS OtterBox Defender with Holster/Belt Clip' , 1, 1, 10, 5, 44000, 47000, 50000, ' Black')," +
    //                    "(54, 'st0054', 'Men Dark Black Lens Gangster Black OG Sunglasse Biker  ' , 1, 1, 10, 5, 6000, 8000, 10500, ' Glasses - Matte')," +
    //                    "(55, 'st0055', 'Otterbox Defender for Apple iPhone 7 Plus Rugged Case Cover With Belt Clip' , 1, 1, 10, 5, 45000, 49000, 52500, 'Cover')," +
    //                    "(56, 'st0056', 'TONYMOLY Egg Pore Tightening Cooling Pack 30g' , 3, 1, 10, 5, 12000, 15000, 18000, 'Cover')," +
    //                    "(57, 'st0057', 'SK-II Facial Treatment Essence' , 3, 2, 10, 5, 169000, 175000, 184000, '330 ml')," +
    //                    "(58, 'st0058', 'SK-II Genoptics Aura Essence' , 3, 2, 10, 5, 129000, 134000, 143000, '30 ml')," +
    //                    "(59, 'st0059', 'SK-II  Spot Essence' , 3, 2, 10, 5, 129000, 134000, 143000, '30 ml')," +
    //                    "(60, 'st0060', 'Sana Hadanomy Collagen Face Mist' , 4, 2, 10, 5, 15000, 17500, 20500, 'Moisturizing Toner - 250ml')," +
    //                    "(61, 'st0061', 'SK-II Radical New Age  Power Radical New Age Essence' , 3, 2, 10, 5, 119000, 135000, 144000, '50ml, face serum, sk2, skii')," +
    //                    "(62, 'st0062', 'SK-II Facial Treatment Special 44' , 3, 2, 10, 5, 175000, 189000, 198000, 'Clear Lotion, Essence, Oil & Cream')," +
    //                    "(63, 'st0063', 'SK-II Radical New Age Power Eye Cream ' , 3, 2, 10, 5, 93000, 100000, 123000, 'Radical New Age (15g)')," +
    //                    "(64, 'st0064', 'SK-II Auractivator CC Cream, Makeup Base, Primer ' , 4, 1, 10, 5, 5000, 6500, 7800, '0.8g with bag')," +
    //                    "(65, 'st0065', 'Peruvian Virgin Human Hair 100% Real Virgin Hair Big Weave Ombre Color' , 5, 1, 10, 5, 148000, 163000, 178000, 'wig')," +
    //                    "(66, 'st0066', 'Brazilian Virgin Hair Ombre Wig With Baby Hair' , 5, 1, 10, 5, 157000, 169000, 185000, 'wig')," +
    //                    "(67, 'st0067', '7A grade short curly glueless wig with baby hair' , 5, 1, 10, 5, 195000, 214000, 231000, 'wig')," +
    //                    "(68, 'st0068', 'Skechers Sunset Retro Athletic Sneakers Girls ' , 10, 1, 10, 5, 23000, 26000, 29500, 'Black/Lavender')," +
    //                    "(69, 'st0069', 'Wild Ferns Manukda Honey Special Care Hand and Nail Creme' , 6, 1, 10, 5, 16000, 19000, 21800, 'Hand Cream 85ML')," +
    //                    "(70, 'st0070', 'Strepsils Child 6+ Strawberry Sugar Free Lozenges' , 1, 1, 10, 5, 5000, 6500, 8000, 'medicine for cough')," +
    //                    "(71, 'st0071', 'Scholl Odour Control Insoles' , 10, 1, 10, 5, 10000, 12000, 14500, 'odourless')," +
    //                    "(72, 'st0072', 'Thin Lizzy 6 In 1 Professional Powder Dark Shade' , 4, 1, 10, 5, 17000, 195000, 22500, ' + Free Brush 1 Set')," +
    //                    "(73, 'st0073', 'Omron Ultra Premium Blood Pressure Monitor' , 1, 1, 10, 5, 168000, 195000, 22500, ' HEM 7320')," +
    //                    "(74, 'st0074', 'Skechers Bikers Satine Charcoal Womens 6.5M' , 10, 1, 10, 5, 21000, 23500, 26500, '')," +
    //                    "(75, 'st0075', 'K-Swiss Belmont SO T White/Splatter/Yucca Women ' , 10, 1, 10, 5, 23000, 25500, 28500, '8 M')," +
    //                    "(76, 'st0076', 'Skechers Upgrades Drizzled White Womens' , 10, 1, 10, 5, 16000, 17800, 21500, '11 M')," +
    //                    "(77, 'st0077', 'Clarks Leisa Annual Sandals Womens Brown' , 10, 1, 10, 5, 40000, 43000, 46200, 'brown')," +
    //                    "(78, 'st0078', 'Sketchers womens black sandals toe loop flip flops shoes' , 10, 1, 10, 5, 19000, 22500, 24500, 'size 8')," +
    //                    "(79, 'st0079', 'Sperry Top Sider Men''s size  loafers leather comfort shoes kilt tassle' , 10, 1, 10, 5, 19000, 22500, 24500, '10 M')," +
    //                    "(80, 'st0080', 'Michael Kors women''s size red sandals slides leather high heel wooden' , 10, 1, 10, 5, 22000, 23500, 25500, '7.5 M')," +
    //                    "(81, 'st0081', 'Salvatore Ferragamo women''s size narrow pumps black med heel' , 10, 1, 10, 5, 29000, 31500, 34500, '')," +
    //                    "(82, 'st0082', 'Ink The Airy Velvet 8g' , 8, 1, 10, 5, 8000, 10000, 12500, '')," +
    //                    "(83, 'st0083', 'Wind Spin Magic Hair Curl Diffuser' , 5, 1, 10, 5, 15000, 17500, 19900, 'Hair Dryer')," +
    //                    "(84, 'st0084', 'Perfumed Hand Moisturizer ' , 6, 1, 10, 5, 4000, 5000, 6000, '30ml')," +
    //                    "(85, 'st0085', 'Procure Damage Clinic Hair Essence 70ml' , 5, 1, 10, 5, 13000, 15000, 17000, 'Hair Coat')," +
    //                    "(86, 'st0086', 'Saemmul Smudge Lip Crayon' , 8, 1, 10, 5, 6000, 8000, 10500, 'Lip Stick')," +
    //                    "(87, 'st0087', 'Victoria''s Secret Beauty Rush Flavored - Satin Lip Gloss Pick' , 8, 1, 10, 5, 5000, 7000, 9500, 'colorful')," +
    //                    "(88, 'st0088', 'Victoria''s Secret Dreamer Flannel PJ Lightweight ' , 1, 1, 10, 5, 42000, 50000, 60000, '3 PIECE SET Pick Color/Size NWT')," +
    //                    "(89, 'st0089', 'Victoria''s Secret The Mayfair PJs Teal Medium' , 1, 1, 10, 5, 41000, 49000, 59000, '3 PIECE COTTON PAJAMA SET  NWT')," +
    //                    "(90, 'st0090', 'Victoria''s Secret Eau De Parfum RollerBall Angel, Night, Heavenly ' , 2, 1, 10, 5, 12000, 13500, 15500, '')," +
    //                    "(91, 'st0091', 'Victoria''s Secret Travel Size Mini Fragrance MIST SPRAY' , 2, 1, 10, 5, 7000, 8500, 10000, '3 PIECE COTTON PAJAMA SET  NWT')," +
    //                    "(92, 'st0092', 'Victoria''s Secret Beauty Rush Key Chain Lip Gloss LIMITED EDITION' , 2, 1, 10, 5, 8000, 9500, 11000, 'key chain lip gloss')," +
    //                    "(93, 'st0093', 'Victoria''s Secret Thermal Long Jane Fireside PJs Pajama' , 1, 1, 10, 5, 41000, 44000, 47000, '3 pcs')," +
    //                    "(94, 'st0094', 'Revlon Ultra HD Gel Lip Color' , 8, 1, 10, 5, 7000, 9000, 11000, '')," +
    //                    "(95, 'st0095', 'NYX Lingerie Liquid Lipstick' , 8, 1, 10, 5, 7000, 9000, 11000, '')," +
    //                    "(96, 'st0096', 'Milani Color Statement Lipstick Matte Finish' , 8, 1, 10, 5, 6000, 8000, 10000, '')," +
    //                    "(97, 'st0097', 'Essie Gel Couture Nail Polish' , 6, 1, 10, 5, 7000, 9000, 11000, '')," +
    //                    "(98, 'st0098', 'Maybelline Colorsensational Lipstick' , 8, 1, 10, 5, 6000, 8000, 10000, '')," +
    //                    "(99, 'st0099', 'Urban Decay UD Gwen Stefani Lipstick & Lip Liner 714 ' , 8, 1, 10, 5, 42000, 45500, 48900, 'Bwob Rare Set')," +
    //                    "(100, 'st0100', 'Conair Ininiti Pro  Spin Air Brush Styler' , 5, 1, 10, 5, 37000, 39500, 43000, '2\" & 1.5\"')," +
    //                    "(101, 'st0101', 'InStyler Max 2-Way Rotating Iron, Black' , 5, 1, 10, 5, 73000, 77000, 83000, '')," +
    //                    "(102, 'st0102', 'Paul Mitchell Platinum Blonde Shampoo (Brighten Blonde Gray White Hair)' , 8, 1, 10, 5, 60000, 77000, 83000, ' 1000ml')," +
    //                    "(103, 'st0103', 'Speedo Vanquisher Optical Swim Goggle ' , 9, 1, 10, 5, 6000, 7700, 8300, ' 6-14, -6.00 Smoke')," +
    //                    "(104, 'st0104', 'Crystaluxe Pink Mother-of-Pearl Flower Pendant w/ Swarovski Crystals in 14K Gold' , 1, 1, 10, 5, 179000, 199000, 219000, '83% off')," +
    //                    "(105, 'st0105', 'Kipling Women''s Challenger Ii Backpack One Size Spellbinder' , 1, 1, 10, 5, 88000, 96000, 115000, '');");
    //            database.execSQL("insert into Supplier(id, name, phoneNo, balance, address) values " +
    //                    "(2, 'Laimeng World', '09970464559', 0.0, 'Hong kong'), " +
    //                    "(3, 'Best Deals US Store', '09785301612', 0.0, 'USA')," +
    //                    "(4, 'Hunpunta', '098640453', 0.0, 'USA')," +
    //                    "(5, 'Denice', '099876565', 0.0, 'USA')," +
    //                    "(6, '5ive Sense', '01546284', 0.0, 'Korea')," +
    //                    "(7, 'Rose Rose Shop', '0258791', 0.0, 'South Korea')," +
    //                    "(8, 'Beauty Zone', '0258791', 0.0, 'United States')," +
    //                    "(9, 'Gr8 Beauty Online', '05221040', 0.0, 'United States')," +
    //                    "(10, 'Zodee', '067854544', 0.0, 'Australia')," +
    //                    "(11, 'Sun-bay', '067854544', 0.0, 'Australia')," +
    //                    "(12, 'Zavko inc', '7854566', 0.0, 'Australia')," +
    //                    "(13, 'Hotsonhua' , '04578544', 0.0, 'China')," +
    //                    "(14, 'Clickrooster' , '047521475', 0.0, 'China')," +
    //                    "(15, 'Box Beauty' , '0527454', 0.0, 'United States')," +
    //                    "(16, 'Roses Boutique' , '0527454', 0.0, 'United States')," +
    //                    "(17, 'Deal Times Perfumes' , '025555421', 0.0, 'United States')," +
    //                    "(18, 'Hub Trends' , '025555421', 0.0, 'United States')," +
    //                    "(19, 'Fragrance Corner' , '025555477', 0.0, 'United States')," +
    //                    "(20, 'Kat Beauty' , '023478694', 0.0, 'United States')," +
    //                    "(21, 'African Goods' , '546575436', 0.0, 'United States')," +
    //                    "(22, 'Runners Discount' , '5445543555', 0.0, 'China')," +
    //                    "(23, 'Skin Australia' , '5445543555', 0.0, 'Australia')," +
    //                    "(24, 'Quality Deals' , '455582154663', 0.0, 'United States')," +
    //                    "(25, 'Cosmetic Love' , '5878575416', 0.0, 'South Korea')," +
    //                    "(26, 'Dama' , '755522454769', 0.0, 'United States')," +
    //                    "(27, 'Niki' , '447478897', 0.0, 'United States')," +
    //                    "(28, 'Warehouse Deals' , '59894554', 0.0, 'United States')," +
    //                    "(29, 'Bestless' , '525894554', 0.0, 'United States')," +
    //                    "(30, '3000 Hot Deals' , '5475494554', 0.0, 'United States')," +
    //                    "(31, 'Shop Jewelry on Ebay' , '5475200554', 0.0, 'United States')," +
    //                    "(32, 'The Fashion Scoop' , '544454540554', 0.0, 'United States');");
    //            database.execSQL("insert into Tax(id, name, rate, type, description, isDefault) values" +
    //                    "(2, 'exclusive commercial tax', 5, 'Exclusive', '', 0), " +
    //                    "(3, 'inclusive commercial tax', 5, 'Inclusive', '', 0);");
    //            database.execSQL("insert into Purchase " +
    //                    "(id, voucherNo, date, supplierID, subTotal, discount, discountAmt, total, taxID, taxType, taxRate, taxAmt, remark, paidAmt, balance, day, month, year, createdDate, time)" +
    //                    "values " +
    //                    "(1, 'P0001', '20161101', 1, 19125, null, 125, 19000, 1, 'Inclusive', 0.0, 0.0, null, 17000, 2000, '1', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-01', time('now', 'localtime')))," +
    //                    "(2, 'P0002', '20161108', 2, 360000, null, 0.0, 378000, 2, 'Exclusive', 5.0, 18000, null, 250000, 128000, '8', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-08', time('now', 'localtime')))," +
    //                    "(3, 'P0003', '20161103', 3, 30000, null, 0.0, 31500, 2, 'Exclusive', 5.0, 1500, null, 31500, 0.0, '3', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-03', time('now', 'localtime')))," +
    //                    "(18, 'P0018', '20161126', 13, 104000, null, 0.0, 109200, 2, 'Exclusive', 5.0, 5200.0, null, 109200, 0.0, '26', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-26', time('now', 'localtime')))," +
    //                    "(19, 'P0019', '20161127', 14, 245000, null, 250.0, 257000, 2, 'Exclusive', 5.0, 12250.0, null, 257000, 0.0, '27', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-27', time('now', 'localtime')))," +
    //                    "(20, 'P0020', '20161115', 15, 89500, null, 975.0, 93000, 2, 'Exclusive', 5.0, 4475.0, null, 93000, 0.0, '15', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-15', time('now', 'localtime')))," +
    //                    "(21, 'P0021', '20161115', 16, 1220000, null, 0.0, 1281000, 2, 'Exclusive', 5.0, 61000.0, null, 900000, 381000.0, '15', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-15', time('now', 'localtime')))," +
    //                    "(22, 'P0022', '20161119', 17, 610000, null, 500.0, 640000, 2, 'Exclusive', 5.0, 30500.0, null, 440000, 200000.0, '19', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-19', time('now', 'localtime')))," +
    //                    "(23, 'P0023', '20161121', 18, 110000, null, 0.0, 115500, 2, 'Exclusive', 5.0, 5500.0, null, 100000, 15500.0, '21', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-21', time('now', 'localtime')))," +
    //                    "(24, 'P0024', '20161123', 18, 105000, null, 250.0, 110000, 2, 'Exclusive', 5.0, 5250.0, null, 100000, 10000.0, '23', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-23', time('now', 'localtime')))," +
    //                    "(25, 'P0025', '20161124', 19, 805000, null, 250.0, 845000, 2, 'Exclusive', 5.0, 40250.0, null, 645000, 200000.0, '24', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-24', time('now', 'localtime')))," +
    //                    "(26, 'P0026', '20161126', 18, 434000, null, 700.0, 455000, 2, 'Exclusive', 5.0, 21700.0, null, 400000, 55000.0, '26', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-26', time('now', 'localtime')))," +
    //                    "(27, 'P0027', '20161128', 18, 3930000, null, 16500.0, 4110000, 2, 'Exclusive', 5.0, 196500.0, null, 4000000, 110000.0, '28', '11', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-28', time('now', 'localtime')))," +
    //                    "(28, 'P0028', '20161201', 20, 24000, null, 200.0, 25000, 2, 'Exclusive', 5.0, 1200.0, null, 25000, 0.0, '1', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-02', time('now', 'localtime')))," +
    //                    "(29, 'P0029', '20161202', 21, 480000, null, 0.0, 504000, 2, 'Exclusive', 5.0, 24000.0, null, 504000, 0.0, '2', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-02', time('now', 'localtime')))," +
    //                    "(30, 'P0030', '20161204', 23, 1026000, null, 2300.0, 1075000, 2, 'Exclusive', 5.0, 51300.0, null, 900000, 175000.0, '4', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-04', time('now', 'localtime')))," +
    //                    "(31, 'P0031', '20161207', 22, 105000, null, 250.0, 110000, 2, 'Exclusive', 5.0, 5250.0, null, 100000, 10000.0, '7', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-07', time('now', 'localtime')))," +
    //                    "(32, 'P0032', '20161209', 22, 470000, null, 500.0, 493000, 2, 'Exclusive', 5.0, 23500.0, null, 493000, 0.0, '9', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-09', time('now', 'localtime')))," +
    //                    "(33, 'P0033', '20161211', 24, 420000, null, 1000.0, 440000, 2, 'Exclusive', 5.0, 21000.0, null, 440000, 0.0, '11', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-11', time('now', 'localtime')))," +
    //                    "(34, 'P0034', '20161213', 25, 207000, null, 350.0, 217000, 2, 'Exclusive', 5.0, 10350.0, null, 217000, 0.0, '13', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-13', time('now', 'localtime')))," +
    //                    "(35, 'P0035', '20161215', 26, 750500, null, 8025.0, 780000, 2, 'Exclusive', 5.0, 37525.0, null, 500000, 280000.0, '15', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-15', time('now', 'localtime')))," +
    //                    "(36, 'P0036', '20161220', 27, 166000, null, 300.0, 174000, 2, 'Exclusive', 5.0, 8300.0, null, 174000, 0.0, '20', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-20', time('now', 'localtime')))," +
    //                    "(37, 'P0037', '20161223', 27, 72000, null, 0.0, 75600, 2, 'Exclusive', 5.0, 3600.0, null, 75600, 0.0, '23', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-23', time('now', 'localtime')))," +
    //                    "(38, 'P0038', '20161224', 28, 495000, null, 750.0, 519000, 2, 'Exclusive', 5.0, 24750.0, null, 519000, 0.0, '24', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-24', time('now', 'localtime')))," +
    //                    "(39, 'P0039', '20161226', 29, 530000, null, 0.0, 556500, 2, 'Exclusive', 5.0, 26500.0, null, 556500, 0.0, '26', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-26', time('now', 'localtime')))," +
    //                    "(40, 'P0040', '20161227', 30, 29500, null, 75.0, 30900, 2, 'Exclusive', 5.0, 1475.0, null, 30900, 0.0, '27', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-27', time('now', 'localtime')))," +
    //                    "(41, 'P0041', '20161228', 31, 156400, null, 0.0, 164220, 2, 'Exclusive', 5.0, 7820.0, null, 164220, 0.0, '28', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-28', time('now', 'localtime')))," +
    //                    "(42, 'P0042', '20161229', 31, 430000, null, 0.0, 451500, 2, 'Exclusive', 5.0, 21500, null, 451500, 0.0, '29', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-29', time('now', 'localtime')))," +
    //                    "(43, 'P0043', '20161230', 31, 215000, null, 750.0, 225000, 2, 'Exclusive', 5.0, 10750.0, null, 225000, 0.0, '30', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-30', time('now', 'localtime')))," +
    //                    "(44, 'P0044', '20161231', 5, 185000, null, 250.0, 194000, 2, 'Exclusive', 5.0, 9250.0, null, 194000, 0.0, '31', '12', '2016', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-31', time('now', 'localtime')))," +
    //                    "(45, 'P0045', '20170101', 10, 505000, null, 250.0, 530000, 2, 'Exclusive', 5.0, 25250.0, null, 450000, 80000.0, '1', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-01', time('now', 'localtime')))," +
    //                    "(46, 'P0046', '20170102', 26, 488000, null, 400.0, 512000, 2, 'Exclusive', 5.0, 24400.0, null, 400000, 112000.0, '2', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-02', time('now', 'localtime')))," +
    //                    "(47, 'P0047', '20170104', 19, 865000, null, 250.0, 908000, 2, 'Exclusive', 5.0, 43250.0, null, 708000, 200000.0, '4', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-04', time('now', 'localtime')))," +
    //                    "(48, 'P0048', '20170106', 10, 490000, null, 0.0, 514500, 2, 'Exclusive', 5.0, 24500.0, null, 314500, 200000.0, '6', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-06', time('now', 'localtime')))," +
    //                    "(49, 'P0049', '20170108', 1, 307500, null, 875.0, 322000, 2, 'Exclusive', 5.0, 15375.0, null, 222000, 100000.0, '8', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-08', time('now', 'localtime')))," +
    //                    "(50, 'P0050', '20170109', 22, 369000, null, 450.0, 387000, 2, 'Exclusive', 5.0, 18450.0, null, 387000, 0.0, '9', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-09', time('now', 'localtime')))," +
    //                    "(51, 'P0051', '20170111', 12, 115000, null, 750.0, 120000, 2, 'Exclusive', 5.0, 5750.0, null, 120000, 0.0, '11', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-11', time('now', 'localtime')))," +
    //                    "(52, 'P0052', '20170113', 29, 531500, null, 750.0, 120000, 2, 'Exclusive', 5.0, 26575.0, null, 120000, 0.0, '13', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-13', time('now', 'localtime')))," +
    //                    "(53, 'P0053', '20170114', 24, 399000, null, 950.0, 418000, 2, 'Exclusive', 5.0, 19950.0, null, 418000, 0.0, '14', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-14', time('now', 'localtime')))," +
    //                    "(54, 'P0054', '20170116', 16, 2365000, null, 3250.0, 2480000, 2, 'Exclusive', 5.0, 118250.0, null, 2000000, 480000.0, '16', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-16', time('now', 'localtime')))," +
    //                    "(55, 'P0055', '20170119', 8, 189500, null, 975.0, 198000, 2, 'Exclusive', 5.0, 9475.0, null, 198000, 0.0, '19', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-19', time('now', 'localtime')))," +
    //                    "(56, 'P0056', '20170123', 31, 1560000, null, 8000.0, 1630000, 2, 'Exclusive', 5.0, 78000.0, null, 1000000, 630000.0, '23', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-23', time('now', 'localtime')))," +
    //                    "(57, 'P0057', '20170125', 6, 1370000, null, 500.0, 1438000, 2, 'Exclusive', 5.0, 68500.0, null, 1000000, 438000.0, '25', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-25', time('now', 'localtime')))," +
    //                    "(58, 'P0058', '20170126', 14, 246500, null, 825.0, 258000, 2, 'Exclusive', 5.0, 68500.0, null, 258000, 0.0, '26', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-26', time('now', 'localtime')))," +
    //                    "(59, 'P0059', '20170127', 17, 610000, null, 500.0, 640000, 2, 'Exclusive', 5.0, 30500.0, null, 340000, 300000.0, '27', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-27', time('now', 'localtime')))," +
    //                    "(60, 'P0060', '20170129', 11, 271500, null, 75.0, 285000, 2, 'Exclusive', 5.0, 13575.0, null, 285000, 0.0, '29', '1', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-29', time('now', 'localtime')))," +
    //                    "(4, 'P0004', '20170203', 5, 364000, null, 0.0, 364000, 1, 'Inclusive', 0.0, 0.0, null, 360000, 4000, '3', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-03', time('now', 'localtime')))," +
    //                    "(5, 'P0005', '20170205', 4, 30000, null, 2000.0, 28000, 1, 'Inclusive', 5.0, 0.0, null, 28000, 0.0, '5', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-05', time('now', 'localtime')))," +
    //                    "(6, 'P0006', '20170207', 2, 50000, null, 3000.0, 49500, 3, 'Inclusive', 5.0, 2500.0, null, 40000, 9500, '7', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-07', time('now', 'localtime')))," +
    //                    "(7, 'P0007', '20170209', 6, 215000, null, 0.0, 225750, 2, 'Exclusive', 5.0, 10750.0, null, 200000, 25750, '9', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-09', time('now', 'localtime')))," +
    //                    "(8, 'P0008', '20170209', 6, 110000, null, 0.0, 115500, 2, 'Exclusive', 5.0, 5500.0, null, 115500, 0.0, '9', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-09', time('now', 'localtime')))," +
    //                    "(9, 'P0009', '20170210', 7, 85000, null, 0.0, 89250, 2, 'Exclusive', 5.0, 4250.0, null, 89250, 0.0, '10', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-10', time('now', 'localtime')))," +
    //                    "(10, 'P0010', '20170210', 7, 25000, null, 0.0, 26250, 2, 'Exclusive', 5.0, 1250.0, null, 20000, 6250.0, '10', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-10', time('now', 'localtime')))," +
    //                    "(11, 'P0011', '20170211', 8, 264300, null, 7515.0, 270000, 2, 'Exclusive', 5.0, 13215.0, null, 270000, 0.0, '11', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-11', time('now', 'localtime')))," +
    //                    "(12, 'P0012', '20170215', 9, 112500, null, 125.0, 118000, 2, 'Exclusive', 5.0, 5625.0, null, 110000, 0.0, '15', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-15', time('now', 'localtime')))," +
    //                    "(13, 'P0013', '20170218', 10, 490000, null, 4500.0, 510000, 2, 'Exclusive', 5.0, 24500.0, null, 250000, 0.0, '18', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-18', time('now', 'localtime')))," +
    //                    "(14, 'P0014', '20170219', 11, 260000, null, 0.0, 273000, 2, 'Exclusive', 5.0, 13000.0, null, 273000, 0.0, '19', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-19', time('now', 'localtime')))," +
    //                    "(15, 'P0015', '20170223', 12, 130000, null, 0.0, 136500, 2, 'Exclusive', 5.0, 6500.0, null, 136500, 0.0, '23', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-23', time('now', 'localtime')))," +
    //                    "(16, 'P0016', '20170224', 9, 40000, null, 0.0, 42000, 2, 'Exclusive', 5.0, 2000.0, null, 42000, 0.0, '24', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-24', time('now', 'localtime')))," +
    //                    "(17, 'P0017', '20170226', 11, 75000, null, 750.0, 78000, 2, 'Exclusive', 5.0, 3750.0, null, 78000, 0.0, '26', '2', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-26', time('now', 'localtime')))," +
    //                    "(61, 'P0061', '20170301', 1, 22000, null, 0, 22000, 1, 'Inclusive', 0.0, 0.0, null, 22000, 0, '1', '3', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-01', time('now', 'localtime')))," +
    //                    "(62, 'P0062', '20170308', 2, 400000, null, 0.0, 420000, 2, 'Exclusive', 5.0, 20000, null, 250000, 2, '8', '3', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-08', time('now', 'localtime')))," +
    //                    "(63, 'P0063', '20170303', 3, 73200, null, 60.0, 76800, 2, 'Exclusive', 5.0, 3660, null, 36800, 40000.0, '3', '3', '2017', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-03', time('now', 'localtime')))");
    //            database.execSQL("insert into PurchaseDetail" +
    //                    "(id, purchaseID, stockID, qty, price, discount, discountAmt, total, createdDate) values" +
    //                    "(1, 1, 2, 7, 900, '0%', 0.0, 6300, strftime('%Y%m%d', date('now')))," +
    //                    "(2, 1, 3, 10, 1282.5, '5%' ,67.5, 12825, strftime('%Y%m%d', date('now')))," +
    //                    "(3, 2, 4, 8, 45000, null, 0.0, 360000, strftime('%Y%m%d', date('now')))," +
    //                    "(4, 3, 6, 12, 2500, null, 0.0, 30000, strftime('%Y%m%d', date('now')))," +
    //                    "(5, 3, 6, 12, 2500, null, 0.0, 30000, strftime('%Y%m%d', date('now')))," +
    //                    "(6, 4, 7, 12, 13000, null, 0.0, 156000, strftime('%Y%m%d', date('now')))," +
    //                    "(7, 4, 8, 8, 26000, null, 0.0, 208000, strftime('%Y%m%d', date('now')))," +
    //                    "(8, 5, 6, 10, 3000, null, 0.0, 30000, strftime('%Y%m%d', date('now')))," +
    //                    "(9, 6, 1, 10, 4000, null, 0.0, 40000, strftime('%Y%m%d', date('now')))," +
    //                    "(10, 6, 2, 10, 1000, null, 0.0, 10000, strftime('%Y%m%d', date('now')))," +
    //                    "(11, 7, 9, 5, 43000, null, 0.0, 215000, strftime('%Y%m%d', date('now')))," +
    //                    "(12, 8, 10, 5, 22000, null, 0.0, 110000, strftime('%Y%m%d', date('now')))," +
    //                    "(13, 9, 11, 5, 17000, null, 0.0, 85000, strftime('%Y%m%d', date('now')))," +
    //                    "(14, 9, 12, 5, 5000, null, 0.0, 25000, strftime('%Y%m%d', date('now')))," +
    //                    "(15, 10, 12, 5, 5000, null, 0.0, 25000, strftime('%Y%m%d', date('now')))," +
    //                    "(16, 11, 13, 5, 2100, null, 0.0, 10500, strftime('%Y%m%d', date('now')))," +
    //                    "(17, 11, 14, 6, 3400, null, 0.0, 20400, strftime('%Y%m%d', date('now')))," +
    //                    "(18, 11, 15, 12, 8200, null, 0.0, 98400, strftime('%Y%m%d', date('now')))," +
    //                    "(19, 11, 16, 5, 27000, null, 0.0, 135000, strftime('%Y%m%d', date('now')))," +
    //                    "(20, 12, 17, 5, 12000, null, 0.0, 60000, strftime('%Y%m%d', date('now')))," +
    //                    "(21, 12, 18, 5, 10500, null, 0.0, 52500, strftime('%Y%m%d', date('now')))," +
    //                    "(22, 13, 19, 5, 51000, null, 0.0, 255000, strftime('%Y%m%d', date('now')))," +
    //                    "(23, 13, 20, 5, 25000, null, 0.0, 125000, strftime('%Y%m%d', date('now')))," +
    //                    "(24, 13, 21, 5, 22000, null, 0.0, 110000, strftime('%Y%m%d', date('now')))," +
    //                    "(25, 14, 22, 5, 16000, null, 0.0, 80000, strftime('%Y%m%d', date('now')))," +
    //                    "(26, 14, 23, 5, 12000, null, 0.0, 60000, strftime('%Y%m%d', date('now')))," +
    //                    "(27, 14, 24, 5, 7000, null, 0.0, 35000, strftime('%Y%m%d', date('now')))," +
    //                    "(28, 14, 25, 5, 17000, null, 0.0, 85000, strftime('%Y%m%d', date('now')))," +
    //                    "(29, 15, 26, 5, 16000, null, 0.0, 80000, strftime('%Y%m%d', date('now')))," +
    //                    "(30, 15, 27, 5, 10000, null, 0.0, 50000, strftime('%Y%m%d', date('now')))," +
    //                    "(31, 16, 29, 5, 8000, null, 0.0, 40000, strftime('%Y%m%d', date('now')))," +
    //                    "(32, 17, 28, 5, 15000, null, 0.0, 75000, strftime('%Y%m%d', date('now')))," +
    //                    "(33, 18, 30, 5, 19000, null, 0.0, 95000, strftime('%Y%m%d', date('now')))," +
    //                    "(34, 18, 31, 5, 1800, null, 0.0, 9000, strftime('%Y%m%d', date('now')))," +
    //                    "(35, 19, 32, 5, 8200, null, 0.0, 41000, strftime('%Y%m%d', date('now')))," +
    //                    "(36, 19, 33, 5, 11500, null, 0.0, 57500, strftime('%Y%m%d', date('now')))," +
    //                    "(37, 19, 34, 5, 6900, null, 0.0, 34500, strftime('%Y%m%d', date('now')))," +
    //                    "(38, 19, 35, 5, 16500, null, 0.0, 82500, strftime('%Y%m%d', date('now')))," +
    //                    "(39, 19, 36, 5, 5900, null, 0.0, 29500, strftime('%Y%m%d', date('now')))," +
    //                    "(40, 20, 37, 5, 17900, null, 0.0, 89500, strftime('%Y%m%d', date('now')))," +
    //                    "(41, 21, 38, 5, 49000, null, 0.0, 245000, strftime('%Y%m%d', date('now')))," +
    //                    "(42, 21, 39, 5, 53000, null, 0.0, 265000, strftime('%Y%m%d', date('now')))," +
    //                    "(43, 21, 40, 5, 9000, null, 0.0, 45000, strftime('%Y%m%d', date('now')))," +
    //                    "(44, 21, 41, 5, 48000, null, 0.0, 240000, strftime('%Y%m%d', date('now')))," +
    //                    "(45, 21, 42, 5, 18000, null, 0.0, 90000, strftime('%Y%m%d', date('now')))," +
    //                    "(46, 21, 43, 5, 47000, null, 0.0, 235000, strftime('%Y%m%d', date('now')))," +
    //                    "(47, 21, 44, 5, 20000, null, 0.0, 100000, strftime('%Y%m%d', date('now')))," +
    //                    "(48, 22, 45, 5, 29000, null, 0.0, 1145000, strftime('%Y%m%d', date('now')))," +
    //                    "(49, 22, 47, 5, 17000, null, 0.0, 85000, strftime('%Y%m%d', date('now')))," +
    //                    "(50, 22, 48, 5, 51000, null, 0.0, 255000, strftime('%Y%m%d', date('now')))," +
    //                    "(51, 22, 49, 5, 25000, null, 0.0, 125000, strftime('%Y%m%d', date('now')))," +
    //                    "(52, 23, 46, 5, 22000, null, 0.0, 110000, strftime('%Y%m%d', date('now')))," +
    //                    "(53, 24, 50, 5, 21000, null, 0.0, 105000, strftime('%Y%m%d', date('now')))," +
    //                    "(54, 25, 51, 5, 31000, null, 0.0, 155000, strftime('%Y%m%d', date('now')))," +
    //                    "(55, 25, 52, 5, 130000, null, 0.0, 650000, strftime('%Y%m%d', date('now')))," +
    //                    "(56, 26, 53, 5, 41000, null, 0.0, 205000, strftime('%Y%m%d', date('now')))," +
    //                    "(57, 26, 54, 5, 5800, null, 0.0, 29000, strftime('%Y%m%d', date('now')))," +
    //                    "(58, 26, 55, 5, 40000, null, 0.0, 200000, strftime('%Y%m%d', date('now')))," +
    //                    "(59, 27, 56, 5, 11000, null, 0.0, 55000, strftime('%Y%m%d', date('now')))," +
    //                    "(60, 27, 57, 5, 159000, null, 0.0, 795000, strftime('%Y%m%d', date('now')))," +
    //                    "(61, 27, 58, 5, 119000, null, 0.0, 595000, strftime('%Y%m%d', date('now')))," +
    //                    "(62, 27, 59, 5, 119000, null, 0.0, 595000, strftime('%Y%m%d', date('now')))," +
    //                    "(63, 27, 60, 5, 13000, null, 0.0, 65000, strftime('%Y%m%d', date('now')))," +
    //                    "(64, 27, 61, 5, 109000, null, 0.0, 545000, strftime('%Y%m%d', date('now')))," +
    //                    "(65, 27, 62, 5, 165000, null, 0.0, 825000, strftime('%Y%m%d', date('now')))," +
    //                    "(66, 27, 63, 5, 91000, null, 0.0, 455000, strftime('%Y%m%d', date('now')))," +
    //                    "(67, 28, 64, 5, 4800, null, 0.0, 24000, strftime('%Y%m%d', date('now'))), " +
    //                    "(68, 29, 65, 1, 140000, null, 0.0, 140000, strftime('%Y%m%d', date('now'))), " +
    //                    "(69, 29, 66, 1, 150000, null, 0.0, 150000, strftime('%Y%m%d', date('now'))), " +
    //                    "(70, 29, 67, 1, 190000, null, 0.0, 190000, strftime('%Y%m%d', date('now'))), " +
    //                    "(71, 30, 69, 5, 15000, null, 0.0,\t75000, strftime('%Y%m%d', date('now'))), " +
    //                    "(72, 30, 70, 5, 4700, null, 0.0, 23500, strftime('%Y%m%d', date('now'))), " +
    //                    "(73, 30, 71, 5, 9500, null, 0.0, 47500, strftime('%Y%m%d', date('now'))), " +
    //                    "(74, 30, 72, 5, 16000, null, 0.0, 80000, strftime('%Y%m%d', date('now'))), " +
    //                    "(75, 30, 73, 5, 160000, null, 0.0, 800000, strftime('%Y%m%d', date('now'))), " +
    //                    "(76, 31, 68, 5, 21000, null, 0.0, 105000, strftime('%Y%m%d', date('now'))), " +
    //                    "(77, 32, 74, 5, 20000, null, 0.0, 100000, strftime('%Y%m%d', date('now'))), " +
    //                    "(78, 32, 75, 5, 21000, null, 0.0, 105000, strftime('%Y%m%d', date('now'))), " +
    //                    "(79, 32, 76, 5, 15000, null, 0.0, 75000, strftime('%Y%m%d', date('now'))), " +
    //                    "(80, 32, 77, 5, 38000, null, 0.0, 190000, strftime('%Y%m%d', date('now')))," +
    //                    "(81, 33, 78, 5, 18000, null, 0.0, 90000, strftime('%Y%m%d', date('now')))," +
    //                    "(82, 33, 79, 5, 18000, null, 0.0, 90000, strftime('%Y%m%d', date('now')))," +
    //                    "(83, 33, 80, 5, 21000, null, 0.0, 105000, strftime('%Y%m%d', date('now')))," +
    //                    "(84, 33, 81, 5, 27000, null, 0.0, 135000, strftime('%Y%m%d', date('now')))," +
    //                    "(85, 34, 82, 5, 7000, null, 0.0, 35000, strftime('%Y%m%d', date('now')))," +
    //                    "(86, 34, 83, 5, 14000, null, 0.0, 70000, strftime('%Y%m%d', date('now')))," +
    //                    "(87, 34, 84, 5, 3800, null, 0.0, 19000, strftime('%Y%m%d', date('now')))," +
    //                    "(88, 34, 85, 5, 11000, null, 0.0, 55000, strftime('%Y%m%d', date('now')))," +
    //                    "(89, 34, 86, 5, 5600, null, 0.0, 28000, strftime('%Y%m%d', date('now')))," +
    //                    "(90, 35, 87, 5, 4800, null, 0.0, 24000, strftime('%Y%m%d', date('now')))," +
    //                    "(91, 35, 88, 5, 40000, null, 0.0, 200000, strftime('%Y%m%d', date('now')))," +
    //                    "(92, 35, 89, 5, 40000, null, 0.0, 200000, strftime('%Y%m%d', date('now')))," +
    //                    "(93, 35, 90, 5, 11000, null, 0.0, 55000, strftime('%Y%m%d', date('now')))," +
    //                    "(94, 35, 91, 5, 6700, null, 0.0, 33500, strftime('%Y%m%d', date('now')))," +
    //                    "(95, 35, 92, 5, 7600, null, 0.0, 38000, strftime('%Y%m%d', date('now')))," +
    //                    "(96, 35, 93, 5, 40000, null, 0.0, 200000, strftime('%Y%m%d', date('now')))," +
    //                    "(97, 36, 94, 5, 6500, null, 0.0, 32500, strftime('%Y%m%d', date('now')))," +
    //                    "(98, 36, 95, 5, 6500, null, 0.0, 32500, strftime('%Y%m%d', date('now')))," +
    //                    "(99, 36, 96, 5, 5800, null, 0.0, 29000, strftime('%Y%m%d', date('now')))," +
    //                    "(100, 36, 97, 5, 8700, null, 0.0, 43500, strftime('%Y%m%d', date('now')))," +
    //                    "(101, 36, 98, 5, 5700, null, 0.0, 28500, strftime('%Y%m%d', date('now')))," +
    //                    "(102, 37, 97, 5, 8800, null, 0.0, 44000, strftime('%Y%m%d', date('now')))," +
    //                    "(103, 37, 98, 5, 5600, null, 0.0, 28000, strftime('%Y%m%d', date('now')))," +
    //                    "(104, 38, 99, 5, 41000, null, 0.0, 205000, strftime('%Y%m%d', date('now')))," +
    //                    "(105, 38, 102, 5, 58000, null, 0.0, 290000, strftime('%Y%m%d', date('now')))," +
    //                    "(106, 39, 100, 5, 35000, null, 0.0, 175000, strftime('%Y%m%d', date('now')))," +
    //                    "(107, 39, 101, 5, 71000, null, 0.0, 355000, strftime('%Y%m%d', date('now')))," +
    //                    "(108, 40, 103, 5, 5900, null, 0.0, 29500, strftime('%Y%m%d', date('now')))," +
    //                    "(109, 41, 104, 5, 31280, '83%', 152720.0, 156400, strftime('%Y%m%d', date('now')))," +
    //                    "(110, 42, 105, 5, 86000, null, 0.0, 430000, strftime('%Y%m%d', date('now')))," +
    //                    "(111, 43, 5, 5, 43000, null, 0.0, 215000, strftime('%Y%m%d', date('now')))," +
    //                    "(112, 44, 7, 5, 12000, null, 0.0, 60000, strftime('%Y%m%d', date('now')))," +
    //                    "(113, 44, 8, 4, 25000, null, 0.0, 125000, strftime('%Y%m%d', date('now')))," +
    //                    "(114, 45, 19, 5, 51000.0, null, 0.0, 255000, strftime('%Y%m%d', date('now')))," +
    //                    "(115, 45, 54, 5, 6000.0, null, 0.0, 30000, strftime('%Y%m%d', date('now')))," +
    //                    "(116, 45, 21, 5, 22000.0, null, 0.0, 110000, strftime('%Y%m%d', date('now')))," +
    //                    "(117, 45, 80, 5, 22000.0, null, 0.0, 110000, strftime('%Y%m%d', date('now')))," +
    //                    "(118, 46, 88, 3, 42000.0, null, 0.0, 126000, strftime('%Y%m%d', date('now')))," +
    //                    "(119, 46, 93, 5, 41000.0, null, 0.0, 205000, strftime('%Y%m%d', date('now')))," +
    //                    "(120, 46, 89, 2, 41000.0, null, 0.0, 82000, strftime('%Y%m%d', date('now')))," +
    //                    "(121, 46, 60, 5, 15000.0, null, 0.0, 75000, strftime('%Y%m%d', date('now')))," +
    //                    "(122, 47, 51, 5, 33000.0, null, 0.0, 165000, strftime('%Y%m%d', date('now')))," +
    //                    "(123, 47, 52, 5, 135000.0, null, 0.0, 675000, strftime('%Y%m%d', date('now')))," +
    //                    "(124, 47, 87, 5, 5000.0, null, 0.0, 25000, strftime('%Y%m%d', date('now')))," +
    //                    "(125, 48, 19, 5, 51000.0, null, 0.0, 255000, strftime('%Y%m%d', date('now')))," +
    //                    "(126, 48, 20, 5, 25000.0, null, 0.0, 125000, strftime('%Y%m%d', date('now')))," +
    //                    "(127, 48, 21, 5, 22000.0, null, 0.0, 110000, strftime('%Y%m%d', date('now')))," +
    //                    "(128, 49, 1, 5, 4000.0, null, 0.0, 20000, strftime('%Y%m%d', date('now')))," +
    //                    "(129, 49, 2, 5, 1000.0, null, 0.0, 5000, strftime('%Y%m%d', date('now')))," +
    //                    "(130, 49, 3, 5, 1500.0, null, 0.0, 7500, strftime('%Y%m%d', date('now')))," +
    //                    "(131, 49, 39, 5, 55000.0, null, 0.0, 275000, strftime('%Y%m%d', date('now')))," +
    //                    "(132, 50, 68, 3, 23000.0, null, 0.0, 69000, strftime('%Y%m%d', date('now')))," +
    //                    "(133, 50, 74, 3, 21000.0, null, 0.0, 63000, strftime('%Y%m%d', date('now')))," +
    //                    "(134, 50, 75, 3, 23000.0, null, 0.0, 69000, strftime('%Y%m%d', date('now')))," +
    //                    "(135, 50, 76, 3, 16000.0, null, 0.0, 48000, strftime('%Y%m%d', date('now')))," +
    //                    "(136, 50, 77, 3, 40000.0, null, 0.0, 120000, strftime('%Y%m%d', date('now')))," +
    //                    "(137, 51, 26, 5, 14000, null, 0.0, 75000, strftime('%Y%m%d', date('now')))," +
    //                    "(138, 51, 27, 5, 8000, null, 0.0, 40000, strftime('%Y%m%d', date('now')))," +
    //                    "(139, 52, 100, 5, 35000.0, null, 0.0, 175000, strftime('%Y%m%d', date('now')))," +
    //                    "(140, 52, 102, 5, 71300.0, null, 0.0, 356500, strftime('%Y%m%d', date('now')))," +
    //                    "(141, 53, 78, 5, 16500.0, null, 0.0, 82500, strftime('%Y%m%d', date('now')))," +
    //                    "(142, 53, 79, 5, 17800.0, null, 0.0, 89000, strftime('%Y%m%d', date('now')))," +
    //                    "(143, 53, 80, 5, 19000.0, null, 0.0, 95000, strftime('%Y%m%d', date('now')))," +
    //                    "(144, 53, 81, 5, 25600.0, null, 0.0, 132500, strftime('%Y%m%d', date('now')))," +
    //                    "(145, 54, 38, 5, 47500.0, null, 0.0, 237500, strftime('%Y%m%d', date('now')))," +
    //                    "(146, 54, 39, 5, 51000.0, null, 0.0, 255000, strftime('%Y%m%d', date('now')))," +
    //                    "(147, 54, 40, 5, 85000.0, null, 0.0, 425000, strftime('%Y%m%d', date('now')))," +
    //                    "(148, 54, 41, 5, 47500.0, null, 0.0, 237500, strftime('%Y%m%d', date('now')))," +
    //                    "(149, 54, 42, 5, 17500.0, null, 0.0, 875000, strftime('%Y%m%d', date('now')))," +
    //                    "(150, 54, 43, 5, 46500.0, null, 0.0, 232500, strftime('%Y%m%d', date('now')))," +
    //                    "(151, 54, 44, 5, 20500.0, null, 0.0, 102500, strftime('%Y%m%d', date('now')))," +
    //                    "(152, 55, 13, 5, 1900.0, null, 0.0, 9500, strftime('%Y%m%d', date('now')))," +
    //                    "(153, 55, 16, 5, 25000.0, null, 0.0, 125000, strftime('%Y%m%d', date('now')))," +
    //                    "(154, 55, 14, 5, 3100.0, null, 0.0, 15500, strftime('%Y%m%d', date('now')))," +
    //                    "(155, 55, 15, 5, 7900.0, null, 0.0, 39500, strftime('%Y%m%d', date('now')))," +
    //                    "(156, 56, 5, 5, 45000.0, null, 0.0, 225000, strftime('%Y%m%d', date('now')))," +
    //                    "(157, 56, 105, 5, 88000.0, null, 0.0, 440000, strftime('%Y%m%d', date('now')))," +
    //                    "(158, 56, 104, 5, 179000.0, null, 0.0, 895000, strftime('%Y%m%d', date('now')))," +
    //                    "(159, 57, 9, 5, 39000.0, null, 0.0, 195000, strftime('%Y%m%d', date('now')))," +
    //                    "(160, 57, 10, 5, 18000.0, null, 0.0, 90000, strftime('%Y%m%d', date('now')))," +
    //                    "(161, 57, 63, 5, 85000.0, null, 0.0, 425000, strftime('%Y%m%d', date('now')))," +
    //                    "(162, 57, 65, 5, 132000.0, null, 0.0, 660000, strftime('%Y%m%d', date('now')))," +
    //                    "(163, 58, 32, 5, 8500.0, null, 0.0, 42500, strftime('%Y%m%d', date('now')))," +
    //                    "(164, 58, 33, 5, 10000.0, null, 0.0, 50000, strftime('%Y%m%d', date('now')))," +
    //                    "(165, 58, 34, 5, 6800.0, null, 0.0, 34000, strftime('%Y%m%d', date('now')))," +
    //                    "(166, 58, 35, 5, 18500.0, null, 0.0, 92500, strftime('%Y%m%d', date('now')))," +
    //                    "(167, 58, 36, 5, 5500.0, null, 0.0, 27500, strftime('%Y%m%d', date('now')))," +
    //                    "(168, 59, 45, 5, 28000.0, null, 0.0, 140000, strftime('%Y%m%d', date('now')))," +
    //                    "(169, 59, 47, 5, 17500.0, null, 0.0, 87500, strftime('%Y%m%d', date('now')))," +
    //                    "(170, 59, 48, 5, 51000.0, null, 0.0, 255000, strftime('%Y%m%d', date('now')))," +
    //                    "(171, 59, 49, 5, 25500.0, null, 0.0, 127500, strftime('%Y%m%d', date('now')))," +
    //                    "(172, 60, 22, 5, 13000.0, null, 0.0, 65000, strftime('%Y%m%d', date('now')))," +
    //                    "(173, 60, 23, 5, 9000.0, null, 0.0, 45000, strftime('%Y%m%d', date('now')))," +
    //                    "(174, 60, 24, 5, 5800.0, null, 0.0, 29000, strftime('%Y%m%d', date('now')))," +
    //                    "(175, 60, 25, 5, 14000.0, null, 0.0, 70000, strftime('%Y%m%d', date('now')))," +
    //                    "(176, 60, 28, 5, 12500.0, null, 0.0, 62500, strftime('%Y%m%d', date('now')))," +
    //                    "(177, 61, 2, 7, 1000, null, 0.0, 7000, strftime('%Y%m%d', date('now')))," +
    //                    "(178, 61, 3, 10, 1500, null , 0.0, 15000, strftime('%Y%m%d', date('now')))," +
    //                    "(179, 62, 4, 8, 50000, null, 0.0, 400000, strftime('%Y%m%d', date('now')))," +
    //                    "(180, 63, 6, 12, 3100, null, 0.0, 37200, strftime('%Y%m%d', date('now')))," +
    //                    "(181, 63, 6, 12, 3000, null, 0.0, 36000, strftime('%Y%m%d', date('now')));");
    //            database.execSQL("insert into ExpenseName (id, type, name, createdDate) values" +
    //                    "(2, 'Income', 'Comission', strftime('%Y%m%d', date('now')))," +
    //                    "(3, 'Expense', 'Snack', strftime('%Y%m%d', date('now')))," +
    //                    "(4, 'Expense', 'HouseHold Expenses', strftime('%Y%m%d', date('now')))," +
    //                    "(5, 'Income', 'Real Estate Agent Fees', strftime('%Y%m%d', date('now')))," +
    //                    "(6, 'Expense', 'Orphange Fund', strftime('%Y%m%d', date('now')));");
    //            database.execSQL("insert into Expense(id, expenseNameID, amount, day, month, year, date, createdDate, time) values " +
    //                    "(1, 2, 30000, '6', '11', '2016', '20161106', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-06', time('now', 'localtime')))," +
    //                    "(2, 3, 5000, '7', '11', '2016', '20161107', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-07', time('now', 'localtime')))," +
    //                    "(3, 3, 7000, '16', '11', '2016', '20161116', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-16', time('now', 'localtime')))," +
    //                    "(4, 3, 11000, '21', '11', '2016', '20161121', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-21', time('now', 'localtime')))," +
    //                    "(5, 4, 150000, '30', '11', '2016', '20161130', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-30', time('now', 'localtime')))," +
    //                    "(6, 2, 110000, '30', '11', '2016', '20161130', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-30', time('now', 'localtime')))," +
    //                    "(7, 5, 80000, '27', '11', '2016', '20161127', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-27', time('now', 'localtime')))," +
    //                    "(8, 2, 55000, '3', '12', '2016', '20161203', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-03', time('now', 'localtime')))," +
    //                    "(9, 3, 8000, '8', '12', '2016', '20161208', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-08', time('now', 'localtime')))," +
    //                    "(10, 5, 150000, '12', '12', '2016', '20161212', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-12', time('now', 'localtime')))," +
    //                    "(11, 3, 9000, '15', '12', '2016', '20161215', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-15', time('now', 'localtime')))," +
    //                    "(12, 6, 90000, '18', '12', '2016', '20161218', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-18', time('now', 'localtime')))," +
    //                    "(13, 4, 300000, '31', '12', '2016', '20161231', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-31', time('now', 'localtime')))," +
    //                    "(14, 3, 6000, '1', '1', '2017', '20170101', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-01', time('now', 'localtime')))," +
    //                    "(15, 3, 7000, '4', '1', '2017', '20170104', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-04', time('now', 'localtime')))," +
    //                    "(16, 3, 8500, '8', '1', '2017', '20170108', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-08', time('now', 'localtime')))," +
    //                    "(17, 2, 60000, '10', '1', '2017', '20170110', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-10', time('now', 'localtime')))," +
    //                    "(18, 6, 110000, '14', '1', '2017', '20170114', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-14', time('now', 'localtime')))," +
    //                    "(19, 4, 250000, '31', '1', '2017', '20170131', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-31', time('now', 'localtime')))," +
    //                    "(20, 3, 5600, '1', '2', '2017', '20170201', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-01', time('now', 'localtime')))," +
    //                    "(21, 3, 4500, '10', '2', '2017', '20170210', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-10', time('now', 'localtime')))," +
    //                    "(22, 5, 250000, '16', '2', '2017', '20170216', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-16', time('now', 'localtime')))," +
    //                    "(23, 5, 300000, '18', '2', '2017', '20170218', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-18', time('now', 'localtime')))," +
    //                    "(24, 2, 39000, '26', '2', '2017', '20170226', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-26', time('now', 'localtime')))," +
    //                    "(25, 4, 250000, '28', '2', '2017', '20170228', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-28', time('now', 'localtime')));"
    //            );
    //            database.execSQL("insert into Damage(id, stockID, stockQty, day, month, year, date, remark, createdDate, time, userID) values" +
    //                    "(1, 5, 1, '11', '11', '2016', '20161111', 'broken cover', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-11', time('now', 'localtime')), 1)," +
    //                    "(2, 99, 1, '12', '12', '2016', '20161212', 'broken cover', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-12', time('now', 'localtime')), 1)," +
    //                    "(3, 104, 1, '17', '12', '2016', '20161217', 'broken necklace chain', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-17', time('now', 'localtime')), 1)," +
    //                    "(4, 56, 2, '25', '1', '2017', '20170125', 'with mouse bite', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-25', time('now', 'localtime')), 1)," +
    //                    "(5, 90, 1, '24', '2', '2017', '20170224', 'dropped by customer', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-24', time('now', 'localtime')), 1)," +
    //                    "(6, 5, 1, '13', '11', '2016', '20161112', 'broken cover', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-12', time('now', 'localtime')), 1)," +
    //                    "(7, 99, 1, '13', '12', '2016', '20161213', 'broken cover', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-13', time('now', 'localtime')), 1)," +
    //                    "(8, 104, 1, '18', '12', '2016', '20161218', 'broken necklace chain', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-18', time('now', 'localtime')), 1)," +
    //                    "(9, 56, 2, '26', '1', '2017', '20170126', 'with mouse bite', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-26', time('now', 'localtime')), 1)," +
    //                    "(10, 90, 1, '25', '2', '2017', '20170225', 'dropped by customer', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-25', time('now', 'localtime')), 1)," +
    //                    "(11, 8, 1, '11', '3', '2017', '20170311', 'broken cover', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-11', time('now', 'localtime')), 1)," +
    //                    "(12, 14, 1, '12', '3', '2017', '20170312', 'broken cover', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-12', time('now', 'localtime')), 1)," +
    //                    "(13, 23, 1, '17', '3', '2017', '20170317', 'broken necklace chain', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-17', time('now', 'localtime')), 1)," +
    //                    "(14, 65, 2, '25', '3', '2017', '20170325', 'with mouse bite', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-25', time('now', 'localtime')), 1)," +
    //                    "(15, 59, 1, '24', '3', '2017', '20170324', 'dropped by customer', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-24', time('now', 'localtime')), 1)," +
    //                    "(16, 8, 1, '13', '3', '2017', '20170313', 'broken cover', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-13', time('now', 'localtime')), 1)," +
    //                    "(17, 14, 1, '15', '3', '2017', '20170315', 'broken cover', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-15', time('now', 'localtime')), 1)," +
    //                    "(18, 23, 1, '19', '3', '2017', '20170319', 'broken necklace chain', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-15', time('now', 'localtime')), 1)," +
    //                    "(19, 65, 2, '27', '3', '2017', '20170327', 'with mouse bite', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-27', time('now', 'localtime')), 1)," +
    //                    "(20, 59, 1, '26', '3', '2017', '20170326', 'dropped by customer', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-26', time('now', 'localtime')), 1)");
    //            database.execSQL("insert into Lost(id, stockID, stockQty, day, month, year, date, remark, createdDate, time, userID) values" +
    //                    "(1, 68, 1, '30', '11', '2016', '20161130', 'stolen by customer', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-30', time('now', 'localtime')), 1)," +
    //                    "(2, 72, 1, '12', '12', '2016', '20161212', 'stolen by customer', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-12', time('now', 'localtime')), 1)," +
    //                    "(3, 46, 1, '18', '12', '2016', '20161218', 'lost during delivery', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-18', time('now', 'localtime')), 1)," +
    //                    "(4, 52, 1, '26', '1', '2017', '20170126', 'lost during delivery', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-26', time('now', 'localtime')), 1)," +
    //                    "(5, 83, 1, '26', '2', '2017', '20170226', 'lost during delivery', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-26', time('now', 'localtime')), 1)," +
    //                    "(6, 68, 1, '30', '11', '2016', '20161130', 'stolen by customer', strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-30', time('now', 'localtime')), 1)," +
    //                    "(7, 72, 1, '12', '12', '2016', '20161212', 'stolen by customer', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-12', time('now', 'localtime')), 1)," +
    //                    "(8, 46, 1, '18', '12', '2016', '20161218', 'lost during delivery', strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-18', time('now', 'localtime')), 1)," +
    //                    "(9, 52, 1, '26', '1', '2017', '20170126', 'lost during delivery', strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-26', time('now', 'localtime')), 1)," +
    //                    "(10, 83, 1, '26', '2', '2017', '20170226', 'lost during delivery', strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-26', time('now', 'localtime')), 1)," +
    //                    "(11, 29, 1, '30', '3', '2017', '20170330', 'stolen by customer', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-30', time('now', 'localtime')), 1)," +
    //                    "(12, 48, 1, '12', '3', '2017', '20170312', 'stolen by customer', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-12', time('now', 'localtime')), 1)," +
    //                    "(13, 36, 1, '18', '3', '2017', '20170318', 'lost during delivery', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-18', time('now', 'localtime')), 1)," +
    //                    "(14, 97, 1, '26', '3', '2017', '20170326', 'lost during delivery', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-26', time('now', 'localtime')), 1)," +
    //                    "(15, 44, 1, '26', '3', '2017', '20170326', 'lost during delivery', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-26', time('now', 'localtime')), 1)," +
    //                    "(16, 29, 1, '30', '3', '2017', '20170330', 'stolen by customer', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-30', time('now', 'localtime')), 1)," +
    //                    "(17, 48, 1, '12', '3', '2017', '20170312', 'stolen by customer', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-12', time('now', 'localtime')), 1)," +
    //                    "(18, 36, 1, '18', '3', '2017', '20170318', 'lost during delivery', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-18', time('now', 'localtime')), 1)," +
    //                    "(19, 97, 1, '26', '3', '2017', '20170326', 'lost during delivery', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-26', time('now', 'localtime')), 1)," +
    //                    "(20, 44, 1, '26', '3', '2017', '20170326', 'lost during delivery', strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-26', time('now', 'localtime')), 1)");
    //            database.execSQL("insert into Customer (id, name, phoneNo, balance) values" +
    //                    "(2, 'Su Su', '098654373', 0.0);");
    //            database.execSQL("insert into Sales(id, voucherNo, customerID, type, subTotal, discount, discountAmt, taxID, taxAmt, taxRate, taxType, totalAmount, paidAmt, balance, date, day, month, year, createdDate, time, remark) values" +
    //                    "(1, 'S0001', 1, 'Sales', 7100, null, 0.0, 3, 355, 5.0, 'Inclusive', 7100, 7100, 0.0, '20170203', '3', '2', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-03', time('now', 'localtime')), null), " +
    //                    "(2, 'S0002', 1, 'Sales', 105500, null, 0.0, 3, 5275, 5.0, 'Inclusive', 105500, 105500, 0.0, '20170204', '4', '2', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-04', time('now', 'localtime')), null), " +
    //                    "(3, 'S0003', 1, 'Sales', 51700, null, 0.0, 3, 2585, 5.0, 'Inclusive', 51700, 51700, 0.0, '20170206', '6', '2', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-06', time('now', 'localtime')), null), " +
    //                    "(4, 'S0004', 1, 'Sales', 101500, null, 0.0, 3, 5075, 5.0, 'Inclusive', 101500, 101500, 0.0, '20170207', '7', '2', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-07', time('now', 'localtime')), null), " +
    //                    "(5, 'S0005', 1, 'Sales', 225800, null, 0.0, 3, 11290, 5.0, 'Inclusive', 225800, 225800, 0.0, '20170209', '9', '2', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-09', time('now', 'localtime')), null), " +
    //                    "(6, 'S0006', 2, 'Sales', 74800, null, 0.0, 3, 3740, 5.0, 'Inclusive', 74800, 50000, 24800.0, '20170210', '10', '2', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-10', time('now', 'localtime')), null), " +
    //                    "(7, 'S0007', 1, 'Sales', 67600, null, 0.0, 3, 3380, 5.0, 'Inclusive', 67600, 67600, 0.0, '20170212', '12', '2', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-12', time('now', 'localtime')), null), " +
    //                    "(8, 'S0008', 1, 'Sales', 47800, null, 0.0, 3, 2390, 5.0, 'Inclusive', 47800, 47800, 0.0, '20170213', '13', '2', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-13', time('now', 'localtime')), null), " +
    //                    "(9, 'S0009', 1, 'Sales', 15000, null, 0.0, 3, 750, 5.0, 'Inclusive', 15000, 15000, 0.0, '20170215', '15', '2', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-15', time('now', 'localtime')), null), " +
    //                    "(10, 'S0010', 1, 'Sales', 229130, null, 0.0, 3, 11456.6, 5.0, 'Inclusive', 229310, 229310, 0.0, '20170217', '17', '2', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-17', time('now', 'localtime')), null), " +
    //                    "(11, 'S0011', 1, 'Sales', 37230, null, 0.0, 3, 1861.5, 5.0, 'Inclusive', 37230, 37230, 0.0, '20170220', '20', '2', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-20', time('now', 'localtime')), null), " +
    //                    "(12, 'S0012', 1, 'Sales', 37230, null, 0.0, 3, 1861.5, 5.0, 'Inclusive', 37230, 37230, 0.0, '20170223', '23', '2', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-02-23', time('now', 'localtime')), null)," +
    //                    "(13, 'S0013', 1, 'Sales', 702600, null, 0.0, 3, 35130, 5.0, 'Inclusive', 702600, 702600, 0.0, '20170101', '1', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-01', time('now', 'localtime')), null)," +
    //                    "(14, 'S0014', 1, 'Sales', 665000, null, 0.0, 3, 33250, 5.0, 'Inclusive', 665000, 665000, 0.0, '20170102', '2', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-02', time('now', 'localtime')), null)," +
    //                    "(15, 'S0015', 1, 'Sales', 581000, null, 0.0, 3, 29050, 5.0, 'Inclusive', 665000, 665000, 0.0, '20170104', '4', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-04', time('now', 'localtime')), null)," +
    //                    "(16, 'S0016', 1, 'Sales', 162000, null, 0.0, 3, 8100, 5.0, 'Inclusive', 162000, 162000, 0.0, '20170106', '6', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-06', time('now', 'localtime')), null)," +
    //                    "(17, 'S0017', 1, 'Sales', 112800, null, 0.0, 3, 5640, 5.0, 'Inclusive', 112800, 112800, 0.0, '20170108', '8', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-08', time('now', 'localtime')), null)," +
    //                    "(18, 'S0018', 1, 'Sales', 168800, null, 0.0, 3, 8440, 5.0, 'Inclusive', 168800, 168800, 0.0, '20170110', '10', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-10', time('now', 'localtime')), null)," +
    //                    "(19, 'S0019', 1, 'Sales', 186400, null, 0.0, 3, 9320, 5.0, 'Inclusive', 186400, 186400, 0.0, '20170112', '12', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-12', time('now', 'localtime')), null)," +
    //                    "(20, 'S0020', 1, 'Sales', 59300, null, 0.0, 3, 2965, 5.0, 'Inclusive', 59300, 59300, 0.0, '20170115', '15', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-15', time('now', 'localtime')), null)," +
    //                    "(21, 'S0021', 1, 'Sales', 323200, null, 0.0, 3, 16160, 5.0, 'Inclusive', 323200, 323200, 0.0, '20170117', '17', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-17', time('now', 'localtime')), null)," +
    //                    "(22, 'S0022', 1, 'Sales', 126600, null, 0.0, 3, 6330, 5.0, 'Inclusive', 126600, 126600, 0.0, '20170121', '21', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-21', time('now', 'localtime')), null)," +
    //                    "(23, 'S0023', 1, 'Sales', 187300, null, 0.0, 3, 9365, 5.0, 'Inclusive', 187300, 187300, 0.0, '20170124', '24', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-24', time('now', 'localtime')), null)," +
    //                    "(24, 'S0024', 1, 'Sales', 289400, null, 0.0, 3, 14470, 5.0, 'Inclusive', 289400, 289400, 0.0, '20170128', '28', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-28', time('now', 'localtime')), null)," +
    //                    "(25, 'S0025', 1, 'Sales', 159000, null, 0.0, 3, 7950, 5.0, 'Inclusive', 159000, 159000, 0.0, '20170128', '28', '1', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-01-28', time('now', 'localtime')), null)," +
    //                    "(26, 'S0026', 1, 'Sales', 311400, null, 0.0, 3, 15570, 5.0, 'Inclusive', 311400, 311400, 0.0, '20161201', '1', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-01', time('now', 'localtime')), null)," +
    //                    "(27, 'S0027', 1, 'Sales', 366500, null, 0.0, 3, 18325, 5.0, 'Inclusive', 366500, 366500, 0.0, '20161203', '3', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-03', time('now', 'localtime')), null)," +
    //                    "(28, 'S0028', 1, 'Sales', 193100, null, 0.0, 3, 9655, 5.0, 'Inclusive', 193100, 193100, 0.0, '20161206', '6', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-06', time('now', 'localtime')), null)," +
    //                    "(29, 'S0029', 1, 'Sales', 246000, null, 0.0, 3, 12300, 5.0, 'Inclusive', 246000, 246000, 0.0, '20161210', '10', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-10', time('now', 'localtime')), null)," +
    //                    "(30, 'S0030', 1, 'Sales', 75100, null, 0.0, 3, 3755, 5.0, 'Inclusive', 75100, 75100, 0.0, '20161210', '10', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-10', time('now', 'localtime')), null)," +
    //                    "(31, 'S0031', 1, 'Sales', 209000, null, 0.0, 3, 10450, 5.0, 'Inclusive', 209000, 209000, 0.0, '20161213', '13', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-13', time('now', 'localtime')), null)," +
    //                    "(32, 'S0032', 1, 'Sales', 42500, null, 0.0, 3, 2125, 5.0, 'Inclusive', 42500, 42500, 0.0, '20161217', '17', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-17', time('now', 'localtime')), null)," +
    //                    "(33, 'S0033', 1, 'Sales', 79000, null, 0.0, 3, 3950, 5.0, 'Inclusive', 79000, 79000, 0.0, '20161219', '19', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-19', time('now', 'localtime')), null)," +
    //                    "(34, 'S0034', 1, 'Sales', 94000, null, 0.0, 3, 4700, 5.0, 'Inclusive', 94000, 94000, 0.0, '20161222', '22', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-22', time('now', 'localtime')), null)," +
    //                    "(35, 'S0035', 1, 'Sales', 138000, null, 0.0, 3, 6900, 5.0, 'Inclusive', 138000, 138000, 0.0, '20161225', '25', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-25', time('now', 'localtime')), null)," +
    //                    "(36, 'S0036', 1, 'Sales', 39600, null, 0.0, 3, 1980, 5.0, 'Inclusive', 39600, 39600, 0.0, '20161227', '27', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-27', time('now', 'localtime')), null)," +
    //                    "(37, 'S0037', 1, 'Sales', 5100, null, 0.0, 3, 255, 5.0, 'Inclusive', 5100, 5100, 0.0, '20161229', '29', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-29', time('now', 'localtime')), null)," +
    //                    "(38, 'S0038', 1, 'Sales', 186000, null, 150.0, 3, 9307.5, 5.0, 'Inclusive', 186000, 186000, 0.0, '20161231', '31', '12', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-12-31', time('now', 'localtime')), null)," +
    //                    "(39, 'S0039', 1, 'Sales', 37400, null, 0.0, 3, 1870, 5.0, 'Inclusive', 37400, 37400, 0.0, '20161101', '1', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-01', time('now', 'localtime')), null)," +
    //                    "(40, 'S0040', 1, 'Sales', 139000, null, 0.0, 3, 6950, 5.0, 'Inclusive', 37400, 37400, 0.0, '20161104', '4', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-04', time('now', 'localtime')), null)," +
    //                    "(41, 'S0041', 1, 'Sales', 65900, null, 0.0, 3, 3295, 5.0, 'Inclusive', 65900, 65900, 0.0, '20161106', '6', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-06', time('now', 'localtime')), null)," +
    //                    "(42, 'S0042', 1, 'Sales', 11500, null, 0.0, 3, 575, 5.0, 'Inclusive', 11500, 11500, 0.0, '20161109', '9', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-09', time('now', 'localtime')), null)," +
    //                    "(43, 'S0043', 1, 'Sales', 76500, null, 0.0, 3, 3825, 5.0, 'Inclusive', 76500, 76500, 0.0, '20161111', '11', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-11', time('now', 'localtime')), null)," +
    //                    "(44, 'S0044', 1, 'Sales', 97000, null, 0.0, 3, 4850, 5.0, 'Inclusive', 97000, 97000, 0.0, '20161115', '15', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-15', time('now', 'localtime')), null)," +
    //                    "(45, 'S0045', 1, 'Sales', 173500, null, 0.0, 3, 1275, 5.0, 'Inclusive', 173500, 173500, 0.0, '20161119', '19', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-19', time('now', 'localtime')), null)," +
    //                    "(46, 'S0046', 1, 'Sales', 54900, null, 0.0, 3, 2745, 5.0, 'Inclusive', 54900, 54900, 0.0, '20161120', '20', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-20', time('now', 'localtime')), null)," +
    //                    "(47, 'S0047', 1, 'Sales', 215000, null, 0.0, 3, 10750, 5.0, 'Inclusive', 215000, 215000, 0.0, '20161122', '22', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-22', time('now', 'localtime')), null)," +
    //                    "(48, 'S0048', 1, 'Sales', 61900, null, 0.0, 3, 3095, 5.0, 'Inclusive', 61900, 61900, 0.0, '20161123', '23', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-23', time('now', 'localtime')), null)," +
    //                    "(49, 'S0049', 1, 'Sales', 74500, null, 0.0, 3, 3725, 5.0, 'Inclusive', 74500, 74500, 0.0, '20161125', '25', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-25', time('now', 'localtime')), null)," +
    //                    "(50, 'S0050', 1, 'Sales', 295900, null, 0.0, 3, 14795, 5.0, 'Inclusive', 295900, 295900, 0.0, '20161127', '27', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-27', time('now', 'localtime')), null)," +
    //                    "(51, 'S0051', 1, 'Sales', 51300, null, 0.0, 3, 2565, 5.0, 'Inclusive', 51300, 51300, 0.0, '20161130', '30', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-30', time('now', 'localtime')), null)," +
    //                    "(52, 'S0052', 1, 'Sales', 53000, null, 0.0, 3, 2650, 5.0, 'Inclusive', 53000, 53000, 0.0, '20161130', '30', '11', '2016',strftime('%Y%m%d', date('now')), strftime('%s', '2016-11-30', time('now', 'localtime')), null)," +
    //                    "(53, 'S0053', 2, 'Sales', 78500, null, 425.0, 3, 3925, 5.0, 'Inclusive', 78500, 78500, 40000.0, '20170325', '25', '3', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-25', time('now', 'localtime')), null)," +
    //                    "(54, 'S0054', 1, 'Sales', 302500, null, 625.0, 3, 15125, 5.0, 'Inclusive', 302500, 302500, 0.0, '20170327', '27', '3', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-27', time('now', 'localtime')), null)," +
    //                    "(55, 'S0055', 1, 'Sales', 50000, null, 0.0, 3, 2500, 5.0, 'Inclusive', 50000, 50000, 0.0, '20170330', '30', '3', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-30', time('now', 'localtime')), null)," +
    //                    "(56, 'S0056', 1, 'Sales', 56000, null, 0.0, 3, 2800, 5.0, 'Inclusive', 56000, 56000, 0.0, '20170330', '30', '3', '2017',strftime('%Y%m%d', date('now')), strftime('%s', '2017-03-30', time('now', 'localtime')), null);");
    //            database.execSQL("insert into SalesDetail(id, salesID, stockID, qty, price, total, discount, discountAmt, createdDate) values " +
    //                    "(1, 1, 1, 1, 5000, 5000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(2, 1, 3, 1, 2100, 2100, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(3, 2, 2, 1, 1500, 1500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(4, 2, 4, 1, 55000, 55000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(5, 2, 5, 1, 49000, 49000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(6, 3, 6, 1, 3700, 3700, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(7, 3, 7, 1, 18000, 18000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(8, 3, 8, 1, 30000, 30000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(9, 4, 10, 1, 29000, 29000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(10, 4, 9, 1, 49000, 49000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(11, 4, 11, 1, 23500, 23500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(12, 5, 41, 1, 54500, 54500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(13, 5, 43, 1, 55000, 55000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(14, 5, 39, 1, 59800, 59800, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(15, 5, 38, 1, 56500, 56500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(16, 6, 29, 1, 13800, 13800, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(17, 6, 27, 2, 13500, 27000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(18, 6, 30, 1, 26000, 26000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(19, 6, 31, 2, 4000, 8000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(20, 7, 22, 1, 19200, 19200, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(21, 7, 26, 1, 21500, 21500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(22, 7, 31, 1, 4000, 4000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(23, 7, 37, 1, 22900, 22900, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(24, 8, 15, 1, 9600, 9600, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(25, 8, 13, 2, 2600, 5200, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(26, 8, 18, 1, 10500, 10500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(27, 8, 25, 1, 22500, 22500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(28, 9, 27, 1, 13500, 13500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(29, 9, 2, 1, 1500, 1500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(30, 10, 104, 1, 219000, 37230, '83%', 181770.0, strftime('%Y%m%d', date('now')))," +
    //                    "(31, 10, 99, 1, 48900, 48900, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(32, 10, 58, 1, 143000, 143000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(33, 11, 104, 1, 219000, 37230, '83%', 181770.0, strftime('%Y%m%d', date('now')))," +
    //                    "(34, 12, 104, 1, 219000, 37230, '83%', 181770.0, strftime('%Y%m%d', date('now')))," +
    //                    "(35, 13, 28, 3, 17700, 53100, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(36, 13, 52, 3, 135000, 405000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(37, 13, 35, 5, 14500, 72500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(38, 13, 78, 8, 21500, 172000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(39, 14, 55, 5, 50000, 250000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(40, 14, 60, 1, 18500, 18500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(41, 14, 43, 3, 53000, 159000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(42, 14, 11, 1, 21500, 21500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(43, 14, 105, 2, 108000, 216000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(44, 15, 58, 2, 138000, 276000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(45, 15, 45, 2, 35000, 70000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(46, 15, 63, 2, 115000, 230000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(47, 15, 72, 2, 20000, 40000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(48, 16, 70, 2, 6500, 13000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(49, 16, 85, 2, 15000, 30000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(50, 16, 23, 2, 9000, 18000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(51, 16, 41, 2, 50500, 101000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(52, 17, 87, 2, 7800, 15600, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(53, 17, 12, 2, 7600, 15200, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(54, 17, 16, 2, 28000, 56000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(55, 17, 32, 2, 13000, 26000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(56, 18, 16, 2, 29500, 59000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(57, 18, 47, 2, 25000, 50000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(58, 18, 8, 2, 28000, 56000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(59, 18, 3, 2, 1900, 3800, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(60, 19, 6, 2, 3100, 6200, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(61, 19, 2, 2, 1300, 2600, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(62, 19, 58, 1, 134000, 134000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(63, 19, 96, 2, 9000, 18000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(64, 19, 34, 1, 12800, 12800, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(65, 20, 35, 1, 16800, 16800, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(66, 20, 29, 1, 12500, 12500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(67, 20, 97, 1, 10300, 10300, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(68, 20, 64, 1, 6900, 6900, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(69, 21, 88, 1, 58000, 58000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(70, 21, 33, 1, 11800, 11800, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(71, 21, 44, 1, 25500, 25500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(72, 21, 66, 1, 180000, 180000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(73, 21, 99, 1, 47900, 47900, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(74, 22, 44, 1, 24500, 24500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(75, 22, 88, 1, 57000, 57000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(76, 22, 29, 1, 11800, 11800, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(77, 22, 21, 1, 25800, 25800, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(78, 22, 12, 1, 7500, 7500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(79, 23, 7, 1, 16500, 16500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(80, 23, 4, 2, 53000, 106000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(81, 23, 76, 1, 20300, 20300, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(82, 23, 82, 3, 11500, 34500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(83, 23, 94, 1, 10000, 10000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(84, 24, 92, 1, 10200, 10200, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(85, 24, 62, 1, 188000, 188000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(86, 24, 78, 1, 22700, 22700, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(87, 24, 51, 1, 39500, 39500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(88, 24, 46, 1, 29000, 29000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(89, 25, 36, 2, 21500, 43000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(90, 25, 69, 3, 20500, 61500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(91, 25, 24, 4, 8500, 34000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(92, 25, 26, 1, 20500, 20500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(93, 26, 58, 1, 139000, 139000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(94, 26, 87, 1, 8300, 8300, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(95, 26, 59, 1, 136000, 136000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(96, 26, 25, 1, 19500, 19500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(97, 26, 24, 1, 8600, 8600, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(98, 27, 15, 1, 9000, 9000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(99, 27, 28, 3, 19000, 57000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(100, 27, 59, 2, 139000, 278000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(101, 27, 79, 1, 22500, 22500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(102, 28, 25, 1, 20500, 20500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(103, 28, 63, 1, 119000, 119000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(104, 28, 38, 1, 53600, 53600, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(105, 29, 79, 1, 23500, 23500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(106, 29, 58, 1, 139000, 139000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(107, 29, 16, 1, 29000, 29000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(108, 29, 38, 1, 54500, 54500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(109, 30, 15, 1, 8600, 8600, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(110, 30, 28, 1, 18500, 18500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(111, 30, 93, 1, 48000, 48000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(112, 31, 104, 1, 209000, 209000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(113, 32, 100, 1, 41000, 41000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(114, 32, 2, 1, 1500, 1500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(115, 33, 4, 1, 51000, 51000, null, 3000.0, strftime('%Y%m%d', date('now')))," +
    //                    "(116, 33, 8, 1, 28000, 28000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(117, 34, 5, 1, 46000, 28000, null, 1000.0, strftime('%Y%m%d', date('now')))," +
    //                    "(118, 34, 9, 1, 48000, 28000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(119, 35, 61, 1, 138000, 138000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(120, 36, 75, 1, 27800, 27800, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(121, 36, 36, 1, 11800, 11800, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(122, 37, 31, 1, 3800, 3800, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(123, 37, 2, 1, 1300, 1300, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(124, 38, 104, 1, 186150, 186150, '15%', 32850.0, strftime('%Y%m%d', date('now')))," +
    //                    "(125, 39, 56, 1, 16900, 16900, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(126, 39, 69, 1, 20500, 20500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(127, 40, 61, 1, 139000, 139000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(128, 41, 53, 1, 47000, 47000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(129, 41, 28, 1, 18900, 18900, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(130, 42, 82, 1, 11500, 11500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(131, 43, 9, 1, 47000, 47000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(132, 43, 20, 1, 29500, 29500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(133, 44, 85, 1, 16000, 16000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(134, 44, 101, 1, 81000, 81000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(135, 45, 52, 1, 148000, 148000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(136, 45, 74, 1, 25500, 25500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(137, 46, 37, 1, 21900, 21900, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(138, 46, 49, 1, 33000, 33000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(139, 47, 21, 1, 26800, 26800, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(140, 47, 35, 1, 18200, 18200, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(141, 47, 65, 1, 170000, 170000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(142, 48, 50, 1, 27500, 27500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(143, 48, 73, 1, 21500, 21500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(144, 48, 29, 1, 12900, 12900, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(145, 49, 16, 1, 29000, 29000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(146, 49, 26, 1, 20500, 20500, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(147, 49, 47, 1, 25000, 25000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(148, 50, 41, 1, 53600, 53600, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(149, 50, 85, 1, 16300, 16300, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(150, 50, 67, 1, 226000, 226000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(151, 51, 53, 1, 48000, 48000, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(152, 52, 74, 1, 24500, 3300, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(153, 52, 68, 1, 28500, 3300, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(154, 53, 16, 1, 31000.0, 31000.0, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(155, 53, 26, 1, 21500.0, 21500.0, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(156, 53, 47, 1, 26000.0, 26000.0, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(157, 54, 41, 1, 54500.0, 54500.0, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(158, 54, 85, 1, 17000.0, 17000.0, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(159, 54, 67, 1, 231000.0, 231000.0, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(160, 55, 53, 1, 50000.0, 50000.0, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(161, 56, 74, 1, 26500.0, 26500.0, null, 0.0, strftime('%Y%m%d', date('now')))," +
    //                    "(162, 56, 68, 1, 29500.0, 29500.0, null, 0.0, strftime('%Y%m%d', date('now')));");
    //            database.setTransactionSuccessful();
    //        }catch (SQLiteException e){
    //            e.printStackTrace();
    //        }finally {
    //            database.endTransaction();
    //        }
    //
    //        Bitmap bitmap= BitmapFactory.decodeResource(context.getResources(), R.mipmap.st0013);
    //        ByteArrayOutputStream bos=new ByteArrayOutputStream();
    //        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
    //        byte[] img=bos.toByteArray();
    //        ContentValues contentValues = new ContentValues();
    //        contentValues.put(AppConstant.STOCK_IMAGE_ID, 1);
    //        contentValues.put(AppConstant.STOCK_IMAGE_STOCK_ID, 13);
    //        contentValues.put(AppConstant.STOCK_IMAGE_IMG, img);
    //        database.insert(AppConstant.STOCK_IMAGE_TABLE_NAME, null, contentValues);
    //
    //        bitmap= BitmapFactory.decodeResource(context.getResources(), R.mipmap.st0019);
    //        bos=new ByteArrayOutputStream();
    //        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
    //        img=bos.toByteArray();
    //        contentValues = new ContentValues();
    //        contentValues.put(AppConstant.STOCK_IMAGE_ID, 2);
    //        contentValues.put(AppConstant.STOCK_IMAGE_STOCK_ID, 19);
    //        contentValues.put(AppConstant.STOCK_IMAGE_IMG, img);
    //        database.insert(AppConstant.STOCK_IMAGE_TABLE_NAME, null, contentValues);
    //
    //        bitmap= BitmapFactory.decodeResource(context.getResources(), R.mipmap.st0019b);
    //        bos=new ByteArrayOutputStream();
    //        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
    //        img=bos.toByteArray();
    //        contentValues = new ContentValues();
    //        contentValues.put(AppConstant.STOCK_IMAGE_ID, 3);
    //        contentValues.put(AppConstant.STOCK_IMAGE_STOCK_ID, 19);
    //        contentValues.put(AppConstant.STOCK_IMAGE_IMG, img);
    //        database.insert(AppConstant.STOCK_IMAGE_TABLE_NAME, null, contentValues);
    //
    //        bitmap= BitmapFactory.decodeResource(context.getResources(), R.mipmap.st0019f);
    //        bos=new ByteArrayOutputStream();
    //        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
    //        img=bos.toByteArray();
    //        contentValues = new ContentValues();
    //        contentValues.put(AppConstant.STOCK_IMAGE_ID, 4);
    //        contentValues.put(AppConstant.STOCK_IMAGE_STOCK_ID, 19);
    //        contentValues.put(AppConstant.STOCK_IMAGE_IMG, img);
    //        database.insert(AppConstant.STOCK_IMAGE_TABLE_NAME, null, contentValues);
    //
}