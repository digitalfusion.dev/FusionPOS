package com.digitalfusion.android.pos.database.model;

/**
 * Created by MD002 on 8/18/16.
 */
public class LostItemInDetail {
    private Long id;
    private Long lostID;
    private Long stockID;
    private String stockName;
    private String stockCodeNo;
    private String stockBarcodeNo;
    private Long stockCategoryID;
    private String stockCategoryName;
    private Integer qty;
    private Double purchasePrice;
    private Double total;
    private Long unitID;
    private String unit;
    private String remark;

    public LostItemInDetail() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLostID() {
        return lostID;
    }

    public void setLostID(Long lostID) {
        this.lostID = lostID;
    }

    public Long getStockID() {
        return stockID;
    }

    public void setStockID(Long stockID) {
        this.stockID = stockID;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockCodeNo() {
        return stockCodeNo;
    }

    public void setStockCodeNo(String stockCodeNo) {
        this.stockCodeNo = stockCodeNo;
    }

    public String getStockBarcodeNo() {
        return stockBarcodeNo;
    }

    public void setStockBarcodeNo(String stockBarcodeNo) {
        this.stockBarcodeNo = stockBarcodeNo;
    }

    public Long getStockCategoryID() {
        return stockCategoryID;
    }

    public void setStockCategoryID(Long stockCategoryID) {
        this.stockCategoryID = stockCategoryID;
    }

    public String getStockCategoryName() {
        return stockCategoryName;
    }

    public void setStockCategoryName(String stockCategoryName) {
        this.stockCategoryName = stockCategoryName;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(Double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Long getUnitID() {
        return unitID;
    }

    public void setUnitID(Long unitID) {
        this.unitID = unitID;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
