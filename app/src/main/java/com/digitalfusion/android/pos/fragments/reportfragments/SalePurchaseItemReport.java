package com.digitalfusion.android.pos.fragments.reportfragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForCategoryMD;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForSalePurchaseItemReport;
import com.digitalfusion.android.pos.database.business.CategoryManager;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.AidlUtil;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.BluetoothUtil;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.ExportImportUtil;
import com.digitalfusion.android.pos.util.GrantPermission;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.PrintImage;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.views.FolderChooserDialog;
import com.digitalfusion.android.pos.views.FusionToast;
import com.digitalfusion.android.pos.views.Spinner;
import com.prologic.printapi.PrintCanvas;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.digitalfusion.android.pos.util.ESCUtil.GS;

public class SalePurchaseItemReport extends Fragment implements Serializable {
    private final static byte[] CUT_PAPER = new byte[]{GS, 0x56, 0x00};

    private String today;
    private String yesterday;
    private String thisWeek;
    private String lastWeek;
    private String thisMonth;
    private String lastMonth;
    private String customRange;
    private String pageTitle;

    private TextView dateFilterTextView;

    private RVAdapterForFilter rvAdapterForDateFilter;

    private MaterialDialog dateFilterDialog;

    private List<String> filterList;
    private List<ReportItem> reportItemList;

    private ReportManager reportManager;
    private View mainLayoutView;
    private Context context;
    private BluetoothUtil bluetoothUtil;

    private RecyclerView recyclerView;
    private TextView noDataText;
    private MaterialDialog customRangeDialog;
    private TextView startDateTextView, endDateTextView, categoryFilterView, totalValueView;
    private TextView traceDate;
    private Button customRangeOkBtn, customRangeCancelBtn;
    private DatePickerDialog startDatePickerDialog;

    private String startDate;
    private String endDate;
    private int startLimit;
    private int endLimit;
    private Double totalAmt;
    private String reportType;
    private String customStartDate,
            customEndDate;

    private String categoryID;
    private CategoryManager categoryManager;
    private boolean shouldLoad = true;
    private List<Category> categoryList;
    private List<Category> categoryFilterList;
    private String type;
    private ParentRVAdapterForReports rvAdapterForCategoryMD;
    private MaterialDialog categoryFilterDialog;
    private boolean isAll = true;

    private RVAdapterForSalePurchaseItemReport mRVAdapterForSalePurchaseItemReport;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_sale_purchase_item_report, container, false);
        context = getContext();
        setHasOptionsMenu(true);
        dateFilterTextView = mainLayoutView.findViewById(R.id.dateFilter    );
        recyclerView       = mainLayoutView.findViewById(R.id.itemList      );
        categoryFilterView = mainLayoutView.findViewById(R.id.categoryFilter);
        totalValueView     = mainLayoutView.findViewById(R.id.totalValue    );

        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            reportType = getArguments().getString("reportType");
        }

        if (reportType.equalsIgnoreCase("sale")) {
            pageTitle = ThemeUtil.getString(context, R.attr.sale_item_report);
        } else {
            pageTitle = ThemeUtil.getString(context, R.attr.purchase_item_report);
        }
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(pageTitle);

        initializeVariables();

        new LoadProgressDialog().execute("");

        configFilters();
        buildDateFilterDialog();
        buildingCustomRangeDialog();
        buildCategoryChooserDialog();
        clickListeners();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void configFilters() {
        today = ThemeUtil.getString(getContext(), R.attr.today);
        yesterday = ThemeUtil.getString(getContext(), R.attr.yesterday);
        thisWeek = ThemeUtil.getString(getContext(), R.attr.this_week);
        lastWeek = ThemeUtil.getString(getContext(), R.attr.last_week);
        thisMonth = ThemeUtil.getString(getContext(), R.attr.this_month);
        lastMonth = ThemeUtil.getString(getContext(), R.attr.last_month);
        customRange = ThemeUtil.getString(getContext(), R.attr.custom_range);

        filterList = new ArrayList<>();
        filterList.add(today);
        filterList.add(yesterday);
        filterList.add(thisWeek);
        filterList.add(lastWeek);
        filterList.add(thisMonth);
        filterList.add(lastMonth);
        filterList.add(customRange);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);
        setDatesAndLimits();

        categoryManager = new CategoryManager(context);
        categoryList = categoryManager.getAllCategories();
        categoryFilterList = new ArrayList<>();
        String all = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);
        categoryFilterList.add(new Category(all));
        for (Category c : categoryList) {
            categoryFilterList.add(c);
        }
        type = categoryFilterList.get(0).getName();
        categoryFilterView.setText(type);
        categoryID = "<> 0";

    }


    private void initializeList() {
        if (reportType.equalsIgnoreCase("sale")) {
            reportItemList = reportManager.allSales(startDate, endDate, startLimit, endLimit, categoryID);
            totalAmt = reportManager.getTotalAmountOfTotalAmountOfAllSales(startDate, endDate, categoryID);
            // Log.e("size", reportItemList.size()+ " ida");
        } else {
            reportItemList = reportManager.allPurchases(startDate, endDate, startLimit, endLimit, categoryID);
            totalAmt = reportManager.getTotalAmountOfTotalAmountOfAllPurchases(startDate, endDate, categoryID);
        }
    }

    public void buildingCustomRangeDialog() {
        buildingCustomRangeDatePickerDialog();
        String date = ThemeUtil.getString(context, R.attr.date_range);
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(date)
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    private void setDateFilterTextView(String msg) {

        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {
        String date = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);

        dateFilterDialog = new MaterialDialog.Builder(context)
                .title(date)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))
                .build();

    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                if (traceDate == startDateTextView) {
                    customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                    startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                    startDate = customStartDate;
                } else {
                    customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                    endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                    endDate = customEndDate;
                }
            }
        }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));


    }

    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        //            rvAdapterForPieChart = new RVAdapterForPieChart(reportItemList);
        //            recyclerView.setAdapter(rvAdapterForPieChart);
        mRVAdapterForSalePurchaseItemReport = new RVAdapterForSalePurchaseItemReport(reportItemList);
        recyclerView.setAdapter(mRVAdapterForSalePurchaseItemReport);

    }

    private void setDatesAndLimits() {
        startDate = endDate = DateUtility.getTodayDate();
        startLimit = 0;
        endLimit = 10;
    }

    private void clickListeners() {
        ((RVAdapterForCategoryMD) rvAdapterForCategoryMD).setmItemClickListener(new RVAdapterForCategoryMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                categoryFilterDialog.dismiss();
                type = categoryFilterList.get(position).getName();
                categoryFilterView.setText(type);
                if (position != 0) {
                    isAll = false;
                    categoryID = "= " + categoryList.get(position - 1).getId().toString();
                } else {
                    isAll = true;
                    categoryID = "<> 0";
                }
                scrollListener();
                new LoadProgressDialog().execute("");
            }
        });

        scrollListener();
        categoryFilterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryFilterDialog.show();
            }
        });

        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                dateFilterDialog.dismiss();
                if (filterList.get(position).equalsIgnoreCase(today)) {
                    startDate = endDate = DateUtility.getTodayDate();
                    new LoadProgressDialog().execute("");
                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(yesterday)) {
                    startDate = endDate = DateUtility.getYesterDayDate();
                    new LoadProgressDialog().execute("");
                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(thisWeek)) {
                    startDate = DateUtility.getStartDateOfWeekString();
                    endDate = DateUtility.getEndDateOfWeekString();
                    new LoadProgressDialog().execute("");
                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastWeek)) {
                    startDate = DateUtility.getStartDateOfLastWeekString();
                    endDate = DateUtility.getEndDateOfLastWeekString();
                    new LoadProgressDialog().execute("");
                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(thisMonth)) {
                    startDate = DateUtility.getThisMonthStartDate();
                    endDate = DateUtility.getThisMonthEndDate();
                    new LoadProgressDialog().execute("");
                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {
                    startDate = DateUtility.getLastMonthStartDate();
                    endDate = DateUtility.getLastMonthEndDate();
                    new LoadProgressDialog().execute("");
                    setDateFilterTextView(filterList.get(position));
                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {
                    customRangeDialog.show();

                }
                scrollListener();

            }
        });

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String startDate = customStartDate;
                String endDate   = customEndDate;

                dateFilterTextView.setText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));

                new LoadProgressDialog().execute("");

                customRangeDialog.dismiss();
            }
        });

        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRangeDialog.dismiss();
            }
        });
    }

    private void buildCategoryChooserDialog() {

        rvAdapterForCategoryMD = new RVAdapterForCategoryMD(categoryFilterList);

        String filter = ThemeUtil.getString(context, R.attr.filter_by_category);

        categoryFilterDialog = new MaterialDialog.Builder(context)
                .title(filter)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForCategoryMD, new LinearLayoutManager(context))
                .build();
    }

    private void scrollListener() {
        recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {
                if (shouldLoad) {
                    mRVAdapterForSalePurchaseItemReport.setShowLoader(true);
                    loadmore();
                }

            }
        });
    }

    public void loadmore() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                reportItemList.addAll(loadMore(reportItemList.size(), 9));
                mRVAdapterForSalePurchaseItemReport.setShowLoader(false);
                mRVAdapterForSalePurchaseItemReport.notifyItemRangeChanged(reportItemList.size() - 1, 10);

            }
        }, 500);
    }

    public List<ReportItem> loadMore(int startLimit, int endLimit) {
        if (reportType.equalsIgnoreCase("sale")) {
            return reportManager.allSales(startDate, endDate, startLimit, endLimit, categoryID);
        } else {
            return reportManager.allPurchases(startDate, endDate, startLimit, endLimit, categoryID);
        }
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        Spinner spinner = new Spinner(context);

        @Override
        protected String doInBackground(String... params) {
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.show();
        }

        @Override
        protected void onPostExecute(String a) {
            configRecyclerView();
            if (!reportItemList.isEmpty()) {
                totalValueView.setText(POSUtil.NumberFormat(totalAmt));
            } else {
                mRVAdapterForSalePurchaseItemReport.notifyDataSetChanged();
                totalValueView.setText("0");
            }

            spinner.dismiss();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.menu_print_export, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_print:
                preparePrint();
                break;
            case R.id.action_export:
                FolderChooserDialog folderChooserDialog =new FolderChooserDialog();
                folderChooserDialog.setmCallback(new FolderChooserDialog.FolderSelectionCallback() {
                    @Override
                    public void onFolderSelection(File folder) {
                        new ExportTask().execute(folder.getAbsolutePath());
                    }
                });
                GrantPermission grantPermission = new GrantPermission(this);
                if (grantPermission.requestPermissions()) {
                    //isImport=true;
                    folderChooserDialog.show(getActivity());
                }
                break;
        }
        return true;
    }

    private void preparePrint() {
        String itemName = ThemeUtil.getString(context, R.attr.name  );
        String itemQty  = ThemeUtil.getString(context, R.attr.qty   );
        String amount   = ThemeUtil.getString(context, R.attr.amount);
        String total    = ThemeUtil.getString(context, R.attr.total);

        bluetoothUtil = BluetoothUtil.getInstance(this);
        final Spinner spinner = new Spinner(context);

        if (!bluetoothUtil.isConnected() && !POSUtil.isInternalPrinter(context)) {
            Toast.makeText(context, "Not connected to printer", Toast.LENGTH_SHORT).show();
            bluetoothUtil.setBluetoothConnectionListener(new BluetoothUtil.JobListener() {

                @Override
                public void onPrepare() {
                    spinner.show();
                    Log.e("Debugging", "P");
                }

                @Override
                public void onSuccess() {
                    spinner.dismiss();

                    if (bluetoothUtil.isConnected()) {

                        String connected = ThemeUtil.getString(context, R.attr.connected);
                        FusionToast.toast(context, FusionToast.TOAST_TYPE.SUCCESS, connected);

                    } else {
                        String notConnected = ThemeUtil.getString(context, R.attr.not_connected);
                        FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR, notConnected);
                    }
                }

                @Override
                public void onFailure() {
                    spinner.dismiss();

                    String notConnected = ThemeUtil.getString(context, R.attr.not_connected);
                    FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR, notConnected);
                }

                @Override
                public void onDisconnected() {
                    Log.e("Debugging", "D");
                    String notConnected = ThemeUtil.getString(context, R.attr.not_connected);
                    FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR, notConnected);
                }
            });
            bluetoothUtil.createConnection(false);

        } else {

            Typeface mTypeface = Typeface.createFromAsset(context.getApplicationContext()
                    .getAssets(), String.format("fonts/%s", "Zawgyi-One.ttf"));
            PrintCanvas printCanvas;

            // Paper type
            if (POSUtil.is80MMPrinter(getContext())) {
                printCanvas = new PrintCanvas(PrintCanvas.PAPERSIZE._80MM, mTypeface, 30);
            } else {
                printCanvas = new PrintCanvas(PrintCanvas.PAPERSIZE._58MM, mTypeface, 20);
            }

            // start from here
            PrintCanvas.Row    row    = printCanvas.new Row();
            PrintCanvas.Column column = printCanvas.new Column(100, pageTitle, PrintCanvas.ALIGN.CENTER);
            row.addColumn(column);
            printCanvas.writeRow(row);
            printCanvas.nextLine();

            // if start date and end date is the same, leave out the end date
            row    = printCanvas.new Row();
            column = printCanvas.new Column(100,
                    DateUtility.makeDateFormatWithSlash(startDate) + (endDate.equalsIgnoreCase(startDate) ? "" : " - " + DateUtility.makeDateFormatWithSlash(endDate)),
                    PrintCanvas.ALIGN.RIGHT);
            row.addColumn(column);
            printCanvas.writeRow(row);
            printCanvas.writeLine();

            // name
            row    = printCanvas.new Row();
            column = printCanvas.new Column(40, itemName, PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            // qty
            column = printCanvas.new Column(43, itemQty, PrintCanvas.ALIGN.CENTER);
            row.addColumn(column);

            // amount
            column = printCanvas.new Column(29, amount, PrintCanvas.ALIGN.CENTER);
            row.addColumn(column);
            printCanvas.writeRow(row);
            printCanvas.writeLine();

            for (ReportItem item : reportItemList) {
                row = printCanvas.new Row();
                column = printCanvas.new Column(45, item.getName(), PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);

                column = printCanvas.new Column(32, String.valueOf(item.getQty()), PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);

                column = printCanvas.new Column(30,  POSUtil.doubleToString(item.getAmount()), PrintCanvas.ALIGN.RIGHT);
                row.addColumn(column);
                printCanvas.writeRow(row);
            }

            printCanvas.nextLine();
            printCanvas.writeLine();

            row = printCanvas.new Row();
            column = printCanvas.new Column(100, total + " : " + POSUtil.NumberFormat(totalAmt) + AppConstant.CURRENCY, PrintCanvas.ALIGN.CENTER);
            row.addColumn(column);
            printCanvas.writeRow(row);

            //printCanvas.drawWaterMarkDiagonal();

            Bitmap bmpMonochrome = Bitmap.createBitmap(printCanvas.getBitmap().getWidth(), printCanvas.getBitmap().getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas1       = new Canvas(bmpMonochrome);
            Paint  paint1        = new Paint();

            canvas1.drawColor(Color.WHITE);
            canvas1.drawBitmap(printCanvas.getBitmap(), 0, 0, paint1);

            PrintImage printImage = new PrintImage(bmpMonochrome);

            try {

                if (POSUtil.isInternalPrinter(getContext())) {

                    AidlUtil.getInstance().initPrinter();
//                    // AidlUtil.getInstance().printText(printViewSlip.getPrintString(), 24, false, false,"Type monospace");
                    AidlUtil.getInstance().printBitmap(printCanvas.getBitmap());
                } else {
                        bluetoothUtil.getOutputStream().write(printImage.getPrintImageData());
                        bluetoothUtil.getOutputStream().write(CUT_PAPER);
                }

                ;
                //
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        bluetoothUtil.onActivityResultBluetooth(requestCode, resultCode, data);
    }

    @SuppressLint("StaticFieldLeak")
    class ExportTask extends AsyncTask<String, Void, Void> {
        Spinner spinner = new Spinner(context);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            String itemName = ThemeUtil.getString(context, R.attr.name  );
            String itemQty  = ThemeUtil.getString(context, R.attr.qty   );
            String amount   = ThemeUtil.getString(context, R.attr.amount);
            String total    = ThemeUtil.getString(context, R.attr.total);
            final String[] titles = {itemName, itemQty, amount, total};
            final String path = params[0];
            ExportImportUtil.exportSalePurchaseItemReport(reportType.equalsIgnoreCase("sale") ? "SaleItemReport" : "PurchaseItemReport",
                    titles, reportItemList, totalAmt, startDate, endDate, path);
            POSUtil.scanMedia(path, context);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            spinner.dismiss();
        }
    }
}
