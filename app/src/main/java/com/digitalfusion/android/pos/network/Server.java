package com.digitalfusion.android.pos.network;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.AuthorizationManager;
import com.digitalfusion.android.pos.database.business.CustomerManager;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.database.model.AccessLog;
import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.database.model.Device;
import com.digitalfusion.android.pos.database.model.Discount;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.database.model.SalesHeader;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.database.model.Stock;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.database.model.Tax;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.fragments.settingfragment.controlserver.ServerTabsFragment;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.koushikdutta.async.AsyncServer;
import com.koushikdutta.async.http.body.JSONObjectBody;
import com.koushikdutta.async.http.body.UrlEncodedFormBody;
import com.koushikdutta.async.http.server.AsyncHttpServer;
import com.koushikdutta.async.http.server.AsyncHttpServerRequest;
import com.koushikdutta.async.http.server.AsyncHttpServerResponse;
import com.koushikdutta.async.http.server.HttpServerRequestCallback;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by lyx on 2/26/18.
 */
public class Server {
    private AsyncHttpServer server = new AsyncHttpServer();
    private AsyncServer asyncServer = new AsyncServer();
    private static Server instance = new Server();

    public static Server getInstance() {
        return instance;
    }

    private Server() {
    }

    public void startServer(final Context context) {
        final AuthorizationManager authorizationManager = new AuthorizationManager(context);
        final AccessLogManager     accessLogManager     = new AccessLogManager(context);
        server.get("/", (request, response) -> {
            response.code(AppConstant.RESPONSE_ALLOWED);
            response.end();
        });

        server.post("/connect", (request, response) -> {
            GsonBuilder builder      = new GsonBuilder();
            Gson        gson         = builder.create();
            Device      device       = gson.fromJson(request.getBody().get().toString(), Device.class);
            int         responseCode = authorizationManager.generateResponseCode(device.getSerialNo());

            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                } else {
                    authorizationManager.insertDevice(device);
                    Intent intent = new Intent(ServerTabsFragment.ACTION_DEVICE_REQUEST);
                    context.sendBroadcast(intent);

                    responseCode = AppConstant.RESPONSE_ALLOWED;
                }
            }

            response.code(responseCode);
            response.end();
        });

        //        server.post("/getRequiredData", new HttpServerRequestCallback() {
        //            @Override
        //            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
        //                GsonBuilder builder    = new GsonBuilder();
        //                Gson        gson       = builder.create();
        //                Device      device     = gson.fromJson(request.getBody().get().toString(), Device.class);
        //                String      permission = authorization.generateResponseCode(device.getSerialNo());
        //
        //                if (permission == null) {
        //                    if (authorization.isDeviceLimitReached()) {
        //                        response.send("limit_reached");
        //                    } else {
        //                        authorization.insertDevice(device);
        //                        Intent intent = new Intent(ControlServerFragment.ACTION_DEVICE_REQUEST);
        //                        context.sendBroadcast(intent);
        //
        //                        // default is allowed
        //                        permission = AppConstant.PERMISSION_ALLOWED;
        //                    }
        //                }
        //
        //                if (permission.equalsIgnoreCase(AppConstant.PERMISSION_BLOCKED)) {
        //                    response.code(403);
        //                    response.end();
        //                } else if (permission.equalsIgnoreCase(AppConstant.PERMISSION_ALLOWED)) {
        //                    DataWrapper      dataWrapper      = new DataWrapper();
        //                    UnitManager     unitBusiness     = new UnitManager(context);
        //                    SettingManager  settingBusiness  = new SettingManager(context);
        //                    CustomerManager customerBusiness = new CustomerManager(context);
        //                    ApiDAO           apiDAO           = ApiDAO.getApiDAOInstance(context);
        //
        //                    dataWrapper.setUnitList(unitBusiness.getAllUnits());
        //                    dataWrapper.setTaxList(settingBusiness.getAllTaxs());
        //                    dataWrapper.setDiscountList(settingBusiness.getAllDiscounts());
        //                    dataWrapper.setCustomerList(customerBusiness.getAllCustomers());
        //                    dataWrapper.setUserList(apiDAO.getAllUserRoles());
        //
        //                    String json = gson.toJson(dataWrapper);
        //                    response.send(json);
        //                } else {
        //                    response.end();
        //                }
        //            }
        //        });

        //region LoginApi
        server.post("/getUserList", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, final AsyncHttpServerResponse response) {
                UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
                String             deviceSerialNo = requestData.get().getString("deviceSerialNo");
                int                responseCode   = authorizationManager.generateResponseCode(deviceSerialNo);
                List<User>         userList;

                if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                    if (authorizationManager.isDeviceLimitReached()) {
                        responseCode = AppConstant.RESPONSE_FULL;
                    }
                } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                    GsonBuilder builder = new GsonBuilder();
                    Gson        gson    = builder.create();
                    userList = ApiDAO.getApiDAOInstance(context).getAllUserRoles();
                    String jsonList = gson.toJson(userList);

                    response.code(responseCode);
                    response.send(jsonList);
                }

                response.code(responseCode);
                response.end();
            }
        });

        server.post("/isUserLoggedIn", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
                Long               userId         = Long.parseLong(requestData.get().getString("userId"));
                String             deivceSerialNo = requestData.get().getString("deviceSerialNo");

                int responseCode = authorizationManager.generateResponseCode(deivceSerialNo);
                if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                    if (authorizationManager.isDeviceLimitReached()) {
                        responseCode = AppConstant.RESPONSE_FULL;
                    }
                } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                    response.code(responseCode);
                    response.send(accessLogManager.isUserLoggedIn(userId).toString());
                }

                response.code(responseCode);
                response.end();
            }
        });

        server.post("/logIn", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
                Long               userId         = Long.parseLong(requestData.get().getString("userId"));
                String             deivceSerialNo = requestData.get().getString("deviceSerialNo");

                int responseCode = authorizationManager.generateResponseCode(deivceSerialNo);
                if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                    if (authorizationManager.isDeviceLimitReached()) {
                        responseCode = AppConstant.RESPONSE_FULL;
                    }
                } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                    Long      deviceId  = authorizationManager.getDeviceId(deivceSerialNo);
                    AccessLog accessLog = new AccessLog();
                    accessLog.setUserId(userId);
                    accessLog.setDeviceId(deviceId);
                    accessLog.setDatetime(DateUtility.getCurrentDateTime());
                    accessLog.setEvent(AppConstant.EVENT_IN);

                    accessLogManager.insertAccessLog(accessLog);
                }

                response.code(responseCode);
                response.end();
            }
        });

        server.post("/logOut", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
                Long               userId         = Long.parseLong(requestData.get().getString("userId"));
                String             deivceSerialNo = requestData.get().getString("deviceSerialNo");

                int responseCode = authorizationManager.generateResponseCode(deivceSerialNo);
                if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                    if (authorizationManager.isDeviceLimitReached()) {
                        responseCode = AppConstant.RESPONSE_FULL;
                    }
                } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                    Long      deviceId  = authorizationManager.getDeviceId(deivceSerialNo);
                    AccessLog accessLog = new AccessLog();
                    accessLog.setUserId(userId);
                    accessLog.setDeviceId(deviceId);
                    accessLog.setDatetime(DateUtility.getCurrentDateTime());
                    accessLog.setEvent(AppConstant.EVENT_OUT);

                    accessLogManager.insertAccessLog(accessLog);
                }

                response.code(responseCode);
                response.end();
            }
        });

        server.post("/getCurrentlyLoggedInUser", new HttpServerRequestCallback() {
            @Override
            public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
                String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

                int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
                if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                    if (authorizationManager.isDeviceLimitReached()) {
                        responseCode = AppConstant.RESPONSE_FULL;
                    }
                } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                    AccessLogManager     accessLogManager     = new AccessLogManager(context);
                    AuthorizationManager authorizationManager = new AuthorizationManager(context);
                    Long                 deviceId             = authorizationManager.getDeviceId(Build.SERIAL);
                    User                 currentUser          = accessLogManager.getCurrentlyLoggedInUser(deviceId);

                    GsonBuilder builder = new GsonBuilder();
                    Gson        gson    = builder.create();

                    String jsonList = gson.toJson(currentUser);
                    response.send(jsonList);
                }

                response.code(responseCode);
                response.end();
            }
        });

        server.post("/isUserLoggedInWithDevice", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");
            Long               userId = Long.parseLong(requestData.get().getString("userId"));

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                Long                 deviceId              = authorizationManager.getDeviceId(deviceSerialNo);

                response.send(accessLogManager.isUserLoggedInWithDevice(deviceId, userId).toString());
            }

            response.code(responseCode);
            response.end();
        });
        //endregion

        //region AddEditSaleApi
        server.post("/addSale", (request, response) -> {
            String deviceSerialNo = request.getHeaders().get("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder builder            = new GsonBuilder();
                Gson        gson               = builder.create();
                JSONObject  json               = ((JSONObjectBody) request.getBody()).get();
                SalesHeader salesHeader        = gson.fromJson(json.toString(), SalesHeader.class);
                SalesHeader reponseSalesHeader = new SalesHeader();

                SalesManager salesManager = new SalesManager(context);
                if (salesHeader != null) {

                    // These are unnecessary. Remove them.
                    reponseSalesHeader = salesManager.addNewSalesRest(salesHeader.getCustomerName(), salesHeader.getCustomerPhone(), salesHeader.getTotal(), salesHeader.getDiscount(),
                            salesHeader.getTaxID(), salesHeader.getSubtotal(), salesHeader.getType().toString(),
                            salesHeader.getDiscountAmt(), salesHeader.getRemark(), salesHeader.getPaidAmt(), salesHeader.getBalance(),
                            salesHeader.getTaxAmt(), salesHeader.getTaxRate(), Integer.toString(Integer.parseInt(salesHeader.getDay())),
                            Integer.toString(Integer.parseInt(salesHeader.getMonth())), Integer.toString(Integer.parseInt(salesHeader.getYear())),
                            salesHeader.getSalesAndPurchaseItemList(), null, null, salesHeader.getTaxType(), salesHeader.getDiscountID(), salesHeader.getUserId(),salesHeader.getChangeBalance());
                }
                String jsonString = gson.toJson(reponseSalesHeader);
                response.send(jsonString);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/makeHoldToSale", (request, response) -> {
            String deviceSerialNo = request.getHeaders().get("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder            = new GsonBuilder();
                Gson         gson               = builder.create();
                JSONObject   json               = ((JSONObjectBody) request.getBody()).get();
                SalesHeader  salesHeader        = gson.fromJson(json.toString(), SalesHeader.class);
                SalesHeader  reponseSalesHeader = new SalesHeader();
                SalesManager salesManager       = new SalesManager(context);

                if (salesHeader != null) {
                    reponseSalesHeader = salesManager.addHeldSalesRest(salesHeader.getSaleVocherNo(), salesHeader.getCustomerName(), salesHeader.getCustomerPhone(), salesHeader.getTotal(),
                            salesHeader.getDiscount(), salesHeader.getTaxID(), salesHeader.getSubtotal(),
                            salesHeader.getType().toString(), salesHeader.getDiscountAmt(), salesHeader.getRemark(), salesHeader.getPaidAmt(),
                            salesHeader.getBalance(), salesHeader.getTaxAmt(), salesHeader.getTaxRate(), Integer.toString(Integer.parseInt(salesHeader.getDay())),
                            Integer.toString(Integer.parseInt(salesHeader.getMonth())), Integer.toString(Integer.parseInt(salesHeader.getYear())),
                            salesHeader.getSalesAndPurchaseItemList(), null, null, salesHeader.getTaxType(), salesHeader.getDiscountID(), salesHeader.getUserId(),salesHeader.getChangeBalance());
                }
                salesManager.deleteHoldSales(salesHeader.getId());
                String jsonString = gson.toJson(reponseSalesHeader);
                response.send(jsonString);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/addHold", (request, response) -> {
            String deviceSerialNo = request.getHeaders().get("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder            = new GsonBuilder();
                Gson         gson               = builder.create();
                JSONObject   json               = ((JSONObjectBody) request.getBody()).get();
                SalesHeader  salesHeader        = gson.fromJson(json.toString(), SalesHeader.class);
                SalesHeader  reponseSalesHeader = new SalesHeader();
                SalesManager salesManager       = new SalesManager(context);

                if (salesHeader != null) {
                    reponseSalesHeader = salesManager.holdNewSales(salesHeader.getCustomerName(), salesHeader.getCustomerPhone(), salesHeader.getTotal(), salesHeader.getDiscount(),
                            salesHeader.getTaxID(), salesHeader.getSubtotal(), salesHeader.getType().toString(),
                            salesHeader.getDiscountAmt(), salesHeader.getRemark(), salesHeader.getPaidAmt(), salesHeader.getBalance(),
                            salesHeader.getTaxAmt(), salesHeader.getTaxRate(), Integer.toString(Integer.parseInt(salesHeader.getDay())),
                            Integer.toString(Integer.parseInt(salesHeader.getMonth())), Integer.toString(Integer.parseInt(salesHeader.getYear())),
                            salesHeader.getSalesAndPurchaseItemList(), null, null, salesHeader.getTaxType(), salesHeader.getDiscountID(), salesHeader.getUserId());
                }
                String jsonString = gson.toJson(reponseSalesHeader);
                response.send(jsonString);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/updateHold", (request, response) -> {
            String deviceSerialNo = request.getHeaders().get("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder            = new GsonBuilder();
                Gson         gson               = builder.create();
                JSONObject   json               = ((JSONObjectBody) request.getBody()).get();
                SalesHeader  salesHeader        = gson.fromJson(json.toString(), SalesHeader.class);
                SalesHeader  reponseSalesHeader = new SalesHeader();
                SalesManager salesManager       = new SalesManager(context);
                SalesHeader  result             = new SalesHeader();

                if (salesHeader != null) {
                    result = salesManager.updateHoldSalesBySalesIDRest(salesHeader.getId(), salesHeader.getSaleDate(), salesHeader.getCustomerName(), salesHeader.getCustomerPhone(), salesHeader.getTotal(),
                            salesHeader.getDiscount(),
                            salesHeader.getTaxID(), salesHeader.getSubtotal(), salesHeader.getDiscountAmt(), salesHeader.getRemark(), salesHeader.getPaidAmt(), salesHeader.getBalance(), salesHeader.getTaxAmt(),
                            Integer.toString(Integer.parseInt(salesHeader.getDay())), Integer.toString(Integer.parseInt(salesHeader.getMonth())), Integer.toString(Integer.parseInt(salesHeader.getYear())), salesHeader.getType().toString(),
                            salesHeader.getTaxRate(), salesHeader.getUpdateList(), salesHeader.getDeleteList(), null, null, salesHeader.getTaxType(), salesHeader.getDiscountID());
                }
                String jsonString = gson.toJson(result);
                response.send(jsonString);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getStockItem", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder      = new GsonBuilder();
                Gson         gson         = builder.create();
                StockManager stockManager = new StockManager(context);
                String       stockId      = requestData.get().getString("stockId");
                Stock        stock        = stockManager.findStockByID(Long.valueOf(stockId));

                String jsonList = gson.toJson(stock);
                response.send(jsonList);
            }

            response.code(responseCode);
            response.end();
        });

        //endregion

        //region SaleHistoryApi
        server.post("/getSaleHistoryViewById", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            Long               id             = Long.parseLong(requestData.get().getString("id"));
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder      = new GsonBuilder();
                Gson         gson         = builder.create();
                SalesManager salesManager = new SalesManager(context);
                SalesHistory salesHistory = salesManager.getSaleHistoryViewById(id);

                String jsonList = gson.toJson(salesHistory);
                response.send(jsonList);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getTransactionHistory", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            int                startLimit     = Integer.parseInt(requestData.get().getString("startLimit"));
            int                endLimit       = Integer.parseInt(requestData.get().getString("endLimit"));
            String             startDate      = requestData.get().getString("startDate");
            String             endDate        = requestData.get().getString("endDate");
            String             userId         = requestData.get().getString("userId");
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder      = new GsonBuilder();
                Gson         gson         = builder.create();
                SalesManager salesManager = new SalesManager(context);

                List<SalesHistory> salesHistories = salesManager.getAllSaleTransactions(startLimit, endLimit, startDate, endDate, Long.parseLong(userId));

                String jsonList = gson.toJson(salesHistories);
                response.send(jsonList);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getHolds", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            int                startLimit     = Integer.parseInt(requestData.get().getString("startLimit"));
            int                endLimit       = Integer.parseInt(requestData.get().getString("endLimit"));
            String             startDate      = requestData.get().getString("startDate");
            String             endDate        = requestData.get().getString("endDate");
            String             userId         = requestData.get().getString("userId");
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder      = new GsonBuilder();
                Gson         gson         = builder.create();
                SalesManager salesManager = new SalesManager(context);

                List<SalesHistory> salesHistories = salesManager.getAllSalesHold(startLimit, endLimit, startDate, endDate, new InsertedBooleanHolder(), userId);

                String jsonList = gson.toJson(salesHistories);
                response.send(jsonList);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getSalesHeaderView", (request, response) -> {

            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            Long               id             = Long.parseLong(requestData.get().getString("id"));
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder      = new GsonBuilder();
                Gson         gson         = builder.create();
                SalesManager salesManager = new SalesManager(context);

                SalesHeader salesHeader = salesManager.getSalesHeaderView(id);

                String json = gson.toJson(salesHeader);
                response.send(json);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getSaleDetailsBySalesID", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            Long               id             = Long.parseLong(requestData.get().getString("id"));
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder      = new GsonBuilder();
                Gson         gson         = builder.create();
                SalesManager salesManager = new SalesManager(context);

                List<SalesAndPurchaseItem> list = salesManager.getSaleDetailsBySalesID(id);

                String json = gson.toJson(list);
                response.send(json);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getSaleDetailsHoldBySalesID", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            Long               id             = Long.parseLong(requestData.get().getString("id"));
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder      = new GsonBuilder();
                Gson         gson         = builder.create();
                SalesManager salesManager = new SalesManager(context);

                List<SalesAndPurchaseItem> list = salesManager.getSaleDetailsHoldBySalesID(id);

                String json = gson.toJson(list);
                response.send(json);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getHoldSalesHeader", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            Long               id             = Long.parseLong(requestData.get().getString("id"));
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder      = new GsonBuilder();
                Gson         gson         = builder.create();
                SalesManager salesManager = new SalesManager(context);

                SalesHeader salesHeader = salesManager.getHoldSalesHeaderView(id);

                String json = gson.toJson(salesHeader);
                response.send(json);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getSaleId", (request, response) -> {

            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            String             invoiceNo      = requestData.get().getString("invoiceNo");
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                SalesManager salesManager = new SalesManager(context);
                Long         id           = salesManager.findIDBySalesInvoice(invoiceNo);

                response.send(id.toString());
            }

            response.code(responseCode);
            response.end();
        });
        //endregion

        //region AutoCompleteApi
        server.post("/getCustomer", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            int                startLimit     = Integer.parseInt(requestData.get().getString("startLimit"));
            int                endLimit       = Integer.parseInt(requestData.get().getString("endLimit"));
            String             constraint     = requestData.get().getString("constraint");
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder     builder         = new GsonBuilder();
                Gson            gson            = builder.create();
                CustomerManager customerManager = new CustomerManager(context);
                List<Customer>  customerList    = customerManager.getAllCustomersByNameOnSearch(startLimit, endLimit, constraint);
                String          jsonList        = gson.toJson(customerList);

                response.send(jsonList);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getStocks", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            int                startLimit     = Integer.parseInt(requestData.get().getString("startLimit"));
            int                endLimit       = Integer.parseInt(requestData.get().getString("endLimit"));
            String             query          = requestData.get().getString("query");
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder     builder      = new GsonBuilder();
                Gson            gson         = builder.create();
                StockManager    stockManager = new StockManager(context);
                List<StockItem> stockList    = stockManager.getActiveStocksByNameOrCode(startLimit, endLimit, query);

                String jsonList = gson.toJson(stockList);
                response.send(jsonList);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getTransactionWithVoucherNoOrCustomer", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            int                startLimit     = Integer.parseInt(requestData.get().getString("startLimit"));
            int                endLimit       = Integer.parseInt(requestData.get().getString("endLimit"));
            String             constraint     = requestData.get().getString("constraint");
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder      = new GsonBuilder();
                Gson         gson         = builder.create();
                SalesManager salesManager = new SalesManager(context);

                List<SalesHistory> salesHistories = salesManager.getSalesOnSearchWithVoucherNoOrCustomer(startLimit, endLimit, constraint);

                String jsonList = gson.toJson(salesHistories);
                response.send(jsonList);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getHoldsWithVoucherNoOrCustomer", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            int                startLimit     = Integer.parseInt(requestData.get().getString("startLimit"));
            int                endLimit       = Integer.parseInt(requestData.get().getString("endLimit"));
            String             userId         = requestData.get().getString("userId");
            String             constraint     = requestData.get().getString("constraint");
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder      = new GsonBuilder();
                Gson         gson         = builder.create();
                SalesManager salesManager = new SalesManager(context);

                List<SalesHistory> salesHistories = salesManager.getHoldSalesWithVoucherNoOrCustomer(startLimit, endLimit, constraint, userId);

                String jsonList = gson.toJson(salesHistories);
                response.send(jsonList);
            }

            response.code(responseCode);
            response.end();
        });
        //endregion

        //region SettingsApi
        server.post("/getAllDiscounts", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder    builder      = new GsonBuilder();
                Gson           gson         = builder.create();
                SettingManager settingManager = new SettingManager(context);

                List<Discount> discountList = settingManager.getAllDiscounts();

                String jsonList = gson.toJson(discountList);
                response.send(jsonList);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getDefaultDiscount", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder      = new GsonBuilder();
                Gson         gson         = builder.create();
                SettingManager settingManager = new SettingManager(context);
                Discount        discount        = settingManager.getDefaultDiscount();

                String jsonList = gson.toJson(discount);
                response.send(jsonList);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getDefaultTax", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder  builder      = new GsonBuilder();
                Gson         gson         = builder.create();
                SettingManager settingManager = new SettingManager(context);
                Tax        tax        = settingManager.getDefaultTax();

                String jsonList = gson.toJson(tax);
                response.send(jsonList);
            }

            response.code(responseCode);
            response.end();
        });

        server.post("/getAllTaxes", (request, response) -> {
            UrlEncodedFormBody requestData    = (UrlEncodedFormBody) request.getBody();
            String             deviceSerialNo = requestData.get().getString("deviceSerialNo");

            int responseCode = authorizationManager.generateResponseCode(deviceSerialNo);
            if (responseCode == AppConstant.RESPONSE_NOT_FOUND) {
                if (authorizationManager.isDeviceLimitReached()) {
                    responseCode = AppConstant.RESPONSE_FULL;
                }
            } else if (responseCode == AppConstant.RESPONSE_ALLOWED) {
                GsonBuilder    builder      = new GsonBuilder();
                Gson           gson         = builder.create();
                SettingManager settingManager = new SettingManager(context);

                List<Tax> taxList = settingManager.getAllTaxs();

                String jsonList = gson.toJson(taxList);
                response.send(jsonList);
            }

            response.code(responseCode);
            response.end();
        });
        //endregion

        server.listen(asyncServer, 8668);
    }

    public void stopServer() {
        server.stop();
        asyncServer.stop();
    }

    public boolean isServerRunning() {
        return asyncServer.isRunning();
    }
}
