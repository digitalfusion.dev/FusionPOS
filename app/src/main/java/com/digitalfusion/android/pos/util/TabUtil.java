package com.digitalfusion.android.pos.util;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;

/**
 * Created by MD001 on 1/2/17.
 */

public class TabUtil {

    public static View renderTabView(Context context, int titleResource) {
        FrameLayout view = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.tab_with_badge_view, null);
        // We need to manually set the LayoutParams here because we don't have a view root
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ((TextView) view.findViewById(R.id.tab_text)).setText(titleResource);
        //view.findViewById(R.id.tab_text).setBackgroundResource(backgroundResource);
        return view;
    }

    public static View renderTabView(Context context, String titleResource) {
        FrameLayout view = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.tab_with_badge_view, null);
        // We need to manually set the LayoutParams here because we don't have a view root
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ((TextView) view.findViewById(R.id.tab_text)).setText(titleResource);
        //view.findViewById(R.id.tab_text).setBackgroundResource(backgroundResource);
        return view;
    }

    public static View renderTabView(Context context, int titleResource, int badgeNumber) {
        FrameLayout view = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.tab_with_badge_view2, null);
        // We need to manually set the LayoutParams here because we don't have a view root
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ((TextView) view.findViewById(R.id.tab_text)).setText(titleResource);
        //view.findViewById(R.id.tab_text).setBackgroundResource(backgroundResource);
        updateTabBadge((TextView) view.findViewById(R.id.tab_badge), badgeNumber);
        return view;
    }

    public static View renderTabView(Context context, String titleResource, int badgeNumber) {
        FrameLayout view = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.tab_with_badge_view2, null);
        // We need to manually set the LayoutParams here because we don't have a view root
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ((TextView) view.findViewById(R.id.tab_text)).setText(titleResource);
        //view.findViewById(R.id.tab_text).setBackgroundResource(backgroundResource);
        updateTabBadge((TextView) view.findViewById(R.id.tab_badge), badgeNumber);
        return view;
    }

    public static void updateTabBadge(TabLayout.Tab tab, int badgeNumber) {
        updateTabBadge((TextView) tab.getCustomView().findViewById(R.id.tab_badge), badgeNumber);
    }

    private static void updateTabBadge(TextView view, int badgeNumber) {
        if (badgeNumber > 0) {
            view.setVisibility(View.VISIBLE);
            view.setText(Integer.toString(badgeNumber));
        } else {
            view.setVisibility(View.GONE);
        }
    }
}
