package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD003 on 9/7/16.
 */
public class RVAdapterForStockItemInSaleAndPurchaseDetail extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SalesAndPurchaseItem> saleItemViewInSaleList;
    private ClickListener editClickListener;
    private ClickListener deleteClickListener;


    public RVAdapterForStockItemInSaleAndPurchaseDetail(List<SalesAndPurchaseItem> saleItemViewInSaleList) {
        this.saleItemViewInSaleList = saleItemViewInSaleList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_item_view_in_sale_and_purchase_detail, parent, false);

        return new SaleItemViewInSale(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof SaleItemViewInSale) {

            SaleItemViewInSale saleItemViewInSale = (SaleItemViewInSale) holder;

            POSUtil.makeZebraStrip(saleItemViewInSale.itemView, position);

            saleItemViewInSale.noTextView.setText(Integer.toString(position + 1));
            saleItemViewInSale.invoiceNoTextView.setText(saleItemViewInSaleList.get(position).getItemName());
            saleItemViewInSale.qtyPerPriceTextView.setText(saleItemViewInSaleList.get(position).getQty() + " x " + POSUtil.NumberFormat(saleItemViewInSaleList.get(position).getPrice()));
            saleItemViewInSale.totalTexView.setText(POSUtil.NumberFormat(saleItemViewInSaleList.get(position).getTotalPrice()));

            saleItemViewInSale.disLinearLayout.setVisibility(View.GONE);

        }


    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    @Override
    public int getItemCount() {
        return saleItemViewInSaleList.size();
    }

    public List<SalesAndPurchaseItem> getSaleItemViewInSaleList() {
        return saleItemViewInSaleList;
    }

    public void setSaleItemViewInSaleList(List<SalesAndPurchaseItem> saleItemViewInSaleList) {
        this.saleItemViewInSaleList = saleItemViewInSaleList;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public class SaleItemViewInSale extends RecyclerView.ViewHolder {
        TextView noTextView;
        TextView invoiceNoTextView;
        TextView totalTexView;
        TextView itemDiscountTextView;
        TextView qtyPerPriceTextView;
        LinearLayout disLinearLayout;

        View view;

        public SaleItemViewInSale(View itemView) {
            super(itemView);

            this.view = itemView;

            disLinearLayout      = (LinearLayout) itemView.findViewById(R.id.dis_layout                        );
            noTextView           = (TextView    ) itemView.findViewById(R.id.no_in_sale_item_view_tv           );
            invoiceNoTextView    = (TextView    ) itemView.findViewById(R.id.item_name_in_sale_item_view_tv    );
            totalTexView         = (TextView    ) itemView.findViewById(R.id.total_amount_in_sale_item_view_tv );
            itemDiscountTextView = (TextView    ) itemView.findViewById(R.id.item_discount_in_sale_item_view_tv);
            qtyPerPriceTextView  = (TextView    ) itemView.findViewById(R.id.qty_per_price                     );
        }

    }
}
