package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.database.model.Device;
import com.digitalfusion.android.pos.util.AppConstant;

import java.util.List;

/**
 * Created by lyx on 3/22/18.
 */

public class DeviceRVAdapter extends RecyclerSwipeAdapter<DeviceRVAdapter.ViewHolder> {
    Context context;
    List<Device> deviceList;
    ApiDAO apiDAO;

    public DeviceRVAdapter(Context context, List<Device> deviceList) {
        this.context = context;
        this.deviceList = deviceList;
        apiDAO = ApiDAO.getApiDAOInstance(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_device, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Device device = deviceList.get(position);

        holder.nameTextView.setText(device.getDeviceName());
        holder.serialNoTextView.setText(device.getSerialNo());

        changeViewsByPermission(holder, device.getPermission());

        holder.blockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.blockButton.getText().toString().equalsIgnoreCase("Block")) {
                    device.setPermission(AppConstant.PERMISSION_BLOCKED);
                } else {
                    device.setPermission(AppConstant.PERMISSION_ALLOWED);
                }

                apiDAO.updateDevicePermission(device);
                changeViewsByPermission(holder, device.getPermission());
                holder.swipeLayout.close();
            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                apiDAO.deleteDevice(device.getId());
                deviceList.remove(device);
                notifyItemRemoved(holder.getAdapterPosition());
                notifyItemRangeChanged(holder.getAdapterPosition(), getItemCount());
            }
        });

        mItemManger.bindView(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return deviceList.size();
    }

    private void changeViewsByPermission(ViewHolder holder, String permission) {
        if (permission.equalsIgnoreCase(AppConstant.PERMISSION_ALLOWED)) {
            holder.blockButton.setText("Block");
            holder.statusTextView.setBackgroundColor(Color.parseColor("#4CAF50"));
            holder.statusTextView.setText(AppConstant.PERMISSION_ALLOWED);
        } else if (permission.equalsIgnoreCase(AppConstant.PERMISSION_BLOCKED)){
            holder.blockButton.setText("Unblock");
            holder.statusTextView.setBackgroundColor(Color.parseColor("#DD2C00"));
            holder.statusTextView.setText(AppConstant.PERMISSION_BLOCKED);
        }
    }

    public void swapData(List<Device> newDeviceList) {
        if (newDeviceList == null || newDeviceList.size() == 0) {
            return;
        }
        deviceList.clear();
        deviceList.addAll(newDeviceList);
        notifyDataSetChanged();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView;
        TextView statusTextView;
        TextView serialNoTextView;
        LinearLayout buttonContainer;
        Button deleteButton;
        Button blockButton;
        SwipeLayout swipeLayout;

        ViewHolder(final View itemView) {
            super(itemView);

            nameTextView = itemView.findViewById(R.id.name_textview);
            statusTextView = itemView.findViewById(R.id.status_textview);
            serialNoTextView = itemView.findViewById(R.id.serial_no_textview);
            buttonContainer = itemView.findViewById(R.id.button_container);
            deleteButton = itemView.findViewById(R.id.delete_button);
            blockButton = itemView.findViewById(R.id.block_button);
            swipeLayout = itemView.findViewById(R.id.swipe);
        }


    }

}
