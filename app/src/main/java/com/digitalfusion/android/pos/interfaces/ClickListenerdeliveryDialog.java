package com.digitalfusion.android.pos.interfaces;

import android.view.View;

import com.digitalfusion.android.pos.database.model.SaleDelivery;

/**
 * Created by MD003 on 8/30/16.
 */
public interface ClickListenerdeliveryDialog {

    void onClick(View v, SaleDelivery deliveryVO, String customerName);


}
