package com.digitalfusion.android.pos.database;

/**
 * Created by MD003 on 11/23/16.
 */

public class ThemeObj {

    private int ImageResource;

    private String themeName;

    private int ThemeResourceNoActionBar;
    private int ThemeResourceActionBar;

    private int primaryColor;
    private int secondaryColor;

    private int drawableId;

    private String themeNameID;

    public ThemeObj() {
    }

    public ThemeObj(String themeName, int themeResourceNoActionBar, int themeResourceActionBar, String themeNameID) {
        this.themeName = themeName;
        ThemeResourceNoActionBar = themeResourceNoActionBar;
        ThemeResourceActionBar = themeResourceActionBar;
        this.themeNameID = themeNameID;
    }

    public ThemeObj(int imageResource, int themeResourceNoActionBar, int themeResourceActionBar) {
        ImageResource = imageResource;
        ThemeResourceNoActionBar = themeResourceNoActionBar;
        ThemeResourceActionBar = themeResourceActionBar;
    }

    public ThemeObj(String themeName, int themeResourceNoActionBar, int themeResourceActionBar) {
        this.themeName = themeName;
        ThemeResourceNoActionBar = themeResourceNoActionBar;
        ThemeResourceActionBar = themeResourceActionBar;
    }

    public ThemeObj(String themeName, int themeResourceNoActionBar, int themeResourceActionBar, String themeNameID, int primaryColor, int secondaryColor) {
        this.themeName = themeName;
        ThemeResourceNoActionBar = themeResourceNoActionBar;
        ThemeResourceActionBar = themeResourceActionBar;
        this.primaryColor = primaryColor;
        this.secondaryColor = secondaryColor;
        this.themeNameID = themeNameID;
    }

    public ThemeObj(String themeName, int themeResourceNoActionBar, int themeResourceActionBar, String themeNameID, int primaryColor, int secondaryColor, int drawableId) {
        this.themeName = themeName;
        ThemeResourceNoActionBar = themeResourceNoActionBar;
        ThemeResourceActionBar = themeResourceActionBar;
        this.primaryColor = primaryColor;
        this.secondaryColor = secondaryColor;
        this.themeNameID = themeNameID;
        this.drawableId = drawableId;
    }

    public ThemeObj(int imageResource, String themeName, int themeResourceNoActionBar, int themeResourceActionBar, int primaryColor, int secondaryColor, String themeNameID) {
        ImageResource = imageResource;
        this.themeName = themeName;
        ThemeResourceNoActionBar = themeResourceNoActionBar;
        ThemeResourceActionBar = themeResourceActionBar;
        this.primaryColor = primaryColor;
        this.secondaryColor = secondaryColor;
        this.themeNameID = themeNameID;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public int getThemeResourceNoActionBar() {
        return ThemeResourceNoActionBar;
    }

    public void setThemeResourceNoActionBar(int themeResourceNoActionBar) {
        ThemeResourceNoActionBar = themeResourceNoActionBar;
    }

    public int getThemeResourceActionBar() {
        return ThemeResourceActionBar;
    }

    public void setThemeResourceActionBar(int themeResourceActionBar) {
        ThemeResourceActionBar = themeResourceActionBar;
    }

    public int getImageResource() {
        return ImageResource;
    }

    public void setImageResource(int imageResource) {
        ImageResource = imageResource;
    }

    public String getThemeNameID() {
        return themeNameID;
    }

    public void setThemeNameID(String themeNameID) {
        this.themeNameID = themeNameID;
    }

    public int getPrimaryColor() {
        return primaryColor;
    }

    public void setPrimaryColor(int primaryColor) {
        this.primaryColor = primaryColor;
    }

    public int getSecondaryColor() {
        return secondaryColor;
    }

    public void setSecondaryColor(int secondaryColor) {
        this.secondaryColor = secondaryColor;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }
}
