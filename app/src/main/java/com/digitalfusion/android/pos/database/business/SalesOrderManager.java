package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.database.dao.sales.DeliveryDAO;
import com.digitalfusion.android.pos.database.dao.sales.PickupDAO;
import com.digitalfusion.android.pos.database.dao.sales.SalesDAO;
import com.digitalfusion.android.pos.database.model.SaleDeliveryAndPickUp;
import com.digitalfusion.android.pos.database.model.SalesHistory;

import java.util.List;

/**
 * Created by MD002 on 9/7/16.
 */
public class SalesOrderManager {
    private Context context;
    private PickupDAO pickupDAO;
    private DeliveryDAO deliveryDAO;
    private SalesDAO salesDAO;

    public SalesOrderManager(Context context) {
        this.context = context;
        pickupDAO = PickupDAO.getPickupDaoInstance(context);
        deliveryDAO = DeliveryDAO.getDeliveryDaoInstance(context);
        salesDAO = SalesDAO.getSalesDaoInstance(context);
    }

    public List<SaleDeliveryAndPickUp> getUpcomingPickupAndDelivery(String startDate, String endDate, int startLimit, int endLimit) {
        return salesDAO.getUpcomingPickupAndDelivery(startDate, endDate, startLimit, endLimit);
    }


    public List<SaleDeliveryAndPickUp> getUpcomingPickupAndDeliveryOnSearch(String startDate, String endDate, int startLimit, int endLimit, String searchStr) {
        return salesDAO.getUpcomingPickupAndDeliveryOnSearch(startDate, endDate, startLimit, endLimit, searchStr);
    }

    public List<SaleDeliveryAndPickUp> getOverduePickupAndDelivery(String startDate, String endDate, int startLimit, int endLimit) {
        return salesDAO.getOverduePickupAndDelivery(startDate, endDate, startLimit, endLimit);
    }


    public List<SaleDeliveryAndPickUp> getOverduePickupAndDeliveryOnSearch(String startDate, String endDate, int startLimit, int endLimit, String searchStr) {
        return salesDAO.getOverduePickupAndDeliveryOnSearch(startDate, endDate, startLimit, endLimit, searchStr);
    }

    public List<SaleDeliveryAndPickUp> getDeliveredPickupAndDelivery(String startDate, String endDate, int startLimit, int endLimit) {
        return salesDAO.getDeliveredPickupAndDelivery(startDate, endDate, startLimit, endLimit);
    }

    public List<SaleDeliveryAndPickUp> getDeliveredPickupAndDeliveryOnSearch(String startDate, String endDate, int startLimit, int endLimit, String searchStr) {
        return salesDAO.getDeliveredPickupAndDeliveryOnSearch(startDate, endDate, startLimit, endLimit, searchStr);
    }


    public List<SalesHistory> getDeliveryCancel(String startDate, String endDate, int startLimit, int endLimit) {
        return salesDAO.getDeliveryCancels(startLimit, endLimit, startDate, endDate);
    }


    public List<SalesHistory> getDeliveryCancelsOnSearch(int startLimit, int endLimit, String startDate, String endDate, String searchStr) {
        return salesDAO.getDeliveryCancelsOnSearch(startLimit, endLimit, startDate, endDate, searchStr);
    }

    public boolean makeOrderUnDelivered(Long deliveryID) {
        return deliveryDAO.makeOrderUnDelivered(deliveryID);
    }

    public boolean makeOrderDelivered(Long deliveryID) {
        return deliveryDAO.makeOrderDelivered(deliveryID);
    }

    public boolean cancelOrder(Long salesID, String type) {
        return salesDAO.cancelOrder(salesID, type);
    }

    public boolean putBackOrder(Long salesID, String type) {
        return salesDAO.putBackOrder(salesID, type);
    }

    public boolean makeOrderPicked(Long pickupID) {
        return pickupDAO.makeOrderPicked(pickupID);
    }

    public boolean makeOrderUnPicked(Long pickupID) {
        return pickupDAO.makeOrderUnPicked(pickupID);
    }

    public int getOverdueDeliveryCount() {
        return salesDAO.getOverdueDeliveryCount();
    }
}
