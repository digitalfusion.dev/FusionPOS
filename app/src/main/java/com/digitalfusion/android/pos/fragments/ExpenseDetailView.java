package com.digitalfusion.android.pos.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ExpenseIncome;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;

/**
 * Created by MD003 on 9/23/16.
 */

public class ExpenseDetailView extends Fragment {

    private View mainLayout;

    private ExpenseIncome expenseIncome;

    private TextView expenseNameTextView;

    private TextView dateTextView;

    private TextView balanceTextView;

    private TextView descTextView;

    private TextView typeTextView;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.expense_manager_item_detail_view, null);

        expenseIncome = (ExpenseIncome) getArguments().getSerializable("expense");

        context = getContext();

        String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.detail}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);

        loadUI();

        MainActivity.setCurrentFragment(this);

        String income = context.getTheme().obtainStyledAttributes(new int[]{R.attr.income}).getString(0);

        String expense = context.getTheme().obtainStyledAttributes(new int[]{R.attr.expense}).getString(0);

        if (expenseIncome.getType().toString().equalsIgnoreCase(ExpenseIncome.Type.Expense.toString())) {

            typeTextView.setBackgroundColor(Color.parseColor("#DD2C00"));

            typeTextView.setText(expense);


        } else {

            typeTextView.setBackgroundColor(Color.parseColor("#4CAF50"));

            typeTextView.setText(income);

        }

        loadOldData();

        return mainLayout;
    }

    private void loadOldData() {

        //String dayDes[] = DateUtility.dayDes(expenseIncome.getDate());

        //String yearMonthDes = DateUtility.monthYearDes(expenseIncome.getDate());

        dateTextView.setText(DateUtility.makeDateFormatWithSlash(expenseIncome.getYear(),
                expenseIncome.getMonth(), expenseIncome.getDay()));


        expenseNameTextView.setText(expenseIncome.getName());

        balanceTextView.setText(POSUtil.NumberFormat(expenseIncome.getAmount()));

        if (expenseIncome.isEmptyRemark()) {

            descTextView.setText(null);

            String noRemark = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_remark}).getString(0);

            descTextView.setHint(noRemark);

        } else {
            descTextView.setText(expenseIncome.getRemark());
        }


    }


    public void loadUI() {

        expenseNameTextView = (TextView) mainLayout.findViewById(R.id.expense_name);

        dateTextView = (TextView) mainLayout.findViewById(R.id.date);

        balanceTextView = (TextView) mainLayout.findViewById(R.id.balance);

        descTextView = (TextView) mainLayout.findViewById(R.id.description);

        typeTextView = (TextView) mainLayout.findViewById(R.id.type);

    }


}
