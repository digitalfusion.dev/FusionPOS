package com.digitalfusion.android.pos.network;

import com.digitalfusion.android.pos.util.AppConstant;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by MD002 on 8/21/17.
 */

public class AuthenticationInterceptor implements Interceptor {

    private String authToken;
    private String tokenName;
    private String tokenValue;

    public AuthenticationInterceptor(String token, String tokenName, String tokenValue) {
        this.authToken = token;
        this.tokenName = tokenName;
        this.tokenValue = tokenValue;
    }

    public AuthenticationInterceptor(String token) {
        this.authToken = token;
        this.tokenName = tokenName;
        this.tokenValue = tokenValue;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        /*String[] headerArr = new String[2];
        headerArr[0] = "SECRET_ACCESS_TOKEN";
        headerArr[1] = "";
        Headers headers = new Headers(headerArr);*/
        Request.Builder builder = original.newBuilder()
                .header("Authorization", authToken)
                .header(AppConstant.token_name, AppConstant.token);

        Request request = builder.build();
        return chain.proceed(request);
    }
}