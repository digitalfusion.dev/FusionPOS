package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.UnitManager;
import com.digitalfusion.android.pos.database.model.Unit;
import com.digitalfusion.android.pos.views.FusionToast;

/**
 * Created by MD003 on 3/9/17.
 */

public class AddEditUnitFragment extends Fragment {


    public static final String KEY = "unit";
    Context context;
    private EditText unitNameTextInputEditText;
    private EditText unitDescTextInputEditText;
    private View mainLayoutView;
    private Unit unit;
    private UnitManager unitManager;
    private boolean isEdit = false;
    private String unitName;
    private String unitDesc;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.new_unit, container, false);

        context = getContext();

        unitManager = new UnitManager(getContext());

        setHasOptionsMenu(true);

        loadUI();

        if (getArguments().getSerializable(KEY) == null) {
            String addUnit = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_unit}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(addUnit);
            isEdit = false;
        } else {
            String editUnit = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_unit}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(editUnit);
            isEdit = true;
            unit = (Unit) getArguments().get(KEY);
            initializeOldData();
        }

        return mainLayoutView;

    }

    @Override
    public void onResume() {

        super.onResume();

    }

    private void takeValueFromView() {

        unitName = unitNameTextInputEditText.getText().toString().trim();

        unitDesc = unitDescTextInputEditText.getText().toString().trim();


    }

    private void initializeOldData() {

        unitName = unit.getUnit();
        unitDesc = unit.getDescription();

        unitNameTextInputEditText.setText(unit.getUnit());
        unitDescTextInputEditText.setText(unit.getDescription());

    }

    private void clearData() {
        unitNameTextInputEditText.setText(null);
        unitNameTextInputEditText.setError(null);
        unitDescTextInputEditText.setText(null);
        unitDescTextInputEditText.setError(null);

    }

    public boolean checkUnitValidation() {

        boolean status = true;

        if (unitNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;

            String enterUnit = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_unit_name}).getString(0);
            unitNameTextInputEditText.setError(enterUnit);

        } else {

            unitName = unitNameTextInputEditText.getText().toString().trim();
            unitDesc = unitDescTextInputEditText.getText().toString().trim();

            if (!isEdit) {
                if (unitManager.checkUnitExists(unitName)) {
                    status = false;
                    String alreadyExist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.unit_already_exist}).getString(0);
                    unitNameTextInputEditText.setError(alreadyExist);
                }
            } else {
                unitName = unitNameTextInputEditText.getText().toString().trim();
                unitDesc = unitDescTextInputEditText.getText().toString().trim();

                if (unitManager.checkUnitExists(unitName) && !unit.getUnit().equals(unitName)) {
                    status = false;
                    String alreadyExist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.unit_already_exist}).getString(0);
                    unitDescTextInputEditText.setError(alreadyExist);
                }
            }

        }

        return status;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save) {

            if (!isEdit) {
                saveUnit();
            } else {

                if (checkUnitValidation()) {
                    takeValueFromView();
                    unitManager.updateUnit(unitName, unitDesc, unit.getId());
                    getActivity().onBackPressed();
                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                }
            }

        }
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void saveUnit() {
        if (checkUnitValidation()) {

            takeValueFromView();

            boolean status = unitManager.addNewUnit(unitName, unitDesc);

            if (status) {

                clearData();
                getActivity().onBackPressed();
                FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);
     /*   TypedArray save=context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        FontTextView fontTextView= (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.menu_text,null);

        menu.getItem(0).setActionView(fontTextView);

        fontTextView.setText(save.getString(0));

        fontTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menu.getItem(0));
            }
        });
*/


        super.onCreateOptionsMenu(menu, inflater);

    }

    private void loadUI() {
        unitNameTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.unit_name_in_unit_add_dg);

        unitDescTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.unit_desc_in_unit_add_dg);

    }

}
