package com.digitalfusion.android.pos.database.dao;

import android.content.Context;

import com.digitalfusion.android.pos.database.model.LostItemInDetail;

import java.util.List;

/**
 * Created by MD002 on 8/18/16.
 */
public class LostDetailDAO extends ParentDAO {
    private static ParentDAO lostDetailDaoInstance;
    private Context context;
    private IdGeneratorDAO idGeneratorDAO;
    private LostItemInDetail lostDetailView;
    private List<LostItemInDetail> lostDetailViewList;
    private List<Long> idList;

    private LostDetailDAO(Context context) {
        super(context);
        this.context = context;
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        lostDetailView = new LostItemInDetail();
    }

    public static LostDetailDAO getLostDetailDaoInstance(Context context) {
        if (lostDetailDaoInstance == null) {
            lostDetailDaoInstance = new LostDetailDAO(context);
        }
        return (LostDetailDAO) lostDetailDaoInstance;
    }

    /*public Long addNewStockLostDetail (Long lostID, Long stockID, int qty, Double purchasePrice, String remark, Double total){
        genID = idGeneratorDAO.getLastIdValue("LostDetail");
        genID+=1;
        database = databaseHelper.getWritableDatabase();
        query = "insert into " + AppConstant.LOST_DETAIL_TABLE_NAME + "(" + AppConstant.LOST_DETAIL_ID + ", " + AppConstant.LOST_DETAIL_LOST_ID + ", "
                + AppConstant.LOST_DETAIL_STOCK_ID + ", " + AppConstant.LOST_DETAIL_QTY + ", " + AppConstant.LOST_DETAIL_PURCHASE_PRICE + ", " + AppConstant.LOST_DETAIL_TOTAL
                + ", " + AppConstant.LOST_DETAIL_REMARK + ") values (?,?,?,?,?,?,?)";
        statement = database.compileStatement(query);
        statement.bindString(1, genID.toString());
        // statement.bindString(1, "lkdkk");
        statement.bindString(2, lostID.toString());
        statement.bindString(3, stockID.toString());
        statement.bindString(4, Integer.toString(qty));
        statement.bindString(5, purchasePrice.toString());
        statement.bindString(6, total.toString());
        //statement.bindString(7, unitID.toString());
       // remark = "ieoane";
        if (remark == null){
            statement.bindNull(7);
        }else {
            statement.bindString(7, remark);
        }
        statement.execute();
        statement.clearBindings();
        return genID;
    }

    public List<LostItemInDetail> getAllLostDetailByLostID (Long lostID){
        lostDetailViewList = new ArrayList<>();
        query = "select l." + AppConstant.LOST_DETAIL_ID + ", l." + AppConstant.LOST_DETAIL_LOST_ID + ", l." + AppConstant.LOST_DETAIL_STOCK_ID + ", s." + AppConstant.STOCK_NAME +
                ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_BARCODE + ", s." + AppConstant.STOCK_CATEGORY_ID + ", c." + AppConstant.CATEGORY_NAME + ", l." + AppConstant.LOST_DETAIL_QTY +
                ", s." + AppConstant.LOST_DETAIL_PURCHASE_PRICE + ", l." + AppConstant.LOST_DETAIL_TOTAL + ", s." + AppConstant.STOCK_UNIT_ID + ", u." + AppConstant.UNIT_UNIT + ", l."
                + AppConstant.LOST_DETAIL_REMARK + " from " + AppConstant.LOST_DETAIL_TABLE_NAME + " l, " + AppConstant.STOCK_TABLE_NAME + " s left outer join " + AppConstant.UNIT_TABLE_NAME + " u on s."
                + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + ", " + AppConstant.CATEGORY_TABLE_NAME +
                " c where l." + AppConstant.LOST_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and s." + AppConstant.STOCK_CATEGORY_ID + " = c."
                + AppConstant.CATEGORY_ID + " and l." + AppConstant.LOST_DETAIL_LOST_ID + " = " + lostID;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query,null);
            if (cursor.moveToFirst()) {
                do {
                    lostDetailView = new LostItemInDetail();
                    lostDetailView.setId(cursor.getLong(0));
                    lostDetailView.setLostID(cursor.getLong(1));
                    lostDetailView.setStockID(cursor.getLong(2));
                    lostDetailView.setStockName(cursor.getString(3));
                    lostDetailView.setStockCodeNo(cursor.getString(4));
                    lostDetailView.setStockBarcodeNo(cursor.getString(5));
                    lostDetailView.setStockCategoryID(cursor.getLong(6));
                    lostDetailView.setStockCategoryName(cursor.getString(7));
                    lostDetailView.setQty(cursor.getInt(8));
                    lostDetailView.setPurchasePrice(cursor.getDouble(9));
                    lostDetailView.setTotal(cursor.getDouble(10));
                    lostDetailView.setUnitID(cursor.getLong(11));
                    lostDetailView.setUnit(cursor.getString(12));
                    lostDetailView.setRemark(cursor.getString(13));
                    lostDetailViewList.add(lostDetailView);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        }catch (SQLiteException e){
            e.printStackTrace();
        }finally {
            cursor.close();
            database.endTransaction();
        }
        return lostDetailViewList;
    }

    public boolean updateStockLostDetail(Long id, Long stockID, int qty, Double purchasePrice, Double total, String remark){
        query = "update " + AppConstant.LOST_DETAIL_TABLE_NAME + " set " + AppConstant.LOST_DETAIL_STOCK_ID + "=?, " + AppConstant.LOST_DETAIL_QTY + "=?, "
                + AppConstant.LOST_DETAIL_PURCHASE_PRICE + "=?, " + AppConstant.LOST_DETAIL_TOTAL + "=?, " +
                AppConstant.LOST_DETAIL_REMARK + "=? where " + AppConstant.LOST_DETAIL_ID + "=?";
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query, new String[]{stockID.toString(), Integer.toString(qty), purchasePrice.toString(), total.toString(), remark, id.toString()});
        return true;
    }

    public boolean deleteStockLostDetail(Long id){
        query = "delete from " + AppConstant.LOST_DETAIL_TABLE_NAME + " where " + AppConstant.LOST_DETAIL_ID + " = " + id;
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query);
       // databaseHelper.close();
        return true;
    }

    public List<Long> getLostIdList(Long lostID) {
        idList = new ArrayList<>();
        query = "select " + AppConstant.LOST_ID + " from " + AppConstant.LOST_TABLE_NAME + " where " + AppConstant.LOST_DETAIL_LOST_ID + " = " + lostID;
        database = databaseHelper.getWritableDatabase();
        cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                idList.add(cursor.getLong(0));
            }while (cursor.moveToNext());
            cursor.close();
        }
        return idList;
    }*/
}
