package com.digitalfusion.android.pos.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.TownShipSearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForBusinessType;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.information.AppInfo;
import com.digitalfusion.android.pos.information.DeviceInfo;
import com.digitalfusion.android.pos.information.LicenceInfo;
import com.digitalfusion.android.pos.information.PaymentInfo;
import com.digitalfusion.android.pos.information.Registration;
import com.digitalfusion.android.pos.information.Subscription;
import com.digitalfusion.android.pos.database.model.BusinessType;
import com.digitalfusion.android.pos.network.ApiRetrofit;
import com.digitalfusion.android.pos.network.NetworkClient;
import com.digitalfusion.android.pos.util.ConnectivityUtil;
import com.digitalfusion.android.pos.util.GrantPermissionFromActivity;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.FusionToast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterationActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int REQUEST_CHECK_SETTINGS = 10;
    private final long INTERVAL = 1000 * 10;
    private final long FASTEST_INTERVAL = 1000 * 5;
    private EditText usernameEditText;
    private EditText businessNameEditText;
    private TextView businessTypeTextView;
    private EditText addressEditText;
    private EditText phoneNoEditText;
    private MaterialDialog businessTypeMaterialDialog;
    private RVAdapterForBusinessType rvAdapterForBusinessType;
    private List<BusinessType> businessTypeList;
    private List<String> townShipList;
    private List<String> stateList;
    private int autoTimeAvailable = 0;
    private View view;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private Boolean mRequestingLocationUpdates;
    private String region = null;
    private int theme;
    private KProgressHUD hud;
    private RVAdapterForFilter rvAdapterForstateList;
    private TownShipSearchAdapter townShipSearchAdapter;
    private MaterialDialog stateMaterialDialog;
    private TextView stateTextView;
    private AutoCompleteTextView autoCompleteTextView;
    private GrantPermissionFromActivity grantPermission;
    private boolean isRegister = false;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration);
        theme = POSUtil.getDefaultThemeNoActionBar(this);
        setTheme(theme);
        usernameEditText = findViewById(R.id.username_edit_text);
        businessNameEditText = findViewById(R.id.business_name_edit_text);
        businessTypeTextView = findViewById(R.id.business_type_text_view);
        addressEditText = findViewById(R.id.address_edit_text);
        phoneNoEditText = findViewById(R.id.phone_no_edit_text);
        autoTimeAvailable = 0;
        mRequestingLocationUpdates = false;
        stateTextView = findViewById(R.id.state_txt);
        autoCompleteTextView = findViewById(R.id.town_ship);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        DeviceInfo deviceInfo = new DeviceInfo();
        Log.w("MAC ADDRESSS", deviceInfo.getMacAddress() + " MAC");

        try {
            autoTimeAvailable = Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME);
        } catch (Settings.SettingNotFoundException e) {

            Log.w("Here", autoTimeAvailable + " :D");
            e.printStackTrace();

        }
        buildBusinessTypeDialog();
        clickListener();

        //Log.w("TESTING", autoTimeAvailable + " :D");

        // navigateActivity();

        Log.e("on", "create");
        buildGoogleApiClient();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //            setButtonsEnabledState();
            //            startLocationUpdates();

            //generateAddress();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                showRationaleDialog();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        }

        grantPermission = new GrantPermissionFromActivity(this);

    }

    private void buildBusinessTypeDialog() {

        stateList = new ArrayList<>();
        stateList.add("ကခ်င္");
        stateList.add("ကယား");
        stateList.add("ကရင္");
        stateList.add("ခ်င္း");
        stateList.add("မြန္");
        stateList.add("ရခိုင္");
        stateList.add("ရွမ္း");
        stateList.add("စစ္ကိုင္း");
        stateList.add("တနသၤာရီ");
        stateList.add("ပဲခူး");
        stateList.add("မေကြး");
        stateList.add("ရန္ကုန္");
        stateList.add("ဧရာဝတီ");
        stateList.add("မႏၲေလး");
        stateList.add("ေနျပည္ေတာ္");

        townShipList = new ArrayList<>();
        townShipList.add("ေထာက္ၾကန္႔");
        townShipList.add("ေမွာ္ဘီ");
        townShipList.add("လွည္းကူး");
        townShipList.add("တိုက္ႀကီး");
        townShipList.add("အုတ္ကန္");
        townShipList.add("ထန္းတပင္");
        townShipList.add("ေရႊျပည္သာ");
        townShipList.add("လိွုင္သာယာ");
        townShipList.add("ေတာင္ဒဂံု");
        townShipList.add("ေျမာင္ဒဂံု;");
        townShipList.add("သန္လ်င္");
        townShipList.add("ေက်ာက္တန္း");
        townShipList.add("သံုးခြ");
        townShipList.add("ခရမ္း");
        townShipList.add("တြံေတး");
        townShipList.add("ေကာ့မႉး");
        townShipList.add("ကြမ္းျခံကုန္း");
        townShipList.add("ကိုကိုးကၽြန္း");
        townShipList.add("အင္းစိန္");
        townShipList.add("သာေကတ");
        townShipList.add("သကၤန္းကြ်န္း");
        townShipList.add("တာေမြ");
        townShipList.add("ဗိုလ္တေထာင္");
        townShipList.add("အလံု");
        townShipList.add("ဗဟန္း");
        townShipList.add("ဒဂံုဆိပ္ကမ္း");
        townShipList.add("ဒဂံု");
        townShipList.add("ဒလ");
        townShipList.add("ေဒါပံု");
        townShipList.add("လိွုင္");
        townShipList.add("အင္းစိန္");
        townShipList.add("ကမာရြတ္");
        townShipList.add("လမ္းမေတာ္");
        townShipList.add("လသာ");
        townShipList.add("မရမ္းကုန္း");
        townShipList.add("မဂၤလာေတာင္ညြန္႔");
        townShipList.add("မဂၤလာဒံု");
        townShipList.add("ပန္းပဲတန္း");
        townShipList.add("ပုဇြန္ေတာင္");
        townShipList.add("စမ္းေခ်ာင္း");
        townShipList.add("ဆိပ္ကမ္း");
        townShipList.add("ဆိပ္ႀကီးခေနာင္တုိ");
        townShipList.add("ေတာင္ဥကၠလာ");
        townShipList.add("ေျမာက္ဥကၠလာ");
        townShipList.add("အေရွ႕ဒဂံု");
        townShipList.add("ရန္ကင္း");
        townShipList.add("ေအာင္ေျမသာစံ");
        townShipList.add("ခ်မ္းေအးသာစံ");
        townShipList.add("ခ်မ္းျမသာစည္");
        townShipList.add("ေက်ာက္ပန္းေတာင္း");
        townShipList.add("ေက်ာက္ဆည္");
        townShipList.add("မဟာေအာင္ေျမ");
        townShipList.add("မလုိုင္");
        townShipList.add("မိတၳီလာ");
        townShipList.add("မုိးကုတ္");
        townShipList.add("ျမင္းျခံ");
        townShipList.add("ျမစ္သား");
        townShipList.add("နထိုးႀကီး");
        townShipList.add("ငဇြန္");
        townShipList.add("ေညာင္ဦး");
        townShipList.add("ပုသိမ္ႀကီး");
        townShipList.add("ေပ်ာ္ဘြယ္");
        townShipList.add("ျပည္ႀကီးတံခြန္");
        townShipList.add("ျပင္ဦးလြင္");
        townShipList.add("စဥ့္ကူး");
        townShipList.add("စဥ့္ကိုင္");
        townShipList.add("တံတားဦး");
        townShipList.add("ေတာင္သာ");
        townShipList.add("သပိတ္က်င္း");
        townShipList.add("သာစည္");
        townShipList.add("ဝမ္းတြင္း");
        townShipList.add("ရမည္းသင္း");
        townShipList.add("ငါန္းဇြန္");
        townShipList.add("ဒဂံုၿမိဳ႕သစ္အေရွ႕ပိုင္း");
        townShipList.add("ဒဂံုၿမိဳ႕သစ္ေတာင္ပိုင္း");
        townShipList.add("ဒဂံုၿမိဳ႕သစ္ေျမာက္ပိုင္း");


        businessTypeList = new ArrayList<>();
        businessTypeList.add(new BusinessType("အလွကုန္", true));
        businessTypeList.add(new BusinessType("လူသံုးကုန္"));
        businessTypeList.add(new BusinessType("စားေသာက္ကုန္"));
        businessTypeList.add(new BusinessType("စာေရးကိရိယာ"));
        businessTypeList.add(new BusinessType("အဝတ္အထည္"));
        businessTypeList.add(new BusinessType("အထည္အလိပ္"));
        businessTypeList.add(new BusinessType("ပရိေဘာဂ"));
        businessTypeList.add(new BusinessType("အိမ္ေဆာက္ပစၥည္း/ကုန္မာ"));
        businessTypeList.add(new BusinessType("ကေလးပစၥည္း"));
        businessTypeList.add(new BusinessType("ကုန္ေျခာက္"));
        businessTypeList.add(new BusinessType("ေရပိုက္ႏွင့္ဆက္စပ္ပစၥည္း"));
        businessTypeList.add(new BusinessType("လၽွပ္စစ္ပစၥည္း"));
        businessTypeList.add(new BusinessType("ဖန္စီပစၥည္း"));
        businessTypeList.add(new BusinessType("ေရသန္႔"));
        businessTypeList.add(new BusinessType("စာေရးကိရိယာ"));
        businessTypeList.add(new BusinessType("ကြန္ပ်ဴတာႏွင့္ဆက္စပ္ပစၥည္း"));
        businessTypeList.add(new BusinessType("Mobile ဖုန္းဆိုင္"));
        businessTypeList.add(new BusinessType("ဖိိနပ္ဆိုင္"));
        businessTypeList.add(new BusinessType("Other"));
        rvAdapterForBusinessType = new RVAdapterForBusinessType(businessTypeList);

        rvAdapterForstateList = new RVAdapterForFilter(stateList);

        townShipSearchAdapter = new TownShipSearchAdapter(this, townShipList);

        autoCompleteTextView.setAdapter(townShipSearchAdapter);
        autoCompleteTextView.setThreshold(1);

        stateMaterialDialog = new MaterialDialog.Builder(this)
                .adapter(rvAdapterForstateList, new LinearLayoutManager(this)).build();

        String choose = getTheme().obtainStyledAttributes(new int[]{R.attr.choose_business_type}).getString(0);
        businessTypeMaterialDialog = new MaterialDialog.Builder(this).

                title(choose)

                .adapter(rvAdapterForBusinessType, new LinearLayoutManager(this))
                .positiveText(Html.fromHtml("<font color='#424242'>Ok</font>"))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        String types = "";
                        for (BusinessType businessType : businessTypeList) {
                            if (businessType.isSelected()) {
                                types += businessType.getName() + ", ";
                            }
                        }
                        if (types != "") {
                            types = types.substring(0, types.length() - 2);
                        }
                        businessTypeTextView.setText(types);
                    }
                })
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }


    private void generateAddress() {
        Log.e(mCurrentLocation == null ? "null" : "not null", "dkfjdjfkjad");
        if (mCurrentLocation != null) {
            Geocoder geocoder = new Geocoder(RegisterationActivity.this, Locale.ENGLISH);
            Log.e("locale", Locale.getDefault() + " kdjfajfkjadfk " + Locale.ENGLISH);
            try {
                List<Address> addresses = geocoder.getFromLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 1);
                Log.e("size", addresses.size() + " a");
                for (Address address : addresses) {
                    region = address.getSubLocality();
                    //                Log.e("address ", address.toString());
                    //                Log.e("address " + address.getSubAdminArea(), address.getPostalCode() + " poo " + ", " + address.getPremises());
                    //                Log.e("lkjk " + address.getSubLocality() + ", " + address.getAdminArea() + ", " + address.getCountryName(), "lklkl " + address.getLocality());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void clickListener() {


        findViewById(R.id.state_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateMaterialDialog.show();
            }
        });
        rvAdapterForstateList.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                stateTextView.setText(stateList.get(position));
                stateMaterialDialog.dismiss();
            }
        });

        findViewById(R.id.regiseter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRegister = true;


                Log.e("on ", "click");
                if (isValid()) {
                    view = v;
                    if (grantPermission.permission()) {
                        processWithApi();
                    }

                    /*Intent intent = new Intent(RegisterationActivity.this, ActivationActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("userID", "aaa");
                    bundle.putString("code", "bbb");
                    intent.putExtras(bundle);
                    startActivity(intent);*/
                }

            }
        });

        businessTypeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                businessTypeMaterialDialog.show();
            }
        });
    }

    //    private void navigateActivity() {
    //        if (POSUtil.isUnExpectedChangeBoolean(this)) {
    //            finish();
    //            Intent intent = new Intent(RegisterationActivity.this, ActivationActivity.class);
    //            startActivity(intent);
    //        } else {
    //            if (POSUtil.isActivatedBoolean(this)) {
    //                finish();
    //                Intent intent = new Intent(RegisterationActivity.this, LoginActivity.class);
    //                startActivity(intent);
    //            }
    //        }
    //    }

    private void showRationaleDialog() {
        new AlertDialog.Builder(this)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(RegisterationActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                        Log.e("ok", "button");

                        //generateAddress();
                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(RegisterationActivity.this, "process cancel", Toast.LENGTH_SHORT).show();
                        mRequestingLocationUpdates = false;
                    }
                })
                .setCancelable(false)
                .setMessage("requestPermissions")
                .show();
    }

    protected synchronized void buildGoogleApiClient() {
        //Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void processWithApi() {
        generateAddress();
        //LicenceInfo licenceInfo = new LicenceInfo("licence type", new Date(2017, 6, 12), new Date(2017, 9, 12), "3", "ABCDEFG", "Active");
        //PaymentInfo paymentInfo = new PaymentInfo("mpt", "dkjfidjai", "odifjaijf", 10000l);
        LicenceInfo  licenceInfo  = new LicenceInfo();
        PaymentInfo  paymentInfo  = new PaymentInfo();
        Registration registration = new Registration();
        if (mCurrentLocation != null) {
            registration.setLatitude(mCurrentLocation.getLatitude() + "");
            registration.setLongitude("" + mCurrentLocation.getLongitude());
        }
        registration.setUserName(usernameEditText.getText().toString());
        registration.setAddress(addressEditText.getText().toString());
        registration.setBusinessName(businessNameEditText.getText().toString());
        registration.setBusinessType(businessTypeTextView.getText().toString());
        //        registration.setEmail(emailEditText.getText().toString());
        registration.setPhone(phoneNoEditText.getText().toString());

        registration.setTownship(autoCompleteTextView.getText().toString());

        registration.setState(stateTextView.getText().toString());
        // registration.setRegion(region);

        final DeviceInfo deviceInfo = new DeviceInfo();

        AppInfo            appInfo      = new AppInfo();
        final Subscription subscription = new Subscription(false, licenceInfo, paymentInfo, registration, appInfo, deviceInfo);

        Log.w("Hrere", "ON CLICK ");
        Log.w("DEVICE INFO", deviceInfo.toString() + " S");

        //        Log.e("date ", new Date(1992, 2, 12) + " dafjldkjfkadf " + new Date());

        //        final ApiRetrofit apiRetrofit = NetworkClient.getClient().create(ApiRetrofit.class);
        final ApiRetrofit apiRetrofit = NetworkClient.createService(ApiRetrofit.class, "digital_fusion", "digital_fusion");
        //        /*OkHttpClient clientNormal;
        //        OkHttpClient.Builder builder1 = new OkHttpClient().newBuilder();
        //        builder1.authenticator(new Authenticator() {
        //            @Override
        //            public Request authenticate(Route route, okhttp3.Response response) throws IOException {
        //                String authentication = Credentials.basic(CLIENT_ID, CLIENT_SECRET);
        //                Request.Builder builder = response.request().newBuilder().addHeader("AuthorizationManager", authentication);
        //                return builder.build();
        //            }
        //        });
        //        clientNormal = builder1.build();
        //        Retrofit retrofit;
        //
        //        Gson gson = new GsonBuilder()
        //                .setDateFormat("yyyy-MM-dd")
        //                .create();
        //        retrofit = new Retrofit.Builder().client(clientNormal).
        //                baseUrl("http://128.199.69.212:8080/BluemixCRM/api/").
        //                addConverterFactory(GsonConverterFactory.create(gson)).build();
        //
        //        ///
        //        */

        View      processDialog = View.inflate(this, R.layout.process_dialog_custom_layout, null);
        ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
        imageView.setBackgroundResource(R.drawable.spin_animation);
        AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
        drawable.start();

        //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setDetailsLabel("")
                .setBackgroundColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);

        Call<Subscription> call = apiRetrofit.register(subscription);
        Log.e("callhey", "call");

        Log.w("REGISTRATION", registration.toString());
        hud.show();
        call.enqueue(new Callback<Subscription>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(Call<Subscription> call, Response<Subscription> response) {
                if (POSUtil.isAutomaticTimeZone(RegisterationActivity.this)) {
                    if (response.code() == 200) {
                        Subscription responseSubscription = response.body();
                        if (responseSubscription != null) {
                            Log.e("responseFrom", responseSubscription.toString());
                            POSUtil.acitvate(responseSubscription, RegisterationActivity.this);
                            Intent intent = new Intent(RegisterationActivity.this, AddNewPasscodeActivity.class);
                            startActivity(intent);


                        }
                    } else if (response.code() == 409) {
                        Log.e("nocontent", "hey");
                    } else {
                        FusionToast.toast(RegisterationActivity.this, FusionToast.TOAST_TYPE.ERROR, response.code() + " sss");
                    }
                    Log.e("nocontent", response.code() + " code");
                    ;
                } else {
                    Intent intent = new Intent(RegisterationActivity.this, VerifyActivity.class);

                    startActivity(intent);
                }

                hud.dismiss();
            }

            @Override
            public void onFailure(Call<Subscription> call, Throwable t) {
                t.printStackTrace();
                ConnectivityUtil.handleFailureProcess(t, view);
                Log.w("FAilure", "Failure");
                Intent intent = new Intent(RegisterationActivity.this, VerifyActivity.class);
                startActivity(intent);
                hud.dismiss();
            }
        });
        //        call.enqueue(new Callback<HashMap>() {
        //            @Override
        //            public void onResponse(Call<HashMap> call, Response<HashMap> response) {
        //                Log.w("HERERR", response.code() + " CODE");
        //                if (response.code() == 200) {
        //
        //                    Log.w("HERE Response", (response.body() == null) ? "null" : "not null");
        //
        //                    HashMap hashMap = response.body();
        //                    Log.e("hash", hashMap == null ? "null" : "not");
        //                    if (hashMap != null) {
        //                        String value = hashMap.get("userId").toString();
        //                        List<String> valueList = Arrays.asList(value.split(","));
        //                        Intent intent = new Intent(RegisterationActivity.this, ActivationActivity.class);
        //                        Bundle bundle = new Bundle();
        //                        bundle.putString("userID", valueList.get(0));
        //                        bundle.putString("code", valueList.get(1));
        //                        bundle.putString("phone", phoneNoEditText.getText().toString());
        //                        intent.putExtras(bundle);
        //                        startActivity(intent);
        //                    }
        //                } else if (response.code() == 204){
        //                    DeviceRequest deviceRequest = new DeviceRequest(deviceInfo.getSerial(), deviceInfo.getMacAddress());
        //                    Call<Subscription> call1 = apiRetrofit.checkDevice(deviceRequest);
        //                    call1.enqueue(new Callback<Subscription>() {
        //                        @Override
        //                        public void onResponse(Call<Subscription> call, Response<Subscription> response) {
        //                            Subscription subscription1 = response.body();
        //
        //                            Intent intent = new Intent(RegisterationActivity.this, ActivationActivity.class);
        //                            Bundle bundle = new Bundle();
        //                            bundle.putString("userID", subscription1.getRegistration().getUserId());
        //                            bundle.putString("phone", phoneNoEditText.getText().toString());
        //                            bundle.putString("code", "code");
        //                            Log.e("dodfaofo", "name " + subscription1.getRegistration().getBusinessName() + " " + subscription1.getRegistration().getCreatedDate());
        //                            intent.putExtras(bundle);
        //                            startActivity(intent);
        //
        //                            Snackbar snackbar = Snackbar.make(view, subscription1.getRegistration().getBusinessName(), Snackbar.LENGTH_LONG);
        //                            snackbar.show();
        //                        }
        //
        //                        @Override
        //                        public void onFailure(Call<Subscription> call, Throwable t) {
        //                            t.printStackTrace();
        //                            NetworkUtil.handleFailureProcess(t, view);
        //                        }
        //                    });
        //                }
        //            }
        //
        //            @Override
        //            public void onFailure(Call<HashMap> call, Throwable t) {
        //
        //                t.printStackTrace();
        //                NetworkUtil.handleFailureProcess(t, view);
        //                Log.w("FAilure", "Failure");
        //            }
        //        });
    }

    public boolean isValid() {
        boolean status = true;

        if (usernameEditText.getText().length() < 1 && usernameEditText.getText().toString().equalsIgnoreCase("")) {

            status = false;
            usernameEditText.setError("Enter user name");
        }
        if (businessNameEditText.getText().length() < 1 && businessNameEditText.getText().toString().equalsIgnoreCase("")) {

            status = false;
            businessNameEditText.setError("Enter business name");
        }

        if (autoCompleteTextView.getText().length() < 1 && autoCompleteTextView.getText().toString().equalsIgnoreCase("")) {

            status = false;
            autoCompleteTextView.setError("Enter Town Ship");
        }

        if (stateTextView.getText().length() < 1 && stateTextView.getText().toString().equalsIgnoreCase("")) {

            status = false;
            stateTextView.setError("Choose State");
        }

        if (phoneNoEditText.getText().length() < 1 && phoneNoEditText.getText().toString().equalsIgnoreCase("")) {

            status = false;
            phoneNoEditText.setError("Enter phone");
        }

        if (businessTypeTextView.getText().length() < 1 && businessNameEditText.getText().toString().equalsIgnoreCase("")) {

            status = false;
            businessTypeTextView.setError("Select at least one business type");
        }
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                autoTimeAvailable = Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME);
                if (autoTimeAvailable != 1) {
                    status = false;
                    final MaterialDialog materialDialog = new MaterialDialog.Builder(this)
                            .customView(R.layout.network_automatic_dialog_view, true)
                            .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                            .build();
                    materialDialog.show();
                    materialDialog.findViewById(R.id.dismiss).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            materialDialog.dismiss();
                        }
                    });
                }
            }
        } catch (Settings.SettingNotFoundException e) {

            Log.w("Here", autoTimeAvailable + " :D");
            e.printStackTrace();


            status = false;

        }

        return status;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == android.R.id.home) {

            super.onBackPressed();

            Log.w("here ComplainFActivity", "OnItem Selected here");
        }


        return false;
    }

    public boolean isPlayServicesAvailable(Context context) {
        //        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        //        if (resultCode != ConnectionResult.SUCCESS) {
        //            GoogleApiAvailability.getInstance().getErrorDialog((Activity) context, resultCode, 2).show();
        //            return false;
        //        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (isRegister) {
                if (isValid()) {
                    processWithApi();
                }
            }

        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            grantPermission.askAgainPermission();
        } else {
            grantPermission.forcePermissionSetting();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        if (mCurrentLocation == null) {
                            if (ActivityCompat.checkSelfPermission(RegisterationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                                    RegisterationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                                Log.e("curr", "loc");
                            }
                            //            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                            //            updateUI();
                        }
                        //                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("on", "start");
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        isPlayServicesAvailable(this);

        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates if the user has requested them.

        //
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        //        if (mGoogleApiClient.isConnected()) {
        //            stopLocationUpdates();
        //        }
    }

    @Override
    protected void onStop() {
        //stopLocationUpdates();
        mGoogleApiClient.disconnect();

        super.onStop();
    }

    @Override
    public void onBackPressed() {
       /* if (doubleBackToExitPressedOnce) {
            Log.e("exit", " after ");
            Log.e("finish", "hey");

            //MainActivity.close=true;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                finishAffinity();
            }
            System.exit(0);

            //}
        }
        Log.e("jkj", "kjoijikjioj");
        this.doubleBackToExitPressedOnce = true;
        POSUtil.showSnackBar(rootView, "Please click BACK again to exit");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("make", "false");

                doubleBackToExitPressedOnce = false;
            }
        }, 2000);*/

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("on", "onConnected");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            //            Log.e("loc", "cur" + mCurrentLocation.getLongitude());
            //            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            //            updateUI();
        }

        if (mRequestingLocationUpdates) {
            //            startLocationUpdates();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //        /Log.i(TAG, "onLocationChanged");
        mCurrentLocation = location;
        //        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        //        updateUI();
        Toast.makeText(this, getResources().getString(R.string.contents_location), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        //        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        //        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }
}
