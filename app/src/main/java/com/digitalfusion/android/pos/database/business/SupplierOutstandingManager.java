package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.database.dao.SupplierOutstandingDAO;
import com.digitalfusion.android.pos.database.model.OutstandingPaymentDetail;
import com.digitalfusion.android.pos.database.model.PurchaseHistory;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InvoiceNoGenerator;

import java.util.List;

/**
 * Created by hninhsuhantharkyaw on 10/24/16.
 */

public class SupplierOutstandingManager {
    private Context context;
    private SupplierOutstandingDAO supplierOutstandingDAO;
    private InvoiceNoGenerator invoiceNoGenerator;
    private boolean flag;

    public SupplierOutstandingManager(Context context) {
        this.context = context;
        supplierOutstandingDAO = SupplierOutstandingDAO.getSupplierOutstandingDaoInstance(context);
        invoiceNoGenerator = new InvoiceNoGenerator(context);
    }

    public List<PurchaseHistory> getSupplierOutstandingList(int startLimit, int endLimit, String startDate, String endDate) {
        return supplierOutstandingDAO.getSupplierOutstandingList(startLimit, endLimit, startDate, endDate);
    }


    public List<PurchaseHistory> getSupplierOutstandingListOnSearch(int startLimit, int endLimit, String startDate, String endDate, String searchStr) {
        return supplierOutstandingDAO.getSupplierOutstandingListOnSearch(startLimit, endLimit, startDate, endDate, searchStr);
    }

    public List<PurchaseHistory> getSupplierOutstandingListBySupplier(int startLimit, int endLimit, String startDate, String endDate, Long supplierId) {
        return supplierOutstandingDAO.getSupplierOutstandingListBySupplierId(startLimit, endLimit, startDate, endDate, supplierId);
    }

    public boolean addSupplierOutstandingPayment(String invoiceNo, Long supplierID, Long purchaseID, Double paidAmt, String day, String month, String year) {
        flag = supplierOutstandingDAO.addSupplierOutstandingPayment(invoiceNo, supplierID, purchaseID, paidAmt, day, month, year, DateUtility.makeDate(year, month, day));
        if (flag) {
            invoiceNoGenerator.updateInvoiceNextNum();
        }
        return flag;
    }

    public String getSupplierOutstandingPaymentInvoiceNo() {
        return invoiceNoGenerator.getInvoiceNo(AppConstant.SUPPLIER_OUTSTANDING_INVOICE_NO);
    }

    public OutstandingPaymentDetail getSupplierOutstandingPaymentsByPurchaseID(Long purchaseID) {
        return supplierOutstandingDAO.getSupplierOutstandingPaymentsByPurchaseID(purchaseID);
    }

    public Double getPaidAmtByPurchaseID(Long purchaseID) {

        return supplierOutstandingDAO.getPaidAmtByPurchaseID(purchaseID);
    }

    public Double getTotalPayableAmt(String startDate, String endDate) {

        return supplierOutstandingDAO.getTotalPayableAmt(startDate, endDate);
    }

    public boolean deleteSupplierOutstandingPayment(Long id) {
        return supplierOutstandingDAO.deleteSupplierOutstandingPayment(id);
    }

    public boolean updateSupplierOutstandingPayment(Long purchaseID, Long supplierID, Double paidAmt, Long id, String day, String month, String year) {
        return supplierOutstandingDAO.updateSupplierOutstandingPayment(purchaseID, supplierID, paidAmt, id, day, month, year);
    }
}
