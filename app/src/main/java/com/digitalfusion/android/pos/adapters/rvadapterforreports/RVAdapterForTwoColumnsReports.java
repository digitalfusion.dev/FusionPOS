package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD002 on 12/3/16.
 */

public class RVAdapterForTwoColumnsReports extends ParentRVAdapterForReports {
    private final int HEADER_TYPE = 10000;
    //private List<ReportItem> expenseReportItemViewList;
    private Type type;
    private List<ReportItem> reportItemList;
    private Context context;

    private String[] months;

    public RVAdapterForTwoColumnsReports(List<ReportItem> reportItemList, Type type, Context context) {
        this.reportItemList = reportItemList;
        this.type = type;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.two_columns_header, parent, false);
            return new HeaderItemView(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_for_month_qty, parent, false);

            return new ReportItemViewHolder(v);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        switch (type) {
            case MONTH_TRANSACTION:
                if (viewHolder instanceof ReportItemViewHolder) {
                    ReportItemViewHolder holder = (ReportItemViewHolder) viewHolder;
                    holder.monthTextView.setText(AppConstant.MONTHS[reportItemList.get(position - 1).getMonth() - 1]);
                    holder.qtyTextView.setText(POSUtil.NumberFormat(reportItemList.get(position - 1).getQty()));
                    //holder.expenseTextView.setText(Double.toString(expenseReportItemViewList.get(position).getBalance()));
                } else {
                    HeaderItemView holder = (HeaderItemView) viewHolder;
                    holder.firstColumn.setText(ThemeUtil.getString(context, R.attr.month));
                    holder.secondColumn.setText(ThemeUtil.getString(context, R.attr.transaction));
                }

                break;
            case MONTH_AMOUNT:
                if (viewHolder instanceof ReportItemViewHolder) {
                    ReportItemViewHolder holder = (ReportItemViewHolder) viewHolder;
                    holder.monthTextView.setText(months[position - 1 ]);
                    holder.qtyTextView.setText(POSUtil.NumberFormat(getTotalAmount(position)));
//                    holder.monthTextView.setText(AppConstant.MONTHS[reportItemList.get(position - 1).getMonth() - 1]);
//                    holder.qtyTextView.setText(POSUtil.NumberFormat(reportItemList.get(position - 1).getTotalAmt()));
                    //holder.expenseTextView.setText(Double.toString(expenseReportItemViewList.get(position).getBalance()));
                } else {
                    HeaderItemView holder = (HeaderItemView) viewHolder;
                    holder.firstColumn.setText(ThemeUtil.getString(context, R.attr.month));
                    holder.secondColumn.setText(ThemeUtil.getString(context, R.attr.amount));
                }

                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_TYPE;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return 13;
    }

    public List<ReportItem> getReportItemList() {
        return reportItemList;
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public class ReportItemViewHolder extends RecyclerView.ViewHolder {
        TextView monthTextView;
        TextView qtyTextView;
        //TextView expenseTextView;

        public ReportItemViewHolder(View itemView) {
            super(itemView);

            monthTextView = (TextView) itemView.findViewById(R.id.month_text_view);
            qtyTextView = (TextView) itemView.findViewById(R.id.qty_text_view);

            //expenseTextView = (TextView) itemView.findViewById(R.id.expense_text_view);

        }

    }

    public class HeaderItemView extends RecyclerView.ViewHolder {
        TextView firstColumn;
        TextView secondColumn;

        HeaderItemView(View itemView) {
            super(itemView);

            firstColumn = itemView.findViewById(R.id.first_column);
            secondColumn = itemView.findViewById(R.id.second_column);
        }

    }

    private double getTotalAmount(int position) {
        for (ReportItem reportItem : reportItemList) {
            if (AppConstant.MONTHS[reportItem.getMonth() - 1].equals(months[position - 1])) {
                return reportItem.getTotalAmt();
            }
        }
        return 0;
    }

    public void sortMonths(Boolean isLast12Months){
        if (isLast12Months) {
            months = new String[12];
            Calendar cal = Calendar.getInstance();
            int currentMonth = cal.get(Calendar.MONTH);

            for (int i = 0; i < 12; i++) {
                months[i] = AppConstant.MONTHS[currentMonth--];
                if (currentMonth < 0) {
                    currentMonth = 11;
                }
            }
            Log.e("Debugging", Arrays.toString(months));
        } else {
            months = AppConstant.MONTHS;
        }
    }
    public enum Type { MONTH_TRANSACTION, MONTH_AMOUNT }
}
