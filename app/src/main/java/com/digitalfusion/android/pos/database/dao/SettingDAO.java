package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.digitalfusion.android.pos.database.model.Currency;
import com.digitalfusion.android.pos.database.model.BusinessSetting;
import com.digitalfusion.android.pos.database.model.Discount;
import com.digitalfusion.android.pos.database.model.Tax;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/24/16.
 */
public class SettingDAO extends ParentDAO {
    private static ParentDAO settingDaoInstance;
    private Context context;
    private BusinessSetting businessSetting;
    private String currency;
    private IdGeneratorDAO idGeneratorDAO;
    private List<Tax> taxList;
    private List<Discount> discountList;
    private Discount discount;
    private Tax tax;
    private Double taxRate;

    private SettingDAO(Context context) {
        super(context);
        this.context = context;
        businessSetting = new BusinessSetting();
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        tax = new Tax();
    }

    public static SettingDAO getSettingDaoInstance(Context context) {
        if (settingDaoInstance == null) {
            settingDaoInstance = new SettingDAO(context);
        }
        return (SettingDAO) settingDaoInstance;
    }

    public boolean addBusinessSetting(String businessName, String phoneNo, String email, String website, String street, String valuationMethod, Long currencyID, byte[] logo, String state, String city, String township) {
        genID = idGeneratorDAO.getLastIdValue("BusinessSettng");
        genID++;
        query = "insert into " + AppConstant.BUSINESS_SETTING_TABLE_NAME + "(" + AppConstant.BUSINESS_SETTING_BUSINESS_NAME + ", " + AppConstant.BUSINESS_SETTING_PHONE_NUM + ", " +
                AppConstant.BUSINESS_SETTING_EMAIL + ", " + AppConstant.BUSINESS_SETTING_WEBSITE + ", " + AppConstant.BUSINESS_SETTING_STREET + ", " + AppConstant.BUSINESS_SETTING_VALUATION_METHOD + ", "
                + AppConstant.BUSINESS_SETTING_CURRENCY_ID + ", " + AppConstant.BUSINESS_SETTING_LOGO + ", " + AppConstant.CREATED_DATE + ", " + AppConstant.BUSINESS_SETTING_ID + ", " + AppConstant.BUSINESS_SETTING_CITY + ", " +
                AppConstant.BUSINESS_SETTING_TOWNSHIP + ", " + AppConstant.BUSINESS_SETTING_STATE + ") VALUES(?,?,?,?,?,?,?," + logo + ",?," + genID + ", ?, ?, ?);";
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{businessName, phoneNo, email, website, street, valuationMethod, currencyID.toString(), DateUtility.getTodayDate(), city, township, state});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean updateBusinessSetting(String businessName, String userName, String bussinessType, String phoneNo, String email, String website, String street, String valuationMethod, Long currencyID, byte[] logo, String township, String city, String state) {
        query = "update " + AppConstant.BUSINESS_SETTING_TABLE_NAME + " set " + AppConstant.BUSINESS_SETTING_BUSINESS_NAME + "=?, " + AppConstant.BUSINESS_SETTING_PHONE_NUM + "=?, " +
                AppConstant.BUSINESS_SETTING_EMAIL + "=?, " + AppConstant.BUSINESS_SETTING_WEBSITE + "=?, " + AppConstant.BUSINESS_SETTING_STREET + "=?, " + AppConstant.BUSINESS_SETTING_VALUATION_METHOD + "=?,"
                + AppConstant.BUSINESS_SETTING_CURRENCY_ID + " =?, " + AppConstant.BUSINESS_SETTING_LOGO + "= ?, " + AppConstant.BUSINESS_SETTING_TOWNSHIP + " = ?, " + AppConstant.BUSINESS_SETTING_CITY + " = ?, " +
                AppConstant.BUSINESS_SETTING_STATE + " = ?, " + AppConstant.BUSINESS_SETTING_CUSTOM_FIELD_1 + " = ?, " + AppConstant.BUSINESS_SETTING_CUSTOM_FIELD_2 + " = ? " + " where " + AppConstant.BUSINESS_SETTING_ID + "=" + 1;
        databaseWriteTransaction(flag);
        try {
            //database.execSQL(query, new String[]{businessName,phoneNo,email,website,address,valuationMethod,currency});
            SQLiteStatement sqLiteStatement = database.compileStatement(query);

            sqLiteStatement.clearBindings();

            sqLiteStatement.bindString(1, businessName);

            sqLiteStatement.bindString(2, phoneNo);

            sqLiteStatement.bindString(3, email);

            sqLiteStatement.bindString(4, website);

            sqLiteStatement.bindString(5, street);

            sqLiteStatement.bindString(6, valuationMethod);

            sqLiteStatement.bindLong(7, currencyID);

            if (logo != null) {
                sqLiteStatement.bindBlob(8, logo);

            }

            if (!township.equalsIgnoreCase(null)) {
                sqLiteStatement.bindString(9, township);
            } else {
                sqLiteStatement.bindNull(9);
            }
            if (!city.equalsIgnoreCase(null)) {
                sqLiteStatement.bindString(10, city);
            } else {
                sqLiteStatement.bindNull(10);
            }
            if (!state.equalsIgnoreCase(null)) {
                sqLiteStatement.bindString(11, state);
            } else {
                sqLiteStatement.bindNull(11);
            }

            if (!bussinessType.equalsIgnoreCase(null)) {
                sqLiteStatement.bindString(12, bussinessType);
            } else {
                sqLiteStatement.bindNull(12);
            }

            if (!userName.equalsIgnoreCase(null)) {
                sqLiteStatement.bindString(13, userName);
            } else {
                sqLiteStatement.bindNull(13);
            }
            sqLiteStatement.executeInsert();

            sqLiteStatement.clearBindings();

            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }


    public boolean updateBusinessSetting(String businessName, String userName, String businessType, String phoneNo, String city, String state) {
        query = "update " + AppConstant.BUSINESS_SETTING_TABLE_NAME + " set " + AppConstant.BUSINESS_SETTING_BUSINESS_NAME + "=?, " + AppConstant.BUSINESS_SETTING_PHONE_NUM + "=?, "
                + AppConstant.BUSINESS_SETTING_TOWNSHIP + " = ?, " +
                AppConstant.BUSINESS_SETTING_STATE + " = ?, " + AppConstant.BUSINESS_SETTING_CUSTOM_FIELD_1 + " = ?, " + AppConstant.BUSINESS_SETTING_CUSTOM_FIELD_2 + " = ? " + " where " + AppConstant.BUSINESS_SETTING_ID + "=" + 1;
        databaseWriteTransaction(flag);
        try {
            //database.execSQL(query, new String[]{businessName,phoneNo,email,website,address,valuationMethod,currency});
            SQLiteStatement sqLiteStatement = database.compileStatement(query);

            sqLiteStatement.clearBindings();

            sqLiteStatement.bindString(1, businessName);

            sqLiteStatement.bindString(2, phoneNo);

            if (!city.equalsIgnoreCase(null)) {
                sqLiteStatement.bindString(3, city);
            } else {
                sqLiteStatement.bindNull(3);
            }
            if (!state.equalsIgnoreCase(null)) {
                sqLiteStatement.bindString(4, state);
            } else {
                sqLiteStatement.bindNull(4);
            }

            if (!businessType.equalsIgnoreCase(null)) {
                sqLiteStatement.bindString(5, businessType);
            } else {
                sqLiteStatement.bindNull(5);
            }
            if (!userName.equalsIgnoreCase(null)) {
                sqLiteStatement.bindString(6, userName);
            } else {
                sqLiteStatement.bindNull(6);
            }
            sqLiteStatement.executeInsert();

            sqLiteStatement.clearBindings();

            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public Currency getCurrency() {
        Currency currency = new Currency();
        query = "select " + AppConstant.CURRENCY_NAME + ", " + AppConstant.CUSTOMER_ID + " from " + AppConstant.CURRENCY_TABLE_NAME + " where " + AppConstant.CURRENCY_IS_DEFAULT + " = 1";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            currency.setDisplayName(cursor.getString(0));
            currency.setId(cursor.getLong(1));
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return currency;
    }

    public BusinessSetting getBusinessSetting() {
        query = "select b." + AppConstant.BUSINESS_SETTING_ID + ", " + AppConstant.BUSINESS_SETTING_BUSINESS_NAME + ", " + AppConstant.BUSINESS_SETTING_PHONE_NUM + ", " +
                AppConstant.BUSINESS_SETTING_EMAIL + ", " + AppConstant.BUSINESS_SETTING_WEBSITE + ", " + AppConstant.BUSINESS_SETTING_STREET + ", " + AppConstant.BUSINESS_SETTING_VALUATION_METHOD
                + ", " + AppConstant.BUSINESS_SETTING_CURRENCY_ID + ", " + AppConstant.CURRENCY_NAME + ", " + AppConstant.BUSINESS_SETTING_LOGO + ", c." + AppConstant.CURRENCY_SYMBOL
                + ", " + AppConstant.BUSINESS_SETTING_TOWNSHIP + ", " + AppConstant.BUSINESS_SETTING_CITY + ", " + AppConstant.BUSINESS_SETTING_STATE + "," + AppConstant.BUSINESS_SETTING_STATE + ",b." + AppConstant.BUSINESS_SETTING_CUSTOM_FIELD_1
                + ",b." + AppConstant.BUSINESS_SETTING_CUSTOM_FIELD_2 + " from " + AppConstant.BUSINESS_SETTING_TABLE_NAME +
                " b, " + AppConstant.CURRENCY_TABLE_NAME + " c where " + AppConstant.BUSINESS_SETTING_CURRENCY_ID + " = c." + AppConstant.CURRENCY_ID;
        database = databaseHelper.getReadableDatabase();
        database.beginTransaction();
        cursor = database.rawQuery(query, null);
        try {
            if (cursor.moveToFirst()) {
                businessSetting.setId(cursor.getLong(0));
                businessSetting.setBusinessName(cursor.getString(1));
                businessSetting.setPhoneNo(cursor.getString(2));
                businessSetting.setEmail(cursor.getString(3));
                businessSetting.setWebsite(cursor.getString(4));
                businessSetting.setStreet(cursor.getString(5));
                businessSetting.setValuationMethod(cursor.getString(6));
                businessSetting.setCurrencyID(cursor.getLong(7));
                businessSetting.setCurrency(cursor.getString(8));
                businessSetting.setLogo(cursor.getBlob(9));
                businessSetting.setSign(cursor.getString(10));
                businessSetting.setTownship(cursor.getString(11));
                businessSetting.setCity(cursor.getString(12));
                businessSetting.setState(cursor.getString(13));
                businessSetting.setBusinessType(cursor.getString(15));
                businessSetting.setUserName(cursor.getString(16));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            cursor.close();
        }
        return businessSetting;
    }

    public Long addNewTax(String name, Double rate, String type, String description, Integer isDefault) {
        genID = idGeneratorDAO.getLastIdValue("Tax");
        genID += 1;
        query = "insert into " + AppConstant.TAX_TABLE_NAME + "(" + AppConstant.TAX_ID + ", " + AppConstant.TAX_NAME + ", " + AppConstant.TAX_RATE + ", " + AppConstant.TAX_TYPE +
                ", " + AppConstant.TAX_DESCRIPTION + ", " + AppConstant.TAX_IS_DEFAULT + ", " + AppConstant.CREATED_DATE + ") values (?,?,?,?,?,?,?);";
        databaseWriteTransaction(flag);
        try {
            statement = database.compileStatement(query);
            statement.bindLong(1, genID);
            statement.bindString(2, name);
            statement.bindDouble(3, rate);
            statement.bindString(4, type);
            if (description == null) {
                statement.bindNull(5);
            } else {
                statement.bindString(5, description);
            }
            statement.bindString(6, Integer.toString(isDefault));
            statement.bindString(7, DateUtility.getTodayDate());
            statement.execute();
            statement.clearBindings();
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return genID;
    }

    public Long addNewDiscount(String name, Double amount, Integer isPercent, String description, Integer isDefault) {
        genID = idGeneratorDAO.getLastIdValue("Discount");
        genID += 1;
        Log.e("geniD", genID.toString());
        query = "insert into " + AppConstant.DISCOUNT_TABLE_NAME + "(" + AppConstant.DISCOUNT_ID + ", " + AppConstant.DISCOUNT_NAME + ", " + AppConstant.DISCOUNT_AMOUNT + ", " + AppConstant.DISCOUNT_IS_PERCENT +
                ", " + AppConstant.DISCOUNT_DESCRIPTION + ", " + AppConstant.DISCOUNT_IS_DEFAULT + ", " + AppConstant.CREATED_DATE + ") values (?,?,?,?,?,?,?);";
        databaseWriteTransaction(flag);
        try {
            statement = database.compileStatement(query);
            statement.bindLong(1, genID);
            statement.bindString(2, name);
            statement.bindDouble(3, amount);
            statement.bindString(4, Integer.toString(isPercent));
            if (description == null) {
                statement.bindNull(5);
            } else {
                statement.bindString(5, description);
            }
            statement.bindString(6, Integer.toString(isDefault));
            statement.bindString(7, DateUtility.getTodayDate());
            statement.execute();
            statement.clearBindings();
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return genID;
    }

    public List<Discount> getAllDiscounts() {
        discountList = new ArrayList<>();
        query = "select " + AppConstant.DISCOUNT_ID + ", " + AppConstant.DISCOUNT_NAME + ", " + AppConstant.DISCOUNT_AMOUNT + ", " + AppConstant.DISCOUNT_IS_PERCENT +
                ", " + AppConstant.DISCOUNT_DESCRIPTION + ", " + AppConstant.DISCOUNT_IS_DEFAULT + " from " + AppConstant.DISCOUNT_TABLE_NAME + " order by " + AppConstant.DISCOUNT_IS_PERCENT + " desc";

        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    discount = new Discount();
                    discount.setId(cursor.getLong(0));
                    discount.setName(cursor.getString(1));
                    discount.setAmount(cursor.getDouble(2));
                    discount.setIsPercent(cursor.getInt(3));
                    discount.setDescription(cursor.getString(4));
                    discount.setIsDefault(cursor.getInt(5));
                    discountList.add(discount);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            cursor.close();
        }
        return discountList;
    }

    public Discount getDefaultDiscount() {
        discount = new Discount();
        query = "select " + AppConstant.DISCOUNT_ID + ", " + AppConstant.DISCOUNT_NAME + ", " + AppConstant.DISCOUNT_AMOUNT + ", " + AppConstant.DISCOUNT_IS_PERCENT +
                ", " + AppConstant.DISCOUNT_DESCRIPTION + ", " + AppConstant.DISCOUNT_IS_DEFAULT + " from " + AppConstant.DISCOUNT_TABLE_NAME + " where " + AppConstant.DISCOUNT_IS_DEFAULT + " = " + 1;

        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                discount.setId(cursor.getLong(0));
                discount.setName(cursor.getString(1));
                discount.setAmount(cursor.getDouble(2));
                discount.setIsPercent(cursor.getInt(3));
                discount.setDescription(cursor.getString(4));
                discount.setIsDefault(cursor.getInt(5));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            cursor.close();
        }
        return discount;
    }

    public boolean updateDiscount(String name, Double rate, int isPercent, String description, Integer isDefault, Long id) {
        query = "update " + AppConstant.DISCOUNT_TABLE_NAME + " set " + AppConstant.DISCOUNT_NAME + "=?, " + AppConstant.DISCOUNT_AMOUNT + "= " + rate + ", " + AppConstant.DISCOUNT_IS_PERCENT +
                " = " + isPercent + " , " + AppConstant.DISCOUNT_DESCRIPTION + "=?, " + AppConstant.DISCOUNT_IS_DEFAULT + "= " + isDefault + " where " + AppConstant.DISCOUNT_ID + "= " + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{name, description});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean deleteDiscount(Long id) {
        databaseWriteTransaction();
        try {
            count = database.delete(AppConstant.DISCOUNT_TABLE_NAME, AppConstant.DISCOUNT_ID + " = ?", new String[]{id.toString()});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return count > 0;
    }

    public boolean checkDiscountNameAlreadyExists(String name) {
        flag.setInserted(false);
        query = "select " + AppConstant.DISCOUNT_ID + " from " + AppConstant.DISCOUNT_TABLE_NAME + " where " + AppConstant.DISCOUNT_NAME + "=?";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{name});
            if (cursor.moveToFirst()) {
                flag.setInserted(true);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            cursor.close();
        }
        return flag.isInserted();
    }

    public Double getTaxRateByID(Long id) {
        databaseWriteTransaction();
        query = "select " + AppConstant.TAX_RATE + " from " + AppConstant.TAX_TABLE_NAME + " where " + AppConstant.TAX_ID + "=" + id;
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                taxRate = cursor.getDouble(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            cursor.close();
        }
        return taxRate;
    }

    public boolean checkTaxNameAlreadyExists(String name) {
        flag.setInserted(false);
        query = "select " + AppConstant.TAX_ID + " from " + AppConstant.TAX_TABLE_NAME + " where " + AppConstant.TAX_NAME + "=?";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{name});
            if (cursor.moveToFirst()) {
                flag.setInserted(true);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            cursor.close();
        }
        return flag.isInserted();
    }

    public List<Tax> getAllTaxs() {
        taxList = new ArrayList<>();
        query = "select " + AppConstant.TAX_ID + ", " + AppConstant.TAX_NAME + ", " + AppConstant.TAX_RATE + ", " + AppConstant.TAX_TYPE +
                ", " + AppConstant.TAX_DESCRIPTION + ", " + AppConstant.TAX_IS_DEFAULT + " from " + AppConstant.TAX_TABLE_NAME;

        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    tax = new Tax();
                    tax.setId(cursor.getLong(0));
                    tax.setName(cursor.getString(1));
                    tax.setRate(cursor.getDouble(2));
                    tax.setType(cursor.getString(3));
                    tax.setDescription(cursor.getString(4));
                    tax.setIsDefault(cursor.getInt(5));
                    taxList.add(tax);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            cursor.close();
        }
        return taxList;
    }

    public Tax getDefaultTax() {
        tax = new Tax();
        query = "select " + AppConstant.TAX_ID + ", " + AppConstant.TAX_NAME + ", " + AppConstant.TAX_RATE + ", " + AppConstant.TAX_TYPE +
                ", " + AppConstant.TAX_DESCRIPTION + ", " + AppConstant.TAX_IS_DEFAULT + " from " + AppConstant.TAX_TABLE_NAME + " where " + AppConstant.TAX_IS_DEFAULT + " = " + 1;

        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                tax.setId(cursor.getLong(0));
                tax.setName(cursor.getString(1));
                tax.setRate(cursor.getDouble(2));
                tax.setType(cursor.getString(3));
                tax.setDescription(cursor.getString(4));
                tax.setIsDefault(cursor.getInt(5));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            cursor.close();
        }
        return tax;
    }

    public boolean updateTax(String name, Double rate, String type, String description, Integer isDefault, Long id) {
        query = "update " + AppConstant.TAX_TABLE_NAME + " set " + AppConstant.TAX_NAME + "=?, " + AppConstant.TAX_RATE + "= " + rate + ", " + AppConstant.TAX_TYPE +
                "=?, " + AppConstant.TAX_DESCRIPTION + "=?, " + AppConstant.TAX_IS_DEFAULT + "= " + isDefault + " where " + AppConstant.TAX_ID + "= " + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{name, type, description});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean deleteTax(Long id) {
        databaseWriteTransaction();
        try {
            count = database.delete(AppConstant.TAX_TABLE_NAME, AppConstant.TAX_ID + " = ?", new String[]{id.toString()});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return count > 0;
    }
}

/*public boolean addBusinesSettingBind(String businessName, String phoneNo, String email, String website, String address, String valuationMethod, Long currencyID, byte[] logo) {
        genID = idGeneratorDAO.getLastIdValue("BusinessSettng");
        genID++;
        query = "insert into " + AppConstant.BUSINESS_SETTING_TABLE_NAME + "(" + AppConstant.BUSINESS_SETTING_BUSINESS_NAME + ", " + AppConstant.BUSINESS_SETTING_PHONE_NUM + ", " +
                AppConstant.BUSINESS_SETTING_EMAIL + ", " + AppConstant.BUSINESS_SETTING_WEBSITE + ", " + AppConstant.BUSINESS_SETTING_ADDRESS + ", " + AppConstant.BUSINESS_SETTING_VALUATION_METHOD + ", "
                + AppConstant.BUSINESS_SETTING_CURRENCY_ID + ", " + AppConstant.BUSINESS_SETTING_LOGO + ", " + AppConstant.CREATED_DATE + ", " + AppConstant.BUSINESS_SETTING_ID + ") VALUES(?,?,?,?,?,?,?,?,?,?);";
        databaseWriteTransaction(flag);

        Log.w("hello byte", logo + " ss");
        try {
            SQLiteStatement sqLiteStatement = database.compileStatement(query);
            sqLiteStatement.clearBindings();

            sqLiteStatement.bindString(1, businessName);
            sqLiteStatement.bindString(2, phoneNo);
            sqLiteStatement.bindString(3, email);
            sqLiteStatement.bindString(4, website);
            sqLiteStatement.bindString(5, address);
            sqLiteStatement.bindString(6, valuationMethod);
            sqLiteStatement.bindLong(7, currencyID);
            sqLiteStatement.bindBlob(8, logo);
            sqLiteStatement.bindString(9, DateUtility.getTodayDate());
            sqLiteStatement.bindLong(10, genID);

            sqLiteStatement.execute();
            sqLiteStatement.clearBindings();
            //  database.execSQL(query, new String[] {businessName, phoneNo, email, website, address, valuationMethod, currency, DateUtility.getTodayDate()});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }*/
