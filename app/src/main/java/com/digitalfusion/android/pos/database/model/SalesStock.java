package com.digitalfusion.android.pos.database.model;

import java.io.Serializable;

/**
 * Created by MD002 on 9/12/16.
 */
public class SalesStock implements Serializable {
    private Long stockID;
    private int stockQty;

    public SalesStock() {
    }

    public Long getStockID() {

        return stockID;
    }

    public void setStockID(Long stockID) {
        this.stockID = stockID;
    }

    public int getStockQty() {
        return stockQty;
    }

    public void setStockQty(int stockQty) {
        this.stockQty = stockQty;
    }
}
