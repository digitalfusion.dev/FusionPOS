package com.digitalfusion.android.pos.database.model;

import android.graphics.drawable.Drawable;

/**
 * Created by MD001 on 3/6/17.
 */

public class ReportTitleItem {
    private Drawable image;
    private String titleTxt;

    public ReportTitleItem() {
    }

    public ReportTitleItem(Drawable image, String titleTxt) {
        this.image = image;
        this.titleTxt = titleTxt;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getTitleTxt() {
        return titleTxt;
    }

    public void setTitleTxt(String titleTxt) {
        this.titleTxt = titleTxt;
    }
}
