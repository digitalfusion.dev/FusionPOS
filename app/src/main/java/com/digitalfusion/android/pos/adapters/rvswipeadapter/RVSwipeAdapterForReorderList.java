package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD001 on 1/3/17.
 */

public class RVSwipeAdapterForReorderList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    protected boolean showLoader = false;
    private List<StockItem> stockItemList;
    private ClickListener reoderClickListener;
    private LoaderViewHolder loaderViewHolder;

    public RVSwipeAdapterForReorderList(List<StockItem> stockItemList) {

        this.stockItemList = stockItemList;

    }

    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            if (stockItemList != null && stockItemList.size() != 0) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;

            }

        }

        return VIEWTYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEWTYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_item_view_in_reorder_list, parent, false);


            return new StockItemViewHolder(v);

        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            return new LoaderViewHolder(v);
        }


    }

    public List<StockItem> getStockItemList() {
        return stockItemList;
    }

    public void setStockItemList(List<StockItem> stockItemList) {
        this.stockItemList = stockItemList;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {


        if (viewHolder instanceof LoaderViewHolder) {

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;

            this.loaderViewHolder = loaderViewHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);

        }
        if (viewHolder instanceof StockItemViewHolder) {

            final StockItemViewHolder holder = (StockItemViewHolder) viewHolder;

            // holder.noTextView.setText(Integer.toString(position+1));

            POSUtil.makeZebraStrip(holder.itemView, position);

            holder.swipeLayout.setSwipeEnabled(false);

            holder.stockCodeTextView.setText(stockItemList.get(position).getCodeNo());
            holder.itemNameTextView.setText(stockItemList.get(position).getName());
            holder.categoryTextView.setText(stockItemList.get(position).getCategoryName());

            String unit = "";

            if (stockItemList.get(position).getUnitName() != null && stockItemList.get(position).getUnitName().length() > 0) {
                unit = " " + stockItemList.get(position).getUnitName();
            }

            holder.inventoryQtyTextView.setText(Integer.toString(stockItemList.get(position).getInventoryQty()) + unit);

            holder.totalValueTextView.setText(POSUtil.NumberFormat(stockItemList.get(position).getPurchasePrice()));

            // holder.totalValueTextView.setText(POSUtil.NumberFormat(10000000000.0));


            //   if(stockItemList.get(position).getUnitName()==null||stockItemList.get(position).getUnitID()==null||stockItemList.get(position).getUnitID()==0){
            //                holder.unitTextView.setText("");
            //     }else {
            //        holder.unitTextView.setText(stockItemList.get(position).getUnitName());
            //     }

            holder.reorderImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (reoderClickListener != null) {
                        reoderClickListener.onClick(position);
                    }
                }
            });


            //  POSUtil.makeZebraStrip(holder.itemView,position);

            mItemManger.bindView(holder.itemView, position);

        } else {
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }

    }


    public ClickListener getReoderClickListener() {
        return reoderClickListener;
    }

    public void setReoderClickListener(ClickListener reoderClickListener) {
        this.reoderClickListener = reoderClickListener;
    }

    @Override
    public int getItemCount() {
        if (stockItemList == null || stockItemList.size() == 0) {
            return 0;
        } else {
            return stockItemList.size() + 1;
        }

    }

    public void setShowLoader(boolean showLoader) {
        this.showLoader = showLoader;

        if (loaderViewHolder != null) {
            Log.w("hele", "loader full not null");
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe2;
    }

    public class StockItemViewHolder extends RecyclerView.ViewHolder {
        TextView stockCodeTextView;
        TextView itemNameTextView;
        TextView categoryTextView;
        TextView inventoryQtyTextView;
        TextView totalValueTextView;
        TextView noTextView;

        ImageButton reorderImageButton;

        SwipeLayout swipeLayout;


        LinearLayout linearLayout;

        View itemView;

        public StockItemViewHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe2);

            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll);

            noTextView = (TextView) itemView.findViewById(R.id.no);


            reorderImageButton = (ImageButton) itemView.findViewById(R.id.reorder);

            stockCodeTextView = (TextView) itemView.findViewById(R.id.stock_code_in_stock_item_view);
            itemNameTextView = (TextView) itemView.findViewById(R.id.item_name_in_stock_item_view);
            categoryTextView = (TextView) itemView.findViewById(R.id.category_in_stock_item_view);
            inventoryQtyTextView = (TextView) itemView.findViewById(R.id.qty_in_stock_item_view);
            totalValueTextView = (TextView) itemView.findViewById(R.id.total_value_in_stock_item_view);


        }
    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar mProgressBar;

        public View mView;

        public LoaderViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);

        }
    }

}