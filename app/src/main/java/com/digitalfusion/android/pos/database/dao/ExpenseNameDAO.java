package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import com.digitalfusion.android.pos.database.model.ExpenseName;
import com.digitalfusion.android.pos.database.model.ExpenseIncome;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 9/6/16.
 */
public class ExpenseNameDAO extends ParentDAO {
    private static ParentDAO expenseNameDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private List<ExpenseName> expenseNameList;
    private ExpenseName expenseName;

    private ExpenseNameDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
    }

    public static ExpenseNameDAO getExpenseNameDaoInstance(Context context) {
        if (expenseNameDaoInstance == null) {
            expenseNameDaoInstance = new ExpenseNameDAO(context);
        }
        return (ExpenseNameDAO) expenseNameDaoInstance;
    }

    public Long addNewExpenseOrIncomeName(String name, String type) {
        genID = idGeneratorDAO.getLastIdValue("ExpenseName");
        genID++;
        contentValues.put(AppConstant.EXPENSE_NAME_ID, genID);
        contentValues.put(AppConstant.EXPENSE_NAME_NAME, name);
        contentValues.put(AppConstant.EXPENSE_NAME_TYPE, type);
        contentValues.put(AppConstant.CREATED_DATE, DateUtility.getTodayDate());
        databaseWriteTransaction(flag);
        try {
            id = database.insert(AppConstant.EXPENSE_NAME_TABLE_NAME, null, contentValues);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return id;
    }

    public Long checkNameAndTypeAlreadyExists(String name, String type) {
        id = null;
        query = "select " + AppConstant.EXPENSE_NAME_ID + " from " + AppConstant.EXPENSE_NAME_TABLE_NAME + " where " + AppConstant.EXPENSE_NAME_NAME + " = ? and "
                + AppConstant.EXPENSE_NAME_TYPE + " = ?";
        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{name, type});
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }

    public List<ExpenseName> getAllExpenseOrIncomeNames() {
        expenseNameList = new ArrayList<>();
        query = "select " + AppConstant.EXPENSE_NAME_ID + ", " + AppConstant.EXPENSE_NAME_NAME + ", "
                + AppConstant.EXPENSE_NAME_TYPE + " from " + AppConstant.EXPENSE_NAME_TABLE_NAME;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    expenseName = new ExpenseName();
                    expenseName.setId(cursor.getLong(0));
                    expenseName.setName(cursor.getString(1));
                    expenseName.setType(cursor.getString(2));
                    expenseNameList.add(expenseName);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return expenseNameList;
    }

    public List<ExpenseName> getAllIncomeNames() {
        expenseNameList = new ArrayList<>();
        query = "select " + AppConstant.EXPENSE_NAME_ID + ", " + AppConstant.EXPENSE_NAME_NAME + ", "
                + AppConstant.EXPENSE_NAME_TYPE + " from " + AppConstant.EXPENSE_NAME_TABLE_NAME + " where " + AppConstant.EXPENSE_NAME_TYPE + " = ?";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{ExpenseIncome.Type.Income.toString()});
            if (cursor.moveToFirst()) {
                do {
                    expenseName = new ExpenseName();
                    expenseName.setId(cursor.getLong(0));
                    expenseName.setName(cursor.getString(1));
                    expenseName.setType(cursor.getString(2));
                    expenseNameList.add(expenseName);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return expenseNameList;
    }

    public List<ExpenseName> incomeNamesSearch(int startLimit, int endLimit, String queryText) {
        expenseNameList = new ArrayList<>();
        query = "select " + AppConstant.EXPENSE_NAME_ID + ", " + AppConstant.EXPENSE_NAME_NAME + ", "
                + AppConstant.EXPENSE_NAME_TYPE + " from " + AppConstant.EXPENSE_NAME_TABLE_NAME + " where " + AppConstant.EXPENSE_NAME_TYPE + " = ? and " + AppConstant.EXPENSE_NAME_ID + " <> 1 and " + AppConstant.EXPENSE_NAME_NAME + " like ? ||'%' " + "  limit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{ExpenseIncome.Type.Income.toString(), queryText});
            if (cursor.moveToFirst()) {
                do {
                    expenseName = new ExpenseName();
                    expenseName.setId(cursor.getLong(0));
                    expenseName.setName(cursor.getString(1));
                    expenseName.setType(cursor.getString(2));
                    expenseNameList.add(expenseName);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return expenseNameList;
    }

    public List<ExpenseName> expenseNamesSearch(int startLimit, int endLimit, String queryText) {
        expenseNameList = new ArrayList<>();
        query = "select " + AppConstant.EXPENSE_NAME_ID + ", " + AppConstant.EXPENSE_NAME_NAME + ", "
                + AppConstant.EXPENSE_NAME_TYPE + " from " + AppConstant.EXPENSE_NAME_TABLE_NAME + " where " + AppConstant.EXPENSE_NAME_TYPE + " = ? and " + AppConstant.EXPENSE_NAME_NAME + " like ? ||'%' " + "  limit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{ExpenseIncome.Type.Expense.toString(), queryText});
            if (cursor.moveToFirst()) {
                do {
                    expenseName = new ExpenseName();
                    expenseName.setId(cursor.getLong(0));
                    expenseName.setName(cursor.getString(1));
                    expenseName.setType(cursor.getString(2));
                    expenseNameList.add(expenseName);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return expenseNameList;
    }

    public List<ExpenseName> getAllExpenseNames() {
        expenseNameList = new ArrayList<>();
        query = "select " + AppConstant.EXPENSE_NAME_ID + ", " + AppConstant.EXPENSE_NAME_NAME + ", "
                + AppConstant.EXPENSE_NAME_TYPE + " from " + AppConstant.EXPENSE_NAME_TABLE_NAME + " where " + AppConstant.EXPENSE_NAME_TYPE + " = ?";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{ExpenseIncome.Type.Expense.toString()});
            if (cursor.moveToFirst()) {
                do {
                    expenseName = new ExpenseName();
                    expenseName.setId(cursor.getLong(0));
                    expenseName.setName(cursor.getString(1));
                    expenseName.setType(cursor.getString(2));
                    expenseNameList.add(expenseName);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return expenseNameList;
    }

    public ArrayList<ExpenseName> getAllExpenseOrIncomeNameSearch(String name, int startLimit, int endLimit) {
        ArrayList expenseList = new ArrayList<>();
        query = "select " + AppConstant.EXPENSE_NAME_ID + ", " + AppConstant.EXPENSE_NAME_NAME + ", "
                + AppConstant.EXPENSE_NAME_TYPE + " from " + AppConstant.EXPENSE_NAME_TABLE_NAME + " where " + AppConstant.EXPENSE_NAME_NAME + " = ? limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{name});
            if (cursor.moveToFirst()) {
                do {
                    expenseName = new ExpenseName();
                    expenseName.setId(cursor.getLong(0));
                    expenseName.setName(cursor.getString(1));
                    expenseName.setType(cursor.getString(2));
                    expenseNameList.add(expenseName);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return expenseList;
    }
}
