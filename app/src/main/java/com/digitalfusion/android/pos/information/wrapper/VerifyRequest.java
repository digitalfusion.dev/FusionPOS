package com.digitalfusion.android.pos.information.wrapper;

public class VerifyRequest {
    private String code;
    private String userId;
    private String phone;

    public VerifyRequest(String code, String userId, String phone) {
        this.code = code;
        this.userId = userId;
        this.phone = phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "VerifyRequest{" +
                "code='" + code + '\'' +
                ", userId='" + userId + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
