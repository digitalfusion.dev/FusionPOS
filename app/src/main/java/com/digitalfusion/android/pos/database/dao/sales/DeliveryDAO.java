package com.digitalfusion.android.pos.database.dao.sales;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.dao.ParentDAO;
import com.digitalfusion.android.pos.database.dao.IdGeneratorDAO;
import com.digitalfusion.android.pos.database.model.SaleDelivery;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

/**
 * Created by MD002 on 8/30/16.
 */
public class DeliveryDAO extends ParentDAO {

    private static ParentDAO deliveryDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private SaleDelivery saleDelivery;


    private DeliveryDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
    }

    public static DeliveryDAO getDeliveryDaoInstance(Context context) {
        if (deliveryDaoInstance == null) {
            deliveryDaoInstance = new DeliveryDAO(context);
        }
        return (DeliveryDAO) deliveryDaoInstance;
    }

    public boolean addNewStockDelivery(String date, String phoneNo, String address, String agent, double charges, Long salesID, String day, String month, String year) {
        genID = idGeneratorDAO.getLastIdValue("Delivery");
        genID++;
        query = "insert into " + AppConstant.DELIVERY_TABLE_NAME + "(" + AppConstant.DELIVERY_ID + ", " + AppConstant.DELIVERY_DATE +
                ", " + AppConstant.DELIVERY_PHONE_NUM + ", " + AppConstant.DELIVERY_ADDRESS + ", " + AppConstant.DELIVERY_AGENT + ", " + AppConstant.DELIVERY_CHARGES + ", " +
                AppConstant.DELIVERY_IS_INCLUSIVE + ", " + AppConstant.DELIVERY_SALES_ID + ", " + AppConstant.DELIVERY_STATUS + ", "
                + AppConstant.CREATED_DATE + ", " + AppConstant.DELIVERY_DAY + ", " + AppConstant.DELIVERY_MONTH + ", " + AppConstant.DELIVERY_YEAR + ", " + AppConstant.DELIVERY_TIME +
                ") values(" + genID + ", ?, ?, ?, ?, " + charges + ", " + 0 + ", " + salesID + "," + 0 + ", ?,?,?,?, strftime('%s', ?, time('now', 'localtime')));";
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query, new String[]{date, phoneNo, address, agent, DateUtility.getTodayDate(), day, month, year, formatDateWithDash(day, month, year)});
        return true;
    }

    public SaleDelivery getDeliveryInfoBySalesID(Long salesID, InsertedBooleanHolder flag) {
        query = "select d." + AppConstant.DELIVERY_ID + ", d." + AppConstant.DELIVERY_DATE +
                ", d." + AppConstant.DELIVERY_PHONE_NUM + ", d." + AppConstant.DELIVERY_ADDRESS + ", d." + AppConstant.DELIVERY_AGENT + ", d." + AppConstant.DELIVERY_CHARGES + ", d." +
                AppConstant.DELIVERY_IS_INCLUSIVE + ", d." + AppConstant.DELIVERY_SALES_ID + ", d." + AppConstant.DELIVERY_STATUS + ", c." + AppConstant.CUSTOMER_NAME + ", s." + AppConstant.SALES_CUSTOMER_ID
                + ", d." + AppConstant.DELIVERY_DAY + ", d." + AppConstant.DELIVERY_MONTH + ", d." + AppConstant.DELIVERY_YEAR
                + " from " + AppConstant.DELIVERY_TABLE_NAME + " as d, " + AppConstant.SALES_TABLE_NAME + " as s, " + AppConstant.CUSTOMER_TABLE_NAME + " as c where d." + AppConstant.DELIVERY_SALES_ID +
                "= s." + AppConstant.SALES_ID + " and s." + AppConstant.SALES_CUSTOMER_ID + "= c." + AppConstant.CUSTOMER_ID + " and d." + AppConstant.DELIVERY_SALES_ID + "=" + salesID;
        databaseReadTransaction(flag);
        saleDelivery = new SaleDelivery();
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                saleDelivery.setId(cursor.getLong(0));
                saleDelivery.setDate(cursor.getString(1));
                saleDelivery.setPhoneNo(cursor.getString(2));
                saleDelivery.setAddress(cursor.getString(3));
                saleDelivery.setAgent(cursor.getString(4));
                saleDelivery.setCharges(cursor.getDouble(5));
                saleDelivery.setIsInclusive(cursor.getInt(6));
                saleDelivery.setSalesID(cursor.getLong(7));
                saleDelivery.setStatus(cursor.getInt(8));
                saleDelivery.setCustomerName(cursor.getString(9));
                saleDelivery.setCustomerID(cursor.getLong(10));
                saleDelivery.setDay(cursor.getString(11));
                saleDelivery.setMonth(cursor.getString(12));
                saleDelivery.setYear(cursor.getString(13));

                Log.w("here year", cursor.getString(13) + " year");
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return saleDelivery;
    }

    public boolean updateDeliveryByID(Long id, String date, String phoneNo, String address, String agent, double charges, Long salesID, String day, String month, String year, int status) {
        query = "update " + AppConstant.DELIVERY_TABLE_NAME + " set " + AppConstant.DELIVERY_DATE + "=?, " + AppConstant.DELIVERY_PHONE_NUM + "=?, " + AppConstant.DELIVERY_ADDRESS + "=?, "
                + AppConstant.DELIVERY_AGENT + "=?, " + AppConstant.DELIVERY_CHARGES + " =" + charges + ", " +
                AppConstant.DELIVERY_SALES_ID + "=" + salesID + ", " + AppConstant.DELIVERY_STATUS + "=" + status + ", "
                + AppConstant.DELIVERY_DAY + "=?, " + AppConstant.DELIVERY_MONTH + "=?, " + AppConstant.DELIVERY_YEAR + "=? where " + AppConstant.DELIVERY_ID + "=" + id;
        database = databaseHelper.getWritableDatabase();
        Log.e(day, month + " " + year);
        database.execSQL(query, new String[]{date, phoneNo, address, agent, day, month, year});
        return true;
    }

    public boolean makeOrderDelivered(Long deliveryID) {
        query = "update " + AppConstant.DELIVERY_TABLE_NAME + " set " + AppConstant.DELIVERY_STATUS + " = " + 1 + " where " + AppConstant.DELIVERY_ID + " = " + deliveryID;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean makeOrderUnDelivered(Long deliveryID) {
        query = "update " + AppConstant.DELIVERY_TABLE_NAME + " set " + AppConstant.DELIVERY_STATUS + " = " + 0 + " where " + AppConstant.DELIVERY_ID + " = " + deliveryID;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }
}
