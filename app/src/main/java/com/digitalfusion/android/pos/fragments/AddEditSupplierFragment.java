package com.digitalfusion.android.pos.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.SupplierManager;
import com.digitalfusion.android.pos.database.model.Supplier;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

/**
 * Created by MD003 on 3/8/17.
 */

public class AddEditSupplierFragment extends Fragment {


    //UI component

    public static final String KEY = "customer";
    private EditText supplierNameTextInputEditTextMd;
    private EditText supplierBusinessNameTextInputEditTextMd;
    private EditText supplierPhoneTextInputEditTextMd;
    private EditText supplierAddressTextInputEditTextMd;
    private View mainLayoutView;
    private Supplier supplier;
    private SupplierManager supplierManager;
    private boolean isEdit = false;

    private String supplierName;

    private String supplierBusinessName;

    private String supplierPhone;

    private String supplierAddress;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.new_supplier, container, false);

        context = getContext();

        supplierManager = new SupplierManager(getContext());

        setHasOptionsMenu(true);

        loadUI();

        if (getArguments().getSerializable(KEY) == null) {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_new_supplier}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
            isEdit = false;
        } else {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_supplier}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
            isEdit = true;
            supplier = (Supplier) getArguments().get(KEY);
            initializeOldData();
        }

        return mainLayoutView;

    }

    @Override
    public void onResume() {

        super.onResume();

    }

    public void getValuesFromView() {

        supplierName = supplierNameTextInputEditTextMd.getText().toString().trim();

        supplierPhone = supplierPhoneTextInputEditTextMd.getText().toString().trim();

        supplierAddress = supplierAddressTextInputEditTextMd.getText().toString().trim();

    }


    private void initializeOldData() {

        supplierName = supplier.getName();
        supplierPhone = supplier.getPhoneNo();
        supplierAddress = supplier.getAddress();

        supplierNameTextInputEditTextMd.setText(supplier.getName());
        supplierPhoneTextInputEditTextMd.setText(supplier.getPhoneNo());
        supplierAddressTextInputEditTextMd.setText(supplier.getAddress());

    }

    private void clearData() {
        supplierNameTextInputEditTextMd.setText(null);
        supplierNameTextInputEditTextMd.setError(null);
        supplierPhoneTextInputEditTextMd.setText(null);
        supplierPhoneTextInputEditTextMd.setError(null);

        supplierAddressTextInputEditTextMd.setText(null);
        supplierAddressTextInputEditTextMd.setError(null);

    }

    public boolean checkValidation() {
        boolean status = true;

        if (supplierNameTextInputEditTextMd.getText().toString().trim().length() < 1) {

            String enterSName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_supplier_name}).getString(0);
            supplierNameTextInputEditTextMd.setError(enterSName);

            status = false;

        }

        return status;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save) {

            if (!isEdit) {
                saveCategory();
            } else {

                if (checkValidation()) {

                    getValuesFromView();

                    boolean status = supplierManager.updateSupplierByID(supplierName, supplierBusinessName, supplierAddress, supplierPhone, supplier.getId());

                    if (status) {
                        getActivity().onBackPressed();
                    }

                }
            }

        }
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void saveCategory() {
        if (checkValidation()) {

            getValuesFromView();

            supplierManager.addNewSupplier(supplierName, supplierBusinessName, supplierAddress, supplierPhone, new InsertedBooleanHolder());

            clearData();
            getActivity().onBackPressed();
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);

       /* TypedArray save=context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        FontTextView fontTextView= (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.menu_text,null);

        menu.getItem(0).setActionView(fontTextView);

        fontTextView.setText(save.getString(0));

        fontTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menu.getItem(0));
            }
        });*/


        super.onCreateOptionsMenu(menu, inflater);

    }

    private void loadUI() {
        supplierNameTextInputEditTextMd = (EditText) mainLayoutView.findViewById(R.id.suppl_name_in_add_supplier_TIET);

        supplierBusinessNameTextInputEditTextMd = (EditText) mainLayoutView.findViewById(R.id.suppl_business_name_in_add_supplier_TIET);

        supplierPhoneTextInputEditTextMd = (EditText) mainLayoutView.findViewById(R.id.suppl_phone_in_add_supplier_TIET);

        supplierAddressTextInputEditTextMd = (EditText) mainLayoutView.findViewById(R.id.suppl_address_in_add_supplier_TIET);


    }
}