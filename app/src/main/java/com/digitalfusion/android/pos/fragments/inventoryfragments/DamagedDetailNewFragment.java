package com.digitalfusion.android.pos.fragments.inventoryfragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForDetailDamageValueList;
import com.digitalfusion.android.pos.database.model.DamagedItem;

/**
 * Created by MD003 on 3/6/17.
 */

public class DamagedDetailNewFragment extends Fragment {

    public static final String KEY = "DATA";
    private View mainLayout;
    private DamagedItem damagedItem;

    private TextView stockItemNameTextView;

    private TextView stockCodeTextView;

    private TextView stockCategoryTextView;

    private RecyclerView valueRecyclerView;

    private TextView descriptionTextView;

    private RVAdapterForDetailDamageValueList rvAdapterForDetailDamageValueList;

    private TextView totalQtyTextView;

    private TextView valueTextView;

    private LinearLayout GIbtn;

    private LinearLayout GILinearLayout;

    private LinearLayout Vbtn;

    private LinearLayout valueLinearLayout;

    private ImageView GIImageButton;

    private ImageView valueImageButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.damaged_detail_view, container, false);

        damagedItem = (DamagedItem) getArguments().getSerializable(KEY);

        loadUI();

        stockItemNameTextView.setText(damagedItem.getStockName());

        stockCodeTextView.setText(damagedItem.getStockCode());

        //stockCategoryTextView.setText(damagedItem.g);

        descriptionTextView.setText(damagedItem.getRemark());

        rvAdapterForDetailDamageValueList = new RVAdapterForDetailDamageValueList(damagedItem.getStockValueList(), damagedItem.getStockUnit());

        valueRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        valueRecyclerView.setAdapter(rvAdapterForDetailDamageValueList);

        totalQtyTextView.setText(damagedItem.getStockQtyString());

        valueTextView.setText(damagedItem.getTotalValueString());

        GIbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GILinearLayout.isShown()) {
                    GIImageButton.setSelected(true);
                    GILinearLayout.setVisibility(View.GONE);
                } else {
                    GIImageButton.setSelected(false);
                    GILinearLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        Vbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valueLinearLayout.isShown()) {
                    valueImageButton.setSelected(true);
                    valueLinearLayout.setVisibility(View.GONE);
                } else {
                    valueImageButton.setSelected(false);
                    valueLinearLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        return mainLayout;

    }

    private void loadUI() {

        GIImageButton         = (ImageView   ) mainLayout.findViewById(R.id.dd_btn1_arrow         );
        valueImageButton      = (ImageView   ) mainLayout.findViewById(R.id.dd_btn2_arrow         );
        valueLinearLayout     = (LinearLayout) mainLayout.findViewById(R.id.value_ll              );
        GILinearLayout        = (LinearLayout) mainLayout.findViewById(R.id.general_information_ll);
        GIbtn                 = (LinearLayout) mainLayout.findViewById(R.id.dd_btn1               );
        Vbtn                  = (LinearLayout) mainLayout.findViewById(R.id.dd_btn2               );
        stockItemNameTextView = (TextView    ) mainLayout.findViewById(R.id.stock_name            );
        stockCodeTextView     = (TextView    ) mainLayout.findViewById(R.id.code_no               );
        valueRecyclerView     = (RecyclerView) mainLayout.findViewById(R.id.rv                    );
        stockCategoryTextView = (TextView    ) mainLayout.findViewById(R.id.category_name         );
        descriptionTextView   = (TextView    ) mainLayout.findViewById(R.id.description           );
        totalQtyTextView      = (TextView    ) mainLayout.findViewById(R.id.total_qty             );
        valueTextView         = (TextView    ) mainLayout.findViewById(R.id.total_value           );

    }
}
