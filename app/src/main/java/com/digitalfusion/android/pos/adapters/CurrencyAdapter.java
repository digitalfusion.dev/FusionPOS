package com.digitalfusion.android.pos.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Currency;

import java.util.List;

/**
 * Created by MD003 on 11/17/16.
 */

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.FilterViewHolder> {


    List<Currency> filterNameList;

    private OnItemClickListener mItemClickListener;

    private RadioButton oldRb;


    public CurrencyAdapter(List<Currency> filterNameList) {
        this.filterNameList = filterNameList;

    }

    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.curreny_item_view, parent, false);

        return new FilterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FilterViewHolder holder, final int position) {

        holder.name.setText(filterNameList.get(position).getDisplayName());
        holder.sign.setText(filterNameList.get(position).getSign());
        if (position == 0) {
            oldRb = holder.rb;
            holder.rb.setChecked(true);
        } else {
            holder.rb.setChecked(false);
        }
        if (filterNameList.get(position).isActive()) {

            if (oldRb != null) {

                oldRb.setChecked(false);

            }
            oldRb = holder.rb;
            holder.rb.setChecked(true);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {

                    if (oldRb != null) {
                        oldRb.setChecked(false);
                        oldRb = holder.rb;
                        holder.rb.setChecked(true);
                    }

                    mItemClickListener.onItemClick(v, position);
                }
            }

        });
    }


    public void setCurrentPos(int pos) {

        Log.w("positon", pos + " Ss");
        if (pos > filterNameList.size() - 1) {

        } else {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(null, pos);
            }
        }
    }

    @Override
    public int getItemCount() {
        return filterNameList.size();
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class FilterViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        RadioButton rb;
        View itemView;
        TextView sign;

        public FilterViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            rb = (RadioButton) itemView.findViewById(R.id.rb);
            name = (TextView) itemView.findViewById(R.id.name);
            sign = (TextView) itemView.findViewById(R.id.sign);

        }
    }
}

