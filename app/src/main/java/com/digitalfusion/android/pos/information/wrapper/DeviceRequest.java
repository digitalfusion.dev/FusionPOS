package com.digitalfusion.android.pos.information.wrapper;

import android.os.Build;

import com.digitalfusion.android.pos.util.ConnectivityUtil;

public final class DeviceRequest {
    private String serial;
    private String macAddress;

    public DeviceRequest() {
        this.serial = Build.SERIAL;
        this.macAddress = ConnectivityUtil.getMacAddr();
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
}
