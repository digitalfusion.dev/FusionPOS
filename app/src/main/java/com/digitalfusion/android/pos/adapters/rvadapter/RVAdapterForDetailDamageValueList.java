package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.StockValue;

import java.util.List;

/**
 * Created by MD003 on 3/6/17.
 */

public class RVAdapterForDetailDamageValueList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<StockValue> valueList;
    private ClickListener editClickListener;
    private ClickListener deleteClickListener;
    private String unit = "";

    public RVAdapterForDetailDamageValueList(List<StockValue> valueList, String unit) {
        this.valueList = valueList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.damage_detail_item_view, parent, false);

        return new ItemViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ItemViewHolder) {

            StockValue stockValue = valueList.get(position);

            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            itemViewHolder.qtyTextView.setText(stockValue.getQtyString() + " " + unit);

            itemViewHolder.valueTextView.setText(stockValue.getValueString());

            itemViewHolder.totalValueTextView.setText(stockValue.getTotalValueString());

        }


    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    @Override
    public int getItemCount() {
        return valueList.size();
    }

    public List<StockValue> getValueList() {
        return valueList;
    }

    public void setValueList(List<StockValue> valueList) {
        this.valueList = valueList;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView qtyTextView;
        TextView valueTextView;
        TextView totalValueTextView;

        View view;

        public ItemViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            qtyTextView = (TextView) itemView.findViewById(R.id.qty);

            valueTextView = (TextView) itemView.findViewById(R.id.value);

            totalValueTextView = (TextView) itemView.findViewById(R.id.total_value);
        }

    }
}
