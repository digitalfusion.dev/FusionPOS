package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.fragments.CustomerListFragment;
import com.digitalfusion.android.pos.fragments.SupplierListFragment;
import com.digitalfusion.android.pos.util.TabUtil;
import com.example.searchview.MaterialSearchView;

/**
 * Created by MD003 on 11/21/16.
 */

public class CustomerSupplierTabFragment extends Fragment {

    private View mainLayoutView;

    //Tab
    private TabLayout tabLayout;

    private CustomerListFragment customerListFragment;

    private SupplierListFragment supplierListFragment;

    private MaterialSearchView searchView;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.customer_supplier_tab_fragment, null);

        context = getContext();

        setHasOptionsMenu(true);

        String contacts = context.getTheme().obtainStyledAttributes(new int[]{R.attr.contacts}).getString(0);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(contacts);

        loadUI();

        setupTabLayout();
        MainActivity.setCurrentFragment(this);
        return mainLayoutView;

    }

    private void setupTabLayout() {

        customerListFragment = new CustomerListFragment();

        supplierListFragment = new SupplierListFragment();

        MainActivity.replacingTabFragment(customerListFragment);

        String customer = context.getTheme().obtainStyledAttributes(new int[]{R.attr.customer}).getString(0);
        String supplier = context.getTheme().obtainStyledAttributes(new int[]{R.attr.supplier}).getString(0);
        tabLayout.addTab(tabLayout.newTab().setCustomView(TabUtil.renderTabView(mainLayoutView.getContext(), customer)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(TabUtil.renderTabView(mainLayoutView.getContext(), supplier)));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setCurrentTabFragment(int tabPostion) {
        switch (tabPostion) {
            case 0:
                MainActivity.replacingTabFragment(customerListFragment);

                break;
            case 1:
                MainActivity.replacingTabFragment(supplierListFragment);

                break;
        }
    }

    private void loadUIFromToolbar() {
        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        searchView.setCursorDrawable(attributeResourceId);
    }

    public void loadUI() {
        loadUIFromToolbar();
        tabLayout = (TabLayout) mainLayoutView.findViewById(R.id.tablayout);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.main, menu);

        MenuItem item = menu.findItem(R.id.action_search);

        searchView.setMenuItem(item);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }


}
