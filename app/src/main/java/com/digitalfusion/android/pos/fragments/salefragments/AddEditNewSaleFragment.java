package com.digitalfusion.android.pos.fragments.salefragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.CustomerAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockCodeAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockNameAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockNameOrCodeAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForCustomerMD;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForStockChooser;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForTaxMD;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterforStockListMaterialDialog;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForSaleItemViewInSale;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.AuthorizationManager;
import com.digitalfusion.android.pos.database.business.CustomerManager;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.database.model.Discount;
import com.digitalfusion.android.pos.database.model.SaleDelivery;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.database.model.SalesHeader;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.database.model.Stock;
import com.digitalfusion.android.pos.database.model.StockAutoComplete;
import com.digitalfusion.android.pos.database.model.StockImage;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.database.model.Tax;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.interfaces.ActionDoneListener;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.OnTrasactionFinishListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.BluetoothUtil;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.views.DiscountDialogFragment;
import com.digitalfusion.android.pos.views.FusionToast;
import com.digitalfusion.android.pos.views.IgnoableAutoCompleteTextView;
import com.digitalfusion.android.pos.views.Spinner;
import com.example.numberpad.KeyBoardView;
import com.google.zxing.client.android.CaptureActivity;
import com.innovattic.font.FontEditText;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.digitalfusion.android.pos.util.POSUtil.isTabetLand;

/**
 * Created by MD003 on 8/29/16.
 */

public class AddEditNewSaleFragment extends Fragment implements ActionDoneListener {

    private final String DEFAULT_CUSTOMER = "Default";
    private final int SCANNER_BAR_CODE = 10;

    public View mainLayout;
    PopupWindow popupWindow;
    DiscountDialogFragment discountDialogFragment;
    //UI components from main
    private EditText phoneNoEditText;
    private Button addSaletemItemBtn;
    private TextView deliveryTextView;
    private ImageView deliveryImageViewBtn;
    private RecyclerView saleItemRecyclerView;
    private TextView saleIdTextView;
    private TextView dateTextView;
    private LinearLayout dateLinearLayout;
    private AutoCompleteTextView customerTextInputEditText;
    private TextView taxRateTextView;
    private TextView taxAmtTextView;
    private TextView discountTextView;
    private TextView subtotalTextView;
    private TextView totalTextView;
    private ImageButton discountTextViewBtn;
    private AppCompatImageButton taxTextViewBtn;

    private MaterialDialog addItemMaterialDialog;
    private MaterialDialog editItemMaterialDialog;
    private MaterialDialog addDiscountMaterialDialog;
    //UI components from add item MD
    private ImageButton img;
    private ImageButton imgEdit;
    private MaterialDialog payMaterialDialog;
    private AppCompatImageButton stockListBtnAddItemMd;
    private TextView itemCodeTextInputEditTextAddItemMd;
    private TextView itemNameTextInputEditTextAddItemMd;
    //    private EditText qtyTextInputEditTextAddItemMd;
    //    private AppCompatImageButton minusQtyAppCompatImageButtonAddItemMd;
    //    private ImageButton dropDownPriceBtn;
    //    private AppCompatImageButton plusQtyAppCompatImageButtonAddItemMd;
    //    private EditText priceTextInputEditTextAddItemMd;
    private SwitchCompat focSwitchCompatAddItemMd;
    private EditText discountTextInputEditTextAddItemMd;
    private Button saveMdButtonAddItemMd;
    //UI components from edit item MD
    private Button cancelMdButtonAddItemMd;
    private AppCompatImageButton stockListBtnEditItemMd;
    private TextView itemCodeTextInputEditTextEditItemMd;
    private TextView itemNameTextInputEditTextEditItemMd;
    private EditText qtyTextInputEditTextEditItemMd;
    private AppCompatImageButton minusQtyAppCompatImageButtonEditItemMd;
    private AppCompatImageButton plusQtyAppCompatImageButtonEditItemMd;
    private EditText priceTextInputEditTextEditItemMd;
    private SwitchCompat focSwitchCompatEditItemMd;
    private EditText discountTextInputEditTextEditItemMd;
    private Button saveMdButtonEditItemMd;
    //UI components for delivery

    private EditText deliveryAgentEditText;
    private TextView deliveryDateTextView;
    private EditText deliveryChargesEditText;
    private EditText deliveryCustomerNameEditText;
    private EditText deliveryPhoneNumberEditText;
    private EditText deliveryAddressEditText;
    //Action buttons from delivery  dialog
    private LinearLayout deliverDatePickBtn;
    private Button saveDeliverBtn;
    private Button cancelOrderBtn;
    //UI components from disc MD
    private LinearLayout deliverLayout;
    private EditText discountTextInputEditTextMd;
    private Button saveDiscountBtn;
    //UI components for Pay MD
    private Button cancelDiscountBtn;
    private EditText totalAmountTextInputEditTextPayMd;
    private EditText paidAmountTextInputEditTextPayMd;
    private EditText changeTextViewPayMd;
    private EditText saleOustandting;
    private Button saveTransaction;
    //Business
    private Button printTransaction;
    private StockManager stockManager;
    private CustomerManager customerManager;
    //values    //values

    private SalesManager salesManager;
    private Context context;
    private RVSwipeAdapterForSaleItemViewInSale rvSwipeAdapterForSaleItemViewInSale;
    private List<SalesAndPurchaseItem> salesAndPurchaseItemList;
    private RVAdapterforStockListMaterialDialog rvAdapterforStockListMaterialDialogEdit;
    private RVAdapterforStockListMaterialDialog rvAdapterforStockListMaterialDialog;
    private ArrayList<StockItem> stockItemList;
    private MaterialDialog stockListMaterialDialog;
    private MaterialDialog stockListMaterialDialogEdit;
    //    private MaterialDialog discountListMaterialDialog;
    private MaterialDialog taxListMaterialDialog;
    private SalesAndPurchaseItem salesItemInSale;
    private RVAdapterForTaxMD rvAdapterForTaxMD;
    //    private RVAdapterForDiscountMD rvAdapterForDiscountMD;
    private RVAdapterForCustomerMD rvAdapterForCustomerMD;
    private MaterialDialog customerListMaterialDialog;
    private CustomerAutoCompleteAdapter customerAutoCompleteAdapter;
    private ArrayList<Customer> customers;
    private List<Customer> customerList;
    //Values
    // private ImageButton customerListBtn;
    private String customerName = DEFAULT_CUSTOMER;
    private String saleInvoiceNo;
    private int qty = 0;
    private Boolean foc = false;
    private Double totalAmount = 0.0;
    private Double voucherDiscount = 0.0;

    private Double taxAmt = 0.0;
    private Tax tax;
    private Double subtotal = 0.0;
    private Double paidAmount = 0.0;
    private String type;
    private int editposition;
    private Calendar now;
    private int saleDay;
    private int saleMonth;
    private int saleYear;
    private DatePickerDialog datePickerDialog;
    private String date;
    private List<Tax> taxList;
    private Discount discount;
    private List<Discount> discountList;
    private SettingManager settingManager;
    private StockCodeAutoCompleteAdapter stockCodeAutoCompleteAdapter;
    private StockCodeAutoCompleteAdapter addstockCodeAutoCompleteAdapter;
    private StockNameAutoCompleteAdapter editstockNameAutoCompleteAdapter;
    private StockNameAutoCompleteAdapter addstockNameAutoCompleteAdapter;
    private StockCodeAutoCompleteAdapter editstockCodeAutoCompleteAdapter;
    private ArrayList<StockAutoComplete> stockAutoCompleteList;
    private MaterialDialog alertMaterialDialog;
    private StockCodeAutoCompleteAdapter.Callback addCallback;
    private StockCodeAutoCompleteAdapter.Callback editCallback;
    private String userInputQty;
    private Double deliveryCharges = 0.0;
    private SaleDelivery deliveryView;
    private String deliveryDate;
    private int deliverDay;
    private int deliverMonth;
    private int deliverYear;
    private MaterialDialog deliveryMaterialDialog;
    private MaterialDialog qtyInputMaterialDialog;
    private MaterialDialog priceInputMaterialDialog;
    //UI components for qtyInput
    private FontEditText qtyInputEditText;
    //UI components for priceInput
    private IgnoableAutoCompleteTextView valueTextView;

    private DatePickerDialog deliveryDatePickerDialog;
    private SalesHeader saleHeaderView;
    private SalesHistory salesHistory;
    private List<SalesAndPurchaseItem> updateList;
    private List<Long> deleteList;
    private Boolean isEdit = false;
    private Boolean isSaleEdit = false;
    private Boolean isHold = false;
    private Double discountRate = 0.0;
    private boolean isFirst = false;
    private TextView customerInPayDialog;
    private String phoneNo = "";
    private boolean darkmode;
    private KeyBoardView keyBoardView;
    private LinearLayout payLinearLayout;
    private LinearLayout addItemLinearLayout;
    private AutoCompleteTextView itemOrCodeAutocomplete;
    private Animation animation1;
    private Animation animation2;
    private boolean isPay;

    private TextView statusTextView;
    private TextView printDeviceNameTextView;
    private StockNameOrCodeAutoCompleteAdapter stockNameOrCodeAutoCompleteAdapter;
    private FrameLayout saleDetailPrint;
    private MaterialDialog customerMaterialDialog;
    private AutoCompleteTextView customerNameTextView;
    private EditText customerPhoneNoEditText;
    private AppCompatImageButton customerImageButton;
    private Button customerSaveButton;
    private Button customerCancelButton;
    private StockItem stockAutoCompleteView;
    private String discountPercent;
    private ImageButton barcodeBtn;
    private ImageButton addNewStock;
    private boolean isNewStockAdd = false;
    private MaterialDialog addNewStockMaterialDialog;
    private EditText itemName;
    private EditText codeNo;
    private EditText retailPrice;
    private EditText purchasePrice;
    private TextView retailPriceDropDown;
    private TextView wholeSalePriceDropDown;
    private LinearLayout wholeLinearLayout;
    private LinearLayout retailLinearLayout;
    private MaterialDialog stockChooserDialog;
    private RVAdapterForStockChooser stockChooserAdapter;
    private boolean isFromAuto = false;

    private ImageButton holdBtn;
    private User currentUser;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainLayout = inflater.inflate(R.layout.new_sale, null);
        context = getContext();
        darkmode = POSUtil.isNightMode(context);

        String title = ThemeUtil.getString(context, R.attr.sale_invoice);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);

        MainActivity.setCurrentFragment(this);


        AccessLogManager accessLogManager = new AccessLogManager(context);
        AuthorizationManager authorizationManager = new AuthorizationManager(context);
        Long deviceId = authorizationManager.getDeviceId(Build.SERIAL);
        currentUser = accessLogManager.getCurrentlyLoggedInUser(deviceId);
        if (currentUser == null) {
            POSUtil.noUserIsLoggedIn(context);
            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            return null;
        }


        salesAndPurchaseItemList = new ArrayList<>();
        stockItemList = new ArrayList<>();
        stockChooserAdapter = new RVAdapterForStockChooser();
        tax = new Tax();
        updateList = new ArrayList<>();
        deleteList = new ArrayList<>();
        now = Calendar.getInstance();
        deliveryView = new SaleDelivery();
        salesItemInSale = new SalesAndPurchaseItem();
        stockManager = new StockManager(context);
        settingManager = new SettingManager(context);
        customerManager = new CustomerManager(context);
        salesManager = new SalesManager(context);
        taxList = settingManager.getAllTaxs();
        discountList = settingManager.getAllDiscounts();
        discount = settingManager.getDefaultDiscount();
        tax = settingManager.getDefaultTax();

        if (tax == null) {
            tax = new Tax();
        }

        stockItemList = (ArrayList<StockItem>) stockManager.getAllActiveStocks(0, 10);
        deliverDay = now.get(Calendar.DAY_OF_MONTH);
        deliverMonth = now.get(Calendar.MONTH) + 1;
        deliverYear = now.get(Calendar.YEAR);
        type = SalesManager.SaleType.Sales.toString();
        stockAutoCompleteList = new ArrayList<>();
        stockAutoCompleteView = new StockItem();
        now = Calendar.getInstance();

        buildDatePickerDialog();

        saleDay = now.get(Calendar.DAY_OF_MONTH);
        saleMonth = now.get(Calendar.MONTH) + 1;
        saleYear = now.get(Calendar.YEAR);
        customers = (ArrayList<Customer>) customerManager.getAllCustomers(0, 100);
        customerList = customerManager.getAllCustomers(0, 100);
        rvAdapterForCustomerMD = new RVAdapterForCustomerMD(customerList);
        stockNameOrCodeAutoCompleteAdapter = new StockNameOrCodeAutoCompleteAdapter(getContext(), stockManager);

        //buildCustomerDialog();
        buildStockListDialog();
        buildDatePickerDialog();
        buildStockListDialogEdit();
        loadAddItemDialog();
        //  buildCustomerListDialog();
        loadEditItemDialog();
        buildAddDiscountDialog();
        buildPayDialog();
        buildTaxListDialogEdit();
        buildDeliveryDatePickerDialog();
        buildDeliveryAndPickupDialog();
        loadNewItemDialog();
        loadUI();
        buildingAlertDialog();
        popupWindowBuild();

        /*customerListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerListMaterialDialog.show();
            }
        });*/

        customerImageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                customerMaterialDialog.show();
            }
        });

        addCallback = new StockCodeAutoCompleteAdapter.Callback() {
            @Override
            public void onOneResut(final StockAutoComplete stockAutoComplete) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        assigValuesForitemAdd(stockAutoComplete);
                    }
                });

                salesItemInSale.setDiscountAmount(0.0);
                salesItemInSale.setBarcode(stockAutoComplete.getBarcode());
                salesItemInSale.setStockCode(stockAutoComplete.getCodeNo());
                salesItemInSale.setItemName(stockAutoComplete.getItemName());
                salesItemInSale.setQty(1);
                qty = 1;
                salesItemInSale.setStockID(stockAutoComplete.getId());
            }
        };

        editCallback = new StockCodeAutoCompleteAdapter.Callback() {
            @Override
            public void onOneResut(final StockAutoComplete stockAutoComplete) {
                if (isFirst) {

                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            assigValuesForitemEdit(stockAutoComplete);
                        }
                    });

                    editSaleItem(stockAutoComplete);
                }
            }
        };

        customerAutoCompleteAdapter = new CustomerAutoCompleteAdapter(context, customers);
        addstockCodeAutoCompleteAdapter = new StockCodeAutoCompleteAdapter(context, stockManager, addCallback);
        addstockNameAutoCompleteAdapter = new StockNameAutoCompleteAdapter(context, stockManager);
        editstockNameAutoCompleteAdapter = new StockNameAutoCompleteAdapter(context, stockManager);
        editstockCodeAutoCompleteAdapter = new StockCodeAutoCompleteAdapter(context, stockManager, editCallback);

        configRecyclerView();
        settingOnClick();
        POSUtil.hideKeyboard(mainLayout, context);

        customerTextInputEditText.setThreshold(1);
        customerTextInputEditText.setAdapter(customerAutoCompleteAdapter);

        // itemCodeTextInputEditTextAddItemMd.setThreshold(1);

        //itemCodeTextInputEditTextEditItemMd.setAdapter(editstockCodeAutoCompleteAdapter);

        // itemCodeTextInputEditTextEditItemMd.setThreshold(1);

        // itemCodeTextInputEditTextAddItemMd.setAdapter(addstockCodeAutoCompleteAdapter);

      /*  itemCodeTextInputEditTextEditItemMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockAutoCompleteView = editstockCodeAutoCompleteAdapter.getSuggestionList().get(position);

                assigValuesForitemEdit(stockAutoCompleteView);

                editSaleItem(stockAutoCompleteView);

                itemCodeTextInputEditTextEditItemMd.clearFocus();
            }
        });*/

        /*itemCodeTextInputEditTextAddItemMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockAutoCompleteView = addstockCodeAutoCompleteAdapter.getSuggestionList().get(position);

                assigValuesForitemAdd(stockAutoCompleteView);

                addSaleItem(stockAutoCompleteView);

                itemCodeTextInputEditTextAddItemMd.clearFocus();

            }
        });*/
        itemCodeTextInputEditTextAddItemMd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    //priceTextInputEditTextAddItemMd.requestFocus();
                }
                return false;
            }
        });

        itemNameTextInputEditTextAddItemMd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    // itemCodeTextInputEditTextAddItemMd.requestFocus();
                }
                return false;
            }
        });

        itemCodeTextInputEditTextEditItemMd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    //  priceTextInputEditTextEditItemMd.requestFocus();
                }
                return false;
            }
        });

        itemNameTextInputEditTextEditItemMd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    //itemCodeTextInputEditTextEditItemMd.requestFocus();
                }
                return false;
            }
        });

       /* itemNameTextInputEditTextAddItemMd.setThreshold(1);

        itemNameTextInputEditTextAddItemMd.setAdapter(addstockNameAutoCompleteAdapter);

        itemNameTextInputEditTextAddItemMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockAutoCompleteView = addstockNameAutoCompleteAdapter.getSuggestionList().get(position);

                assigValuesForitemAdd(stockAutoCompleteView);

                addSaleItem(stockAutoCompleteView);

                itemNameTextInputEditTextAddItemMd.clearFocus();
            }
        });*/

        /*itemNameTextInputEditTextEditItemMd.setThreshold(1);

        itemNameTextInputEditTextEditItemMd.setAdapter(editstockNameAutoCompleteAdapter);

        itemNameTextInputEditTextEditItemMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockAutoCompleteView = editstockNameAutoCompleteAdapter.getSuggestionList().get(position);

                assigValuesForitemEdit(stockAutoCompleteView);

                editSaleItem(stockAutoCompleteView);

                itemNameTextInputEditTextEditItemMd.clearFocus();
            }
        });*/
        //        rvAdapterForDiscountMD.setmItemClickListener(new RVAdapterForDiscountMD.OnItemClickListener() {
        //            @Override
        //            public void onItemClick(View view, int position) {
        //
        //                discountListMaterialDialog.dismiss();
        //
        //                discount = discountList.get(position);
        //
        //
        //
        //                updateViewData();
        //
        //            }
        //        });


      /*  barCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(barCode){

                    barCodeBtn.setActivated(true);

                    barCode=false;

                    stockCodeTIL.setVisibility(View.GONE);

                    autoCompleteTextView.setVisibility(View.GONE);

                    barCodeTIL.setVisibility(View.VISIBLE);

                    barCodeTIET.setVisibility(View.VISIBLE);

                    barCodeTIET.requestFocus();

                }else {

                    barCodeBtn.setActivated(false);

                    barCode=true;

                    autoCompleteTextView.setVisibility(View.VISIBLE);

                    barCodeTIET.setVisibility(View.VISIBLE);

                    stockCodeTIL.setVisibility(View.VISIBLE);

                    barCodeTIL.setVisibility(View.GONE);

                    autoCompleteTextView.requestFocus();

                }

            }
        });
*/
        /*
        addItemBarCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(addItemBarCode){

                    addItemBarCodeBtn.setActivated(true);

                    addItemBarCode=false;

                    itemCodeTextInputLayoutAddItemMd.setVisibility(View.GONE);

                    itemCodeTextInputEditTextAddItemMd.setVisibility(View.GONE);

                    addItemBarCodeTIL.setVisibility(View.VISIBLE);

                    addItemBarCodeTIET.setVisibility(View.VISIBLE);

                    addItemBarCodeTIET.requestFocus();

                }else {

                    addItemBarCodeBtn.setActivated(false);

                    addItemBarCode=true;

                    itemCodeTextInputLayoutAddItemMd.setVisibility(View.VISIBLE);

                    itemCodeTextInputEditTextAddItemMd.setVisibility(View.VISIBLE);

                    addItemBarCodeTIL.setVisibility(View.GONE);

                    addItemBarCodeTIET.setVisibility(View.GONE);

                    itemCodeTextInputLayoutAddItemMd.requestFocus();

                }

            }
        });
        */

   /*     addItemBarCodeTIET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                final Stock stockVO=stockManager.findStockByStockCode(s.toString());

                if(stockVO.getStockCode()!=null){

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {



                            assigValuesForitemAdd(stockVO);

                        }
                    });

                    salesItemInSale.setDiscountAmount(0.0);

                    salesItemInSale.setBarcode(stockVO.getBarCode());

                    salesItemInSale.setStockCode(stockVO.getStockCode());

                    salesItemInSale.setItemName(stockVO.getItemName());

                    salesItemInSale.setQty(1);

                    qty=1;

                    salesItemInSale.setStockID(stockVO.getId());

                }

            }

        });*/

     /*   barCodeTIET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                Stock stockVO=stockManager.findStockByStockCode(s.toString());

                if(stockVO.getStockCode()!=null){

                    salesItemInSale=new SalesAndPurchaseItem();

                    salesItemInSale.setItemName(stockVO.getItemName());

                    salesItemInSale.setQty(1);

                    salesItemInSale.setDiscountPercent("0.0%");

                    salesItemInSale.setDiscountAmount(0.0);

                    salesItemInSale.setStockCode(stockVO.getStockCode());

                    salesItemInSale.setPrice(stockVO.getNormalSalePrice());

                    salesItemInSale.setTotalPrice(salesItemInSale.getQty()*salesItemInSale.getPrice());

                    salesAndPurchaseItemList.add(salesItemInSale);

                    rvSwipeAdapterForSaleItemViewInSale.setValueList(salesAndPurchaseItemList);

                    rvSwipeAdapterForSaleItemViewInSale.notifyItemInserted(salesAndPurchaseItemList.size());

                    barCodeTIET.setSelection(0,barCodeTIET.length());

                    updateViewDataForBarScan();
                }

            }

        });*/

        rvAdapterForTaxMD.setmItemClickListener(new RVAdapterForTaxMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                taxListMaterialDialog.dismiss();

                tax = taxList.get(position);

                updateViewData();
            }
        });

        rvAdapterForCustomerMD.setmItemClickListener(new RVAdapterForCustomerMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                customerTextInputEditText.setText(customerList.get(position).getName().trim());

                customerListMaterialDialog.dismiss();
            }
        });

        if (getArguments() != null && getArguments().getSerializable("saleheader") != null) {

            salesHistory = (SalesHistory) getArguments().getSerializable("saleheader");
            if (salesHistory.isHold()) {
                Log.w("In HOLD", "get Header");
                saleHeaderView = salesManager.getHoldSalesHeaderView(salesHistory.getId());
            } else {
                Log.w(" Not In HOLD", "get Header");
                saleHeaderView = salesManager.getSalesHeaderView(salesHistory.getId());
            }
        }

        if (saleHeaderView != null) {
            isSaleEdit = true;
            isHold = saleHeaderView.isHold();

            initializeOldData();
            refreshRecyclerView();
            configUI();
            updateViewData();

            if (!isHold) {
                holdBtn.setVisibility(View.GONE);
            }
        } else {
            saleIdTextView.setVisibility(View.GONE);
            mainLayout.findViewById(R.id.saleLabel).setVisibility(View.GONE);
            mainLayout.findViewById(R.id.hashLabel).setVisibility(View.GONE);
        }

/*        cancelOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clearDeliverDialog();
                deliveryView=new SaleDelivery();
                type= SalesManager.SaleType.Sales.toString();

                deliveryMaterialDialog.dismiss();


            }
        });*/

        cancelDiscountBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addDiscountMaterialDialog.dismiss();
            }
        });

        cancelMdButtonAddItemMd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addItemMaterialDialog.dismiss();
            }
        });

        editItemMaterialDialog.findViewById(R.id.cancel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                editItemMaterialDialog.dismiss();
            }
        });

               customerTextInputEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    //  phoneNoEditText.requestFocus();
                }
                return false;
            }
        });

        //        qtyTextInputEditTextAddItemMd.addTextChangedListener(new TextWatcher() {
        //            @Override
        //            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //
        //            }
        //
        //            @Override
        //            public void onTextChanged(CharSequence s, int start, int before, int count) {
        //
        //            }
        //
        //            @Override
        //            public void afterTextChanged(Editable s) {
        //
        //                if (qtyTextInputEditTextAddItemMd.getText().toString().trim().length() > 0) {
        //                    qty = Integer.parseInt(qtyTextInputEditTextAddItemMd.getText().toString().trim());
        //                }
        //
        //            }
        //        });
        //
        //        qtyTextInputEditTextEditItemMd.addTextChangedListener(new TextWatcher() {
        //            @Override
        //            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //
        //            }
        //
        //            @Override
        //            public void onTextChanged(CharSequence s, int start, int before, int count) {
        //
        //            }
        //
        //            @Override
        //            public void afterTextChanged(Editable s) {
        //
        //                if (qtyTextInputEditTextEditItemMd.getText().toString().trim().length() > 0) {
        //                    qty = Integer.parseInt(qtyTextInputEditTextEditItemMd.getText().toString().trim());
        //                }
        //
        //            }
        //        });

        phoneNoEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                phoneNo = phoneNoEditText.getText().toString().trim();
            }
        });

        return mainLayout;
    }

    private void buildStockChooserDialog() {
        String stockChooser = context.getTheme().obtainStyledAttributes(new int[]{R.attr.stock_chooser}).getString(0);

        stockChooserDialog = new MaterialDialog.Builder(context).

                title(stockChooser)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(stockChooserAdapter, new LinearLayoutManager(context))

                .build();
    }

    public void clearDeliverDialog() {
        clearDeliveryDialogInput();
        clearDeliveryDialogErrorMsg();
    }

    private void clearDeliveryDialogErrorMsg() {
        deliveryAddressEditText.setError(null);
        deliveryAgentEditText.setError(null);
        deliveryPhoneNumberEditText.setError(null);
        deliveryChargesEditText.setError(null);
    }

    private void clearDeliveryDialogInput() {
        deliveryAgentEditText.setText(null);
        deliveryChargesEditText.setText(null);
        deliveryPhoneNumberEditText.setText(null);
        deliveryAddressEditText.setText(null);
    }


    /**
     * no use
     */

    public void buildCustomerDialog() {
        final TypedArray customer = context.getTheme().obtainStyledAttributes(new int[]{R.attr.customer});
        customerMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.fragment_sales_customer_dialog_layout, true).title(customer.getString(0))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").build();

        customerTextInputEditText = (AppCompatAutoCompleteTextView) customerMaterialDialog.findViewById(R.id.customer_name_edit_text);
        customerNameTextView = (AutoCompleteTextView) customerMaterialDialog.findViewById(R.id.customer_name_edit_text);
        customerPhoneNoEditText = (EditText) customerMaterialDialog.findViewById(R.id.phone_no_edit_text);
        customerSaveButton = (Button) customerMaterialDialog.findViewById(R.id.save_btn);
        customerCancelButton = (Button) customerMaterialDialog.findViewById(R.id.cancel_btn);
        customerSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customerNameTextView.getText().toString().trim() != null && customerNameTextView.getText().toString().trim().length() > 0) {
                    customerName = customerNameTextView.getText().toString().trim();
                    if (customerName.equalsIgnoreCase(DEFAULT_CUSTOMER)) {
                        customerNameTextView.setText(null);
                    }
                } else {
                    customerName = DEFAULT_CUSTOMER;
                }
                phoneNo = customerPhoneNoEditText.getText().toString().trim();

                customerMaterialDialog.dismiss();
            }
        });

        customerNameTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (customerNameTextView.getText().toString().trim() != null && customerNameTextView.getText().toString().trim().length() > 0) {
                    customerName = customerNameTextView.getText().toString().trim();
                    if (customerName.equalsIgnoreCase(DEFAULT_CUSTOMER)) {
                        customerNameTextView.setText(null);
                    }
                } else {
                    customerName = DEFAULT_CUSTOMER;
                }
            }
        });

        customerNameTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                customerPhoneNoEditText.setText(customerAutoCompleteAdapter.getSuggestion().get(i).getPhoneNo());
            }
        });
        customerCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                customerMaterialDialog.dismiss();
            }
        });

        customerNameTextView.setThreshold(1);
        customerAutoCompleteAdapter = new CustomerAutoCompleteAdapter(context, customers);
        customerNameTextView.setAdapter(customerAutoCompleteAdapter);
        rvAdapterForCustomerMD.setmItemClickListener(new RVAdapterForCustomerMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                customerNameTextView.setText(customerList.get(position).getName().trim());

                customerListMaterialDialog.dismiss();
            }
        });

        customerNameTextView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    customerPhoneNoEditText.requestFocus();
                }
                return false;
            }
        });

        customerPhoneNoEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void initializeOldData() {

        saleInvoiceNo = saleHeaderView.getSaleVocherNo();

        if (salesHistory.isHold()) {
            salesAndPurchaseItemList = salesManager.getSaleDetailsHoldBySalesID(saleHeaderView.getId());
        } else {
            salesAndPurchaseItemList = salesManager.getSaleDetailsBySalesID(saleHeaderView.getId());
        }

        saleDay = Integer.parseInt(saleHeaderView.getDay());
        saleMonth = Integer.parseInt(saleHeaderView.getMonth());
        saleYear = Integer.parseInt(saleHeaderView.getYear());
        now.set(saleYear, saleMonth - 1, saleDay);

        type = saleHeaderView.getType();

        paidAmount = saleHeaderView.getPaidAmt();
        voucherDiscount = saleHeaderView.getDiscountAmt();
        customerName = saleHeaderView.getCustomerName();
        tax = new Tax(saleHeaderView.getTaxID(), saleHeaderView.getTaxName(), saleHeaderView.getTaxRate(), saleHeaderView.getTaxType(), "");
        discount = new Discount(saleHeaderView.getDiscountID(), saleHeaderView.getDiscount(), saleHeaderView.getDiscountAmt());
        customerPhoneNoEditText.setText(customerManager.getCustomerByID(saleHeaderView.getCustomerID()).getPhoneNo());


        if (type.equalsIgnoreCase(SalesManager.SaleType.Delivery.toString())) {
            deliveryView = salesManager.getDeliveryInfoBySalesID(saleHeaderView.getId(), new InsertedBooleanHolder());
            deliveryCustomerNameEditText.setText(deliveryView.getCustomerName());
            deliveryAgentEditText.setText(deliveryView.getAgent());
            deliveryAddressEditText.setText(deliveryView.getAddress());
            deliveryChargesEditText.setText(deliveryView.getCharges().toString());
            deliveryPhoneNumberEditText.setText(deliveryView.getPhoneNo());
            deliverDay = Integer.parseInt(deliveryView.getDay());
            deliverMonth = Integer.parseInt(deliveryView.getMonth());
            deliverYear = Integer.parseInt(deliveryView.getYear());
            deliveryDate = deliveryView.getDate();
        }
    }

    private void addSaleItem(StockAutoComplete stockAutoComplete) {
        salesItemInSale.setDiscountAmount(0.0);
        salesItemInSale.setBarcode(stockAutoComplete.getBarcode());
        salesItemInSale.setStockCode(stockAutoComplete.getCodeNo());
        salesItemInSale.setItemName(stockAutoComplete.getItemName());
        salesItemInSale.setQty(1);

        qty = 1;

        salesItemInSale.setStockID(stockAutoComplete.getId());

        List<StockImage> stockImages = stockManager.getImagesByStockID(stockAutoComplete.getId());

        if (stockImages.isEmpty()) {
            img.setImageResource(R.drawable.add_image);
        } else {
            img.setImageBitmap(stockImages.get(0).getImageBitmap());
        }
    }

    private void addSaleItem(StockItem stockItem) {
        salesItemInSale.setDiscountAmount(0.0);
        salesItemInSale.setBarcode(stockItem.getBarcode());
        salesItemInSale.setStockCode(stockItem.getCodeNo());
        salesItemInSale.setItemName(stockItem.getName());
        salesItemInSale.setPrice(stockItem.getRetailPrice());
        salesItemInSale.setQty(1);

        qty = 1;

        salesItemInSale.setStockID(stockItem.getId());
        salesItemInSale.setTotalPrice(stockItem.getRetailPrice());
    }

    public void configUI() {
        setCustomerNameForDeliveryAndPickupDialog();
        configDeliveryDateUI();

        saleIdTextView.setText(saleInvoiceNo);
        customerTextInputEditText.setText(customerName);
        customerNameTextView.setText(customerName);
        configDateUI();
    }

    public void configDateUI() {
        date = DateUtility.makeDate(Integer.toString(saleYear), Integer.toString(saleMonth), Integer.toString(saleDay));
        String dayDes[] = DateUtility.dayDes(date);
        String yearMonthDes = DateUtility.monthYearDes(date);
        //dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));
        dateTextView.setText(DateUtility.makeDateFormatWithSlash(date));
    }

    public void buildDeliveryDatePickerDialog() {

        deliveryDatePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        deliverDay = dayOfMonth;
                        deliverMonth = monthOfYear + 1;
                        deliverYear = year;
                        deliveryView.setDay(String.valueOf(deliverDay));
                        deliveryView.setMonth(String.valueOf(deliverMonth));
                        deliveryView.setYear(String.valueOf(deliverYear));
                        configDeliveryDateUI();
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        if (darkmode)
            deliveryDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        deliveryDatePickerDialog.setThemeDark(darkmode);
    }

    @SuppressLint("ResourceAsColor")
    public void buildDatePickerDialog() {

        datePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        saleDay = dayOfMonth;
                        saleMonth = monthOfYear + 1;
                        saleYear = year;
                        date = DateUtility.makeDate(Integer.toString(year), Integer.toString(saleMonth), Integer.toString(dayOfMonth));

                        String dayDes[] = DateUtility.dayDes(date);

                        String yearMonthDes = DateUtility.monthYearDes(date);

                        // dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

                        dateTextView.setText(DateUtility.makeDateFormatWithSlash(date));
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)

        );

        if (darkmode)
            datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        datePickerDialog.setThemeDark(darkmode);
    }

    private void settingOnClickForDeliveryAndPickupDialog() {

/*
        deliveryAndPickUpDialog.setDeliveryCancelClickListener(new ClickListenerdeliveryDialog() {
            @Override
            public void onClick(View v, SaleDelivery deliveryVO, String customer) {
                deliveryAndPickUpDialog.dismiss();
            }
        });

        deliveryAndPickUpDialog.setPickUpSaveClickListener( new ClickListenerPickupDialog() {
            @Override
            public void onClick(View v, SalePickup pickupVO) {

                pickupView=pickupVO;

                Log.w("hello",pickupVO.getDay()+ pickupVO.getMonth()+pickupVO.getYear());
                deliveryView=null;
                type=SalesManager.SaleType.Pickup;
                deliveryAndPickUpDialog.dismiss();

                updateViewData();
            }
        });


        deliveryAndPickUpDialog.setPickUpCancelClickListener(new ClickListenerPickupDialog() {
            @Override
            public void onClick(View v,SalePickup pickupVO) {
                deliveryAndPickUpDialog.dismiss();
            }
        });


        deliveryAndPickUpDialog.setDeliverySaveClickListener(new ClickListenerdeliveryDialog() {
            @Override
            public void onClick(View v, SaleDelivery deliveryVO,String customer) {

                deliveryView=deliveryVO;
                 pickupView=null;
                type=SalesManager.SaleType.Delivery;
                deliveryAndPickUpDialog.dismiss();
                updateViewData();
            }
        });*/

        deliverDatePickBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                clearErrorMsgDeliver();
                deliveryDatePickerDialog.show(getActivity().getFragmentManager(), "deliver");
            }
        });

/*        saveDeliverBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    if(checkValidationForDelivery()){

                        type= SalesManager.SaleType.Delivery.toString();

                        getDataFromDeliveryView();

                        deliveryMaterialDialog.dismiss();

                        updateViewData();

                    }


            }
        });*/

    }

    private void setCustomerNameForDeliveryAndPickupDialog() {

        //  nameEditText.setText(customerName);
        deliveryCustomerNameEditText.setText(customerName);
        customerTextInputEditText.setText(customerName);
    }

    private void configDeliveryDateUI() {
        deliveryDate = DateUtility.makeDate(Integer.toString(deliverYear), Integer.toString(deliverMonth), Integer.toString(deliverDay));
        //String dayDes[] = DateUtility.dayDes(deliveryDate);
        //String yearMonthDes = DateUtility.monthYearDes(deliveryDate);
        //   deliveryDateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));
        deliveryDateTextView.setText(DateUtility.makeDateFormatWithSlash(deliveryDate));
    }

    public void configRecyclerView() {
        rvSwipeAdapterForSaleItemViewInSale = new RVSwipeAdapterForSaleItemViewInSale(salesAndPurchaseItemList, context);
        saleItemRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        saleItemRecyclerView.setAdapter(rvSwipeAdapterForSaleItemViewInSale);
    }

    public void refreshRecyclerView() {
        rvSwipeAdapterForSaleItemViewInSale.setSaleItemViewInSaleList(salesAndPurchaseItemList);
        rvSwipeAdapterForSaleItemViewInSale.notifyDataSetChanged();
    }

    public void setItemValue() {
        //salesItemInSale.setId(stockAutoCompleteView.getId());
        salesItemInSale.setItemName(stockAutoCompleteView.getName());
        salesItemInSale.setStockCode(stockAutoCompleteView.getCodeNo());
        salesItemInSale.setQty(qty);
        salesItemInSale.setPrice(stockAutoCompleteView.getRetailPrice());
        salesItemInSale.setDiscountAmount(0.0);

        Stock stock = stockManager.findStockByStockCode(salesItemInSale.getStockCode());
        salesItemInSale.setStockID(stockAutoCompleteView.getId());

        if (stock.getStockCode() == null) {

            //   salesItemInSale.setStockID(stockManager.addQuickStockSetupSale(salesItemInSale.getItemName(), salesItemInSale.getStockCode(),
            //  salesItemInSale.getPrice(),  ));

            try {
                throw new Exception("INVALID STOCK CODE");
            } catch (Exception e) {

            }
        }

        salesItemInSale.setTotalPrice(calculatePrice(salesItemInSale.getPrice(), qty, salesItemInSale.getDiscountAmount()));
    }

    public void setItemValue(StockItem stockAutoCompleteView) {
        //salesItemInSale.setId(stockAutoCompleteView.getId());
        salesItemInSale.setItemName(stockAutoCompleteView.getName());
        salesItemInSale.setStockCode(stockAutoCompleteView.getCodeNo());
        salesItemInSale.setQty(1);
        salesItemInSale.setPrice(stockAutoCompleteView.getRetailPrice());
        salesItemInSale.setDiscountAmount(0.0);

        Stock stock = stockManager.findStockByStockCode(salesItemInSale.getStockCode());
        salesItemInSale.setStockID(stockAutoCompleteView.getId());

        if (stock.getStockCode() == null) {

            //   salesItemInSale.setStockID(stockManager.addQuickStockSetupSale(salesItemInSale.getItemName(), salesItemInSale.getStockCode(),
            //  salesItemInSale.getPrice(),  ));

            try {
                throw new Exception("INVALID STOCK CODE");
            } catch (Exception e) {

            }
        }

        salesItemInSale.setTotalPrice(calculatePrice(salesItemInSale.getPrice(), salesItemInSale.getQty(), salesItemInSale.getDiscountAmount()));
    }

    private void buildingAlertDialog() {

        String noItem = context.getTheme().obtainStyledAttributes(new int[]{R.attr.there_is_no_item}).getString(0);
        alertMaterialDialog = new MaterialDialog.Builder(context).
                title(noItem).
                build();
    }

    // lyx Setup listeners on views of quantity input material dialog
    private void setQtyInputMDOnClickListener(final int position) {
        qtyInputMaterialDialog.findViewById(R.id.ok_btn).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        qtyInputMaterialDialog.dismiss();
                        int qty = POSUtil.isTextViewEmpty(qtyInputEditText) ? 1 : Integer.parseInt(qtyInputEditText.getText().toString());
                        salesAndPurchaseItemList.get(position).setQty(qty);
                        salesAndPurchaseItemList.get(position).setTotalPrice(qty * salesAndPurchaseItemList.get(position).getPrice());
                        rvSwipeAdapterForSaleItemViewInSale.notifyDataSetChanged();
                        rvSwipeAdapterForSaleItemViewInSale.closeAllItems();
                        updateViewData();
                        if (!updateList.contains(salesAndPurchaseItemList.get(position))) {
                            updateList.add(salesAndPurchaseItemList.get(position));
                        }
                    }
                });
        qtyInputMaterialDialog.findViewById(R.id.cancel_btn).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        qtyInputMaterialDialog.dismiss();
                    }
                });
    }

    // lyx Setup listeners on views of price input material dialog
    private void setPriceInputMDOnClickListener(final int position, final List<String> priceList) {
        buildPricePopup();
        priceInputMaterialDialog.findViewById(R.id.drop_down).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.setWidth(valueTextView.getWidth());
                popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

                retailPriceDropDown.setText(priceList.get(0));
                wholeSalePriceDropDown.setText(priceList.get(1));
                popupWindow.showAsDropDown(valueTextView);
            }
        });

        priceInputMaterialDialog.findViewById(R.id.ok_btn).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        priceInputMaterialDialog.dismiss();
                        Double price = POSUtil.isTextViewEmpty(valueTextView) ? 0 : Double.parseDouble(valueTextView.getText().toString());
                        salesAndPurchaseItemList.get(position).setPrice(price);
                        salesAndPurchaseItemList.get(position).setTotalPrice(salesAndPurchaseItemList.get(position).getQty() * price);
                        rvSwipeAdapterForSaleItemViewInSale.notifyDataSetChanged();
                        rvSwipeAdapterForSaleItemViewInSale.closeAllItems();
                        updateViewData();

                        if (!updateList.contains(salesAndPurchaseItemList.get(position))) {
                            updateList.add(salesAndPurchaseItemList.get(position));
                        }
                    }
                });
        priceInputMaterialDialog.findViewById(R.id.cancel_btn).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        priceInputMaterialDialog.dismiss();
                    }
                });
    }

    // lyx

    /**
     * Build a popup dropdown list of retail price and wholesale price.
     *
     * @return built popup window.
     */
    public PopupWindow buildPricePopup() {

        // initialize a pop up window type
        popupWindow = new PopupWindow(getContext());
        popupWindow.setFocusable(true);

        //        Log.w("WIDTH",priceTextInputEditTextEditItemMd.getWidth()+" SS");

        // set the list view as pop up window content

        int labelWhite = getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.backcolor}).getColor(0, Color.TRANSPARENT);
        // if(labelWhite){
        popupWindow.setBackgroundDrawable(new ColorDrawable(labelWhite));

        View view = LayoutInflater.from(getContext()).inflate(R.layout.price_list_item_view, null);

        retailPriceDropDown = view.findViewById(R.id.retail_sale_price_amt);
        wholeSalePriceDropDown = view.findViewById(R.id.whole_sale_price_amt);
        retailLinearLayout = view.findViewById(R.id.retail_ll);
        wholeLinearLayout = view.findViewById(R.id.whole_ll);

        retailLinearLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                valueTextView.setText(retailPriceDropDown.getText().toString());
                popupWindow.dismiss();
            }
        });
        wholeLinearLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                valueTextView.setText(wholeSalePriceDropDown.getText().toString());
                popupWindow.dismiss();
            }
        });

        popupWindow.setContentView(view);

        return popupWindow;
    }

    public void settingOnClick() {

        taxTextViewBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                taxListMaterialDialog.show();
            }
        });

        dateLinearLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog.show(getActivity().getFragmentManager(), "Sale Pick");
            }
        });

        deliveryImageViewBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                setCustomerNameForDeliveryAndPickupDialog();

                deliveryMaterialDialog.show();

          /*     deliveryAndPickUpDialog.setCustomerName(customerName);
                if(deliveryView!=null){
                    deliveryAndPickUpDialog.setDataFromDelivery(deliveryView,customerName);
                }else if(pickupView!=null) {

                    deliveryAndPickUpDialog.clearDataFromDelivery();
                    deliveryAndPickUpDialog.setDataPickupData(pickupView,customerName);
                }
                deliveryAndPickUpDialog.show(getChildFragmentManager(),"D&P");
*/
            }
        });

        // lyx set up listener on quantity EditText of sale item
        rvSwipeAdapterForSaleItemViewInSale.setEditQtyClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                String quantity = ThemeUtil.getString(context, R.attr.quantity);

                qtyInputMaterialDialog = new MaterialDialog.Builder(context)
                        .customView(R.layout.qty_input_md, false)
                        .canceledOnTouchOutside(true)
                        .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                        .build();
                qtyInputMaterialDialog
                        .show();

                qtyInputEditText = (FontEditText) qtyInputMaterialDialog.findViewById(R.id.qty_input_edittext);
                POSUtil.showKeyboard(qtyInputEditText);
                //position is needed for changing current item's qty
                setQtyInputMDOnClickListener(postion);
            }
        });

        //lyx set up listener on price EditText of sale item
        rvSwipeAdapterForSaleItemViewInSale.setEditPriceClickListener(new ClickListener() {
            @Override
            public void onClick(int position) {
                String price = ThemeUtil.getString(context, R.attr.price);
                Log.e("title", price);

                Stock stock = stockManager.findStockByID(salesAndPurchaseItemList.get(position).getStockID());
                List<String> priceList = new ArrayList<>();
                // priceList = {retail price, wholesale price, current price}
                priceList.add(POSUtil.doubleToString(stock.getRetailPrice()));
                priceList.add(POSUtil.doubleToString(stock.getWholeSalePrice()));
                priceList.add(POSUtil.doubleToString(salesAndPurchaseItemList.get(position).getPrice()));

                priceInputMaterialDialog = new MaterialDialog.Builder(context).
                        customView(R.layout.price_input_dialog, false)
                        .canceledOnTouchOutside(true)
                        .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                        .build();
                priceInputMaterialDialog
                        .show();

                valueTextView = (IgnoableAutoCompleteTextView) priceInputMaterialDialog.findViewById(R.id.value_text_view);

                POSUtil.showKeyboard(valueTextView);
                setPriceInputMDOnClickListener(position, priceList);
            }
        });

        //lyx set up listener on minus button of sale item
        rvSwipeAdapterForSaleItemViewInSale.setQtyMinusClickListener(new ClickListener() {
            @Override
            public void onClick(int position) {
                updateViewData();
                if (!updateList.contains(salesAndPurchaseItemList.get(position))) {
                    updateList.add(salesAndPurchaseItemList.get(position));
                }
            }
        });

        //lyx set up listener on plus button of sale item
        rvSwipeAdapterForSaleItemViewInSale.setQtyPlusClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                updateViewData();
                if (!updateList.contains(salesAndPurchaseItemList.get(postion))) {
                    updateList.add(salesAndPurchaseItemList.get(postion));
                }
            }
        });

        itemOrCodeAutocomplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (itemOrCodeAutocomplete.getText().toString().trim().length() > 1) {
                    itemOrCodeAutocomplete.setError(null);
                }

                itemOrCodeAutocomplete.showDropDown();
            }
        });

        //for Phone
        if (addSaletemItemBtn != null) {

            addSaletemItemBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    addSaleItem();
                }
            });
        }

        stockChooserAdapter.setmItemClickListener(new RVAdapterForStockChooser.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                qty = 1;
                setItemValue(stockChooserAdapter.getSelectedStock());

                if (salesAndPurchaseItemList.contains(salesItemInSale)) {

                    SalesAndPurchaseItem salesAndPurchaseItem = salesAndPurchaseItemList.get(salesAndPurchaseItemList.indexOf(salesItemInSale));

                    salesAndPurchaseItem.setQty(salesAndPurchaseItem.getQty() + 1);
                    salesAndPurchaseItem.setTotalPrice((salesAndPurchaseItem.getQty() * salesAndPurchaseItem.getPrice()));
                } else {
                    salesAndPurchaseItemList.add(salesItemInSale);
                }

                itemOrCodeAutocomplete.setText("");
                refreshRecyclerView();

                stockChooserDialog.dismiss();
                updateViewData();

                updateList.add(0, salesItemInSale);
                foc = false;
                salesItemInSale = new SalesAndPurchaseItem();
                //clearInputAddItemMaterialDialog();
                stockAutoCompleteView = new StockItem();
            }
        });

        rvSwipeAdapterForSaleItemViewInSale.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                isFirst = true;
                isEdit = true;
                editposition = postion;

                setInputEditItemMaterialDialog();
                List<StockImage> stockImages = stockManager.getImagesByStockID(salesAndPurchaseItemList.get(editposition).getStockID());

                Log.w("HERE EDIT", stockImages.size() + "sss");

                if (stockImages.isEmpty()) {
                    imgEdit.setImageResource(R.drawable.add_image);
                } else {
                    imgEdit.setImageBitmap(stockImages.get(0).getImageBitmap());
                }
                if (!isTabetLand(context)) {

                    editItemMaterialDialog.show();
                }
            }
        });

        rvSwipeAdapterForSaleItemViewInSale.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                deleteList.add(salesAndPurchaseItemList.get(postion).getId());
                salesAndPurchaseItemList.remove(postion);
                rvSwipeAdapterForSaleItemViewInSale.setSaleItemViewInSaleList(salesAndPurchaseItemList);
                rvSwipeAdapterForSaleItemViewInSale.notifyItemRemoved(postion);
                rvSwipeAdapterForSaleItemViewInSale.notifyDataSetChanged();

                updateViewData();
            }
        });

        discountTextViewBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // discountTextInputEditTextMd.setText(POSUtil.NumberFormat(voucherDiscount));
                //discountTextInputEditTextMd.setSelection(discountTextInputEditTextMd.getText().length());

                String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.discount}).getString(0);
                Log.e("titke", title);
                discountDialogFragment = DiscountDialogFragment.newInstance(title);
                discountDialogFragment.setTargetFragment(AddEditNewSaleFragment.this, 1);
                if (discount != null) {
                    Log.e("discountVo", discount.getAmount() + " ");
                    discountDialogFragment.setDefaultDiscount(discount);
                }
                discountDialogFragment.show(getFragmentManager().beginTransaction(), "dialog");
            }
        });

        payMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                POSUtil.hideKeyboard(paidAmountTextInputEditTextPayMd, context);
            }
        });


        holdBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSaleEdit) {
                    Double balance = totalAmount - paidAmount;

                    if (balance < 0) {
                        balance = 0.0;
                    }

                    String noRemark = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_remark}).getString(0);

                    if (discount.getIsPercent() == 1) {
                        discountPercent = discount.getAmount() + " %";
                    } else {
                        discountPercent = "";
                    }

                    salesManager.holdNewSales(customerName, phoneNo, totalAmount, discountPercent, tax.getId(), subtotal, type.toString(),
                            voucherDiscount, noRemark, paidAmount, balance, taxAmt, tax.getRate(), Integer.toString(saleDay),
                            Integer.toString(saleMonth), Integer.toString(saleYear), salesAndPurchaseItemList, null, deliveryView, tax.getType(), discount.getId(), currentUser.getId());
                    clearInputAndOpenNewVoucher();
                } else {
                    Double balance = totalAmount - paidAmount;

                    if (balance < 0) {
                        balance = 0.0;
                    }

                    //          Log.w("tota ",totalAmount.toString());

                    Log.e("tax", tax.getRate() + " " + tax.getType());

                    if (discount.getIsPercent() == 1) {
                        discountPercent = discount.getAmount() + " %";
                    } else {
                        discountPercent = "";
                    }
                    String noRemark = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_remark}).getString(0);
                    boolean status = salesManager.updateHoldSalesBySalesID(saleHeaderView.getId(), date, customerName, phoneNo, totalAmount, discountPercent,
                            tax.getId(), subtotal, voucherDiscount, noRemark, paidAmount, balance, taxAmt,
                            Integer.toString(saleDay), Integer.toString(saleMonth), Integer.toString(saleYear), type.toString(),
                            tax.getRate(), updateList, deleteList, null, deliveryView, tax.getType(), discount.getId());

                    if (status) {
                        getActivity().onBackPressed();
                        clearInputAndOpenNewVoucher();
                    }
                }
            }
        });

        settingOnClickForDeliveryAndPickupDialog();
        settingOnClickForEditSaleItem();
        settingOnClickForAddSaleItemDialog();
        settingOnClickForPayMd();
        settingOnClickForDiscountMD();
    }

    private void addSaleItem() {
        foc = false;
        isEdit = false;
        salesItemInSale = new SalesAndPurchaseItem();

        //clearInputAddItemMaterialDialog();
        //addItemMaterialDialog.show();

        if (itemOrCodeAutocomplete.getText().toString().trim().length() > 0) {
            List<StockItem> stockItems = stockManager.getActiveStocksByNameOrCode(0, 10000, itemOrCodeAutocomplete.getText().toString().trim());

            if (stockItems.isEmpty()) {

            } else if (stockItems.size() == 1) {
                setItemValue(stockItems.get(0));

                if (salesAndPurchaseItemList.contains(salesItemInSale)) {

                    SalesAndPurchaseItem salesAndPurchaseItem = salesAndPurchaseItemList.get(salesAndPurchaseItemList.indexOf(salesItemInSale));
                    salesAndPurchaseItem.setQty(salesAndPurchaseItem.getQty() + 1);
                    salesAndPurchaseItem.setTotalPrice((salesAndPurchaseItem.getQty() * salesAndPurchaseItem.getPrice()));
                    updateList.add(salesAndPurchaseItem);
                } else {
                    salesAndPurchaseItemList.add(0, salesItemInSale);
                    updateList.add(salesItemInSale);
                }

                refreshRecyclerView();

                addItemMaterialDialog.dismiss();

                updateViewData();

                foc = false;

                salesItemInSale = new SalesAndPurchaseItem();

                //clearInputAddItemMaterialDialog();

                stockAutoCompleteView = new StockItem();
                itemOrCodeAutocomplete.setText("");
            } else {
                //stockChooserAdapter.setFilterNameList(stockItems);
                Log.e("SHOW", "SHOW");
                // stockChooserDialog.show();

            }
        }
    }

    private void clearInputAndOpenNewVoucher() {

        //        saleInvoiceNo = salesManager.getSalesInvoiceNo();

        saleIdTextView.setText(saleInvoiceNo);
        customerTextInputEditText.setText(null);
        phoneNoEditText.setText(null);
        salesAndPurchaseItemList = new ArrayList<>();
        rvSwipeAdapterForSaleItemViewInSale.clearData();
        paidAmount = 0.0;
        tax = settingManager.getDefaultTax();

        voucherDiscount = 0.0;
        changeTextViewPayMd.setText("0");
        paidAmountTextInputEditTextPayMd.setText("0");

        updateViewData();

        isPay = false;
        if (POSUtil.isTabetLand(getContext())) {
            toggleView();
        }
    }

    private void clearErrorMsgDeliver() {
        deliveryAgentEditText.setError(null);
        deliveryCustomerNameEditText.setError(null);
        deliveryChargesEditText.setError(null);
        deliveryAddressEditText.setError(null);
    }

    public boolean isInputDataValid() {

        boolean status = true;

        if (paidAmountTextInputEditTextPayMd.getText().toString().trim().length() < 1) {

            status = false;

            String enterAmt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_paid_amount}).getString(0);
            paidAmountTextInputEditTextPayMd.setError(enterAmt);
        } else {

            paidAmount = Double.valueOf(paidAmountTextInputEditTextPayMd.getText().toString());

            if (paidAmount > totalAmount) {
                paidAmount = totalAmount;
            }
        }

        double paidamt = 0.0;
        if (paidAmountTextInputEditTextPayMd.getText().toString().trim().length() > 0) {
            paidamt = Double.parseDouble(paidAmountTextInputEditTextPayMd.getText().toString());
        }

        if (customerName.equalsIgnoreCase(DEFAULT_CUSTOMER) && paidamt < totalAmount) {
            customerNameTextView.setError("Enter Customer Name");
            status = false;
        }

        if (customerPhoneNoEditText.getText().toString().trim().length() > 0) {
            if (customerNameTextView.getText().toString().trim().length() < 1) {
                customerNameTextView.setError("Enter Customer Name");
                status = false;
            }
        }
        return status;
    }

    private void loadDeliveryUI() {
        deliveryAgentEditText = (EditText) deliveryMaterialDialog.findViewById(R.id.agent_in_delivery);
        deliveryDateTextView = (TextView) deliveryMaterialDialog.findViewById(R.id.date);
        deliveryChargesEditText = (EditText) deliveryMaterialDialog.findViewById(R.id.charges_in_delivery);
        deliveryCustomerNameEditText = (EditText) deliveryMaterialDialog.findViewById(R.id.customer_name_in_delivery);
        deliveryPhoneNumberEditText = (EditText) deliveryMaterialDialog.findViewById(R.id.customer_phone_in_delivery);
        deliveryAddressEditText = (EditText) deliveryMaterialDialog.findViewById(R.id.address_in_delivery);
        deliverDatePickBtn = (LinearLayout) deliveryMaterialDialog.findViewById(R.id.date_ll);
    }

    private void getDataFromDeliveryView() {
        deliveryView.setAgent(deliveryAgentEditText.getText().toString().trim());
        deliveryView.setDate(DateUtility.makeDate(Integer.toString(deliverYear), Integer.toString(deliverMonth), Integer.toString(deliverDay)));
        deliveryView.setAddress(deliveryAddressEditText.getText().toString().trim());
        deliveryView.setPhoneNo(deliveryPhoneNumberEditText.getText().toString().trim());
        deliveryView.setCharges(Double.parseDouble(deliveryChargesEditText.getText().toString().trim()));
        customerName = deliveryCustomerNameEditText.getText().toString().trim();
        deliveryView.setDay(Integer.toString(deliverDay));
        deliveryView.setMonth(Integer.toString(deliverMonth));
        deliveryView.setYear(Integer.toString(deliverYear));
        deliveryView.setCharges(Double.parseDouble(deliveryChargesEditText.getText().toString().trim()));
    }

    private boolean checkValidationForDelivery() {

        boolean status = true;

        if (deliveryAgentEditText.getText().toString().trim().length() < 1) {

            String enterAgent = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_Enter_agent}).getString(0);
            deliveryAgentEditText.setError(enterAgent);

            status = false;
        }

        if (deliveryDateTextView.getText().toString().trim().length() < 1) {

        }

        if (deliveryChargesEditText.getText().toString().trim().length() < 1) {

            String enterDeliveryCharges = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_delivery_charges}).getString(0);
            deliveryChargesEditText.setError(enterDeliveryCharges);

            status = false;
        } else {

            deliveryView.setCharges(Double.parseDouble(deliveryChargesEditText.getText().toString().trim()));
        }

        if (deliveryCustomerNameEditText.getText().toString().trim().length() < 1) {

            String enterCustomerName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_customer_name}).getString(0);
            deliveryChargesEditText.setError(enterCustomerName);

            status = false;
        }

        if (deliveryAddressEditText.getText().toString().length() < 1) {

            status = false;

            String enterAddToDelivery = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_address_for_delivery}).getString(0);
            deliveryAddressEditText.setError(enterAddToDelivery);
        }

        return status;
    }

    // To animate view slide out from left to right
    public void slideToRight(View view) {
        TranslateAnimation animate = new TranslateAnimation(0, view.getWidth(), 0, 0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }   // To animate view slide out from left to right

    public void slideToLeft(View view) {
        TranslateAnimation animate = new TranslateAnimation(-view.getWidth(), view.getWidth(), 0, 0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    @Override
    public void onResume() {

        super.onResume();
        buildStockChooserDialog();
        // if(!POSUtil.isTabetLand(context)){

        // }else {
        //buildCustomerDialog();
        // }
        configUI();
        updateViewData();
       /* addItemMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                isNewStockAdd=false;
            }
        });*/

        //       dropDownPriceBtn.setOnClickListener(new View.OnClickListener() {
        //           @Override
        //           public void onClick(View v) {
        //
        //               popupWindow.setWidth(priceTextInputEditTextEditItemMd.getWidth());
        //               popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        //
        //               Stock stockVO=stockManager.findStockByID(salesAndPurchaseItemList.get(editposition).getStockID());
        //
        //               retailPriceDropDown.setText(POSUtil.doubleToString(stockVO.getRetailPrice()));
        //
        //               wholeSalePriceDropDown.setText(POSUtil.doubleToString(stockVO.getWholeSalePrice()));
        //
        //               popupWindow.showAsDropDown(priceTextInputEditTextEditItemMd);
        //           }
        //       });

        if (addNewStock != null) {
            addNewStock.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //                    addNewStockMaterialDialog.show();
                    Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                    addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_STOCK);

                    startActivity(addCurrencyIntent);
                }
            });
        }

        if (barcodeBtn != null) {

            barcodeBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.w("herer", "Bar code onclick");
                    checkPermission();
                    // Here, thisActivity is the current activity
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_GRANTED) {

                        Intent intent = new Intent(getContext(), CaptureActivity.class);
                        intent.setAction("com.google.zxing.client.android.SCAN");
                        intent.putExtra("SAVE_HISTORY", false);
                        startActivityForResult(intent, SCANNER_BAR_CODE);
                    }
                }
            });
        }

        itemOrCodeAutocomplete.requestFocus();
        itemOrCodeAutocomplete.setThreshold(1);
        itemOrCodeAutocomplete.setAdapter(stockNameOrCodeAutoCompleteAdapter);

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //region I don't think this part is used at all coz landscape is disabled
        if (POSUtil.isTabetLand(context)) {
            payLinearLayout.setVisibility(View.GONE);
            saleDetailPrint.setVisibility(View.GONE);
            addItemLinearLayout.setVisibility(View.VISIBLE);

            mainLayout.findViewById(R.id.pay_save_and_print).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    POSUtil.hideKeyboard(mainLayout, context);
                    /*if (!isSaleEdit && !isHold) {
                        saveTransaction();
                    } else if (isHold) {
                        saveHeldTransaction(saleInvoiceNo);
                    } else {
                        updateTransaction();
                    }*/

                    Long saleId = salesManager.findIDBySalesInvoice(saleInvoiceNo);
                    SaleDetailFragment saleDetailFragment = new SaleDetailFragment();
                    Bundle bundle = new Bundle();
                    bundle.putLong(SaleDetailFragment.KEY, saleId);
                    bundle.putBoolean(SaleDetailFragment.PRINTKEY, true);

                    Log.w("Sale ID", "Sale ID  " + saleId);

                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    fragmentTransaction.replace(R.id.sale_detail_print, saleDetailFragment);

                    //fragmentTransaction.addToBackStack("back");
                    saleDetailFragment.setArguments(bundle);

                    payLinearLayout.setVisibility(View.GONE);
                    saleDetailPrint.setVisibility(View.VISIBLE);

                    fragmentTransaction.commit();
                }
            });


            /*itemOrCodeAutocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                    stockAutoCompleteView = stockNameOrCodeAutoCompleteAdapter.getSuggestionList().get(position);
                    assigValuesForitemAdd(stockAutoCompleteView);
                    itemOrCodeAutocomplete.setText("");
                    addSaleItem(stockAutoCompleteView);
                    itemOrCodeAutocomplete.clearFocus();
                    discountTextInputEditTextAddItemMd.requestFocus();
                    discountTextInputEditTextAddItemMd.clearFocus();
                    POSUtil.hideKeyboard(mainLayout ,getContext());

                }
            });*/

            final BluetoothUtil bluetoothUtil = BluetoothUtil.getInstance(this);
            final Spinner spinner = new Spinner(context);

            bluetoothUtil.setBluetoothConnectionListener(new BluetoothUtil.JobListener() {
                @Override
                public void onPrepare() {
                    spinner.show();
                }

                @Override
                public void onSuccess() {
                    spinner.dismiss();

                    printDeviceNameTextView.setText(bluetoothUtil.getDevice().getName());

                    if (bluetoothUtil.isConnected()) {
                        statusTextView.setTextColor(Color.parseColor("#2E7D32"));
                        String connected = ThemeUtil.getString(context, R.attr.connected);
                        statusTextView.setText(connected);
                    } else {
                        statusTextView.setTextColor(Color.parseColor("#DD2C00"));
                        String nonConnected = ThemeUtil.getString(context, R.attr.not_connected);
                        statusTextView.setText(nonConnected);
                    }
                }

                @Override
                public void onFailure() {
                    String noDevice = ThemeUtil.getString(context, R.attr.no_device);
                    String notConnected = ThemeUtil.getString(context, R.attr.not_connected);

                    printDeviceNameTextView.setText(noDevice);
                    statusTextView.setText(notConnected);

                    spinner.dismiss();

                }

                @Override
                public void onDisconnected() {
                    String noDevice = ThemeUtil.getString(context, R.attr.no_device);
                    String notConnected = ThemeUtil.getString(context, R.attr.not_connected);

                    printDeviceNameTextView.setText(noDevice);
                    statusTextView.setTextColor(Color.parseColor("#DD2C00"));
                    statusTextView.setText(notConnected);
                }
            });

            mainLayout.findViewById(R.id.blue_toothLL).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    bluetoothUtil.createConnection(true);
                }
            });

            //   itemOrCodeAutocomplete.requestFocus();
            keyBoardView.setKeyPressListner(new KeyBoardView.KeyPressListner() {
                @Override
                public void onKeyPress(int num) {

                    if (paidAmountTextInputEditTextPayMd.length() < 1 || Long.parseLong(paidAmountTextInputEditTextPayMd.getText().toString()) == 0) {
                        paidAmountTextInputEditTextPayMd.setText(Long.toString(num));
                    } else {
                        paidAmountTextInputEditTextPayMd.setText(paidAmountTextInputEditTextPayMd.getText().toString() + Long.toString(num));
                    }
                }

                @Override
                public void onClear() {
                    paidAmountTextInputEditTextPayMd.setText("0");
                }

                @Override
                public void onDelete() {
                    if (paidAmountTextInputEditTextPayMd.length() == 1) {
                        paidAmountTextInputEditTextPayMd.setText("0");
                    } else {

                        String temp = paidAmountTextInputEditTextPayMd.getText().toString().substring(0, paidAmountTextInputEditTextPayMd.getText().toString().length() - 1);
                        //paidAmountTextInputEditTextPayMd.getText().charAt(paidAmountTextInputEditTextPayMd.length()-1);
                        Log.e("Here", paidAmountTextInputEditTextPayMd.getText().charAt(paidAmountTextInputEditTextPayMd.length() - 1) + "CHAR LAST INDEX");
                        paidAmountTextInputEditTextPayMd.setText(temp);
                    }
                }

                @Override
                public void onAddNum(int num) {
                    if (paidAmountTextInputEditTextPayMd.length() < 1 || Long.parseLong(paidAmountTextInputEditTextPayMd.getText().toString()) == 0) {
                        paidAmountTextInputEditTextPayMd.setText(Long.toString(num));
                    } else {
                        paidAmountTextInputEditTextPayMd.setText(Long.toString(Long.parseLong(paidAmountTextInputEditTextPayMd.getText().toString()) + num));
                    }
                }
            });

            animation1 = AnimationUtils.loadAnimation(getContext(), R.anim.to_middle);
            animation1.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    if (isPay) {
                        payLinearLayout.clearAnimation();
                        payLinearLayout.setAnimation(animation2);
                        payLinearLayout.startAnimation(animation2);
                        addItemLinearLayout.setVisibility(View.GONE);
                    } else {
                        addItemLinearLayout.clearAnimation();
                        addItemLinearLayout.setAnimation(animation2);
                        addItemLinearLayout.startAnimation(animation2);
                        payLinearLayout.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            animation2 = AnimationUtils.loadAnimation(context, R.anim.from_middle);
            animation2.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    //addItemLinearLayout.setVisibility(View.VISIBLE);

                    if (isPay) {
                        payLinearLayout.setVisibility(View.VISIBLE);
                        addItemLinearLayout.setVisibility(View.GONE);
                    } else {
                        payLinearLayout.setVisibility(View.GONE);
                        addItemLinearLayout.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            Log.w("Is tablet", "Land");
            mainLayout.findViewById(R.id.pay_cancel).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //payLinearLayout.setVisibility(View.GONE);
                    // slideToRight(payLinearLayout);
                    // payLinearLayout.setVisibility(View.GONE);
                    isPay = false;
                    toggleView();
                }
            });
           // add pay_in_new_sale
            mainLayout.findViewById(R.id.pay_in_new_sale).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (salesAndPurchaseItemList.size() >= 1) {
                        if (!payLinearLayout.isShown()) {
                            isPay = true;
                            // slideToLeft(payLinearLayout);
                            toggleView();
                        }
                    }
                }
            });
        }
        //endregion

        else {
            mainLayout.findViewById(R.id.pay_in_new_sale).setOnClickListener(v -> {

                if (salesAndPurchaseItemList.size() < 1) {
                    //  alertMaterialDialog.show();
                } else {
                    //POSUtil.showKeyboard(paidAmountTextInputEditTextPayMd);
                    // payMaterialDialog.show();
                    String noRemark = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_remark}).getString(0);

                    if (discount.getIsPercent() == 1) {
                        discountPercent = discount.getAmount() + " %";
                    } else {
                        discountPercent = "";
                    }


                    SalesHeader salesHeader = new SalesHeader();
                    salesHeader.setTotal(totalAmount);
                    salesHeader.setDisInPercent(discountPercent);
                    salesHeader.setTaxID(tax.getId());
                    salesHeader.setSubtotal(subtotal);
                    salesHeader.setType(type.toString());
                    salesHeader.setDiscountAmt(voucherDiscount);
                    salesHeader.setRemark(noRemark);
                    salesHeader.setPaidAmt(paidAmount);
                    salesHeader.setTaxAmt(taxAmt);
                    salesHeader.setTaxRate(tax.getRate());
                    salesHeader.setDay(Integer.toString(saleDay));
                    salesHeader.setMonth(Integer.toString(saleMonth));
                    salesHeader.setYear(Integer.toString(saleYear));
                    salesHeader.setSalesAndPurchaseItemList(salesAndPurchaseItemList);
                    salesHeader.setTaxType(tax.getType());
                    salesHeader.setDiscountID(discount.getId());
                    salesHeader.setUserId(currentUser.getId());
                    salesHeader.setSaleDelivery(deliveryView);
                    salesHeader.setSaleEdit(isSaleEdit);
                    salesHeader.setSaleHold(isHold);
                    if (isSaleEdit) {
                        salesHeader.setSaleVocherNo(saleHeaderView.getSaleVocherNo());
                        salesHeader.setCustomerID(saleHeaderView.getCustomerID());
                        salesHeader.setCustomerName(customerName);
                        salesHeader.setId(saleHeaderView.getId());
                        salesHeader.setDeleteList(deleteList);
                        salesHeader.setSaleDate(date);
                    }

                    Intent addPayIntent = new Intent(context, DetailActivity.class);
                    addPayIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.SALE_PAYMENT);
                    addPayIntent.putExtra("saleHeader", salesHeader);

                    getActivity().startActivityForResult(addPayIntent, 1);
                }
            });
            itemOrCodeAutocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    stockAutoCompleteView = stockNameOrCodeAutoCompleteAdapter.getSuggestionList().get(position);
                    //assigValuesForitemAdd(stockAutoCompleteView);
                    itemOrCodeAutocomplete.setText("");

                    // itemOrCodeAutocomplete.setSelection(itemOrCodeAutocomplete.getText().toString().length());
                    //itemOrCodeAutocomplete.selectAll();
                    // discountTextInputEditTextAddItemMd.requestFocus();

                    addSaleItem(stockAutoCompleteView);
                    POSUtil.hideKeyboard(mainLayout, getContext());

                    if (salesAndPurchaseItemList.contains(salesItemInSale)) {
                        SalesAndPurchaseItem salesAndPurchaseItem = salesAndPurchaseItemList.get(salesAndPurchaseItemList.indexOf(salesItemInSale));
                        salesAndPurchaseItem.setQty(salesAndPurchaseItem.getQty() + 1);
                        salesAndPurchaseItem.setTotalPrice((salesAndPurchaseItem.getQty() * salesAndPurchaseItem.getPrice()));
                        updateList.add(salesAndPurchaseItem);
                    } else {
                        salesAndPurchaseItemList.add(0, salesItemInSale);
                        updateList.add(salesItemInSale);
                    }

                    updateViewData();

                    foc = false;

                    salesItemInSale = new SalesAndPurchaseItem();
                }
            });
        }
    }

    private void toggleView() {
        addItemLinearLayout.clearAnimation();
        addItemLinearLayout.setAnimation(animation1);
        addItemLinearLayout.startAnimation(animation1);
    }

    private void setDataForPayMd() {
        changeTextViewPayMd.setEnabled(false);
        customerInPayDialog.setText(customerName);
        paidAmountTextInputEditTextPayMd.setError(null);
        totalAmountTextInputEditTextPayMd.setEnabled(false);
        totalAmountTextInputEditTextPayMd.setText(POSUtil.doubleToString(totalAmount));

        if (isSaleEdit) {
            paidAmountTextInputEditTextPayMd.setText(POSUtil.doubleToString(paidAmount));
        } else {
            paidAmountTextInputEditTextPayMd.setText(POSUtil.doubleToString(totalAmount));
        }
        paidAmountTextInputEditTextPayMd.setSelectAllOnFocus(true);
    }

    private void settingOnClickForPayMd() {
        paidAmountTextInputEditTextPayMd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (paidAmountTextInputEditTextPayMd.getText().toString().trim().length() > 0) {

                    Double changeAmount = Double.parseDouble(paidAmountTextInputEditTextPayMd.getText().toString()) - totalAmount;

                    if (changeAmount > 0) {

                        changeTextViewPayMd.setText(POSUtil.doubleToString(changeAmount));

                        //saleOustandting.setText("0.0");

                    } else {

                        changeTextViewPayMd.setText("0");

                        // saleOustandting.setText(POSUtil.NumberFormat(Math.abs(changeAmount)));
                    }
                }
            }
        });
    }

    private void settingOnClickForDiscountMD() {
        saveDiscountBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (discountTextInputEditTextMd.getText().toString().trim().length() >= 1) {
                    voucherDiscount = Double.parseDouble(discountTextInputEditTextMd.getText().toString().trim());
                } else {
                    voucherDiscount = 0.0;
                }

                updateViewData();
                addDiscountMaterialDialog.dismiss();
            }
        });
    }

    public void checkPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the requestPermissions.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA}, 200);
            } else {

                // No explanation needed, we can request the requestPermissions.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA}, 200);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    private void settingOnClickForEditSaleItem() {
        stockListBtnEditItemMd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stockListMaterialDialogEdit.show();
            }
        });

        rvAdapterforStockListMaterialDialogEdit.setmItemClickListener(new RVAdapterforStockListMaterialDialog.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                assigValuesForitemEdit(stockItemList.get(position));
                editSaleItem(stockItemList.get(position));
                stockListMaterialDialogEdit.dismiss();
            }
        });

        //        minusQtyAppCompatImageButtonEditItemMd.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //
        //                if (qtyTextInputEditTextEditItemMd.getText().toString().trim().length() > 0) {
        //
        //                    qty = Integer.parseInt(qtyTextInputEditTextEditItemMd.getText().toString());
        //                }
        //
        //                if (qty > 0) {
        //
        //                    qty--;
        //
        //                    qtyTextInputEditTextEditItemMd.setText(Integer.toString(qty));
        //
        //                    qtyTextInputEditTextEditItemMd.setSelection(qtyTextInputEditTextEditItemMd.getText().length());
        //
        //                }
        //
        //            }
        //        });
        //
        //        plusQtyAppCompatImageButtonEditItemMd.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //
        //                if (qtyTextInputEditTextEditItemMd.getText().toString().trim().length() > 0) {
        //
        //                    qty = Integer.parseInt(qtyTextInputEditTextEditItemMd.getText().toString());
        //
        //                }
        //
        //                if (qty >= 0) {
        //
        //                    qty++;
        //
        //                    qtyTextInputEditTextEditItemMd.setText(Integer.toString(qty));
        //
        //                    qtyTextInputEditTextEditItemMd.setSelection(qtyTextInputEditTextEditItemMd.getText().length());
        //
        //                }
        //
        //            }
        //        });

        focSwitchCompatEditItemMd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                foc = isChecked;

                if (foc) {
                    discountTextInputEditTextEditItemMd.setText(priceTextInputEditTextEditItemMd.getText().toString());
                } else {
                    discountTextInputEditTextEditItemMd.setText(null);
                }
            }
        });

        saveMdButtonEditItemMd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkVaildationForEditSaleItem()) {
                    getValuesFromEditSaleItemDialog();
                    rvSwipeAdapterForSaleItemViewInSale.notifyItemChanged(editposition);
                    editItemMaterialDialog.dismiss();

                    updateViewData();

                    if (!updateList.contains(salesAndPurchaseItemList.get(editposition))) {
                        updateList.add(salesAndPurchaseItemList.get(editposition));
                    }
                }
            }
        });

        discountTextInputEditTextAddItemMd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                Log.w("hello", Pattern.compile("([0-9])").matcher(discountTextInputEditTextAddItemMd.getText().toString().trim()).matches() + " S");

               /* if(discountTextInputEditTextAddItemMd.getText().toString().trim().length()>1&&Pattern.compile("([0-9][%])|([0-9])").matcher(discountTextInputEditTextAddItemMd.getText().toString().trim()).matches()) {


                    if (discountTextInputEditTextAddItemMd.getText().toString().trim().contains("%")) {

                        try{
                            discountRate= parseNumber(discountTextInputEditTextAddItemMd.getText().toString().trim());

                        }catch (NumberFormatException e){

                            discountTextInputEditTextAddItemMd.setError("Discount Must be % or amount(10% or 1000)");

                        }



                    } else {

                        salesItemInSale.setDiscountAmount(Double.valueOf(discountTextInputEditTextAddItemMd.getText().toString().trim()));

                    }

                }else if(discountTextInputEditTextAddItemMd.getText().toString().trim().length()>0){

                    discountTextInputEditTextAddItemMd.setError("Discount Must be % or amount(10% or 1000)");

                }*/
            }
        });
    }

    private Double parseNumber(String str) {

        Log.w("STRING", "STRING" + str);
        String output = "";
        Matcher match = Pattern.compile("([0-9])").matcher(str);
        while (match.find()) {
            output += match.group();
        }

        return Double.valueOf(output);
    }

    private void editSaleItem(StockAutoComplete stockAutoComplete) {
        salesAndPurchaseItemList.get(editposition).setDiscountAmount(0.0);
        salesAndPurchaseItemList.get(editposition).setBarcode(stockAutoComplete.getBarcode());
        salesAndPurchaseItemList.get(editposition).setStockCode(stockAutoComplete.getCodeNo());
        salesAndPurchaseItemList.get(editposition).setItemName(stockAutoComplete.getItemName());
        salesAndPurchaseItemList.get(editposition).setQty(1);
        qty = 1;
        salesAndPurchaseItemList.get(editposition).setStockID(stockAutoComplete.getId());
    }

    private void editSaleItem(StockItem stockItem) {
        salesAndPurchaseItemList.get(editposition).setDiscountAmount(0.0);
        salesAndPurchaseItemList.get(editposition).setBarcode(stockItem.getBarcode());
        salesAndPurchaseItemList.get(editposition).setStockCode(stockItem.getCodeNo());
        salesAndPurchaseItemList.get(editposition).setItemName(stockItem.getName());
        salesAndPurchaseItemList.get(editposition).setQty(1);
        qty = 1;
        salesAndPurchaseItemList.get(editposition).setStockID(stockItem.getId());
    }

    public void settingOnClickForAddSaleItemDialog() {
        stockListBtnAddItemMd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stockListMaterialDialog.show();
            }
        });

        rvAdapterforStockListMaterialDialog.setmItemClickListener(new RVAdapterforStockListMaterialDialog.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                assigValuesForitemAdd(stockItemList.get(position));
                addSaleItem(stockItemList.get(position));
                stockListMaterialDialog.dismiss();
            }
        });

        //        minusQtyAppCompatImageButtonAddItemMd.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //
        //                if (qtyTextInputEditTextAddItemMd.getText().toString().trim().length() > 0) {
        //
        //                    qty = Integer.parseInt(qtyTextInputEditTextAddItemMd.getText().toString());
        //
        //                }
        //
        //                if (qty > 0) {
        //
        //                    qty--;
        //
        //                    qtyTextInputEditTextAddItemMd.setText(Integer.toString(qty));
        //
        //                    qtyTextInputEditTextAddItemMd.setSelection(qtyTextInputEditTextAddItemMd.getText().length());
        //
        //                }
        //
        //            }
        //        });
        //
        //        plusQtyAppCompatImageButtonAddItemMd.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //
        //                if (qtyTextInputEditTextAddItemMd.getText().toString().trim().length() > 0) {
        //
        //                    qty = Integer.parseInt(qtyTextInputEditTextAddItemMd.getText().toString());
        //
        //                }
        //
        //                if (qty >= 0) {
        //
        //                    qty++;
        //
        //                    qtyTextInputEditTextAddItemMd.setText(Integer.toString(qty));
        //
        //                    qtyTextInputEditTextAddItemMd.setSelection(qtyTextInputEditTextAddItemMd.getText().length());
        //
        //                }
        //
        //            }
        //        });

        focSwitchCompatAddItemMd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                foc = isChecked;

                if (foc) {
                    //                    discountTextInputEditTextAddItemMd.setText(priceTextInputEditTextAddItemMd.getText().toString());
                } else {
                    discountTextInputEditTextAddItemMd.setText(null);
                }
            }
        });

        saveMdButtonAddItemMd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNewStockAdd) {

                }

                if (!isEdit) {

                    if (checkVaildationFroAddSaleItem()) {
                        getValuesFromAddSaleItemDialog();

                        salesAndPurchaseItemList.add(salesItemInSale);
                        refreshRecyclerView();

                        addItemMaterialDialog.dismiss();
                        updateViewData();

                        updateList.add(salesItemInSale);
                        foc = false;
                        salesItemInSale = new SalesAndPurchaseItem();
                        clearInputAddItemMaterialDialog();
                    }
                } else {

                    if (checkVaildationForEditSaleItem()) {

                        getValuesFromEditSaleItemDialog();

                        rvSwipeAdapterForSaleItemViewInSale.notifyItemChanged(editposition);

                        editItemMaterialDialog.dismiss();

                        updateViewData();

                        if (!updateList.contains(salesAndPurchaseItemList.get(editposition))) {
                            updateList.add(salesAndPurchaseItemList.get(editposition));
                        }
                        clearInputAddItemMaterialDialog();
                    }
                }
            }
        });
    }

    public void getValuesFromAddSaleItemDialog() {
        salesItemInSale.setItemName(itemNameTextInputEditTextAddItemMd.getText().toString().trim());
        salesItemInSale.setStockCode(itemCodeTextInputEditTextAddItemMd.getText().toString().trim());
        salesItemInSale.setQty(qty);

        //        salesItemInSale.setPrice(Double.parseDouble(priceTextInputEditTextAddItemMd.getText().toString().trim()));

        if (discountTextInputEditTextAddItemMd.getText().toString().trim().length() > 0) {

            salesItemInSale.setDiscountAmount(Double.parseDouble(discountTextInputEditTextAddItemMd.getText().toString().trim()));
        } else {
            salesItemInSale.setDiscountAmount(0.0);
        }

        Stock stock = stockManager.findStockByStockCode(salesItemInSale.getStockCode());

        if (stock.getStockCode() == null) {

            //salesItemInSale.setStockID(stockManager.addQuickStockSetupSale(salesItemInSale.getItemName(), salesItemInSale.getStockCode(),
            //    salesItemInSale.getPrice(), ));

        }

        salesItemInSale.setTotalPrice(calculatePrice(salesItemInSale.getPrice(), qty, salesItemInSale.getDiscountAmount()));
    }

    public void getValuesFromEditSaleItemDialog() {
        salesAndPurchaseItemList.get(editposition).setQty(qty);

        if (discountTextInputEditTextEditItemMd.getText().toString().trim().length() > 0) {
            salesAndPurchaseItemList.get(editposition).setDiscountAmount(Double.parseDouble(discountTextInputEditTextEditItemMd.getText().toString().trim()));
        }
        salesAndPurchaseItemList.get(editposition).setPrice(Double.parseDouble(priceTextInputEditTextEditItemMd.getText().toString().trim()));
        salesAndPurchaseItemList.get(editposition).setTotalPrice(calculatePrice(salesAndPurchaseItemList.get(editposition).getPrice(), qty, salesAndPurchaseItemList.get(editposition).getDiscountAmount()));
    }

    public boolean checkVaildationFroAddSaleItem() {
        boolean status = true;

        if (!POSUtil.isTabetLand(context)) {
            if (itemOrCodeAutocomplete.getText().toString().trim().length() < 1) {
                status = false;
                String itemCode = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_itemcode_or_choose}).getString(0);
                itemOrCodeAutocomplete.setError(itemCode);
            }
        }

        if (itemCodeTextInputEditTextAddItemMd.getText().toString().trim().length() < 1) {

            status = false;

            String itemCode = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_itemcode_or_choose}).getString(0);
            itemCodeTextInputEditTextAddItemMd.setError(itemCode);
        }

        if (itemNameTextInputEditTextAddItemMd.getText().toString().trim().length() < 1) {

            status = false;

            String itemName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_name_or_choose}).getString(0);
            itemNameTextInputEditTextAddItemMd.setError(itemName);
        }

        //        if (priceTextInputEditTextAddItemMd.getText().toString().trim().length() < 1) {
        //
        //            status = false;
        //
        //            String price = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_price_or_choose}).getString(0);
        //            priceTextInputEditTextAddItemMd.setError(price);
        //
        //        }

        //        if (qtyTextInputEditTextAddItemMd.getText().toString().trim().length() < 1 || Integer.parseInt(qtyTextInputEditTextAddItemMd.getText().toString().trim()) < 1) {
        //
        //            status = false;
        //
        //            String qty = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_qty_greater_than_0}).getString(0);
        //            qtyTextInputEditTextAddItemMd.setError(qty);
        //
        //        }

        Log.w("Status", status + " SS");

        return status;
    }

    public boolean checkVaildationForEditSaleItem() {

        boolean status = true;

        if (itemCodeTextInputEditTextEditItemMd.getText().toString().trim().length() < 1) {

            status = false;

            String enterItemCode = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_itemcode_or_choose}).getString(0);
            itemCodeTextInputEditTextEditItemMd.setError(enterItemCode);
        } else {

            Stock stock = stockManager.findStockByStockCode(itemCodeTextInputEditTextEditItemMd.getText().toString().trim());

            if (stock.getStockCode() == null) {

                status = false;

                String invalid = context.getTheme().obtainStyledAttributes(new int[]{R.attr.invalid_stock_code}).getString(0);
                itemCodeTextInputEditTextEditItemMd.setError(invalid);
            }
        }

        if (itemNameTextInputEditTextEditItemMd.getText().toString().trim().length() < 1) {

            status = false;

            String name = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_name_or_choose}).getString(0);
            itemNameTextInputEditTextEditItemMd.setError(name);
        }

        if (priceTextInputEditTextEditItemMd.getText().toString().trim().length() < 1) {

            status = false;

            String price = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_price_or_choose}).getString(0);
            priceTextInputEditTextEditItemMd.setError(price);
        }
        if (qtyTextInputEditTextEditItemMd.getText().toString().trim().length() < 1 || Integer.parseInt(qtyTextInputEditTextEditItemMd.getText().toString().trim()) < 1) {

            status = false;

            String qty = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_qty_greater_than_0}).getString(0);
            qtyTextInputEditTextEditItemMd.setError(qty);
        }

        return status;
    }

    public void assigValuesForitemAdd(StockItem stockItem) {
        if (!itemCodeTextInputEditTextAddItemMd.getText().toString().trim().equalsIgnoreCase(stockItem.getCodeNo().toString().trim())) {
            itemCodeTextInputEditTextAddItemMd.setText(stockItem.getCodeNo().trim());
        }
        //itemCodeTextInputEditTextAddItemMd.setSelection(stockItem.getCodeNo().length());
        itemNameTextInputEditTextAddItemMd.setText(stockItem.getName());
        // itemNameTextInputEditTextAddItemMd.setSelection(stockItem.getName().length());
        //        qtyTextInputEditTextAddItemMd.setText("1");
        // qtyTextInputEditTextAddItemMd.requestFocus();
        //        qtyTextInputEditTextAddItemMd.setSelection(qtyTextInputEditTextAddItemMd.getText().length());
        foc = false;
        //        priceTextInputEditTextAddItemMd.setText(stockItem.getRetailPrice().toString());
        //        priceTextInputEditTextAddItemMd.setSelection(stockItem.getRetailPrice().toString().length());
        //priceTextInputLayoutAddItemMd = (TextInputLayout) addItemMaterialDialog.findViewById(R.saleInvoiceNo.code_no_in_sale_purchase_TIET);
        focSwitchCompatAddItemMd.setChecked(false);
        discountTextInputEditTextAddItemMd.setText(null);
    }

    public void assigValuesForitemEdit(StockItem stockItem) {
        if (!itemCodeTextInputEditTextEditItemMd.getText().toString().trim().equalsIgnoreCase(stockItem.getCodeNo().toString().trim())) {
            itemCodeTextInputEditTextEditItemMd.setText(stockItem.getCodeNo().trim());
        }

        itemCodeTextInputEditTextEditItemMd.setText(stockItem.getCodeNo());
        //itemCodeTextInputEditTextEditItemMd.setSelection(stockItem.getCodeNo().length());
        itemNameTextInputEditTextEditItemMd.setText(stockItem.getName());
        // itemNameTextInputEditTextEditItemMd.setSelection(stockItem.getName().length());
        qtyTextInputEditTextEditItemMd.setText("1");
        //qtyTextInputEditTextEditItemMd.requestFocus();
        qtyTextInputEditTextEditItemMd.setSelection(qtyTextInputEditTextEditItemMd.getText().length());
        foc = false;
        priceTextInputEditTextEditItemMd.setText(POSUtil.doubleToString(stockItem.getRetailPrice()));
        priceTextInputEditTextEditItemMd.setSelection(stockItem.getRetailPrice().toString().length());
        //priceTextInputLayoutAddItemMd = (TextInputLayout) addItemMaterialDialog.findViewById(R.saleInvoiceNo.code_no_in_sale_purchase_TIET);
        focSwitchCompatEditItemMd.setChecked(false);
        discountTextInputEditTextAddItemMd.setText(null);
    }

    public void assigValuesForitemEdit(StockAutoComplete stockView) {
        if (!itemCodeTextInputEditTextEditItemMd.getText().toString().trim().equalsIgnoreCase(stockView.getCodeNo().toString().trim())) {
            itemCodeTextInputEditTextEditItemMd.setText(stockView.getCodeNo().trim());
        }

        // itemCodeTextInputEditTextEditItemMd.setSelection(stockView.getCodeNo().length());
        itemNameTextInputEditTextEditItemMd.setText(stockView.getItemName());
        // itemNameTextInputEditTextEditItemMd.setSelection(stockView.getItemName().length());
        qtyTextInputEditTextEditItemMd.setText("1");
        // qtyTextInputEditTextEditItemMd.requestFocus();
        qtyTextInputEditTextEditItemMd.setSelection(qtyTextInputEditTextEditItemMd.getText().length());
        foc = false;
        priceTextInputEditTextEditItemMd.setText(POSUtil.doubleToString(stockView.getRetailPrice()));
        priceTextInputEditTextEditItemMd.setSelection(stockView.getRetailPrice().toString().length());
        //priceTextInputLayoutAddItemMd = (TextInputLayout) addItemMaterialDialog.findViewById(R.saleInvoiceNo.code_no_in_sale_purchase_TIET);

        focSwitchCompatEditItemMd.setChecked(false);
        discountTextInputEditTextAddItemMd.setText(null);
    }

    public void assigValuesForitemAdd(StockAutoComplete stockView) {

        if (!itemCodeTextInputEditTextAddItemMd.getText().toString().trim().equalsIgnoreCase(stockView.getCodeNo().toString().trim())) {
            itemCodeTextInputEditTextAddItemMd.setText(stockView.getCodeNo().trim());
        }

        itemNameTextInputEditTextAddItemMd.setText(stockView.getItemName());

        //itemNameTextInputEditTextAddItemMd.setSelection(stockView.getItemName().length());

        //        qtyTextInputEditTextAddItemMd.setText("1");

        // qtyTextInputEditTextAddItemMd.requestFocus();

        //        qtyTextInputEditTextAddItemMd.setSelection(qtyTextInputEditTextAddItemMd.getText().length());

        foc = false;

        //        priceTextInputEditTextAddItemMd.setText(POSUtil.doubleToString(stockView.getRetailPrice()));
        //
        //        priceTextInputEditTextAddItemMd.setSelection(stockView.getRetailPrice().toString().length());
        //priceTextInputLayoutAddItemMd = (TextInputLayout) addItemMaterialDialog.findViewById(R.saleInvoiceNo.code_no_in_sale_purchase_TIET);

        focSwitchCompatAddItemMd.setChecked(false);
        discountTextInputEditTextAddItemMd.setText(null);
    }

    private void loadUI() {
        holdBtn = mainLayout.findViewById(R.id.hold);
        addNewStock = mainLayout.findViewById(R.id.add_new_stock);
        barcodeBtn = mainLayout.findViewById(R.id.barcode);

        String connecting = ThemeUtil.getString(context, R.attr.connecting);

        View processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
        ImageView imageView = (ImageView) processDialog.findViewById(R.id.image_view);
        imageView.setBackgroundResource(R.drawable.spin_animation);
        AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
        drawable.start();

        statusTextView = (TextView) mainLayout.findViewById(R.id.bt_status);
        printDeviceNameTextView = (TextView) mainLayout.findViewById(R.id.device_name);
        saleDetailPrint = (FrameLayout) mainLayout.findViewById(R.id.sale_detail_print);
        itemOrCodeAutocomplete = (AutoCompleteTextView) mainLayout.findViewById(R.id.item_name_or_code);
        payLinearLayout = (LinearLayout) mainLayout.findViewById(R.id.pay_view);
        addItemLinearLayout = (LinearLayout) mainLayout.findViewById(R.id.add_item_view);
        keyBoardView = (KeyBoardView) mainLayout.findViewById(R.id.key_board);
        deliveryTextView = (TextView) mainLayout.findViewById(R.id.delivery_charges_in_new_sale_tv);
        customerInPayDialog = (TextView) payMaterialDialog.findViewById(R.id.customer_in_sale_change);
        saleItemRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.sale_item_list_rv);
        saleIdTextView = (TextView) mainLayout.findViewById(R.id.sale_id_in_new_sale_tv);
        dateTextView = (TextView) mainLayout.findViewById(R.id.sale_date_in_new_sale_tv);
        dateLinearLayout = (LinearLayout) mainLayout.findViewById(R.id.sale_date_in_new_sale_ll);
        customerTextInputEditText = (AppCompatAutoCompleteTextView) mainLayout.findViewById(R.id.customer);
        phoneNoEditText = (EditText) mainLayout.findViewById(R.id.phone_no);
        taxRateTextView = (TextView) mainLayout.findViewById(R.id.tax_rate_in_new_sale_tv);
        taxAmtTextView = (TextView) mainLayout.findViewById(R.id.tax_amt_in_new_sale_tv);
        discountTextView = (TextView) mainLayout.findViewById(R.id.discount_in_new_sale_tv);
        subtotalTextView = (TextView) mainLayout.findViewById(R.id.subtotal_in_new_sale_tv);
        totalTextView = (TextView) mainLayout.findViewById(R.id.total_in_new_sale_tv);
        discountTextViewBtn = (ImageButton) mainLayout.findViewById(R.id.discount_in_new_sale_tv_btn);
        taxTextViewBtn = (AppCompatImageButton) mainLayout.findViewById(R.id.tax_in_new_sale_tv_btn);
        deliveryImageViewBtn = (ImageView) mainLayout.findViewById(R.id.delivery_in_new_sale_imgv_btn);
        addSaletemItemBtn = (Button) mainLayout.findViewById(R.id.add_item_in_new_sale_btn);

        loadDeliveryUI();

        discountTextInputEditTextMd = (EditText) addDiscountMaterialDialog.findViewById(R.id.add_vocher_disc_in_sale_md);
        saveDiscountBtn = (Button) addDiscountMaterialDialog.findViewById(R.id.save);
        cancelDiscountBtn = (Button) addDiscountMaterialDialog.findViewById(R.id.cancel);
        cancelOrderBtn = (Button) deliveryMaterialDialog.findViewById(R.id.cancel);
        saveDeliverBtn = (Button) deliveryMaterialDialog.findViewById(R.id.save);
        deliverLayout = (LinearLayout) deliveryMaterialDialog.findViewById(R.id.deliver_layout);
        customerImageButton = (AppCompatImageButton) mainLayout.findViewById(R.id.customer_image_button);

        View view;
        if (POSUtil.isTabetLand(context)) {
            loadUIFromAddItemMaterialDialog(mainLayout);
            loadUIFromEditItemMaterialDialog(mainLayout);

            view = mainLayout;
        } else {
            loadUIFromAddItemMaterialDialog(addItemMaterialDialog.getView());
            loadUIFromEditItemMaterialDialog(editItemMaterialDialog.getView());

            view = payMaterialDialog.getView();
        }

        totalAmountTextInputEditTextPayMd = (EditText) view.findViewById(R.id.total_amount_in_sale_change);
        paidAmountTextInputEditTextPayMd = (EditText) view.findViewById(R.id.paid_amount_in_sale_change);
        saleOustandting = (EditText) view.findViewById(R.id.oustanding_in_sale_change);
        changeTextViewPayMd = (EditText) view.findViewById(R.id.change_in_sale_change);
        saveTransaction = (Button) view.findViewById(R.id.save);
        printTransaction = (Button) view.findViewById(R.id.print);
        customerNameTextView = (AutoCompleteTextView) view.findViewById(R.id.customer_in_sale_change);
        customerPhoneNoEditText = (EditText) view.findViewById(R.id.ph_in_sale_change);

        //        customerTextInputEditText         = (AppCompatAutoCompleteTextView) mainLayout.findViewById(R.id.customer);
        //        customerListBtn = (ImageButton) payMaterialDialog.findViewById(R.saleInvoiceNo.customer_list_btn);

    }

    public void buildDeliveryAndPickupDialog() {
        String delivery = ThemeUtil.getString(context, R.attr.delivery);
        String cancel = ThemeUtil.getString(context, R.attr.cancel);

        deliveryMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.delivery_and_pick_up_md, true)
                .title(delivery).neutralText(cancel)
                .build();
    }

    public void loadAddItemDialog() {
        String addSaleItem = ThemeUtil.getString(context, R.attr.add_sale_item);

        addItemMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.add_item_sale_purchase, true)
                .title(addSaleItem)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    public void loadNewItemDialog() {
        String addSaleItem = ThemeUtil.getString(context, R.attr.stock);

        addNewStockMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.add_new_stock_quick, true)
                .title(addSaleItem)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

        itemName = (EditText) addNewStockMaterialDialog.findViewById(R.id.item_name);
        codeNo = (EditText) addNewStockMaterialDialog.findViewById(R.id.code_no);
        retailPrice = (EditText) addNewStockMaterialDialog.findViewById(R.id.retail_price);
        purchasePrice = (EditText) addNewStockMaterialDialog.findViewById(R.id.purchase_price);

        addNewStockMaterialDialog.findViewById(R.id.cancel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewStockMaterialDialog.dismiss();
            }
        });

        addNewStockMaterialDialog.findViewById(R.id.save).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                StockItem stockItem = new StockItem();

                if (validateStock()) {
                    stockItem.setName(itemName.getText().toString());
                    stockItem.setCodeNo(codeNo.getText().toString());
                    stockItem.setRetailPrice(Double.valueOf(retailPrice.getText().toString()));
                    stockItem.setPurchasePrice(Double.valueOf(purchasePrice.getText().toString()));
                }

                Log.w("IS VAILID", stockItem.isVaild() + " ");

                if (stockItem.isVaild()) {
                    stockManager.addQuickStockSetupSale(stockItem.getName(), stockItem.getCodeNo(), stockItem.getRetailPrice(), stockItem.getPurchasePrice());

                    addNewStockMaterialDialog.dismiss();
                } else {
                    Log.e("INVALID ", "STOCK WHILE ADDING QUICK SETUP");
                }
            }
        });
    }

    public boolean validateStock() {

        boolean status = true;

        if (itemName.getText().toString().trim().length() < 1) {
            status = false;
            itemName.setError("Invalid Item Name");
        }

        if (codeNo.getText().toString().trim().length() < 1) {
            status = false;
            codeNo.setError("Invalid Code Number");
        }

        if (retailPrice.getText().toString().trim().length() < 1) {
            status = false;
            retailPrice.setError("Invalid Item Name");
        }

        if (purchasePrice.getText().toString().trim().length() < 1) {
            status = false;
            purchasePrice.setError("Invalid Item Name");
        }

        return status;
    }

    public void loadEditItemDialog() {
        String editSaleitem = ThemeUtil.getString(context, R.attr.edit_sale_item);
        editItemMaterialDialog = new MaterialDialog
                .Builder(context)
                .customView(R.layout.add_item_sale_purchase, true)
                .title(editSaleitem)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    public void clearInputAddItemMaterialDialog() {
        //        qtyTextInputEditTextAddItemMd.setText("0");
        //        priceTextInputEditTextAddItemMd.setText(null);
        //        priceTextInputEditTextAddItemMd.setError(null);
        //        qtyTextInputEditTextAddItemMd.setError(null);
        //priceTextInputLayoutAddItemMd = (TextInputLayout) addItemMaterialDialog.findViewById(R.saleInvoiceNo.code_no_in_sale_purchase_TIET);

        if (itemOrCodeAutocomplete != null) {
            itemOrCodeAutocomplete.setText("");
        }

        itemCodeTextInputEditTextAddItemMd.setError(null);
        itemCodeTextInputEditTextAddItemMd.setText(null);
        itemNameTextInputEditTextAddItemMd.setText(null);
        itemNameTextInputEditTextAddItemMd.setError(null);
        focSwitchCompatAddItemMd.setChecked(false);
        discountTextInputEditTextAddItemMd.setText(null);
    }

    public void setInputEditItemMaterialDialog() {
        // qtyTextInputEditTextEditItemMd.requestFocus();

        itemCodeTextInputEditTextEditItemMd.setText(salesAndPurchaseItemList.get(editposition).getStockCode());
        itemNameTextInputEditTextEditItemMd.setText(salesAndPurchaseItemList.get(editposition).getItemName());
        qtyTextInputEditTextEditItemMd.setText(Integer.toString(salesAndPurchaseItemList.get(editposition).getQty()));
        qtyTextInputEditTextEditItemMd.setSelection(qtyTextInputEditTextEditItemMd.getText().length());
        priceTextInputEditTextEditItemMd.setText(POSUtil.doubleToString((salesAndPurchaseItemList.get(editposition).getPrice())));

        if (salesAndPurchaseItemList.get(editposition).getDiscountAmount() == salesAndPurchaseItemList.get(editposition).getTotalPrice()) {
            focSwitchCompatEditItemMd.setChecked(true);
        } else {
            focSwitchCompatEditItemMd.setChecked(false);
        }

        discountTextInputEditTextEditItemMd.setText(POSUtil.doubleToString((salesAndPurchaseItemList.get(editposition).getDiscountAmount())));
    }

    public PopupWindow popupWindowBuild() {

        // initialize a pop up window type
        popupWindow = new PopupWindow(getContext());
        popupWindow.setFocusable(true);

        //        Log.w("WIDTH",priceTextInputEditTextEditItemMd.getWidth()+" SS");

        // set the list view as pop up window content

        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = LayoutInflater.from(getContext()).inflate(R.layout.price_list_item_view, null);

        retailPriceDropDown = view.findViewById(R.id.retail_sale_price_amt);
        wholeSalePriceDropDown = view.findViewById(R.id.whole_sale_price_amt);
        retailLinearLayout = view.findViewById(R.id.retail_ll);
        wholeLinearLayout = view.findViewById(R.id.whole_ll);

        retailLinearLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                priceTextInputEditTextEditItemMd.setText(retailPriceDropDown.getText().toString());
                popupWindow.dismiss();
            }
        });
        wholeLinearLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                priceTextInputEditTextEditItemMd.setText(wholeSalePriceDropDown.getText().toString());
                popupWindow.dismiss();
            }
        });

        popupWindow.setContentView(view);

        return popupWindow;
    }

    public void loadUIFromAddItemMaterialDialog(View view) {
        //            dropDownPriceBtn=mainLayout.findViewById(R.id.drop_down);
        //            qtyTextInputEditTextAddItemMd = (EditText) mainLayout.findViewById(R.id.qty_in_sale_purchase_TIET);
        //            minusQtyAppCompatImageButtonAddItemMd = (AppCompatImageButton) mainLayout.findViewById(R.id.minus_qty_in_sale_purchase_ImgBtn);
        //            plusQtyAppCompatImageButtonAddItemMd = (AppCompatImageButton) mainLayout.findViewById(R.id.plus_qty_in_sale_purchase_ImgBtn);
        //            priceTextInputEditTextAddItemMd = (EditText) mainLayout.findViewById(R.id.sale_price_in_sale_purchase_TIET);

        img = (ImageButton) view.findViewById(R.id.img);
        stockListBtnAddItemMd = (AppCompatImageButton) view.findViewById(R.id.stock_list_in_add_sale_item_btn);
        itemCodeTextInputEditTextAddItemMd = (TextView) view.findViewById(R.id.code_no_in_sale_purchase_TIET);
        itemNameTextInputEditTextAddItemMd = (TextView) view.findViewById(R.id.item_name_in_sale_purchase_TIET);
        focSwitchCompatAddItemMd = (SwitchCompat) view.findViewById(R.id.foc_in_sale_purchase_SC);
        discountTextInputEditTextAddItemMd = (EditText) view.findViewById(R.id.item_discount_in_sale_purchase_TIET);
        saveMdButtonAddItemMd = (Button) view.findViewById(R.id.save);
        cancelMdButtonAddItemMd = (Button) view.findViewById(R.id.cancel);
    }

    public void loadUIFromEditItemMaterialDialog(View view) {
        //            dropDownPriceBtn= (ImageButton) editItemMaterialDialog.findViewById(R.id.drop_down);
        //            qtyTextInputEditTextEditItemMd = (EditText) editItemMaterialDialog.findViewById(R.id.qty_in_sale_purchase_TIET);
        //            minusQtyAppCompatImageButtonEditItemMd = (AppCompatImageButton) editItemMaterialDialog.findViewById(R.id.minus_qty_in_sale_purchase_ImgBtn);
        //            plusQtyAppCompatImageButtonEditItemMd = (AppCompatImageButton) editItemMaterialDialog.findViewById(R.id.plus_qty_in_sale_purchase_ImgBtn);
        //            priceTextInputEditTextEditItemMd = (EditText) editItemMaterialDialog.findViewById(R.id.sale_price_in_sale_purchase_TIET);

        imgEdit = (ImageButton) view.findViewById(R.id.img);
        stockListBtnEditItemMd = (AppCompatImageButton) view.findViewById(R.id.stock_list_in_add_sale_item_btn);
        itemCodeTextInputEditTextEditItemMd = (TextView) view.findViewById(R.id.code_no_in_sale_purchase_TIET);
        itemNameTextInputEditTextEditItemMd = (TextView) view.findViewById(R.id.item_name_in_sale_purchase_TIET);
        focSwitchCompatEditItemMd = (SwitchCompat) view.findViewById(R.id.foc_in_sale_purchase_SC);
        discountTextInputEditTextEditItemMd = (EditText) view.findViewById(R.id.item_discount_in_sale_purchase_TIET);
        saveMdButtonEditItemMd = (Button) view.findViewById(R.id.save);
    }

    public void buildStockListDialog() {
        rvAdapterforStockListMaterialDialog = new RVAdapterforStockListMaterialDialog(stockItemList);

        stockListMaterialDialog = new MaterialDialog.Builder(context).
                adapter(rvAdapterforStockListMaterialDialog, new LinearLayoutManager(context)).build();

        stockListMaterialDialog.getRecyclerView().addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {
            }

            @Override
            public void onScrollDown() {
            }

            @Override
            public void onLoadMore() {
                rvAdapterforStockListMaterialDialog.setShowLoader(true);

                // load more
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        stockItemList.addAll(stockManager.getAllActiveStocks(stockItemList.size(), 9));
                        rvAdapterforStockListMaterialDialog.setShowLoader(false);

                        refreshStockList();
                    }
                }, 500);
            }
        });
    }

    private void refreshStockList() {
        rvAdapterforStockListMaterialDialog.setStockItemList(stockItemList);
        rvAdapterforStockListMaterialDialog.notifyDataSetChanged();
    }

    public void buildCustomerListDialog() {
        customerListMaterialDialog = new MaterialDialog.Builder(context).
                adapter(rvAdapterForCustomerMD, new LinearLayoutManager(context)).build();
    }

    public void buildStockListDialogEdit() {
        rvAdapterforStockListMaterialDialogEdit = new RVAdapterforStockListMaterialDialog(stockItemList);

        stockListMaterialDialogEdit = new MaterialDialog.Builder(context).
                adapter(rvAdapterforStockListMaterialDialogEdit, new LinearLayoutManager(context)).
                build();
    }

    public void buildTaxListDialogEdit() {
        if (isEdit) {
            rvAdapterForTaxMD = new RVAdapterForTaxMD(taxList, saleHeaderView.getTaxID());
        } else {
            rvAdapterForTaxMD = new RVAdapterForTaxMD(taxList, tax.getId());
        }

        taxListMaterialDialog = new MaterialDialog.Builder(context).
                adapter(rvAdapterForTaxMD, new LinearLayoutManager(context)).build();
    }

    public void buildAddDiscountDialog() {
        String discount = ThemeUtil.getString(context, R.attr.discount);

        addDiscountMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.add_discount_dialog, true)
                .title(discount)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    public void buildPayDialog() {
        String payment = ThemeUtil.getString(context, R.attr.payment);
        payMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.sale_change, true)
                .title(payment)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    private Double calculatePrice(Double salePrice, int qty, Double discAmt) {
        Double totalPrice = 0.0;

        if (foc) {
            return 0.0;
        } else {
            salePrice -= discAmt;
            totalPrice = qty * salePrice;

            return totalPrice;
        }
    }

    //    private void updateViewDataForBarScan() {
    //
    //        calculateValues();
    //
    //        //   new Handler().postDelayed(new Runnable() {
    //        //       @Override
    //        //       public void run() {
    //        //           barCodeTIET.setText(null);
    //        //       }
    //        //   },1000);
    //
    //        if (deliveryView != null) {
    //            deliveryCharges = deliveryView.getCharges();
    //            deliveryTextView.setText(Double.toString(deliveryView.getCharges()));
    //        } else {
    //            deliveryTextView.setText("0");
    //            deliveryCharges = 0.0;
    //        }
    //
    //        taxRateTextView.setText(tax.getRate().toString());
    //        taxAmtTextView.setText(POSUtil.NumberFormat(taxAmt));
    //        discountTextView.setText(POSUtil.NumberFormat(voucherDiscount));
    //        subtotalTextView.setText(POSUtil.NumberFormat(subtotal));
    //        totalTextView.setText(POSUtil.NumberFormat(totalAmount));
    //    }

    private void updateViewData() {
        refreshRecyclerView();
        Log.e("deli", deliveryView.getCharges() + " ");

        if (deliveryView.getCharges() != null) {
            deliveryCharges = deliveryView.getCharges();
            deliveryTextView.setText(POSUtil.doubleToString(deliveryCharges));
        } else {
            deliveryTextView.setText("0");
            deliveryCharges = 0.0;
        }

        calculateValues();

        taxRateTextView.setText(tax.getRate().toString());
        taxAmtTextView.setText(POSUtil.doubleToString(taxAmt));
        discountTextView.setText(POSUtil.NumberFormat(voucherDiscount));
        subtotalTextView.setText(POSUtil.NumberFormat(subtotal));
        totalTextView.setText(POSUtil.NumberFormat(totalAmount));
    }

    private void calculateValues() {
        Double voucherTax;
        subtotal = 0.0;

        for (SalesAndPurchaseItem s : salesAndPurchaseItemList) {

            subtotal += s.getTotalPrice();
        }

        Log.w("Tax type", tax.getType());

        if (tax.getType().equalsIgnoreCase(Tax.TaxType.Inclusive.toString())) {

            voucherTax = 0.0;
            taxAmt = (subtotal / 100) * tax.getRate();
        } else {

            voucherTax = (subtotal / 100) * tax.getRate();

            taxAmt = voucherTax;
        }
        if (discount.getIsPercent() == 0) {
            voucherDiscount = discount.getAmount();
        } else {
            Log.e("total ", "amt " + subtotal);
            voucherDiscount = discount.getAmount() * subtotal / 100;
        }

        totalAmount = subtotal + voucherTax + deliveryCharges - voucherDiscount;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == SCANNER_BAR_CODE) {
            // Handle scan intent
            if (resultCode == Activity.RESULT_OK) {
                // Handle successful scan
                String contents = intent.getStringExtra("SCAN_RESULT");
                String formatName = intent.getStringExtra("SCAN_RESULT_FORMAT");
                byte[] rawBytes = intent.getByteArrayExtra("SCAN_RESULT_BYTES");
                int intentOrientation = intent.getIntExtra("SCAN_RESULT_ORIENTATION", Integer.MIN_VALUE);
                Integer orientation = (intentOrientation == Integer.MIN_VALUE) ? null : intentOrientation;
                String errorCorrectionLevel = intent.getStringExtra("SCAN_RESULT_ERROR_CORRECTION_LEVEL");

                itemOrCodeAutocomplete.clearListSelection();
                itemOrCodeAutocomplete.setText(contents);
                itemOrCodeAutocomplete.setSelection(itemOrCodeAutocomplete.getText().toString().length());
                itemOrCodeAutocomplete.setSelected(true);
                itemOrCodeAutocomplete.setSelectAllOnFocus(true);
                itemOrCodeAutocomplete.selectAll();
                addSaleItem();

                //                new Handler().postDelayed(new Runnable() {
                //                        @Override
                //                        public void run() {
                //                            itemOrCodeAutocomplete.setText("");
                //                        }
                //                },1000);
            }
        }  // Handle other intents
    }

    @Override
    public void onActionDone(Discount discount) {
        discountDialogFragment.dismiss();
        this.discount = discount;
        discountDialogFragment.setDefaultDiscount(discount);
        updateViewData();
    }

}