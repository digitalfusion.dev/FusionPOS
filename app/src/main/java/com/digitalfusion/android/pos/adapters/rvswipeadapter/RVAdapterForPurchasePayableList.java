package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.content.res.TypedArray;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.PurchaseHistory;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 10/24/16.
 */

public class RVAdapterForPurchasePayableList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    public int currentDate;
    protected boolean showLoader = false;
    private List<PurchaseHistory> purchasePayableList;
    private ClickListener moreClickListener;
    private ClickListener editClickListener;
    private ClickListener viewDetailListener;
    private ClickListener viewPaymentListener;
    private ClickListener addPaymentListener;
    private ClickListener mItemClickListerner;
    private LoaderViewHolder loaderViewHolder;

    public RVAdapterForPurchasePayableList(List<PurchaseHistory> purchasePayableList) {
        this.purchasePayableList = purchasePayableList;
        Calendar now = Calendar.getInstance();

        currentDate = Integer.parseInt(DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)),
                Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(now.get(Calendar.DAY_OF_MONTH))));


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEWTYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.payable_item_view, parent, false);

            return new PayableViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            return new LoaderViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof PayableViewHolder) {

            final PayableViewHolder viewHolder = (PayableViewHolder) holder;

            POSUtil.makeZebraStrip(viewHolder.itemView, position);

            viewHolder.saleIdTextView.setText("#" + purchasePayableList.get(position).getVoucherNo());

            viewHolder.customerTextView.setText(purchasePayableList.get(position).getSupplierName());

            viewHolder.totalAmountTextView.setText(POSUtil.NumberFormat(purchasePayableList.get(position).getBalance()));

            // String dayDes[]= DateUtility.dayDes(purchasePayableList.get(position).getDate());

            //   String yearMonthDes= DateUtility.monthYearDes(purchasePayableList.get(position).getDate());

            //  viewHolder.dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));
            int date = Integer.parseInt(purchasePayableList.get(position).getDate());

            if (date == currentDate) {

                viewHolder.dateTextView.setText(viewHolder.today.getString(0));

            } else if (date == currentDate - 1) {

                viewHolder.dateTextView.setText(viewHolder.yesterday.getString(0));

            } else {

                viewHolder.dateTextView.setText(DateUtility.makeDateFormatWithSlash(purchasePayableList.get(position).getYear(),
                        purchasePayableList.get(position).getMonth(),
                        purchasePayableList.get(position).getDay()));

            }
            viewHolder.addPaymentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (addPaymentListener != null) {
                        addPaymentListener.onClick(position);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                viewHolder.swipeLayout.close();
                            }
                        }, 500);
                    }
                }
            });

            viewHolder.viewDetailBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewDetailListener != null) {

                        viewDetailListener.onClick(position);
                    }
                }
            });

      /*      viewHolder.moreButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PopupMenu popupMenu=new PopupMenu(v.getContext(),viewHolder.moreButton);
                    popupMenu.getMenuInflater().inflate(R.menu.outstanding_popup_menu,popupMenu.getMenu());

                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            if(item.getItemId()==R.id.item_add_payment){

                                if(addPaymentListener!=null){

                                    viewHolder.swipeLayout.close(true);

                                    addPaymentListener.onClick(position);
                                }

                            }else if(item.getItemId()==R.id.item_view_payment){

                                if(viewPaymentListener!=null){

                                    viewHolder.swipeLayout.close(true);

                                    viewPaymentListener.onClick(position);
                                }

                            }else if(item.getItemId()==R.id.item_view_detail){
                                if(viewDetailListener!=null){

                                    viewHolder.swipeLayout.close(true);

                                    viewDetailListener.onClick(position);
                                }
                            }

                            return false;
                        }
                    });
                    popupMenu.show();
                }
            });//closing the popupmenu click listener


            viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {

                @Override

                public void onClick(View v) {

                    if(mItemClickListerner!=null){

                        viewHolder.swipeLayout.close(true);

                        mItemClickListerner.onClick(position);

                    }

                }

            });*/

            mItemManger.bindView(viewHolder.view, position);

        } else {
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) holder;

            this.loaderViewHolder = loaderViewHolder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public int getItemCount() {
        if (purchasePayableList == null || purchasePayableList.size() == 0) {
            return 0;
        } else {
            return purchasePayableList.size() + 1;
        }

    }


    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            if (purchasePayableList != null && purchasePayableList.size() != 0) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;

            }

        }

        return VIEWTYPE_ITEM;
    }

    public void setShowLoader(boolean showLoader) {
        this.showLoader = showLoader;

        if (loaderViewHolder != null) {
            Log.w("hele", "loader full not null");
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public ClickListener getmItemClickListerner() {
        return mItemClickListerner;
    }

    public void setmItemClickListerner(ClickListener mItemClickListerner) {

        this.mItemClickListerner = mItemClickListerner;

    }

    public List<PurchaseHistory> getPurchasePayableList() {

        return purchasePayableList;

    }

    public void setPurchasePayableList(List<PurchaseHistory> purchasePayableList) {

        this.purchasePayableList = purchasePayableList;

    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {

        this.editClickListener = editClickListener;

    }

    public ClickListener getViewDetailListener() {
        return viewDetailListener;
    }

    public void setViewDetailListener(ClickListener viewDetailListener) {
        this.viewDetailListener = viewDetailListener;
    }

    public ClickListener getViewPaymentListener() {
        return viewPaymentListener;
    }

    public void setViewPaymentListener(ClickListener viewPaymentListener) {
        this.viewPaymentListener = viewPaymentListener;
    }

    public ClickListener getAddPaymentListener() {
        return addPaymentListener;
    }

    public void setAddPaymentListener(ClickListener addPaymentListener) {
        this.addPaymentListener = addPaymentListener;
    }

    public ClickListener getMoreClickListener() {
        return moreClickListener;
    }

    public void setMoreClickListener(ClickListener moreClickListener) {
        this.moreClickListener = moreClickListener;
    }

    public class PayableViewHolder extends RecyclerView.ViewHolder {

        TextView saleIdTextView;

        TextView customerTextView;

        TextView dateTextView;

        TextView totalAmountTextView;

        LinearLayout linearLayout;

        ImageButton addPaymentButton;

        ImageButton viewDetailBtn;

        View view;

        SwipeLayout swipeLayout;


        TypedArray today;
        TypedArray yesterday;


        public PayableViewHolder(View itemView) {

            super(itemView);

            this.view = itemView;

            viewDetailBtn = (ImageButton) itemView.findViewById(R.id.view_detail);

            today = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.today});

            yesterday = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday});

            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll);

            addPaymentButton = (ImageButton) itemView.findViewById(R.id.add_payment_purchase_payable);

            saleIdTextView = (TextView) itemView.findViewById(R.id.purchase_id_in_purchase_payable);

            customerTextView = (TextView) itemView.findViewById(R.id.supplier_in_purchase_payable);

            dateTextView = (TextView) itemView.findViewById(R.id.date_in_purchase_payable);

            totalAmountTextView = (TextView) itemView.findViewById(R.id.total_amount_in_purchase_payable);

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);

        }

    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar mProgressBar;

        public View mView;

        public LoaderViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);

        }
    }
}
