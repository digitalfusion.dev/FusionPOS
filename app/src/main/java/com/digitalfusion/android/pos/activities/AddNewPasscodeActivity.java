package com.digitalfusion.android.pos.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.ApiManager;
import com.digitalfusion.android.pos.database.model.AccessLog;
import com.digitalfusion.android.pos.fragments.salefragments.SaleListHistoryTabFragment;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.example.passcode.Passcode;
import com.example.passcode.interfaces.PasscodeListener;

public class AddNewPasscodeActivity extends AppCompatActivity {
    private Passcode passCodeView;
    private TextView textView;
    private boolean isOld;
    private boolean isNew;
    private String oldPass = "1234";
    private String newPass = "";
    private ApiManager apiManager;
    private String passcodeErrorText = "";
    private String passwordAndConfirmErrorText = "";
    private TextView passcodeErrorTextView;
    private TextView forgotPasscodeTextView;
    private AccessLogManager accessLogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_passcode);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        passCodeView = findViewById(R.id.pass_code_view);
        passCodeView.setLable("Enter Your New Passcode");
        passcodeErrorText = getTheme().obtainStyledAttributes(new int[]{R.attr.wrong_passcode}).getString(0);
        passcodeErrorTextView = passCodeView.findViewById(R.id.passcode_error_text_view);
        forgotPasscodeTextView = passCodeView.findViewById(R.id.forget_password_text_view);
        forgotPasscodeTextView.setVisibility(View.GONE);
        passwordAndConfirmErrorText = getTheme().obtainStyledAttributes(new int[]{R.attr.password_and_confirm_password_are_not_match}).getString(0);
        apiManager = new ApiManager(this);
        accessLogManager = new AccessLogManager(this);

        isOld = true;

        passCodeView.setPasscodeListener(new PasscodeListener() {
            @Override
            public void onEndOfPassCode(String passcode) {
                if (isOld) {
                    isOld = false;
                    newPass = passcode;
                    passCodeView.setLable("Confirm Your New Passcode");
                    passCodeView.clearInput();
                } else {
                    if (newPass.equals(passcode)) {
                        apiManager.changePasscode(apiManager.getOwnerId(), passcode);

                        AccessLog accessLog = new AccessLog();
                        accessLog.setUserId(1L);
                        accessLog.setDeviceId(1L);
                        accessLog.setDatetime(DateUtility.getCurrentDateTime());
                        accessLog.setEvent(AppConstant.EVENT_IN);
                        accessLogManager.insertAccessLog(accessLog);

                        navigateToSaleListHistoryTabFragment();
                        passCodeView.loginPasscodeSuccess();
                        finish();
                    } else {
                        passcodeErrorTextView.setText(passwordAndConfirmErrorText);
                        passCodeView.setLable("Confirm Your New Passcode");
                        passCodeView.clearInput();
                    }
                }
            }

            @Override
            public void onStartOfPassCode() {
                if (!passcodeErrorTextView.getText().toString().equalsIgnoreCase("")) {
                    new Handler().postDelayed(() -> passcodeErrorTextView.setText(""), 300);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
       /* if (doubleBackToExitPressedOnce) {
            Log.e("exit", " after ");
            Log.e("finish", "hey");

            //MainActivity.close=true;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                finishAffinity();
            }
            System.exit(0);

            //}
        }
        Log.e("jkj", "kjoijikjioj");
        this.doubleBackToExitPressedOnce = true;
        POSUtil.showSnackBar(rootView, "Please click BACK again to exit");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e("make", "false");

                doubleBackToExitPressedOnce = false;
            }
        }, 2000);*/

    }

    private void navigateToSaleListHistoryTabFragment() {
        MainActivity.setCurrentFragment(new SaleListHistoryTabFragment());

        Intent intent = new Intent(AddNewPasscodeActivity.this, MainActivity.class);
        intent.putExtra("navi", true);
        startActivity(intent);
    }
}