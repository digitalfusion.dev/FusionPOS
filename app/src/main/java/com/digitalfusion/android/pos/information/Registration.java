package com.digitalfusion.android.pos.information;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by MD003 on 8/14/17.
 */

public class Registration implements Serializable {

    private Long id;
    private Date createdDate;
    private boolean isDeleted = false;
    private String userId;
    private String userStatus;
    private String userName;
    private String businessName;
    private String phone;
    private String email;
    private String address;
    private String longitude;
    private String latitude;
    private String township;
    private String state;
    private String businessType;

    //private AppInfo appInfo;
    // private DeviceInfo deviceInfo;
    //private Customer customer;
    //private List<Subscription> subscriptionList=new ArrayList<>();


    public Registration() {
        //this.createdDate = new Date(2017, 6, 12);
    }

    public Registration(Date createdDate, boolean isDeleted, String userId, String userStatus, String userName, String businessName, String phone, String email, String address, String longitude, String latitude, String region) {
        this.createdDate = createdDate;
        this.isDeleted = isDeleted;
        this.userId = userId;
        this.userStatus = userStatus;
        this.userName = userName;
        this.businessName = businessName;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
        this.township = region;
    }


    public Registration(Long id, Date createdDate, boolean isDeleted, String userId, String userStatus, String userName, String businessName, String phone, String email, String address, String longitude, String latitude, String township, String state, String businessType) {
        this.id = id;
        this.createdDate = createdDate;
        this.isDeleted = isDeleted;
        this.userId = userId;
        this.userStatus = userStatus;
        this.userName = userName;
        this.businessName = businessName;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
        this.township = township;
        this.state = state;
        this.businessType = businessType;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getTownship() {
        return township;
    }

    public void setTownship(String township) {
        this.township = township;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    /*public AppInfo getAppInfo() {
        return appInfo;
    }

    public void setAppInfo(AppInfo appInfo) {
        this.appInfo = appInfo;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "id=" + id +
                ", createdDate=" + createdDate +
                ", isDeleted=" + isDeleted +
                ", userId=" + userId +
                ", userStatus='" + userStatus + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                /*", appInfo=" + appInfo +
                ", deviceInfo=" + deviceInfo +
                ", customer=" + customer +
                ", subscriptionList=" + subscriptionList +*/
                ", userName = " + userName + "\'" +
                ", businessName = " + businessName + "\'"
                + ", phone = " + phone + "\'" +
                ", email =  " + email + "\'" +
                ", address = " + address +
                '}';
    }
}
