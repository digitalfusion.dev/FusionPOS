package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportTitleItem;

import java.util.List;

/**
 * Created by MD001 on 3/6/17.
 */

public class RVAdapterForReportTitleList extends RecyclerView.Adapter<RVAdapterForReportTitleList.ReportsListViewHolder> {

    private List<ReportTitleItem> reportTitleList;
    private OnItemClickListener mItemClickListener;

    public RVAdapterForReportTitleList(List<ReportTitleItem> reportTitleList) {
        this.reportTitleList = reportTitleList;
    }

    @Override
    public ReportsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_title_list_item_view, parent, false);
        return new ReportsListViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ReportsListViewHolder holder, final int position) {

        holder.reportImage.setBackground(reportTitleList.get(position).getImage());

        holder.reportTitle.setText(reportTitleList.get(position).getTitleTxt());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(v, position);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return reportTitleList.size();
    }

    public List<ReportTitleItem> getReportTitleList() {
        return reportTitleList;
    }

    public void setReportTitleList(List<ReportTitleItem> reportTitleList) {
        this.reportTitleList = reportTitleList;
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ReportsListViewHolder extends RecyclerView.ViewHolder {

        View view;

        ImageView reportImage;

        TextView reportTitle;

        public ReportsListViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            reportImage = (ImageView) itemView.findViewById(R.id.report_image);

            reportTitle = (TextView) itemView.findViewById(R.id.report_title);
        }
    }
}
