package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.ExpenseManager;
import com.digitalfusion.android.pos.database.model.ExpenseIncome;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 12/3/16.
 */

public class IncomeSearchAdapter extends ArrayAdapter<ExpenseIncome> {


    List<ExpenseIncome> searchList;

    List<ExpenseIncome> suggestion;

    ExpenseManager expenseBussiness;

    int lenght = 0;

    String queryText = "";

    ExpenseIncome expenseNameVO;

    Context context;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {

            String str = ((ExpenseIncome) resultValue).getName();

            expenseNameVO = (ExpenseIncome) resultValue;

            return str;

        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            if (constraint != null && !constraint.equals("")) {

                searchList.clear();

                queryText = constraint.toString();

                lenght = constraint.length();

                searchList = expenseBussiness.getAllIncomeNameSearch(constraint.toString(), 0, 10);

                Log.w("size", searchList.size() + " SS");

                FilterResults filterResults = new FilterResults();

                filterResults.values = searchList;

                filterResults.count = searchList.size();

                return filterResults;
            } else {

                return new FilterResults();

            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            List<ExpenseIncome> filterList = (ArrayList<ExpenseIncome>) results.values;

            suggestion = searchList;

            if (results != null && results.count > 0) {

                clear();

                for (ExpenseIncome expenseIncome : filterList) {

                    add(expenseIncome);

                    notifyDataSetChanged();

                }

            }

        }

    };

    public IncomeSearchAdapter(Context context, ExpenseManager expenseManager) {

        super(context, -1);

        this.context = context;

        this.expenseBussiness = expenseManager;

        suggestion = new ArrayList<>();
        searchList = new ArrayList<>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ExpenseOrIncomeSearchAdapter.ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.sale_history_search_suggest_view, parent, false);

        }

        viewHolder = new ExpenseOrIncomeSearchAdapter.ViewHolder(convertView);

        if (suggestion.size() > 0 && !suggestion.isEmpty()) {
            if (suggestion.get(position).getName().toLowerCase().startsWith(queryText.toLowerCase())) {

                Spannable spanText = new SpannableString(suggestion.get(position).getName());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.voucherNoTextView.setText(spanText);

            }
            // viewHolder.customerNameTextView.setText(salesHistoryViewList.get(position).getItemName());


            viewHolder.customerNameTextView.setText(suggestion.get(position).getAmount().toString());

        }


        return convertView;

    }

    @Override
    public int getCount() {
        return suggestion.size();
    }

    public List<ExpenseIncome> getSuggestion() {

        return suggestion;

    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    public ExpenseIncome getSelectedItem() {
        return expenseNameVO;
    }

    static class ViewHolder {

        TextView voucherNoTextView;

        TextView customerNameTextView;

        public ViewHolder(View itemView) {

            this.customerNameTextView = (TextView) itemView.findViewById(R.id.voucher_no);

            this.voucherNoTextView = (TextView) itemView.findViewById(R.id.customer_name);

        }

    }

}