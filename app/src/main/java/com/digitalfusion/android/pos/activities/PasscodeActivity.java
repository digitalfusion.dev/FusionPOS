package com.digitalfusion.android.pos.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForUserListPasscode;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.database.model.AccessLog;
import com.digitalfusion.android.pos.fragments.salefragments.SaleListHistoryTabFragment;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.example.passcode.Passcode;
import com.example.passcode.interfaces.ChangeUserClickListener;
import com.example.passcode.interfaces.PasscodeListener;

import java.util.List;

public class PasscodeActivity extends ParentActivity {

    boolean doubleBackToExitPressedOnce = false;
    //    private View rootView;
    private Passcode passcodeView;
    private TextView passcodeErrorTextView;
    private TextView forgotPasscodeTextView;
    private String passcodeErrorText = "";
    private boolean passcodeErrorFlag = false;
//    private int currentSelectedUserPosition;
    private List<User> userList;
    private User currentUser;
    private AccessLogManager accessLogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pass_code_new_activity);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final ApiDAO apiDAO = ApiDAO.getApiDAOInstance(this);
        accessLogManager = new AccessLogManager(this);

        userList = apiDAO.getAllUserRoles();
        currentUser = userList.get(0);

        passcodeView = (Passcode) findViewById(R.id.passcode);
        passcodeErrorText = ThemeUtil.getString(this, R.attr.wrong_passcode);
        passcodeErrorTextView = passcodeView.findViewById(R.id.passcode_error_text_view);
        forgotPasscodeTextView = passcodeView.findViewById(R.id.forget_password_text_view);

        ((TextView) passcodeView.findViewById(R.id.nameView)).setText(userList.get(0).getUserName());
        ((TextView) passcodeView.findViewById(R.id.userRoleView)).setText(userList.get(0).getRole());

        forgotPasscodeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PasscodeActivity.this, VerifyActivity.class);
                intent.putExtra(VerifyActivity.FORGETPASS, true);
                startActivity(intent);
            }
        });

        passcodeView.setChangeUserClickListener(new ChangeUserClickListener() {
            @Override
            public void onClick() {
                showUserList();
            }
        });

        passcodeView.setPasscodeListener(new PasscodeListener() {
            @Override
            public void onEndOfPassCode(String passcode) {
                if (currentUser.getPasscode().equalsIgnoreCase(passcode)) {
                    if (accessLogManager.isUserLoggedIn(currentUser.getId())) {
                        buildAlreadyLoggedInDialog().show();
                    } else {
                        AccessLog accessLog = new AccessLog();
                        accessLog.setUserId(currentUser.getId());
                        accessLog.setDeviceId(1L);
                        accessLog.setDatetime(DateUtility.getCurrentDateTime());
                        accessLog.setEvent(AppConstant.EVENT_IN);

                        accessLogManager.insertAccessLog(accessLog);
                        finish();
                        navigateToSaleListHistoryTabFragment();
                    }
                } else {
                    passcodeErrorTextView.setText(passcodeErrorText);
                    passcodeView.clearInput();
                    passcodeErrorFlag = true;
                }

            }

            @Override
            public void onStartOfPassCode() {
                Log.e("passcode_start", "");
                if (passcodeErrorFlag) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            passcodeErrorTextView.setText("");
                        }
                    }, 100);
                }
            }
        });

    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity();
            System.exit(1);
        }
        this.doubleBackToExitPressedOnce = true;
        Snackbar snackbar = Snackbar.make(findViewById(R.id.root), "Press BACK again to exit", Snackbar.LENGTH_SHORT);
        snackbar.show();
        new Handler().postDelayed(() -> {
            Log.e("make", "false");
            doubleBackToExitPressedOnce = false;
        }, 2000);
    }

    private void showUserList() {
        final RVAdapterForUserListPasscode RVAdapterForUserListPasscode = new RVAdapterForUserListPasscode(userList);

        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .canceledOnTouchOutside(true)
                .adapter(RVAdapterForUserListPasscode, new LinearLayoutManager(this))
                .build();

        //TODO: Uncomment these lines for displaying picture of users
        //        RVAdapterForUserListPasscode.setSelectedPosition(currentSelectedUserPosition);
        RVAdapterForUserListPasscode.setLoginItemClickHandler(new RVAdapterForUserListPasscode.LoginVRadapterEventHandler() {
            @Override
            public void onItemClick(int position) {
                //                currentSelectedUserPosition = position;
                currentUser = userList.get(position);
                // Show forgot passcode view to only the owner whose id is 1
                if (currentUser.getId() == 1) {
                    forgotPasscodeTextView.setVisibility(View.VISIBLE);
                } else {
                    forgotPasscodeTextView.setVisibility(View.INVISIBLE);
                }
                //                if (userList.get(position).getPicture() != null) {
                //                    Bitmap image = POSUtil.getBitmapFromByteArray(userList.get(position).getPicture());
                //                    ((ImageButton) passcodeView.findViewById(R.id.changeUserButton)).setImageBitmap(image);
                //                }
                ((TextView) passcodeView.findViewById(R.id.nameView)).setText(userList.get(position).getUserName());
                ((TextView) passcodeView.findViewById(R.id.userRoleView)).setText(userList.get(position).getRole());
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private MaterialDialog buildAlreadyLoggedInDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(PasscodeActivity.this)
                .content("User is currently logged in.\nDo you want to log out from other device?")
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .positiveText("Yes")
                .negativeText("No")
                .canceledOnTouchOutside(true)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        AccessLog accessLog = new AccessLog();
                        accessLog.setUserId(currentUser.getId());
                        accessLog.setDeviceId(1L);
                        accessLog.setDatetime(DateUtility.getCurrentDateTime());
                        accessLog.setEvent(AppConstant.EVENT_OUT);

                        // logout first
                        accessLogManager.insertAccessLog(accessLog);
                        // and login
                        accessLog.setEvent(AppConstant.EVENT_IN);
                        accessLogManager.insertAccessLog(accessLog);
                        finish();

                        navigateToSaleListHistoryTabFragment();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        passcodeView.clearInput();
                        dialog.dismiss();
                    }
                })
                .build();

        return dialog;
    }

    private void navigateToSaleListHistoryTabFragment() {
        MainActivity.setCurrentFragment(new SaleListHistoryTabFragment());

        Intent intent = new Intent(PasscodeActivity.this, MainActivity.class);
        intent.putExtra("navi", true);
        startActivity(intent);

        passcodeView.loginPasscodeSuccess();
    }
}
