package com.digitalfusion.android.pos.fragments.settingfragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.NoInternetException;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.database.business.ApiManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.dao.DataManagementDao;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.information.Registration;
import com.digitalfusion.android.pos.network.ApiRetrofit;
import com.digitalfusion.android.pos.network.NetworkClient;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.ConnectivityUtil;
import com.digitalfusion.android.pos.util.ExportImportUtil;
import com.digitalfusion.android.pos.util.GrantPermission;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.views.FileChooserDialog;
import com.digitalfusion.android.pos.views.FolderChooserDialog;
import com.digitalfusion.android.pos.views.FusionToast;
import com.digitalfusion.android.pos.views.Spinner;
import com.example.passcode.PasscodeWithoutKeyboard;

import org.apache.poi.util.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MD003 on 8/24/16.
 */
public class DataManagementFragment extends Fragment implements FileChooserDialog.FileCallback, FolderChooserDialog.FolderSelectionCallback {
    private enum Message {
        RESTORE_SUCCESS, RESTORE_FAILED, BACKUP_SUCCESS, BACKUP_FAILED, CLEAR_SUCCESS, CLEAR_FAILED
    }

    private View mainLayoutView;
    private TextView backupDirTextView;
    private TextView restoreDirTextView;
    private Button backupButton;
    private Button restoreButton;
    private Button clearDataButton;
    private Button resetTransactionDataButton;
    private DataManagementDao dataManagementDao;
    private TextView exportDirTextView;
    private Button exportButton;
    private TextView importDirTextView;
    private Button importButton;
    //For Lost list
    private GrantPermission grantPermission;
    private boolean flag;
    private Context context;
    private String backupDir;
    private String restoreDir;
    //private boolean isBackup;
    private String importDir;
    //private boolean isImport;
    private boolean isFileChooser;
    private boolean isFolderChooser;
    private String type;
    private FolderChooserDialog folderChooserDialog;
    private MaterialDialog backupConfirmDialog;
    private MaterialDialog exportConfirmDialog;
    private MaterialDialog importConfirmDialog;
    private MaterialDialog changeDatabaseConfirmDialog;
    private Button yesChangeDbConfirmMDButton;
    private Button cancelChangeDbConfirmMDbuttton;
    private Button yesImportConfirmMdButton;
    private Button yesExportConfirmMdButton;
    private Button yesBackupConfirmMdButton;
    private MaterialDialog restoreConfirmDialog;
    private Button yesRestoreConfirmMDButton;
    private MaterialDialog clearConfirmDialog;
    private Button yesClearConfirmMDButton;
    private MaterialDialog resetConfirmDialog;
    private Button yesResetConfirmMDButton;
    private StockManager stockManager;
    private Message message;
    private boolean isExport = false;
    private List<StockItem> stockItemList;
    private boolean isStoragePermissionForTV = false;
    private MaterialDialog filterDialog;
    private RVAdapterForFilter rvAdapterForFilter;
    private List<String> filterList;
    private String sampleDatabase;
    private String ownDatabase;
    private TextView usingDatabaseTextView;
    private Button changeButton;
    private FileChooserDialog.Builder fileChooserDialog;
    private Button cloudBackupButton;
    private Button cloudRestoreButton;
    private Boolean restoreFromCloudSuccess = false;
    private MaterialDialog passcodeMaterialDialog;
    private PasscodeWithoutKeyboard passcodeView;
    private TextView passcodeErrorTextView;

    private String headerTxt;
    private String header;
    private String passcodeErrorText;
    private boolean passcodeErrorFlag = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainLayoutView = inflater.inflate(R.layout.data_management_setting, null);

        context = getContext();

        stockManager = new StockManager(getContext());

        String dataManagement = context.getTheme().obtainStyledAttributes(new int[]{R.attr.data_management}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(dataManagement);

        sampleDatabase = context.getTheme().obtainStyledAttributes(new int[]{R.attr.sample_database}).getString(0);
        ownDatabase = context.getTheme().obtainStyledAttributes(new int[]{R.attr.own_database}).getString(0);

        filterList = new ArrayList<>();
        filterList.add(sampleDatabase);
        filterList.add(ownDatabase);

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        loadUI();
        initializeVariables();
        buildConfirmDialogs();
        clickListeners();
        MainActivity.setCurrentFragment(this);

        buildFilterDialog();

        importButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "import";
                if (grantPermission.requestPermissions()) {
                    //isImport=true;
                    importConfirmDialog.show();
                }
            }
        });

        yesImportConfirmMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (grantPermission.requestPermissions()) {

                    new LoadProgressDialog().execute("import");
                }
                importConfirmDialog.dismiss();
            }
        });

        importDirTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "import";
                //isImport=true;
                isStoragePermissionForTV = true;
                isFileChooser = true;
                isFolderChooser = false;
                buildFileChooserDialog();
                fileChooserDialog.mimeTypes(Arrays.asList("application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
                if (grantPermission.requestPermissions()) {
                    fileChooserDialog.show();
                }
            }
        });

        return mainLayoutView;
    }

    private void saveOnSharedPreference() {
        Log.e("header on", headerTxt);
        SharedPreferences sharedPref = getActivity().getSharedPreferences("database", Context.MODE_PRIVATE);
        //String temp_header = sharedPref.getString("database", "db");
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("database", headerTxt);
        editor.apply();
        editor.commit();
        AppConstant.SHARED_PREFERENCE_DB = headerTxt;
    }

    private void buildFilterDialog() {

        SharedPreferences sharedPref  = getActivity().getSharedPreferences("database", Context.MODE_PRIVATE);
        String            temp_header = sharedPref.getString("database", "db");
        Log.e("temp header", temp_header);
        if (temp_header.equalsIgnoreCase(AppConstant.SAMPLE_DATABASE_NAME)) {
            usingDatabaseTextView.setText(sampleDatabase);
            header = sampleDatabase;
        } else if (temp_header.equalsIgnoreCase(AppConstant.DATABASE_NAME)) {
            usingDatabaseTextView.setText(ownDatabase);
            header = ownDatabase;
        }

        TypedArray filterByDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.databases});

        filterDialog = new MaterialDialog.Builder(context)
                .title(filterByDate.getString(0))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))
                .build();

        rvAdapterForFilter.setCurrentPos(filterList.indexOf(header));
    }

    private void initializeVariables() {
        restoreDir = "/storage/emulated/0/inventory_backup.zip";
        importDir = "/storage/emulated/0/stock.xls";
        dataManagementDao = DataManagementDao.getDataManagementDaoInstance(context);
        grantPermission = new GrantPermission(this);
        backupDir = "/storage/emulated/0";
        backupDirTextView.setText(backupDir);
        restoreDirTextView.setText(restoreDir);
        exportDirTextView.setText(backupDir);
    }

    private void clickListeners() {
        backupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "backup";
                isStoragePermissionForTV = false;
                isExport = false;
                backupConfirmDialog.show();
            }
        });

        exportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "export";
                isExport = true;
                isStoragePermissionForTV = false;
                if (grantPermission.requestPermissions()) {
                    exportConfirmDialog.show();
                }
            }
        });

        restoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "restore";
                isStoragePermissionForTV = false;
                isExport = false;
                restoreConfirmDialog.show();
            }
        });

        restoreDirTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "restore";
                isStoragePermissionForTV = true;
                isFileChooser = true;
                isFolderChooser = false;
                isExport = false;
                buildFileChooserDialog();
                fileChooserDialog.mimeType("application/zip");
                if (grantPermission.requestPermissions()) {
                    fileChooserDialog.show();
                }
            }
        });

        backupDirTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "backup";
                isFolderChooser = true;
                isFileChooser = false;
                buildFolderChooserDialog();
                isStoragePermissionForTV = true;
                if (grantPermission.requestPermissions()) {
                    folderChooserDialog.show(getActivity());
                }
            }
        });

        exportDirTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "exportStocks";
                isFileChooser = false;
                isFolderChooser = true;
                isStoragePermissionForTV = true;
                buildFolderChooserDialog();
                if (grantPermission.requestPermissions()) {
                    folderChooserDialog.show(getActivity());
                }
            }
        });

        clearDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //isStoragePermissionForTV = false;
                clearConfirmDialog.show();
            }
        });

        cloudBackupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "cloud_backup";
                if (grantPermission.requestPermissions()) {
                    new LoadProgressDialog().execute("cloud_backup");
                    //                    proceedWithApi();
                }
            }
        });

        cloudRestoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "cloud_restore";
                if (grantPermission.requestPermissions()) {
                    new LoadProgressDialog().execute("cloud_restore");
                    //                    proceedWithApi();
                }
            }
        });

        resetTransactionDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetConfirmDialog.show();
            }
        });

        yesBackupConfirmMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backupConfirmDialog.dismiss();
                type = "backup";
                if (grantPermission.requestPermissions()) {
                    new LoadProgressDialog().execute("backup");
                }
            }
        });

        yesExportConfirmMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "exportStocks";
                exportConfirmDialog.dismiss();
                if (grantPermission.requestPermissions()) {
                    new LoadProgressDialog().execute("export");
                }
            }
        });

        yesChangeDbConfirmMDButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDatabaseConfirmDialog.dismiss();
                saveOnSharedPreference();
                dataManagementDao.deactivateHelper();
                showSnackBar();
            }
        });

        yesRestoreConfirmMDButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "restore";
                restoreConfirmDialog.dismiss();
                if (grantPermission.requestPermissions()) {
                    String restore = context.getTheme().obtainStyledAttributes(new int[]{R.attr.restore}).getString(0);
                    new LoadProgressDialog().execute("restore");
                }
            }
        });

        yesClearConfirmMDButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "clear";
                clearConfirmDialog.dismiss();
                String clear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.clear}).getString(0);

                passcodeView.clearText();
                passcodeView.goodDigit();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(passcodeView.getEditText(), 0);
                    }
                }, 300);
                passcodeMaterialDialog.show();
            }
        });

        yesResetConfirmMDButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "reset";
                resetConfirmDialog.dismiss();
                String reset = context.getTheme().obtainStyledAttributes(new int[]{R.attr.reset}).getString(0);
                passcodeView.clearText();
                passcodeView.goodDigit();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(passcodeView.getEditText(), 0);
                    }
                }, 300);
                passcodeMaterialDialog.show();
            }
        });

        usingDatabaseTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStoragePermissionForTV = false;
                filterDialog.show();
            }
        });

        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStoragePermissionForTV = false;
                changeDatabaseConfirmDialog.show();
            }
        });

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                usingDatabaseTextView.setText(filterList.get(position));
                if (filterList.get(position) == sampleDatabase) {
                    headerTxt = AppConstant.SAMPLE_DATABASE_NAME;
                } else if (filterList.get(position) == ownDatabase) {
                    headerTxt = AppConstant.DATABASE_NAME;
                }
                filterDialog.dismiss();
            }
        });
    }

    public void buildFolderChooserDialog() {
        if (folderChooserDialog == null) {
            folderChooserDialog = new FolderChooserDialog();
            folderChooserDialog.setmCallback(this);
        }
    }

    public void buildFileChooserDialog() {
        fileChooserDialog = new FileChooserDialog.Builder(DataManagementFragment.this);
    }

    private void restore(String dir) {

        if (dataManagementDao.restore(dir)) {
            message = Message.RESTORE_SUCCESS;
        } else {
            message = Message.RESTORE_FAILED;
        }
    }

    private void backup(String dir) {
        if (dataManagementDao.backup(dir) != null) {
            message = Message.BACKUP_SUCCESS;
        } else {
            message = Message.BACKUP_FAILED;
        }
    }

    private String getDatabasePath() {

        if (android.os.Build.VERSION.SDK_INT >= 17) {

            return context.getApplicationInfo().dataDir + "/";
        } else {
            return "/data/data/" + context.getPackageName() + "/";
        }
    }

    private void clearTransactionData() {
        message = dataManagementDao.clearTransactionData() ? Message.CLEAR_SUCCESS : Message.CLEAR_FAILED;
    }

    private void clearAllData() {
        message = dataManagementDao.clearAllData() ? Message.CLEAR_SUCCESS : Message.CLEAR_FAILED;
    }

    public void loadUI() {
        backupDirTextView = (TextView) mainLayoutView.findViewById(R.id.backup_dir_text_view);
        restoreDirTextView = (TextView) mainLayoutView.findViewById(R.id.restore_dir_text_view);
        backupButton = (Button) mainLayoutView.findViewById(R.id.backup_button);
        restoreButton = (Button) mainLayoutView.findViewById(R.id.restore_button);
        clearDataButton = (Button) mainLayoutView.findViewById(R.id.clear_data_button);
        resetTransactionDataButton = (Button) mainLayoutView.findViewById(R.id.reset_button);
        exportDirTextView = (TextView) mainLayoutView.findViewById(R.id.xls_dir_tv);
        exportButton = (Button) mainLayoutView.findViewById(R.id.export);
        importDirTextView = (TextView) mainLayoutView.findViewById(R.id.impt_dir_tv);
        importButton = (Button) mainLayoutView.findViewById(R.id.impt);
        usingDatabaseTextView = (TextView) mainLayoutView.findViewById(R.id.using_database_text_view);
        changeButton = (Button) mainLayoutView.findViewById(R.id.change_button);
        backupDirTextView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        restoreDirTextView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        exportDirTextView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        importDirTextView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        usingDatabaseTextView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        cloudBackupButton = mainLayoutView.findViewById(R.id.cloud_backup_button);
        cloudRestoreButton = mainLayoutView.findViewById(R.id.cloud_restore_button);
    }

    public void replacingFragment(Fragment fragment) {
        final FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.replace(R.id.frame_replace, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //recreate();
            //reload my activity with requestPermissions granted or use the features what required the requestPermissions
            //requestPermissions(backingUp);

            //Log.w("exportStocks", isExport + " SS");
            if (!isStoragePermissionForTV) {
                if (type.equalsIgnoreCase("export")) {
                    new LoadProgressDialog().execute("export");
                } else if (type.equalsIgnoreCase("backup")) {
                    new LoadProgressDialog().execute("backup");
                } else if (type.equalsIgnoreCase("restore")) {
                    new LoadProgressDialog().execute("restore");
                }
            } else {
                if (isFolderChooser) {
                    folderChooserDialog.show(getActivity());
                }
                if (isFileChooser) {
                    fileChooserDialog.show();
                }
            }

            //Log.e("backup", flag + " ai");
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            grantPermission.askAgainPermission();
        } else {
            grantPermission.forcePermissionSetting();
        }
    }

    @Override
    public void onFileSelection(@NonNull FileChooserDialog dialog, @NonNull File file) {
        if (type.equalsIgnoreCase("import")) {
            importDirTextView.setText(file.getPath());
            importDir = file.getPath();
        } else if (type.equalsIgnoreCase("restore")) {
            restoreDirTextView.setText(file.getPath());
            restoreDir = file.getPath();
        }
    }

    @Override
    public void onFolderSelection(File folder) {
        backupDir = folder.getAbsolutePath();

        if (type.equalsIgnoreCase("backup")) {
            backupDirTextView.setText(backupDir + "/inventory_backup.zip");
        } else if (type.equalsIgnoreCase("exportStocks")) {
            exportDirTextView.setText(folder.getAbsolutePath() + "/stock.xls");
        }
    }

    private void buildConfirmDialogs() {
        backupConfirmDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .build();

        String sureBackup = ThemeUtil.getString(context, R.attr.are_you_sure_want_to_backup_data);
        ((TextView) backupConfirmDialog.findViewById(R.id.title)).setText(sureBackup);

        backupConfirmDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backupConfirmDialog.dismiss();
            }
        });

        yesBackupConfirmMdButton = (Button) backupConfirmDialog.findViewById(R.id.save);

        exportConfirmDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();

        String exportData = ThemeUtil.getString(context, R.attr.are_you_sure_want_to_export_data);
        ((TextView) exportConfirmDialog.findViewById(R.id.title)).setText(exportData);

        yesExportConfirmMdButton = (Button) exportConfirmDialog.findViewById(R.id.save);

        exportConfirmDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportConfirmDialog.dismiss();
            }
        });

        importConfirmDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();

        String importData = ThemeUtil.getString(context, R.attr.are_you_sure_want_to_import_data);
        ((TextView) importConfirmDialog.findViewById(R.id.title)).setText(importData);

        yesImportConfirmMdButton = (Button) importConfirmDialog.findViewById(R.id.save);

        importConfirmDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                importConfirmDialog.dismiss();
            }
        });

        restoreConfirmDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .build();

        String restoreData = ThemeUtil.getString(context, R.attr.are_you_sure_want_to_restore_data);
        ((TextView) restoreConfirmDialog.findViewById(R.id.title)).setText(restoreData);

        yesRestoreConfirmMDButton = (Button) restoreConfirmDialog.findViewById(R.id.save);

        restoreConfirmDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restoreConfirmDialog.dismiss();
            }
        });

        clearConfirmDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .build();

        String clearData = ThemeUtil.getString(context, R.attr.are_you_sure_want_to_clear_all_data);
        ((TextView) clearConfirmDialog.findViewById(R.id.title)).setText(clearData);

        yesClearConfirmMDButton = (Button) clearConfirmDialog.findViewById(R.id.save);

        clearConfirmDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearConfirmDialog.dismiss();
            }
        });

        resetConfirmDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();

        String resetData = ThemeUtil.getString(context, R.attr.are_you_sure_want_to_reset_all_transaction_data);
        ((TextView) resetConfirmDialog.findViewById(R.id.title)).setText(resetData);

        yesResetConfirmMDButton = (Button) resetConfirmDialog.findViewById(R.id.save);

        resetConfirmDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetConfirmDialog.dismiss();
            }
        });

        changeDatabaseConfirmDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();

        String sureChangeDB = ThemeUtil.getString(context, R.attr.are_you_sure_to_change_database);
        ((TextView) changeDatabaseConfirmDialog.findViewById(R.id.title)).setText(sureChangeDB);

        changeDatabaseConfirmDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDatabaseConfirmDialog.dismiss();
            }
        });

        yesChangeDbConfirmMDButton = (Button) changeDatabaseConfirmDialog.findViewById(R.id.save);

        passcodeMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.passcode_custom_material_dialog_layout, false)
                .build();

        passcodeErrorTextView = (TextView) passcodeMaterialDialog.findViewById(R.id.passcode_error_text_view);
        passcodeErrorText = context.getTheme().obtainStyledAttributes(new int[]{R.attr.wrong_passcode}).getString(0);

        passcodeView = (PasscodeWithoutKeyboard) passcodeMaterialDialog.findViewById(R.id.pass_code_view);

        passcodeView.setPasscodeFocusListener(new PasscodeWithoutKeyboard.PasscodeFocusListener() {
            @Override
            public void writePasscode() {
                Log.e("passcode_write", passcodeErrorFlag + " ");
                if (passcodeErrorFlag) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            passcodeErrorTextView.setText("");
                            passcodeView.goodDigit();
                        }
                    }, 300);
                }
            }
        });

        passcodeView.setOnPinEnteredListener(new PasscodeWithoutKeyboard.OnPinEnteredListener() {
            @Override
            public void onPinEntered(String pin) {
                Log.e("pin", "on enter");
                ApiManager apiManager = new ApiManager(context);

                if (apiManager.validatePasscode(1, pin)) {
                    passcodeMaterialDialog.dismiss();
                    new LoadProgressDialog().execute(type);
                } else {
                    passcodeView.errorDigit();
                    passcodeErrorTextView.setText(passcodeErrorText);
                    //                    YoYo.with(Techniques.Tada)
                    //                            .duration(700)
                    //                            .playOn(passcodeView);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            passcodeView.clearText();
                            passcodeErrorFlag = true;
                        }
                    }, 200);
                }
            }
        });
    }

    private void showSnackBar() {
        FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
      /*  Snackbar snackbar = Snackbar.make(mainLayoutView, "", Snackbar.LENGTH_LONG);
        // Get the Snackbar's layout view
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        // Hide the text
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);

        // Inflate our custom view
        View snackView = LayoutInflater.from(getContext()).inflate(R.layout.custom_snack_bar, null);
        //snackView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.black));
        // Configure the view
        TextView myTextView = (TextView) snackView.findViewById(R.id.snackbar_text_view);
        Log.e("mes", msg);
        myTextView.setText(msg);
        myTextView.setTextColor(Color.WHITE);

        // Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
        // Show the Snackbar
        snackbar.show();*/
    }

    private void proceedWithApi() throws NoInternetException, IOException {
        if (!ConnectivityUtil.isConnected(context)) {
            throw new NoInternetException();
        }

        File backupFile = dataManagementDao.backup(backupDir);

        if (!backupFile.exists() || backupFile == null) {
            throw new IOException();
        }
        final File tempFile = new File(backupFile.getParent(), "fusion_inventory_backup.zip");
        backupFile.renameTo(tempFile);

        ApiRetrofit service     = NetworkClient.createService(ApiRetrofit.class, "digital_fusion", "digital_fusion");
        RequestBody requestFile = RequestBody.create(MediaType.parse("application/zip"), tempFile);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body              = MultipartBody.Part.createFormData("file", tempFile.getName(), requestFile);
        ApiManager         apiManager        = new ApiManager(context);
        Registration       registration      = apiManager.getRegistration();
        String             descriptionString = registration.getUserId();
        RequestBody        description       = RequestBody.create(MultipartBody.FORM, descriptionString);

        Call<ResponseBody> call = service.backupDB(description, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {
                tempFile.delete();
                FusionToast.toast(context, FusionToast.TOAST_TYPE.SUCCESS, ThemeUtil.getString(context, R.attr.backup_process_is_successful));
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                ConnectivityUtil.handleFailureProcess(t, mainLayoutView);
                FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR);
            }
        });
    }

    private void proceedWithApiForCloudRestore() throws NoInternetException {
        if (!ConnectivityUtil.isConnected(context)) {
            throw new NoInternetException();
        }

        ApiRetrofit  service           = NetworkClient.createService(ApiRetrofit.class, "digital_fusion", "digital_fusion");
        ApiManager   apiManager        = new ApiManager(context);
        Registration registration      = apiManager.getRegistration();
        String       descriptionString = registration.getUserId();

        Call<ResponseBody> call = service.restoreDB(descriptionString);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   final Response<ResponseBody> response) {
                if (response.code() == 200) {
                    //                    Log.e("content type ", response.headers().get("Content-Type"));
                    File tempFile = new File(context.getExternalFilesDir(null) + File.separator + "fusion_inventory_backup.zip");

                    try {
                        OutputStream outputStream = new FileOutputStream(tempFile);
                        InputStream  inputStream  = response.body().byteStream();
                        IOUtils.copy(inputStream, outputStream);
                        inputStream.close();
                        outputStream.close();

                        restore(tempFile.getPath());
                        tempFile.delete();

                        FusionToast.toast(context, FusionToast.TOAST_TYPE.SUCCESS, ThemeUtil.getString(context, R.attr.restore_process_is_successful));
                    } catch (IOException e) {
                        e.printStackTrace();
                        FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR, ThemeUtil.getString(context, R.attr.restore_process_is_unsuccessful));
                    }
                } else if (response.code() == 204) {
                    FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR, ThemeUtil.getString(context, R.attr.restore_process_is_unsuccessful));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                ConnectivityUtil.handleFailureProcess(t, mainLayoutView);
            }
        });
    }


    /*private String getDatabasePath() {
        Log.e("db","path");
        if(android.os.Build.VERSION.SDK_INT >= 17){
            //Log.e("oaeioa",context.getApplicationInfo().dataDir + "/");
            return context.getApplicationInfo().dataDir + "/";
        } else {
            return  "/data/data/" + context.getPackageName() + "/";
        }
    }*/

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private Exception e = null;
        private Spinner spinner = new Spinner(context);

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            Log.e("api", params[0].toString());
            switch (params[0].toString().toLowerCase()) {
                case "backup":
                    backup(backupDir);
                    break;
                case "restore":
                    restore(restoreDir);
                    break;
                case "clear":
                    clearAllData();
                    break;
                case "reset":
                    clearTransactionData();
                    break;
                case "export":
                    ExportImportUtil.exportStocks(stockManager.getAllStocks(0, stockManager.getAllStockCounts()), backupDir);
                    POSUtil.scanMedia(backupDir, context);
                    break;
                case "import":
                    stockItemList = ExportImportUtil.importStocksFromExcel(importDir);
                    break;
                case "cloud_backup":
                    try {
                        proceedWithApi();
                    } catch (NoInternetException | IOException e) {
                        this.e = e;
                    }
                    break;
                case "cloud_restore":
                    try {
                        proceedWithApiForCloudRestore();
                    } catch (NoInternetException e) {
                        this.e = e;
                    }
                    break;
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            spinner.show();
        }

        @Override
        protected void onPostExecute(String a) {
            spinner.dismiss();

            if (e != null) {
                if (e instanceof NoInternetException) {
                    FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR, ThemeUtil.getString(context, R.attr.no_connection));
                }
                if (e instanceof IOException) {
                    FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR, "File not found");
                }
            } else {
                if (type.equalsIgnoreCase("import")) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(StockListPreviewFragment.KEY, (Serializable) stockItemList);

                    Intent previewIntent = new Intent(context, DetailActivity.class);
                    previewIntent.putExtras(bundle);
                    previewIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.STOCK_LIST_PREVIEW);
                    startActivity(previewIntent);
                } else if (type.equalsIgnoreCase("export")) {
                    FusionToast.toast(context, FusionToast.TOAST_TYPE.SUCCESS, ThemeUtil.getString(context, R.attr.export_excel_is_successful));
                } else if (message != null) {
                    switch (message) {
                        case RESTORE_FAILED:
                            FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR, ThemeUtil.getString(context, R.attr.restore_process_is_unsuccessful));
                            break;
                        case RESTORE_SUCCESS:
                            FusionToast.toast(context, FusionToast.TOAST_TYPE.SUCCESS, ThemeUtil.getString(context, R.attr.restore_process_is_successful));
                            break;
                        case BACKUP_FAILED:
                            FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR);
                            break;
                        case BACKUP_SUCCESS:
                            FusionToast.toast(context, FusionToast.TOAST_TYPE.SUCCESS, ThemeUtil.getString(context, R.attr.backup_process_is_successful));
                            break;
                        case CLEAR_FAILED:
                            FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR);
                            break;
                        case CLEAR_SUCCESS:
                            FusionToast.toast(context, FusionToast.TOAST_TYPE.SUCCESS);
                    }
                }
            }
        }
    }
}
