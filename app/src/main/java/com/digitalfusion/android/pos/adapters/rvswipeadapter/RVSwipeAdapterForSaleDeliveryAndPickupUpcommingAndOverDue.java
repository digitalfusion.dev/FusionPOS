package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.SaleDeliveryAndPickUp;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 9/9/16.
 */

public class RVSwipeAdapterForSaleDeliveryAndPickupUpcommingAndOverDue extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    public int currentDate;
    private List<SaleDeliveryAndPickUp> deliveryAndPickUpViewList;
    private ClickListener mDeliverClickListener;
    private ClickListener mOrderCancelClickListener;
    private ClickListener viewDetailClicklistener;

    public RVSwipeAdapterForSaleDeliveryAndPickupUpcommingAndOverDue(List<SaleDeliveryAndPickUp> deliveryAndPickUpViewList) {
        this.deliveryAndPickUpViewList = deliveryAndPickUpViewList;

        Calendar now = Calendar.getInstance();

        currentDate = Integer.parseInt(DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(now.get(Calendar.DAY_OF_MONTH))));

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.delivery_pickup_item_view_upcom_and_over, parent, false);

        return new DeliveryAndPickupItemView(v);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof DeliveryAndPickupItemView) {

            DeliveryAndPickupItemView deliveryAndPickupItemView = (DeliveryAndPickupItemView) holder;

            POSUtil.makeZebraStrip(deliveryAndPickupItemView.itemView, position);

            int date = Integer.parseInt(deliveryAndPickUpViewList.get(position).getPickupOrDeliveryDate());

            if (date == currentDate) {

                deliveryAndPickupItemView.dueDateTextView.setText(deliveryAndPickupItemView.today.getString(0));

            } else if (date == currentDate - 1) {

                deliveryAndPickupItemView.dueDateTextView.setText(deliveryAndPickupItemView.yesterday.getString(0));

            } else {

                deliveryAndPickupItemView.dueDateTextView.setText(DateUtility.makeDateFormatWithSlash(deliveryAndPickUpViewList.get(position).getPickupOrDeliveryYear(),
                        deliveryAndPickUpViewList.get(position).getPickupOrDeliverymonth(),
                        deliveryAndPickUpViewList.get(position).getPickupOrDeliveryDay()));
            }
            deliveryAndPickupItemView.invoiceNoTextView.setText("#" + deliveryAndPickUpViewList.get(position).getSaleVoucherNo());
            deliveryAndPickupItemView.customerTextView.setText(deliveryAndPickUpViewList.get(position).getCustomerName());


            deliveryAndPickupItemView.deliverBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDeliverClickListener != null) {
                        mDeliverClickListener.onClick(holder.getLayoutPosition());
                    }
                }
            });
            deliveryAndPickupItemView.cancelOrderBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOrderCancelClickListener != null) {

                        mOrderCancelClickListener.onClick(holder.getLayoutPosition());
                    }
                }
            });

            deliveryAndPickupItemView.viewDetailsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewDetailClicklistener != null) {
                        viewDetailClicklistener.onClick(position);
                    }
                }
            });

            mItemManger.bindView(deliveryAndPickupItemView.itemView, position);

        }

    }

    @Override
    public int getItemCount() {
        return deliveryAndPickUpViewList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public ClickListener getViewDetailClicklistener() {
        return viewDetailClicklistener;
    }

    public void setViewDetailClicklistener(ClickListener viewDetailClicklistener) {
        this.viewDetailClicklistener = viewDetailClicklistener;
    }

    public ClickListener getmOrderCancelClickListener() {
        return mOrderCancelClickListener;
    }

    public void setmOrderCancelClickListener(ClickListener mOrderCancelClickListener) {
        this.mOrderCancelClickListener = mOrderCancelClickListener;
    }

    public List<SaleDeliveryAndPickUp> getDeliveryAndPickUpViewList() {
        return deliveryAndPickUpViewList;
    }

    public void setDeliveryAndPickUpViewList(List<SaleDeliveryAndPickUp> deliveryAndPickUpViewList) {
        this.deliveryAndPickUpViewList = deliveryAndPickUpViewList;
    }

    public ClickListener getmDeliverClickListener() {
        return mDeliverClickListener;
    }

    public void setmDeliverClickListener(ClickListener mDeliverClickListener) {
        this.mDeliverClickListener = mDeliverClickListener;
    }

    public class DeliveryAndPickupItemView extends RecyclerView.ViewHolder {
        TextView invoiceNoTextView;
        TextView dueDateTextView;

        TextView customerTextView;
        ImageButton deliverBtn;
        ImageButton cancelOrderBtn;
        ImageButton viewDetailsBtn;

        TypedArray today;
        TypedArray yesterday;
        View view;

        public DeliveryAndPickupItemView(View itemView) {
            super(itemView);

            today = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.today});
            yesterday = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday});
            this.view = itemView;

            viewDetailsBtn = (ImageButton) itemView.findViewById(R.id.view_detail);

            cancelOrderBtn = (ImageButton) itemView.findViewById(R.id.cancel_order);
            deliverBtn = (ImageButton) itemView.findViewById(R.id.deliver);

            invoiceNoTextView = (TextView) itemView.findViewById(R.id.voucher_no);
            dueDateTextView = (TextView) itemView.findViewById(R.id.due_date);
            customerTextView = (TextView) itemView.findViewById(R.id.customer_name);


        }

    }
}