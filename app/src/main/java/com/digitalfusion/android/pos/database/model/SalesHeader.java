package com.digitalfusion.android.pos.database.model;

import com.digitalfusion.android.pos.util.POSUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created by MD003 on 8/29/16.
 */
public class SalesHeader implements Serializable{

    private Long id;
    private Long customerID;
    private String customerName;
    private String customerPhone;
    private String customerAddress;
    private String saleDate;
    private String taxName;
    private Long taxID;
    private double taxRate;
    private String discount;
    private String saleVocherNo;
    private Double subtotal;
    private Double total;
    private String type;
    private Double discountAmt;
    private Double taxAmt;
    private Double paidAmt;
    private Double balance;
    private String remark;
    private String day;
    private String month;
    private String year;
    private String headerText;
    private String taxType;
    private String footerText;
    private Long discountID;
    private int isHold = 0;
    private List<SalesAndPurchaseItem> salesAndPurchaseItemList;
    private List<SalesAndPurchaseItem> updateList;
    private List<Long> deleteList;
    private Long userId;
    private Boolean isSaleEdit;
    private Boolean isSaleHold;

    public Boolean getSaleEdit() {
        return isSaleEdit;
    }

    public void setSaleEdit(Boolean saleEdit) {
        isSaleEdit = saleEdit;
    }

    public Boolean getSaleHold() {
        return isSaleHold;
    }

    public void setSaleHold(Boolean saleHold) {
        isSaleHold = saleHold;
    }

    public String getDisInPercent() {
        return disInPercent;
    }

    public void setDisInPercent(String disInPercent) {
        this.disInPercent = disInPercent;
    }

    private String disInPercent;

    public SaleDelivery getSaleDelivery() {
        return saleDelivery;
    }

    public void setSaleDelivery(SaleDelivery saleDelivery) {
        this.saleDelivery = saleDelivery;
    }

    private SaleDelivery saleDelivery;

    public String getChangeBalance() {
        return changeBalance;
    }

    public void setChangeBalance(String changeBalance) {
        this.changeBalance = changeBalance;
    }

    private  String changeBalance; // add change_blance_string



    public SalesHeader() {
    }

    public String getFooterText() {
        return footerText;
    }

    public void setFooterText(String footerText) {
        this.footerText = footerText;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getCustomerID() {

        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public Long getTaxID() {
        return taxID;
    }

    public void setTaxID(Long taxID) {
        this.taxID = taxID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public String getSaleVocherNo() {
        return saleVocherNo;
    }

    public void setSaleVocherNo(String saleVocherNo) {
        this.saleVocherNo = saleVocherNo;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(Double discountAmt) {
        this.discountAmt = discountAmt;
    }

    public Double getTaxAmt() {
        return taxAmt;
    }

    public void setTaxAmt(Double taxAmt) {
        this.taxAmt = taxAmt;
    }

    public Double getPaidAmt() {
        return paidAmt;
    }

    public void setPaidAmt(Double paidAmt) {
        this.paidAmt = paidAmt;
    }

    public String getPaidAmtString() {
        return POSUtil.NumberFormat(paidAmt);
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public Long getDiscountID() {
        return discountID;
    }

    public void setDiscountID(Long discountID) {
        this.discountID = discountID;
    }

    public int getIsHold() {
        return isHold;
    }

    public void setIsHold(int isHold) {
        this.isHold = isHold;
    }

    public boolean isHold() {
        return isHold == 1;
    }

    public List<SalesAndPurchaseItem> getSalesAndPurchaseItemList() {
        return salesAndPurchaseItemList;
    }

    public void setSalesAndPurchaseItemList(List<SalesAndPurchaseItem> salesAndPurchaseItemList) {
        this.salesAndPurchaseItemList = salesAndPurchaseItemList;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public List<SalesAndPurchaseItem> getUpdateList() {
        return updateList;
    }

    public void setUpdateList(List<SalesAndPurchaseItem> updateList) {
        this.updateList = updateList;
    }

    public List<Long> getDeleteList() {
        return deleteList;
    }

    public void setDeleteList(List<Long> deleteList) {
        this.deleteList = deleteList;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return customerName + balance;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }
}
