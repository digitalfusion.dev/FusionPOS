package com.digitalfusion.android.pos.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.DeliveryCancelSearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForSaleCancelTransaction;
import com.digitalfusion.android.pos.database.business.SalesOrderManager;
import com.digitalfusion.android.pos.fragments.salefragments.SaleDetailFragment;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.util.DateUtility;
import com.example.searchview.MaterialSearchView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 11/4/16.
 */

public class DeliveryCancelListFragment extends Fragment {

    View mainLayoutView;

    private RecyclerView recyclerView;

    private Context context;

    private SalesOrderManager salesOrderManager;

    private RVSwipeAdapterForSaleCancelTransaction rvSwipeAdapterForSaleCancelTransaction;

    private List<SalesHistory> saleCancelList;

    private MaterialSearchView searchView;


    private String todayDate;

    private List<String> filterList;

    private RVAdapterForFilter rvAdapterForFilter;

    private MaterialDialog filterDialog;

    private TextView filterLayout;

    private MaterialDialog orderPutBackAlertDialog;

    private MDButton yesOrderPutBackMdButton;

    private int clickPosition;

    private String startDate;

    private String endDate;

    private DeliveryCancelSearchAdapter deliveryCancelSearchAdapter;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {

            String str = ((SalesHistory) resultValue).getVoucherNo();

            return str;

        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            if (constraint != null && !constraint.equals("")) {

                //suggestion.clear();

                //  queryText=constraint.toString();

                //   lenght=constraint.length();

                //    suggestion=salesBusiness.getSalesOnSearchWithVoucherNoOrCustomer(0,100,constraint.toString());

                deliveryCancelSearchAdapter.setSuggestion(salesOrderManager.getDeliveryCancelsOnSearch(0, 20, startDate, endDate, constraint.toString()));
                FilterResults filterResults = new FilterResults();

                deliveryCancelSearchAdapter.lenght = constraint.length();

                deliveryCancelSearchAdapter.queryText = constraint.toString();

                filterResults.values = deliveryCancelSearchAdapter.getSuggestion();

                filterResults.count = deliveryCancelSearchAdapter.getSuggestion().size();

                return filterResults;
            } else {

                return new FilterResults();

            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            List<SalesHistory> filterList = (ArrayList<SalesHistory>) results.values;

            if (results != null && results.count > 0) {

                deliveryCancelSearchAdapter.notifyDataSetChanged();


            }

        }

    };
    private TextView noTransactionTextView;
    private TextView searchedResultTxt;
    private MDButton customRangeOkBtn, customRangeCancelBtn;
    private MaterialDialog customRangeDialog;
    private TextView startDateTextView, endDateTextView;
    private String customStartDate,
            customEndDate;
    private TextView traceDate;
    private DatePickerDialog startDatePickerDialog;
    private DatePickerDialog customeDatePickerDialog;
    private Calendar calendar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.delivery_and_pick_up_list_fragment, null);

        context = getContext();

        calendar = Calendar.getInstance();

        salesOrderManager = new SalesOrderManager(context);

        todayDate = DateUtility.getTodayDate();

        noTransactionTextView = (TextView) mainLayoutView.findViewById(R.id.no_transaction);

        filterLayout = (TextView) mainLayoutView.findViewById(R.id.filter_one);

        searchedResultTxt = (TextView) mainLayoutView.findViewById(R.id.searched_result_txt);

        loadUIFromToolbar();

        buildOrderCancelAlertDialog();
        MainActivity.setCurrentFragment(this);
        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        filterList = new ArrayList<>();

        filterList.add("All");

        filterList.add("This Week");

        filterList.add("Last  Week");

        filterList.add("This Month");

        filterList.add("Last Month");

        filterList.add("This Year");

        filterList.add("Last Year");

        filterList.add("Custom Range");

        filterList.add("Custom Date");

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();

        prepareForCancelDelivery();

        buildingCustomRangeDialog();

        filterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });

        deliveryCancelSearchAdapter = new DeliveryCancelSearchAdapter(context, nameFilter);

        searchView.setAdapter(deliveryCancelSearchAdapter);

        rvAdapterForFilter.setCurrentPos(3);

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                saleCancelList = new ArrayList<SalesHistory>();

                saleCancelList.add(deliveryCancelSearchAdapter.getSuggestion().get(position));

                refreshList();

                searchView.closeSearch();

                filterLayout.setText("-");

                searchedResultTxt.setVisibility(View.VISIBLE);
            }
        });


        buildingCustomRangeDatePickerDialog();

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                saleCancelList = salesOrderManager.getDeliveryCancelsOnSearch(0, 20, startDate, endDate, query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        rvSwipeAdapterForSaleCancelTransaction.setViewDetailClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle = new Bundle();

                bundle.putLong(SaleDetailFragment.KEY, saleCancelList.get(postion).getId());

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.SALE_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);
            }
        });


        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDate = customStartDate;

                endDate = customEndDate;

                String startDayDes[] = DateUtility.dayDes(startDate);

                String startYearMonthDes = DateUtility.monthYearDes(startDate);


                String endDayDes[] = DateUtility.dayDes(endDate);

                String endYearMonthDes = DateUtility.monthYearDes(endDate);


                //f.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));

                setFilterText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));

                // loadSaleHistory(0, 10);

                // refreshRecyclerView();

                loadList(0, 10);

                refreshList();

                customRangeDialog.dismiss();
            }
        });


    }

    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        searchView.setCursorDrawable(attributeResourceId);

    }

    private void configCustomDatePickerDialog() {
        customeDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


                        startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                        String date = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        String dayDes[] = DateUtility.dayDes(date);

                        String yearMonthDes = DateUtility.monthYearDes(date);

                        setFilterText(date);
                        //.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

                        loadList(0, 10);

                        refreshList();
                    }
                },
                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)
        );

        TypedArray allTransArr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.pickerAccentColor});

        customeDatePickerDialog.setAccentColor(allTransArr.getColor(0, 0));

    }

    private void prepareForCancelDelivery() {


        setFilterText(filterList.get(0));

        saleCancelList = salesOrderManager.getDeliveryCancel(todayDate, "99999999", 0, 100);

        rvSwipeAdapterForSaleCancelTransaction = new RVSwipeAdapterForSaleCancelTransaction(saleCancelList);

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.delivery_pickup_list_rv);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(rvSwipeAdapterForSaleCancelTransaction);


        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                searchedResultTxt.setVisibility(View.INVISIBLE);

                if (position == 0) {

                    setFilterText(filterList.get(position));

                    startDate = "0000000";

                    endDate = "999999999";

                    loadList(0, 10);

                    refreshList();

                } else if (position == 1) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getStartDateOfWeekString();

                    endDate = DateUtility.getEndDateOfWeekString();

                    loadList(0, 10);

                    refreshList();

                } else if (position == 2) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getStartDateOfLastWeekString();

                    endDate = DateUtility.getEndDateOfLastWeekString();

                    loadList(0, 10);

                    refreshList();

                } else if (position == 3) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getThisMonthStartDate();

                    endDate = DateUtility.getThisMonthEndDate();

                    loadList(0, 10);

                    refreshList();

                } else if (position == 4) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    loadList(0, 10);

                    refreshList();

                } else if (position == 5) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    loadList(0, 10);

                    refreshList();

                } else if (position == 6) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    loadList(0, 10);

                    refreshList();

                } else if (position == 7) {

                    customRangeDialog.show();

                } else if (position == 8) {

                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "custom");
                }

                filterDialog.dismiss();
            }
        });
    }

    private void loadList(int startLimit, int endLimit) {
        saleCancelList = salesOrderManager.getDeliveryCancel(startDate, endDate, startLimit, endLimit);
    }

    private void refreshList() {

        if (saleCancelList.size() < 1) {

            recyclerView.setVisibility(View.GONE);

            noTransactionTextView.setVisibility(View.VISIBLE);

        } else {


            recyclerView.setVisibility(View.VISIBLE);

            noTransactionTextView.setVisibility(View.GONE);

            rvSwipeAdapterForSaleCancelTransaction.setSalesHistoryList(saleCancelList);

            rvSwipeAdapterForSaleCancelTransaction.notifyDataSetChanged();

        }
    }

    private void buildOrderCancelAlertDialog() {
        orderPutBackAlertDialog = new MaterialDialog.Builder(context)
                .title("Are you sure  want to put back?")
                .positiveText("Yes")
                .negativeText("No")
                .build();

        yesOrderPutBackMdButton = orderPutBackAlertDialog.getActionButton(DialogAction.POSITIVE);


    }

    public void buildingCustomRangeDialog() {

        TypedArray dateRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range});
        TypedArray cancel    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});
        TypedArray ok        = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ok});

        buildingCustomRangeDatePickerDialog();
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(dateRange.getString(0))
                .customView(R.layout.custome_range, true)
                .negativeText(cancel.getString(0))
                .positiveText(ok.getString(0))
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = customRangeDialog.getActionButton(DialogAction.POSITIVE);
        customRangeCancelBtn = customRangeDialog.getActionButton(DialogAction.POSITIVE);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    public void buildingCustomRangeDatePickerDialog() {

        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)

                , now.get(Calendar.MONTH)

                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });
    }

    private void setFilterText(String txt) {
        filterLayout.setText(txt);
    }

    private void buildDateFilterDialog() {
        filterDialog = new MaterialDialog.Builder(context).
                title("Filter By Date")
                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))
                .build();
    }


}
