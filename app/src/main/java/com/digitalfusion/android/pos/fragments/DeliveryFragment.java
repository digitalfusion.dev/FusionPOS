package com.digitalfusion.android.pos.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.DeliverySearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForSaleDeliveryAndPickupDelivered;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForSaleDeliveryAndPickupUpcommingAndOverDue;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.business.SalesOrderManager;
import com.digitalfusion.android.pos.fragments.salefragments.SaleDetailFragment;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.SaleDeliveryAndPickUp;
import com.digitalfusion.android.pos.database.model.SalesHeader;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.example.searchview.MaterialSearchView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 9/9/16.
 */
public class DeliveryFragment extends Fragment {

    View mainLayoutView;

    private RecyclerView recyclerView;

    private Context context;

    private SalesOrderManager salesOrderManager;

    private RVSwipeAdapterForSaleDeliveryAndPickupUpcommingAndOverDue rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue;

    private RVSwipeAdapterForSaleDeliveryAndPickupDelivered rvSwipeAdapterForSaleDeliveryDelivered;

    private List<SaleDeliveryAndPickUp> saleDeliveryViewList;

    private String todayDate;

    private List<String> filterList;

    private RVAdapterForFilter rvAdapterForFilter;

    private MaterialDialog filterDialog;

    private TextView filterLayout;

    private MaterialDialog orderCancelAlertDialog;

    private MaterialDialog orderMarkAsDelivered;

    private MDButton yesOrderMarkAsDelivered;

    private MaterialDialog orderMarkAsUnDelivered;

    private MDButton yesOrderMarkAsUnDelivered;

    private MDButton yesOrderCancelMdButton;

    private int clickPosition;

    private String startDate;

    private String endDate;

    private TextView noTransactionTextView;


    private Calendar calendar;

    private DeliverySearchAdapter deliverySearchAdapter;

    private MaterialSearchView searchView;

    private MaterialDialog refundDialog;

    private MDButton refundYesBtn;

    private TextView totalAmountTextView;

    private TextView paidAmountTextView;

    private EditText refundAmountEditText;

    private EditText refundRemarkEditText;

    private TextView refundDateTextView;

    private SalesHeader salesHistoryView;

    private SalesManager salesManager;

    private int refundDay;

    private int refundMonth;

    private int refundYear;

    private DatePickerDialog refundDatePickerDialog;

    private Double refundAmount = 0.0;

    private boolean canRefund;

    private String refundRemark;

    private String refundDateString;

    private boolean isUpcomming;

    private DatePickerDialog startDatePickerDialog;

    private MaterialDialog customRangeDialog;

    private TextView startDateTextView, endDateTextView;

    private String customStartDate,
            customEndDate;

    private TextView traceDate;

    private MDButton customRangeOkBtn, customRangeCancelBtn;

    private DatePickerDialog customeDatePickerDialog;

    private TextView searchedResultTxt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.delivery_and_pick_up_list_fragment, null);

        context = getContext();

        salesOrderManager = new SalesOrderManager(context);

        calendar = Calendar.getInstance();

        todayDate = DateUtility.getTodayDate();

        loadUIFromToolbar();
        MainActivity.setCurrentFragment(this);
        refundDay = calendar.get(Calendar.DAY_OF_MONTH);

        refundMonth = calendar.get(Calendar.MONTH) + 1;

        refundYear = calendar.get(Calendar.YEAR);

        salesManager = new SalesManager(context);

        filterLayout = (TextView) mainLayoutView.findViewById(R.id.filter_one);

        searchedResultTxt = (TextView) mainLayoutView.findViewById(R.id.searched_result_txt);

        noTransactionTextView = (TextView) mainLayoutView.findViewById(R.id.no_transaction);

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.delivery_pickup_list_rv);

        buildOrderCancelAlertDialog();

        buildRefundDialogDialog();

        buildRefundDatePickerDialog();

        return mainLayoutView;

    }

    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});

        int attributeResourceId = a.getResourceId(0, 0);

        searchView.setCursorDrawable(attributeResourceId);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        yesOrderCancelMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                orderCancelAlertDialog.dismiss();

                salesHistoryView = salesManager.getSalesHeaderView(saleDeliveryViewList.get(clickPosition).getSaleID());

                totalAmountTextView.setText(POSUtil.NumberFormat(salesHistoryView.getTotal()));

                paidAmountTextView.setText(POSUtil.NumberFormat(salesHistoryView.getPaidAmt()));

                refundDialog.show();

            }
        });

        refundDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refundDatePickerDialog.show(getActivity().getFragmentManager(), "refund");
            }
        });

        refundYesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean status = salesOrderManager.cancelOrder(saleDeliveryViewList.get(clickPosition).getSaleID(), SalesManager.SaleType.DeliveryCancel.toString());

                salesManager.addNewRefund(saleDeliveryViewList.get(clickPosition).getSaleID(), saleDeliveryViewList.get(clickPosition).getCustomerID(), refundAmount,
                        refundRemark, refundDateString, Integer.toString(refundDay), Integer.toString(refundMonth), Integer.toString(refundYear), saleDeliveryViewList.get(clickPosition).getSaleVoucherNo());

                saleDeliveryViewList.remove(clickPosition);

                if (isUpcomming) {
                    refreshUpcommingLIst();
                } else {
                    refreshOverDueList();
                }

                refundDialog.dismiss();
            }
        });

        refundRemarkEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                refundRemark = refundRemarkEditText.getText().toString();

            }
        });

        refundAmountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s != null && !s.toString().equalsIgnoreCase("")) {
                    refundAmount = Double.parseDouble(refundAmountEditText.getText().toString());
                    if (salesHistoryView.getBalance() < refundAmount) {
                        canRefund = false;
                        refundAmountEditText.setError("Refund Amount can't be greater than Paid Amount!!");
                    } else {
                        refundAmountEditText.setError(null);

                        canRefund = true;
                    }

                } else {
                    refundAmountEditText.setError(null);
                    canRefund = true;
                    refundAmount = 0.0;
                }

            }
        });


        if (getArguments().getString("type").equalsIgnoreCase("Upcoming")) {

            isUpcomming = true;
            prepareForUpcomingDelivery();

            configUpcomingCustomDatePickerDialog();

        } else if (getArguments().getString("type").equalsIgnoreCase("Delivered")) {
            isUpcomming = false;
            prepareForDeliverdDelivery();

            configDeliveredCustomDatePickerDialog();

        } else if (getArguments().getString("type").equalsIgnoreCase("OverDue")) {
            isUpcomming = false;
            prepareOverdueDelivery();

            configOverduedCustomDatePickerDialog();

        }

        filterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });

        rvAdapterForFilter.setCurrentPos(1);

    }


    private void prepareOverdueDelivery() {


        TypedArray allTrans = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all});

        TypedArray thisMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month});

        TypedArray lastMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month});

        TypedArray thisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year});

        TypedArray lastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year});

        final TypedArray customRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_range});

        TypedArray customDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_date});

        filterList = new ArrayList<>();

        filterList.add(allTrans.getString(0));

        filterList.add(thisMonth.getString(0));

        filterList.add(lastMonth.getString(0));

        filterList.add(thisYear.getString(0));

        filterList.add(lastYear.getString(0));

        //filterList.add(customRange.getString(0));

        //  filterList.add(customDate.getString(0));

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();

        setFilterText(filterList.get(0));

        buildOrderMarkAsDeliveredAlertDialog();

        saleDeliveryViewList = salesOrderManager.getOverduePickupAndDelivery("00000000", todayDate, 0, 10);

        startDate = "000000000";

        endDate = todayDate;

        rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue = new RVSwipeAdapterForSaleDeliveryAndPickupUpcommingAndOverDue(saleDeliveryViewList);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue);

        loadList(0, 10);

        Filter nameFilter = new Filter() {
            @Override
            public CharSequence convertResultToString(Object resultValue) {

                String str = ((SalesHistory) resultValue).getVoucherNo();

                return str;

            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                if (constraint != null && !constraint.equals("")) {

                    // suggestion.clear();

                    // queryText=constraint.toString();

                    // lenght=constraint.length();
                    if (constraint.toString().startsWith("#")) {
                        deliverySearchAdapter.setSuggestion(salesOrderManager.getOverduePickupAndDeliveryOnSearch(startDate, endDate, 0, 30, constraint.toString().substring(1, constraint.length())));
                    } else {
                        deliverySearchAdapter.setSuggestion(salesOrderManager.getOverduePickupAndDeliveryOnSearch(startDate, endDate, 0, 30, constraint.toString()));
                    }

                    FilterResults filterResults = new FilterResults();

                    deliverySearchAdapter.lenght = constraint.length();

                    deliverySearchAdapter.queryText = constraint.toString();

                    filterResults.values = deliverySearchAdapter.getSuggestion();

                    filterResults.count = deliverySearchAdapter.getSuggestion().size();

                    return filterResults;
                } else {

                    return new FilterResults();

                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                if (results != null && results.count > 0) {

                    deliverySearchAdapter.notifyDataSetChanged();

                }

            }

        };

        deliverySearchAdapter = new DeliverySearchAdapter(context, nameFilter);

        searchView.setAdapter(deliverySearchAdapter);

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                saleDeliveryViewList = new ArrayList<SaleDeliveryAndPickUp>();

                saleDeliveryViewList.add(deliverySearchAdapter.getSuggestion().get(position));

                refreshOverDueList();

                searchView.closeSearch();

                filterLayout.setText("-");

                searchedResultTxt.setVisibility(View.VISIBLE);

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                saleDeliveryViewList = salesOrderManager.getUpcomingPickupAndDeliveryOnSearch(startDate, endDate, 0, 20, query);

                refreshOverDueList();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.setViewDetailClicklistener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle = new Bundle();

                bundle.putLong(SaleDetailFragment.KEY, saleDeliveryViewList.get(postion).getSaleID());

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.SALE_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);

            }
        });

        rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.setmDeliverClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                if (saleDeliveryViewList.get(postion).getPickupOrDeliveryStatus() != 1) {

                    clickPosition = postion;

                    orderMarkAsDelivered.show();

                }

            }
        });

        yesOrderMarkAsDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean status = salesOrderManager.makeOrderDelivered(saleDeliveryViewList.get(clickPosition).getPickupOrDeliveryID());

                if (status) {

                    saleDeliveryViewList.remove(clickPosition);

                    refreshList(clickPosition);

                    orderMarkAsDelivered.dismiss();

                }
            }
        });


        rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.setmOrderCancelClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                if (saleDeliveryViewList.get(postion).getPickupOrDeliveryStatus() != 1) {

                    clickPosition = postion;

                    orderCancelAlertDialog.show();

                }
            }
        });

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                searchedResultTxt.setVisibility(View.INVISIBLE);

                if (position == 0) {

                    startDate = "0000000000";

                    endDate = Integer.toString(Integer.parseInt(todayDate) - 1);

                    setFilterText(filterList.get(position));

                    loadList(0, 10);

                } else if (position == 1) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getThisMonthStartDate();

                    endDate = Integer.toString(Integer.parseInt(todayDate) - 1);

                    loadList(0, 10);

                } else if (position == 2) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    loadList(0, 10);

                } else if (position == 3) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getThisYearStartDate();

                    endDate = Integer.toString(Integer.parseInt(todayDate) - 1);

                    loadList(0, 10);

                } else if (position == 4) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    loadList(0, 10);

                } else if (position == 5) {

                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "custom");

                }
                Log.w("end date", endDate + " s" + startDate);
                filterDialog.dismiss();
            }
        });
    }

    private void loadList(int startLimit, int endLimit) {

        saleDeliveryViewList = salesOrderManager.getOverduePickupAndDelivery(startDate, endDate, startLimit, endLimit);

        refreshOverDueList();

    }

    private void refreshOverDueList() {
        if (saleDeliveryViewList.size() > 0 && !saleDeliveryViewList.isEmpty()) {

            rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.setDeliveryAndPickUpViewList(saleDeliveryViewList);

            rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.notifyDataSetChanged();

            noTransactionTextView.setVisibility(View.GONE);

            recyclerView.setVisibility(View.VISIBLE);

        } else {

            noTransactionTextView.setVisibility(View.VISIBLE);

            recyclerView.setVisibility(View.GONE);

        }
    }

    private void prepareForDeliverdDelivery() {


        TypedArray allTrans = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all});

        TypedArray thisMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month});

        TypedArray lastMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month});

        TypedArray thisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year});

        TypedArray lastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year});

        final TypedArray customRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_range});

        TypedArray customDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_date});

        filterList = new ArrayList<>();

        filterList.add(allTrans.getString(0));

        filterList.add(thisMonth.getString(0));

        filterList.add(lastMonth.getString(0));

        filterList.add(thisYear.getString(0));

        filterList.add(lastYear.getString(0));

        filterList.add(customRange.getString(0));

        filterList.add(customDate.getString(0));

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();

        setFilterText(filterList.get(0));

        buildOrderMarkAsUnDeliveredAlertDialog();

        startDate = "000000000000";

        endDate = "999999999999";

        saleDeliveryViewList = salesOrderManager.getDeliveredPickupAndDelivery("0000000", "99999999", 0, 10);

        rvSwipeAdapterForSaleDeliveryDelivered = new RVSwipeAdapterForSaleDeliveryAndPickupDelivered(saleDeliveryViewList);


        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(rvSwipeAdapterForSaleDeliveryDelivered);

        Filter nameFilter = new Filter() {
            @Override
            public CharSequence convertResultToString(Object resultValue) {

                String str = ((SalesHistory) resultValue).getVoucherNo();

                return str;

            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                if (constraint != null && !constraint.equals("")) {

                    // suggestion.clear();

                    // queryText=constraint.toString();

                    // lenght=constraint.length();

                    if (constraint.toString().startsWith("#")) {
                        deliverySearchAdapter.setSuggestion(salesOrderManager.getDeliveredPickupAndDeliveryOnSearch(startDate, endDate, 0, 30, constraint.toString().substring(1, constraint.length())));
                    } else {
                        deliverySearchAdapter.setSuggestion(salesOrderManager.getDeliveredPickupAndDeliveryOnSearch(startDate, endDate, 0, 30, constraint.toString()));
                    }

                    FilterResults filterResults = new FilterResults();

                    deliverySearchAdapter.lenght = constraint.length();

                    deliverySearchAdapter.queryText = constraint.toString();

                    filterResults.values = deliverySearchAdapter.getSuggestion();

                    filterResults.count = deliverySearchAdapter.getSuggestion().size();

                    return filterResults;
                } else {

                    return new FilterResults();

                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                if (results != null && results.count > 0) {

                    deliverySearchAdapter.notifyDataSetChanged();

                }

            }

        };

        deliverySearchAdapter = new DeliverySearchAdapter(context, nameFilter);

        searchView.setAdapter(deliverySearchAdapter);

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                saleDeliveryViewList = new ArrayList<SaleDeliveryAndPickUp>();

                saleDeliveryViewList.add(deliverySearchAdapter.getSuggestion().get(position));

                refreshDeliveredList();

                searchView.closeSearch();

                filterLayout.setText("-");

                searchedResultTxt.setVisibility(View.VISIBLE);

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                saleDeliveryViewList = salesOrderManager.getUpcomingPickupAndDeliveryOnSearch(startDate, endDate, 0, 20, query);

                refreshDeliveredList();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        rvSwipeAdapterForSaleDeliveryDelivered.setViewDetailsClicklistener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle = new Bundle();

                bundle.putLong(SaleDetailFragment.KEY, saleDeliveryViewList.get(postion).getSaleID());

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.SALE_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);
            }
        });

        refreshDeliveredList();

        rvSwipeAdapterForSaleDeliveryDelivered.setCancelClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                if (saleDeliveryViewList.get(postion).getPickupOrDeliveryStatus() == 1) {

                    clickPosition = postion;
                    orderMarkAsUnDelivered.show();

                }

            }
        });

        yesOrderMarkAsUnDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean status = salesOrderManager.makeOrderUnDelivered(saleDeliveryViewList.get(clickPosition).getPickupOrDeliveryID());

                if (status) {

                    orderMarkAsUnDelivered.dismiss();

                    saleDeliveryViewList.remove(clickPosition);

                    refreshDeliveredListRemove(clickPosition);


                }
            }
        });

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                searchedResultTxt.setVisibility(View.INVISIBLE);

                if (position == 0) {

                    setFilterText(filterList.get(position));

                    startDate = "000000000000";

                    endDate = "9999999999999999";

                    loadDeliveredList(0, 10);

                    refreshDeliveredList();

                } else if (position == 1) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getThisMonthStartDate();

                    endDate = DateUtility.getThisMonthEndDate();

                    loadDeliveredList(0, 10);

                    refreshDeliveredList();

                } else if (position == 2) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    loadDeliveredList(0, 10);

                    refreshDeliveredList();

                } else if (position == 3) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    loadDeliveredList(0, 10);

                    refreshDeliveredList();

                } else if (position == 4) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    loadDeliveredList(0, 10);

                    refreshDeliveredList();

                } else if (position == 5) {

                    customRangeDialog.show();

                } else if (position == 6) {

                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "custom");

                }

                filterDialog.dismiss();
            }
        });


        buildingDeliveredCustomRangeDialog();

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDate = customStartDate;

                endDate = customEndDate;

                // String startDayDes[]= DateUtility.dayDes(startDate);

                //String startYearMonthDes= DateUtility.monthYearDes(startDate);


                // String endDayDes[]= DateUtility.dayDes(endDate);

                // String endYearMonthDes= DateUtility.monthYearDes(endDate);


                // filterTextView.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));

                setFilterText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));

                loadDeliveredList(0, 10);

                refreshDeliveredList();

                customRangeDialog.dismiss();
            }
        });

    }

    private void loadDeliveredList(int startLimit, int endLimit) {

        saleDeliveryViewList = salesOrderManager.getDeliveredPickupAndDelivery(startDate, endDate, 0, 10);

    }

    private void refreshDeliveredList() {

        rvSwipeAdapterForSaleDeliveryDelivered.setDeliveryAndPickUpViewList(saleDeliveryViewList);

        recyclerView.setAdapter(rvSwipeAdapterForSaleDeliveryDelivered);

        rvSwipeAdapterForSaleDeliveryDelivered.notifyDataSetChanged();

        if (saleDeliveryViewList.size() < 1 && saleDeliveryViewList.isEmpty()) {

            noTransactionTextView.setVisibility(View.VISIBLE);

            recyclerView.setVisibility(View.GONE);

        } else {

            noTransactionTextView.setVisibility(View.GONE);

            recyclerView.setVisibility(View.VISIBLE);

        }

    }

    private void refreshDeliveredListRemove(int postion) {

        if (postion != 0) {


            rvSwipeAdapterForSaleDeliveryDelivered.setDeliveryAndPickUpViewList(saleDeliveryViewList);

            rvSwipeAdapterForSaleDeliveryDelivered.notifyItemRemoved(postion);

            rvSwipeAdapterForSaleDeliveryDelivered.notifyItemRangeChanged(postion, saleDeliveryViewList.size());

        } else {

            rvSwipeAdapterForSaleDeliveryDelivered.notifyDataSetChanged();

        }
    }


    private void prepareForUpcomingDelivery() {


        Filter nameFilter = new Filter() {
            @Override
            public CharSequence convertResultToString(Object resultValue) {

                String str = ((SalesHistory) resultValue).getVoucherNo();

                return str;

            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                if (constraint != null && !constraint.equals("")) {

                    // suggestion.clear();

                    // queryText=constraint.toString();

                    // lenght=constraint.length();

                    if (constraint.toString().startsWith("#")) {
                        deliverySearchAdapter.setSuggestion(salesOrderManager.getUpcomingPickupAndDeliveryOnSearch(startDate, endDate, 0, 30, constraint.toString().substring(1, constraint.length())));
                    } else {
                        deliverySearchAdapter.setSuggestion(salesOrderManager.getUpcomingPickupAndDeliveryOnSearch(startDate, endDate, 0, 30, constraint.toString()));
                    }

                    FilterResults filterResults = new FilterResults();

                    deliverySearchAdapter.lenght = constraint.length();

                    deliverySearchAdapter.queryText = constraint.toString();

                    filterResults.values = deliverySearchAdapter.getSuggestion();

                    filterResults.count = deliverySearchAdapter.getSuggestion().size();

                    return filterResults;
                } else {

                    return new FilterResults();

                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                if (results != null && results.count > 0) {

                    deliverySearchAdapter.notifyDataSetChanged();

                }

            }

        };

        deliverySearchAdapter = new DeliverySearchAdapter(context, nameFilter);

        searchView.setAdapter(deliverySearchAdapter);

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                saleDeliveryViewList = new ArrayList<SaleDeliveryAndPickUp>();

                saleDeliveryViewList.add(deliverySearchAdapter.getSuggestion().get(position));

                refreshUpcommingLIst();

                searchView.closeSearch();

                filterLayout.setText("-");

                searchedResultTxt.setVisibility(View.VISIBLE);

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                saleDeliveryViewList = salesOrderManager.getUpcomingPickupAndDeliveryOnSearch(startDate, endDate, 0, 20, query);

                refreshUpcommingLIst();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        TypedArray all = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all});

        TypedArray withinOneWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.within_one_week});

        TypedArray withinTwoWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.within_two_week});

        TypedArray withinOneMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.within_one_month});

        TypedArray withinOneYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.within_one_year});

        // TypedArray customDate=context.getTheme().obtainStyledAttributes(new int[]{R.attr.customDate});

        filterList = new ArrayList<>();

        filterList.add(all.getString(0));

        filterList.add(withinOneWeek.getString(0));

        filterList.add(withinTwoWeek.getString(0));

        filterList.add(withinOneMonth.getString(0));

        filterList.add(withinOneYear.getString(0));

        //  filterList.add(customDate.getString(0));

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();

        buildOrderMarkAsDeliveredAlertDialog();

        setFilterText(filterList.get(0));

        saleDeliveryViewList = salesOrderManager.getUpcomingPickupAndDelivery(todayDate, "99999999", 0, 10);

        rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue = new RVSwipeAdapterForSaleDeliveryAndPickupUpcommingAndOverDue(saleDeliveryViewList);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue);

        refreshUpcommingLIst();

        rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.setViewDetailClicklistener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle = new Bundle();

                bundle.putLong(SaleDetailFragment.KEY, saleDeliveryViewList.get(postion).getSaleID());

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.SALE_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);

            }
        });

        rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.setmDeliverClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                if (saleDeliveryViewList.get(postion).getPickupOrDeliveryStatus() != 1) {

                    clickPosition = postion;

                    orderMarkAsDelivered.show();

                }

            }
        });

        yesOrderMarkAsDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean status = salesOrderManager.makeOrderDelivered(saleDeliveryViewList.get(clickPosition).getPickupOrDeliveryID());

                if (status) {

                    saleDeliveryViewList.remove(clickPosition);

                    Log.w("here", clickPosition + " S");

                    Log.w("here", saleDeliveryViewList.size() + " S");

                    refreshList(clickPosition);

                    orderMarkAsDelivered.dismiss();


                    //POSUtil.showSnackBar(v);
                }
            }
        });

        rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.setmOrderCancelClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                if (saleDeliveryViewList.get(postion).getPickupOrDeliveryStatus() != 1) {

                    clickPosition = postion;

                    orderCancelAlertDialog.show();

                }
            }
        });

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                searchedResultTxt.setVisibility(View.INVISIBLE);
                if (position == 0) {

                    setFilterText(filterList.get(position));

                    saleDeliveryViewList = salesOrderManager.getUpcomingPickupAndDelivery(todayDate, "99999999", 0, 10);

                    refreshUpcommingLIst();

                } else if (position == 1) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getTodayDate();

                    endDate = DateUtility.getEndDateOfWeekString();


                    saleDeliveryViewList = salesOrderManager.getUpcomingPickupAndDelivery(startDate, endDate, 0, 10);

                    refreshUpcommingLIst();

                } else if (position == 2) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getTodayDate();

                    endDate = DateUtility.getEndDateOfTwoWeekString();

                    saleDeliveryViewList = salesOrderManager.getUpcomingPickupAndDelivery(startDate, endDate, 0, 10);

                    refreshUpcommingLIst();

                } else if (position == 3) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getTodayDate();

                    endDate = DateUtility.getThisMonthEndDate();

                    saleDeliveryViewList = salesOrderManager.getUpcomingPickupAndDelivery(startDate, endDate, 0, 10);

                    refreshUpcommingLIst();

                } else if (position == 4) {

                    setFilterText(filterList.get(position));

                    startDate = DateUtility.getTodayDate();

                    endDate = DateUtility.getThisYearEndDate();

                    saleDeliveryViewList = salesOrderManager.getUpcomingPickupAndDelivery(startDate, endDate, 0, 10);

                    refreshUpcommingLIst();

                } else if (position == 5) {
                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "custom");

                }
                Log.w("end date", endDate + " s" + startDate);

                filterDialog.dismiss();
            }
        });
    }

    private void refreshUpcommingLIst() {

        recyclerView.setAdapter(rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue);

        rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.setDeliveryAndPickUpViewList(saleDeliveryViewList);

        rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.notifyDataSetChanged();

        Log.w("here", saleDeliveryViewList.size() + " SS");

        if (saleDeliveryViewList.isEmpty() && saleDeliveryViewList.size() < 1) {

            recyclerView.setVisibility(View.GONE);

            noTransactionTextView.setVisibility(View.VISIBLE);

        } else {

            recyclerView.setVisibility(View.VISIBLE);

            noTransactionTextView.setVisibility(View.GONE);

        }
    }

    private void refreshList(int postion) {

        if (postion != 0) {

            rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.setDeliveryAndPickUpViewList(saleDeliveryViewList);

            rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.notifyItemRemoved(postion);

            rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.notifyItemRangeChanged(postion, saleDeliveryViewList.size());

        } else {

            rvSwipeAdapterForSaleDeliveryUpcommingAndOverDue.notifyDataSetChanged();

        }

        if (saleDeliveryViewList.size() < 1 && saleDeliveryViewList.isEmpty()) {

            recyclerView.setVisibility(View.GONE);

            noTransactionTextView.setVisibility(View.VISIBLE);


        } else {

            recyclerView.setVisibility(View.VISIBLE);

            noTransactionTextView.setVisibility(View.GONE);

        }


    }


    private void buildOrderCancelAlertDialog() {

        TypedArray no  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        orderCancelAlertDialog = new MaterialDialog.Builder(context)
                .title("Are you sure  want to cancel?")
                .positiveText(yes.getString(0))
                .negativeText(no.getString(0))
                .build();

        yesOrderCancelMdButton = orderCancelAlertDialog.getActionButton(DialogAction.POSITIVE);


    }


    private void buildOrderMarkAsDeliveredAlertDialog() {

        TypedArray no  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        orderMarkAsDelivered = new MaterialDialog.Builder(context)
                .title("Mark as Delivered?")
                .positiveText(yes.getString(0))
                .negativeText(no.getString(0))
                .build();

        yesOrderMarkAsDelivered = orderMarkAsDelivered.getActionButton(DialogAction.POSITIVE);


    }

    private void buildOrderMarkAsUnDeliveredAlertDialog() {

        TypedArray no  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        orderMarkAsUnDelivered = new MaterialDialog.Builder(context)
                .title("Mark as UnDelivered?")
                .positiveText(yes.getString(0))
                .negativeText(no.getString(0))
                .build();

        yesOrderMarkAsUnDelivered = orderMarkAsUnDelivered.getActionButton(DialogAction.POSITIVE);


    }

    private void buildRefundDialogDialog() {

        TypedArray no  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        refundDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.refund_dialog, true)
                .title("Refund")
                .positiveText(yes.getString(0))
                .negativeText(no.getString(0))
                .build();

        refundYesBtn = refundDialog.getActionButton(DialogAction.POSITIVE);

        totalAmountTextView = (TextView) refundDialog.findViewById(R.id.total_amt);

        paidAmountTextView = (TextView) refundDialog.findViewById(R.id.paid_amt);

        refundAmountEditText = (EditText) refundDialog.findViewById(R.id.refund_amt);

        refundRemarkEditText = (EditText) refundDialog.findViewById(R.id.remark_et);

        refundDateTextView = (TextView) refundDialog.findViewById(R.id.date);

        refundDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(refundYear), Integer.toString(refundMonth), Integer.toString(refundDay)));

        refundDateString = DateUtility.makeDate(Integer.toString(refundYear), Integer.toString(refundMonth), Integer.toString(refundDay));

    }


    private void setFilterText(String txt) {
        filterLayout.setText(txt);
    }

    private void buildDateFilterDialog() {


        TypedArray filterByDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date});

        filterDialog = new MaterialDialog.Builder(context).
                title(filterByDate.getString(0))
                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))
                .build();
    }

    private void configUpcomingCustomDatePickerDialog() {
        customeDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


                        startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                        String date = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        String dayDes[] = DateUtility.dayDes(date);

                        String yearMonthDes = DateUtility.monthYearDes(date);

                        //filterLayout.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

                        filterLayout.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));

                        refreshUpcommingLIst();

                    }
                },
                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)
        );

        TypedArray allTransArr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.pickerAccentColor});

        customeDatePickerDialog.setAccentColor(allTransArr.getColor(0, 0));

        if (POSUtil.isNightMode(context)) {
            customeDatePickerDialog.setThemeDark(true);
        } else {
            customeDatePickerDialog.setThemeDark(false);
        }
    }

    private void buildRefundDatePickerDialog() {
        refundDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        //    startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        //  endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                        //    String date = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        //String dayDes[] = DateUtility.dayDes(date);

                        //  String yearMonthDes = DateUtility.monthYearDes(date);

                        //filterLayout.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

                        refundDay = dayOfMonth;

                        refundMonth = monthOfYear + 1;

                        refundYear = year;

                        refundDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));

                        refreshUpcommingLIst();

                    }
                },
                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)
        );

        TypedArray allTransArr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.pickerAccentColor});

        refundDatePickerDialog.setAccentColor(allTransArr.getColor(0, 0));

        if (POSUtil.isNightMode(context)) {
            refundDatePickerDialog.setThemeDark(true);
        } else {
            refundDatePickerDialog.setThemeDark(false);
        }
    }


    private void configDeliveredCustomDatePickerDialog() {
        customeDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


                        startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        String date = DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        //  String dayDes[] = DateUtility.dayDes(date);

                        //    String yearMonthDes = DateUtility.monthYearDes(date);

                        filterLayout.setText(date);

                        loadDeliveredList(0, 10);

                        refreshDeliveredList();

                    }
                },
                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)
        );

        TypedArray allTransArr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.pickerAccentColor});

        customeDatePickerDialog.setAccentColor(allTransArr.getColor(0, 0));

        if (POSUtil.isNightMode(context)) {
            customeDatePickerDialog.setThemeDark(true);
        } else {
            customeDatePickerDialog.setThemeDark(false);
        }
    }

    private void configOverduedCustomDatePickerDialog() {
        customeDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


                        startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                        String date = DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        // String dayDes[] = DateUtility.dayDes(date);

                        //String yearMonthDes = DateUtility.monthYearDes(date);

                        // filterLayout.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

                        filterLayout.setText(date);

                        loadList(0, 10);

                    }
                },
                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)
        );

        TypedArray allTransArr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.pickerAccentColor});

        customeDatePickerDialog.setAccentColor(allTransArr.getColor(0, 0));

        if (POSUtil.isNightMode(context)) {
            customeDatePickerDialog.setThemeDark(true);
        } else {
            customeDatePickerDialog.setThemeDark(false);
        }

    }

    public void buildingDeliveredCustomRangeDialog() {

        TypedArray dateRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range});

        TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        TypedArray ok = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ok});

        buildingDeliveredCustomRangeDatePickerDialog();

        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(dateRange.getString(0))
                .customView(R.layout.custome_range, true)
                .negativeText(cancel.getString(0))
                .positiveText(ok.getString(0))
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);

        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);

        Calendar now = Calendar.getInstance();

        customRangeOkBtn = customRangeDialog.getActionButton(DialogAction.POSITIVE);

        customRangeCancelBtn = customRangeDialog.getActionButton(DialogAction.POSITIVE);

        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));

        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));

        Integer endDay = DateUtility.getMonthEndDate(now);

        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));

        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                traceDate = startDateTextView;

                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                traceDate = endDateTextView;

                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();

            }
        });

    }

    public void buildingDeliveredCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });


    }


}
