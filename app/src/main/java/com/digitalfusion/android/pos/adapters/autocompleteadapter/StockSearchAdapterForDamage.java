package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.model.StockItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 3/17/17.
 */

public class StockSearchAdapterForDamage extends ArrayAdapter<StockItem> {


    private List<StockItem> suggestion;

    private List<StockItem> searchList;

    private Context context;

    private String queryText;

    private int length;

    //private String startDate, endDate;

    private StockManager stockManager;

    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((StockItem) resultValue).getCodeNo();

            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null && constraint.toString().trim().length() > 0) {
                searchList.clear();

                queryText = constraint.toString().trim();

                length = constraint.length();

                searchList = stockManager.getDamagesForSearch(0, 10, queryText);

                //                Log.w("searhc size",searchList.size()+" SSS");

                FilterResults filterResults = new FilterResults();
                filterResults.values = searchList;
                filterResults.count = searchList.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<StockItem> filterList = (ArrayList<StockItem>) results.values;
            suggestion = searchList;
            if (results != null && results.count > 0) {
                clear();
                for (StockItem vehicle1 : filterList) {
                    add(vehicle1);
                    notifyDataSetChanged();
                }
            }
        }
    };

    public StockSearchAdapterForDamage(Context context) {
        super(context, -1);

        this.context = context;

        stockManager = new StockManager(context);

        // this.startDate = startDate;
        //  this.endDate = endDate;

        suggestion = new ArrayList<>();
        searchList = new ArrayList<>();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.stock_item_auto_complete_view, parent, false);
        }
        viewHolder = new ViewHolder(convertView);

        if (!suggestion.isEmpty() && suggestion.size() > 0) {

            if (suggestion.get(position).getName().toLowerCase().startsWith(queryText.toLowerCase())) {
                Spannable spanText = new SpannableString(suggestion.get(position).getName());


                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, length, 0);

                viewHolder.nameTextView.setText(spanText);

            } else {
                viewHolder.nameTextView.setText(suggestion.get(position).getName());
            }

            if (suggestion.get(position).getCodeNo().toLowerCase().startsWith(queryText.toLowerCase())) {
                Spannable spanText = new SpannableString(suggestion.get(position).getCodeNo());


                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, length, 0);

                viewHolder.codeTextView.setText(spanText);

            } else {
                viewHolder.codeTextView.setText(suggestion.get(position).getCodeNo());
            }

        }
        return convertView;
    }

    @Override
    public int getCount() {
        //Log.e("size", suggestionList.size()+ " od");
        return suggestion.size();
    }

    public List<StockItem> getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(List<StockItem> suggestion) {
        this.suggestion = suggestion;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    static class ViewHolder {
        TextView codeTextView;
        TextView nameTextView;

        public ViewHolder(View itemView) {
            this.nameTextView = (TextView) itemView.findViewById(R.id.name);
            this.codeTextView = (TextView) itemView.findViewById(R.id.code);
        }
    }


}