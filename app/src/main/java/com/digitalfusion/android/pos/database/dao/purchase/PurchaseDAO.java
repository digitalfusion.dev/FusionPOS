package com.digitalfusion.android.pos.database.dao.purchase;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.dao.ParentDAO;
import com.digitalfusion.android.pos.database.dao.IdGeneratorDAO;
import com.digitalfusion.android.pos.database.model.PurchaseHeader;
import com.digitalfusion.android.pos.database.model.PurchaseHistory;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 9/5/16.
 */
public class PurchaseDAO extends ParentDAO {
    private static ParentDAO purchaseDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private PurchaseDetailDAO purchaseDetailDAO;
    private List<PurchaseHistory> purchaseHistoryList;
    private PurchaseHistory purchaseHistory;
    private PurchaseHeader purchaseHeader;
    private List<Long> idList;
    private String db_users;

    private PurchaseDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        purchaseDetailDAO = PurchaseDetailDAO.getPurchaseDetailDaoInstance(context);
        purchaseHeader = new PurchaseHeader();
        db_users = context.getDatabasePath(AppConstant.API_DATABASE).getPath();
    }

    public static PurchaseDAO getPurchaseDaoInstance(Context context) {
        if (purchaseDaoInstance == null) {
            purchaseDaoInstance = new PurchaseDAO(context);
        }
        return (PurchaseDAO) purchaseDaoInstance;
    }

    public boolean addNewPurchase(String voucherNo, String date, Long supplierID, double totalAmt, String discount, Long taxID, double subTotal,
                                  double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year,
                                  Double taxRate, String taxType, List<SalesAndPurchaseItem> purchaseDetailItemList, Long discountID, Long userId) {
        genID = idGeneratorDAO.getLastIdValue("Purchase");
        genID++;
        /*contentValues.put(AppConstant.PURCHASE_ID, genID);
        contentValues.put(AppConstant.PURCHASE_VOUCHER_NUM, voucherNo);
        contentValues.put(AppConstant.PURCHASE_DATE, date);
        contentValues.put(AppConstant.PURCHASE_SUPPLIER_ID, supplierID);
        contentValues.put(AppConstant.PURCHASE_TOTAL_AMOUNT, totalAmt);
        contentValues.put(AppConstant.PURCHASE_DISCOUNT, discount);
        contentValues.put(AppConstant.PURCHASE_TAX_ID, taxID    );
        contentValues.put(AppConstant.PURCHASE_SUB_TOTAL, subTotal);
        contentValues.put(AppConstant.PURCHASE_DISCOUNT_AMOUNT, discountAmt);
        contentValues.put(AppConstant.PURCHASE_REMARK, remark);
        contentValues.put(AppConstant.PURCHASE_PAID_AMOUNT, paidAmt);
        contentValues.put(AppConstant.PURCHASE_BALANCE, balance);
        contentValues.put(AppConstant.PURCHASE_TAX_AMOUNT, taxAmt);
        contentValues.put(AppConstant.PURCHASE_DAY, day);
        contentValues.put(AppConstant.PURCHASE_MONTH, month);
        contentValues.put(AppConstant.PURCHASE_YEAR, year);
        contentValues.put(AppConstant.PURCHASE_TAX_RATE, taxRate);*/
        query = "insert into " + AppConstant.PURCHASE_TABLE_NAME + "(" +
                AppConstant.PURCHASE_ID             + ", " + AppConstant.PURCHASE_VOUCHER_NUM    + ", " +
                AppConstant.PURCHASE_DATE           + ", " + AppConstant.PURCHASE_SUPPLIER_ID    + ", " +
                AppConstant.PURCHASE_TOTAL_AMOUNT   + " , " + AppConstant.PURCHASE_DISCOUNT      + " , " +
                AppConstant.PURCHASE_TAX_ID         + " , " + AppConstant.PURCHASE_SUB_TOTAL     + " , " +
                AppConstant.PURCHASE_DISCOUNT_AMOUNT + " , " + AppConstant.PURCHASE_REMARK       + " , " +
                AppConstant.PURCHASE_PAID_AMOUNT     + " , " + AppConstant.PURCHASE_BALANCE      + " , " +
                AppConstant.PURCHASE_TAX_AMOUNT      + " , " + AppConstant.CREATED_DATE          + " , " +
                AppConstant.PURCHASE_DAY             + " , " + AppConstant.PURCHASE_MONTH        + " , " +
                AppConstant.PURCHASE_YEAR            + " , " + AppConstant.PURCHASE_TAX_RATE     + ", " +
                AppConstant.PURCHASE_TIME            + ", " + AppConstant.PURCHASE_TAX_TYPE      + ", " +
                AppConstant.PURCHASE_DISCOUNT_ID + "," +
                AppConstant.CURRENT_USER_ID +
                ")" +
                "values(" +
                genID    + ", ?, ?, "      + supplierID + ", " +
                totalAmt + ", ?, "         + taxID + ", " +
                subTotal + ", " + discountAmt + ", ?, " +
                paidAmt + ", " + balance + ", " +
                taxAmt    + ", ?,?,?,?, " + taxRate +
                ", strftime('%s', ?, time('now', 'localtime')), ?, " +
                discountID + ",?" +
                ")";
        Log.e("q", query);
        databaseWriteTransaction(flag);
        try {
            //id =database.insert(AppConstant.PURCHASE_TABLE_NAME, null, contentValues);
            database.execSQL(query, new String[]{voucherNo, date, discount, remark, DateUtility.getTodayDate(), day, month, year, formatDateWithDash(day, month, year), taxType , userId.toString()});
            for (SalesAndPurchaseItem purchaseDetail : purchaseDetailItemList) {
                id = purchaseDetailDAO.addNewPurchaseDetail(genID, purchaseDetail.getStockID(), purchaseDetail.getQty(), purchaseDetail.getPrice(), purchaseDetail.getDiscountPercent(),
                        purchaseDetail.getTaxID(), purchaseDetail.getTotalPrice(), purchaseDetail.getTaxAmt(), purchaseDetail.getDiscountAmount(), purchaseDetail.getTaxRate());
            }
            /*cursor = database.rawQuery("select strftime(? , ?, ?), strftime(?, ?, time(?, ? )), time(?, ?), datetime(?, ?);",
                    new String[]{"%s", "now", "localtime", "%s", "2017-01-30", "now", "localtime", "now","localtime" , "now", "localtime"});
            if (cursor.moveToFirst())
                Log.e(cursor.getString(0) + " daf" + cursor.getString(2), cursor.getString(2) + " da"+ cursor.getString(3));*/
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public List<PurchaseHistory> getAllPurchases(int startLimit, int endLimit, InsertedBooleanHolder flag,  String startDate, String endDate , Long userId ) {

        database =  databaseHelper.getReadableDatabase();
        database.execSQL(" ATTACH '" + db_users + "' AS purchaseUser_db;");

        String userTable = "purchaseUser_db."+ AppConstant.TABLE_USER_ROLES;
        String whereCondition = userId == null ? "" :  " and p." + AppConstant.CURRENT_USER_ID + " = " + userId ;
        purchaseHistoryList = new ArrayList<>();
        query = "select p." + AppConstant.PURCHASE_ID +
                ", p." + AppConstant.PURCHASE_VOUCHER_NUM +
                ", p." + AppConstant.PURCHASE_DATE +
                ", p." + AppConstant.PURCHASE_TOTAL_AMOUNT+
                ", s." + AppConstant.SUPPLIER_NAME +
                ", p." + AppConstant.PURCHASE_SUPPLIER_ID
                + ", p." + AppConstant.PURCHASE_DAY +
                ", p." + AppConstant.PURCHASE_MONTH +
                ", p." + AppConstant.PURCHASE_YEAR
                + ", p." + AppConstant.PURCHASE_DAY +
                ", p." + AppConstant.PURCHASE_MONTH +
                ",p." + AppConstant.PURCHASE_YEAR +
                " ,p." + AppConstant.PURCHASE_PAID_AMOUNT +
                " ,u." + AppConstant.COLUMN_USER_NAME +
                ", p."  + AppConstant.PURCHASE_BALANCE +
                " - ifnull((select sum(" + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +") from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID +
                " ), 0.0) as bal" + ", " + AppConstant.PURCHASE_TIME +
                " from " +  AppConstant.SUPPLIER_TABLE_NAME + " s," +
                AppConstant.PURCHASE_TABLE_NAME + " p  LEFT JOIN " + userTable + " u ON p." + AppConstant.CURRENT_USER_ID + " = u." + AppConstant.COLUMN_USER_ID +
                 "  where p." + AppConstant.PURCHASE_SUPPLIER_ID + "=s." + AppConstant.SUPPLIER_ID +
                " and p." + AppConstant.PURCHASE_DATE +
                ">=? and p." + AppConstant.PURCHASE_DATE + "<=? " +
               whereCondition +
                " order by " + AppConstant.PURCHASE_TIME + " desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    //Log.w("In pur","in pur");

                    purchaseHistory = new PurchaseHistory();
                    purchaseHistory.setId(cursor.getLong(0));
                    purchaseHistory.setVoucherNo(cursor.getString(1));
                    purchaseHistory.setDate(cursor.getString(2));
                    purchaseHistory.setTotalAmount(cursor.getDouble(3));
                    purchaseHistory.setSupplierName(cursor.getString(4));
                    purchaseHistory.setSupplierID(cursor.getLong(5));
                    purchaseHistory.setDay(cursor.getString(6));
                    purchaseHistory.setMonth(cursor.getString(7));
                    purchaseHistory.setYear(cursor.getString(8));
                    purchaseHistory.setDay(cursor.getString(9));
                    purchaseHistory.setMonth(cursor.getString(10));
                    purchaseHistory.setYear(cursor.getString(11));
                    purchaseHistory.setPaidAmt(cursor.getDouble(12));
                    purchaseHistory.setTotalBal(cursor.getDouble(13));
                    purchaseHistory.setCurrentUserName(cursor.getString(cursor.getColumnIndex(AppConstant.COLUMN_USER_NAME)));
                    //Log.e("tiem", cursor.getString(4)+"Lll");
                    purchaseHistoryList.add(purchaseHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
            database.execSQL("DETACH purchaseUser_db");

        }
        return purchaseHistoryList;
    }

    public PurchaseHistory getPurchase(Long id) {
        purchaseHistory = new PurchaseHistory();
        query = "select p." + AppConstant.PURCHASE_ID + ", p." + AppConstant.PURCHASE_VOUCHER_NUM + ", p." + AppConstant.PURCHASE_DATE + ", p." + AppConstant.PURCHASE_TOTAL_AMOUNT
                + ", s." + AppConstant.SUPPLIER_NAME + ", p." + AppConstant.PURCHASE_SUPPLIER_ID
                + ", p." + AppConstant.PURCHASE_DAY + ", p." + AppConstant.PURCHASE_MONTH + ", p." + AppConstant.PURCHASE_YEAR
                + ", p." + AppConstant.PURCHASE_DAY + ", p." + AppConstant.PURCHASE_MONTH + ",p." + AppConstant.PURCHASE_YEAR + " ,p." + AppConstant.PURCHASE_PAID_AMOUNT + ", p."
                + AppConstant.PURCHASE_BALANCE + " - ifnull((select sum(" + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID +
                " ), 0.0) as bal" + ", " + AppConstant.PURCHASE_TIME +
                " from " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.SUPPLIER_TABLE_NAME
                + " s where p." + AppConstant.PURCHASE_SUPPLIER_ID + "=s." + AppConstant.SUPPLIER_ID + " and p." + AppConstant.PURCHASE_ID + " = " + id;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                purchaseHistory.setId(cursor.getLong(0));
                purchaseHistory.setVoucherNo(cursor.getString(1));
                purchaseHistory.setDate(cursor.getString(2));
                purchaseHistory.setTotalAmount(cursor.getDouble(3));
                purchaseHistory.setSupplierName(cursor.getString(4));
                purchaseHistory.setSupplierID(cursor.getLong(5));
                purchaseHistory.setDay(cursor.getString(6));
                purchaseHistory.setMonth(cursor.getString(7));
                purchaseHistory.setYear(cursor.getString(8));
                purchaseHistory.setDay(cursor.getString(9));
                purchaseHistory.setMonth(cursor.getString(10));
                purchaseHistory.setYear(cursor.getString(11));
                purchaseHistory.setPaidAmt(cursor.getDouble(12));
                purchaseHistory.setTotalBal(cursor.getDouble(13));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            database.endTransaction();
        }
        return purchaseHistory;
    }

    public PurchaseHeader getPurchaseByPurchaseID(Long id, InsertedBooleanHolder flag) {
        query = "select p." + AppConstant.PURCHASE_ID + ", p." + AppConstant.PURCHASE_VOUCHER_NUM + ", p." + AppConstant.PURCHASE_DATE + ", p." + AppConstant.PURCHASE_SUPPLIER_ID + ", s." +
                AppConstant.SUPPLIER_NAME + ", p." + AppConstant.PURCHASE_TOTAL_AMOUNT + ", p." + AppConstant.PURCHASE_DISCOUNT + ", p." + AppConstant.PURCHASE_SUB_TOTAL + ", p." +
                AppConstant.PURCHASE_TAX_ID + ", p." + AppConstant.PURCHASE_TAX_TYPE + ", t." + AppConstant.TAX_NAME + ", p." + AppConstant.PURCHASE_DISCOUNT_AMOUNT
                + ", p." + AppConstant.PURCHASE_REMARK + ", p." + AppConstant.PURCHASE_PAID_AMOUNT + ", p." + AppConstant.PURCHASE_BALANCE + ", p." + AppConstant.PURCHASE_TAX_AMOUNT
                + ", p." + AppConstant.PURCHASE_DAY + ", p." + AppConstant.PURCHASE_MONTH + ", p." + AppConstant.PURCHASE_YEAR + ", p."
                + AppConstant.PURCHASE_TAX_RATE + ", " + AppConstant.PURCHASE_DISCOUNT_ID + "," + AppConstant.SUPPLIER_PHONE_NUM + "," + AppConstant.SUPPLIER_ADDRESS + " from " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.SUPPLIER_TABLE_NAME + " s," + AppConstant.TAX_TABLE_NAME + " t where p."
                + AppConstant.PURCHASE_SUPPLIER_ID + "=s." + AppConstant.SUPPLIER_ID +
                " and p." + AppConstant.PURCHASE_TAX_ID + "= t." + AppConstant.TAX_ID + " and p." + AppConstant.PURCHASE_ID + " = " + id;
        databaseReadTransaction(flag);

        Log.w("in db", "in db");
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                purchaseHeader = new PurchaseHeader();
                purchaseHeader.setId(cursor.getLong(0));
                purchaseHeader.setPurchaseVocherNo(cursor.getString(1));
                purchaseHeader.setSaleDate(cursor.getString(2));
                purchaseHeader.setSupplierID(cursor.getLong(3));
                purchaseHeader.setSupplierName(cursor.getString(4));
                purchaseHeader.setTotal(cursor.getDouble(5));
                purchaseHeader.setDiscount(cursor.getString(6));
                purchaseHeader.setSubtotal(cursor.getDouble(7));
                purchaseHeader.setTaxID(cursor.getLong(8));
                purchaseHeader.setTaxType(cursor.getString(9));
                purchaseHeader.setTaxName(cursor.getString(10));
                purchaseHeader.setDiscountAmt(cursor.getDouble(11));
                purchaseHeader.setRemark(cursor.getString(12));
                purchaseHeader.setPaidAmt(cursor.getDouble(13));
                purchaseHeader.setBalance(cursor.getDouble(14));
                purchaseHeader.setTaxAmt(cursor.getDouble(15));
                purchaseHeader.setDay(cursor.getString(16));
                purchaseHeader.setMonth(cursor.getString(17));
                purchaseHeader.setYear(cursor.getString(18));
                purchaseHeader.setTaxRate(cursor.getDouble(19));
                purchaseHeader.setDiscountID(cursor.getLong(20));
                purchaseHeader.setSupplierPhone(cursor.getString(cursor.getColumnIndex(AppConstant.SUPPLIER_PHONE_NUM)));
                purchaseHeader.setSupplierAddress(cursor.getString(cursor.getColumnIndex(AppConstant.SUPPLIER_ADDRESS)));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return purchaseHeader;
    }

    public boolean updatePurchaseByID(Long id, String date, Long supplierID, double totalAmt, String discount, Long taxID, double subTotal,
                                      double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year,
                                      Double taxRate, List<SalesAndPurchaseItem> updateDetailList, List<Long> deletedIDList, Long discountID) {
        if (discountID != null && discountID == 0) {
            discountID = null;
        }
        /*contentValues.put(AppConstant.PURCHASE_DATE, date);
        contentValues.put(AppConstant.PURCHASE_SUPPLIER_ID, supplierID);
        contentValues.put(AppConstant.PURCHASE_TOTAL_AMOUNT, totalAmt);
        contentValues.put(AppConstant.PURCHASE_DISCOUNT, discount);
        contentValues.put(AppConstant.PURCHASE_TAX_ID, taxID);
        contentValues.put(AppConstant.PURCHASE_SUB_TOTAL, subTotal);
        contentValues.put(AppConstant.PURCHASE_DISCOUNT_AMOUNT, discountAmt);
        contentValues.put(AppConstant.PURCHASE_REMARK, remark);
        contentValues.put(AppConstant.PURCHASE_PAID_AMOUNT, paidAmt);
        contentValues.put(AppConstant.PURCHASE_BALANCE, balance);
        contentValues.put(AppConstant.PURCHASE_TAX_AMOUNT, taxAmt);
        contentValues.put(AppConstant.PURCHASE_DAY, day);
        contentValues.put(AppConstant.PURCHASE_MONTH, month);
        contentValues.put(AppConstant.PURCHASE_YEAR, year);
        contentValues.put(AppConstant.PURCHASE_TAX_RATE, taxRate);*/
        query = "update " + AppConstant.PURCHASE_TABLE_NAME + " set " + AppConstant.PURCHASE_DATE + "=?, " + AppConstant.PURCHASE_SUPPLIER_ID + "=" + supplierID + ", " + AppConstant.PURCHASE_TOTAL_AMOUNT + "=" + totalAmt
                + " , " + AppConstant.PURCHASE_DISCOUNT + "= ?, " + AppConstant.PURCHASE_TAX_ID + "=" + taxID + " , " + AppConstant.PURCHASE_SUB_TOTAL + "=" + subTotal + " , " +
                AppConstant.PURCHASE_DISCOUNT_AMOUNT + "=" + discountAmt + " , " + AppConstant.PURCHASE_REMARK + "=?, " + AppConstant.PURCHASE_PAID_AMOUNT + "= " + paidAmt + " , " + AppConstant.PURCHASE_BALANCE
                + "=" + balance + " , " + AppConstant.PURCHASE_TAX_AMOUNT + "=" + taxAmt + " , " + AppConstant.PURCHASE_DAY + "=?, " + AppConstant.PURCHASE_MONTH + "=?, " + AppConstant.PURCHASE_YEAR + "=?, " + AppConstant.PURCHASE_TAX_RATE +
                "=" + taxRate + ", " + AppConstant.PURCHASE_DISCOUNT_ID + " = " + discountID + " where " + AppConstant.PURCHASE_ID + "=" + id;

        databaseWriteTransaction(flag);
        try {
            //database.update(AppConstant.PURCHASE_TABLE_NAME, contentValues, AppConstant.PURCHASE_ID + " = ?", new String[]{id.toString()});
            database.execSQL(query, new String[]{date, discount, remark, day, month, year});
            for (SalesAndPurchaseItem purDetailView : updateDetailList) {
                if (purDetailView.getId() == null) {
                    purchaseDetailDAO.addNewPurchaseDetail(id, purDetailView.getStockID(), purDetailView.getQty(), purDetailView.getPrice(), purDetailView.getDiscountPercent(), purDetailView.getTaxID(),
                            purDetailView.getTotalPrice(), purDetailView.getTaxAmt(), purDetailView.getTaxAmt(), purDetailView.getTaxRate());
                } else {
                    purchaseDetailDAO.updatePurchaseDetailByID(purDetailView.getId(), id, purDetailView.getStockID(), purDetailView.getQty(), purDetailView.getPrice(), purDetailView.getDiscountPercent(), purDetailView.getTaxID(),
                            purDetailView.getTotalPrice(), purDetailView.getTaxAmt(), purDetailView.getTaxAmt(), purDetailView.getTaxRate());
                }
            }
            for (Long deletedID : deletedIDList) {
                purchaseDetailDAO.deletePurchaseDetailByID(deletedID);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    /*public boolean deletePurchase(Long id) {
        databaseWriteTransaction(flag);
        try {
            idList = purchaseDetailDAO.getPurchaseDetailIdList(id);
            for (Long detailID : idList) {
                purchaseDetailDAO.deletePurchaseDetailByID(detailID);
            }
            database.delete(AppConstant.PURCHASE_TABLE_NAME, AppConstant.PURCHASE_ID + " = ?", new String[]{id.toString()});
            database.setTransactionSuccessful();
        }catch (SQLiteException e){
            e.printStackTrace();
        }finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }*/

    public List<PurchaseHistory> getPurchaseWithVoucherNoOrSupplier(int startLimit, int endLimit, String searchStr) {
        query = "select s." + AppConstant.PURCHASE_ID + ", s." + AppConstant.PURCHASE_VOUCHER_NUM + ", s." + AppConstant.PURCHASE_DATE + ", s." + AppConstant.PURCHASE_TOTAL_AMOUNT
                + ", c." + AppConstant.SUPPLIER_NAME + ", s." + AppConstant.PURCHASE_SUPPLIER_ID
                + ", s." + AppConstant.PURCHASE_DAY + ", s." + AppConstant.PURCHASE_MONTH + ", s." + AppConstant.PURCHASE_YEAR
                + ", s." + AppConstant.PURCHASE_DAY + ", s." + AppConstant.PURCHASE_MONTH + ",s." + AppConstant.PURCHASE_YEAR + ", s."
                + AppConstant.PURCHASE_BALANCE + " - ifnull((select sum(" + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID +
                " ), 0.0) as bal"
                + " from " + AppConstant.PURCHASE_TABLE_NAME + " s, " + AppConstant.SUPPLIER_TABLE_NAME
                + " c where s." + AppConstant.PURCHASE_SUPPLIER_ID + "=c." + AppConstant.SUPPLIER_ID +
                " and (c." + AppConstant.SUPPLIER_NAME + " like ? || '%' or s." + AppConstant.PURCHASE_VOUCHER_NUM + " like ? || '%') limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        purchaseHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    purchaseHistory = new PurchaseHistory();
                    purchaseHistory.setId(cursor.getLong(0));
                    purchaseHistory.setVoucherNo(cursor.getString(1));
                    purchaseHistory.setDate(cursor.getString(2));
                    purchaseHistory.setTotalAmount(cursor.getDouble(3));
                    purchaseHistory.setSupplierName(cursor.getString(4));
                    purchaseHistory.setSupplierID(cursor.getLong(5));
                    purchaseHistory.setDay(cursor.getString(6));
                    purchaseHistory.setMonth(cursor.getString(7));
                    purchaseHistory.setYear(cursor.getString(8));
                    purchaseHistory.setDay(cursor.getString(9));
                    purchaseHistory.setMonth(cursor.getString(10));
                    purchaseHistory.setYear(cursor.getString(11));
                    purchaseHistory.setTotalBal(cursor.getDouble(12));
                    purchaseHistoryList.add(purchaseHistory);
                } while (cursor.moveToNext());
            }

            Log.w("hele", purchaseHistoryList.size() + " SSS ");
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return purchaseHistoryList;
    }

    public List<PurchaseHistory> getPurchaseByVoucherCustomerNameOnSearch(String searchStr) {
        query = "select s." + AppConstant.PURCHASE_ID + ", s." + AppConstant.PURCHASE_VOUCHER_NUM + ", s." + ", c." + AppConstant.SUPPLIER_NAME + " from " + AppConstant.PURCHASE_TABLE_NAME + " s, " +
                AppConstant.SUPPLIER_TABLE_NAME + " c where s." + AppConstant.PURCHASE_SUPPLIER_ID + "=c." + AppConstant.SUPPLIER_ID +
                " not like '%Cancel' and (c." + AppConstant.CATEGORY_NAME + " like ? || '%' or s." + AppConstant.PURCHASE_VOUCHER_NUM + " like ? || '%'))";
        databaseReadTransaction(flag);
        purchaseHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    purchaseHistory = new PurchaseHistory();
                    purchaseHistory.setId(cursor.getLong(0));
                    purchaseHistory.setVoucherNo(cursor.getString(1));
                    //purchaseHistory.setDate(cursor.getString(2));
                    //purchaseHistory.setTotalAmount(cursor.getDouble(3));
                    purchaseHistory.setSupplierName(cursor.getString(2));
                    //purchaseHistory.setSupplierID(cursor.getLong(5));
                    /*purchaseHistory.setDay(cursor.getString(6));
                    purchaseHistory.setMonth(cursor.getString(7));
                    purchaseHistory.setYear(cursor.getString(8));
                    purchaseHistory.setDay(cursor.getString(9));
                    purchaseHistory.setMonth(cursor.getString(10));
                    purchaseHistory.setYear(cursor.getString(11));*/
                    purchaseHistoryList.add(purchaseHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return purchaseHistoryList;
    }

    public Long findIDByPurchaseInvoice(String invoiceNo) {
        query = "select " + AppConstant.PURCHASE_ID + " from " + AppConstant.PURCHASE_TABLE_NAME + " where " + AppConstant.PURCHASE_VOUCHER_NUM + " = ?";
        id = 0l;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{invoiceNo});
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }

    public List<PurchaseHistory> getPurchaseByVoucherNoOnSearch(String voucherNo) {
        query = "select s." + AppConstant.PURCHASE_ID + ", s." + AppConstant.PURCHASE_VOUCHER_NUM + ", s." + ", c." + AppConstant.CUSTOMER_NAME + " from " + AppConstant.SALES_TABLE_NAME + " s, " +
                AppConstant.CUSTOMER_TABLE_NAME + " c where s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID +
                " not like '%Cancel' and s." + AppConstant.PURCHASE_VOUCHER_NUM + " like ? || '%')";
        databaseReadTransaction(flag);
        purchaseHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{voucherNo});
            if (cursor.moveToFirst()) {
                do {
                    purchaseHistory = new PurchaseHistory();
                    purchaseHistory.setId(cursor.getLong(0));
                    purchaseHistory.setVoucherNo(cursor.getString(1));
                    //purchaseHistory.setDate(cursor.getString(2));
                    //purchaseHistory.setTotalAmount(cursor.getDouble(3));
                    purchaseHistory.setSupplierName(cursor.getString(2));
                    /*purchaseHistory.setSupplierID(cursor.getLong(5));
                    purchaseHistory.setDay(cursor.getString(6));
                    purchaseHistory.setMonth(cursor.getString(7));
                    purchaseHistory.setYear(cursor.getString(8));
                    purchaseHistory.setDay(cursor.getString(9));
                    purchaseHistory.setMonth(cursor.getString(10));
                    purchaseHistory.setYear(cursor.getString(11));*/
                    purchaseHistoryList.add(purchaseHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return purchaseHistoryList;
    }

    public List<PurchaseHistory> getPurchaseByCustomerNameOnSearch(String customerName) {
        query = "select s." + AppConstant.PURCHASE_ID + ", s." + AppConstant.PURCHASE_VOUCHER_NUM + ", s." + ", c." + AppConstant.CUSTOMER_NAME + " from " + AppConstant.SALES_TABLE_NAME + " s, " +
                AppConstant.CUSTOMER_TABLE_NAME + " c where s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID +
                " not like '%Cancel' and c." + AppConstant.CUSTOMER_NAME + " like ? || '%')";
        databaseReadTransaction(flag);
        purchaseHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{customerName});
            if (cursor.moveToFirst()) {
                do {
                    purchaseHistory = new PurchaseHistory();
                    purchaseHistory.setId(cursor.getLong(0));
                    purchaseHistory.setVoucherNo(cursor.getString(1));
                    //purchaseHistory.setDate(cursor.getString(2));
                    //  purchaseHistory.setTotalAmount(cursor.getDouble(3));
                    purchaseHistory.setSupplierName(cursor.getString(2));
                    /*purchaseHistory.setSupplierID(cursor.getLong(5));
                    purchaseHistory.setDay(cursor.getString(6));
                    purchaseHistory.setMonth(cursor.getString(7));
                    purchaseHistory.setYear(cursor.getString(8));
                    purchaseHistory.setDay(cursor.getString(9));
                    purchaseHistory.setMonth(cursor.getString(10));
                    purchaseHistory.setYear(cursor.getString(11));*/
                    purchaseHistoryList.add(purchaseHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return purchaseHistoryList;
    }

    public boolean deletePurchase(Long id) {
        databaseReadTransaction(flag);
        try {
            query = "delete from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = " + id;
            database.execSQL(query);
            query = "delete from " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = " + id;
            database.execSQL(query);
            query = "delete from " + AppConstant.PURCHASE_TABLE_NAME + " where " + AppConstant.PURCHASE_ID + " = " + id;
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

}
