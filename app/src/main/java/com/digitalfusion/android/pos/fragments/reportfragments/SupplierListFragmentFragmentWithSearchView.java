package com.digitalfusion.android.pos.fragments.reportfragments;


import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.CustomerSupplier;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForCustomerSupplier;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.SupplierSearchAdapter;
import com.digitalfusion.android.pos.database.business.SupplierManager;
import com.digitalfusion.android.pos.database.model.Supplier;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.example.searchview.MaterialSearchView;

import java.io.Serializable;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SupplierListFragmentFragmentWithSearchView extends Fragment implements Serializable {


    private View mainLayout;

    private RecyclerView supplierListRecyclerView;

    private SupplierManager supplierManager;

    private Context context;

    private List<Supplier> supplierList;
    private List<CustomerSupplier> customerSupplierList;

    private ParentRVAdapterForReports rvAdapterForReport;

    private MaterialSearchView searchView;

    private boolean isSearch = false;

    private boolean shouldLoad = true;

    private String searchText = "";

    private SupplierSearchAdapter supplierSearchAdapter;

    private TextView noTransactionTextView;

    private TextView searchedResultTxt;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.fragment_supplier_list_fragment_fragment_with_search_view, null);

        //
        //
        //
        //
        //
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Supplier List");
        context = getContext();
        String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.supplier_payment_history}).getString(0);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        setHasOptionsMenu(true);

        supplierManager = new SupplierManager(context);

        loadUI();
        MainActivity.setCurrentFragment(this);
        configRecyclerView();

        ((RVAdapterForCustomerSupplier) rvAdapterForReport).setClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                handleClickListener(postion);

            }
        });

        supplierListRecyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                if (shouldLoad) {

                    ((RVAdapterForCustomerSupplier) rvAdapterForReport).setShowLoader(true);

                    loadmore();

                }

            }
        });

        supplierSearchAdapter = new SupplierSearchAdapter(context, supplierManager);

        searchView.setAdapter(supplierSearchAdapter);

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                handleClickListener(position);
                //                shouldLoad = false;
                //
                //                supplierList = new ArrayList<Supplier>();
                //
                //                supplierList.add(supplierSearchAdapter.getSuggestionList().get(position));
                //
                //                Log.e("size", supplierList.size() + " " + customerSupplierList.size());
                //
                //                customerSupplierList = new CustomerSupplier().wrapSupplier(supplierList);
                //                Log.e("cs six", customerSupplierList.size() + " kfjdakfj");
                //
                //                refreshRecyclerView();
                //
                //                searchView.closeSearch();
                //
                //                searchedResultTxt.setVisibility(View.VISIBLE);
            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                shouldLoad = true;

                isSearch = true;

                searchText = query;

                supplierSearch(0, 10, query);

                refreshRecyclerView();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                shouldLoad = true;

                isSearch = false;

                return false;

            }
        });

        refreshRecyclerView();

        return mainLayout;
    }

    private void handleClickListener(int postion) {
        Bundle                                   bundle                                   = new Bundle();
        SimpleReportFragmentWithDateFilterSearch simpleReportFragmentWithDateFilterSearch = new SimpleReportFragmentWithDateFilterSearch();
        bundle.putString("reportType", "supplier payment history");
        bundle.putLong("ID", customerSupplierList.get(postion).getId());
        simpleReportFragmentWithDateFilterSearch.setArguments(bundle);
        //  MainActivity.replacingFragment(simpleReportFragmentWithDateFilterSearch);

        bundle.putSerializable("frag", simpleReportFragmentWithDateFilterSearch);

        Intent detailIntent = new Intent(context, DetailActivity.class);

        detailIntent.putExtras(bundle);

        startActivity(detailIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
        //        startLoad();
    }

    private void refreshRecyclerView() {

        if (customerSupplierList.size() > 0) {

            supplierListRecyclerView.setVisibility(View.VISIBLE);

            noTransactionTextView.setVisibility(View.GONE);

            ((RVAdapterForCustomerSupplier) rvAdapterForReport).setCustomerSupplierList(customerSupplierList);

            rvAdapterForReport.notifyDataSetChanged();


        } else {

            supplierListRecyclerView.setVisibility(View.GONE);

            noTransactionTextView.setVisibility(View.VISIBLE);
        }

    }

    public void configRecyclerView() {

        customerSupplierList = supplierManager.getSuppliers(0, 10);
        rvAdapterForReport = new RVAdapterForCustomerSupplier(customerSupplierList);

        supplierListRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        supplierListRecyclerView.setAdapter(rvAdapterForReport);


    }

    public void startLoad() {
        customerSupplierList = supplierManager.getSuppliers(0, 10);

        ((RVAdapterForCustomerSupplier) rvAdapterForReport).setCustomerSupplierList(customerSupplierList);
        rvAdapterForReport.notifyDataSetChanged();
    }

    public void loadmore() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isSearch) {
                    customerSupplierList.addAll(loadMore(supplierList.size(), 9, searchText));
                } else {
                    customerSupplierList.addAll(loadMore(customerSupplierList.size(), 9));
                }

                customerSupplierList.addAll(loadMore(customerSupplierList.size(), 9));

                ((RVAdapterForCustomerSupplier) rvAdapterForReport).setShowLoader(false);

                refreshRecyclerView();
            }
        }, 500);
    }

    public List<CustomerSupplier> loadMore(int startLimit, int endLimit) {

        return supplierManager.getSuppliers(startLimit, endLimit);


    }

    public List<CustomerSupplier> loadMore(int startLimit, int endLimit, String query) {

        return new CustomerSupplier().wrapSupplier(supplierManager.getAllSuppliersByNameOnSearch(startLimit, endLimit, query));


    }

    public void supplierSearch(int startLimit, int endLimit, String query) {

        supplierList = supplierManager.getAllSuppliersByNameOnSearch(startLimit, endLimit, query);


    }

    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        searchView.setCursorDrawable(attributeResourceId);

    }

    public void loadUI() {

        loadUIFromToolbar();

        noTransactionTextView = (TextView) mainLayout.findViewById(R.id.no_transaction);

        searchedResultTxt = (TextView) mainLayout.findViewById(R.id.searched_result_txt);

        supplierListRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.supplier_list_rv);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.main, menu);

        MenuItem item = menu.findItem(R.id.action_search);

        searchView.setMenuItem(item);

        super.onCreateOptionsMenu(menu, inflater);
    }
}
