package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Tax;

import java.util.List;

/**
 * Created by MD003 on 9/8/16.
 */
public class RVAdapterForTaxMD extends RecyclerView.Adapter<RVAdapterForTaxMD.TaxItemViewHolder> {


    List<Tax> taxList;

    private OnItemClickListener mItemClickListener;

    private RadioButton oldRb;

    private Long id;


    public RVAdapterForTaxMD(List<Tax> taxList, Long id) {
        this.taxList = taxList;
        this.id = id;
    }

    @Override
    public TaxItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tax_item_view_in_tax_md, parent, false);

        return new TaxItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TaxItemViewHolder holder, final int position) {

        holder.taxName.setText(taxList.get(position).getName());
        holder.taxRate.setText(taxList.get(position).getRate().toString() + "%");
        if (taxList.get(position).getId() == id) {
            oldRb = holder.taxRb;
            holder.taxRb.setChecked(true);
        } else {
            oldRb = holder.taxRb;
            holder.taxRb.setChecked(false);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {

                    if (oldRb != null) {
                        oldRb.setChecked(false);
                        oldRb = holder.taxRb;
                        holder.taxRb.setChecked(true);
                    }

                    mItemClickListener.onItemClick(v, position);
                }
            }

        });
    }

    @Override
    public int getItemCount() {
        return taxList.size();
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public List<Tax> getTaxVOList() {
        return taxList;
    }

    public void setTaxVOList(List<Tax> stockViewList) {
        this.taxList = stockViewList;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class TaxItemViewHolder extends RecyclerView.ViewHolder {
        TextView taxName;
        TextView taxRate;
        RadioButton taxRb;
        View itemView;

        public TaxItemViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            taxRb = (RadioButton) itemView.findViewById(R.id.rb);
            taxName = (TextView) itemView.findViewById(R.id.tax_name);
            taxRate = (TextView) itemView.findViewById(R.id.tax_rate);

        }
    }
}
