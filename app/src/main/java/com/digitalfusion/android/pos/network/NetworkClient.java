package com.digitalfusion.android.pos.network;

import android.text.TextUtils;

import com.digitalfusion.android.pos.util.AppConstant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by MD003 on 8/14/17.
 */

public class NetworkClient {

    //private static Retrofit retrofit=null;
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Gson gson = new GsonBuilder()
            .setDateFormat("dd/MM/yyyy")
            .create();

    static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .build();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(AppConstant.url)
                    .addConverterFactory(GsonConverterFactory.create(gson));
    private static Retrofit retrofit = builder.build();


    public static <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, null, null);
    }

    public static <S> S createService(Class<S> serviceClass, String username, String password) {
        if (!TextUtils.isEmpty(username)
                && !TextUtils.isEmpty(password)) {
            String authToken = Credentials.basic(AppConstant.user_name, AppConstant.password);
            return createService(serviceClass, authToken);
        }

        return createService(serviceClass, null, null);
    }


    public static <S> S createService(Class<S> serviceClass, final String authToken) {
        if (!TextUtils.isEmpty(authToken)) {

            builder.client(getUnsafeOkHttpClient(authToken));
            retrofit = builder.build();
        }

        return retrofit.create(serviceClass);
    }

    public static Retrofit getClient() {


        //        if(retrofit==null){
        //            Gson gson = new GsonBuilder()
        //                    .setDateFormat("yyyy-MM-dd")
        //                    .create();
        //
        //            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        //
        //            Retrofit.Builder builder =
        //                    new Retrofit.Builder()
        //                            .baseUrl(BASE_URL)
        //                            .addConverterFactory(GsonConverterFactory.create(gson));
        //
        //            retrofit = builder.build();
        //            retrofit = new Retrofit.Builder()
        //                    .baseUrl(BASE_URL)
        //                    .addConverterFactory(GsonConverterFactory.create(gson))
        //                    .build();
        //            Log.w("build","new build");
        //        }
        return retrofit;
    }

    public static OkHttpClient getUnsafeOkHttpClient(String authToken) {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier((hostname, session) -> true);

            AuthenticationInterceptor interceptor =
                    new AuthenticationInterceptor(authToken);
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            if (!builder.interceptors().contains(interceptor)) {
                builder.addInterceptor(interceptor);
            }
            if (!builder.interceptors().contains(logging)) {
                builder.addInterceptor(logging);
            }

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

}
