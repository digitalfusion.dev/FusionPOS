package com.digitalfusion.android.pos.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForPhone;
import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 9/22/16.
 */
public class CustomerDetailView extends Fragment {

    private View mainLayout;

    private Customer customer;

    private TextView customerNameTextView;

    private TextView phoneNoTextView;

    private TextView balanceTextView;

    private TextView addressTextView;

    private ImageButton callBtn;

    private RVAdapterForPhone rvAdapterForPhone;

    private RecyclerView phoneRecyclerView;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.customer_item_detail_view, null);

        context = getContext();

        customer = (Customer) getArguments().getSerializable("customer");

        String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.detail}).getString(0);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);

        rvAdapterForPhone = new RVAdapterForPhone();

        loadUI();

        phoneRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        phoneRecyclerView.setAdapter(rvAdapterForPhone);

        loadOldData();
        MainActivity.setCurrentFragment(this);
       /* callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                String temp = "tel:" + customer.getPhoneNo();
                intent.setData(Uri.parse(temp));

                startActivity(intent);
            }
        });*/

        rvAdapterForPhone.setClickListener(new RVAdapterForPhone.ClickListener() {
            @Override
            public void call(String phoneNo) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                String temp   = "tel:" + phoneNo;
                intent.setData(Uri.parse(temp));

                startActivity(intent);
            }
        });

        return mainLayout;
    }

    private void loadOldData() {

        customerNameTextView.setText(customer.getName());

        if (customer.getPhoneNo() == null || customer.getPhoneNo().equalsIgnoreCase("")) {
            phoneNoTextView.setText("-");
            callBtn.setVisibility(View.INVISIBLE);
        } else {

            List<String> phoneList = new ArrayList<>();

            String[] s = customer.getPhoneNo().split(",");

            for (String s1 : s) {
                Log.w("sss", s1 + " phone");
                phoneList.add(s1);
            }

            // phoneNoTextView.setText(customer.getPhoneNo());
            //callBtn.setVisibility(View.VISIBLE);

            rvAdapterForPhone.replaceList(phoneList);
        }

        if (customer.getAddress() == null || customer.getAddress().equalsIgnoreCase("")) {
            addressTextView.setText("-");
        } else {
            addressTextView.setText(customer.getAddress());
        }

        balanceTextView.setText(POSUtil.NumberFormat(customer.getBalance()));

    }


    public void loadUI() {

        addressTextView = (TextView) mainLayout.findViewById(R.id.street_address);

        customerNameTextView = (TextView) mainLayout.findViewById(R.id.customer_name);

        phoneNoTextView = (TextView) mainLayout.findViewById(R.id.phone_no);

        balanceTextView = (TextView) mainLayout.findViewById(R.id.balance);

        callBtn = (ImageButton) mainLayout.findViewById(R.id.call_action);

        phoneRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.phone_rv);

    }


}
