package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForMenuMD;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.model.Menu;
import com.digitalfusion.android.pos.database.model.DocumentNumberSetting;
import com.digitalfusion.android.pos.views.FusionToast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 8/24/16.
 */
public class DocumentSettingFragment extends Fragment {

    DocumentNumberSetting documentNumberSetting;
    private View mainLayoutView;
    private List<Menu> menuList;
    private SettingManager settingManager;
    private RVAdapterForMenuMD rvAdapterForMenuMD;
    private MaterialDialog menuChooserMD;
    //UI ID
    private TextView prefixPreviewTextView;

    private TextView numberPreviewTextView;

    private TextView suffixPreviewTextView;

    private TextView menuTextView;

    private EditText prefixEditText;

    private EditText suffixEditText;


    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.document_number_setting, null);

        context = getContext();

        menuList = new ArrayList<>();

        settingManager = new SettingManager(context);

        String documentSetting = context.getTheme().obtainStyledAttributes(new int[]{R.attr.document_setting}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(documentSetting);

        documentNumberSetting = new DocumentNumberSetting();

        menuList = settingManager.getAllMenus();

        buildMenuChooserDialog(menuList);

        setHasOptionsMenu(true);

        loadUI();

        assignOldValue();

        MainActivity.setCurrentFragment(this);
        menuTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuChooserMD.show();
            }
        });


        rvAdapterForMenuMD.setmItemClickListener(new RVAdapterForMenuMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                menuTextView.setText(menuList.get(position).getName());

                documentNumberSetting = settingManager.getDocSettingByMenuID(menuList.get(position).getName());

                if (documentNumberSetting != null) {

                    prefixPreviewTextView.setText(documentNumberSetting.getPrefix());

                    numberPreviewTextView.setText("0001");

                    suffixPreviewTextView.setText(documentNumberSetting.getSuffix());

                    prefixEditText.setText(documentNumberSetting.getPrefix());

                    suffixEditText.setText(documentNumberSetting.getSuffix());

                    clearError();
                }
                menuChooserMD.dismiss();
            }
        });

        prefixEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                prefixPreviewTextView.setText(prefixEditText.getText().toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        suffixEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                suffixPreviewTextView.setText(suffixEditText.getText().toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return mainLayoutView;
    }


    private void loadUI() {
        prefixPreviewTextView = (TextView) mainLayoutView.findViewById(R.id.prefix_preview_tv);
        numberPreviewTextView = (TextView) mainLayoutView.findViewById(R.id.number_preview_tv);
        suffixPreviewTextView = (TextView) mainLayoutView.findViewById(R.id.suffix_preview_tv);
        menuTextView = (TextView) mainLayoutView.findViewById(R.id.menu_tv);
        prefixEditText = (EditText) mainLayoutView.findViewById(R.id.prefix_in_document_number_setting_TIET);
        suffixEditText = (EditText) mainLayoutView.findViewById(R.id.suffix_in_document_number_setting_TIET);
    }

    private void assignOldValue() {

        DocumentNumberSetting documentNumberSetting = settingManager.getDocSettingByMenuID(menuList.get(0).getName());

        prefixPreviewTextView.setText(documentNumberSetting.getPrefix());
        prefixEditText.setText(documentNumberSetting.getPrefix());
        numberPreviewTextView.setText("0001");
        suffixPreviewTextView.setText(documentNumberSetting.getSuffix());
        suffixEditText.setText(documentNumberSetting.getSuffix());
        menuTextView.setText(menuList.get(0).getName());

        clearError();
    }

    private void clearError() {
        prefixEditText.setError(null);
        suffixEditText.setError(null);
    }

    private void buildMenuChooserDialog(List<Menu> menus) {
        rvAdapterForMenuMD = new RVAdapterForMenuMD(menus);
        menuChooserMD = new MaterialDialog.Builder(context).adapter(rvAdapterForMenuMD, null).build();

    }

    private boolean checkValidation() {
        if (!menuTextView.getText().toString().trim().isEmpty()) {
            if (!prefixEditText.getText().toString().trim().isEmpty() || !suffixEditText.getText().toString().trim().isEmpty())
                return true;
            else {
                String prefix = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_prefix_or_suffix}).getString(0);
                String suffix = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_prefix_or_suffix}).getString(0);
                prefixEditText.setError(prefix);
                suffixEditText.setError(suffix);
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save) {

            saveOrUpdateDocumentSetting();

        }

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(final android.view.Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);

       /* TypedArray save=context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        FontTextView fontTextView= (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.menu_text,null);

        menu.getItem(0).setActionView(fontTextView);

        fontTextView.setText(save.getString(0));


        fontTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menu.getItem(0));
            }
        });
        */


        super.onCreateOptionsMenu(menu, inflater);
    }

    private void saveOrUpdateDocumentSetting() {
        if (checkValidation()) {

            String prefix;
            if (prefixEditText.getText().toString().trim().isEmpty())
                prefix = "";
            else prefix = prefixEditText.getText().toString().trim();

            String suffix;
            if (suffixEditText.getText().toString().trim().isEmpty())
                suffix = "";
            else suffix = suffixEditText.getText().toString().trim();

            String menu = menuTextView.getText().toString().trim();

            if (settingManager.updateDocumentNumberSetting(prefix, 4, menu, suffix)) {
                String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.successfully_updated}).getString(0);
                FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
            }
        }
    }
}

