package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.StockImage;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD003 on 12/3/16.
 */

public class StockImagePagerAdapter extends PagerAdapter {

    Context mContext;

    LayoutInflater mLayoutInflater;

    private List<StockImage> stockImageList;

    public StockImagePagerAdapter(Context context, @NonNull List<StockImage> imageResources) {

        mContext = context;

        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.stockImageList = imageResources;
    }

    @Override
    public int getCount() {
        return stockImageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.theme_image_view, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        imageView.setImageBitmap(POSUtil.getBitmapFromByteArray(stockImageList.get(position).getImage()));

        // imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setAdjustViewBounds(true);

        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        Point size = new Point();
        //    display.getSize(size);
        //  int width = size.x;
        //  int height = width;
        imageView.setLayoutParams(new ViewGroup.LayoutParams(container.getWidth(), container.getWidth()));
        //   RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(1080, 1920);

        //  imageView.setLayoutParams(layoutParams);

        container.addView(itemView);


        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
