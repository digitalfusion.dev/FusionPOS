package com.digitalfusion.android.pos.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.SupplierSearchAdapter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwiperAdapterForSupplierList;
import com.digitalfusion.android.pos.database.business.SupplierManager;
import com.digitalfusion.android.pos.database.model.Supplier;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.views.FusionToast;
import com.example.searchview.MaterialSearchView;

import java.util.List;

/**
 * Created by MD001 on 9/5/16.
 */
public class SupplierListFragment extends Fragment {

    private View mainLayout;

    private FloatingActionButton addNewSupplierFab;

    private RecyclerView supplierListRecyclerView;

    private SupplierManager supplierManager;    ///////////

    private Context context;

    private List<Supplier> supplierList;    //////////

    private RVSwiperAdapterForSupplierList rvSwiperAdapterForSupplierList;

    private MaterialDialog addNewSupplierMaterialDialog;

    //UI component for addNewSupplierDialog

    private EditText supplierNameTextInputEditTextMd;

    private EditText supplierBusinessNameTextInputEditTextMd;

    private EditText supplierPhoneTextInputEditTextMd;

    private EditText supplierAddressTextInputEditTextMd;

    private MDButton saveBtnMd;

    private MaterialDialog editSupplierMaterialDialog;

    //UI component for EditCustomerDialog

    private EditText editSupplierNameTextInputEditTextMd;

    private EditText editSupplierBusinessNameTextInputEditTextMd;

    private EditText editSupplierPhoneTextInputEditTextMd;

    private EditText editSupplierAddressTextInputEditTextMd;

    private MDButton editSaveBtnMd;

    //Values

    private String supplierName;

    private String supplierBusinessName;

    private String supplierPhone;

    private String supplierAddress;

    private int editPosition;

    private MaterialSearchView searchView;

    private boolean isEdit;

    private boolean isSearch = false;

    private boolean shouldLoad = true;

    private String searchText = "";

    private SupplierSearchAdapter supplierSearchAdapter;

    private TextView noTransactionTextView;

    private TextView searchedResultTxt;

    private MaterialDialog deleteAlertDialog;

    private Button yesSaleDeleteMdButton;

    private int deletepos;

    private Supplier supplier;

    private boolean isAdd;
    private MaterialDialog deniedDeleteAlertDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.supplier, null);

        context = getContext();

        //  ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Supplier List");
        MainActivity.setCurrentFragment(this);
        supplierManager = new SupplierManager(context);    //////////

        buildEditSupplierDialog();

        buildAddNewSupplierDialog();

        buildDeleteAlertDialog();

        loadUI();

        configRecyclerView();

        addNewSupplierFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             /*   clearDialog();

                addNewSupplierMaterialDialog.setTitle("New Supplier");

                addNewSupplierMaterialDialog.show();*/

                isAdd = true;
                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_SUPPLIER);

                startActivity(addCurrencyIntent);

            }
        });

        rvSwiperAdapterForSupplierList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

            /*    isEdit=true;

                editPosition=postion;

                setDataEditDialog();

                addNewSupplierMaterialDialog.setTitle("Edit Supplier");

                addNewSupplierMaterialDialog.show();*/

                supplier = supplierList.get(postion);

                Bundle bundle = new Bundle();

                bundle.putSerializable(AddEditSupplierFragment.KEY, supplierList.get(postion));

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtras(bundle);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_SUPPLIER);

                startActivity(addCurrencyIntent);

            }
        });

        addNewSupplierMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                isEdit = false;
            }
        });

        rvSwiperAdapterForSupplierList.setCallClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                String[] s = supplierList.get(postion).getPhoneNo().split(",");

                Intent intent = new Intent(Intent.ACTION_DIAL);
                String tel    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.tel}).getString(0);
                String temp   = "tel:" + s[0];
                intent.setData(Uri.parse(temp));

                startActivity(intent);
            }
        });

        rvSwiperAdapterForSupplierList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                deletepos = postion;

                deleteAlertDialog.show();
            }
        });

        rvSwiperAdapterForSupplierList.setDetailClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle = new Bundle();

                bundle.putSerializable("supplier", supplierList.get(postion));

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.SUPPLIER_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);


            }

        });

        saveBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEdit) {
                    if (checkValidation()) {

                        getValuesFromView();

                        boolean status = supplierManager.updateSupplierByID(supplierName, supplierBusinessName, supplierAddress, supplierPhone, supplierList.get(editPosition).getId());  ////////

                        if (status) {

                            String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.supplier_updated_successfully}).getString(0);
                            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);


                            addNewSupplierMaterialDialog.dismiss();

                            refreshSupplierList();

                        }

                    }
                } else {
                    if (checkValidation()) {

                        getValuesFromView();

                        InsertedBooleanHolder status = new InsertedBooleanHolder();

                        supplierManager.addNewSupplier(supplierName, supplierBusinessName, supplierAddress, supplierPhone, status);  ////////

                        if (status.isInserted()) {
                            String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_supplier_added_successfully}).getString(0);

                            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);


                            addNewSupplierMaterialDialog.dismiss();

                            refreshSupplierList();

                        }

                    }
                }


            }
        });

        supplierListRecyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                if (shouldLoad) {

                    rvSwiperAdapterForSupplierList.setShowLoader(true);

                    loadmore();

                }

            }
        });

        supplierSearchAdapter = new SupplierSearchAdapter(context, supplierManager);

        searchView.setAdapter(supplierSearchAdapter);

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // shouldLoad = false;

                // supplierList = new ArrayList<>();

                // supplierList.add(supplierSearchAdapter.getSuggestionList().get(position));

                // refreshRecyclerView();

                searchView.closeSearch();

                Bundle bundle = new Bundle();

                bundle.putSerializable("supplier", supplierSearchAdapter.getSuggestion().get(position));

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.SUPPLIER_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);
                // searchedResultTxt.setVisibility(View.VISIBLE);

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                shouldLoad = true;

                isSearch = true;

                searchText = query;

                supplierSearch(0, 10, query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                isSearch = false;

                shouldLoad = true;

                return false;
            }
        });

        refreshRecyclerView();

        supplierListRecyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {

                Log.w("V X", velocityX + " S");

                Log.w("V Y", velocityY + " S");

                if (velocityY > 100) {
                    addNewSupplierFab.hide();
                } else if (velocityY < -100) {
                    addNewSupplierFab.show();
                }


                return false;
            }
        });

        yesSaleDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // supplierManager.(salesHistoryViewList.get(deletepos).getId());


                //                Log.w("hello delete",supplierList.get(deletepos).getName()+"DDD");

                //rvSwipeAdapterForSaleTransactions.setSalesHistoryList(salesHistoryViewList);

                if (supplierManager.checkSupplierBalance(supplierList.get(deletepos).getId())) {
                    supplierManager.deleteSupplier(supplierList.get(deletepos).getId());
                    if (supplierList.size() == 1) {
                        supplierList.remove(deletepos);
                        rvSwiperAdapterForSupplierList.setSupplierList(supplierList);
                        rvSwiperAdapterForSupplierList.notifyDataSetChanged();
                    } else {

                        supplierList.remove(deletepos);
                        rvSwiperAdapterForSupplierList.notifyItemRemoved(deletepos);
                        rvSwiperAdapterForSupplierList.notifyItemRangeChanged(deletepos, supplierList.size());

                    }

                    deleteAlertDialog.dismiss();
                } else {
                    deleteAlertDialog.dismiss();
                    deniedDeleteAlertDialog.show();
                }

            }
        });

        return mainLayout;

    }

    @Override
    public void onResume() {
        super.onResume();

        Log.e("resume", "kjfkd");

        if (supplier != null) {
            Log.e("jakfj", "kdjfkadjf");

            supplier = supplierManager.getSupplierById(supplier.getId());
            rvSwiperAdapterForSupplierList.updateItem(supplier);
            supplier = null;
        }
        if (isAdd) {
            Log.e("kdfjakd", "fjkajdfka");
            isAdd = false;
            startLoad();
        }

    }

    public void supplierSearch(int startLimit, int endLimit, String query) {

        supplierList = supplierManager.getAllActiveSuppliersByNameOnSearch(startLimit, endLimit, query);


    }

    private void buildDeleteAlertDialog() {

        //TypedArray no = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        //TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        String   sureToDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete}).getString(0);
        TextView textView     = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(sureToDelete);


        yesSaleDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });


        deniedDeleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.alert_dialog_with_only_ok_button, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        String   titleText     = context.getTheme().obtainStyledAttributes(new int[]{R.attr.denied_supplier_delete}).getString(0);
        TextView titleTextView = (TextView) deniedDeleteAlertDialog.findViewById(R.id.title);
        titleTextView.setText(titleText);


        deniedDeleteAlertDialog.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deniedDeleteAlertDialog.dismiss();
            }
        });
    }


    public void loadmore() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isSearch) {
                    supplierList.addAll(loadMore(supplierList.size(), 9, searchText));
                } else {
                    supplierList.addAll(loadMore(supplierList.size(), 9));
                }

                supplierList.addAll(loadMore(supplierList.size(), 9));

                rvSwiperAdapterForSupplierList.setShowLoader(false);

                refreshRecyclerView();
            }
        }, 500);
    }

    public List<Supplier> loadMore(int startLimit, int endLimit) {

        return supplierManager.getAllSuppliers(startLimit, endLimit);

    }

    public List<Supplier> loadMore(int startLimit, int endLimit, String query) {

        return supplierManager.getAllActiveSuppliersByNameOnSearch(startLimit, endLimit, query);


    }


    public void buildAddNewSupplierDialog() {

        addNewSupplierMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.new_supplier, true).title("New Supplier").positiveText("Save").negativeText("Cancel").build();

    }

    public void buildEditSupplierDialog() {

        editSupplierMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.new_supplier, true).title("Edit Supplier").positiveText("Save").negativeText("Cancel").build();

    }

    public boolean checkValidation() {
        boolean status = true;

        if (supplierNameTextInputEditTextMd.getText().toString().trim().length() < 1) {

            String sName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_supplier_name}).getString(0);
            supplierNameTextInputEditTextMd.setError(sName);

            status = false;

        }

        if (supplierBusinessNameTextInputEditTextMd.getText().toString().trim().length() < 1) {

            supplierBusinessNameTextInputEditTextMd.setError("Please Enter Supplier's Business Name");

            status = false;

        }

        return status;

    }

    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});

        int attributeResourceId = a.getResourceId(0, 0);

        searchView.setCursorDrawable(attributeResourceId);

    }

    public void clearDialog() {

        supplierNameTextInputEditTextMd.setText(null);

        supplierNameTextInputEditTextMd.setError(null);

        supplierBusinessNameTextInputEditTextMd.setText(null);

        supplierBusinessNameTextInputEditTextMd.setError(null);

        supplierPhoneTextInputEditTextMd.setText(null);

        supplierAddressTextInputEditTextMd.setText(null);

    }

    public void setDataEditDialog() {

        supplierNameTextInputEditTextMd.setText(supplierList.get(editPosition).getName());

        supplierBusinessNameTextInputEditTextMd.setText(supplierList.get(editPosition).getBusinessName());

        supplierPhoneTextInputEditTextMd.setText(supplierList.get(editPosition).getPhoneNo());

        supplierAddressTextInputEditTextMd.setText(supplierList.get(editPosition).getAddress());

    }


    public void getValuesFromView() {

        supplierName = supplierNameTextInputEditTextMd.getText().toString().trim();

        supplierBusinessName = supplierBusinessNameTextInputEditTextMd.getText().toString().trim();

        supplierPhone = supplierPhoneTextInputEditTextMd.getText().toString().trim();

        supplierAddress = supplierAddressTextInputEditTextMd.getText().toString().trim();

    }

    public void refreshSupplierList() {

        supplierList = supplierManager.getAllSuppliers(0, 10); //////////

        refreshRecyclerView();

    }

    private void refreshRecyclerView() {
        if (supplierList.size() > 0) {

            noTransactionTextView.setVisibility(View.GONE);

            supplierListRecyclerView.setVisibility(View.VISIBLE);

            rvSwiperAdapterForSupplierList.setSupplierList(supplierList);

            rvSwiperAdapterForSupplierList.notifyDataSetChanged();

        } else {


            noTransactionTextView.setVisibility(View.VISIBLE);

            supplierListRecyclerView.setVisibility(View.GONE);

        }
    }

    public void configRecyclerView() {

        supplierList = supplierManager.getAllSuppliers(0, 10);   ///////////

        rvSwiperAdapterForSupplierList = new RVSwiperAdapterForSupplierList(supplierList);

        supplierListRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        supplierListRecyclerView.setAdapter(rvSwiperAdapterForSupplierList);

    }

    public void startLoad() {

        Log.w("HERE START ", "LOAD");


        supplierList = supplierManager.getAllSuppliers(0, 10);   ///////////


        Log.w("HERE START  Size", supplierList.size() + "");

        rvSwiperAdapterForSupplierList.setSupplierList(supplierList);


        rvSwiperAdapterForSupplierList.notifyDataSetChanged();


        refreshRecyclerView();

        supplierListRecyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                if (shouldLoad) {

                    rvSwiperAdapterForSupplierList.setShowLoader(true);

                    loadmore();

                }

            }
        });


    }

    public void loadUI() {

        loadUIFromToolbar();

        noTransactionTextView = (TextView) mainLayout.findViewById(R.id.no_transaction);

        searchedResultTxt = (TextView) mainLayout.findViewById(R.id.searched_result_txt);

        supplierListRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.supplier_list_rv);

        addNewSupplierFab = (FloatingActionButton) mainLayout.findViewById(R.id.add_new_supplier);

        saveBtnMd = addNewSupplierMaterialDialog.getActionButton(DialogAction.POSITIVE);

        supplierNameTextInputEditTextMd = (EditText) addNewSupplierMaterialDialog.findViewById(R.id.suppl_name_in_add_supplier_TIET);

        supplierBusinessNameTextInputEditTextMd = (EditText) addNewSupplierMaterialDialog.findViewById(R.id.suppl_business_name_in_add_supplier_TIET);

        supplierPhoneTextInputEditTextMd = (EditText) addNewSupplierMaterialDialog.findViewById(R.id.suppl_phone_in_add_supplier_TIET);

        supplierAddressTextInputEditTextMd = (EditText) addNewSupplierMaterialDialog.findViewById(R.id.suppl_address_in_add_supplier_TIET);

        editSaveBtnMd = editSupplierMaterialDialog.getActionButton(DialogAction.POSITIVE);

        editSupplierNameTextInputEditTextMd = (EditText) editSupplierMaterialDialog.findViewById(R.id.suppl_name_in_add_supplier_TIET);

        editSupplierBusinessNameTextInputEditTextMd = (EditText) editSupplierMaterialDialog.findViewById(R.id.suppl_business_name_in_add_supplier_TIET);

        editSupplierPhoneTextInputEditTextMd = (EditText) editSupplierMaterialDialog.findViewById(R.id.suppl_phone_in_add_supplier_TIET);

        editSupplierAddressTextInputEditTextMd = (EditText) editSupplierMaterialDialog.findViewById(R.id.suppl_address_in_add_supplier_TIET);

    }


}
