package com.digitalfusion.android.pos.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.digitalfusion.android.pos.database.ApiDatabaseHelper;
import com.digitalfusion.android.pos.database.model.AccessLog;
import com.digitalfusion.android.pos.util.AppConstant;

import retrofit2.http.Body;

/**
 * Created by lyx on 5/23/18.
 */
public class AccessLogDAO {
    private ApiDatabaseHelper apiDatabaseHelper;
    private static AccessLogDAO instance;

    private AccessLogDAO(Context context) {
        apiDatabaseHelper = ApiDatabaseHelper.getHelperInstance(context);
    }

    public static AccessLogDAO getInstance(Context context) {
        if (instance == null) {
            instance = new AccessLogDAO(context);
        }
        return instance;
    }

    public Long insert(AccessLog accessLog) {
        ContentValues values = new ContentValues();
        values.put(AppConstant.COLUMN_ACCESS_ID, accessLog.getId());
        values.put(AppConstant.COLUMN_ACCESS_USER_ID, accessLog.getUserId());
        values.put(AppConstant.COLUMN_ACCESS_DEVICE_ID, accessLog.getDeviceId());
        values.put(AppConstant.COLUMN_ACCESS_DATE_TIME, accessLog.getDatetime());
        values.put(AppConstant.COLUMN_ACCESS_EVENT, accessLog.getEvent());

        SQLiteDatabase db = apiDatabaseHelper.getWritableDatabase();

        return db.insert(AppConstant.TABLE_ACCESS_LOGS, null, values);
    }

    public Long getLastLoggedInUserId(Long deviceId) {
        String projection[] =  new String[] {AppConstant.COLUMN_ACCESS_USER_ID};
        String selection = AppConstant.COLUMN_ACCESS_EVENT + " = ? AND " +
                            AppConstant.COLUMN_ACCESS_DEVICE_ID + " = ? ";
        String selectionArgs[] = new String[] {AppConstant.EVENT_IN, deviceId.toString()};
        String orderBy = AppConstant.COLUMN_ACCESS_DATE_TIME + " DESC ";

        SQLiteDatabase db = apiDatabaseHelper.getReadableDatabase();
        Cursor cursor = db.query(AppConstant.TABLE_ACCESS_LOGS,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                orderBy,
                "1");

        Long returnUserId = null;
        if (cursor.moveToFirst()) {
            returnUserId = cursor.getLong(0);
        }

        cursor.close();

        return  returnUserId;
    }

//    /**
//     * @param userId
//     * @param lastLoginDateTime <br>
//     *<pre>
//     * SELECT * FROM access_logs
//     * WHERE
//     *  user_id = userId
//     *  AND
//     *  date_time >= lastLoginDateTime
//     *</pre>
//     *
//     */
//    public Boolean isUserLoggedIn(Long userId, String lastLoginDateTime) {
//
//        String selection = AppConstant.COLUMN_ACCESS_USER_ID + " = ? AND " +
//                           AppConstant.COLUMN_ACCESS_DATE_TIME + " >= ? ";
//        String selectionArgs[] = new String[] {userId.toString(), lastLoginDateTime};
//
//        SQLiteDatabase db = apiDatabaseHelper.getReadableDatabase();
//        Cursor cursor = db.query(AppConstant.TABLE_ACCESS_LOGS,
//                null,
//                selection,
//                selectionArgs,
//                null,
//                null,
//                null);
//
//        boolean returnBool = cursor.getCount() > 0;
//
//        cursor.close();
//
//        return returnBool;
//    }

    /**
     * @param userId <br>
     *
     * SELECT * FROM access_logs
     * WHERE user_id = userId
     * ORDER BY date_time DESC
     * LIMIT 1
     *
     */
    public AccessLog getLatestUserAccessLog(Long userId) {
        String selection = AppConstant.COLUMN_ACCESS_USER_ID + " = ? ";
        String selectionArgs[] = new String[] {userId.toString()};
        String orderBy = AppConstant.COLUMN_ACCESS_DATE_TIME + " DESC, " +
                         AppConstant.COLUMN_ACCESS_ID + " DESC ";

        SQLiteDatabase db = apiDatabaseHelper.getReadableDatabase();
        Cursor cursor = db.query(AppConstant.TABLE_ACCESS_LOGS,
                null,
                selection,
                selectionArgs,
                null,
                null,
                orderBy,
                "1");

        AccessLog accessLog = null;
        if (cursor.moveToFirst()) {
            int accessLogIdIndex = cursor.getColumnIndex(AppConstant.COLUMN_ACCESS_ID);
            int userIdIndex = cursor.getColumnIndex(AppConstant.COLUMN_ACCESS_USER_ID);
            int deviceIdIndex = cursor.getColumnIndex(AppConstant.COLUMN_ACCESS_DEVICE_ID);
            int dateTimeIdIndex = cursor.getColumnIndex(AppConstant.COLUMN_ACCESS_DATE_TIME);
            int eventIdIndex = cursor.getColumnIndex(AppConstant.COLUMN_ACCESS_EVENT);

            accessLog = new AccessLog();
            accessLog.setId(cursor.getLong(accessLogIdIndex));
            accessLog.setUserId(cursor.getLong(userIdIndex));
            accessLog.setDeviceId(cursor.getLong(deviceIdIndex));
            accessLog.setDatetime(cursor.getString(dateTimeIdIndex));
            accessLog.setEvent(cursor.getString(eventIdIndex));
        }
        cursor.close();

        return accessLog;
    }



}
