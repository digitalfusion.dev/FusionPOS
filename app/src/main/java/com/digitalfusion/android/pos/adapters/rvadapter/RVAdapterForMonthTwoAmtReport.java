package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;

import java.util.List;

/**
 * Created by MD002 on 12/3/16.
 */

public class RVAdapterForMonthTwoAmtReport extends RecyclerView.Adapter<RVAdapterForMonthTwoAmtReport.ReportItemViewHolder> {

    protected String[] mMonths = new String[]{
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };
    private List<ReportItem> incomeReportItemList;
    private List<ReportItem> expenseReportItemList;

    public RVAdapterForMonthTwoAmtReport(List<ReportItem> incomeReportItemList, List<ReportItem> expenseReportItemList) {
        this.incomeReportItemList = incomeReportItemList;
        this.expenseReportItemList = expenseReportItemList;
    }

    @Override
    public ReportItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_month_two_amount, parent, false);

        return new ReportItemViewHolder(v);

    }


    @Override
    public void onBindViewHolder(ReportItemViewHolder holder, final int position) {
        holder.monthTextView.setText(mMonths[position]);

        Log.e("bal", incomeReportItemList.get(position).getBalance() + "doajf");
        holder.incomeTextView.setText(Double.toString(incomeReportItemList.get(position).getBalance()));
        holder.expenseTextView.setText(Double.toString(expenseReportItemList.get(position).getBalance()));
    }

    @Override
    public int getItemCount() {
        return incomeReportItemList.size();
    }

    public List<ReportItem> getIncomeReportItemList() {
        return incomeReportItemList;
    }

    public void setIncomeReportItemList(List<ReportItem> incomeReportItemList) {
        this.incomeReportItemList = incomeReportItemList;
    }

    public class ReportItemViewHolder extends RecyclerView.ViewHolder {
        TextView monthTextView;
        TextView incomeTextView;
        TextView expenseTextView;

        View view;

        public ReportItemViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            monthTextView = (TextView) itemView.findViewById(R.id.month_text_view);

            incomeTextView = (TextView) itemView.findViewById(R.id.income_text_view);

            expenseTextView = (TextView) itemView.findViewById(R.id.expense_text_view);

        }

    }

}
