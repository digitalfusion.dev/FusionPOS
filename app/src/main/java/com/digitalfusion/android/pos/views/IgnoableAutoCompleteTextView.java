package com.digitalfusion.android.pos.views;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.AttributeSet;

import com.digitalfusion.android.pos.util.TypefaceSpan;
import com.innovattic.font.AutoCompleteFontTextView;

/**
 * Created by MD003 on 10/20/17.
 */

public class IgnoableAutoCompleteTextView extends AutoCompleteFontTextView {
    public IgnoableAutoCompleteTextView(Context context) {
        super(context);
    }

    public IgnoableAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IgnoableAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void setError(CharSequence error) {

        if (error != null) {
            SpannableString s = new SpannableString(error.toString());
            s.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            super.setError(s);
        } else {
            super.setError(error);
        }

    }

}
