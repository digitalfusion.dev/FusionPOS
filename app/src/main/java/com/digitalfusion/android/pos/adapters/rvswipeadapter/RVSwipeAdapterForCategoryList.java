package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD003 on 8/24/16.
 */
public class RVSwipeAdapterForCategoryList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private List<Category> categoryList;

    private ClickListener editClickListener;

    private ClickListener deleteClickListener;


    public RVSwipeAdapterForCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item_view, parent, false);

        return new CategoryViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof CategoryViewHolder) {
            final CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;

            POSUtil.makeZebraStrip(categoryViewHolder.itemView, position);

            categoryViewHolder.categoryTextView.setText(categoryList.get(position).getName());
            if (categoryList.get(position).getDescription() != null && !categoryList.get(position).getDescription().equalsIgnoreCase("")) {

                categoryViewHolder.categoryDescTextView.setText(categoryList.get(position).getDescription());
            } else {
                categoryViewHolder.categoryDescTextView.setText(null);
                categoryViewHolder.categoryDescTextView.setHint(categoryViewHolder.nodes);
            }
            categoryViewHolder.editImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (editClickListener != null) {
                        editClickListener.onClick(position);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                categoryViewHolder.swipeLayout.close();
                            }
                        }, 500);
                    }
                }
            });

            categoryViewHolder.deleteImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteClickListener != null) {
                        deleteClickListener.onClick(position);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                categoryViewHolder.swipeLayout.close();
                            }
                        }, 500);
                    }
                }
            });


            mItemManger.bindView(categoryViewHolder.view, position);

        }


    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView categoryTextView;
        TextView categoryDescTextView;
        ImageButton editImageButton;
        ImageButton deleteImageButton;
        SwipeLayout swipeLayout;

        View view;

        String nodes;

        public CategoryViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            deleteImageButton = (ImageButton) itemView.findViewById(R.id.delete);
            editImageButton = (ImageButton) itemView.findViewById(R.id.edit_category);
            categoryTextView = (TextView) itemView.findViewById(R.id.category_name_in_category_item_view);
            categoryDescTextView = (TextView) itemView.findViewById(R.id.category_desc_in_category_item_view);


            nodes = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.no_description}).getString(0);
        }

    }
}
