package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.DamagedItem;
import com.digitalfusion.android.pos.database.model.LostItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 12/20/16.
 */

public class LostSearchAdapter extends ArrayAdapter<DamagedItem> {


    private List<LostItem> suggestion;

    private Context context;

    private String queryText;

    private int lenght;

    private Filter filter;

    public LostSearchAdapter(Context context, Filter filter) {
        super(context, -1);

        this.context = context;

        suggestion = new ArrayList<>();

        this.filter = filter;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        StockSearchAdapter.ViewHolder viewHolder;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.stock_item_auto_complete_view, parent, false);
        }
        viewHolder = new StockSearchAdapter.ViewHolder(convertView);

        if (!suggestion.isEmpty() && suggestion.size() > 0) {

            viewHolder.codeTextView.setText(suggestion.get(position).getStockCodeNo());

            Spannable spanText = new SpannableString(suggestion.get(position).getStockName());

            spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                    .getColor(R.color.accent)), 0, lenght, 0);

            viewHolder.nameTextView.setText(spanText);

        }

        Log.w("hello", position + " ss");

        return convertView;
    }


    @Override
    public int getCount() {

        return suggestion.size();

    }

    public List<LostItem> getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(List<LostItem> suggestion) {
        this.suggestion = suggestion;
    }

    @Override
    public Filter getFilter() {

        Log.w("gettin g", "filter");
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public String getQueryText() {
        return queryText;
    }

    public void setQueryText(String queryText) {
        this.queryText = queryText;
    }

    public int getLenght() {
        return lenght;
    }

    public void setLenght(int lenght) {
        this.lenght = lenght;
    }

    static class ViewHolder {
        TextView codeTextView;
        TextView nameTextView;

        public ViewHolder(View itemView) {
            this.nameTextView = (TextView) itemView.findViewById(R.id.name);
            this.codeTextView = (TextView) itemView.findViewById(R.id.code);
        }
    }
}