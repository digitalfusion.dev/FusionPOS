package com.digitalfusion.android.pos.database.dao;

import android.content.Context;

import com.digitalfusion.android.pos.database.model.DamageItemInDetail;

import java.util.List;

/**
 * Created by MD002 on 8/18/16.
 */
public class DamageDetailDAO extends ParentDAO {
    private static ParentDAO damageDetailDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private DamageItemInDetail damageItemInDetail;
    private List<DamageItemInDetail> damageItemInDetailList;
    private List<Long> idList;

    private DamageDetailDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        damageItemInDetail = new DamageItemInDetail();
    }

    public static DamageDetailDAO getDamageDetailDaoInstance(Context context) {
        if (damageDetailDaoInstance == null) {
            damageDetailDaoInstance = new DamageDetailDAO(context);
        }
        return (DamageDetailDAO) damageDetailDaoInstance;
    }

    /*public Long addNewStockDamageDetail (Long damageID, Long stockID, int qty, Double purchasePrice, Double total, String remark){
        genID = idGeneratorDAO.getLastIdValue("DamageDetail");
        genID +=1;
        database = databaseHelper.getWritableDatabase();
        query = "insert into " + AppConstant.DAMAGE_DETAIL_TABLE_NAME + " (" + AppConstant.DAMAGE_DETAIL_ID + ", " + AppConstant.DAMAGE_DETAIL_DAMAGE_ID + ", " +
                AppConstant.DAMAGE_DETAIL_STOCK_ID + ", " + AppConstant.DAMAGE_DETAIL_QTY + ", " + AppConstant.DAMAGE_DETAIL_PURCHASE_PRICE + ", " +
                AppConstant.DAMAGE_DETAIL_TOTAL + ", " +
                AppConstant.DAMAGE_DETAIL_REMARK + ", " + AppConstant.CREATED_DATE + ") values (?,?,?,?,?,?,?,?);";
        statement = database.compileStatement(query);
        statement.bindString(1, genID.toString());
       // statement.bindString(1, "lkdkk");
        statement.bindString(2, damageID.toString());
        statement.bindString(3, stockID.toString());
        statement.bindString(4, Integer.toString(qty));
        statement.bindString(5, purchasePrice.toString());
        statement.bindString(6, total.toString());
        //statement.bindString(7, unitID.toString());
        if (remark == null){
            statement.bindNull(7);
        }else {
            statement.bindString(7, remark);
        }
        statement.bindString(8, DateUtility.getTodayDate());
        statement.execute();
        statement.clearBindings();
        return genID;
    }

    public List<DamageItemInDetail> getAllDamageDetailByDamageID (Long damageID){
        damageItemInDetailList = new ArrayList<>();
        query = "select d." + AppConstant.DAMAGE_DETAIL_ID + ", d." + AppConstant.DAMAGE_DETAIL_DAMAGE_ID + ", d." + AppConstant.DAMAGE_DETAIL_STOCK_ID + ", s." + AppConstant.STOCK_NAME +
                ", s." + AppConstant.STOCK_CODE_NUM + ", s." + AppConstant.STOCK_BARCODE + ", s." + AppConstant.STOCK_CATEGORY_ID + ", c." + AppConstant.CATEGORY_NAME + ", d." + AppConstant.DAMAGE_DETAIL_QTY +
                ", s." + AppConstant.DAMAGE_DETAIL_PURCHASE_PRICE + ", d." + AppConstant.DAMAGE_DETAIL_TOTAL + ", s." + AppConstant.STOCK_UNIT_ID + ", u." + AppConstant.UNIT_UNIT + ", d."
                + AppConstant.DAMAGE_DETAIL_REMARK + " from " + AppConstant.DAMAGE_DETAIL_TABLE_NAME + " d, " + AppConstant.STOCK_TABLE_NAME + " s left outer join "
                + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID  + ", " + AppConstant.CATEGORY_TABLE_NAME + " c  "
                +" where d." + AppConstant.DAMAGE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and s." + AppConstant.STOCK_CATEGORY_ID + " = c."
                + AppConstant.CATEGORY_ID + " and d." + AppConstant.DAMAGE_DETAIL_DAMAGE_ID + " = " + damageID;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    damageItemInDetail = new DamageItemInDetail();
                    damageItemInDetail.setId(cursor.getLong(0));
                    damageItemInDetail.setDamageID(cursor.getLong(1));
                    damageItemInDetail.setStockID(cursor.getLong(2));
                    damageItemInDetail.setStockName(cursor.getString(3));
                    damageItemInDetail.setStockCodeNo(cursor.getString(4));
                    damageItemInDetail.setStockBarcodeNo(cursor.getString(5));
                    damageItemInDetail.setStockCategoryID(cursor.getLong(6));
                    damageItemInDetail.setStockCategoryName(cursor.getString(7));
                    damageItemInDetail.setQty(cursor.getInt(8));
                    damageItemInDetail.setPurchasePrice(cursor.getDouble(9));
                    damageItemInDetail.setTotal(cursor.getDouble(10));
                    damageItemInDetail.setUnitID(cursor.getLong(11));
                    damageItemInDetail.setUnit(cursor.getString(12));
                    damageItemInDetail.setRemark(cursor.getString(13));
                    damageItemInDetailList.add(damageItemInDetail);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        }catch (SQLiteException e){
            e.printStackTrace();
        }finally {
            database.endTransaction();
            cursor.close();
        }
        return damageItemInDetailList;
    }

    public List<Long> getDamageDetailIdList(Long damageID) {
        query = "select " + AppConstant.DAMAGE_DETAIL_ID + " from " + AppConstant.DAMAGE_DETAIL_TABLE_NAME + " where " + AppConstant.DAMAGE_DETAIL_DAMAGE_ID + " = " + damageID;
        database = databaseHelper.getWritableDatabase();
        cursor = database.rawQuery(query, null);
        idList = new ArrayList<>();
        if (cursor.moveToFirst()){
            do {
                id = new Long(0l);
                id = cursor.getLong(0);
                idList.add(id);
            }while (cursor.moveToNext());
        }
        if (cursor != null){
            cursor.close();
        }
        return idList;
    }

    public boolean updateStockDamageDetail(Long id, Long stockID, int qty, Double purchasePrice, Double total, String remark){
        query = "update " + AppConstant.DAMAGE_DETAIL_TABLE_NAME + " set " + AppConstant.DAMAGE_DETAIL_STOCK_ID + "=?, " + AppConstant.DAMAGE_DETAIL_QTY + "=?, "
                + AppConstant.DAMAGE_DETAIL_PURCHASE_PRICE + "=?, " + AppConstant.DAMAGE_DETAIL_TOTAL + "=?, " +
                AppConstant.DAMAGE_DETAIL_REMARK + "=? where " + AppConstant.DAMAGE_DETAIL_ID + "=?";
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query, new String[]{stockID.toString(), Integer.toString(qty), purchasePrice.toString(), total.toString(), remark, id.toString()});
        //databaseHelper.close();
        return true;
    }

    public boolean deleteStockDamageDetail(Long id){
        query = "delete from " + AppConstant.DAMAGE_DETAIL_TABLE_NAME + " where " + AppConstant.DAMAGE_DETAIL_ID + " = " + id;
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query);
       // databaseHelper.close();
        return true;
    }*/
}
