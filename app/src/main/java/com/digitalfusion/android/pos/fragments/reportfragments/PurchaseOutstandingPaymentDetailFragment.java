package com.digitalfusion.android.pos.fragments.reportfragments;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAPrintPreviewAdapter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForOutstandingReport;
import com.digitalfusion.android.pos.database.business.PurchaseManager;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.model.BusinessSetting;
import com.digitalfusion.android.pos.database.model.PurchaseHeader;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.util.GrantPermission;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.PrintInvoice;
import com.digitalfusion.android.pos.views.FolderChooserDialog;
import com.itextpdf.text.DocumentException;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.LENGTH_SHORT;

/**
 * A simple {@link Fragment} subclass.
 */
public class PurchaseOutstandingPaymentDetailFragment extends Fragment implements Serializable, FolderChooserDialog.FolderSelectionCallback {


    public TextView printDeviceNameTextView;

    //UI components from main
    MaterialDialog fileOverrideMaterialDialog;
    KProgressHUD saveHud;
    KProgressHUD printHud;
    KProgressHUD connectHud;
    private View mainLayout;
    private View deliverDashLine;
    private RecyclerView purchaseItemRecyclerView;
    private TextView purchaseIdTextView;
    private TextView dateTextView;
    private TextView discountTextView;
    private TextView subtotalTextView;
    private TextView totalTextView;
    private TextView supplierNameTextView;
    private TextView taxRateTextView;
    private TextView taxAmtTextView;
    private TextView taxTypeTextView;
    private TextView businessNameTextView;

    //Business
    private TextView addressTextView;
    private TextView phoneTextView;
    private TextView totalPaidAmtTextView;
    private TextView headerTextView;

    //Values
    private TextView footerTextView;
    private PurchaseManager purchaseManager;

    //private Double voucherDiscount = 0.0;

    // private Double voucherTax = 0.0;

    //  private Double subtotal = 0.0;
    //values
    private Context context;
    private RVAPrintPreviewAdapter rvAdapterForStockItemInSaleAndPurchaseDetail;
    private List<SalesAndPurchaseItem> salesAndPurchaseItemList;
    private String purchaseID;
    private Double totalAmount = 0.0;
    private String date;
    private ReportItem reportItem;
    private PurchaseHeader purchaseHeader;
    private BusinessSetting businessSetting;
    private SettingManager settingManager;
    private RVAdapterForOutstandingReport rvAdapterForOutstandingReport;
    private List<ReportItem> reportItemList;

    //private TextView totalReceivableTextView;
    private RecyclerView outstandingRecyclerView;

    // private DeliveryAndPickUpDialogFragment deliveryAndPickUpDialog;
    private ReportManager reportManager;
    private LinearLayout outstandingLinearLayout;
    private TextView receivableAmtTextView;
    private TextView totalPaidTextView;
    private Double totalPaidAmt;
    private FolderChooserDialog folderChooserDialog;
    private MaterialDialog fileNameMaterialDialog;
    private EditText fileNameEditText;
    private Button savePdfButton;
    private String path;
    private String fileName = "";
    private TextView statusTextView;
    private boolean isPrint = false;

    //    private BluetoothUtil bluetoothUtil;
    //
    //    private PrintViewSlip printViewSlip;
    private GrantPermission grantPermission;

    private String header;

    private String headerText;

    private String footerText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayout = inflater.inflate(R.layout.pur_out_print_preview, container, false);

        initializeVariables();

        //        setHasOptionsMenu(true);

        reportItem = (ReportItem) getArguments().getSerializable("purchase header");

        //        bluetoothUtil= BluetoothUtil.getInstance(this);
        //
        //        printViewSlip=new PrintViewSlip();

        grantPermission = new GrantPermission(this);

        initializeOldData();

        loadUI();

        configUI();

        configRecyclerView();

        updateViewData();

        //        print();

        buildAndConfigDialog();

        //        mainLayout.findViewById(R.id.blue_toothLL).setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //
        //                isPrint=false;
        //
        //                bluetoothUtil.createConnection(false);
        //            }
        //        });
        //
        //        bluetoothUtil.setBluetoothConnectionListener(new BluetoothUtil.JobListener() {
        //
        //            @Override
        //            public void onPrepare(){
        //                connectHud.show();
        //            }
        //
        //            @Override
        //            public void onSuccess() {
        //
        //                Log.w("on Success","on success");
        //
        //                if(connectHud.isShowing()){
        //
        //                    connectHud.dismiss();
        //
        //                }
        //
        //                printDeviceNameTextView.setText(bluetoothUtil.getDevice().getName());
        //
        //
        //                if(bluetoothUtil.isConnected()){
        //
        //                    statusTextView.setTextColor(Color.parseColor("#2E7D32"));
        //
        //                    String connect= context.getTheme().obtainStyledAttributes(new int[]{R.attr.connected}).getString(0);
        //                    statusTextView.setText(connect);
        //
        //                }else {
        //                    String notConnect= context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);
        //
        //                    statusTextView.setTextColor(Color.parseColor("#DD2C00"));
        //
        //                    statusTextView.setText(notConnect);
        //
        //                }
        //
        //                if(isPrint){
        //                    try {
        //
        //                        printViewSlip.printNow(bluetoothUtil.getOutputStream());
        //
        //                    } catch (Exception e) {
        //
        //                        e.printStackTrace();
        //
        //                    }
        //
        //                }
        //
        //            }
        //
        //            @Override
        //            public void onFailuare() {
        //
        //                String noDevice= context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_device}).getString(0);
        //                String notConnected= context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);
        //                String toast= context.getTheme().obtainStyledAttributes(new int[]{R.attr.make_sure_bluetooth_turn_on_both_devices_and_choose_printer_only}).getString(0);
        //
        //                printDeviceNameTextView.setText(noDevice);
        //
        //                statusTextView.setText(notConnected);
        //
        //                DisplayToast(toast);
        //
        //                if(connectHud.isShowing()){
        //
        //                    connectHud.dismiss();
        //
        //                }
        //
        //            }
        //            @Override
        //            public void onDisconnected() {
        //                String noDevice= context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_device}).getString(0);
        //                String notConnected= context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);
        //                String toast= context.getTheme().obtainStyledAttributes(new int[]{R.attr.connection_lost}).getString(0);
        //
        //                DisplayToast(toast);
        //
        //                printDeviceNameTextView.setText(noDevice);
        //
        //                statusTextView.setTextColor(Color.parseColor("#DD2C00"));
        //
        //                statusTextView.setText(notConnected);
        //
        //
        //            }
        //        });

        return mainLayout;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.w("ACITVITY", "RESULT");

        //        bluetoothUtil.onActivityResultBluetooth(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void initializeVariables() {
        salesAndPurchaseItemList = new ArrayList<>();

        context = getContext();

        settingManager = new SettingManager(context);

        businessSetting = settingManager.getBusinessSetting();

        String detail = context.getTheme().obtainStyledAttributes(new int[]{R.attr.detail}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(detail);

        purchaseManager = new PurchaseManager(context);

        reportItemList = new ArrayList<>();

        reportManager = new ReportManager(context);

        totalPaidAmt = 0.0;

    }


    //    private void print(){
    //
    //
    //
    //        if(!businessSetting.isEmptyBusinessName()){
    //
    //            printViewSlip.addString(businessSetting.getBusinessName(), PrintViewSlip.Alignment.CENTER);
    //
    //            printViewSlip.nextLine();
    //
    //        }
    //
    //        if(!businessSetting.isEmptyAddress()){
    //
    //            printViewSlip.addString(businessSetting.getAddress(), PrintViewSlip.Alignment.CENTER);
    //
    //            printViewSlip.nextLine();
    //        }
    //        if (!businessSetting.isEmptyPhoneNo()){
    //
    //            printViewSlip.addString(businessSetting.getPhoneNo(), PrintViewSlip.Alignment.CENTER);
    //
    //            printViewSlip.nextLine();
    //
    //        }
    //
    //        printViewSlip.nextLine();
    //
    //        String sale= context.getTheme().obtainStyledAttributes(new int[]{R.attr.sale}).getString(0);
    //        printViewSlip.addString(PrintViewSlip.STYLE.BOLD,sale);
    //
    //        printViewSlip.addSpace(5);
    //
    //        printViewSlip.addFullColon();
    //
    //        printViewSlip.addString(PrintViewSlip.STYLE.BOLD,"#");
    //
    //        printViewSlip.addString(PrintViewSlip.STYLE.BOLD,purchaseHeader.getPurchaseVocherNo());
    //
    //        printViewSlip.addString(DateUtility.makeDateFormatWithSlash(purchaseHeader.getSaleDate()), PrintViewSlip.Alignment.RIGHT);
    //
    //        printViewSlip.nextLine();
    //
    //        String customer= context.getTheme().obtainStyledAttributes(new int[]{R.attr.customer}).getString(0);
    //        printViewSlip.addString(customer+" ");
    //
    //        printViewSlip.addFullColon();
    //
    //        printViewSlip.addString(purchaseHeader.getSupplierName());
    //
    //        printViewSlip.nextLine();
    //
    //
    //
    //
    //        printViewSlip.BuildTable(3);
    //
    //        String no= context.getTheme().obtainStyledAttributes(new int[]{R.attr.no}).getString(0);
    //        String itemName= context.getTheme().obtainStyledAttributes(new int[]{R.attr.item_name}).getString(0);
    //        String price= context.getTheme().obtainStyledAttributes(new int[]{R.attr.price}).getString(0);
    //        printViewSlip.addNewColumn(no,5, PrintViewSlip.Alignment.CENTER,0);
    //
    //        printViewSlip.addNewColumn(itemName,30, PrintViewSlip.Alignment.LEFT,5);
    //
    //        // printViewSlip.addNewColumn("Price",10, PrintViewSlip.Alignment.RIGHT);
    //
    //        printViewSlip.addNewColumn(price,13, PrintViewSlip.Alignment.RIGHT,35);
    //
    //        printViewSlip.addHeader();
    //
    //        int i=1;
    //
    //        for (SalesAndPurchaseItem s:salesAndPurchaseItemList){
    //            printViewSlip.addRow(Integer.toString(i),s.getItemName()+" x "+s.getQty(), POSUtil.NumberFormat(s.getPrice()), POSUtil.NumberFormat(s.getTotalPrice()));
    //            i++;
    //        }
    //
    //        printViewSlip.addDividerLine();
    //
    //        String subtotal= context.getTheme().obtainStyledAttributes(new int[]{R.attr.subtotal}).getString(0);
    //        String tax= context.getTheme().obtainStyledAttributes(new int[]{R.attr.tax}).getString(0);
    //        String discount= context.getTheme().obtainStyledAttributes(new int[]{R.attr.discount}).getString(0);
    //        String total= context.getTheme().obtainStyledAttributes(new int[]{R.attr.total}).getString(0);
    //        String totalPaid= context.getTheme().obtainStyledAttributes(new int[]{R.attr.total_paid}).getString(0);
    //        String payable= context.getTheme().obtainStyledAttributes(new int[]{R.attr.payable}).getString(0);
    //
    //        printViewSlip.addRow("","                    "+subtotal+" :", POSUtil.NumberFormat(purchaseHeader.getSubtotal()));
    //
    //
    //
    //        // printViewSlip.addRow("","","Delivery :",POSUtil.NumberFormat(salesHeaderView.getSubtotal()));
    //
    //        printViewSlip.addRow("","                    "+tax+"      :", POSUtil.NumberFormat(purchaseHeader.getPaidAmt()));
    //
    //        printViewSlip.addRow("","                    "+discount+" :", POSUtil.NumberFormat(purchaseHeader.getDiscountAmt()));
    //
    //        printViewSlip.addDividerLine();
    //
    //        printViewSlip.addRow(PrintViewSlip.STYLE.BIG,"","                    "+total+"    :", POSUtil.NumberFormat(purchaseHeader.getTotal()));
    //
    //        printViewSlip.addRow(PrintViewSlip.STYLE.BIG,"","               "+totalPaid+"    :", POSUtil.NumberFormat(purchaseHeader.getPaidAmt()));
    //
    //
    //        printViewSlip.addDividerLine();
    //
    //        printViewSlip.addRow(PrintViewSlip.STYLE.BIG,"","               "+payable+"    :", POSUtil.NumberFormat(totalAmount));
    //
    //
    //        printViewSlip.addDividerLine();
    //
    //        printViewSlip.nextLine();
    //
    //        printViewSlip.nextLine();
    //
    //
    //        //payment voucher
    //        String paymentHistory= context.getTheme().obtainStyledAttributes(new int[]{R.attr.payment_history}).getString(0);
    //        String date= context.getTheme().obtainStyledAttributes(new int[]{R.attr.date}).getString(0);
    //        String voucherNo= context.getTheme().obtainStyledAttributes(new int[]{R.attr.voucher_no}).getString(0);
    //        String paidAmt= context.getTheme().obtainStyledAttributes(new int[]{R.attr.paid_amount}).getString(0);
    //
    //        printViewSlip.addString(PrintViewSlip.STYLE.BIG,paymentHistory);
    //
    //        printViewSlip.nextLine();
    //
    //        printViewSlip.addDividerLine();
    //
    //        printViewSlip.BuildTable(4);
    //
    //        printViewSlip.addNewColumn(no,5, PrintViewSlip.Alignment.CENTER,0);
    //
    //        printViewSlip.addNewColumn(date,12, PrintViewSlip.Alignment.LEFT,5);
    //
    //        printViewSlip.addNewColumn(voucherNo,15, PrintViewSlip.Alignment.LEFT,17);
    //
    //        printViewSlip.addNewColumn(paidAmt,16, PrintViewSlip.Alignment.RIGHT,32);
    //
    //        printViewSlip.addHeader();
    //
    //        int j=1;
    //
    //        for (ReportItem s:reportItemList){
    //            printViewSlip.addRow(Integer.toString(j),s.getDate(),s.getName(), POSUtil.NumberFormat(s.getAmount()));
    //            j++;
    //        }
    //
    //        printViewSlip.addDividerLine();
    //
    //        String thankU= context.getTheme().obtainStyledAttributes(new int[]{R.attr.thank_you}).getString(0);
    //
    //        printViewSlip.addRow("","",totalPaid+" :", POSUtil.NumberFormat(totalPaidAmt));
    //
    //        printViewSlip.addDividerLine();
    //
    //        printViewSlip.nextLine();
    //
    //        printViewSlip.addStringBold(thankU, PrintViewSlip.Alignment.CENTER);
    //
    //        printViewSlip.nextLine();
    //
    //        printViewSlip.nextLine();
    //
    //        printViewSlip.nextLine();
    //
    //        printViewSlip.nextLine();
    //
    //        printViewSlip.nextLine();
    //
    //
    //        //printViewSlip.addHeader("No.",);
    //
    //
    //    }

    //    @Override
    //    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    //
    //        getActivity().getMenuInflater().inflate(R.menu.only_print_and_save_pdf, menu);
    //
    //        super.onCreateOptionsMenu(menu, inflater);
    //    }
    //
    //    @Override
    //    public boolean onOptionsItemSelected(MenuItem item) {
    //
    //        if(item.getItemId()== R.id.print){
    //
    //            isPrint=true;
    //
    //            if (!bluetoothUtil.isConnected()) {
    //
    //                bluetoothUtil.createConnection(false);
    //
    //            }else {
    //
    //                try {
    //                    Bitmap bitmap = Bitmap.createBitmap(mainLayout.findViewById(R.id.ll).getWidth(), mainLayout.getHeight(), Bitmap.Config.ARGB_8888);
    //
    //                    Canvas canvas = new Canvas(bitmap);
    //
    //                    Log.w("bitmap", bitmap.toString() + " SS");
    //                    mainLayout.findViewById(R.id.aa).draw(canvas);
    //                    //mainLayout.findViewById(R.id.img).setVisibility(View.VISIBLE);
    //                    //mainLayout.findViewById(R.id.aa).setVisibility(View.GONE);
    //                    // mainLayout.findViewById(R.id.ll).setVisibility(View.GONE);
    //                    ImageView imageView = (ImageView) mainLayout.findViewById(R.id.img);
    //
    //                    imageView.setImageBitmap(bitmap);
    //
    //
    //                    Bitmap bmpMonochrome = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    //                    Canvas canvas1 = new Canvas(bmpMonochrome);
    //                    ColorMatrix ma = new ColorMatrix();
    //                    ma.setSaturation(0);
    //                    Paint paint1 = new Paint();
    //
    //                    canvas1.drawBitmap(bitmap, 0, 0, paint1);
    //
    //
    //                    int width2 = bmpMonochrome.getWidth();
    //                    int height2 = bmpMonochrome.getHeight();
    //
    //                    int[] pixels = new int[width2 * height2];
    //                    bmpMonochrome.getPixels(pixels, 0, width2, 0, 0, width2, height2);
    //
    //                    PrintImage printImage = new PrintImage(bmpMonochrome);
    //                    bluetoothUtil.getOutputStream().write(printImage.getPrintImageData());
    //
    //
    //                } catch (Exception e) {
    //                    e.printStackTrace();
    //                }
    //            }
    //        }else if(item.getItemId() == R.id.save_pdf) {
    //            if (grantPermission.requestPermissions()) {
    //                showFolderChooserDialog();
    //            }
    //        }
    //
    //        return true;
    //    }

    public void buildAndConfigDialog() {
        String saving = context.getTheme().obtainStyledAttributes(new int[]{R.attr.saving_as_pdf}).getString(0);

        View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
        ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
        imageView.setBackgroundResource(R.drawable.spin_animation);
        AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
        drawable.start();

        //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        saveHud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setDetailsLabel(saving)
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);

        String fileNamestr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.file_name}).getString(0);

        fileNameMaterialDialog = new MaterialDialog.Builder(getContext())
                .customView(R.layout.file_name, false)
                .title(fileNamestr).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

        fileNameEditText = (EditText) fileNameMaterialDialog.findViewById(R.id.file_name);

        savePdfButton = (Button) fileNameMaterialDialog.findViewById(R.id.save);

        savePdfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fileNameEditText.length() > 0) {
                    String npath = path + "/" + fileNameEditText.getText().toString() + ".pdf";

                    fileName = fileNameEditText.getText().toString();

                    File file = new File(npath);
                    // If file does not exists, then create it
                    if (!file.exists()) {
                        new SavePDFProgress().execute(path);
                        fileNameMaterialDialog.dismiss();
                    } else {
                        fileNameMaterialDialog.dismiss();
                        fileOverrideMaterialDialog.show();
                    }
                } else {
                    String fill = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_fill_file_name}).getString(0);
                    fileNameEditText.setError(fill);
                }


            }
        });

        fileNameMaterialDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileNameMaterialDialog.dismiss();
            }
        });

        fileOverrideMaterialDialog = new MaterialDialog.Builder(getContext())
                .customView(R.layout.confirm_dialog, false).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                // .title("File name already exist.Do you want to override it?")
                .build();

        String override = context.getTheme().obtainStyledAttributes(new int[]{R.attr.file_name_already_exist_do_u_want_to_override_it}).getString(0);
        ((TextView) fileOverrideMaterialDialog.findViewById(R.id.title)).setText(override);

        fileOverrideMaterialDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileOverrideMaterialDialog.dismiss();
                fileNameMaterialDialog.show();
            }
        });

        fileOverrideMaterialDialog.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SavePDFProgress().execute(path);
                fileNameMaterialDialog.dismiss();
            }
        });


    }

    private void showFolderChooserDialog() {
        if (folderChooserDialog == null) {
            folderChooserDialog = new FolderChooserDialog();
            folderChooserDialog.setmCallback(this);
        }
        folderChooserDialog.show(getFragmentManager(), "folderChooser");
    }


    public void initializeOldData() {

        purchaseID = reportItem.getName();//voucher no

        salesAndPurchaseItemList = purchaseManager.getPurchaseDetailsByPurchaseID(reportItem.getId());

        purchaseHeader = purchaseManager.getPurchaseHeaderByPurchaseID(reportItem.getId(), new InsertedBooleanHolder());

        String initial = context.getTheme().obtainStyledAttributes(new int[]{R.attr.initial_paid}).getString(0);
        reportItemList.add(new ReportItem(initial, reportItem.getDate(), reportItem.getAmount()));
        reportItemList.addAll(reportManager.paymentListForPurchaseID(reportItem.getId()));
    }

    public void configUI() {
        //        if(bluetoothUtil.isConnected()){
        //
        //            // printDeviceNameTextView.setTextColor(Color.parseColor("#2E7D32"));
        //
        //            printDeviceNameTextView.setText(bluetoothUtil.getDevice().getName());
        //
        //            statusTextView.setTextColor(Color.parseColor("#2E7D32"));
        //
        //            String connected= context.getTheme().obtainStyledAttributes(new int[]{R.attr.connected}).getString(0);
        //            statusTextView.setText(connected);
        //
        //        }else {
        //
        //            String noDevice= context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_device}).getString(0);
        //            String notConnected= context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);
        //
        //            // printDeviceNameTextView.setTextColor(Color.parseColor("#DD2C00"));
        //
        //            printDeviceNameTextView.setText(noDevice);
        //
        //            statusTextView.setTextColor(Color.parseColor("#DD2C00"));
        //
        //            statusTextView.setText(notConnected);
        //        }

        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        header = context.getTheme().obtainStyledAttributes(new int[]{R.attr.header_text}).getString(0);
        String thankU = context.getTheme().obtainStyledAttributes(new int[]{R.attr.thank_you}).getString(0);

        String temp_header = sharedPref.getString("header", header);
        String temp_footer = sharedPref.getString("footer", thankU);
        if (temp_header.equalsIgnoreCase(header)) {
            headerTextView.setVisibility(View.GONE);
            headerText = "";
        } else {
            headerText = temp_header;
            headerTextView.setText(temp_header);
            headerTextView.setVisibility(View.VISIBLE);
        }

        if (!temp_footer.equalsIgnoreCase("-1")) {
            footerText = temp_footer;
            footerTextView.setText(temp_footer);
        } else {
            footerText = "";
            footerTextView.setText("");
        }


        if (businessSetting.isEmptyBusinessName()) {

            businessNameTextView.setVisibility(View.GONE);

        } else {

            businessNameTextView.setText(businessSetting.getBusinessName());

        }

        if (businessSetting.isEmptyAddress()) {
            Log.w("address", "empty");

            addressTextView.setVisibility(View.GONE);

        } else {

            addressTextView.setText(businessSetting.getAddress());

        }
        if (businessSetting.isEmptyPhoneNo()) {
            Log.w("phone", "empty");
            phoneTextView.setVisibility(View.GONE);

        } else {

            phoneTextView.setText(businessSetting.getPhoneNo());

        }

        purchaseIdTextView.setText(purchaseID);

        taxRateTextView.setText(Double.toString(purchaseHeader.getTaxRate()));

        taxAmtTextView.setText(POSUtil.NumberFormat(purchaseHeader.getTaxAmt()));

        taxTypeTextView.setText(purchaseHeader.getTaxType());

        supplierNameTextView.setText(reportItem.getName1());//customer

        dateTextView.setText(reportItem.getDate());
    }


    public void configRecyclerView() {

        rvAdapterForStockItemInSaleAndPurchaseDetail = new RVAPrintPreviewAdapter(salesAndPurchaseItemList);

        purchaseItemRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        purchaseItemRecyclerView.setAdapter(rvAdapterForStockItemInSaleAndPurchaseDetail);

        if (!reportItemList.isEmpty()) {
            outstandingRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            outstandingLinearLayout.setVisibility(View.VISIBLE);
            rvAdapterForOutstandingReport = new RVAdapterForOutstandingReport(reportItemList, 3);
            outstandingRecyclerView.setAdapter(rvAdapterForOutstandingReport);
        }

    }

    private void loadUI() {
        String pleaseWait = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        String printing   = context.getTheme().obtainStyledAttributes(new int[]{R.attr.printing}).getString(0);
        String connecting = context.getTheme().obtainStyledAttributes(new int[]{R.attr.connected}).getString(0);

        View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
        ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
        imageView.setBackgroundResource(R.drawable.spin_animation);
        AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
        drawable.start();

        //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        printHud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setDetailsLabel(printing)
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);

        //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
        connectHud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setDetailsLabel(connecting)
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);

        statusTextView = (TextView) mainLayout.findViewById(R.id.bt_status);

        printDeviceNameTextView = (TextView) mainLayout.findViewById(R.id.device_name);


        businessNameTextView = (TextView) mainLayout.findViewById(R.id.business_name);

        addressTextView = (TextView) mainLayout.findViewById(R.id.address);

        phoneTextView = (TextView) mainLayout.findViewById(R.id.phone_no);

        supplierNameTextView = (TextView) mainLayout.findViewById(R.id.supplier_name_text_view);

        taxRateTextView = (TextView) mainLayout.findViewById(R.id.tax_rate_tv);

        taxAmtTextView = (TextView) mainLayout.findViewById(R.id.tax_amt_in_purchase_detail_tv);

        taxTypeTextView = (TextView) mainLayout.findViewById(R.id.tax_type_tv);

        purchaseItemRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.purchase_item_list_rv);

        purchaseIdTextView = (TextView) mainLayout.findViewById(R.id.purchase_id_tv);

        dateTextView = (TextView) mainLayout.findViewById(R.id.date_text_view);

        discountTextView = (TextView) mainLayout.findViewById(R.id.discount_in_purchase_detail_tv);

        subtotalTextView = (TextView) mainLayout.findViewById(R.id.subtotal_in_purchase_detail_tv);

        totalTextView = (TextView) mainLayout.findViewById(R.id.total_text_view);

        totalPaidAmtTextView = (TextView) mainLayout.findViewById(R.id.payment_total_paid_amt_text_view);

        outstandingRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.outstanding_recycler_view);

        outstandingLinearLayout = (LinearLayout) mainLayout.findViewById(R.id.outstanding_linear_layout);

        receivableAmtTextView = (TextView) mainLayout.findViewById(R.id.payable_bal_text_view);

        totalPaidTextView = (TextView) mainLayout.findViewById(R.id.total_paid_amt_text_view);

        //totalReceivableTextView = (TextView) mainLayout.findViewById(R.id.total_payable_amt_text_view);

        headerTextView = (TextView) mainLayout.findViewById(R.id.header_text);

        footerTextView = (TextView) mainLayout.findViewById(R.id.footer_text);

    }

    private void updateViewData() {


        discountTextView.setText(POSUtil.NumberFormat(purchaseHeader.getDiscountAmt()));

        subtotalTextView.setText(POSUtil.NumberFormat(purchaseHeader.getSubtotal()));

        totalAmount = purchaseHeader.getTotal();

        totalTextView.setText(POSUtil.NumberFormat(totalAmount));


        totalPaidAmt = 0.0;
        for (ReportItem r : reportItemList) {
            totalPaidAmt += r.getAmount();
        }


        totalPaidAmtTextView.setText(POSUtil.NumberFormat(totalPaidAmt));
        totalPaidTextView.setText(POSUtil.NumberFormat(totalPaidAmt));

        totalAmount -= totalPaidAmt;

        receivableAmtTextView.setText(POSUtil.NumberFormat(totalAmount));
        //totalReceivableTextView.setText(POSUtil.NumberFormat(totalAmount - totalPaidAmt));
    }

    public void replacingFragment(Fragment fragment) {
        final FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.replace(R.id.frame_replace, fragment);
        fragmentTransaction.commit();

    }

    @Override
    public void onFolderSelection(File folder) {

        path = folder.getAbsolutePath();

        fileNameEditText.setError(null);
        fileNameEditText.setText(null);

        fileNameMaterialDialog.show();
    }

    public void DisplayToast(String str) {
        if (getContext() != null) {
            Toast toast = Toast.makeText(getContext(), str, LENGTH_SHORT);
            //����toast��ʾ��λ��
            toast.setGravity(Gravity.BOTTOM, 0, 100);
            //��ʾ��Toast
            toast.show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //recreate();
            //reload my activity with requestPermissions granted or use the features what required the requestPermissions
            showFolderChooserDialog();
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            grantPermission.askAgainPermission();
        } else {
            grantPermission.forcePermissionSetting();
            //}
        }
    }

    class SavePDFProgress extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {


            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            PrintInvoice PrintInvoice = new PrintInvoice(getContext(), params[0]);

            // Print_pdf print_pdf=new Print_pdf();
            try {
                //print_pdf.createPDF("TEST",getContext());
                purchaseHeader.setHeaderText(headerText);
                purchaseHeader.setFooterText(footerText);

                PrintInvoice.createPurchasePDFWithPaymentHistory(fileName, reportItemList, purchaseHeader, salesAndPurchaseItemList, businessSetting);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!saveHud.isShowing()) {
                        saveHud.show();
                    }


                }
            });

        }

        @Override
        protected void onPostExecute(String a) {

            if (saveHud.isShowing()) {

                saveHud.dismiss();

                String saveSuccessfully = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save_successfully}).getString(0);
                DisplayToast(saveSuccessfully);

            }

            fileOverrideMaterialDialog.dismiss();

        }
    }
}