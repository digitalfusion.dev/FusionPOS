package com.digitalfusion.android.pos.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.PurchaseManager;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.database.model.Tax;
import com.digitalfusion.android.pos.util.DateUtility;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DataActivity extends AppCompatActivity {

    double balance;
    boolean outstand = false;
    Random random;
    private List<StockItem> stockItemList;
    private StockManager stockManager;
    private List<SalesAndPurchaseItem> salesAndPurchaseItemList;
    private SalesManager salesManager;
    private String[] customers = {"Default", "Htet Aung", "Hnin Hsu", "Moe Sanda", "Phyo Kyaw", "Su Htwe", "Yin Su Thwin"};
    private Double subTotal;
    private Double paidAmount;
    private Long total;
    private PurchaseManager purchaseManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        stockManager = new StockManager(this);
        stockItemList = stockManager.getAllStocks(1, 380);
        salesManager = new SalesManager(this);
        purchaseManager = new PurchaseManager(this);
        random = new Random();

        findViewById(R.id.sale).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int j = 0; j < 10000; j++) {

                    String customer = customers[random.nextInt(customers.length)];
                    //                    String invoice= salesManager.getSalesInvoiceNo();

                    String invoice       = "";
                    int    saleItemCount = random.nextInt(10) + 1;
                    subTotal = 0.0;
                    outstand = random.nextBoolean();
                    salesAndPurchaseItemList = new ArrayList<SalesAndPurchaseItem>();


                    for (int i = 0; i < saleItemCount; i++) {

                        StockItem stockItem = stockItemList.get(random.nextInt(380));

                        SalesAndPurchaseItem s = wrap(stockItem);

                        subTotal += s.getTotalPrice();

                        salesAndPurchaseItemList.add(s);

                    }


                    if (outstand) {
                        paidAmount = subTotal - (subTotal / 2);
                        balance = subTotal - paidAmount;
                    } else {
                        paidAmount = subTotal;
                        balance = 0;
                    }


                    //                    boolean status = salesManager.addNewSales(invoice, customer, "", subTotal, "", 1l, subTotal, SalesManager.SaleType.Sales.toString(),
                    //                            0.0, "", paidAmount, balance, 0.0, 0.0, Integer.toString(random.nextInt(28)),
                    //                            Integer.toString(random.nextInt(11)), Integer.toString(2017), salesAndPurchaseItemList, null, new SaleDelivery(), Tax.TaxType.Inclusive.toString(), null);

                }

            }
        });

        findViewById(R.id.purchase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int j = 0; j < 10000; j++) {

                    String customer = customers[random.nextInt(customers.length)];
                    String invoice  = purchaseManager.getPurchaseInvoiceNo();

                    int saleItemCount = random.nextInt(10) + 1;
                    subTotal = 0.0;
                    outstand = random.nextBoolean();
                    salesAndPurchaseItemList = new ArrayList<SalesAndPurchaseItem>();

                    int day   = random.nextInt(28);
                    int month = random.nextInt(11);
                    int year  = 2017;

                    for (int i = 0; i < saleItemCount; i++) {

                        StockItem stockItem = stockItemList.get(random.nextInt(380));

                        SalesAndPurchaseItem s = wrap(stockItem);

                        subTotal += s.getTotalPrice();

                        salesAndPurchaseItemList.add(s);

                    }


                    if (outstand) {
                        paidAmount = subTotal - (subTotal / 2);
                        balance = subTotal - paidAmount;
                    } else {
                        paidAmount = subTotal;
                        balance = 0;
                    }

                  /*  boolean status = purchaseManager.addNewPurchase(invoice, DateUtility.makeDate(Integer.toString(year), Integer.toString(month), Integer.toString(day)), customer, "",
                            "", subTotal, "", 1l, subTotal, 0.0, "No remark", paidAmount,
                            balance, 0.0, Integer.toString(day),
                            Integer.toString(month), Integer.toString(year), 0.0, salesAndPurchaseItemList, Tax.TaxType.Inclusive.toString(), null, AppConstant.currentUserId);*/

                }

            }
        });


    }

    private SalesAndPurchaseItem wrap(StockItem stockItem) {

        SalesAndPurchaseItem salesAndPurchaseItem = new SalesAndPurchaseItem();

        salesAndPurchaseItem.setDiscountAmount(0.0);

        salesAndPurchaseItem.setBarcode(stockItem.getBarcode());

        salesAndPurchaseItem.setStockCode(stockItem.getCodeNo());

        salesAndPurchaseItem.setItemName(stockItem.getName());

        salesAndPurchaseItem.setQty(random.nextInt(5) + 1);

        salesAndPurchaseItem.setDiscountPercent("0");


        salesAndPurchaseItem.setDiscountAmount(0.0);


        salesAndPurchaseItem.setStockID(stockItem.getId());

        salesAndPurchaseItem.setPrice(stockItem.getRetailPrice());

        salesAndPurchaseItem.setTotalPrice(stockItem.getRetailPrice() * salesAndPurchaseItem.getQty());

        Log.w("Herer", salesAndPurchaseItem.getId() + " in data activty");

        return salesAndPurchaseItem;
    }

    private SalesAndPurchaseItem wrapPur(StockItem stockItem) {

        SalesAndPurchaseItem salesAndPurchaseItem = new SalesAndPurchaseItem();

        salesAndPurchaseItem.setDiscountAmount(0.0);

        salesAndPurchaseItem.setBarcode(stockItem.getBarcode());

        salesAndPurchaseItem.setStockCode(stockItem.getCodeNo());

        salesAndPurchaseItem.setItemName(stockItem.getName());

        salesAndPurchaseItem.setQty(random.nextInt(5) + 1);

        salesAndPurchaseItem.setDiscountPercent("0");


        salesAndPurchaseItem.setDiscountAmount(0.0);


        salesAndPurchaseItem.setStockID(stockItem.getId());

        salesAndPurchaseItem.setPrice(stockItem.getPurchasePrice());

        salesAndPurchaseItem.setTotalPrice(stockItem.getPurchasePrice() * salesAndPurchaseItem.getQty());

        Log.w("Herer", salesAndPurchaseItem.getId() + " in data activty");

        return salesAndPurchaseItem;
    }
}
