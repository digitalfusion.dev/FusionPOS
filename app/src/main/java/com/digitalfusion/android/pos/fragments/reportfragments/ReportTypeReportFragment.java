package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForReportItemList;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.database.model.ExpenseIncome;
import com.digitalfusion.android.pos.util.ThemeUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD001 on 3/6/17.
 */

public class ReportTypeReportFragment extends Fragment implements Serializable {

    private View mainLayout;

    private Context context;

    private RecyclerView report_rv;

    private RVAdapterForReportItemList rvAdapterForReportList;

    private int reportType;

    private String title;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.report_item_list, null);

        report_rv = (RecyclerView) mainLayout.findViewById(R.id.report);

        context = getContext();

        if (getArguments() != null) {
            reportType = getArguments().getInt("type");
        }

        switch (reportType) {
            case 0:
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.sale_report}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                configRecyclerView_Sale();
                break;
            case 1:
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.purchase_report}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                configRecyclerView_Purchase();
                break;
            case 2:
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.inventory_report}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                configRecyclerView_Inventory();
                break;
            case 3:
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.outstanding_report}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                configRecyclerView_Outstanding();
                break;
            case 4:
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.financial_report}).getString(0);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
                configRecyclerView_Financial();
                break;
            default:
                break;
        }


        return mainLayout;

    }


    private void configRecyclerView_Sale() {
        String topSaleProduct               = ThemeUtil.getString(context, R.attr.top_sales_by_product                 );
        String topSaleCategory              = ThemeUtil.getString(context, R.attr.top_sales_by_category                );
        String topSaleCustomer              = ThemeUtil.getString(context, R.attr.top_sales_by_customer                );
        String topSaleQuantity              = ThemeUtil.getString(context, R.attr.top_sales_by_quantity                );
        String bottomSaleProduct            = ThemeUtil.getString(context, R.attr.bottom_sales_by_product              );
        String monthlySaleTrans             = ThemeUtil.getString(context, R.attr.monthly_sales_transaction            );
        String monthlySaleProfit            = ThemeUtil.getString(context, R.attr.monthly_sales_profit_gross           );
        String monthlySaleProfitProduct     = ThemeUtil.getString(context, R.attr.monthly_sales_profit_gross_by_product);
        String monthlySalesBySaleStaff      = ThemeUtil.getString(context, R.attr.monthly_sales_by_sale_staff          );
        String comparisonOfSalesBySaleStaff = ThemeUtil.getString(context, R.attr.comparison_of_sales_by_sales_staff   );
        String salesDetailBySaleStaff       = ThemeUtil.getString(context, R.attr.sales_detail_by_sale_staff);

        String topSaleProductDes           = ThemeUtil.getString(context, R.attr.top_sales_by_product_description);
        String topSaleCategoryDes          = ThemeUtil.getString(context, R.attr.top_sales_by_category_description);
        String topSaleCustomerDes          = ThemeUtil.getString(context, R.attr.top_sales_by_customer_description);
        String topSaleQuantityDes          = ThemeUtil.getString(context, R.attr.top_sales_by_quantity_description);
        String bottomSaleProductDes        = ThemeUtil.getString(context, R.attr.bottom_sales_by_product_description);
        String monthlySaleTransDes         = ThemeUtil.getString(context, R.attr.monthly_sales_transaction_description);
        String monthlySaleProfitDes        = ThemeUtil.getString(context, R.attr.monthly_sales_profit_gross_description);
        String monthlySaleProfitProductDes = ThemeUtil.getString(context, R.attr.monthly_sales_profit_gross_by_product_description);

        String saleItemReport = ThemeUtil.getString(context, R.attr.sale_item_report);

        //For Sale Reports List
        List<String> saleReportsList = new ArrayList<>();
        saleReportsList.add(topSaleProduct); //0
        saleReportsList.add(topSaleCategory);//1
        saleReportsList.add(topSaleCustomer);//2
        saleReportsList.add(topSaleQuantity);//3
        saleReportsList.add(bottomSaleProduct);//4
        saleReportsList.add(monthlySaleTrans);//5
        saleReportsList.add(monthlySaleProfit);//6
        saleReportsList.add(monthlySaleProfitProduct);//7
        saleReportsList.add(saleItemReport);//8
        saleReportsList.add(monthlySalesBySaleStaff);
        saleReportsList.add(comparisonOfSalesBySaleStaff);
        saleReportsList.add(salesDetailBySaleStaff);

        List<String> saleDesReportsList = new ArrayList<>();
        saleDesReportsList.add(topSaleProductDes);
        saleDesReportsList.add(topSaleCategoryDes);
        saleDesReportsList.add(topSaleCustomerDes);
        saleDesReportsList.add(topSaleQuantityDes);
        saleDesReportsList.add(bottomSaleProductDes);
        saleDesReportsList.add(monthlySaleTransDes);
        saleDesReportsList.add(monthlySaleProfitDes);
        saleDesReportsList.add(monthlySaleProfitProductDes);
        saleDesReportsList.add(saleItemReport);
        saleDesReportsList.add(monthlySalesBySaleStaff);
        saleDesReportsList.add(comparisonOfSalesBySaleStaff);
        saleDesReportsList.add(salesDetailBySaleStaff);


        Drawable saleIcon = ThemeUtil.getDrawable(context, R.attr.ic_sale);


        rvAdapterForReportList = new RVAdapterForReportItemList(saleIcon, saleReportsList, saleDesReportsList);

        report_rv.setLayoutManager(new LinearLayoutManager(context));

        report_rv.setAdapter(rvAdapterForReportList);

        rvAdapterForReportList.setmItemClickListener(new RVAdapterForReportItemList.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position == 0) {
                    //Go to Top 10 Sales Items Report
                    // Log.e("pin", position+" fad");
                    Bundle                         bundle           = new Bundle();
                    BarChartFragmentWithTwoFilters barChartFragment = new BarChartFragmentWithTwoFilters();
                    bundle.putString("reportType", "top sale by products");

                    barChartFragment.setArguments(bundle);
                    //  MainActivity.replaceFragment(barChartFragment);
                    bundle.putSerializable("frag", barChartFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);


                } else if (position == 1) {
                    //Go to Bottom 10 Sales Items Report
                    Bundle           bundle           = new Bundle();
                    PieChartFragment pidChartFragment = new PieChartFragment();
                    bundle.putString("reportType", "top sales by category");
                    pidChartFragment.setArguments(bundle);
                    //  MainActivity.replaceFragment(pidChartFragment);


                    bundle.putSerializable("frag", pidChartFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);

                } else if (position == 2) {
                    Bundle           bundle           = new Bundle();
                    BarChartFragment barChartFragment = new BarChartFragment();
                    bundle.putString("reportType", "top sales by customer");
                    barChartFragment.setArguments(bundle);
                    //MainActivity.replaceFragment(barChartFragment);


                    bundle.putSerializable("frag", barChartFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                    //Go to Top 10 Sales By Category Report

                } else if (position == 3) {
                    //Go to Top 10 Sales Items By Category Report
                    Bundle           bundle           = new Bundle();
                    BarChartFragment barChartFragment = new BarChartFragment();
                    bundle.putString("reportType", "top sale by qty");
                    barChartFragment.setArguments(bundle);
                    //MainActivity.replaceFragment(barChartFragment);


                    bundle.putSerializable("frag", barChartFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 4) {
                    //Go to Sale Transactions Report
                    Bundle                         bundle           = new Bundle();
                    BarChartFragmentWithTwoFilters barChartFragment = new BarChartFragmentWithTwoFilters();
                    bundle.putString("reportType", "bottom sales by products");
                    barChartFragment.setArguments(bundle);


                    bundle.putSerializable("frag", barChartFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);

                } else if (position == 5) {
                    //Go to Top 10 Customers Report

                    Bundle            bundle            = new Bundle();
                    LineChartFragment lineChartFragment = new LineChartFragment();
                    bundle.putString("reportType", "monthly sales transactions");
                    lineChartFragment.setArguments(bundle);
                    //   MainActivity.replaceFragment(lineChartFragment);


                    bundle.putSerializable("frag", lineChartFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 6) {
                    //Go to Sales Tax Report
                    Bundle                       bundle                       = new Bundle();
                    BarChartWithYearViewFragment barChartWithYearViewFragment = new BarChartWithYearViewFragment();
                    bundle.putString("reportType", "monthly gross profit");
                    barChartWithYearViewFragment.setArguments(bundle);
                    //    MainActivity.replaceFragment(barChartWithYearViewFragment);


                    bundle.putSerializable("frag", barChartWithYearViewFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 7) {
                    Bundle              bundle              = new Bundle();
                    StockSearchFragment stockSearchFragment = new StockSearchFragment();
                    bundle.putString("reportType", "monthly gross profit by product");
                    stockSearchFragment.setArguments(bundle);
                    //  MainActivity.replaceFragment(barChartWithSearchViewFragment);

                    bundle.putSerializable("frag", stockSearchFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 8) {
                    Bundle                 bundle                 = new Bundle();
                    SalePurchaseItemReport salePurchaseItemReport = new SalePurchaseItemReport();
                    bundle.putString("reportType", "sale");
                    salePurchaseItemReport.setArguments(bundle);
                    //  MainActivity.replaceFragment(barChartWithSearchViewFragment);
                    bundle.putSerializable("frag", salePurchaseItemReport);
                    Intent detailIntent = new Intent(context, DetailActivity.class);
                    detailIntent.putExtras(bundle);
                    startActivity(detailIntent);
                } else if (position == 9) {
                    LineChartFragmentWithTwoFilters report = new LineChartFragmentWithTwoFilters();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("frag", report);
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else if (position == 10) {
                    PieChartFragment report = new PieChartFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("reportType", getString(R.string.comparison_of_sales_by_sales_staff));
                    bundle.putSerializable("frag", report);
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else if (position == 11) {
                    SimpleReportFragmentWithTwoFilters reportFragmentWithTwoFilters = new SimpleReportFragmentWithTwoFilters();
                    Bundle bundle = new Bundle();
                    bundle.putString("reportType", getString(R.string.sales_detail_by_sale_staff));
                    bundle.putSerializable("frag", reportFragmentWithTwoFilters);
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });

    }

    private void configRecyclerView_Purchase() {
        String topSupplier                = ThemeUtil.getString(context, R.attr.top_supplier);
        String monthlyPurchaseTransaction = ThemeUtil.getString(context, R.attr.monthly_purchase_transaction);
        String productBySupplier          = ThemeUtil.getString(context, R.attr.product_by_supplier);
        String productPriceTrend          = ThemeUtil.getString(context, R.attr.product_price_trend);
        String productCategoryBySupplier  = ThemeUtil.getString(context, R.attr.product_category_by_supplier);

        String topSupplierDes                = ThemeUtil.getString(context, R.attr.top_supplier_description);
        String monthlyPurchaseTransactionDes = ThemeUtil.getString(context, R.attr.monthly_purchase_transaction_description);
        String productBySupplierDes          = ThemeUtil.getString(context, R.attr.product_by_supplier_description);
        String productPriceTrendDes          = ThemeUtil.getString(context, R.attr.product_price_trend_description);
        String productCategoryBySupplierDes  = ThemeUtil.getString(context, R.attr.product_category_by_supplier_description);

        String saleItemReport = ThemeUtil.getString(context, R.attr.purchase_item_report);

        //For Purchase Reports List
        List<String> purchaseReportsList = new ArrayList<>();
        purchaseReportsList.add(topSupplier);
        purchaseReportsList.add(monthlyPurchaseTransaction);
        purchaseReportsList.add(productBySupplier);
        purchaseReportsList.add(productPriceTrend);
        purchaseReportsList.add(productCategoryBySupplier);
        purchaseReportsList.add(saleItemReport);

        List<String> purchaseDesReportsList = new ArrayList<>();
        purchaseDesReportsList.add(topSupplierDes);
        purchaseDesReportsList.add(monthlyPurchaseTransactionDes);
        purchaseDesReportsList.add(productBySupplierDes);
        purchaseDesReportsList.add(productPriceTrendDes);
        purchaseDesReportsList.add(productCategoryBySupplierDes);
        purchaseDesReportsList.add(saleItemReport);


        TypedArray purchaseIcon = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_purchase});


        rvAdapterForReportList = new RVAdapterForReportItemList(purchaseIcon.getDrawable(0), purchaseReportsList, purchaseDesReportsList);

        report_rv.setLayoutManager(new LinearLayoutManager(context));

        report_rv.setAdapter(rvAdapterForReportList);

        rvAdapterForReportList.setmItemClickListener(new RVAdapterForReportItemList.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position == 0) {
                    //Go to Top 10 Purchase Items Report
                    Bundle           bundle           = new Bundle();
                    BarChartFragment barChartFragment = new BarChartFragment();
                    bundle.putString("reportType", "top suppliers");
                    barChartFragment.setArguments(bundle);
                    // MainActivity.replaceFragment(barChartFragment);


                    bundle.putSerializable("frag", barChartFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 1) {
                    //Go to Bottom 10 Purchase Items Report
                    Bundle            bundle            = new Bundle();
                    LineChartFragment lineChartFragment = new LineChartFragment();
                    bundle.putString("reportType", "monthly purchase transactions");
                    lineChartFragment.setArguments(bundle);
                    //   MainActivity.replaceFragment(lineChartFragment);


                    bundle.putSerializable("frag", lineChartFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 2) {
                    //Go to Top 10 Purchase By Category Report
                    Bundle                                  bundle                             = new Bundle();
                    StockSearchWithSuppliedSupplierFragment simpleReportFragmentWithSearchView = new StockSearchWithSuppliedSupplierFragment();
                    bundle.putString("reportType", "supplier by product");
                    simpleReportFragmentWithSearchView.setArguments(bundle);
                    // MainActivity.replaceFragment(simpleReportFragmentWithSearchView);

                    bundle.putSerializable("frag", simpleReportFragmentWithSearchView);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 3) {
                    //Go to Top 10 Purchase Items By Category Report

                    Bundle              bundle              = new Bundle();
                    StockSearchFragment stockSearchFragment = new StockSearchFragment();
                    bundle.putString("reportType", "product price trend");
                    stockSearchFragment.setArguments(bundle);
                    // MainActivity.replaceFragment(lineChartForPriceTrendFragment);


                    bundle.putSerializable("frag", stockSearchFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 4) {

                    Bundle bundle = new Bundle();
                    Log.e("reor", "er");
                    SimpleReportFragmentWithDateFilter simpleReportFragmentWithDateFilter = new SimpleReportFragmentWithDateFilter();
                    bundle.putString("reportType", "product category by supplier");
                    simpleReportFragmentWithDateFilter.setArguments(bundle);
                    // MainActivity.replaceFragment(simpleReportFragmentWithDateFilter);


                    bundle.putSerializable("frag", simpleReportFragmentWithDateFilter);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 5) {
                    Bundle                 bundle                 = new Bundle();
                    SalePurchaseItemReport salePurchaseItemReport = new SalePurchaseItemReport();
                    bundle.putString("reportType", "purchase");
                    salePurchaseItemReport.setArguments(bundle);
                    //  MainActivity.replaceFragment(barChartWithSearchViewFragment);

                    bundle.putSerializable("frag", salePurchaseItemReport);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                }
            }
        });

    }

    private void configRecyclerView_Inventory() {
        String damagedProduct        = ThemeUtil.getString(context, R.attr.damaged_product);
        String lostProduct           = ThemeUtil.getString(context, R.attr.lost_product);
        String lowStockProduct       = ThemeUtil.getString(context, R.attr.low_stock_product);
        String inventoryValuation    = ThemeUtil.getString(context, R.attr.inventory_valuation);
        String topProductByValuation = ThemeUtil.getString(context, R.attr.top_product_by_valuation);
        String stockBalance          = ThemeUtil.getString(context, R.attr.stock_balance);

        String damagedProductDes        = ThemeUtil.getString(context, R.attr.damaged_product_description);
        String lostProductDes           = ThemeUtil.getString(context, R.attr.lost_product_description);
        String lowStockProductDes       = ThemeUtil.getString(context, R.attr.low_stock_product_description);
        String inventoryValuationDes    = ThemeUtil.getString(context, R.attr.inventory_valuation_description);
        String topProductByValuationDes = ThemeUtil.getString(context, R.attr.top_product_by_valuation_description);
        String stockBalanceDes          = ThemeUtil.getString(context, R.attr.stock_balance_description);

        //For Inventory Reports List
        List<String> inventoryReportsList = new ArrayList<>();
        inventoryReportsList.add(damagedProduct);
        inventoryReportsList.add(lostProduct);
        inventoryReportsList.add(lowStockProduct);
        inventoryReportsList.add(inventoryValuation);
        inventoryReportsList.add(topProductByValuation);
        inventoryReportsList.add(stockBalance);

        List<String> inventoryDesReportsList = new ArrayList<>();
        inventoryDesReportsList.add(damagedProductDes);
        inventoryDesReportsList.add(lostProductDes);
        inventoryDesReportsList.add(lowStockProductDes);
        inventoryDesReportsList.add(inventoryValuationDes);
        inventoryDesReportsList.add(topProductByValuationDes);
        inventoryDesReportsList.add(stockBalanceDes);

        TypedArray inventoryIcon = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_inventory});


        rvAdapterForReportList = new RVAdapterForReportItemList(inventoryIcon.getDrawable(0), inventoryReportsList, inventoryDesReportsList);

        report_rv.setLayoutManager(new LinearLayoutManager(context));

        report_rv.setAdapter(rvAdapterForReportList);

        rvAdapterForReportList.setmItemClickListener(new RVAdapterForReportItemList.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position == 0) {
                    //Go to Top 10 Damaged Items Report

                    Bundle                             bundle                             = new Bundle();
                    SimpleReportFragmentWithDateFilter simpleReportFragmentWithDateFilter = new SimpleReportFragmentWithDateFilter();
                    bundle.putString("reportType", "damage list");
                    simpleReportFragmentWithDateFilter.setArguments(bundle);
                    //     MainActivity.replaceFragment(simpleReportFragmentWithDateFilter);


                    bundle.putSerializable("frag", simpleReportFragmentWithDateFilter);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 1) {
                    //Go to Top 10 Lost Items Report

                    Bundle                             bundle                             = new Bundle();
                    SimpleReportFragmentWithDateFilter simpleReportFragmentWithDateFilter = new SimpleReportFragmentWithDateFilter();
                    bundle.putString("reportType", "lost list");
                    simpleReportFragmentWithDateFilter.setArguments(bundle);
                    //     MainActivity.replaceFragment(simpleReportFragmentWithDateFilter);


                    bundle.putSerializable("frag", simpleReportFragmentWithDateFilter);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 2) {
                    //Go to Top 10 Outstanding Customers Report
                    Bundle bundle = new Bundle();
                    Log.e("reor", "er");
                    SimpleReportFragment simpleReportFragment = new SimpleReportFragment();
                    bundle.putString("reportType", "reorder qty");
                    simpleReportFragment.setArguments(bundle);
                    //   MainActivity.replaceFragment(simpleReportFragment);


                    bundle.putSerializable("frag", simpleReportFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 3) {
                    Bundle                             bundle                             = new Bundle();
                    SimpleReportFragmentWithDateFilter simpleReportFragmentwithDateFilter = new SimpleReportFragmentWithDateFilter();
                    bundle.putString("reportType", "inventory valuation");
                    simpleReportFragmentwithDateFilter.setArguments(bundle);
                    //  MainActivity.replaceFragment(simpleReportFragmentwithDateFilter);


                    bundle.putSerializable("frag", simpleReportFragmentwithDateFilter);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 4) {
                    //Go to Top 10 Outstanding Suppliers Report
                    Bundle              bundle              = new Bundle();
                    StockSearchFragment stockSearchFragment = new StockSearchFragment();
                    bundle.putString("reportType", "product by valuation");
                    stockSearchFragment.setArguments(bundle);
                    // MainActivity.replaceFragment(simpleReportFragmentwithDateFilter);

                    bundle.putSerializable("frag", stockSearchFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);

                } else if (position == 5) {
                    Bundle                             bundle               = new Bundle();
                    SimpleReportFragmentWithTwoFilters simpleReportFragment = new SimpleReportFragmentWithTwoFilters();
                    bundle.putString("reportType", getString(R.string.stock_balance_eng));
                    simpleReportFragment.setArguments(bundle);
                    //  MainActivity.replaceFragment(simpleReportFragment);


                    bundle.putSerializable("frag", simpleReportFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                }
            }
        });

    }

    private void configRecyclerView_Outstanding() {


        String receivable             = ThemeUtil.getString(context, R.attr.receivable);
        String payable                = ThemeUtil.getString(context, R.attr.payable);
        String customerPaymentHistory = ThemeUtil.getString(context, R.attr.customer_payment_history);
        String supplierPaymentHistory = ThemeUtil.getString(context, R.attr.supplier_payment_history);


        String receivableDes             = ThemeUtil.getString(context, R.attr.receivable_description);
        String payableDes                = ThemeUtil.getString(context, R.attr.payable_description);
        String customerPaymentHistoryDes = ThemeUtil.getString(context, R.attr.customer_payment_history_description);
        String supplierPaymentHistoryDes = ThemeUtil.getString(context, R.attr.supplier_payment_history_description);

        //For Outstanding Reports List
        List<String> outstandingReportsList = new ArrayList<>();
        outstandingReportsList.add(receivable);
        outstandingReportsList.add(payable);
        outstandingReportsList.add(customerPaymentHistory);
        outstandingReportsList.add(supplierPaymentHistory);

        List<String> outstandingDesReportsList = new ArrayList<>();
        outstandingDesReportsList.add(receivableDes);
        outstandingDesReportsList.add(payableDes);
        outstandingDesReportsList.add(customerPaymentHistoryDes);
        outstandingDesReportsList.add(supplierPaymentHistoryDes);

        TypedArray outstandingIcon = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_outstanding});


        rvAdapterForReportList = new RVAdapterForReportItemList(outstandingIcon.getDrawable(0), outstandingReportsList, outstandingDesReportsList);

        report_rv.setLayoutManager(new LinearLayoutManager(context));

        report_rv.setAdapter(rvAdapterForReportList);

        rvAdapterForReportList.setmItemClickListener(new RVAdapterForReportItemList.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position == 0) {
                    //Go to Receivable Report
                    Bundle                                   bundle               = new Bundle();
                    SimpleReportFragmentForOutstandingFilter simpleReportFragment = new SimpleReportFragmentForOutstandingFilter();
                    bundle.putString("reportType", "receivable header");
                    simpleReportFragment.setArguments(bundle);
                    // MainActivity.replaceFragment(simpleReportFragment);

                    bundle.putSerializable("frag", simpleReportFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 1) {
                    //Go to Payable Report
                    Bundle                                   bundle               = new Bundle();
                    SimpleReportFragmentForOutstandingFilter simpleReportFragment = new SimpleReportFragmentForOutstandingFilter();
                    bundle.putString("reportType", "payable header");
                    simpleReportFragment.setArguments(bundle);
                    // MainActivity.replaceFragment(simpleReportFragment);

                    bundle.putSerializable("frag", simpleReportFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 2) {
                    //Go to Customer Payment History Report

                    Bundle                             bundle                                   = new Bundle();
                    CustomerListFragmentWithSearchView simpleReportFragmentWithDateFilterSearch = new CustomerListFragmentWithSearchView();
                    bundle.putString("reportType", "customer payment history");
                    simpleReportFragmentWithDateFilterSearch.setArguments(bundle);
                    //  MainActivity.replaceFragment(simpleReportFragmentWithDateFilterSearch);
                    bundle.putSerializable("frag", simpleReportFragmentWithDateFilterSearch);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 3) {
                    //Go to Supplier Payment History Report

                    Bundle                                     bundle                                   = new Bundle();
                    SupplierListFragmentFragmentWithSearchView simpleReportFragmentWithDateFilterSearch = new SupplierListFragmentFragmentWithSearchView();
                    bundle.putString("reportType", "supplier payment history");
                    simpleReportFragmentWithDateFilterSearch.setArguments(bundle);
                    //  MainActivity.replaceFragment(simpleReportFragmentWithDateFilterSearch);

                    bundle.putSerializable("frag", simpleReportFragmentWithDateFilterSearch);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                }
            }
        });

    }

    private void configRecyclerView_Financial() {

        String monthlyIncome             = ThemeUtil.getString(context, R.attr.monthly_income);
        String monthlyExpense            = ThemeUtil.getString(context, R.attr.monthly_expense);
        String profitOrLost              = ThemeUtil.getString(context, R.attr.profit_or_lost);
        String comparsionOfIncomeExpense = ThemeUtil.getString(context, R.attr.comparison_of_income_expense);
        String topIncomeExpense          = ThemeUtil.getString(context, R.attr.top_income_expense);
        String salesTax                  = ThemeUtil.getString(context, R.attr.sales_tax_report);

        String monthlyIncomeDes             = ThemeUtil.getString(context, R.attr.monthly_income_description);
        String monthlyExpenseDes            = ThemeUtil.getString(context, R.attr.monthly_expense_description);
        String profitOrLostDes              = ThemeUtil.getString(context, R.attr.profit_or_lost_description);
        String comparsionOfIncomeExpenseDes = ThemeUtil.getString(context, R.attr.comparison_of_income_expense_description);
        String topIncomeExpenseDes          = ThemeUtil.getString(context, R.attr.top_income_expense_desc);
        String salesTaxDesc                 = ThemeUtil.getString(context, R.attr.sales_tax_report_desc);

        //For Financial Reports List
        List<String> financialReportsList = new ArrayList<>();
        financialReportsList.add(monthlyIncome);
        financialReportsList.add(monthlyExpense);
        financialReportsList.add(profitOrLost);
        financialReportsList.add(comparsionOfIncomeExpense);
        financialReportsList.add(topIncomeExpense);
        financialReportsList.add(salesTax);

        List<String> financialDesReportsList = new ArrayList<>();
        financialDesReportsList.add(monthlyIncomeDes);
        financialDesReportsList.add(monthlyExpenseDes);
        financialDesReportsList.add(profitOrLostDes);
        financialDesReportsList.add(comparsionOfIncomeExpenseDes);
        financialDesReportsList.add(topIncomeExpenseDes);
        financialDesReportsList.add(salesTaxDesc);

        TypedArray finacialIcon = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_financial});
        Log.e("des", topIncomeExpense);

        rvAdapterForReportList = new RVAdapterForReportItemList(finacialIcon.getDrawable(0), financialReportsList, financialDesReportsList);

        report_rv.setLayoutManager(new LinearLayoutManager(context));

        report_rv.setAdapter(rvAdapterForReportList);

        rvAdapterForReportList.setmItemClickListener(new RVAdapterForReportItemList.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position == 0) {
                    //Go to Profit and Lost

                    Bundle            bundle            = new Bundle();
                    LineChartFragment lineChartFragment = new LineChartFragment();
                    bundle.putString("reportType", ExpenseIncome.Type.Income.toString());
                    lineChartFragment.setArguments(bundle);
                    // MainActivity.replaceFragment(barChartWithYearViewFragment);

                    bundle.putSerializable("frag", lineChartFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 1) {
                    //Go to Income
                    Bundle            bundle            = new Bundle();
                    LineChartFragment lineChartFragment = new LineChartFragment();
                    bundle.putString("reportType", ExpenseIncome.Type.Expense.toString());
                    lineChartFragment.setArguments(bundle);
                    //   MainActivity.replaceFragment(barChartWithYearViewFragment);
                    bundle.putSerializable("frag", lineChartFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);

                } else if (position == 2) {
                    //Go to Expense

                    Bundle bundle = new Bundle();
                    Log.e("profit", "loss0");
                    ProfitAndLossFragment profitAndLossFragment = new ProfitAndLossFragment();
                    //  MainActivity.replaceFragment(profitAndLossFragment);
                    bundle.putSerializable("frag", profitAndLossFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 3) {
                    //Go to Comparison of Income and Expense

                    Bundle                      bundle                      = new Bundle();
                    BarChartWithTwoBarsFragment barChartWithTwoBarsFragment = new BarChartWithTwoBarsFragment();
                    bundle.putString("reportType", "comparison of income and expense");
                    barChartWithTwoBarsFragment.setArguments(bundle);
                    // MainActivity.replaceFragment(barChartWithTwoBarsFragment);

                    bundle.putSerializable("frag", barChartWithTwoBarsFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 4) {
                    Bundle                         bundle                         = new Bundle();
                    BarChartFragmentWithTwoFilters barChartFragmentWithTwoFilters = new BarChartFragmentWithTwoFilters();
                    bundle.putString("reportType", "top amounts expense manager");
                    barChartFragmentWithTwoFilters.setArguments(bundle);
                    // MainActivity.replaceFragment(barChartWithTwoBarsFragment);

                    bundle.putSerializable("frag", barChartFragmentWithTwoFilters);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 5) {

                    Bundle                       bundle                       = new Bundle();
                    BarChartWithYearViewFragment barChartWithYearViewFragment = new BarChartWithYearViewFragment();
                    bundle.putString("reportType", "sales tax report");
                    barChartWithYearViewFragment.setArguments(bundle);
                    // MainActivity.replaceFragment(barChartWithTwoBarsFragment);

                    bundle.putSerializable("frag", barChartWithYearViewFragment);

                    Intent detailIntent = new Intent(context, DetailActivity.class);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                }
            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }
}
