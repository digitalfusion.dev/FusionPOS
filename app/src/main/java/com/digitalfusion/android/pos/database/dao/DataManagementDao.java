package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.util.Log;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.util.AppConstant;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by MD002 on 10/28/16.
 */

public class DataManagementDao extends ParentDAO {

    private static ParentDAO dataManagementDaoInstance;

    private Context context;

    private DataManagementDao(Context context) {
        super(context);
        this.context = context;
    }

    public static DataManagementDao getDataManagementDaoInstance(Context context) {
        if (dataManagementDaoInstance == null) {
            dataManagementDaoInstance = new DataManagementDao(context);
        }
        return (DataManagementDao) dataManagementDaoInstance;
    }

    public File backup(String dir) {
        File backupFilePath = null;

        if (isExternalStorageWritable()) {
            //String outFileName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getParent() + "/inventory_backup.db";
            String inventoryDb     = AppConstant.DATABASE_NAME;

            String zipFileName = "inventory_backup.zip";
            if (AppConstant.SHARED_PREFERENCE_DB.equalsIgnoreCase(AppConstant.DATABASE_NAME)) {
                inventoryDb = AppConstant.DATABASE_NAME;
            } else if (AppConstant.SHARED_PREFERENCE_DB.equalsIgnoreCase(AppConstant.SAMPLE_DATABASE_NAME)) {
                inventoryDb = AppConstant.SAMPLE_DATABASE_NAME;
            }

            final String inInventoryDb = context.getFilesDir().getParent() + "/databases/" + AppConstant.SHARED_PREFERENCE_DB;
            final String inApiDb       = context.getFilesDir().getParent() + "/databases/" + AppConstant.API_DATABASE;

            //region inventory copy
            File inventoryDbFile = new File(inInventoryDb);
            File inventoryDbOutFile = new File(dir, inventoryDb);
            if (!inventoryDbFile.exists()) {
                inventoryDbFile.mkdir();
            }

            try {
                FileInputStream  fis = new FileInputStream(inventoryDbFile);
                FileOutputStream output;

                // Open the empty db as the output stream
                output = new FileOutputStream(inventoryDbOutFile);

                // Transfer bytes from the inputfile to the outputfile
                byte[] buffer = new byte[1024];
                int    length;
                while ((length = fis.read(buffer)) > 0) {
                    output.write(buffer, 0, length);
                }
                //endregion

                //region api copy
                File apiDbFile    = new File(inApiDb);
                File apiDbOutFile = new File(dir, AppConstant.API_DATABASE);

                fis = new FileInputStream(apiDbFile);

                // Open the empty db as the output stream
                output = new FileOutputStream(apiDbOutFile);

                while ((length = fis.read(buffer)) > 0) {
                    output.write(buffer, 0, length);
                }
                //endregion

                cipher(Cipher.ENCRYPT_MODE, context.getString(R.string.title_name), inventoryDbOutFile, inventoryDbOutFile);
                cipher(Cipher.ENCRYPT_MODE, context.getString(R.string.title_name), apiDbOutFile, apiDbOutFile);


                ZipFile zipFile = new ZipFile(dir + "/" + zipFileName);
                if (zipFile.getFile().exists()) {
                    zipFile.getFile().delete();
                }
                ZipParameters parameters = new ZipParameters();
                parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
                parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

                //Set the encryption flag to true
                parameters.setEncryptFiles(true);

                //Set the encryption method to AES Zip Encryption
                parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
                parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
                parameters.setPassword(context.getString(R.string.title_name) + ";");

                zipFile.addFile(inventoryDbOutFile, parameters);
                zipFile.addFile(apiDbOutFile, parameters);

                backupFilePath = zipFile.getFile();


                // Close the streams
                output.flush();
                output.close();
                fis.close();

                inventoryDbOutFile.delete();
                apiDbOutFile.delete();
            } catch (IOException e) {
                return null;
            } catch (ZipException e) {
                e.printStackTrace();
            }
        }
        return backupFilePath;
    }

    private void cipher(int cipherMode, String key, File inputFile, File outputFile) {
        try {
            SecretKey secretKey = new SecretKeySpec(key.getBytes(), context.getString(R.string.algorithm));
            Cipher    cipher    = Cipher.getInstance(context.getString(R.string.algorithm));
            cipher.init(cipherMode, secretKey);

            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[]          inputBytes  = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);

            byte[] outputBytes = cipher.doFinal(inputBytes);

            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);

            inputStream.close();
            outputStream.close();

        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException e) {
            e.printStackTrace();
        }
    }

//    public boolean temporaryBackup(String dir) throws IOException {
//
//        if (isExternalStorageWritable()) {
//            //String outFileName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getParent() + "/inventory_backup.db";
//            String outFile     = "fusion_inventory_backup.sqlite";
//            String zipFileName = "fusion_inventory_backup.zip";
//            //            if (AppConstant.SHARED_PREFERENCE_DB.equalsIgnoreCase(AppConstant.DATABASE_NAME)){
//            //                outFile = "inventory_backup.sqlite";
//            //                zipFileName = "inventory_backup.zip";
//            //            } else if (AppConstant.SHARED_PREFERENCE_DB.equalsIgnoreCase(AppConstant.SAMPLE_DATABASE_NAME)){
//            //                outFile = "sample_inventory_backup.sqlite";
//            //                zipFileName = "sample_inventory_backup.zip";
//            //            }
//            // Log.e("jj",outFile);
//            final String inFileName = context.getFilesDir().getParent() + "/databases/" + AppConstant.SHARED_PREFERENCE_DB;
//
//            //final String inFileName = getDatabasePath()+"BusinessSuite.sqlite" ;
//            //Log.e("iiii",inFileName);
//            File dbFile = new File(inFileName);
//            File bfFile = new File(dir, outFile);
//            if (!dbFile.exists()) {
//                Log.e("not", "exist");
//                dbFile.mkdir();
//            }
//
//            Log.e("dir", bfFile.getAbsolutePath());
//            FileInputStream fis   = new FileInputStream(dbFile);
//            String          state = Environment.getExternalStorageState();
//
//            FileOutputStream output;
//            // Open the empty db as the output stream
//
//            try {
//                output = new FileOutputStream(bfFile);
//                //Log.e("path",outFile);
//            } catch (FileNotFoundException ex) {
//                ex.printStackTrace();
//                return false;
//
//            }
//            // Transfer bytes from the inputfile to the outputfile
//            byte[] buffer = new byte[1024];
//            int    length;
//            while ((length = fis.read(buffer)) > 0) {
//                output.write(buffer, 0, length);
//            }
//            // Close the streams
//
//            cipher(Cipher.ENCRYPT_MODE, context.getString(R.string.title_name), bfFile, bfFile);
//            try {
//                ZipFile       zipFile    = new ZipFile(dir + "/" + zipFileName);
//                ZipParameters parameters = new ZipParameters();
//                parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
//                parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
//
//                //Set the encryption flag to true
//                parameters.setEncryptFiles(true);
//
//                //Set the encryption method to AES Zip Encryption
//                parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
//                parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
//                parameters.setPassword(context.getString(R.string.title_name) + ";");
//
//                zipFile.addFile(bfFile, parameters);
//
//
//            } catch (ZipException e) {
//                e.printStackTrace();
//            }
//
//            output.flush();
//            output.close();
//            fis.close();
//
//            bfFile.delete();
//
//        } else {
//            Log.e("AH", "ah");
//        }
//        return true;
//    }


    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    private String getDatabasePath() {
        Log.e("db", "path");
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            Log.e("oaeioa ", context.getApplicationInfo().dataDir.replaceFirst("/" + context.getPackageName(), "") + "/");
            return context.getApplicationInfo().dataDir.replaceFirst("/" + context.getPackageName(), "");
        } else {
            return Environment.getExternalStorageDirectory().getAbsolutePath();
        }
    }


    public boolean restore(String dir) {
        return databaseHelper.restore(dir).equalsIgnoreCase("success");
    }

    public boolean clearTransactionData() {
        databaseWriteTransaction(flag);

        Log.w("IN clear", "Clear transaction database");
        try {
            truncateData(AppConstant.SALES_DETAIL_TABLE_NAME);
            truncateData(AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME);
            truncateData(AppConstant.DELIVERY_TABLE_NAME);
            truncateData(AppConstant.REFUND_TABLE_NAME);
            truncateData(AppConstant.SALES_TABLE_NAME);
            truncateData(AppConstant.PURCHASE_DETAIL_TABLE_NAME);
            truncateData(AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME);
            truncateData(AppConstant.PURCHASE_TABLE_NAME);
            truncateData(AppConstant.EXPENSE_TABLE_NAME);
            truncateData(AppConstant.DAMAGE_TABLE_NAME);
            truncateData(AppConstant.LOST_TABLE_NAME);
            database.setTransactionSuccessful();
        } catch (SQLiteException s) {
            s.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean clearAllData() {
        return databaseHelper.clearAllData();
    }

    private boolean truncateData(String tableName) {
        query = "delete from " + tableName;
        database.execSQL(query);
        query = "update " + AppConstant.ID_GENERATOR_TABLE_NAME + " set " + AppConstant.ID_GENERATOR_VALUE + " = 0 " + " where " + AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + " = ?";
        database.execSQL(query, new String[]{tableName});
        return true;
    }


   /* public boolean clearDatabase(){
        databaseWriteTransaction(flag);
        try {

        }
        String sql="DROP TABLE IF EXISTS "+ AppConstant.VEHICLE_TABLE_NAME;
        db.execSQL(sql);
        sql="DROP TABLE IF EXISTS "+ AppConstant.PART_TABLE_NAME;
        db.execSQL(sql);
        sql="DROP TABLE IF EXISTS "+ AppConstant.SERVICE_TABLE_NAME;
        db.execSQL(sql);
        sql="DROP TABLE IF EXISTS "+ AppConstant.HISTORY_TABLE_NAME;
        db.execSQL(sql);
        sql="DROP TABLE IF EXISTS "+ AppConstant.PART_HISTORY_TABLE_NAME;
        db.execSQL(sql);
        db.execSQL("DROP TABLE IF EXISTS " + AppConstant.SERVICE_HISTORY_TABLE_NAME);
        onCreate(db);
        return true;
    }*/
}
