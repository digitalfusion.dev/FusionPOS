package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForReportTitleList;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.database.model.ReportTitleItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD001 on 3/6/17.
 */

public class ReportTitleListFragment extends Fragment implements Serializable {

    private View mainLayout;

    private Context context;

    private RecyclerView reporttitle_rv;

    private RVAdapterForReportTitleList rvAdapterForReportTitleList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.report_title_list, null);
        context = getContext();
        String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.reports}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);

        reporttitle_rv = (RecyclerView) mainLayout.findViewById(R.id.report_title);

        rvAdapterForReportTitleList = new RVAdapterForReportTitleList(getList());

        reporttitle_rv.setLayoutManager(new LinearLayoutManager(context));

        reporttitle_rv.setAdapter(rvAdapterForReportTitleList);

        settingClickListener();

        return mainLayout;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void settingClickListener() {
        rvAdapterForReportTitleList.setmItemClickListener(new RVAdapterForReportTitleList.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                ReportTypeReportFragment reportTypeReportFragment = new ReportTypeReportFragment();
                Bundle                   bundle                   = new Bundle();

                bundle.putInt("type", position);


                Intent detailIntent = new Intent(context, DetailActivity.class);


                if (position == 0) {
                    //Go to sale report list
                  /*  Bundle bundle = new Bundle();
                    bundle.putInt("type",0);
                    reportTypeReportFragment.setArguments(bundle);
                    MainActivity.replacingFragment(reportTypeReportFragment);*/

                    detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.SALE_REPORT);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);

                } else if (position == 1) {
                    //Go to sale report list
                  /*  Bundle bundle = new Bundle();
                    bundle.putInt("type",1);
                    reportTypeReportFragment.setArguments(bundle);
                    MainActivity.replacingFragment(reportTypeReportFragment);*/

                    detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.PURCHASE_REPORT);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 2) {
                    //Go to sale report list
                  /*  Bundle bundle = new Bundle();
                    bundle.putInt("type",2);
                    reportTypeReportFragment.setArguments(bundle);
                    MainActivity.replacingFragment(reportTypeReportFragment);*/
                    detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.INVENTORY_REPORT);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 3) {
                    //Go to sale report list
                   /* Bundle bundle = new Bundle();
                    bundle.putInt("type",3);
                    reportTypeReportFragment.setArguments(bundle);
                    MainActivity.replacingFragment(reportTypeReportFragment);*/
                    detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.OUTSTANDING_REPORT);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                } else if (position == 4) {
                    //Go to sale report list
                   /* Bundle bundle = new Bundle();
                    bundle.putInt("type",4);
                    reportTypeReportFragment.setArguments(bundle);
                    MainActivity.replacingFragment(reportTypeReportFragment);*/
                    detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.FINANCAIL_REPORT);

                    detailIntent.putExtras(bundle);

                    startActivity(detailIntent);
                }
            }
        });
    }

    private List<ReportTitleItem> getList() {
        List<ReportTitleItem> titleList = new ArrayList<>();

        String sale        = context.getTheme().obtainStyledAttributes(new int[]{R.attr.sale_report}).getString(0);
        String purchase    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.purchase_report}).getString(0);
        String inventory   = context.getTheme().obtainStyledAttributes(new int[]{R.attr.inventory_report}).getString(0);
        String outstanding = context.getTheme().obtainStyledAttributes(new int[]{R.attr.outstanding_report}).getString(0);
        String financial   = context.getTheme().obtainStyledAttributes(new int[]{R.attr.financial_report}).getString(0);

        TypedArray saleIcon        = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_sale});
        TypedArray purchaseIcon    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_purchase});
        TypedArray inventoryIcon   = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_inventory});
        TypedArray outstandingIcon = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_outstanding});
        TypedArray finacialIcon    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ic_financial});


        titleList.add(new ReportTitleItem(saleIcon.getDrawable(0), sale));
        titleList.add(new ReportTitleItem(purchaseIcon.getDrawable(0), purchase));
        titleList.add(new ReportTitleItem(inventoryIcon.getDrawable(0), inventory));
        titleList.add(new ReportTitleItem(outstandingIcon.getDrawable(0), outstanding));
        titleList.add(new ReportTitleItem(finacialIcon.getDrawable(0), financial));
        return titleList;
    }
}
