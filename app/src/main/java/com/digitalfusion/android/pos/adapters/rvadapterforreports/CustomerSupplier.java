package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import com.digitalfusion.android.pos.database.model.Supplier;
import com.digitalfusion.android.pos.database.model.Customer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 10/24/17.
 */

public class CustomerSupplier {

    private Long id;

    private String name;

    public CustomerSupplier() {
    }


    public CustomerSupplier(Customer customer) {
        this.id = customer.getId();
        this.name = customer.getName();

    }

    public CustomerSupplier(Supplier supplier) {
        this.id = supplier.getId();
        this.name = supplier.getName();

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CustomerSupplier> wrapCustomer(List<Customer> customerList) {

        List<CustomerSupplier> customerSuppliers = new ArrayList<>();

        for (Customer customer : customerList) {
            customerSuppliers.add(new CustomerSupplier(customer));
        }
        return customerSuppliers;

    }

    public List<Customer> wrapAsCustomerVO(List<CustomerSupplier> customerSupplierList) {

        List<Customer> customers = new ArrayList<>();

        for (CustomerSupplier customerSupplier : customerSupplierList) {
            customers.add(new Customer(customerSupplier.getId(), customerSupplier.getName()));
        }
        return customers;
    }

    public List<CustomerSupplier> wrapSupplier(List<Supplier> supplierList) {

        List<CustomerSupplier> customerSuppliers = new ArrayList<>();

        for (Supplier supplier : supplierList) {
            customerSuppliers.add(new CustomerSupplier(supplier));
        }
        return customerSuppliers;

    }
}
