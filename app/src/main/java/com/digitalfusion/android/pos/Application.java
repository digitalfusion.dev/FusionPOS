package com.digitalfusion.android.pos;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.digitalfusion.android.pos.util.AidlUtil;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.POSUtil;
import com.facebook.stetho.Stetho;
import com.innovattic.font.TypefaceManager;

import io.fabric.sdk.android.Fabric;

/**
 * Created by MD003 on 11/23/16.
 */
public class Application extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initializeWithDefaults(this);
        if (!BuildConfig.DEBUG) {
            // do something for a debug build
            Fabric.with(this, new Crashlytics());
        }

        System.setProperty("org.apache.poi.javax.xml.stream.XMLInputFactory", "com.fasterxml.aalto.stax.InputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLOutputFactory", "com.fasterxml.aalto.stax.OutputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLEventFactory", "com.fasterxml.aalto.stax.EventFactoryImpl");
        setTheme(POSUtil.getDefaultThemeNoActionBar(this));

        if (!POSUtil.isEmbended(this)) {

            TypefaceManager.initialize(this, R.xml.nofonts);
        } else {
            TypefaceManager.initialize(this, R.xml.fonts);
        }

        String[] s = POSUtil.getKeys(this);

        AppConstant.url = s[0];
        AppConstant.token_name = s[1];
        AppConstant.token = s[2];
        AppConstant.user_name = s[3];
        AppConstant.password = s[4];
        Log.d("Application", "Url: " + s[0] + "\ntoken_name: " + s[1] + "\nuser_name: " + s[3] + "\npassword: " + s[4]);
        AidlUtil.getInstance().connectPrinterService(this);
        saveOnSharedPreference();
    }

    @Override
    protected void attachBaseContext(Context base) {

        try {
            super.attachBaseContext(base);
        } catch (Exception ignored) {
            // Multidex support doesn't play well with Robolectric yet
        }
    }

    private void saveOnSharedPreference() {
        SharedPreferences sharedPref  = getSharedPreferences("database", Context.MODE_PRIVATE);
        String            temp_header = sharedPref.getString("database", "db");
        Log.e("temp", temp_header);
        if (temp_header == "db") {
            Log.e("db", "dbd");
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("database", AppConstant.DATABASE_NAME);
            editor.apply();
            editor.commit();
        } else {
            Log.e("tmp", "dbd " + temp_header);
            AppConstant.SHARED_PREFERENCE_DB = temp_header;
        }
        Log.e(AppConstant.SHARED_PREFERENCE_DB, "sharedp");

        /*SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        Map<String, ?> entries = preferences.getAll();
        Set<String> keys = entries.keySet();
        for (String key: keys){
            Log.e("key", key);
        }*/
    }

}
