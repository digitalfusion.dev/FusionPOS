package com.digitalfusion.android.pos.database.model;

import java.io.Serializable;

/**
 * Created by MD002 on 8/17/16.
 */
public class IdGenerator implements Serializable {
    private Long id;
    private String tableName;
    private Long value;

    public IdGenerator(Long id, String tableName, Long value) {
        this.id = id;
        this.tableName = tableName;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
