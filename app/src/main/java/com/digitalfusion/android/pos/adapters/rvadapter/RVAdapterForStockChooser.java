package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.StockItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 1/11/18.
 */

public class RVAdapterForStockChooser extends RecyclerView.Adapter<RVAdapterForStockChooser.FilterViewHolder> {


    List<StockItem> filterNameList;

    private RVAdapterForStockChooser.OnItemClickListener mItemClickListener;

    private RadioButton oldRb;

    private int currentPos;

    private int selectPos;


    public RVAdapterForStockChooser() {
        this.filterNameList = new ArrayList<>();
    }

    public RVAdapterForStockChooser(List<StockItem> filterNameList) {
        this.filterNameList = filterNameList;
    }

    @Override
    public RVAdapterForStockChooser.FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_item_view, parent, false);

        return new RVAdapterForStockChooser.FilterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RVAdapterForStockChooser.FilterViewHolder holder, final int position) {

        holder.name.setText(filterNameList.get(position).getName());
        if (position == currentPos && currentPos != -1) {
            oldRb = holder.rb;
            holder.rb.setChecked(true);
        } else {
            holder.rb.setChecked(false);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectPos = position;
                if (mItemClickListener != null) {

                    if (oldRb != null) {
                        oldRb.setChecked(false);
                        oldRb = holder.rb;
                        holder.rb.setChecked(true);
                    }

                    mItemClickListener.onItemClick(v, position);
                }
            }

        });
    }

    public List<StockItem> getFilterNameList() {
        return filterNameList;
    }

    public void setFilterNameList(List<StockItem> filterNameList) {
        this.filterNameList = filterNameList;
    }

    public StockItem getSelectedStock() {
        return filterNameList.get(selectPos);
    }


    @Override
    public int getItemCount() {
        return filterNameList.size();
    }

    public RVAdapterForStockChooser.OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(RVAdapterForStockChooser.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public int getCurrentPos() {
        return currentPos;
    }

    public void setCurrentPos(int currentPos) {
        if (currentPos < 0 && currentPos > filterNameList.size() - 1) {
            //throw new Exception("Current Position can't be greater than array size or smaller than 0");
        } else {
            this.currentPos = currentPos;
            notifyDataSetChanged();
            Log.w("in adapter", "click");
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(null, currentPos);
            }
        }

    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class FilterViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        RadioButton rb;
        View itemView;

        public FilterViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            rb = (RadioButton) itemView.findViewById(R.id.rb);
            name = (TextView) itemView.findViewById(R.id.name);


        }
    }
}