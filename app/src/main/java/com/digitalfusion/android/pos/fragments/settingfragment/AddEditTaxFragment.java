package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.model.Tax;
import com.digitalfusion.android.pos.views.FusionToast;

/**
 * Created by MD003 on 3/8/17.
 */

public class AddEditTaxFragment extends Fragment {


    public static final String KEY = "tax";
    public Tax tax;
    public boolean isEdit = false;
    private EditText taxNameTextInputEditText;
    private EditText taxRateTextInputEditText;
    private EditText taxDescTextInputEditText;
    private RadioGroup taxTypeRadioGroup;
    private RadioButton inclusive;
    private RadioButton exclusive;
    private CheckBox useDefaultCheckBox;
    private SettingManager settingManager;
    private View mainLayoutView;
    private boolean isInclusive = true;

    private boolean isDefault = false;

    //For value
    private String taxName;

    private Double taxRate;

    private String taxDesc;

    private String taxType = "Inclusive";

    private Integer useDefault;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.add_tax, container, false);

        settingManager = new SettingManager(getContext());

        setHasOptionsMenu(true);

        loadUI();

        context = getContext();
        useDefaultCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                isDefault = isChecked;

            }
        });

        if (getArguments().getSerializable(KEY) == null) {
            String newTax = context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_tax}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(newTax);
            isEdit = false;
        } else {
            String editTax = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_tax}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(editTax);
            isEdit = true;
            tax = (Tax) getArguments().get(KEY);
            initializeOldData();
        }


        return mainLayoutView;

    }

    @Override
    public void onResume() {
        super.onResume();


        useDefaultCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                isDefault = isChecked;
                if (isChecked) {
                    useDefault = 1;
                } else {
                    useDefault = 0;
                }
            }
        });
        taxTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                isInclusive = inclusive.getId() == checkedId;

            }
        });

    }

    private void takeValueFromView() {

        taxName = taxNameTextInputEditText.getText().toString().trim();

        taxRate = Double.parseDouble(taxRateTextInputEditText.getText().toString().trim());

        taxDesc = taxDescTextInputEditText.getText().toString().trim();

        if (isInclusive) {

            taxType = "Inclusive";

        } else {

            taxType = "Exclusive";

        }

        if (isDefault) {

            useDefault = 1;

        } else {

            useDefault = 0;

        }

    }

    private void initializeOldData() {


        taxName = tax.getName();
        taxDesc = tax.getDescription();
        taxRate = tax.getRate();

        taxNameTextInputEditText.setText(taxName);
        taxDescTextInputEditText.setText(taxDesc);
        taxRateTextInputEditText.setText(tax.getRateString());
        useDefaultCheckBox.setChecked(tax.isActive());

        if (tax.getType().equalsIgnoreCase("Inclusive")) {
            inclusive.setChecked(true);
        } else {
            exclusive.setChecked(true);
        }
    }

    private void clearData() {


        taxNameTextInputEditText.setText(null);
        taxNameTextInputEditText.setError(null);
        taxRateTextInputEditText.setText(null);
        taxRateTextInputEditText.setError(null);
        taxDescTextInputEditText.setText(null);
        taxDescTextInputEditText.setError(null);
        useDefaultCheckBox.setChecked(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save) {

            if (!isEdit) {
                saveCurrency();
            } else {

                if (checkValidation()) {
                    takeValueFromView();

                    Log.w("use default", useDefault + " SS");

                    settingManager.updateTax(taxName, taxRate, taxType, taxDesc, useDefault, tax.getId());
                    getActivity().onBackPressed();
                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                }
            }

        }
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void saveCurrency() {
        if (checkValidation()) {

            takeValueFromView();

            settingManager.addNewTax(taxName, taxRate, taxType, taxDesc, useDefault);

            clearData();

            getActivity().onBackPressed();

            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);
     /*   TypedArray save=context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        FontTextView fontTextView= (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.menu_text,null);

        menu.getItem(0).setActionView(fontTextView);

        fontTextView.setText(save.getString(0));


        fontTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menu.getItem(0));
            }
        });
*/


        super.onCreateOptionsMenu(menu, inflater);

    }

    private boolean checkValidation() {

        boolean status = true;

        if (taxNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;

            String taxName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_tax_name}).getString(0);
            taxNameTextInputEditText.setError(taxName);

        }

        if (taxRateTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;

            String taxRate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_tax_rate}).getString(0);
            taxRateTextInputEditText.setError(taxRate);

        }

        return status;

    }

    private void loadUI() {
        taxNameTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.tax_name_in_add_tax_et);

        taxRateTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.tax_rate_in_add_tax_et);

        taxDescTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.tax_desc_in_add_tax_et);


        taxTypeRadioGroup = (RadioGroup) mainLayoutView.findViewById(R.id.tax_type_in_add_tax_rg);

        inclusive = (RadioButton) mainLayoutView.findViewById(R.id.inclusive);

        exclusive = (RadioButton) mainLayoutView.findViewById(R.id.exclusive);

        useDefaultCheckBox = (CheckBox) mainLayoutView.findViewById(R.id.use_default_in_add_tax);


    }


}
