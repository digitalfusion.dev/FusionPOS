package com.digitalfusion.android.pos.util;

import android.content.Context;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ExpenseIncome;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 4/7/17.
 */

public class ExpenseManagerUtil {


    public static List<FilterName> getFilterNameList(Context context) {

        String all = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);

        String incomeStr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.income}).getString(0);

        String expenseStr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.expense}).getString(0);

        List<FilterName> filterNames = new ArrayList<>();

        filterNames.add(new FilterName(all, ExpenseIncome.Type.All));

        filterNames.add(new FilterName(expenseStr, ExpenseIncome.Type.Expense));

        filterNames.add(new FilterName(incomeStr, ExpenseIncome.Type.Income));


        return filterNames;


    }

    public static class FilterName {

        String name;
        ExpenseIncome.Type type;

        public FilterName(String name, ExpenseIncome.Type type) {
            this.name = name;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ExpenseIncome.Type getType() {
            return type;
        }

        public void setType(ExpenseIncome.Type type) {
            this.type = type;
        }
    }

}
