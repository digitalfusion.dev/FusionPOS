package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.database.dao.UserDAO;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/18/16.
 *
 * NOTE: This class even properly implemented or used. Please implement required
 * methods in here and use this class instead of DAO.
 * ---
 * lyx
 * ---
 */
public class UserManager {
    private Context context;
    private ApiDAO apiDAO;
    private List<User> userList;

    public UserManager(Context context) {
        this.context = context;
        apiDAO = ApiDAO.getApiDAOInstance(context);
        userList = new ArrayList<>();
    }

    public List<User> getAllUserRoles() {
        return apiDAO.getAllUserRoles();
    }
}
