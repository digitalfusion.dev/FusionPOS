package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.StockItem;

import java.util.List;

/**
 * Created by MD002 on 10/25/17.
 */

public class StockSearchAdapter extends ParentRVAdapterForReports {

    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    protected boolean showLoader = false;
    private List<StockItem> stockItemList;
    private LoaderViewHolder loaderViewHolder;
    private ClickListener clickListener;

    public StockSearchAdapter(List<StockItem> stockItemList) {

        this.stockItemList = stockItemList;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            if (stockItemList != null) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;
            }

        }

        return VIEWTYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEWTYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_stock_search_item_view, parent, false);


            return new StockItemViewHolder(v);

        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            return new LoaderViewHolder(v);
        }
    }

    public List<StockItem> getStockItemList() {
        return stockItemList;
    }

    public void setStockItemList(List<StockItem> stockItemList) {
        this.stockItemList = stockItemList;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof LoaderViewHolder) {

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;

            this.loaderViewHolder = loaderViewHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);

        }
        if (viewHolder instanceof StockItemViewHolder) {

            final StockItemViewHolder holder = (StockItemViewHolder) viewHolder;

            holder.stockCodeTextView.setText(stockItemList.get(position).getCodeNo());
            holder.itemNameTextView.setText(stockItemList.get(position).getName());
            holder.categoryTextView.setText(stockItemList.get(position).getCategoryName());
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onClick(position);
                }
            });

        } else {
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public int getItemCount() {
        if (stockItemList == null || stockItemList.size() == 0) {
            return 0;
        } else {
            return stockItemList.size() + 1;
        }

    }

    public void setShowLoader(boolean showLoader) {
        this.showLoader = showLoader;

        if (loaderViewHolder != null) {
            Log.w("hele", "loader full not null");
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }

        }
    }

    public class StockItemViewHolder extends RecyclerView.ViewHolder {
        TextView stockCodeTextView;
        TextView itemNameTextView;
        TextView categoryTextView;
        LinearLayout linearLayout;
        View itemView;

        public StockItemViewHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;
            stockCodeTextView = (TextView) itemView.findViewById(R.id.stock_code_in_stock_item_view);
            itemNameTextView = (TextView) itemView.findViewById(R.id.item_name_in_stock_item_view);
            categoryTextView = (TextView) itemView.findViewById(R.id.category_in_stock_item_view);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linear_layout);
        }
    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar mProgressBar;

        public View mView;

        public LoaderViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);

        }
    }
}
