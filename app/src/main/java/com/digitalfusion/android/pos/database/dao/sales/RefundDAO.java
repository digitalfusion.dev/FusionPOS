package com.digitalfusion.android.pos.database.dao.sales;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import com.digitalfusion.android.pos.database.dao.ParentDAO;
import com.digitalfusion.android.pos.database.dao.IdGeneratorDAO;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;

/**
 * Created by MD002 on 12/21/16.
 */

public class RefundDAO extends ParentDAO {
    private static ParentDAO refundDaoInstance;
    private Context context;
    private IdGeneratorDAO idGeneratorDAO;

    private RefundDAO(Context context) {
        super(context);
        this.context = context;
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
    }

    public static RefundDAO getRefundDaoInstance(Context context) {
        if (refundDaoInstance == null) {
            refundDaoInstance = new RefundDAO(context);
        }
        return (RefundDAO) refundDaoInstance;
    }

    public boolean addNewRefund(Long salesID, Long customerID, Double amount, String remark, String date, String day, String month, String year, String voucherNo) {
        genID = idGeneratorDAO.getLastIdValue("Refund");
        genID += 1;
        query = "insert into " + AppConstant.REFUND_TABLE_NAME + "(" + AppConstant.REFUND_ID + ", " + AppConstant.REFUND_VOUCHER_NO + " , " +
                AppConstant.REFUND_SALES_ID + " , " + AppConstant.REFUND_CUSTOMER_ID + " , " + AppConstant.REFUND_DATE + " , " + AppConstant.REFUND_DAY + " , " + AppConstant.REFUND_MONTH +
                " , " + AppConstant.REFUND_YEAR + " , " + AppConstant.REFUND_AMOUNT + " , " + AppConstant.REFUND_REMARK + " , " + AppConstant.CREATED_DATE + ", " + AppConstant.REFUND_TIME + ") values(" + genID + ", ?, " +
                salesID + ", " + customerID + ", ?, ?, ?, ?, " + amount + ", ?, " + DateUtility.getTodayDate() + ", strftime('%s', ?, time('now', 'localtime')))";
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{voucherNo, date, day, month, year, remark, formatDateWithDash(day, month, year)});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean deleteRefund(Long id) {
        query = "delete from " + AppConstant.REFUND_TABLE_NAME + " where " + AppConstant.REFUND_ID + " = " + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public Long findRefundIdBySalesID(Long salesID) {
        query = "select " + AppConstant.REFUND_ID + " from " + AppConstant.REFUND_TABLE_NAME + " where " + AppConstant.REFUND_SALES_ID + " = " + salesID;
        id = 0l;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }

}
