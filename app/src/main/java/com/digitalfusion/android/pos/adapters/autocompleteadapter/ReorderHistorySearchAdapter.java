package com.digitalfusion.android.pos.adapters.autocompleteadapter;


import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.model.StockItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD001 on 1/31/17.
 */

public class ReorderHistorySearchAdapter extends ArrayAdapter<StockItem> {


    private List<StockItem> suggestion;

    private List<StockItem> searchList;

    private Context context;

    private String queryText;

    private int lenght;

    private StockManager stockManager;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((StockItem) resultValue).getCodeNo();

            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null && constraint.toString().trim().length() > 0) {
                searchList.clear();

                queryText = constraint.toString().trim();

                lenght = constraint.length();

                searchList = stockManager.getActiveStocksByNameOrCode(0, 10, queryText);

                //                Log.w("searhc size", searchList.size() + " SSS");

                FilterResults filterResults = new FilterResults();
                filterResults.values = searchList;
                filterResults.count = searchList.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<StockItem> filterList = (ArrayList<StockItem>) results.values;
            suggestion = searchList;
            if (results != null && results.count > 0) {
                clear();
                for (StockItem vehicle1 : filterList) {
                    add(vehicle1);
                    notifyDataSetChanged();
                }
            }
        }
    };

    public ReorderHistorySearchAdapter(Context context, StockManager stockManager) {
        super(context, -1);

        this.context = context;

        this.stockManager = stockManager;

        suggestion = new ArrayList<>();
        searchList = new ArrayList<>();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        StockSearchAdapter.ViewHolder viewHolder;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.stock_item_auto_complete_view, parent, false);
        }
        viewHolder = new StockSearchAdapter.ViewHolder(convertView);

        if (!suggestion.isEmpty() && suggestion.size() > 0) {

            //

            if (suggestion.get(position).getCodeNo().contains(queryText)) {
                Spannable spanText = new SpannableString(suggestion.get(position).getCodeNo());


                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.codeTextView.setText(spanText);

            } else {
                viewHolder.codeTextView.setText(suggestion.get(position).getCodeNo());
            }


            if (suggestion.get(position).getName().contains(queryText)) {
                Spannable spanText = new SpannableString(suggestion.get(position).getName());


                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.nameTextView.setText(spanText);

            } else {
                viewHolder.nameTextView.setText(suggestion.get(position).getName());
            }


        }

        Log.w("stock", "view");

        return convertView;
    }

    @Override
    public int getCount() {
        return suggestion.size();
    }

    public List<StockItem> getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(List<StockItem> suggestion) {
        this.suggestion = suggestion;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    static class ViewHolder {
        TextView codeTextView;
        TextView nameTextView;

        public ViewHolder(View itemView) {
            this.nameTextView = (TextView) itemView.findViewById(R.id.name);
            this.codeTextView = (TextView) itemView.findViewById(R.id.code);
        }
    }

}