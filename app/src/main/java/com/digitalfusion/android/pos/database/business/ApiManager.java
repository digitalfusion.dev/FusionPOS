package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.information.LicenceInfo;
import com.digitalfusion.android.pos.information.Registration;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.ExternalFile;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.Date;
import java.util.List;

/**
 * Created by MD002 on 1/17/18.
 */

public class ApiManager {

    private Context context;
    private ApiDAO apiDAO;

    public ApiManager(Context context) {
        this.context = context;
        apiDAO = ApiDAO.getApiDAOInstance(context);
    }

    public boolean updateRegistration(String username, String userID, String businessName, String userStatus) {
        return apiDAO.updateRegistration(username, userID, businessName, userStatus);
    }

    public boolean addLicense(String duration, Date startDate, Date endDate) {
        return apiDAO.addLicense(duration, startDate, endDate);
    }

    public Registration getRegistration() {
        return apiDAO.getRegistration();
    }

    public LicenceInfo getLicense() {
        return apiDAO.getLicense();
    }

    public boolean


    updateLicense(String duration, Date startDate, Date endDate) {
        ExternalFile externalFile = new ExternalFile();
        externalFile.createFile(DateUtility.makeDate(endDate), context);
        POSUtil.setEndDate(context, DateUtility.makeDate(endDate));

        return apiDAO.updateLicense(duration, startDate, endDate);
    }

    public boolean validatePasscode(long userId, String passcode) {
        return apiDAO.validatePasscode(userId, passcode);
    }

    public boolean updatePasscode(int passcode_id, String passcode) {
        return apiDAO.updatePasscode(passcode_id, passcode);
    }

    public boolean changePasscode(long userId, String passcode) {
        return apiDAO.changePasscode(userId, passcode);
    }

    public boolean isPasscodeSet(long user_id) {
        return apiDAO.isPasscodeSet(user_id);
    }

    //    public boolean addNewUserRole(String name, String role) {
    //        return apiDAO.addNewUser(name, role);
    //    }

    //    public boolean updateUserRole(long id, String username, String role) {
    //        return apiDAO.updateUserRole(id, username, role) > 0;
    //    }
    public List<User> getAllUserRoles() {
        return apiDAO.getAllUserRoles();
    }

    public boolean deleteUser(long id) {
        return apiDAO.deleteUser(id);
    }

    public Long getOwnerId() {
        return apiDAO.getOwnerId();
    }
}
