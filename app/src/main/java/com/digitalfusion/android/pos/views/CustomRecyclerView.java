package com.digitalfusion.android.pos.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by MD002 on 1/12/18.
 */

public class CustomRecyclerView extends RecyclerView {
    private Context context;

    public CustomRecyclerView(Context context) {
        super(context);
        this.context = context;
    }

    public CustomRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public CustomRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }


    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        //        super.onMeasure(widthSpec, heightSpec);
        heightSpec = MeasureSpec.makeMeasureSpec(525, MeasureSpec.AT_MOST);
        super.onMeasure(widthSpec, heightSpec);
    }
}
