package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForInventoryValuation;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForReportWithDateNameQty;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterforProductCategoryBySupplier;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SimpleReportFragmentWithDateFilter extends Fragment implements Serializable {
    private TextView dateFilterTextView;


    private RVAdapterForFilter rvAdapterForDateFilter;

    private MaterialDialog dateFilterDialog;

    private List<String> filterList;

    private String reportName;
    private View mainLayoutView;
    private Context context;

    private RecyclerView recyclerView;
    private ParentRVAdapterForReports rvAdapterForReport;

    private ReportManager reportManager;

    private List<ReportItem> reportItemList;
    //private ReportItem reportItemView;
    private List<Double> yValList;

    private String startDate;
    private String endDate;
    private int startLimit;
    private int endLimit;

    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;

    private String customRange;

    private MaterialDialog customRangeDialog;

    private TextView startDateTextView, endDateTextView;
    private TextView totalValueTextView;

    private String customStartDate,
            customEndDate;

    private TextView traceDate;

    private Button customRangeOkBtn, customRangeCancelBtn;

    private DatePickerDialog startDatePickerDialog;

    private boolean shouldLoad = true;

    private List<ReportItem> renderReportItemList;
    private LinearLayout totalLinearLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_simple_report_fragment_with_date_filter, container, false);
        if (getArguments() != null) {
            reportName = getArguments().getString("reportType");
        }
        if (reportName.equalsIgnoreCase("product category by supplier")) {
            String title = ThemeUtil.getString(context, R.attr.product_category_by_supplier);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        } else if (reportName.equalsIgnoreCase("damage list")) {
            String title = ThemeUtil.getString(context, R.attr.damaged_product);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        } else if (reportName.equalsIgnoreCase("lost list")) {
            String title = ThemeUtil.getString(context, R.attr.lost_product);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        } else if (reportName.equalsIgnoreCase("inventory valuation")) {
            String title = ThemeUtil.getString(context, R.attr.inventory_valuation);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
            totalLinearLayout = mainLayoutView.findViewById(R.id.total_value_linear_layout);
            totalLinearLayout.setVisibility(View.VISIBLE);
        }

        loadUI();

        return mainLayoutView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initializeVariables();
        configFilters();
        buildDateFilterDialog();
        setDateFilterTextView(thisMonth);
        buildingCustomRangeDialog();
        clickListeners();

        new LoadProgressDialog().execute("");

    }

    private void clickListeners() {
        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                dateFilterDialog.dismiss();
                if (filterList.get(position).equalsIgnoreCase(thisMonth)) {

                    startDate = DateUtility.getThisMonthStartDate();
                    endDate = DateUtility.getThisMonthEndDate();


                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {

                    startDate = DateUtility.getLastMonthStartDate();
                    endDate = DateUtility.getLastMonthEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {

                    startDate = DateUtility.getThisYearStartDate();
                    endDate = DateUtility.getThisYearEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {

                    startDate = DateUtility.getLastYearStartDate();
                    endDate = DateUtility.getLastYearEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));
                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {

                    customRangeDialog.show();

                }


            }
        });

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String startDate = DateUtility.makeDateFormatWithSlash(customStartDate);
                String endDate   = DateUtility.makeDateFormatWithSlash(customEndDate);

                //String startDayDes[]= DateUtility.dayDes(startDate);
                //String startYearMonthDes= DateUtility.monthYearDes(startDate);
                //String endDayDes[]= DateUtility.dayDes(endDate);
                //String endYearMonthDes= DateUtility.monthYearDes(endDate);

                dateFilterTextView.setText(startDate + " - " + endDate);


                new LoadProgressDialog().execute("");
                customRangeDialog.dismiss();
            }
        });

        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRangeDialog.dismiss();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                if (shouldLoad) {
                    rvAdapterForReport.setShowLoader(true);
                    loadmore();

                }

            }
        });
    }

    private void configFilters() {
        thisMonth = ThemeUtil.getString(context, R.attr.this_month);
        lastMonth = ThemeUtil.getString(context, R.attr.last_month);
        thisYear = ThemeUtil.getString(context, R.attr.this_year);
        lastYear = ThemeUtil.getString(context, R.attr.last_year);
        thisMonth = ThemeUtil.getString(context, R.attr.this_month);
        customRange = ThemeUtil.getString(context, R.attr.custom_range);

        filterList = new ArrayList<>();
        filterList.add(thisMonth);
        filterList.add(lastMonth);
        filterList.add(thisYear);
        filterList.add(lastYear);
        filterList.add(customRange);
        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });


    }

    public void buildingCustomRangeDialog() {
        buildingCustomRangeDatePickerDialog();
        String date = ThemeUtil.getString(context, R.attr.date_range);
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(date)
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    private void setDateFilterTextView(String msg) {

        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {

        String filter = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);
        dateFilterDialog = new MaterialDialog.Builder(context)
                .title(filter)
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

    }

    private void loadUI() {
        dateFilterTextView = (TextView) mainLayoutView.findViewById(R.id.date_filter);
        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.recycler_view);
        totalValueTextView = mainLayoutView.findViewById(R.id.total_value);
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);
        setDatesAndLimits();

    }

    private void initializeList() {
        switch (reportName) {
            case "inventory valuation":
                reportManager.dropPurchasePriceTempTable();
                reportManager.createPurchasePriceTempTableForValuation(endDate);
                reportItemList = reportManager.inventoryValuation(startLimit, endLimit);
                break;
            case "damage list":
                reportItemList = reportManager.damageList(startDate, endDate, startLimit, endLimit);
                break;
            case "lost list":
                reportItemList = reportManager.lostList(startDate, endDate, startLimit, endLimit);
                break;
            case "product category by supplier":
                reportItemList = reportManager.noOfProductCategoryBySupplier(startDate, endDate, startLimit, endLimit);
                break;
        }


    }

    public void configRecyclerView() {
        switch (reportName) {
            case "inventory valuation":
                rvAdapterForReport = new RVAdapterForInventoryValuation(renderReportItemList);
                break;
            case "damage list":
            case "lost list":
                rvAdapterForReport = new RVAdapterForReportWithDateNameQty(renderReportItemList);
                break;
            case "product category by supplier":
                rvAdapterForReport = new RVAdapterforProductCategoryBySupplier(renderReportItemList, context);
                ((RVAdapterforProductCategoryBySupplier) rvAdapterForReport).setClickListener(new ClickListener() {
                    @Override
                    public void onClick(int postion) {
                        Bundle                                  bundle                                  = new Bundle();
                        ProductCategoryBySupplierDetailFragment productCategoryBySupplierDetailFragment = new ProductCategoryBySupplierDetailFragment();
                        ReportItem                              reportItem                              = new ReportItem();
                        reportItem.setId(renderReportItemList.get(postion - 1).getId());
                        reportItem.setName(renderReportItemList.get(postion - 1).getName());
                        reportItem.setDate(startDate);
                        reportItem.setDate1(endDate);
                        bundle.putSerializable("supplier", reportItem);
                        productCategoryBySupplierDetailFragment.setArguments(bundle);

                        bundle.putSerializable("frag", productCategoryBySupplierDetailFragment);

                        Intent detailIntent = new Intent(context, DetailActivity.class);

                        detailIntent.putExtras(bundle);

                        startActivity(detailIntent);
                    }
                });
                break;
        }
        recyclerView.setAdapter(rvAdapterForReport);
    }

    private void setDatesAndLimits() {
        startDate = DateUtility.getThisMonthStartDate();
        endDate = DateUtility.getThisMonthEndDate();
        startLimit = 0;
        endLimit = 10;
    }

    public void loadmore() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                renderReportItemList.addAll(loadMore(renderReportItemList.size(), 9));

                rvAdapterForReport.setShowLoader(false);

                configRecyclerView();
            }
        }, 500);
    }

    public List<ReportItem> loadMore(int startLimit, int endLimit) {
        Log.e("limit", startLimit + " , " + endLimit);

        switch (reportName) {
            case "inventory valuation":
                //                reportManager.createPurchasePriceTempTableForValuation(endDate);
                reportItemList = reportManager.inventoryValuation(startLimit, endLimit);
                //                reportManager.dropPurchasePriceTempTable();
                break;
            case "damage list":
                reportItemList = reportManager.damageList(startDate, endDate, startLimit, endLimit);
                break;
            case "lost list":
                reportItemList = reportManager.lostList(startDate, endDate, startLimit, endLimit);
                break;
            case "product category by supplier":
                reportItemList = reportManager.noOfProductCategoryBySupplier(startDate, endDate, startLimit, endLimit);
                break;
        }
        return reportItemList;
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/

            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String label = "";

            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {

            if (reportName.equalsIgnoreCase("inventory valuation")) {
                Double totalValue = reportManager.totalInventoryValuation();
                totalValueTextView.setText(POSUtil.NumberFormat(totalValue));
            }
            renderReportItemList = reportItemList;
            configRecyclerView();

            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
