package com.digitalfusion.android.pos.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.CustomerManager;
import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

/**
 * Created by MD003 on 3/8/17.
 */

public class AddEditCustomerFragment extends Fragment {


    //UI component

    public static final String KEY = "customer";
    private EditText customerNameTextInputEditTextMd;
    private EditText customerPhoneTextInputEditTextMd;
    private EditText customerAddressTextInputEditTextMd;
    private View mainLayoutView;
    private Customer customer;
    private CustomerManager customerManager;
    private boolean isEdit = false;

    private String customerName;

    private String customerPhone;

    private String customerAddress;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.new_customer_md, container, false);

        context = getContext();

        customerManager = new CustomerManager(getContext());

        setHasOptionsMenu(true);

        loadUI();

        if (getArguments().getSerializable(KEY) == null) {
            isEdit = false;
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_new_customer}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        } else {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_customer}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
            isEdit = true;
            customer = (Customer) getArguments().get(KEY);
            initializeOldData();
        }

        return mainLayoutView;

    }

    @Override
    public void onResume() {

        super.onResume();

    }

    public void getValuesFromView() {

        customerName = customerNameTextInputEditTextMd.getText().toString().trim();

        customerPhone = customerPhoneTextInputEditTextMd.getText().toString().trim();

        customerAddress = customerAddressTextInputEditTextMd.getText().toString().trim();

    }


    private void initializeOldData() {

        customerName = customer.getName();
        customerPhone = customer.getPhoneNo();
        customerAddress = customer.getAddress();

        customerNameTextInputEditTextMd.setText(customer.getName());
        customerPhoneTextInputEditTextMd.setText(customer.getPhoneNo());
        customerAddressTextInputEditTextMd.setText(customer.getAddress());

    }

    private void clearData() {
        customerNameTextInputEditTextMd.setText(null);
        customerNameTextInputEditTextMd.setError(null);
        customerPhoneTextInputEditTextMd.setText(null);
        customerPhoneTextInputEditTextMd.setError(null);

        customerAddressTextInputEditTextMd.setText(null);
        customerAddressTextInputEditTextMd.setError(null);

    }

    public boolean checkValidation() {

        boolean status = true;

        if (customerNameTextInputEditTextMd.getText().toString().trim().length() < 1) {

            String enterName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_customer_name}).getString(0);
            customerNameTextInputEditTextMd.setError(enterName);

            status = false;

        }

        return status;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save) {

            if (!isEdit) {
                saveCategory();
            } else {

                if (checkValidation()) {
                    getValuesFromView();

                    boolean status = customerManager.updateCustomer(customerName, customerAddress, customerPhone, customer.getId());

                    if (status) {
                        getActivity().onBackPressed();
                    }

                }
            }

        }
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void saveCategory() {
        if (checkValidation()) {

            getValuesFromView();

            customerManager.addNewCustomer(customerName, customerAddress, customerPhone, 0.0, new InsertedBooleanHolder());

            clearData();
            getActivity().onBackPressed();
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);

      /*  TypedArray save=context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        FontTextView fontTextView= (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.menu_text,null);

        menu.getItem(0).setActionView(fontTextView);

        fontTextView.setText(save.getString(0));

        fontTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menu.getItem(0));
            }
        });*/

        super.onCreateOptionsMenu(menu, inflater);

    }

    private void loadUI() {
        customerNameTextInputEditTextMd = (EditText) mainLayoutView.findViewById(R.id.cust_name_in_add_customer_TIET);

        customerPhoneTextInputEditTextMd = (EditText) mainLayoutView.findViewById(R.id.cust_phone_in_add_customer_TIET);

        customerAddressTextInputEditTextMd = (EditText) mainLayoutView.findViewById(R.id.cust_address_in_add_customer_TIET);


    }
}

