package com.digitalfusion.android.pos.database.model;

import android.graphics.Bitmap;

import com.digitalfusion.android.pos.util.POSUtil;

/**
 * Created by MD002 on 11/30/16.
 */

public class StockImage {
    private Long id;
    private Long stockID;
    private byte[] image;

    public StockImage() {
    }

    public StockImage(Long id, Long stockID, byte[] image) {
        this.id = id;
        this.stockID = stockID;
        this.image = image;
    }

    public StockImage(byte[] image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStockID() {
        return stockID;
    }

    public void setStockID(Long stockID) {
        this.stockID = stockID;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Bitmap getImageBitmap() {
        return POSUtil.getBitmapFromByteArray(image);
    }
}
