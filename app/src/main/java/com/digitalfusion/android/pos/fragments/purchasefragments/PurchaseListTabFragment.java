package com.digitalfusion.android.pos.fragments.purchasefragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.util.TabUtil;
import com.example.searchview.MaterialSearchView;

/**
 * Created by MD003 on 2/23/18.
 */

public class PurchaseListTabFragment extends Fragment {

    private View mainLayoutView;

    //Tab
    private TabLayout tabLayout;

    private MaterialSearchView searchView;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainLayoutView = inflater.inflate(R.layout.damage_and_lost_tab_fragment, null);

        setHasOptionsMenu(true);


        context = getContext();

        TypedArray inventory = context.getTheme().obtainStyledAttributes(new int[]{R.attr.purchase});
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(inventory.getString(0));

        loadUI();
        MainActivity.setCurrentFragment(this);
        setupTabLayout();
        MainActivity.replacingTabFragment(new PurchaseHistoryListFragment());

        return mainLayoutView;

    }


    private void setupTabLayout() {

        //  tabLayout.addTab(tabLayout.newTab().setText("Stock"),true);
        // tabLayout.addTab(tabLayout.newTab().setText("Reorder"));
        //  tabLayout.addTab(tabLayout.newTab().setText("Damage"));
        //  tabLayout.addTab(tabLayout.newTab().setText("Lost"));

        TypedArray purchase = context.getTheme().obtainStyledAttributes(new int[]{R.attr.purchase_invoice});
        TypedArray hold     = context.getTheme().obtainStyledAttributes(new int[]{R.attr.hold});

        tabLayout.addTab(tabLayout.newTab().setCustomView(TabUtil.renderTabView(mainLayoutView.getContext(), purchase.getString(0), 0)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(TabUtil.renderTabView(mainLayoutView.getContext(), hold.getString(0), 0)));


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setCurrentTabFragment(int tabPostion) {
        switch (tabPostion) {
            case 0:
                MainActivity.replacingTabFragment(new PurchaseHistoryListFragment());
                break;
            case 1:
                MainActivity.replacingTabFragment(new PurchaseHoldListFragment());
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void loadUIFromToolbar() {
        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        searchView.setCursorDrawable(attributeResourceId);
    }

    public void loadUI() {
        loadUIFromToolbar();
        tabLayout = (TabLayout) mainLayoutView.findViewById(R.id.tablayout);

    }


}