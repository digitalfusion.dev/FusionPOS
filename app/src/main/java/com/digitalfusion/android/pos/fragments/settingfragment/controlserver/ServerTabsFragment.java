package com.digitalfusion.android.pos.fragments.settingfragment.controlserver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForOnlineUserList;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.DeviceRVAdapter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForUserRoleList;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.database.model.Device;
import com.digitalfusion.android.pos.database.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lyx on 5/25/18.
 */
public class ServerTabsFragment extends Fragment {
    public static final String PAGE = "PAGE";
    public static final String ACTION_DEVICE_REQUEST = "com.digitalfusion.android.pos.android.action.DEVICE_REQUEST";

    private int page;
    private Context context;

    private RecyclerView deviceListView;
    private RecyclerView userListView;

    DeviceRVAdapter deviceRVAdapter;

    public static ServerTabsFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(PAGE, page);
        ServerTabsFragment fragment = new ServerTabsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.page = getArguments().getInt(PAGE);
        this.deviceRVAdapter = new DeviceRVAdapter(context, new ArrayList<Device>());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_server_tabs, container, false);

        deviceListView = view.findViewById(R.id.deviceListView);
        userListView = view.findViewById(R.id.userListView);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();

        if (page == 0) {
            showDeviceList();
        } else {
            showUserList();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(ACTION_DEVICE_REQUEST);
        getActivity().registerReceiver(broadcastReceiver, filter);
    }

    @Override
    public void onDestroyView() {
        getActivity().unregisterReceiver(broadcastReceiver);
        super.onDestroyView();
    }

    private void showDeviceList() {
        deviceListView.setVisibility(View.VISIBLE);
        userListView.setVisibility(View.GONE);

        deviceListView.setAdapter(deviceRVAdapter);
        deviceListView.setLayoutManager(new LinearLayoutManager(getContext()));
        refreshDeviceList();
    }

    private void showUserList() {
        deviceListView.setVisibility(View.GONE);
        userListView.setVisibility(View.VISIBLE);

        AccessLogManager accessLogManager = new AccessLogManager(context);
        List<User> userList = accessLogManager.getOnlineUsers();
        RVAdapterForOnlineUserList rvAdapter = new RVAdapterForOnlineUserList(userList);
        userListView.setAdapter(rvAdapter);
        userListView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    public void refreshDeviceList() {
        deviceRVAdapter.swapData(ApiDAO.getApiDAOInstance(context).getAllDevices());
    }

    public void refreshUserList() {
        AccessLogManager accessLogManager = new AccessLogManager(context);
        ((RVAdapterForOnlineUserList) userListView.getAdapter()).swapData(accessLogManager.getOnlineUsers());
    }

    /**
     * Sorry I don't know how to handle this case anymore. There might be a better solution than this.
     * But whatever. ¯\_(ツ)_/¯
     *
     * This method is intended for updating device list shown to the user, right from Server.class
     */
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case ACTION_DEVICE_REQUEST:
                    refreshDeviceList();
                    break;
            }
        }
    };
}
