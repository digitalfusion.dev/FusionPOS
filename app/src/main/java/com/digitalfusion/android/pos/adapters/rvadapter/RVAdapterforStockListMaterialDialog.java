package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.StockItem;

import java.util.List;

/**
 * Created by MD003 on 8/18/16.
 */
public class RVAdapterforStockListMaterialDialog extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    protected boolean showLoader = false;
    List<StockItem> stockItemList;
    private OnItemClickListener mItemClickListener;
    private LoaderViewHolder loaderViewHolder;

    public RVAdapterforStockListMaterialDialog(List<StockItem> stockItemList) {
        this.stockItemList = stockItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEWTYPE_ITEM) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_item_view_stock_list_md, parent, false);

            return new StockItemViewHolder(view);

        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            return new LoaderViewHolder(v);
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof StockItemViewHolder) {

            StockItemViewHolder stockItemViewHolder = (StockItemViewHolder) holder;

            stockItemViewHolder.stockItemNameTextView.setText(stockItemList.get(position).getName());

            stockItemViewHolder.stockCodeTextView.setText(stockItemList.get(position).getCodeNo());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mItemClickListener != null) {

                        mItemClickListener.onItemClick(v, position);

                    }
                }

            });
        } else {
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) holder;

            this.loaderViewHolder = loaderViewHolder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (stockItemList == null || stockItemList.size() == 0) {
            return 0;
        } else {
            return stockItemList.size() + 1;
        }

    }


    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            if (stockItemList != null && stockItemList.size() != 0) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;

            }

        }

        return VIEWTYPE_ITEM;
    }

    public void setShowLoader(boolean showLoader) {
        this.showLoader = showLoader;

        if (loaderViewHolder != null) {
            Log.w("hele", "loader full not null");
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }

        }
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public List<StockItem> getStockItemList() {
        return stockItemList;
    }

    public void setStockItemList(List<StockItem> stockItemList) {
        this.stockItemList = stockItemList;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class StockItemViewHolder extends RecyclerView.ViewHolder {
        TextView noTextView;
        TextView stockCodeTextView;
        TextView stockItemNameTextView;
        View itemView;

        public StockItemViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            noTextView = (TextView) itemView.findViewById(R.id.no);
            stockCodeTextView = (TextView) itemView.findViewById(R.id.stock_code_in_stock_item_view_md);
            stockItemNameTextView = (TextView) itemView.findViewById(R.id.item_name_in_stock_item_view_md);


        }
    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar mProgressBar;

        public View mView;

        public LoaderViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);

        }
    }
}
