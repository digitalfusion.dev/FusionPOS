package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForTaxList;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.Tax;

import java.util.List;

/**
 * Created by MD003 on 8/24/16.
 */
public class TaxSetupFragment extends Fragment {

    private View mainLayoutView;

    private RecyclerView recyclerView;

    private FloatingActionButton addTaxFab;

    private SettingManager settingManager;

    //Add tax dialog
    private MaterialDialog taxMaterialDialog;

    private MDButton addTaxMdButton;

    private EditText taxNameTextInputEditText;

    private EditText taxRateTextInputEditText;

    private EditText taxDescTextInputEditText;

    private RadioGroup taxTypeRadioGroup;

    private RadioButton inclusive;

    private RadioButton exclusive;

    private CheckBox useDefaultCheckBox;


    //Edit tax dialog
    private MaterialDialog editTaxMaterialDialog;

    private MDButton editAddTaxMdButton;

    private EditText editTaxNameTextInputEditText;

    private EditText editTaxRateTextInputEditText;

    private EditText editTaxDescTextInputEditText;

    private RadioGroup editTaxTypeRadioGroup;

    private RadioButton editInclusive;

    private RadioButton editExclusive;

    private CheckBox editUseDefaultCheckBox;

    //For Tax list
    private RVSwipeAdapterForTaxList rvSwipeAdapterForTaxList;

    private List<Tax> taxList;

    private boolean isInclusive = true;

    private boolean isDefault = false;

    //For value
    private String taxName;

    private Double taxRate;

    private String taxDesc;

    private String taxType;

    private Integer useDefault;

    //For edit value
    private int editPosition;

    private String editTaxName;

    private Double editTaxRate;

    private String editTaxDesc;

    private String editTaxType;

    private Integer editUseDefault;

    private boolean editIsInclusive = true;

    private boolean editIsDefault = false;

    private Context context;

    private MaterialDialog deleteAlertDialog;

    private Button yesDeleteMdButton;

    private int deletePos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.tax, null);

        context = getContext();

        settingManager = new SettingManager(context);

        String tax = context.getTheme().obtainStyledAttributes(new int[]{R.attr.tax_setup}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(tax);

        buildingEditTaxDialog();

        buildingAddTaxDialog();

        loadUI();

        buildDeleteAlertDialog();

        configUI();
        MainActivity.setCurrentFragment(this);
        addTaxFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // clearInput();

                // taxMaterialDialog.show();


                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_TAX);

                startActivity(addCurrencyIntent);

            }
        });

        taxTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                isInclusive = inclusive.getId() == checkedId;

            }
        });

        editTaxTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                editIsInclusive = editInclusive.getId() == checkedId;

            }
        });

        editUseDefaultCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                editIsDefault = isChecked;

            }
        });


        useDefaultCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                isDefault = isChecked;

            }
        });

        addTaxMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // settingManager.addNewTax();

                if (checkValidation()) {

                    takeValueFromView();

                    settingManager.addNewTax(taxName, taxRate, taxType, taxDesc, useDefault);

                    taxMaterialDialog.dismiss();

                    refreshTaxList();

                }

            }
        });

        rvSwipeAdapterForTaxList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

            /*    editPosition=postion;

                editTaxNameTextInputEditText.setText(taxList.get(postion).getName());

                editTaxRateTextInputEditText.setText(taxList.get(postion).getRate().toString());

                editTaxDescTextInputEditText.setText(taxList.get(postion).getDescription());

                if(taxList.get(postion).getFeedbackType().equalsIgnoreCase("Inclusive")){

                    editTaxTypeRadioGroup.check(editInclusive.getId());

                }else {

                    editTaxTypeRadioGroup.check(editExclusive.getId());

                }
                if(taxList.get(postion).getIsDefault()>0){

                    editUseDefaultCheckBox.setChecked(true);

                }else {

                    editUseDefaultCheckBox.setChecked(false);

                }

                editTaxMaterialDialog.show();*/

                Bundle bundle = new Bundle();

                bundle.putSerializable(AddEditTaxFragment.KEY, taxList.get(postion));

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtras(bundle);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_TAX);

                startActivity(addCurrencyIntent);

            }
        });

        editAddTaxMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkEditValidation()) {

                    takeValueFromEditView();

                    taxList.get(editPosition).setIsDefault(editUseDefault);

                    taxList.get(editPosition).setName(editTaxName);

                    taxList.get(editPosition).setType(editTaxType);

                    taxList.get(editPosition).setDescription(editTaxDesc);

                    taxList.get(editPosition).setRate(editTaxRate);

                    settingManager.updateTax(editTaxName, editTaxRate, editTaxType, editTaxDesc, editUseDefault, taxList.get(editPosition).getId());

                    rvSwipeAdapterForTaxList.notifyItemChanged(editPosition);

                    editTaxMaterialDialog.dismiss();

                }

            }
        });


        rvSwipeAdapterForTaxList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                if (postion != 0) {
                    deletePos = postion;
                    deleteAlertDialog.show();
                }
            }
        });

        yesDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                settingManager.deleteTax(taxList.get(deletePos).getId());

                rvSwipeAdapterForTaxList.deletePos(deletePos);
                deleteAlertDialog.dismiss();
            }
        });


        return mainLayoutView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshTaxList();
    }

    private boolean checkValidation() {

        boolean status = true;

        if (taxNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;
            String taxName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_tax_name}).getString(0);

            taxNameTextInputEditText.setError(taxName);

        }

        if (taxRateTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;
            String taxRate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_tax_rate}).getString(0);

            taxRateTextInputEditText.setError(taxRate);

        }

        return status;

    }

    private boolean checkEditValidation() {

        boolean status = true;

        if (editTaxNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;
            String taxName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_tax_name}).getString(0);

            editTaxNameTextInputEditText.setError(taxName);

        }

        if (editTaxRateTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;
            String taxRate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_tax_rate}).getString(0);

            editTaxRateTextInputEditText.setError(taxRate);

        }

        return status;

    }

    private void takeValueFromView() {
        String inclusive = context.getTheme().obtainStyledAttributes(new int[]{R.attr.inclusive}).getString(0);

        String exclusive = context.getTheme().obtainStyledAttributes(new int[]{R.attr.exclusive}).getString(0);

        taxName = taxNameTextInputEditText.getText().toString().trim();

        taxRate = Double.parseDouble(taxRateTextInputEditText.getText().toString().trim());

        taxDesc = taxDescTextInputEditText.getText().toString().trim();

        if (isInclusive) {

            taxType = inclusive;

        } else {

            taxType = exclusive;

        }

        if (isDefault) {

            useDefault = 1;

        } else {

            useDefault = 0;

        }

    }

    private void takeValueFromEditView() {
        String inclusive = context.getTheme().obtainStyledAttributes(new int[]{R.attr.inclusive}).getString(0);

        String exclusive = context.getTheme().obtainStyledAttributes(new int[]{R.attr.exclusive}).getString(0);

        editTaxName = editTaxNameTextInputEditText.getText().toString().trim();

        editTaxRate = Double.parseDouble(editTaxRateTextInputEditText.getText().toString().trim());

        editTaxDesc = editTaxDescTextInputEditText.getText().toString().trim();

        if (editIsInclusive) {

            editTaxType = inclusive;

        } else {

            editTaxType = exclusive;

        }

        if (editIsDefault) {

            editUseDefault = 1;

        } else {

            editUseDefault = 0;

        }

    }

    private void clearInput() {

        taxNameTextInputEditText.setError(null);

        taxNameTextInputEditText.setText(null);

        taxRateTextInputEditText.setText(null);

        taxRateTextInputEditText.setError(null);

        taxDescTextInputEditText.setText(null);

        taxTypeRadioGroup.check(inclusive.getId());

    }

    public void refreshTaxList() {

        taxList = settingManager.getAllTaxs();

        rvSwipeAdapterForTaxList.setTaxList(taxList);

        rvSwipeAdapterForTaxList.notifyDataSetChanged();

    }

    public void configUI() {

        taxList = settingManager.getAllTaxs();

        rvSwipeAdapterForTaxList = new RVSwipeAdapterForTaxList(taxList);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(rvSwipeAdapterForTaxList);

    }


    private void buildingAddTaxDialog() {

        taxMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.add_tax, true).negativeText("Cancel").positiveText("Save").title("Add Tax").build();

    }

    private void buildingEditTaxDialog() {

        editTaxMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.add_tax, true).negativeText("Cancel").positiveText("Save").title("Edit Tax").build();

    }


    public void loadUI() {

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.tax_list_rv);

        addTaxFab = (FloatingActionButton) mainLayoutView.findViewById(R.id.add_new_tax);

        taxNameTextInputEditText = (EditText) taxMaterialDialog.findViewById(R.id.tax_name_in_add_tax_et);

        taxRateTextInputEditText = (EditText) taxMaterialDialog.findViewById(R.id.tax_rate_in_add_tax_et);

        taxDescTextInputEditText = (EditText) taxMaterialDialog.findViewById(R.id.tax_desc_in_add_tax_et);

        addTaxMdButton = taxMaterialDialog.getActionButton(DialogAction.POSITIVE);

        taxTypeRadioGroup = (RadioGroup) taxMaterialDialog.findViewById(R.id.tax_type_in_add_tax_rg);

        inclusive = (RadioButton) taxMaterialDialog.findViewById(R.id.inclusive);

        exclusive = (RadioButton) taxMaterialDialog.findViewById(R.id.exclusive);

        useDefaultCheckBox = (CheckBox) taxMaterialDialog.findViewById(R.id.use_default_in_add_tax);

        editTaxNameTextInputEditText = (EditText) editTaxMaterialDialog.findViewById(R.id.tax_name_in_add_tax_et);

        editTaxRateTextInputEditText = (EditText) editTaxMaterialDialog.findViewById(R.id.tax_rate_in_add_tax_et);

        editTaxDescTextInputEditText = (EditText) editTaxMaterialDialog.findViewById(R.id.tax_desc_in_add_tax_et);

        editAddTaxMdButton = editTaxMaterialDialog.getActionButton(DialogAction.POSITIVE);

        editTaxTypeRadioGroup = (RadioGroup) editTaxMaterialDialog.findViewById(R.id.tax_type_in_add_tax_rg);

        editInclusive = (RadioButton) editTaxMaterialDialog.findViewById(R.id.inclusive);

        editExclusive = (RadioButton) editTaxMaterialDialog.findViewById(R.id.exclusive);

        editUseDefaultCheckBox = (CheckBox) editTaxMaterialDialog.findViewById(R.id.use_default_in_add_tax);

    }

    private void buildDeleteAlertDialog() {

        //TypedArray no = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        //TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        String   sureToDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete}).getString(0);
        TextView textView     = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(sureToDelete);

        yesDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });

    }


}