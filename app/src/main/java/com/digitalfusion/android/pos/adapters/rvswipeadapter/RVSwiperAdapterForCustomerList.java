package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD003 on 8/29/16.
 */
public class RVSwiperAdapterForCustomerList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    protected boolean showLoader = false;
    private List<Customer> customerList;
    private ClickListener editClickListener;
    private ClickListener viewDetailClickListener;
    private ClickListener deleteClickListener;
    private ClickListener callClickListener;
    private LoaderViewHolder loaderViewHolder;

    private int editPos;

    public RVSwiperAdapterForCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEWTYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_item_view, parent, false);

            return new CustomerViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            return new LoaderViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        Log.w("herer", "IN adapter bind");

        if (holder instanceof CustomerViewHolder) {
            final CustomerViewHolder customerViewHolder = (CustomerViewHolder) holder;

            POSUtil.makeZebraStrip(customerViewHolder.itemView, position);

            customerViewHolder.customerNameTextView.setText(customerList.get(position).getName());


            if (customerList.get(position).getPhoneNo() != null && customerList.get(position).getPhoneNo().length() > 1) {

                String[] s = customerList.get(position).getPhoneNo().split(",");

                SpannableString content = new SpannableString(s[0]);
                content.setSpan(new UnderlineSpan(), 0, s[0].length(), 0);
                customerViewHolder.customerPhoneTextView.setText(content);

                customerViewHolder.callBtn.setVisibility(View.VISIBLE);

                customerViewHolder.customerPhoneTextView.setActivated(true);

                customerViewHolder.customerPhoneTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callClickListener.onClick(position);
                    }
                });

            } else {
                customerViewHolder.customerPhoneTextView.setText("");

                customerViewHolder.customerPhoneTextView.setHint("No phone");

                customerViewHolder.customerPhoneTextView.setActivated(false);

                customerViewHolder.callBtn.setVisibility(View.GONE);
            }

            customerViewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editPos = position;
                    if (editClickListener != null) {
                        customerViewHolder.swipeLayout.close();
                        editClickListener.onClick(position);
                    }
                }
            });

            customerViewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteClickListener != null) {
                        customerViewHolder.swipeLayout.close();
                        deleteClickListener.onClick(position);
                    }
                }
            });

            customerViewHolder.detailButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (viewDetailClickListener != null) {
                        customerViewHolder.swipeLayout.close();
                        viewDetailClickListener.onClick(position);
                    }

                }
            });

            customerViewHolder.callBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callClickListener != null) {
                        customerViewHolder.swipeLayout.close();
                        callClickListener.onClick(position);

                    }
                }
            });

            mItemManger.bindView(customerViewHolder.view, position);

        } else {
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) holder;

            this.loaderViewHolder = loaderViewHolder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }

    }

    public ClickListener getViewDetailClickListener() {
        return viewDetailClickListener;
    }

    public void setViewDetailClickListener(ClickListener viewDetailClickListener) {
        this.viewDetailClickListener = viewDetailClickListener;
    }

    public ClickListener getDeleteClickListener() {

        return deleteClickListener;

    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {

        this.deleteClickListener = deleteClickListener;

    }

    public ClickListener getCallClickListener() {
        return callClickListener;
    }

    public void setCallClickListener(ClickListener callClickListener) {
        this.callClickListener = callClickListener;
    }

    @Override
    public int getItemCount() {
        if (customerList == null || customerList.size() == 0) {
            return 0;
        } else {
            return customerList.size() + 1;
        }

    }

    public void updateItem(Customer customer) {

        //Log.w("here updateRegistration",salesHistoryView.getId()+"");

        // Log.w("position",purchaseHistoryViewList.indexOf(purchaseHistoryView)+" S");

        //Log.w("position",purchaseHistoryViewList.get(purchaseHistoryViewList.indexOf(purchaseHistoryView)).getSupplierName()+" S");

        // Log.w("position",purchaseHistoryView.getSupplierName()+" S");

        customerList.set(editPos, customer);

        notifyItemChanged(editPos, customer);

        //  notifyDataSetChanged();


    }

    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            if (customerList != null && customerList.size() != 0) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;

            }

        }

        return VIEWTYPE_ITEM;
    }

    public void setShowLoader(boolean showLoader) {
        this.showLoader = showLoader;

        if (loaderViewHolder != null) {
            Log.w("hele", "loader full not null");
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {

        this.customerList = customerList;

    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {

        this.editClickListener = editClickListener;

    }

    public class CustomerViewHolder extends RecyclerView.ViewHolder {

        TextView customerNameTextView;

        TextView customerPhoneTextView;

        ImageButton editButton;

        ImageButton detailButton;

        ImageButton deleteButton;

        ImageButton callBtn;

        SwipeLayout swipeLayout;

        LinearLayout ll;

        View view;

        public CustomerViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);

            customerNameTextView = (TextView) itemView.findViewById(R.id.cust_name_in_custome_item_view);

            customerPhoneTextView = (TextView) itemView.findViewById(R.id.cust_phone_in_custome_item_view);

            editButton = (ImageButton) itemView.findViewById(R.id.edit_customer);

            detailButton = (ImageButton) itemView.findViewById(R.id.view_detail);

            ll = (LinearLayout) itemView.findViewById(R.id.ll);

            deleteButton = (ImageButton) itemView.findViewById(R.id.delete);

            callBtn = (ImageButton) itemView.findViewById(R.id.call_action);

        }

    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar mProgressBar;

        public View mView;

        public LoaderViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);

        }
    }
}

