package com.digitalfusion.android.pos.database.model;

import java.io.Serializable;

/**
 * Created by MD002 on 8/18/16.
 */
public class User implements Serializable {
    private Long id;
    private String userName;
    // Encrypt it when the time comes ;)
    private String passcode;
    private String role;
    // These two ain't used currently
    private String description;
    private byte[] picture;
    // This one is not available in database
    /*
    Serial number of the device which the user is using
     */
    private String deviceSerialNo;

    public User() {

    }

    public User(Long id, String userName, String passcode, String role) {
        this.id = id;
        this.userName = userName;
        this.passcode = passcode;
        this.role = role;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isVailid() {
        return id != null && userName != null && role != null;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getDeviceSerialNo() {
        return deviceSerialNo;
    }

    public void setDeviceSerialNo(String deviceSerialNo) {
        this.deviceSerialNo = deviceSerialNo;
    }

    @Override
    public String toString() {
        return String.format(
                "Id: %s " +
                        "Username: %s " +
                        "Passcode: %s " +
                        "Role: %s " +
                        "Description: %s " +
                        "Picture: %s"
                , id, userName, passcode, role, description, "");
    }

    public enum ROLE {

        Admin, Sale, Purchase, Manager, Supervisor, Owner

    }
}
