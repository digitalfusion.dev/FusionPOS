package com.digitalfusion.android.pos.fragments.purchasefragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.SupplierAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForSupplierMD;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.AuthorizationManager;
import com.digitalfusion.android.pos.database.business.PurchaseManager;
import com.digitalfusion.android.pos.database.business.SupplierManager;
import com.digitalfusion.android.pos.database.model.PurchaseHeader;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.database.model.SalesHeader;
import com.digitalfusion.android.pos.database.model.Supplier;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.views.FusionToast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.digitalfusion.android.pos.util.AppConstant.DEFAULT_CUSTOMER;
import static com.digitalfusion.android.pos.util.AppConstant.DEFAULT_SUPPLIER;

public class PurchasePaymentFragment extends Fragment {

    @BindView(R.id.supplier_pay)
    AutoCompleteTextView supplierNameTV;
    @BindView(R.id.phone_no_pay)
    EditText supplierPhNo;
    @BindView(R.id.address_in_purchase_change)
    EditText edtsupplierAddress;
    @BindView(R.id.total_amount_in_purcahse_change)
    EditText totalAmountInput;
    @BindView(R.id.paid_amount_in_purchase_change)
    EditText paidAmountInput;
    @BindView(R.id.save)
    Button saveBtn;
    @BindView(R.id.cancel)
    Button cancelBtn;

    private Context context;
    private Unbinder unbinder;
    private Double paidAmount = 0.0;
    private Double totalAmount = 0.0;
    private Boolean isPurchaseEdit = false;
    private Boolean isPurchaseHold = false;
    private String supplierName ;
    private PurchaseHeader purchaseHeader;
    private List<SalesAndPurchaseItem> updateList;
    private List<Long> deleteList;
    private SupplierManager supplierManager;
    private PurchaseManager purchaseManager;
    private ArrayList<Supplier> supplierArrayList;
    private RVAdapterForSupplierMD rvAdapterForSupplierMD;
    private SupplierAutoCompleteAdapter supplierAutoCompleteAdapter;
    private String supplierPhoneNo;
    private String supplierAddress;
    private User currentUser;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View mainLayoutView = inflater.inflate(R.layout.purchase_pay, null);
        context = getContext();
        unbinder = ButterKnife.bind(this, mainLayoutView);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        
        String title = ThemeUtil.getString(context, R.attr.payment);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        MainActivity.setCurrentFragment(this);

        AccessLogManager accessLogManager = new AccessLogManager(context);
        AuthorizationManager authorizationManager = new AuthorizationManager(context);
        Long deviceId = authorizationManager.getDeviceId(Build.SERIAL);
        currentUser = accessLogManager.getCurrentlyLoggedInUser(deviceId);
        if (currentUser == null) {
            POSUtil.noUserIsLoggedIn(context);
            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            return null;
        }

        POSUtil.hideKeyboard(mainLayoutView, context);
        Bundle arguments = getArguments();
        purchaseHeader = (PurchaseHeader) arguments.getSerializable("purchaseHeader");
        totalAmount = purchaseHeader.getTotal();
        paidAmount = purchaseHeader.getPaidAmt();
        isPurchaseEdit = purchaseHeader.getPurchaseEdit();
        isPurchaseHold = purchaseHeader.getPurchaseHold();
        purchaseManager = new PurchaseManager(context);
        supplierManager = new SupplierManager(context);
        supplierArrayList = supplierManager.getAllSuppliersArrayList();
        rvAdapterForSupplierMD = new RVAdapterForSupplierMD(supplierArrayList);
        purchaseHeader.setPurchaseVocherNo(purchaseManager.getPurchaseInvoiceNo());

        if (purchaseHeader.getPurchaseEdit()) {
            isPurchaseEdit = true;
            isPurchaseHold = purchaseHeader.getPurchaseHold();
            if(purchaseHeader.getSupplierName().equalsIgnoreCase(DEFAULT_SUPPLIER))
            {
                supplierNameTV.setText("");
            }
            else
            {
                supplierNameTV.setText(purchaseHeader.getSupplierName());
                supplierPhNo.setText(supplierManager.getSupplierById(purchaseHeader.getSupplierID()).getPhoneNo());
                edtsupplierAddress.setText(supplierManager.getSupplierById(purchaseHeader.getSupplierID()).getAddress());
            }



        }
        paidAmountInput.setError(null);
        totalAmountInput.setEnabled(false);
        totalAmountInput.setText(POSUtil.doubleToString(totalAmount));

        if (isPurchaseEdit) {
            paidAmountInput.setText(POSUtil.doubleToString(paidAmount));
        } else {
            paidAmountInput.setText(POSUtil.doubleToString(totalAmount));
        }
        paidAmountInput.setSelectAllOnFocus(true);

        if(supplierNameTV.getText().toString().equalsIgnoreCase(DEFAULT_SUPPLIER))
        {
            supplierPhNo.setText("");
            edtsupplierAddress.setText("");
        }
        supplierNameTV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                supplierPhNo.setText(supplierAutoCompleteAdapter.getItem(i).getPhoneNo());
                edtsupplierAddress.setText(supplierAutoCompleteAdapter.getItem(i).getAddress());

            }
        });

      /*  supplierNameTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!POSUtil.isTextViewEmpty(supplierNameTV)) {
                    supplierName = supplierNameTV.getText().toString().trim();
                    if (supplierName.equalsIgnoreCase(DEFAULT_SUPPLIER)) {
                        supplierNameTV.setText(null);
                    }

                } else {
                    supplierName = "";
                }
            }
        });*/
        supplierPhNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!POSUtil.isTextViewEmpty(supplierPhNo)) {
                    supplierPhoneNo = supplierPhNo.getText().toString().trim();

                } else {
                    supplierPhoneNo = "";
                }

            }
        });

        edtsupplierAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!POSUtil.isTextViewEmpty(edtsupplierAddress)) {
                    supplierAddress = edtsupplierAddress.getText().toString().trim();

                } else {
                    supplierAddress = "";
                }

            }
        });
        supplierNameTV.setThreshold(1);

        supplierAutoCompleteAdapter = new SupplierAutoCompleteAdapter(context, supplierArrayList);

        supplierNameTV.setAdapter(supplierAutoCompleteAdapter);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveTransaction();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        return mainLayoutView;

    }


    private void saveTransaction() {

        Double balance = purchaseHeader.getTotal() - Double.parseDouble(paidAmountInput.getText().toString());
        if (balance < 0) {

            balance = 0.0;

        }

        supplierName = supplierNameTV.getText().toString().trim();
        supplierPhoneNo = supplierPhNo.getText().toString().trim();
        supplierAddress = edtsupplierAddress.getText().toString().trim();

        if(supplierName == null || supplierName.equalsIgnoreCase(""))
        {
            supplierName = DEFAULT_SUPPLIER;
        }
        if (isPurchaseEdit) {

            if (!isPurchaseHold) {
                if (checkPayValidation()) {


                    boolean status = purchaseManager.updatePurchaseByID(purchaseHeader.getId(), purchaseHeader.getSaleDate(), supplierName, supplierPhoneNo, supplierAddress, totalAmount,

                            purchaseHeader.getDiscountPercent(), purchaseHeader.getTaxID(), purchaseHeader.getSubtotal(), purchaseHeader.getDiscountAmt(), purchaseHeader.getRemark(), paidAmount, balance, purchaseHeader.getTaxAmt(), purchaseHeader.getDay(),

                            purchaseHeader.getMonth(), purchaseHeader.getYear(), purchaseHeader.getTaxRate(), purchaseHeader.getSalesAndPurchaseItemList(), purchaseHeader.getDeleteList(), purchaseHeader.getDiscountID());


                    if (status) {
                        FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                        goToBack();
                    }
                }
            } else {
                if (checkPayValidation()) {

                    boolean status = purchaseManager.addNewPurchase(purchaseHeader.getPurchaseVocherNo(), purchaseHeader.getSaleDate(), supplierName, supplierPhoneNo, supplierAddress,
                            "", totalAmount, purchaseHeader.getDiscountPercent(), purchaseHeader.getTaxID(), purchaseHeader.getSubtotal(), purchaseHeader.getDiscountAmt(), purchaseHeader.getRemark(), paidAmount,
                          balance, purchaseHeader.getTaxAmt(), purchaseHeader.getDay(),
                            purchaseHeader.getMonth(), purchaseHeader.getYear(), purchaseHeader.getTaxRate(), purchaseHeader.getSalesAndPurchaseItemList(), purchaseHeader.getTaxType(), purchaseHeader.getDiscountID(), currentUser.getId());

                    if (status) {
                        purchaseManager.deleteHoldPurchase(purchaseHeader.getId());
                        FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                        goToBack();
                    }

                }
            }


        } else {
            if (checkPayValidation()) {

                boolean status = purchaseManager.addNewPurchase(purchaseHeader.getPurchaseVocherNo(), purchaseHeader.getSaleDate(), supplierName, supplierPhoneNo, supplierAddress,
                        "", totalAmount, purchaseHeader.getDiscountPercent(), purchaseHeader.getTaxID(), purchaseHeader.getSubtotal(), purchaseHeader.getDiscountAmt(), purchaseHeader.getRemark(), paidAmount,
                       balance, purchaseHeader.getTaxAmt(), purchaseHeader.getDay(),
                        purchaseHeader.getMonth(), purchaseHeader.getYear(), purchaseHeader.getTaxRate(), purchaseHeader.getSalesAndPurchaseItemList(), purchaseHeader.getTaxType(), purchaseHeader.getDiscountID(), currentUser.getId());
                if (status) {
                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                    goToBack();
                }

            }
        }
    }

    private void goToBack() {
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    public boolean checkPayValidation() {

        boolean status = true;


        if (paidAmountInput.getText().toString().trim().length() < 1) {

            status = false;

            String enterAmt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_paid_amount}).getString(0);
            paidAmountInput.setError(enterAmt);
        } else {

            paidAmount = Double.valueOf(paidAmountInput.getText().toString());

            if (paidAmount > totalAmount) {
                paidAmount = totalAmount;
            }
        }

        double paidamt = 0.0;
        if (paidAmountInput.getText().toString().trim().length() > 0) {
            paidamt = Double.parseDouble(paidAmountInput.getText().toString());
        }

        if (supplierNameTV.getText().toString().trim().length() < 1 && paidamt < totalAmount) {
            supplierNameTV.setError("Enter Supplier Name");
            supplierPhNo.setError("Enter Supplier PhoneNumber");
            status = false;
        }

        if(supplierNameTV.getText().toString().trim().length()>1 && paidamt < totalAmount)
        {
            if(supplierPhNo.getText().toString().trim().length()<1) {
                supplierPhNo.setError("Enter Supplier PhoneNumber");
                status = false;
            }
        }

        if (supplierPhNo.getText().toString().trim().length() > 0) {
            if (supplierNameTV.getText().toString().trim().length() < 1) {
                supplierNameTV.setError("Enter Supplier Name");
                status = false;
            }
        }


        return status;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}