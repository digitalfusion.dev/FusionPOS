package com.digitalfusion.android.pos.util;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.ThemeObj;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 4/7/17.
 */

public class ThemeUtil {


    public static final String Blue = "blue";
    public static final String Blue_Grey = "blue_grey";

    public static final String Red_Blue = "Red_Blue";
    public static final String Light_Green = "Light_Green";
    public static final String Purple = "Purple";
    public static final String Red = "Red";
    public static final String White = "White";
    public static final String Night = "Night";
    public static final String Gold = "Gold";
    public static final String Cyan = "Cyan";
    public static final String Violet = "Violet";
    public static final String Fusion = "Fusion";
    public static final String Pine_Green = "Pine Green";

    public static List<ThemeObj> getThemeNameList(Context context) {

        List<ThemeObj> themeObjList;

        themeObjList = new ArrayList<>();

        String blue     = context.getTheme().obtainStyledAttributes(new int[]{R.attr.blue_theme}).getString(0);
        String blueGrey = context.getTheme().obtainStyledAttributes(new int[]{R.attr.blue_grey_theme}).getString(0);

        String red    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.red_theme}).getString(0);
        String white  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.white_theme}).getString(0);
        String purple = context.getTheme().obtainStyledAttributes(new int[]{R.attr.purple_theme}).getString(0);
        String cyan   = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cyan_theme}).getString(0);

        String redBlue    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.red_blue_theme}).getString(0);
        String lightGreen = context.getTheme().obtainStyledAttributes(new int[]{R.attr.light_green_theme}).getString(0);
        String gold       = context.getTheme().obtainStyledAttributes(new int[]{R.attr.gold_theme}).getString(0);
        String violet     = context.getTheme().obtainStyledAttributes(new int[]{R.attr.gold_theme}).getString(0);

        themeObjList.add(new ThemeObj("Fusion",
                R.style.Theme_Eng_Fusion,
                R.style.Theme_Eng_Fusion,
                Fusion, R.color.colorPrimary_logo_theme,
                R.color.colorAccent_white,
                R.drawable.circle_fusion));

        themeObjList.add(new ThemeObj("Pine Green",
                R.style.Theme_Eng_Pine_Green,
                R.style.Theme_Eng_Pine_Green,
                Pine_Green,
                R.color.colorPrimary_pine_green,
                R.color.colorAccent_white,
                R.drawable.circle_pine_green));

        themeObjList.add(new ThemeObj(blue,
                R.style.Theme_Eng_Blue_NoActionBar,
                R.style.Theme_Eng_Blue, Blue,
                R.color.colorPrimary_blue,
                R.color.mdtp_white,
                R.drawable.circle_blue_theme));

        themeObjList.add(new ThemeObj(blueGrey,
                R.style.Theme_Eng_BlueGrey_NoActionBar,
                R.style.Theme_Eng_BlueGrey,
                Blue_Grey,
                R.color.colorPrimary_blue_grey,
                R.color.mdtp_white,
                R.drawable.circle_blue_grey_theme));

        themeObjList.add(new ThemeObj(purple,
                R.style.Theme_Eng_Purple_NoActionBar,
                R.style.Theme_Eng_Purple, Purple,
                R.color.colorPrimary_purple,
                R.color.mdtp_white,
                R.drawable.circle_purple_theme));

        themeObjList.add(new ThemeObj(red,
                R.style.Theme_Eng_Red_NoActionBar,
                R.style.Theme_Eng_Red, Red,
                R.color.colorPrimary_red,
                R.color.mdtp_white,
                R.drawable.circle_red_theme));

        themeObjList.add(new ThemeObj(white,
                R.style.Theme_Eng_White_NoActionBar,
                R.style.Theme_Eng_White, White,
                R.color.colorPrimary_white,
                R.color.mdtp_white,
                R.drawable.circle_white_theme));

        themeObjList.add(new ThemeObj(cyan,
                R.style.Theme_Eng_Cyan_NoActionBar,
                R.style.Theme_Eng_Cyan, Cyan,
                R.color.colorPrimary_cyan,
                R.color.background_color,
                R.drawable.circle_cyan));

        themeObjList.add(new ThemeObj(lightGreen,
                R.style.Theme_Eng_light_green_NoActionBar,
                R.style.Theme_Eng_light_green, Light_Green,
                R.color.colorPrimary_light_green,
                R.color.paleBackground_light_green,
                R.drawable.circle_light_green));

        themeObjList.add(new ThemeObj(redBlue,
                R.style.Theme_Eng_sky_blue_NoActionBar,
                R.style.Theme_Eng_sky_blue, Red_Blue,
                R.color.colorPrimary_sky_blue,
                R.color.colorPrimaryLight_sky_blue,
                R.drawable.circle_sky_blue_theme));

        // themeObjList.add(new ThemeObj(gold, R.style.Theme_Eng_Gold_NoActionBar, R.style.Theme_Eng_Gold,Gold));

        themeObjList.add(new ThemeObj(violet,
                R.style.Theme_Eng_Violet_NoActionBar,
                R.style.Theme_Eng_Violet_NoActionBar, Violet,
                R.color.colorPrimary_violet,
                R.color.mdtp_white,
                R.drawable.circle_violet_theme));

        themeObjList.add(new ThemeObj("Mythos Black",
                R.style.Theme_Eng_NightMode,
                R.style.Theme_Eng_White, Night,
                R.color.colorPrimary_darkmode,
                R.color.colorPrimaryLight_darkmode,
                R.drawable.circle_night_mode_theme));

        return themeObjList;

    }


    /**
     * Retrieve a drawable of current applied theme.
     *
     * @param context       needed to get current applied theme.
     * @param AttributeName of needed string
     * @return Drawable
     */
    public static Drawable getDrawable(Context context, int AttributeName) {
        return context.getTheme().obtainStyledAttributes(new int[]{AttributeName}).getDrawable(0);
    }

    /**
     * Retrieve a color of current applied theme.
     *
     * @param context       needed to get current applied theme.
     * @param AttributeName of needed string
     * @param defaultColor
     * @return int
     */
    public static int getColor(Context context, int AttributeName, int defaultColor) {
        return context.getTheme().obtainStyledAttributes(new int[]{AttributeName}).getColor(0, defaultColor);
    }

    /**
     * Retrieve string value of current applied theme.
     *
     * @param context       needed to get current applied theme.
     * @param AttributeName of needed string
     * @return string
     */
    public static String getString(Context context, int AttributeName) {
        return context.getTheme().obtainStyledAttributes(new int[]{AttributeName}).getString(0);
    }
}
