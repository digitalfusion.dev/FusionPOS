package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 3/3/17.
 */

public class RVAdapterForPhone extends RecyclerView.Adapter<RVAdapterForPhone.PhoneVH> {

    public ClickListener clickListener;

    public List<String> phoneList;

    public RVAdapterForPhone() {
        this.phoneList = new ArrayList<>();
    }

    public RVAdapterForPhone(List<String> phoneList) {
        this.phoneList = phoneList;
    }

    @Override
    public RVAdapterForPhone.PhoneVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_phone, parent, false);

        return new PhoneVH(v);
    }

    @Override
    public void onBindViewHolder(RVAdapterForPhone.PhoneVH holder, final int position) {

        holder.phoneNoTextView.setText(phoneList.get(position));

        holder.callActionImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.call(phoneList.get(position));
                }
            }
        });

    }

    public void replaceList(List<String> phoneList) {
        setPhoneList(phoneList);
        notifyDataSetChanged();
    }

    public void setPhoneList(List<String> phoneList) {
        this.phoneList = phoneList;
    }

    @Override
    public int getItemCount() {
        return phoneList.size();
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {

        void call(String phoneNo);

    }

    public class PhoneVH extends RecyclerView.ViewHolder {

        TextView phoneNoTextView;

        ImageButton callActionImageButton;

        public PhoneVH(View itemView) {

            super(itemView);

            phoneNoTextView = (TextView) itemView.findViewById(R.id.phone_no);

            callActionImageButton = (ImageButton) itemView.findViewById(R.id.call_action);

        }
    }
}
