package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.CustomerManager;
import com.digitalfusion.android.pos.database.model.Customer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 12/8/16.
 */

public class


CustomerSearchAdapter extends ArrayAdapter<Customer> {


    List<Customer> suggestion;


    List<Customer> searchList;

    CustomerManager customerManager;

    int lenght = 0;

    String queryText = "";

    Customer expenseNameVO;

    Context context;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {

            String str = ((Customer) resultValue).getName();

            expenseNameVO = (Customer) resultValue;

            searchList = new ArrayList<>();

            return str;

        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            if (constraint != null && !constraint.equals("")) {

                searchList.clear();

                queryText = constraint.toString();

                lenght = constraint.length();

                searchList = customerManager.getAllCustomersByNameOnSearch(0, 10, constraint.toString());

                Log.w("size", searchList.size() + " SS");

                FilterResults filterResults = new FilterResults();

                filterResults.values = searchList;

                filterResults.count = searchList.size();

                return filterResults;
            } else {

                return new FilterResults();

            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            List<Customer> filterList = (ArrayList<Customer>) results.values;
            suggestion = searchList;

            if (results != null && results.count > 0) {

                clear();

                for (Customer customer : filterList) {

                    add(customer);

                    notifyDataSetChanged();

                }

            }

        }

    };

    public CustomerSearchAdapter(Context context, CustomerManager customerManager) {

        super(context, -1);

        this.context = context;

        this.customerManager = customerManager;

        suggestion = new ArrayList<>();
        searchList = new ArrayList<>();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.customer_search_suggestion, parent, false);

        }

        viewHolder = new ViewHolder(convertView);

        if (suggestion.size() > 0 && !suggestion.isEmpty()) {
            if (suggestion.get(position).getName().toLowerCase().startsWith(queryText.toLowerCase())) {

                Spannable spanText = new SpannableString(suggestion.get(position).getName());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.customerNameTextView.setText(spanText);

            }

        }

        return convertView;

    }

    @Override
    public int getCount() {
        return suggestion.size();
    }

    public List<Customer> getSuggestion() {

        return suggestion;

    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    public Customer getSelectedItem() {
        return expenseNameVO;
    }

    static class ViewHolder {
        TextView customerNameTextView;

        public ViewHolder(View itemView) {

            this.customerNameTextView = (TextView) itemView.findViewById(R.id.name_text_view);

        }

    }

}