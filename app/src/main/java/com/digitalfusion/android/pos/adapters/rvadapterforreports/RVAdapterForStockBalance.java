package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD002 on 12/28/16.
 */

public class RVAdapterForStockBalance extends ParentRVAdapterForReports {

    private static final int VIEWTYPE_ITEM = 2;
    private static final int VIEWTYPE_LOADER = 3;
    private final int HEADER_TYPE = 1;
    protected boolean showLoader = false;
    private List<ReportItem> reportItemList;
    private LoaderViewHolder loaderViewHolder;

    /**
     * 1 for stock, category, amount, qty, unit
     * 2 for category, amount
     */
    public RVAdapterForStockBalance(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public void setShowLoader(boolean showLoader) {
        this.showLoader = showLoader;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_stock_balance_header_view, parent, false);

            return new HeaderViewHolder(v);
        } else if (viewType == VIEWTYPE_LOADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);
            return new LoaderViewHolder(v);
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_stock_balance_item_view, parent, false);

        return new StockBalanceViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        //Log.e("bin", position + " fjkd " );

        if (viewHolder instanceof LoaderViewHolder) {

            Log.e("lo", "ader");
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;

            this.loaderViewHolder = loaderViewHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);

        } else if (viewHolder instanceof StockBalanceViewHolder && !reportItemList.isEmpty()) {
            ReportItem currentItem = reportItemList.get(position - 1);
            StockBalanceViewHolder stockBalanceViewHolder = (StockBalanceViewHolder) viewHolder;
            stockBalanceViewHolder.stockNameTextView.setText(currentItem.getName());
            stockBalanceViewHolder.categoryNameTextView.setText(currentItem.getName1());
            //            stockBalanceViewHolder.inventoryQtyTextView.setText(Integer.toString(reportItemList.get(position - 1).getQty()));
            //            stockBalanceViewHolder.inProcessQtyTextView.setText(Integer.toString(reportItemList.get(position - 1).getQty1()));
            stockBalanceViewHolder.totalQtyTextView.setText(Integer.toString(currentItem.getTotalQty()));
            stockBalanceViewHolder.totalAmtTextView.setText(POSUtil.NumberFormat(currentItem.getTotalAmt()));
            stockBalanceViewHolder.unitTextView.setText(currentItem.getName2());
        } else if (viewHolder instanceof HeaderViewHolder && reportItemList.isEmpty()) {
            //Log.e("dafdfadflo", "ader");
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            headerViewHolder.itemViewLayout.setVisibility(View.GONE);
            headerViewHolder.line.setVisibility(View.GONE);
            headerViewHolder.noDataTextView.setVisibility(View.VISIBLE);
        } else {
            //Log.e("ldadfadfao", "ader");
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            headerViewHolder.noDataTextView.setVisibility(View.GONE);
            headerViewHolder.itemViewLayout.setVisibility(View.VISIBLE);
            headerViewHolder.line.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {

        return reportItemList.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        //Log.e("count", getItemCount() + " dkjf");

        if (position != 0 && position == getItemCount() - 1) {

            if (reportItemList != null) {

                return VIEWTYPE_LOADER;

            }

        }
        if (position == 0) {
            return HEADER_TYPE;
        }
        return VIEWTYPE_ITEM;


    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public class StockBalanceViewHolder extends RecyclerView.ViewHolder {

        TextView stockNameTextView;
        TextView categoryNameTextView;
        //        TextView inventoryQtyTextView;
        //        TextView inProcessQtyTextView;
        TextView totalQtyTextView;
        TextView totalAmtTextView;
        TextView unitTextView;

        public StockBalanceViewHolder(View itemView) {
            super(itemView);

            stockNameTextView = (TextView) itemView.findViewById(R.id.stock_name_text_view);
            categoryNameTextView = (TextView) itemView.findViewById(R.id.category_name_text_view);
            //            inventoryQtyTextView = (TextView) itemView.findViewById(R.id.inventory_qty_text_view);
            //            inProcessQtyTextView = (TextView) itemView.findViewById(R.id.in_process_qty_text_view);
            totalQtyTextView = (TextView) itemView.findViewById(R.id.total_qty_text_view);
            totalAmtTextView = (TextView) itemView.findViewById(R.id.total_amt_text_view);
            unitTextView = (TextView) itemView.findViewById(R.id.unit_text_view);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        LinearLayout itemViewLayout;
        TextView noDataTextView;
        View line;

        public HeaderViewHolder(View itemView) {
            super(itemView);

            itemViewLayout = (LinearLayout) itemView.findViewById(R.id.item_view_layout);
            noDataTextView = (TextView) itemView.findViewById(R.id.no_data_text_view);
            line = itemView.findViewById(R.id.line);
        }
    }
}
