package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.adapters.rvadapterforreports.CustomerSupplier;
import com.digitalfusion.android.pos.database.dao.CustomerDAO;
import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.List;

/**
 * Created by MD002 on 8/29/16.
 */
public class CustomerManager {
    private CustomerDAO customerDAO;
    private Context context;

    public CustomerManager(Context context) {
        this.context = context;
        customerDAO = CustomerDAO.getCustomerDaoInstance(context);
    }

    public Long addNewCustomer(String name, String address, String phoneNo, double balance, InsertedBooleanHolder flag) {
        return customerDAO.addNewCustomer(name, address, phoneNo, balance, flag);
    }

    public boolean checkCustomerAlreadyExists(String name) {
        return customerDAO.checkCustomerAlreadyExists(name);
    }

    public List<Customer> getAllCustomers(int startLimit, int endLimit) {
        return customerDAO.getAllCustomers(startLimit, endLimit, 1);
    }

    public List<Customer> getAllCustomers() {
        return customerDAO.getAllCustomers(1);
    }

    public Customer getCustomerByID(Long id) {
        return customerDAO.getCustomerByID(id);
    }


    public List<CustomerSupplier> getCustomers(int startLimit, int endLimit) {
        return new CustomerSupplier().wrapCustomer(customerDAO.getAllCustomers(startLimit, endLimit, 2));
    }

    public List<Customer> getAllCustomersOustanding(int startLimit, int endLimit) {
        return customerDAO.getAllCustomersOustanding(startLimit, endLimit);
    }

    public Long findIdByName(String name) {
        return customerDAO.findIdByName(name);
    }

    public boolean updateCustomer(String name, String address, String phoneNo, Long id) {
        return customerDAO.updateCustomer(name, address, phoneNo, id);
    }

    public boolean deleteCustomer(Long id) {
        return customerDAO.deleteCustomer(id);
    }

    public boolean checkCustomerBalance(Long id) {
        return customerDAO.checkCustomerBalance(id);
    }

    public List<Customer> getAllCustomersByNameOnSearch(int startLimit, int endLimit, String queryStr) {
        return customerDAO.getAllCustomersByNameOnSearch(startLimit, endLimit, queryStr);
    }


}
