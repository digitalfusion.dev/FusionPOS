package com.digitalfusion.android.pos.adapters.rvadapter;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.ThemeObj;

import java.util.List;

/**
 * Created by MD003 on 11/28/16.
 */

public class RVAdapterForTheme extends RecyclerView.Adapter<RVAdapterForTheme.FilterViewHolder> {


    List<ThemeObj> filterNameList;

    private View mainLayoutView;
    private RVAdapterForTheme.OnItemClickListener mItemClickListener;

    private String currentTheme = "";

    private int activePos = 0;

    private RadioButton oldRb;


    public RVAdapterForTheme(List<ThemeObj> filterNameList) {

        this.filterNameList = filterNameList;

    }

    @Override
    public RVAdapterForTheme.FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mainLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.theme_item_view_rd_btn, parent, false);

        return new RVAdapterForTheme.FilterViewHolder(mainLayoutView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final RVAdapterForTheme.FilterViewHolder holder, final int position) {

        holder.name.setText(filterNameList.get(position).getThemeName());

        Log.w("aa", currentTheme + " ss");
        Log.w("BB", filterNameList.get(position).getThemeNameID() + "ss");


        if (mainLayoutView.getViewTreeObserver().isAlive()) {
            mainLayoutView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    // The view is completely loaded now, so getMeasuredWidth() won't return 0
                    holder.linearLayout.setLayoutParams(new LinearLayout.LayoutParams(holder.linearLayout.getMeasuredWidth(), holder.linearLayout.getMeasuredWidth()));
                    // Destroy the onGlobalLayout afterwards, otherwise it keeps changing
                    // the sizes non-stop, even though it's already done
                }
            });
        }
        if (filterNameList.get(position).getThemeNameID().equalsIgnoreCase(currentTheme)) {

            oldRb = holder.rb;

            activePos = position;

            Log.w("here", "theme acitve" + activePos);

            holder.rb.setChecked(true);

        }
        Drawable originalDrawable = holder.primaryColorView.getBackground();
        Drawable wrappedDrawable  = DrawableCompat.wrap(originalDrawable);
        DrawableCompat.setTintList(wrappedDrawable, holder.primaryColorView.getContext().getResources().getColorStateList(filterNameList.get(position).getPrimaryColor()));
        holder.primaryColorView.setBackground(wrappedDrawable);

        //holder.primaryColorView.setBackgroundTintList(holder.primaryColorView.getContext().getResources().getColorStateList(filterNameList.get(position).getPrimaryColor()));

        Drawable originalDrawable1 = holder.secondaryColorView.getBackground();
        Drawable wrappedDrawable1  = DrawableCompat.wrap(originalDrawable1);
        DrawableCompat.setTintList(wrappedDrawable1, holder.secondaryColorView.getContext().getResources().getColorStateList(filterNameList.get(position).getSecondaryColor()));
        holder.secondaryColorView.setBackground(wrappedDrawable1);

        // holder.primaryColorView.setBackgroundResource(filterNameList.get(position).getPrimaryColor());
        //        holder.secondaryColorView.setBackgroundTintList(holder.secondaryColorView.getContext().getResources().getColorStateList(filterNameList.get(position).getSecondaryColor()));


        //  holder.secondaryColorView.setBackgroundResource(filterNameList.get(position).getSecondaryColor());

        holder.view.setBackgroundResource(filterNameList.get(position).getDrawableId());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mItemClickListener != null) {

                    if (oldRb != null) {

                        oldRb.setChecked(false);

                        oldRb = holder.rb;

                        holder.rb.setChecked(true);

                    } else {

                        oldRb = holder.rb;

                        holder.rb.setChecked(true);

                    }

                    mItemClickListener.onItemClick(v, position);
                }
            }

        });
    }


    public void deSelectedCurrentRb() {

        if (oldRb != null) {
            oldRb.setChecked(false);
        }


    }


    public int getActivePos() {
        return activePos;
    }

    public void setActivePos(int activePos) {
        this.activePos = activePos;
    }

    @Override
    public int getItemCount() {
        return filterNameList.size();
    }

    public void setCurrentTheme(String currentTheme) {
        this.currentTheme = currentTheme;
        notifyDataSetChanged();
    }

    public RVAdapterForTheme.OnItemClickListener getmItemClickListener() {

        return mItemClickListener;

    }

    public void setmItemClickListener(RVAdapterForTheme.OnItemClickListener mItemClickListener) {

        this.mItemClickListener = mItemClickListener;

    }

    public interface OnItemClickListener {

        void onItemClick(View view, int position);

    }

    public class FilterViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        RadioButton rb;

        View itemView;

        View primaryColorView;
        LinearLayout linearLayout;
        View secondaryColorView;

        View view;

        public FilterViewHolder(View itemView) {

            super(itemView);

            this.itemView = itemView;
            linearLayout = mainLayoutView.findViewById(R.id.linear_layout);
            primaryColorView = itemView.findViewById(R.id.primary);
            view = itemView.findViewById(R.id.view);

            secondaryColorView = itemView.findViewById(R.id.secondary);

            rb = (RadioButton) itemView.findViewById(R.id.rb);

            name = (TextView) itemView.findViewById(R.id.name);


        }
    }
}
