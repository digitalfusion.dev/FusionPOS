package com.digitalfusion.android.pos.database.model;

/**
 * Created by hninhsuhantharkyaw on 10/24/16.
 */

public class CustomerOutstanding {
    private Long id;
    private String invoiceNo;
    private Long salesID;
    private Long customerID;
    private String saleInvoiceNo;
    private String day;
    private String month;
    private String year;
    private String date;
    private Double saleBalance;

    public CustomerOutstanding() {
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSalesID() {
        return salesID;
    }

    public void setSalesID(Long salesID) {
        this.salesID = salesID;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public String getSaleInvoiceNo() {
        return saleInvoiceNo;
    }

    public void setSaleInvoiceNo(String saleInvoiceNo) {
        this.saleInvoiceNo = saleInvoiceNo;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getSaleBalance() {
        return saleBalance;
    }

    public void setSaleBalance(Double saleBalance) {
        this.saleBalance = saleBalance;
    }
}
