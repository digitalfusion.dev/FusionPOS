package com.digitalfusion.android.pos.fragments.purchasefragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockCodeAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockNameAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.SupplierAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForSupplierMD;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForTaxMD;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterforStockListMaterialDialog;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForSaleItemViewInSale;
import com.digitalfusion.android.pos.database.business.PurchaseManager;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.business.SupplierManager;
import com.digitalfusion.android.pos.database.model.Stock;
import com.digitalfusion.android.pos.database.model.Supplier;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.PurchaseHeader;
import com.digitalfusion.android.pos.database.model.PurchaseHistory;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.database.model.StockAutoComplete;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.database.model.Tax;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD001 on 1/3/17.
 */

public class PurchaseInReorderFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    private final String DEFAULT_SUPPLIER = "Default";

    //UI components from main
    private View mainLayout;
    private EditText phoneNoEditText;
    private Button addpurchaseItemBtn;
    private RecyclerView purchaseItemRecyclerView;
    private TextView purchaseIdTextView;
    private TextView dateTextView;
    private LinearLayout dateLinearLayout;
    private AutoCompleteTextView supplierTextInputEditText;
    private TextView taxTextView;
    private TextView discountTextView;
    private TextView subtotalTextView;
    private TextView totalTextView;
    private ImageButton discountTextViewBtn;
    private TextView taxTextViewBtn;
    private TextView payTextViewBtn;
    private MaterialDialog addItemMaterialDialog;
    private MaterialDialog editItemMaterialDialog;
    private MaterialDialog addDiscountMaterialDialog;
    private MaterialDialog payMaterialDialog;

    //UI components from add item MD
    private MaterialDialog supplierListMaterialDialog;
    private AppCompatImageButton stockListBtnAddItemMd;
    private AutoCompleteTextView itemCodeTextInputEditTextAddItemMd;
    private AutoCompleteTextView itemNameTextInputEditTextAddItemMd;
    private EditText qtyTextInputEditTextAddItemMd;
    private AppCompatImageButton minusQtyAppCompatImageButtonAddItemMd;
    private AppCompatImageButton plusQtyAppCompatImageButtonAddItemMd;
    private EditText priceTextInputEditTextAddItemMd;
    private SwitchCompat focSwitchCompatAddItemMd;
    private EditText discountTextInputEditTextAddItemMd;
    private MDButton saveMdButtonAddItemMd;

    //UI components from edit item MD
    private MDButton cancelMdButtonAddItemMd;
    private AppCompatImageButton stockListBtnEditItemMd;
    private AutoCompleteTextView itemCodeTextInputEditTextEditItemMd;
    private AutoCompleteTextView itemNameTextInputEditTextEditItemMd;
    private EditText qtyTextInputEditTextEditItemMd;
    private AppCompatImageButton minusQtyAppCompatImageButtonEditItemMd;
    private AppCompatImageButton plusQtyAppCompatImageButtonEditItemMd;
    private EditText priceTextInputEditTextEditItemMd;
    private SwitchCompat focSwitchCompatEditItemMd;
    private EditText discountTextInputEditTextEditItemMd;
    private MDButton saveMdButtonEditItemMd;

    //UI components from disc MD
    private MDButton cancelMdButtonEditItemMd;
    private EditText discountTextInputEditTextMd;
    private MDButton saveDiscountBtn;

    //UI components for Pay MD
    private MDButton cancelDiscountBtn;
    private EditText totalAmountTextInputEditTextPayMd;
    private EditText paidAmountTextInputEditTextPayMd;
    private EditText purchaseOustandting;
    private MDButton saveTransaction;
    private MDButton cancelTransaction;

    // private ImageButton supplierListBtn;
    private TextView supplierPayTextView;
    //Business
    private StockManager stockManager;
    private SettingManager settingManager;
    private PurchaseManager purchaseManager;
    //values
    private Context context;
    private RVSwipeAdapterForSaleItemViewInSale rvSwipeAdapterForPurchaseItemViewInPurchase;
    private List<SalesAndPurchaseItem> purchaseItemViewInSaleList;
    private RVAdapterforStockListMaterialDialog rvAdapterforStockListMaterialDialogEdit;
    private RVAdapterforStockListMaterialDialog rvAdapterforStockListMaterialDialog;
    private List<StockItem> stockItemList;
    private MaterialDialog stockListMaterialDialog;
    private MaterialDialog stockListMaterialDialogEdit;

    //Values
    private SalesAndPurchaseItem purchaseItemInPurchase;
    private String purchaseInvoiceNo;
    private int qty = 0;
    private Boolean foc = false;
    private Double totalAmount = 0.0;
    private Double voucherDiscount = 0.0;
    private Double voucherTax = 0.0;
    private Double subtotal = 0.0;
    private Double paidAmount = 0.0;
    private int editposition;
    private Calendar now;
    private int purchaseDay;
    private int purchaseMonth;
    private int purchaseYear;
    private DatePickerDialog datePickerDialog;
    private String date;
    private RVAdapterForTaxMD rvAdapterForTaxMD;
    private Tax tax;
    private List<Tax> taxList;

    // private AutoCompleteTextView autoCompleteTextView;

    //  private StockCodeAutoCompleteAdapter stockCodeAutoCompleteAdapter;
    private MaterialDialog taxListMaterialDialog;
    private StockCodeAutoCompleteAdapter addstockCodeAutoCompleteAdapter;
    private StockNameAutoCompleteAdapter addstockNameAutoCompleteAdapter;
    private StockNameAutoCompleteAdapter editstockNameAutoCompleteAdapter;
    private StockCodeAutoCompleteAdapter editstockCodeAutoCompleteAdapter;
    private ArrayList<StockAutoComplete> stockAutoCompleteList;
    private MaterialDialog alertMaterialDialog;
    private StockCodeAutoCompleteAdapter.Callback addCallback;

    // private StockCodeAutoCompleteAdapter.Callback addItemCallback;
    private StockCodeAutoCompleteAdapter.Callback editCallback;
    private EditText barCodeTIET;
    private ImageButton barCodeBtn;
    private Boolean barCode = true;
    private String supplierName = DEFAULT_SUPPLIER;

    private SupplierManager supplierManager;

    private SupplierAutoCompleteAdapter supplierAutoCompleteAdapter;

    private ArrayList<Supplier> supplierArrayList;

    private RVAdapterForSupplierMD rvAdapterForSupplierMD;

    private PurchaseHistory purchaseHistory;

    private PurchaseHeader purchaseHeader;

    private boolean isEdit = false;

    private List<SalesAndPurchaseItem> updateList;

    private List<Long> deleteList;

    private String phoneNo;

    private StockItem stockItem;

    private boolean darkmode;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.new_purchase, null);

        stockAutoCompleteList = new ArrayList<>();

        updateList = new ArrayList<>();

        deleteList = new ArrayList<>();

        stockManager = new StockManager(context);

        settingManager = new SettingManager(context);

        purchaseManager = new PurchaseManager(context);

        stockItemList = stockManager.getAllStocks(0, 100);

        purchaseInvoiceNo = purchaseManager.getPurchaseInvoiceNo();

        supplierManager = new SupplierManager(context);

        supplierArrayList = supplierManager.getAllSuppliersArrayList();

        rvAdapterForSupplierMD = new RVAdapterForSupplierMD(supplierArrayList);

        tax = new Tax();

        taxList = settingManager.getAllTaxs();

        tax = taxList.get(0);

        purchaseItemViewInSaleList = new ArrayList<>();

        context = getContext();

        darkmode = POSUtil.isNightMode(context);

        now = Calendar.getInstance();

        buildDatePickerDialog();

        purchaseDay = now.get(Calendar.DAY_OF_MONTH);

        purchaseMonth = now.get(Calendar.MONTH) + 1;

        purchaseYear = now.get(Calendar.YEAR);

        TypedArray purchaseInvoice = context.getTheme().obtainStyledAttributes(new int[]{R.attr.purchase_invoice});

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(purchaseInvoice.getString(0));

        buildSupplierListDialog();

        buildStockListDialog();

        buildDatePickerDialog();

        buildStockListDialogEdit();

        loadAddItemDialog();

        buildTaxListDialogEdit();

        loadEditItemDialog();

        buildAddDiscountDialog();

        buildPayDialog();

        loadUI();

        buildingAlertDialog();
        MainActivity.setCurrentFragment(this);

        return mainLayout;

    }

    private void restoreReorderStockValue() {

        itemCodeTextInputEditTextAddItemMd.setText(stockItem.getCodeNo());
        itemNameTextInputEditTextAddItemMd.setText(stockItem.getName());
        priceTextInputEditTextAddItemMd.setText(stockItem.getPurchasePrice().toString());
        qtyTextInputEditTextAddItemMd.setText("1");
        qty = 1;
        qtyTextInputEditTextAddItemMd.requestFocus();
    }

    private void showAddItemDialog() {

        purchaseItemInPurchase = new SalesAndPurchaseItem();

        addItemMaterialDialog.show();
    }


    public void buildSupplierListDialog() {

        supplierListMaterialDialog = new MaterialDialog.Builder(context).

                adapter(rvAdapterForSupplierMD, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        //supplierTextInputEditText.setText(supplierName);

        configUI();

        configRecyclerView();

        settingOnClick();

        POSUtil.hideKeyboard(mainLayout, context);

        rvAdapterForSupplierMD.setmItemClickListener(new RVAdapterForSupplierMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                supplierTextInputEditText.setText(supplierArrayList.get(position).getName().trim());

                supplierListMaterialDialog.dismiss();

            }
        });


        supplierAutoCompleteAdapter = new SupplierAutoCompleteAdapter(context, supplierArrayList);

        supplierTextInputEditText.setAdapter(supplierAutoCompleteAdapter);

        supplierTextInputEditText.setThreshold(1);

        addCallback = new StockCodeAutoCompleteAdapter.Callback() {
            @Override
            public void onOneResut(final StockAutoComplete stockAutoComplete) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        assigValuesForitemAdd(stockAutoComplete);

                    }
                });

                addPurchaseItem(stockAutoComplete);

            }
        };

        editCallback = new StockCodeAutoCompleteAdapter.Callback() {
            @Override
            public void onOneResut(final StockAutoComplete stockAutoComplete) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        assigValuesForitemEdit(stockAutoComplete);

                    }
                });

                editPurchaseItem(stockAutoComplete);

            }
        };

/*        addItemCallback = new StockCodeAutoCompleteAdapter.Callback() {
            @Override
            public void onOneResut(StockAutoComplete stockAutoCompleteView) {

                purchaseItemInPurchase = new SalesAndPurchaseItem();

                purchaseItemInPurchase.setQty(1);

                purchaseItemInPurchase.setDiscountPercent("0.0%");

                purchaseItemInPurchase.setDiscountAmount(0.0);

                purchaseItemInPurchase.setItemName(stockAutoCompleteView.getItemName());

                purchaseItemInPurchase.setStockCode(stockAutoCompleteView.getCodeNo());

                purchaseItemInPurchase.setPrice(stockAutoCompleteView.getPurchasePrice());

                purchaseItemInPurchase.setTotalPrice(purchaseItemInPurchase.getQty() * purchaseItemInPurchase.getPrice());

                purchaseItemViewInSaleList.add(purchaseItemInPurchase);

                rvSwipeAdapterForPurchaseItemViewInPurchase.setValueList(purchaseItemViewInSaleList);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        rvSwipeAdapterForPurchaseItemViewInPurchase.notifyItemInserted(purchaseItemViewInSaleList.size());

                        //autoCompleteTextView.setSelection(0, autoCompleteTextView.length());

                        updateViewDataForBarScan();

                    }
                });


            }
        };*/


        addstockCodeAutoCompleteAdapter = new StockCodeAutoCompleteAdapter(context, stockManager, addCallback);

        addstockNameAutoCompleteAdapter = new StockNameAutoCompleteAdapter(context, stockManager);

        editstockNameAutoCompleteAdapter = new StockNameAutoCompleteAdapter(context, stockManager);

        editstockCodeAutoCompleteAdapter = new StockCodeAutoCompleteAdapter(context, stockManager, editCallback);

        //stockCodeAutoCompleteAdapter=new StockCodeAutoCompleteAdapter(context,stockManager, addItemCallback);

        // autoCompleteTextView= (AutoCompleteTextView) mainLayout.findViewById(R.id.supplier_name);

        barCodeTIET.setSelectAllOnFocus(true);

        //   autoCompleteTextView.setThreshold(1);

        //  autoCompleteTextView.setAdapter(stockCodeAutoCompleteAdapter);

        itemCodeTextInputEditTextAddItemMd.setThreshold(1);

        itemCodeTextInputEditTextAddItemMd.setAdapter(addstockCodeAutoCompleteAdapter);

        itemCodeTextInputEditTextEditItemMd.setAdapter(editstockCodeAutoCompleteAdapter);


        itemCodeTextInputEditTextAddItemMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockAutoComplete = addstockCodeAutoCompleteAdapter.getSuggestion().get(position);

                assigValuesForitemAdd(stockAutoComplete);

                addPurchaseItem(stockAutoComplete);

            }
        });

        itemCodeTextInputEditTextEditItemMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockAutoComplete = editstockCodeAutoCompleteAdapter.getSuggestion().get(position);

                assigValuesForitemEdit(stockAutoComplete);

                editPurchaseItem(stockAutoComplete);

            }
        });

        itemNameTextInputEditTextAddItemMd.setThreshold(1);

        itemNameTextInputEditTextAddItemMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockAutoComplete = addstockNameAutoCompleteAdapter.getSuggestion().get(position);

                assigValuesForitemAdd(stockAutoComplete);

                addPurchaseItem(stockAutoComplete);

            }
        });

        itemNameTextInputEditTextEditItemMd.setThreshold(1);

        itemNameTextInputEditTextEditItemMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockAutoComplete = editstockNameAutoCompleteAdapter.getSuggestion().get(position);

                assigValuesForitemEdit(stockAutoComplete);

                editPurchaseItem(stockAutoComplete);
            }
        });

       /* barCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(barCode){

                    barCodeBtn.setActivated(true);

                    barCode=false;

                    stockCodeTIL.setVisibility(View.GONE);

                    autoCompleteTextView.setVisibility(View.GONE);

                    barCodeTIL.setVisibility(View.VISIBLE);

                    barCodeTIET.setVisibility(View.VISIBLE);

                    barCodeTIET.requestFocus();

                }else {

                    barCodeBtn.setActivated(false);

                    barCode=true;

                    autoCompleteTextView.setVisibility(View.VISIBLE);

                    barCodeTIET.setVisibility(View.VISIBLE);

                    stockCodeTIL.setVisibility(View.VISIBLE);

                    barCodeTIL.setVisibility(View.GONE);

                    autoCompleteTextView.requestFocus();

                }

            }
        });

        barCodeTIET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                Stock stockVO=stockManager.findStockByStockCode(s.toString());

                if(stockVO.getStockCode()!=null){

                    purchaseItemInPurchase=new SalesAndPurchaseItem();

                    purchaseItemInPurchase.setItemName(stockVO.getItemName());


                    purchaseItemInPurchase.setQty(1);

                    purchaseItemInPurchase.setDiscountPercent("0.0%");

                    purchaseItemInPurchase.setDiscountAmount(0.0);

                    purchaseItemInPurchase.setStockCode(stockVO.getStockCode());

                    purchaseItemInPurchase.setPrice(stockVO.getNormalSalePrice());

                    purchaseItemInPurchase.setTotalPrice(purchaseItemInPurchase.getQty()*purchaseItemInPurchase.getPrice());

                    purchaseItemViewInSaleList.add(purchaseItemInPurchase);

                    rvSwipeAdapterForPurchaseItemViewInPurchase.setValueList(purchaseItemViewInSaleList);

                    rvSwipeAdapterForPurchaseItemViewInPurchase.notifyItemInserted(purchaseItemViewInSaleList.size());

                    barCodeTIET.setSelection(0,barCodeTIET.length());

                    updateViewDataForBarScan();

                }

            }

        });*/

        /*autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockItem = stockCodeAutoCompleteAdapter.getSuggestionList().get(position);

                purchaseItemInPurchase = new SalesAndPurchaseItem();

                purchaseItemInPurchase.setItemName(stockItem.getItemName());

                purchaseItemInPurchase.setQty(1);

                purchaseItemInPurchase.setDiscountPercent("0.0%");

                purchaseItemInPurchase.setDiscountAmount(0.0);

                purchaseItemInPurchase.setStockCode(stockItem.getCodeNo());

                purchaseItemInPurchase.setPrice(stockItem.getPurchasePrice());

                purchaseItemInPurchase.setTotalPrice(purchaseItemInPurchase.getQty() * purchaseItemInPurchase.getPrice());

                purchaseItemViewInSaleList.add(purchaseItemInPurchase);

                rvSwipeAdapterForPurchaseItemViewInPurchase.setValueList(purchaseItemViewInSaleList);

                rvSwipeAdapterForPurchaseItemViewInPurchase.notifyItemInserted(purchaseItemViewInSaleList.size());

                autoCompleteTextView.setText(null);

                updateViewData();

            }
        });*/


        rvAdapterForTaxMD.setmItemClickListener(new RVAdapterForTaxMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                taxListMaterialDialog.dismiss();

                tax = taxList.get(position);

                updateViewData();

            }
        });


        supplierTextInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                supplierName = supplierTextInputEditText.getText().toString().trim();

                if (supplierTextInputEditText.getText().toString().trim() != null && supplierTextInputEditText.getText().toString().trim().length() > 0) {
                    supplierName = supplierTextInputEditText.getText().toString().trim();
                    if (supplierName.equalsIgnoreCase(DEFAULT_SUPPLIER)) {
                        supplierTextInputEditText.setText(null);
                    }

                } else {
                    supplierName = DEFAULT_SUPPLIER;
                }

            }
        });

        if (getArguments() != null) {

            if (getArguments().containsKey("reorderstock")) {

                stockItem = (StockItem) getArguments().getSerializable("reorderstock");

            } else if (getArguments().containsKey("purchaseheader")) {

                purchaseHistory = (PurchaseHistory) getArguments().getSerializable("purchaseheader");

                purchaseHeader = purchaseManager.getPurchaseHeaderByPurchaseID(purchaseHistory.getId(), new InsertedBooleanHolder());
            }

        }


        phoneNoEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                phoneNo = phoneNoEditText.getText().toString();

            }
        });


        supplierTextInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (supplierTextInputEditText.getText().toString().trim() != null && supplierTextInputEditText.getText().toString().trim().length() > 0) {

                    supplierName = supplierTextInputEditText.getText().toString().trim();

                    if (supplierName.equalsIgnoreCase(DEFAULT_SUPPLIER)) {

                        supplierTextInputEditText.setText(null);

                    }

                } else {

                    supplierName = DEFAULT_SUPPLIER;

                }

            }
        });

        if (purchaseHistory != null) {

            isEdit = true;

            initializeOldData();

            refreshRecyclerView();

            configUI();

            updateViewData();

        }


        supplierTextInputEditText.setText(DEFAULT_SUPPLIER);


        restoreReorderStockValue();

        showAddItemDialog();
    }

    public void refreshRecyclerView() {

        rvSwipeAdapterForPurchaseItemViewInPurchase.setSaleItemViewInSaleList(purchaseItemViewInSaleList);

        rvSwipeAdapterForPurchaseItemViewInPurchase.notifyDataSetChanged();

    }

    public void initializeOldData() {

        purchaseHeader = purchaseManager.getPurchaseHeaderByPurchaseID(purchaseHistory.getId(), new InsertedBooleanHolder());

        purchaseInvoiceNo = purchaseHeader.getPurchaseVocherNo();

        paidAmount = purchaseHeader.getPaidAmt();

        purchaseItemViewInSaleList = purchaseManager.getPurchaseDetailsByPurchaseID(purchaseHeader.getId());

        purchaseDay = Integer.parseInt(purchaseHeader.getDay());

        purchaseMonth = Integer.parseInt(purchaseHeader.getMonth());

        purchaseYear = Integer.parseInt(purchaseHeader.getYear());

        now.set(purchaseYear, purchaseMonth - 1, purchaseDay);

        voucherDiscount = purchaseHeader.getDiscountAmt();

        supplierName = purchaseHeader.getSupplierName();


        tax = new Tax(purchaseHeader.getTaxID(), purchaseHeader.getTaxName(), purchaseHeader.getTaxRate(), Tax.TaxType.Exclusive.toString(), "");

    }

    private void addPurchaseItem(StockAutoComplete stockAutoComplete) {

        purchaseItemInPurchase.setDiscountAmount(0.0);

        purchaseItemInPurchase.setBarcode(stockAutoComplete.getBarcode());

        purchaseItemInPurchase.setStockCode(stockAutoComplete.getCodeNo());

        purchaseItemInPurchase.setItemName(stockAutoComplete.getItemName());

        purchaseItemInPurchase.setQty(1);

        qty = 1;

        purchaseItemInPurchase.setStockID(stockAutoComplete.getId());
    }

    private void addPurchaseItem(StockItem stockItem) {

        purchaseItemInPurchase.setDiscountAmount(0.0);

        purchaseItemInPurchase.setBarcode(stockItem.getBarcode());

        purchaseItemInPurchase.setStockCode(stockItem.getCodeNo());

        purchaseItemInPurchase.setItemName(stockItem.getName());


        purchaseItemInPurchase.setQty(1);

        qty = 1;

        purchaseItemInPurchase.setStockID(stockItem.getId());

    }

    private void updateViewDataForBarScan() {

        calculateValues();

        //   new Handler().postDelayed(new Runnable() {
        //       @Override
        //       public void run() {
        //           barCodeTIET.setText(null);
        //       }
        //   },1000);


        taxTextView.setText(POSUtil.PercentFromat(tax.getRate()));

        discountTextView.setText(POSUtil.PercentFromat((voucherDiscount)));

        subtotalTextView.setText(POSUtil.PercentFromat(subtotal));

        totalTextView.setText(POSUtil.PercentFromat(totalAmount));

    }


    private void buildingAlertDialog() {

        String noItem = context.getTheme().obtainStyledAttributes(new int[]{R.attr.there_is_no_item}).getString(0);
        alertMaterialDialog = new MaterialDialog.Builder(context).

                title(noItem).

                build();

    }

    public void configUI() {

        purchaseIdTextView.setText(purchaseInvoiceNo);

        supplierTextInputEditText.setText(supplierName);

        configDateUI();

    }

    public void configDateUI() {

        date = DateUtility.makeDate(Integer.toString(purchaseYear), Integer.toString(purchaseMonth), Integer.toString(purchaseDay));

        //String dayDes[] = DateUtility.dayDes(date);

        //String yearMonthDes = DateUtility.monthYearDes(date);

        //  dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

        dateTextView.setText(DateUtility.makeDateFormatWithSlash(date));

    }

    public void buildTaxListDialogEdit() {

        rvAdapterForTaxMD = new RVAdapterForTaxMD(taxList, purchaseHeader.getTaxID());

        taxListMaterialDialog = new MaterialDialog.Builder(context).

                adapter(rvAdapterForTaxMD, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

    }

    public void buildDatePickerDialog() {

        datePickerDialog = DatePickerDialog.newInstance(

                PurchaseInReorderFragment.this,

                now.get(Calendar.YEAR),

                now.get(Calendar.MONTH),

                now.get(Calendar.DAY_OF_MONTH)

        );


        if (darkmode)
            datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        datePickerDialog.setThemeDark(darkmode);


    }

    public void configRecyclerView() {

        rvSwipeAdapterForPurchaseItemViewInPurchase = new RVSwipeAdapterForSaleItemViewInSale(purchaseItemViewInSaleList, context);

        purchaseItemRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        purchaseItemRecyclerView.setAdapter(rvSwipeAdapterForPurchaseItemViewInPurchase);

    }

    public void settingOnClick() {


        qtyTextInputEditTextAddItemMd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (qtyTextInputEditTextAddItemMd.getText().toString().trim().length() > 0) {
                    qty = Integer.parseInt(qtyTextInputEditTextAddItemMd.getText().toString().trim());
                }

            }
        });

        qtyTextInputEditTextEditItemMd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (qtyTextInputEditTextEditItemMd.getText().toString().trim().length() > 0) {
                    qty = Integer.parseInt(qtyTextInputEditTextEditItemMd.getText().toString().trim());
                }

            }
        });


        taxTextViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taxListMaterialDialog.show();
            }
        });

        dateLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog.show(getActivity().getFragmentManager(), "PurchasePick");

            }
        });

        addpurchaseItemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                foc = false;

                purchaseItemInPurchase = new SalesAndPurchaseItem();

                clearInputAddItemMaterialDialog();

                addItemMaterialDialog.show();

            }
        });

        rvSwipeAdapterForPurchaseItemViewInPurchase.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                editposition = postion;

                setInputEditItemMaterialDialog();

                editItemMaterialDialog.show();

            }
        });

        rvSwipeAdapterForPurchaseItemViewInPurchase.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                deleteList.add(purchaseItemViewInSaleList.get(postion).getId());

                purchaseItemViewInSaleList.remove(postion);

                rvSwipeAdapterForPurchaseItemViewInPurchase.setSaleItemViewInSaleList(purchaseItemViewInSaleList);

                rvSwipeAdapterForPurchaseItemViewInPurchase.notifyItemRemoved(postion);

                updateViewData();

            }
        });

        discountTextViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addDiscountMaterialDialog.show();

            }
        });

        payTextViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setDataForPayMd();

                if (purchaseItemViewInSaleList.size() < 1) {

                    //alertMaterialDialog.show();

                } else {

                    POSUtil.showKeyboard(paidAmountTextInputEditTextPayMd);

                    payMaterialDialog.show();

                }

            }
        });

        payMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                POSUtil.hideKeyboard(paidAmountTextInputEditTextPayMd, context);
            }
        });

        saveTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEdit) {

                    if (checkPayValidation()) {

                        Double balance = totalAmount - paidAmount;


                        if (balance < 0) {

                            balance = 0.0;

                        }


                        Log.w("paid amount", paidAmount + " edit ");

                        boolean status = purchaseManager.updatePurchaseByID(purchaseHeader.getId(), date, supplierName, phoneNo, "",totalAmount,

                                "", tax.getId(), subtotal, voucherDiscount, "No Remark", paidAmount, balance, voucherTax, Integer.toString(purchaseDay),

                                Integer.toString(purchaseMonth), Integer.toString(purchaseYear), tax.getRate(), updateList, deleteList, 1l);

                        if (status) {

                            MainActivity.replacingFragment(new PurchaseHistoryListFragment());

                            payMaterialDialog.dismiss();

                        }
                    }


                } else {
                    if (checkPayValidation()) {

                        Double balance = totalAmount - paidAmount;


                        if (balance < 0) {

                            balance = 0.0;

                        }


                        boolean status = purchaseManager.addNewPurchase(purchaseInvoiceNo, date, supplierName, phoneNo,"",
                                "", totalAmount, "", tax.getId(), subtotal, voucherDiscount, "No remark", paidAmount,
                                balance, voucherTax, Integer.toString(purchaseDay),
                                Integer.toString(purchaseMonth), Integer.toString(purchaseYear), tax.getRate(), purchaseItemViewInSaleList, tax.getType(), 1l, AppConstant.currentUserId);

                        if (status) {

                            payMaterialDialog.dismiss();

                            MainActivity.replacingFragment(new AddEditNewPurchaseFragment());

                        }

                    }
                }


            }
        });

        settingOnClickForEditSaleItem();

        settingOnClickForAddSaleItemDialog();

        settingOnClickForPayMd();

        settingOnClickForDiscountMD();

    }


    public boolean checkPayValidation() {

        boolean status = true;

        if (paidAmountTextInputEditTextPayMd.getText().toString().trim().length() < 1) {

            status = false;

            String enterPaidAmt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_paid_amount}).getString(0);
            paidAmountTextInputEditTextPayMd.setError(enterPaidAmt);

        } else {

            paidAmount = Double.valueOf(paidAmountTextInputEditTextPayMd.getText().toString());

        }

        return status;

    }

    private void setDataForPayMd() {

        supplierPayTextView.setText(supplierName);

        paidAmountTextInputEditTextPayMd.setError(null);

        totalAmountTextInputEditTextPayMd.setEnabled(false);

        totalAmountTextInputEditTextPayMd.setText(totalAmount.toString());

        if (isEdit) {
            paidAmountTextInputEditTextPayMd.setText(paidAmount.toString());
        } else {
            paidAmountTextInputEditTextPayMd.setText(totalAmount.toString());
        }

        paidAmountTextInputEditTextPayMd.setSelectAllOnFocus(true);


    }


    private void settingOnClickForPayMd() {

        paidAmountTextInputEditTextPayMd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (paidAmountTextInputEditTextPayMd.getText().toString().trim().length() > 0) {

                    Double changeAmount = Double.parseDouble(paidAmountTextInputEditTextPayMd.getText().toString()) - totalAmount;

                    if (changeAmount > 0) {

                        purchaseOustandting.setText("0.0");

                    } else {

                        purchaseOustandting.setText(Double.toString(Math.abs(changeAmount)));

                    }

                }

            }
        });

    }

    private void settingOnClickForDiscountMD() {

        saveDiscountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (discountTextInputEditTextMd.getText().toString().trim().length() > 1) {

                    voucherDiscount = Double.parseDouble(discountTextInputEditTextMd.getText().toString().trim());

                } else {

                    voucherDiscount = 0.0;

                }

                updateViewData();

                addDiscountMaterialDialog.dismiss();

            }
        });

    }

    private void settingOnClickForEditSaleItem() {

        stockListBtnEditItemMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stockListMaterialDialogEdit.show();
            }
        });

        rvAdapterforStockListMaterialDialogEdit.setmItemClickListener(new RVAdapterforStockListMaterialDialog.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                assigValuesForitemEdit(stockItemList.get(position));

                editPurchaseItem(stockItemList.get(position));

                stockListMaterialDialogEdit.dismiss();

            }
        });

        minusQtyAppCompatImageButtonEditItemMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (qtyTextInputEditTextEditItemMd.getText().toString().trim().length() > 0) {

                    qty = Integer.parseInt(qtyTextInputEditTextEditItemMd.getText().toString());

                }

                if (qty > 0) {

                    qty--;

                    qtyTextInputEditTextEditItemMd.setText(Integer.toString(qty));

                    qtyTextInputEditTextEditItemMd.setSelection(qtyTextInputEditTextEditItemMd.getText().length());

                }

            }
        });

        plusQtyAppCompatImageButtonEditItemMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (qtyTextInputEditTextEditItemMd.getText().toString().trim().length() > 0) {

                    qty = Integer.parseInt(qtyTextInputEditTextEditItemMd.getText().toString());

                }

                if (qty >= 0) {

                    qty++;

                    qtyTextInputEditTextEditItemMd.setText(Integer.toString(qty));

                    qtyTextInputEditTextEditItemMd.setSelection(qtyTextInputEditTextEditItemMd.getText().length());

                }

            }
        });

        focSwitchCompatEditItemMd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                foc = isChecked;

                if (foc) {

                    discountTextInputEditTextEditItemMd.setText("100%");

                } else {

                    discountTextInputEditTextEditItemMd.setText(null);

                }

            }
        });

        saveMdButtonEditItemMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getValuesFromEditSaleItemDialog();

                rvSwipeAdapterForPurchaseItemViewInPurchase.notifyItemChanged(editposition);

                editItemMaterialDialog.dismiss();

                if (!updateList.contains(purchaseItemViewInSaleList.get(editposition))) {

                    updateList.add(purchaseItemViewInSaleList.get(editposition));

                }


                updateViewData();

            }
        });

        discountTextInputEditTextAddItemMd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (discountTextInputEditTextAddItemMd.getText().toString().trim().length() > 1) {

                    if (discountTextInputEditTextAddItemMd.getText().toString().trim().contains("%")) {

                    } else {

                        purchaseItemInPurchase.setDiscountAmount(Double.parseDouble(discountTextInputEditTextAddItemMd.toString().trim()));

                    }

                }
            }
        });
    }

    private void editPurchaseItem(StockItem stockItem) {

        purchaseItemViewInSaleList.get(editposition).setDiscountAmount(0.0);

        purchaseItemViewInSaleList.get(editposition).setBarcode(stockItem.getBarcode());

        purchaseItemViewInSaleList.get(editposition).setStockCode(stockItem.getCodeNo());

        purchaseItemViewInSaleList.get(editposition).setItemName(stockItem.getName());

        purchaseItemViewInSaleList.get(editposition).setQty(1);

        qty = 1;

        purchaseItemViewInSaleList.get(editposition).setStockID(stockItem.getId());
    }

    private void editPurchaseItem(StockAutoComplete stockView) {

        purchaseItemViewInSaleList.get(editposition).setDiscountAmount(0.0);

        purchaseItemViewInSaleList.get(editposition).setBarcode(stockView.getBarcode());

        purchaseItemViewInSaleList.get(editposition).setStockCode(stockView.getCodeNo());

        purchaseItemViewInSaleList.get(editposition).setItemName(stockView.getItemName());

        purchaseItemViewInSaleList.get(editposition).setQty(1);

        qty = 1;

        purchaseItemViewInSaleList.get(editposition).setStockID(stockView.getId());
    }


    public void settingOnClickForAddSaleItemDialog() {

        stockListBtnAddItemMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stockListMaterialDialog.show();
            }
        });

        rvAdapterforStockListMaterialDialog.setmItemClickListener(new RVAdapterforStockListMaterialDialog.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                assigValuesForitemAdd(stockItemList.get(position));

                addPurchaseItem(stockItemList.get(position));

                stockListMaterialDialog.dismiss();

            }
        });

        minusQtyAppCompatImageButtonAddItemMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (qtyTextInputEditTextAddItemMd.getText().toString().trim().length() > 0) {

                    qty = Integer.parseInt(qtyTextInputEditTextAddItemMd.getText().toString());

                }

                if (qty > 0) {

                    qty--;

                    qtyTextInputEditTextAddItemMd.setText(Integer.toString(qty));

                    qtyTextInputEditTextAddItemMd.setSelection(qtyTextInputEditTextAddItemMd.getText().length());

                }

            }
        });

        plusQtyAppCompatImageButtonAddItemMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (qtyTextInputEditTextAddItemMd.getText().toString().trim().length() > 0) {

                    qty = Integer.parseInt(qtyTextInputEditTextAddItemMd.getText().toString());

                }

                if (qty >= 0) {

                    qty++;

                    qtyTextInputEditTextAddItemMd.setText(Integer.toString(qty));

                    qtyTextInputEditTextAddItemMd.setSelection(qtyTextInputEditTextAddItemMd.getText().length());

                }

            }
        });


        focSwitchCompatAddItemMd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                foc = isChecked;

                if (foc) {

                    discountTextInputEditTextAddItemMd.setText("100%");

                } else {

                    discountTextInputEditTextAddItemMd.setText(null);

                }

            }
        });

        saveMdButtonAddItemMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkVaildationFroAddSaleItem()) {

                    getValuesFromAddPurchaseItemDialog();

                    purchaseItemViewInSaleList.add(purchaseItemInPurchase);

                    rvSwipeAdapterForPurchaseItemViewInPurchase.notifyDataSetChanged();

                    addItemMaterialDialog.dismiss();

                    updateViewData();

                    updateList.add(purchaseItemInPurchase);
                }

            }
        });

        discountTextInputEditTextAddItemMd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (discountTextInputEditTextAddItemMd.getText().toString().trim().length() > 1) {

                    if (discountTextInputEditTextAddItemMd.getText().toString().trim().contains("%")) {

                    } else {

                        purchaseItemInPurchase.setDiscountAmount(Double.parseDouble(discountTextInputEditTextAddItemMd.toString().trim()));

                    }

                }
            }
        });

    }

    public void getValuesFromAddPurchaseItemDialog() {

        purchaseItemInPurchase.setQty(qty);

        purchaseItemInPurchase.setPrice(Double.parseDouble(priceTextInputEditTextAddItemMd.getText().toString().trim()));

        purchaseItemInPurchase.setItemName(itemNameTextInputEditTextAddItemMd.getText().toString().trim());

        purchaseItemInPurchase.setStockCode(itemCodeTextInputEditTextAddItemMd.getText().toString().trim());

        Stock stock = stockManager.findStockByStockCode(purchaseItemInPurchase.getStockCode());

        if (discountTextInputEditTextAddItemMd.getText().toString().trim().length() > 0) {

            purchaseItemInPurchase.setDiscountAmount(Double.parseDouble(discountTextInputEditTextAddItemMd.getText().toString().trim()));

        } else {
            purchaseItemInPurchase.setDiscountAmount(0.0);
        }

        if (stock.getStockCode() == null) {
            Log.w("hello price", purchaseItemInPurchase.getPrice() + "SS");

            purchaseItemInPurchase.setStockID(stockManager.addQuickStockSetupPurchase(purchaseItemInPurchase.getItemName(), purchaseItemInPurchase.getStockCode(),
                    purchaseItemInPurchase.getPrice(), 0.0));

        }

        purchaseItemInPurchase.setTotalPrice(calculatePrice(purchaseItemInPurchase.getPrice(), qty, purchaseItemInPurchase.getDiscountAmount()));

    }


    public void getValuesFromEditSaleItemDialog() {

        purchaseItemViewInSaleList.get(editposition).setQty(qty);

        purchaseItemViewInSaleList.get(editposition).setPrice(Double.parseDouble(priceTextInputEditTextEditItemMd.getText().toString().trim()));

        purchaseItemViewInSaleList.get(editposition).setTotalPrice(calculatePrice(purchaseItemViewInSaleList.get(editposition).getPrice(), qty, purchaseItemViewInSaleList.get(editposition).getDiscountAmount()));

    }

    public boolean checkVaildationFroAddSaleItem() {

        boolean status = true;

        if (itemCodeTextInputEditTextAddItemMd.getText().toString().trim().length() < 1) {

            status = false;

            String t = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_itemcode_or_choose}).getString(0);
            itemCodeTextInputEditTextAddItemMd.setError(t);
        }

        if (itemNameTextInputEditTextAddItemMd.getText().toString().trim().length() < 1) {

            status = false;

            String t = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_name_or_choose}).getString(0);
            itemNameTextInputEditTextAddItemMd.setError(t);

        }


        if (priceTextInputEditTextAddItemMd.getText().toString().trim().length() < 1) {

            status = false;

            String t = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_price_or_choose}).getString(0);
            priceTextInputEditTextAddItemMd.setError(t);

        }
        if (qtyTextInputEditTextAddItemMd.getText().toString().trim().length() < 1 || Integer.parseInt(qtyTextInputEditTextAddItemMd.getText().toString().trim()) < 1) {

            status = false;

            String t = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_qty_greater_than_0}).getString(0);
            qtyTextInputEditTextAddItemMd.setError(t);

        }

        return status;

    }

    public void assigValuesForitemAdd(StockItem stockItem) {


        if (!itemCodeTextInputEditTextAddItemMd.getText().toString().trim().equalsIgnoreCase(stockItem.getCodeNo())) {

            itemCodeTextInputEditTextAddItemMd.setText(stockItem.getCodeNo());

        }
        itemCodeTextInputEditTextAddItemMd.setSelection(stockItem.getCodeNo().length());

        itemNameTextInputEditTextAddItemMd.setText(stockItem.getName());

        itemNameTextInputEditTextAddItemMd.setSelection(stockItem.getName().length());

        qtyTextInputEditTextAddItemMd.setText("1");

        foc = false;

        priceTextInputEditTextAddItemMd.setText(stockItem.getPurchasePrice().toString());

        priceTextInputEditTextAddItemMd.setSelection(stockItem.getPurchasePrice().toString().length());

        focSwitchCompatAddItemMd.setChecked(false);

        discountTextInputEditTextAddItemMd.setText(null);


    }


    public void assigValuesForitemAdd(StockAutoComplete stockView) {

        if (!itemCodeTextInputEditTextAddItemMd.getText().toString().trim().equalsIgnoreCase(stockView.getCodeNo())) {

            itemCodeTextInputEditTextAddItemMd.setText(stockView.getCodeNo());

        }

        itemCodeTextInputEditTextAddItemMd.setSelection(itemCodeTextInputEditTextAddItemMd.getText().toString().length());

        itemNameTextInputEditTextAddItemMd.setText(stockView.getItemName());

        itemNameTextInputEditTextAddItemMd.setSelection(stockView.getItemName().length());

        qtyTextInputEditTextAddItemMd.setText("1");

        foc = false;

        priceTextInputEditTextAddItemMd.setText(stockView.getPurchasePrice().toString());

        priceTextInputEditTextAddItemMd.setSelection(stockView.getPurchasePrice().toString().length());

        focSwitchCompatAddItemMd.setChecked(false);

        discountTextInputEditTextAddItemMd.setText(null);

    }


    public void assigValuesForitemEdit(StockItem stockItem) {

        if (!itemCodeTextInputEditTextEditItemMd.getText().toString().trim().equalsIgnoreCase(stockItem.getCodeNo())) {

            itemCodeTextInputEditTextEditItemMd.setText(stockItem.getCodeNo());

        }

        itemCodeTextInputEditTextEditItemMd.setSelection(itemCodeTextInputEditTextAddItemMd.getText().toString().length());

        itemNameTextInputEditTextEditItemMd.setText(stockItem.getName());

        itemNameTextInputEditTextEditItemMd.setSelection(stockItem.getName().length());

        qtyTextInputEditTextEditItemMd.setText("1");

        foc = false;

        priceTextInputEditTextEditItemMd.setText(stockItem.getPurchasePrice().toString());

        priceTextInputEditTextEditItemMd.setSelection(stockItem.getPurchasePrice().toString().length());

        focSwitchCompatEditItemMd.setChecked(false);

        discountTextInputEditTextEditItemMd.setText(null);

    }


    public void assigValuesForitemEdit(StockAutoComplete stockView) {

        if (!itemCodeTextInputEditTextEditItemMd.getText().toString().trim().equalsIgnoreCase(stockView.getCodeNo())) {

            itemCodeTextInputEditTextEditItemMd.setText(stockView.getCodeNo());

        }

        itemCodeTextInputEditTextEditItemMd.setSelection(itemCodeTextInputEditTextEditItemMd.getText().toString().length());

        itemNameTextInputEditTextEditItemMd.setText(stockView.getItemName());

        itemNameTextInputEditTextEditItemMd.setSelection(stockView.getItemName().length());

        qtyTextInputEditTextEditItemMd.setText("1");

        foc = false;

        priceTextInputEditTextEditItemMd.setText(stockView.getPurchasePrice().toString());

        priceTextInputEditTextEditItemMd.setSelection(stockView.getPurchasePrice().toString().length());

        focSwitchCompatEditItemMd.setChecked(false);

        discountTextInputEditTextEditItemMd.setText(null);

    }

    private void loadUI() {

        phoneNoEditText = (EditText) mainLayout.findViewById(R.id.phone_no);

        supplierTextInputEditText = (AppCompatAutoCompleteTextView) mainLayout.findViewById(R.id.supplier_name);

        barCodeTIET = (EditText) mainLayout.findViewById(R.id.bar_code_TIET);

        barCodeBtn = (ImageButton) mainLayout.findViewById(R.id.bar_code_btn);

        dateLinearLayout = (LinearLayout) mainLayout.findViewById(R.id.purchase_date_in_new_purchase_ll);

        purchaseItemRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.purchase_item_list_rv);

        purchaseIdTextView = (TextView) mainLayout.findViewById(R.id.purchase_id_in_new_purcahse_tv);

        dateTextView = (TextView) mainLayout.findViewById(R.id.purchase_date_in_new_purchase_tv);

        taxTextView = (TextView) mainLayout.findViewById(R.id.tax_amt_in_new_purchase_tv);

        discountTextView = (TextView) mainLayout.findViewById(R.id.discount_in_new_purchase_tv);

        subtotalTextView = (TextView) mainLayout.findViewById(R.id.subtotal_in_new_purchase_tv);

        totalTextView = (TextView) mainLayout.findViewById(R.id.total_in_new_purchase_tv);

        discountTextViewBtn = (ImageButton) mainLayout.findViewById(R.id.discount_in_new_purchase_tv_btn);

        taxTextViewBtn = (TextView) mainLayout.findViewById(R.id.tax_in_new_purchase_tv_btn);

        addpurchaseItemBtn = (Button) mainLayout.findViewById(R.id.add_item_in_new_purchase_btn);

        payTextViewBtn = (TextView) mainLayout.findViewById(R.id.pay_in_new_purchase);

        loadUIFromAddItemMaterialDialog();

        loadUIFromEditItemMaterialDialog();

        discountTextInputEditTextMd = (EditText) addDiscountMaterialDialog.findViewById(R.id.add_vocher_disc_in_sale_md);

        saveDiscountBtn = addDiscountMaterialDialog.getActionButton(DialogAction.POSITIVE);

        cancelDiscountBtn = addDiscountMaterialDialog.getActionButton(DialogAction.NEGATIVE);

        totalAmountTextInputEditTextPayMd = (EditText) payMaterialDialog.findViewById(R.id.total_amount_in_purcahse_change);

        paidAmountTextInputEditTextPayMd = (EditText) payMaterialDialog.findViewById(R.id.paid_amount_in_purchase_change);

        purchaseOustandting = (EditText) payMaterialDialog.findViewById(R.id.oustanding_in_purchase_change);

        saveTransaction = payMaterialDialog.getActionButton(DialogAction.POSITIVE);

        cancelTransaction = payMaterialDialog.getActionButton(DialogAction.NEGATIVE);

        supplierPayTextView = (TextView) payMaterialDialog.findViewById(R.id.supplier_pay);

    }


    public void loadAddItemDialog() {

        TypedArray addPurchaseItem = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_purchase_item});
        //TypedArray save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        addItemMaterialDialog = new MaterialDialog.Builder(context).

                customView(R.layout.add_item_sale_purchase, true)
                .title(addPurchaseItem.getString(0))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").

                //positiveText(save.getString(0)).

                //negativeText(cancel.getString(0)).

                        build();

    }

    public void loadEditItemDialog() {

        TypedArray editPurchaseItem = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_purchase_item});
        //TypedArray save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        editItemMaterialDialog = new MaterialDialog.Builder(context).

                customView(R.layout.add_item_sale_purchase, true)
                .title(editPurchaseItem.getString(0)).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").

                //positiveText(save.getString(0)).

                //negativeText(cancel.getString(0)).

                        build();

    }

    public void clearInputAddItemMaterialDialog() {

        itemCodeTextInputEditTextAddItemMd.setError(null);

        itemCodeTextInputEditTextAddItemMd.setText(null);

        itemNameTextInputEditTextAddItemMd.setText(null);

        itemNameTextInputEditTextAddItemMd.setError(null);

        qtyTextInputEditTextAddItemMd.setText("0");

        priceTextInputEditTextAddItemMd.setText(null);

        priceTextInputEditTextAddItemMd.setError(null);

        qtyTextInputEditTextAddItemMd.setError(null);

        focSwitchCompatAddItemMd.setChecked(false);

        discountTextInputEditTextAddItemMd.setText(null);

    }

    public void setInputEditItemMaterialDialog() {

        itemCodeTextInputEditTextEditItemMd.setText(purchaseItemViewInSaleList.get(editposition).getStockCode());

        itemNameTextInputEditTextEditItemMd.setText(purchaseItemViewInSaleList.get(editposition).getItemName());


        qtyTextInputEditTextEditItemMd.setText(Integer.toString(purchaseItemViewInSaleList.get(editposition).getQty()));

        priceTextInputEditTextEditItemMd.setText(Double.toString(purchaseItemViewInSaleList.get(editposition).getPrice()));

        focSwitchCompatEditItemMd.setChecked(false);

        discountTextInputEditTextEditItemMd.setText(Double.toString(purchaseItemViewInSaleList.get(editposition).getDiscountAmount()));

    }

    public void loadUIFromAddItemMaterialDialog() {

        stockListBtnAddItemMd = (AppCompatImageButton) addItemMaterialDialog.findViewById(R.id.stock_list_in_add_sale_item_btn);

        itemCodeTextInputEditTextAddItemMd = (AutoCompleteTextView) addItemMaterialDialog.findViewById(R.id.code_no_in_sale_purchase_TIET);

        itemNameTextInputEditTextAddItemMd = (AutoCompleteTextView) addItemMaterialDialog.findViewById(R.id.item_name_in_sale_purchase_TIET);

        qtyTextInputEditTextAddItemMd = (EditText) addItemMaterialDialog.findViewById(R.id.qty_in_sale_purchase_TIET);
        minusQtyAppCompatImageButtonAddItemMd = (AppCompatImageButton) addItemMaterialDialog.findViewById(R.id.minus_qty_in_sale_purchase_ImgBtn);

        plusQtyAppCompatImageButtonAddItemMd = (AppCompatImageButton) addItemMaterialDialog.findViewById(R.id.plus_qty_in_sale_purchase_ImgBtn);

        priceTextInputEditTextAddItemMd = (EditText) addItemMaterialDialog.findViewById(R.id.sale_price_in_sale_purchase_TIET);

        focSwitchCompatAddItemMd = (SwitchCompat) addItemMaterialDialog.findViewById(R.id.foc_in_sale_purchase_SC);

        discountTextInputEditTextAddItemMd = (EditText) addItemMaterialDialog.findViewById(R.id.item_discount_in_sale_purchase_TIET);

        saveMdButtonAddItemMd = addItemMaterialDialog.getActionButton(DialogAction.POSITIVE);

        cancelMdButtonAddItemMd = addItemMaterialDialog.getActionButton(DialogAction.NEGATIVE);

    }

    public void loadUIFromEditItemMaterialDialog() {

        stockListBtnEditItemMd = (AppCompatImageButton) editItemMaterialDialog.findViewById(R.id.stock_list_in_add_sale_item_btn);

        itemCodeTextInputEditTextEditItemMd = (AutoCompleteTextView) editItemMaterialDialog.findViewById(R.id.code_no_in_sale_purchase_TIET);

        itemNameTextInputEditTextEditItemMd = (AutoCompleteTextView) editItemMaterialDialog.findViewById(R.id.item_name_in_sale_purchase_TIET);

        qtyTextInputEditTextEditItemMd = (EditText) editItemMaterialDialog.findViewById(R.id.qty_in_sale_purchase_TIET);

        minusQtyAppCompatImageButtonEditItemMd = (AppCompatImageButton) editItemMaterialDialog.findViewById(R.id.minus_qty_in_sale_purchase_ImgBtn);

        plusQtyAppCompatImageButtonEditItemMd = (AppCompatImageButton) editItemMaterialDialog.findViewById(R.id.plus_qty_in_sale_purchase_ImgBtn);

        priceTextInputEditTextEditItemMd = (EditText) editItemMaterialDialog.findViewById(R.id.sale_price_in_sale_purchase_TIET);

        focSwitchCompatEditItemMd = (SwitchCompat) editItemMaterialDialog.findViewById(R.id.foc_in_sale_purchase_SC);

        discountTextInputEditTextEditItemMd = (EditText) editItemMaterialDialog.findViewById(R.id.item_discount_in_sale_purchase_TIET);

        saveMdButtonEditItemMd = editItemMaterialDialog.getActionButton(DialogAction.POSITIVE);

        cancelMdButtonEditItemMd = editItemMaterialDialog.getActionButton(DialogAction.NEGATIVE);

    }

    public void buildStockListDialog() {

        rvAdapterforStockListMaterialDialog = new RVAdapterforStockListMaterialDialog(stockItemList);

        stockListMaterialDialog = new MaterialDialog.Builder(context).

                adapter(rvAdapterforStockListMaterialDialog, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    public void buildStockListDialogEdit() {

        rvAdapterforStockListMaterialDialogEdit = new RVAdapterforStockListMaterialDialog(stockItemList);

        stockListMaterialDialogEdit = new MaterialDialog.Builder(context).

                adapter(rvAdapterforStockListMaterialDialogEdit, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    public void buildAddDiscountDialog() {

        TypedArray discount = context.getTheme().obtainStyledAttributes(new int[]{R.attr.discount});
        //TypedArray save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        addDiscountMaterialDialog = new MaterialDialog.Builder(context).

                customView(R.layout.add_discount_dialog, true).title(discount.getString(0)).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").

                //positiveText(save.getString(0)).

                //negativeText(cancel.getString(0)).

                        build();
    }

    public void buildPayDialog() {

        TypedArray payment = context.getTheme().obtainStyledAttributes(new int[]{R.attr.payment});
        //TypedArray save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        payMaterialDialog = new MaterialDialog.Builder(context).

                customView(R.layout.purchase_pay, true).title(payment.getString(0)).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").

                //positiveText(save.getString(0)).

                //negativeText(cancel.getString(0)).

                        build();

    }


    private Double calculatePrice(Double salePrice, int qty, Double discAmt) {

        Double totalPrice = 0.0;

        if (foc) {

            return 0.0;

        } else {

            salePrice -= discAmt;

            totalPrice = qty * salePrice;

            return totalPrice;

        }

    }

    private void updateViewData() {

        calculateValues();

        taxTextView.setText(tax.getRate() + "");

        discountTextView.setText(voucherDiscount.toString());

        subtotalTextView.setText(subtotal.toString());

        totalTextView.setText(totalAmount.toString());

    }

    private void calculateValues() {

        subtotal = 0.0;

        for (SalesAndPurchaseItem s : purchaseItemViewInSaleList) {

            subtotal += s.getTotalPrice();

        }

        if (tax.getType().equalsIgnoreCase("Inclusive")) {

            voucherTax = 0.0;

        } else {

            voucherTax = (subtotal / 100) * tax.getRate();

        }

        totalAmount = subtotal + voucherTax - voucherDiscount;

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        purchaseDay = dayOfMonth;

        purchaseMonth = monthOfYear + 1;

        this.purchaseYear = year;

        date = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

        String dayDes[] = DateUtility.dayDes(date);

        String yearMonthDes = DateUtility.monthYearDes(date);

        // dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

        dateTextView.setText(DateUtility.makeDateFormatWithSlash(date));

    }


}
