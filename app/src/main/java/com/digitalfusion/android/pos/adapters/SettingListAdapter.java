package com.digitalfusion.android.pos.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.SettingList;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int LANGUAGE_VIEW = 1;
    private ArrayList<SettingList> settingLists;
    private ClickListener clickListener;
    private ViewHolder themeSettingViewHolder;
    private LanguageViewOnCheckChangeListener languageViewOnCheckChangeListener;

    public SettingListAdapter(ArrayList<SettingList> settingList) {
        this.settingLists = settingList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (LANGUAGE_VIEW == viewType) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.language, viewGroup, false);
            return new LanguageViewHolder(v);
        } else {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.setting_item_view, viewGroup, false);
            return new ViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int i) {

        if (viewHolder instanceof SettingListAdapter.ViewHolder) {
            ViewHolder settingHolder = (ViewHolder) viewHolder;
            settingHolder.txt.setText(settingLists.get(i).getSettingName());
            settingHolder.photo.setImageDrawable(settingLists.get(i).getSettingImage());
            settingHolder.descriptionTextView.setHint(settingHolder.nodes);

            if (settingLists.get(i).getSettingName().equalsIgnoreCase("Theme")) {
                themeSettingViewHolder = settingHolder;
            }

            viewHolder.itemView.setOnClickListener(v -> {
                if (clickListener != null) {
                    clickListener.onClick(i);
                }
            });
        } else if (viewHolder instanceof LanguageViewHolder) {
            ((LanguageViewHolder) viewHolder).languageSwitch.setOnCheckedChangeListener(languageViewOnCheckChangeListener::onCheckedChanged);
        }
    }

    public ClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public ArrayList<SettingList> getSettingLists() {
        return settingLists;
    }

    public void setSettingLists(ArrayList<SettingList> settingLists) {
        this.settingLists = settingLists;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == settingLists.size()) {
            return LANGUAGE_VIEW;
        } else {
            return super.getItemViewType(position);
        }
    }

    public void setClickableTheme(boolean value) {
        themeSettingViewHolder.itemView.setClickable(value);
    }


    @Override
    public int getItemCount() {
        return settingLists.size() + 1;
    }

    public void setLanguageViewOnCheckChangeListener(LanguageViewOnCheckChangeListener languageViewOnCheckChangeListener) {
        this.languageViewOnCheckChangeListener = languageViewOnCheckChangeListener;
    }

    public interface ClickListener {
        void onClick(int postion);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.setting_photo) ImageView photo;
        @BindView(R.id.setting_name) TextView txt;
        @BindView(R.id.setting_description) TextView descriptionTextView;

        private String nodes;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            nodes = ThemeUtil.getString(itemView.getContext(), R.attr.no_description);
        }
    }

    public static class LanguageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.language_switch) SwitchCompat languageSwitch;

        LanguageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            if (POSUtil.isMyanmarLanguage(itemView.getContext())) {
                languageSwitch.setChecked(false);
            } else {
                languageSwitch.setChecked(true);
            }
        }
    }

    public interface LanguageViewOnCheckChangeListener {
        void onCheckedChanged(CompoundButton compoundButton, boolean isChecked);
    }
}