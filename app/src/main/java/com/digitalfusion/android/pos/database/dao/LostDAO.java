package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.model.LostItem;
import com.digitalfusion.android.pos.database.model.StockValue;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/18/16.
 */
public class LostDAO extends ParentDAO {
    private static ParentDAO lostDaoInstance;
    private Context context;
    private IdGeneratorDAO idGeneratorDAO;
    private LostItem lostItem;
    private List<LostItem> lostItemList;
    private LostDetailDAO lostDetailDAO;
    private List<Long> idList;

    private LostDAO(Context context) {
        super(context);
        this.context = context;
        lostItem = new LostItem();
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        lostDetailDAO = LostDetailDAO.getLostDetailDaoInstance(context);
    }

    public static LostDAO getLostDaoInstance(Context context) {
        if (lostDaoInstance == null) {
            lostDaoInstance = new LostDAO(context);
        }
        return (LostDAO) lostDaoInstance;
    }

    public boolean addNewLost(Long stockID, String date, int stockQty, String remark, String day, String month, String year, Long currentUserId) {
        Long genId = idGeneratorDAO.getLastIdValue("Lost");
        genId += 1;
        query = "insert into " + AppConstant.LOST_TABLE_NAME + "(" + AppConstant.LOST_ID + ", " + AppConstant.LOST_STOCK_ID + ", " + AppConstant.LOST_DATE
                + ", " + AppConstant.LOST_STOCK_QTY + ", " + //AppConstant.LOST_TOTAL_VALUE + ", " +
                AppConstant.LOST_REMARK + ", " + AppConstant.LOST_USER_ID
                + ", " + AppConstant.LOST_DAY + ", " + AppConstant.LOST_MONTH + ", " + AppConstant.LOST_YEAR + ", " + AppConstant.CREATED_DATE + ", " + AppConstant.LOST_TIME +
                ") values (?,?,?,?,?,?,?,?,?,?, strftime('%s', ?, time('now', 'localtime')))";
        databaseWriteTransaction(flag);
        try {
            statement = database.compileStatement(query);
            statement.bindString(1, genId.toString());
            statement.bindLong(2, stockID);
            statement.bindString(3, date);
            statement.bindString(4, Integer.toString(stockQty));
            statement.bindString(5, remark);
            statement.bindString(6, currentUserId.toString());
            statement.bindString(7, day);
            statement.bindString(8, month);
            statement.bindString(9, year);
            statement.bindString(10, DateUtility.getTodayDate());
            statement.bindString(11, formatDateWithDash(day, month, year));
            statement.execute();
            statement.clearBindings();
            /*for (LostItemInDetail l: detailViewList){
                lostDetailDAO.addNewStockLostDetail(genId, l.getStockID(), l.getQty(), l.getPurchasePrice(), l.getRemark(), l.getTotal());
            }*/
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        // Log.e("coml","ha "+flag.isInserted());
        return flag.isInserted();
    }

    public List<LostItem> getAllLostsByDateRange(String startDate, String endDate, int startLimit, int endLimit) {
        createPurchasePriceTempTable(startDate, endDate);
        lostItemList = new ArrayList<>();
        Cursor cursor1 = null;
        query = "select l." + AppConstant.LOST_ID + ", s." + AppConstant.STOCK_NAME + ", l." + AppConstant.LOST_DATE + ", l." + AppConstant.LOST_STOCK_QTY + ", l." +
                //AppConstant.LOST_TOTAL_VALUE + ", l." +
                AppConstant.LOST_REMARK //+ ", u." + AppConstant.USER_USER_NAME + ", l." + AppConstant.LOST_USER_ID + ", u." + AppConstant.USER_ROLE
                + ", l." + AppConstant.LOST_DAY + ", l." + AppConstant.LOST_MONTH + ", l." + AppConstant.LOST_YEAR + ", sum(ifnull(" + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
                ", " + AppConstant.LOST_STOCK_QTY + ") * " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ") totalPrice"
                + ", ifnull(u." + AppConstant.UNIT_UNIT + ", '') unit, s." + AppConstant.STOCK_CODE_NUM + " , t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID
                + " from " + AppConstant.LOST_TABLE_NAME + " l," + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.STOCK_TABLE_NAME + " s left outer join "
                + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u. " + AppConstant.UNIT_ID //+ AppConstant.USER_TABLE_NAME + " u, " + AppConstant.STOCK_TABLE_NAME + " s
                + " where t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " + //l." + AppConstant.LOST_USER_ID + " = u." + AppConstant.USER_ID + " and l." +
                AppConstant.LOST_DATE + " >= ? and l." + AppConstant.LOST_DATE + " <= ? and l." + AppConstant.LOST_STOCK_ID + " = t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " and " +
                AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'l'  and l." + AppConstant.LOST_ID + " = " + AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID
                + " group by l." + AppConstant.LOST_ID + " order by l." + AppConstant.LOST_TIME + " desc limit " + startLimit + ", " + endLimit;
        Log.e("queyr", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    lostItem = new LostItem();
                    lostItem.setId(cursor.getLong(0));
                    lostItem.setStockName(cursor.getString(1));
                    lostItem.setDate(cursor.getString(2));
                    lostItem.setStockQty(cursor.getInt(3));
                    lostItem.setRemark(cursor.getString(4));
                    //lostItem.setUserName(cursor.getString(6));
                    //lostItem.setUserID(cursor.getLong(7));
                    // lostItem.setRole(cursor.getString(8));
                    lostItem.setDay(cursor.getString(5));
                    lostItem.setMonth(cursor.getString(6));
                    lostItem.setYear(cursor.getString(7));
                    lostItem.setTotalValue(cursor.getDouble(8));
                    lostItem.setStockUnit(cursor.getString(9));
                    lostItem.setStockCodeNo(cursor.getString(10));
                    lostItem.setStockID(cursor.getLong(11));
                    query = " select ifnull(" + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + ", " + AppConstant.LOST_STOCK_QTY + ") qty, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE +
                            " from " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.LOST_TABLE_NAME + " l where t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " = l." + AppConstant.LOST_STOCK_ID
                            + " and " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'l' and " + AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID + " = l." + AppConstant.LOST_ID + " and l." + AppConstant.LOST_ID + " = " + cursor.getLong(0);

                    cursor1 = database.rawQuery(query, null);
                    if (cursor1.moveToFirst()) {
                        do {
                            lostItem.getStockValueList().add(new StockValue(cursor1.getDouble(1), cursor1.getInt(0)));
                        } while (cursor1.moveToNext());
                    }
                    lostItemList.add(lostItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();

            if (cursor1 != null)
                cursor1.close();
        }
        dropPurchasePriceTempTable();
        return lostItemList;
    }


    /*public List<LostItem> getAllLostsByDateRangeOnSearch(String startDate, String endDate, int startLimit, int endLimit, String searchStr){
        createPurchasePriceTempTable(AppConstant.LOST_TABLE_NAME, endDate, startDate);
        lostItemList = new ArrayList<>();
        Cursor cursor1 = null;
        query = "select l." + AppConstant.LOST_ID + ", s." + AppConstant.STOCK_NAME + ", l." + AppConstant.LOST_DATE + ", l." + AppConstant.LOST_STOCK_QTY + ", l." +
                //AppConstant.LOST_TOTAL_VALUE + ", l." +
                AppConstant.LOST_REMARK //+ ", u." + AppConstant.USER_USER_NAME + ", l." + AppConstant.LOST_USER_ID + ", u." + AppConstant.USER_ROLE
                + ", l." + AppConstant.LOST_DAY + ", l." + AppConstant.LOST_MONTH + ", l." + AppConstant.LOST_YEAR + ", sum(ifnull(" + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
                ", " + AppConstant.LOST_STOCK_QTY + ") * " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ") totalPrice"
                + ", ifnull(u." + AppConstant.UNIT_UNIT+", '') unit, s."+AppConstant.STOCK_CODE_NUM+" , t."+AppConstant.SALES_PURCHASE_TEMP_STOCK_ID
                + " from " + AppConstant.LOST_TABLE_NAME + " l," + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.STOCK_TABLE_NAME + " s left outer join "
                + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u. " + AppConstant.UNIT_ID //+ AppConstant.USER_TABLE_NAME + " u, " + AppConstant.STOCK_TABLE_NAME + " s
                + " where t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " = s." + AppConstant.STOCK_ID +" and "+ //l." + AppConstant.LOST_USER_ID + " = u." + AppConstant.USER_ID + " and l." +
                AppConstant.LOST_DATE + " >= ? and l." + AppConstant.LOST_DATE + " <= ? and l." + AppConstant.LOST_STOCK_ID + " = t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " and " +
                "((" + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'l' and " + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + " = l." + AppConstant.LOST_ID + ") or " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'a')"
                + " and (s." + AppConstant.STOCK_NAME + " like ? || '%' or s." + AppConstant.STOCK_CODE_NUM + " like ? || '%' ) " + " order by l."
                + AppConstant.LOST_TIME + " desc limit " + startLimit + ", "+ endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    lostItem = new LostItem();
                    lostItem.setId(cursor.getLong(0));
                    lostItem.setStockName(cursor.getString(1));
                    lostItem.setDate(cursor.getString(2));
                    lostItem.setStockQty(cursor.getInt(3));
                    lostItem.setRemark(cursor.getString(4));
                    //lostItem.setUserName(cursor.getString(6));
                    //lostItem.setUserID(cursor.getLong(7));
                    // lostItem.setRole(cursor.getString(8));
                    lostItem.setDay(cursor.getString(5));
                    lostItem.setMonth(cursor.getString(6));
                    lostItem.setYear(cursor.getString(7));
                    lostItem.setTotalValue(cursor.getDouble(8));
                    lostItem.setStockCodeNo(cursor.getString(10));
                    lostItem.setStockUnit(cursor.getString(9));
                    lostItem.setStockID(cursor.getLong(11));
                    query = " select ifnull(" + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + ", " + AppConstant.LOST_STOCK_QTY + ") qty, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE +
                            " from " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.LOST_TABLE_NAME + " d where t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " = l." + AppConstant.LOST_STOCK_ID
                            + " and (" + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'a' or " +
                            "(" + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'l' and " + AppConstant.SALES_PURCHASE_SALES_TEMP_ID + " = l." + AppConstant.LOST_ID + ")) and l." + AppConstant.LOST_ID + " = " + cursor.getLong(0);
                    cursor1 = database.rawQuery(query, null);
                    if (cursor1.moveToFirst()){
                        do {
                            lostItem.getStockValueList().add(new StockValue(cursor1.getDouble(1), cursor1.getInt(0)));
                        }while (cursor1.moveToNext());
                    }
                    lostItemList.add(lostItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        }catch (SQLiteException e){
            e.printStackTrace();
        }finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
            if (cursor1 != null)
                cursor1.close();
        }
        dropPurchasePriceTempTable();
        return lostItemList;
    }*/

    public List<LostItem> getLostByDateRangeOnSearch(String startDate, String endDate, int startLimit, int endLimit, Long id) {
        Log.e(startDate, endDate + " on search " + id);
        createPurchasePriceTempTable(startDate, endDate);
        Cursor cursor1 = null;
        lostItemList = new ArrayList<>();
        query = "select d." + AppConstant.LOST_ID + ", s." + AppConstant.STOCK_NAME + ", d." + AppConstant.LOST_DATE + ", d." + AppConstant.LOST_STOCK_QTY + ", d." +
                //AppConstant.LOST_TOTAL_VALUE + ", d." +
                AppConstant.LOST_REMARK //+ ", u." + AppConstant.USER_USER_NAME + ", d." + AppConstant.LOST_USER_ID + ", u." + AppConstant.USER_ROLE
                + ", d." + AppConstant.LOST_DAY + ", d." + AppConstant.LOST_MONTH + ", d." + AppConstant.LOST_YEAR + ", sum(ifnull(" + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
                ", " + AppConstant.LOST_STOCK_QTY + ") * " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ") totalPrice"
                + ", ifnull(u." + AppConstant.UNIT_UNIT + ", '') unit, s." + AppConstant.STOCK_CODE_NUM + " , t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + ", c." + AppConstant.CATEGORY_NAME
                + " from " + AppConstant.LOST_TABLE_NAME + " d," + AppConstant.CATEGORY_TABLE_NAME + " c," + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.STOCK_TABLE_NAME + " s left outer join "
                + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u. " + AppConstant.UNIT_ID //+ AppConstant.USER_TABLE_NAME + " u, " + AppConstant.STOCK_TABLE_NAME + " s
                + " where s." + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " + //d." + AppConstant.LOST_USER_ID + " = u." + AppConstant.USER_ID + " and d." +
                AppConstant.LOST_DATE + " >= ? and d." + AppConstant.LOST_DATE + " <= ? and d." + AppConstant.LOST_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " +
                AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'l' and l." + AppConstant.LOST_ID + " = " + AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID
                + " and s." + AppConstant.STOCK_ID + " = " + id + " group by d." + AppConstant.LOST_ID + " order by d." + AppConstant.LOST_TIME + " desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    Log.e("cursor", cursor.getCount() + "id");
                    lostItem = new LostItem();
                    lostItem.setId(cursor.getLong(0));
                    lostItem.setStockName(cursor.getString(1));
                    lostItem.setDate(cursor.getString(2));
                    //Log.e("date da", cursor.getString(2));
                    lostItem.setStockQty(cursor.getInt(3));
                    //lostItem.setTotalValue(cursor.getDouble(4));
                    lostItem.setRemark(cursor.getString(4));
                    //lostItem.setUserName(cursor.getString(5));
                    //  lostItem.setUserID(cursor.getLong(6));
                    //  lostItem.setRole(cursor.getString(7));
                    lostItem.setDay(cursor.getString(5));
                    lostItem.setMonth(cursor.getString(6));
                    lostItem.setYear(cursor.getString(7));
                    //lostItem.setValue(cursor.getDouble(8));
                    lostItem.setTotalValue(cursor.getDouble(8));
                    lostItem.setStockCodeNo(cursor.getString(10));
                    lostItem.setStockUnit(cursor.getString(9));
                    lostItem.setStockID(cursor.getLong(11));
                    lostItem.setCategoryName(cursor.getString(12));
                    query = " select ifnull(" + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + ", " + AppConstant.LOST_STOCK_QTY + ") qty, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE +
                            " from " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.LOST_TABLE_NAME + " d where t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " = d." + AppConstant.LOST_STOCK_ID
                            + " and " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'l' and " +
                            AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID + " = d." + AppConstant.LOST_ID + " and d." + AppConstant.LOST_ID + " = " + cursor.getLong(0);
                    //Log.e("id", cursor.getLong(0)+ "f00"+cursor.getDouble(8));
                    cursor1 = database.rawQuery(query, null);
                    if (cursor1.moveToFirst()) {
                        do {
                            //Log.e(cursor1.getInt(0) + " q", cursor1.getDouble(1)+"");
                            lostItem.getStockValueList().add(new StockValue(cursor1.getDouble(1), cursor1.getInt(0)));
                        } while (cursor1.moveToNext());
                    }
                    lostItemList.add(lostItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
            if (cursor1 != null)
                cursor1.close();
        }
        dropPurchasePriceTempTable();

        //Log.e("dam lis size", lostItemList.size() + "fioa");
        return lostItemList;
    }

    public List<LostItem> getAllLosts(int startLimit, int endLimit) {
        lostItemList = new ArrayList<>();
        query = "select l." + AppConstant.LOST_ID + ", s." + AppConstant.STOCK_NAME + ", l." + AppConstant.LOST_DATE + ", l." + AppConstant.LOST_STOCK_QTY + ", l." +
                //AppConstant.LOST_TOTAL_VALUE + ", l." +
                AppConstant.LOST_REMARK + ", u." + AppConstant.USER_USER_NAME + ", l." + AppConstant.LOST_USER_ID + ", u." + AppConstant.USER_ROLE
                + ", l." + AppConstant.LOST_DAY + ", l." + AppConstant.LOST_MONTH + ", l." + AppConstant.LOST_YEAR + " ,s." + AppConstant.STOCK_CODE_NUM
                + " from " + AppConstant.LOST_TABLE_NAME + " l, " + AppConstant.STOCK_TABLE_NAME + " s," + AppConstant.USER_TABLE_NAME + " u where l." + AppConstant.LOST_USER_ID + " = u." + AppConstant.USER_ID +
                " and l." + AppConstant.LOST_STOCK_ID + " = s." + AppConstant.STOCK_ID + " order by l." + AppConstant.LOST_TIME + " desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    lostItem = new LostItem();
                    lostItem.setId(cursor.getLong(0));
                    lostItem.setStockName(cursor.getString(1));
                    lostItem.setDate(cursor.getString(2));
                    lostItem.setStockQty(cursor.getInt(3));
                    lostItem.setRemark(cursor.getString(4));
                    lostItem.setUserName(cursor.getString(5));
                    lostItem.setUserID(cursor.getLong(6));
                    lostItem.setRole(cursor.getString(7));
                    lostItem.setDay(cursor.getString(8));
                    lostItem.setMonth(cursor.getString(9));
                    lostItem.setYear(cursor.getString(10));
                    lostItem.setStockCodeNo(cursor.getString(11));
                    lostItemList.add(lostItem);
                } while (cursor.moveToNext());
            }

            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return lostItemList;
    }

    public boolean updateLost(Long id, String date, Long stockID, int stockQty, String remark, Long userID,
                              String day, String month, String year) {
        query = "update " + AppConstant.LOST_TABLE_NAME + " set " + AppConstant.LOST_DATE + "=?, " + AppConstant.LOST_STOCK_ID + " = " + stockID + " ," + AppConstant.LOST_STOCK_QTY + " = " + stockQty
                // + ", " + AppConstant.LOST_TOTAL_VALUE + "=" + totalValue
                + ", " + AppConstant.LOST_REMARK + "=?, " + AppConstant.LOST_USER_ID + "=" + userID +
                ", " + AppConstant.LOST_DAY + "=?, " + AppConstant.LOST_MONTH + "=?, " + AppConstant.LOST_YEAR + "=? where " + AppConstant.LOST_ID + "=" + id;
        databaseWriteTransaction(flag);
        //Log.e(updateDetailList.size()+ " ion",deleteDetailIdList.size()+ "ioni");
        try {
            database.execSQL(query, new String[]{date, remark, day, month, year});
            /*for (LostItemInDetail detailView : updateDetailList){
                if (detailView.getLostID() == null){
                    lostDetailDAO.addNewStockLostDetail(id, detailView. getStockID(), detailView.getQty(), detailView.getPurchasePrice(), detailView.getRemark(), detailView.getTotal());
                }else {
                    lostDetailDAO.updateStockLostDetail(detailView.getId(), detailView.getStockID(), detailView.getQty(), detailView.getPurchasePrice(), detailView.getTotal(), detailView.getRemark());
                }
            }
            for (Long iD : deleteDetailIdList){
                lostDetailDAO.deleteStockLostDetail(iD);
            }*/
            database.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean deleteLost(Long id) {
        databaseWriteTransaction(flag);
        try {
            /*idList = lostDetailDAO.getLostIdList(id);
            for (Long detailID : idList) {
                lostDetailDAO.deleteStockLostDetail(detailID);
            }*/
            database.delete(AppConstant.LOST_TABLE_NAME, AppConstant.LOST_ID + " = ?", new String[]{id.toString()});
            database.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }
}
