package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Supplier;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD001 on 9/5/16.
 */
public class RVSwiperAdapterForSupplierList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {


    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    protected boolean showLoader = false;
    private List<Supplier> supplierList;    ///////
    private ClickListener editClickListener;
    private ClickListener detailClickListener;
    private ClickListener callClickListener;
    private ClickListener deleteClickListener;
    private LoaderViewHolder loaderViewHolder;
    private int editPos;

    public RVSwiperAdapterForSupplierList(List<Supplier> supplierList) {     //////////

        this.supplierList = supplierList;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEWTYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.supplier_item_view, parent, false);

            return new SupplierViewHolder(v);

        } else {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            return new LoaderViewHolder(v);

        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof SupplierViewHolder) {

            final SupplierViewHolder supplierViewHolder = (SupplierViewHolder) holder;

            POSUtil.makeZebraStrip(supplierViewHolder.itemView, position);

            // supplierViewHolder.supplierNameTextView.setText(supplierList.get(position).getName());

            //    Log.w("hello",supplierList.get(position).getBusinessName()+" ss");


            if (supplierList.get(position).getName().toString().length() > 1) {

                supplierViewHolder.supplierNameTextView.setText(supplierList.get(position).getName());

            } else {

                supplierViewHolder.supplierNameTextView.setText(null);

                supplierViewHolder.supplierNameTextView.setHint("No Business Name");

            }

            if (supplierList.get(position).getPhoneNo() == null || supplierList.get(position).getPhoneNo().equalsIgnoreCase("")) {

                supplierViewHolder.callBtn.setVisibility(View.GONE);

                supplierViewHolder.supplierPhoneNo.setText(null);

                supplierViewHolder.supplierPhoneNo.setActivated(false);

                supplierViewHolder.supplierPhoneNo.setHint("No Phone");


            } else {

                supplierViewHolder.callBtn.setVisibility(View.VISIBLE);

                supplierViewHolder.supplierPhoneNo.setActivated(true);

                String[] s = supplierList.get(position).getPhoneNo().split(",");

                SpannableString content = new SpannableString(s[0]);
                content.setSpan(new UnderlineSpan(), 0, s[0].length(), 0);
                supplierViewHolder.supplierPhoneNo.setText(content);

                supplierViewHolder.callBtn.setVisibility(View.VISIBLE);

                supplierViewHolder.supplierPhoneNo.setActivated(true);

                supplierViewHolder.supplierPhoneNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        supplierViewHolder.swipeLayout.close();
                        callClickListener.onClick(position);
                    }
                });

            }

            //   supplierViewHolder.businessNameTextView.setText(supplierList.get(position).getBusinessName());

            supplierViewHolder.detailButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (detailClickListener != null) {
                        supplierViewHolder.swipeLayout.close();
                        detailClickListener.onClick(position);
                    }
                }
            });

            supplierViewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    editPos = position;
                    if (editClickListener != null) {
                        supplierViewHolder.swipeLayout.close();
                        editClickListener.onClick(position);

                    }
                }
            });

            supplierViewHolder.callBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callClickListener != null) {
                        supplierViewHolder.swipeLayout.close();
                        callClickListener.onClick(position);

                    }
                }
            });


            supplierViewHolder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteClickListener != null) {
                        supplierViewHolder.swipeLayout.close();
                        deleteClickListener.onClick(position);

                    }
                }
            });

            mItemManger.bindView(supplierViewHolder.view, position);

        } else {

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) holder;

            this.loaderViewHolder = loaderViewHolder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }

    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    public ClickListener getDetailClickListener() {
        return detailClickListener;
    }

    public void setDetailClickListener(ClickListener detailClickListener) {
        this.detailClickListener = detailClickListener;
    }

    @Override
    public int getItemCount() {
        if (supplierList == null || supplierList.size() == 0) {
            return 0;
        } else {
            return supplierList.size() + 1;
        }

    }


    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            if (supplierList != null && supplierList.size() != 0) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;

            }

        }

        return VIEWTYPE_ITEM;
    }

    public void setShowLoader(boolean showLoader) {
        this.showLoader = showLoader;

        if (loaderViewHolder != null) {
            Log.w("hele", "loader full not null");
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public List<Supplier> getSupplierList() {
        return supplierList;
    }    ///////////////

    public void setSupplierList(List<Supplier> supplierList) {  ////////

        this.supplierList = supplierList;

    }

    public void updateItem(Supplier supplier) {

        //Log.w("here updateRegistration",salesHistoryView.getId()+"");

        // Log.w("position",purchaseHistoryViewList.indexOf(purchaseHistoryView)+" S");

        //Log.w("position",purchaseHistoryViewList.get(purchaseHistoryViewList.indexOf(purchaseHistoryView)).getSupplierName()+" S");

        // Log.w("position",purchaseHistoryView.getSupplierName()+" S");

        supplierList.set(editPos, supplier);

        notifyItemChanged(editPos, supplier);

        //  notifyDataSetChanged();


    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {

        this.editClickListener = editClickListener;

    }

    public ClickListener getCallClickListener() {
        return callClickListener;
    }

    public void setCallClickListener(ClickListener callClickListener) {
        this.callClickListener = callClickListener;
    }

    public class SupplierViewHolder extends RecyclerView.ViewHolder {

        TextView supplierNameTextView;

        TextView supplierBusinessName;

        TextView supplierPhoneNo;

        ImageButton editButton;

        LinearLayout linearLayout;

        TextView businessNameTextView;

        ImageButton callBtn;

        ImageButton detailButton;

        ImageButton deleteBtn;

        SwipeLayout swipeLayout;

        View view;

        public SupplierViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);

            detailButton = (ImageButton) itemView.findViewById(R.id.view_detail);

            businessNameTextView = (TextView) itemView.findViewById(R.id.business_name);

            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll);

            supplierNameTextView = (TextView) itemView.findViewById(R.id.suppl_name_in_supplier_item_view);

            supplierBusinessName = (TextView) itemView.findViewById(R.id.business_name);

            supplierPhoneNo = (TextView) itemView.findViewById(R.id.phone_no);

            editButton = (ImageButton) itemView.findViewById(R.id.edit_supplier);

            callBtn = (ImageButton) itemView.findViewById(R.id.call_action);

            deleteBtn = itemView.findViewById(R.id.delete);

        }

    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar mProgressBar;

        public View mView;

        public LoaderViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);

        }
    }
}

