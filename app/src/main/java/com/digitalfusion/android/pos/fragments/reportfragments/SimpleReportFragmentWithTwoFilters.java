package com.digitalfusion.android.pos.fragments.reportfragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForCategoryMD;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterSaleStaffsFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForStockBalance;
import com.digitalfusion.android.pos.database.business.CategoryManager;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.util.AidlUtil;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.BluetoothUtil;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.ExportImportUtil;
import com.digitalfusion.android.pos.util.GrantPermission;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.PrintImage;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.views.FolderChooserDialog;
import com.digitalfusion.android.pos.views.FusionToast;
import com.digitalfusion.android.pos.views.Spinner;
import com.prologic.printapi.PrintCanvas;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.digitalfusion.android.pos.util.ESCUtil.GS;

/**
 * A simple {@link Fragment} subclass.
 */
public class SimpleReportFragmentWithTwoFilters extends Fragment implements Serializable {
    private final static byte[] CUT_PAPER = new byte[]{GS, 0x56, 0x00};

    private String pageTitle;
    private String defaultFilterOneText;
    private String categoryID;
    private String reportName;
    private String endDate;
    private String startDate;
    private String customRange;
    private String today;
    private String yesterday;
    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;
    private String customStartDate,
                   customEndDate;

    private double  totalValue;
    private int     startLimit;
    private int     endLimit;
    private boolean shouldLoad = true;
    private boolean isAll      = false;

    private TextView                  filterOneTextView;
    private View                      mainLayoutView;
    private RecyclerView              recyclerView;
    private MaterialDialog            customRangeDialog;
    private TextView                  startDateTextView,
                                      endDateTextView;
    private TextView                  filterLabelOneTextView;
    private TextView                  traceDate;
    private Button                    customRangeOkBtn,
                                      customRangeCancelBtn;
    private DatePickerDialog          startDatePickerDialog;
    private MaterialDialog filterOneDialog;
    private TextView                  totalValueTextView;
    private MaterialDialog            dateFilterDialog;
    private TextView                  dateFilterTextView;

    private Context                   context;
    private ReportManager reportManager;
    private RVAdapterForStockBalance rvAdapterForReport;
    private RecyclerView.Adapter    rvAdapterForFilterOneMD;
    private RVAdapterForFilter        rvAdapterForDateFilter;
    private BluetoothUtil             bluetoothUtil;

    private List<ReportItem> reportItemList;
    private List<String>              filterList;
    private List<Category>            categoryFilterList;
    private List<Category>            categoryList;
    private List<User>                staffList;

    private User selectedStaff;

    //private String categoryName;
    //private MaterialDialog categoryChooserMD;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_simple_report_fragment_with_category_filter, container, false);
        context = getContext();
        loadUI();
        setHasOptionsMenu(true);
        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            reportName = getArguments().getString("reportType");
        }

        new StartupTask().execute();

        new CreateReportTask().execute("");
    }

    private void initViews() {
        filterOneTextView.setText(defaultFilterOneText);
        dateFilterTextView.setText(today);
        if (reportName.equalsIgnoreCase(getString(R.string.stock_balance_eng))) {
            filterLabelOneTextView.setText(ThemeUtil.getString(context, R.attr.category_filter));
        } else {
            filterLabelOneTextView.setText(ThemeUtil.getString(context, R.attr.sale_staff));
        }
    }

    private void configToolBarTitle() {
        if (reportName.equalsIgnoreCase(getString(R.string.stock_balance_eng))) {
            pageTitle = ThemeUtil.getString(context, R.attr.stock_balance);
        } else {
            pageTitle = ThemeUtil.getString(context, R.attr.sales_detail_by_sale_staff);
        }
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(pageTitle);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void configFilters() {
        today       = ThemeUtil.getString(context, R.attr.today       );
        yesterday   = ThemeUtil.getString(context, R.attr.yesterday   );
        thisMonth   = ThemeUtil.getString(context, R.attr.this_month  );
        lastMonth   = ThemeUtil.getString(context, R.attr.last_month  );
        thisYear    = ThemeUtil.getString(context, R.attr.this_year   );
        lastYear    = ThemeUtil.getString(context, R.attr.last_year   );
        thisMonth   = ThemeUtil.getString(context, R.attr.this_month  );
        customRange = ThemeUtil.getString(context, R.attr.custom_range);

        filterList = new ArrayList<>();
        filterList.add(today);
        filterList.add(yesterday);
        filterList.add(thisMonth);
        filterList.add(lastMonth);
        filterList.add(thisYear);
        filterList.add(lastYear);
        filterList.add(customRange);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }


    private void loadUI() {
        filterLabelOneTextView = mainLayoutView.findViewById(R.id.filter_label_one);
        filterOneTextView = mainLayoutView.findViewById(R.id.filter_one);
        recyclerView           = mainLayoutView.findViewById(R.id.recycler_view);
        totalValueTextView     = mainLayoutView.findViewById(R.id.total_value     );
        dateFilterTextView     = mainLayoutView.findViewById(R.id.date_filter  );
    }

    private void initializeVariables() {
        reportItemList     = new ArrayList<>();
        reportManager = new ReportManager(context);
        startLimit         =  0;
        endLimit           = 10;
        startDate          = endDate = DateUtility.getTodayDate();
        categoryID         = "0 or 1 = 1";

        if (reportName.equalsIgnoreCase(getString(R.string.stock_balance_eng))) {
            CategoryManager categoryManager = new CategoryManager(context);
            categoryList = categoryManager.getAllCategories();
            categoryFilterList = new ArrayList<>();
            String all = ThemeUtil.getString(context, R.attr.all);
            categoryFilterList.add(new Category(all));
            categoryFilterList.addAll(categoryList);

            defaultFilterOneText = categoryFilterList.get(0).getName();
        } else {
            staffList      = ApiDAO.getApiDAOInstance(context).getAllUserRoles();
            selectedStaff = staffList.get(0);
            defaultFilterOneText = selectedStaff.getUserName();

        }

        gatherReportData();
    }

    class StartupTask extends AsyncTask<Void, Void, Void> {
        Spinner spinner = new Spinner(context);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            initializeVariables();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            spinner.dismiss();

            configFilters();
            initViews();
            configRecyclerView();
            configToolBarTitle();
            buildFilterOneChooserDialog();
            buildDateFilterDialog();
            buildingCustomRangeDialog();
            buildFilterOneChooserDialog();
            clickListeners();
        }
    }

    public void buildFilterOneChooserDialog() {
        String filter;
        if (reportName.equalsIgnoreCase(getString(R.string.stock_balance_eng))) {
            rvAdapterForFilterOneMD = new RVAdapterForCategoryMD(categoryFilterList);
            filter = ThemeUtil.getString(context, R.attr.filter_by_category);
        } else {
            rvAdapterForFilterOneMD = new RVAdapterSaleStaffsFilter(staffList);
            filter = ThemeUtil.getString(context, R.attr.sale_staff);
        }
        filterOneDialog = new MaterialDialog.Builder(context)
                .title(filter)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForFilterOneMD, new LinearLayoutManager(context))
                .build();
    }


    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        rvAdapterForReport = new RVAdapterForStockBalance(reportItemList);
        recyclerView.setAdapter(rvAdapterForReport);
        /*else if (reportName.equalsIgnoreCase("top sales by customer") || reportName.equalsIgnoreCase("top sale by products") ||
                reportName.equalsIgnoreCase("top outstanding suppliers") || reportName.equalsIgnoreCase("bottom sales by products")){
            rvAdapterForReport = new RVAdapterForBarChartReportWithNameAmt(renderReportItemList);
            recyclerView.setAdapter(rvAdapterForReport);
        }*/
    }
    
    public void buildingCustomRangeDialog() {
        buildingCustomRangeDatePickerDialog();
        String date = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range}).getString(0);
        customRangeDialog = new MaterialDialog.Builder(context)
                .title(date)
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
        
        Calendar now         = Calendar.getInstance();

        startDateTextView    = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView      = (TextView) customRangeDialog.findViewById(R.id.endDate  );
        customRangeOkBtn     = (Button  ) customRangeDialog.findViewById(R.id.save     );
        customRangeCancelBtn = (Button  ) customRangeDialog.findViewById(R.id.cancel   );
        customStartDate      = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)),
                                                    Integer.toString(now.get(Calendar.MONTH) + 1), 
                                                    Integer.toString(1));
        customEndDate        = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)),
                                                    Integer.toString(now.get(Calendar.MONTH) + 1), 
                                                    Integer.toString(DateUtility.getMonthEndDate(now)));

        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(DateUtility.getMonthEndDate(now))));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");
            }
        });
        
        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");
            }
        });
        
        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRangeDialog.dismiss();
            }
        });

    }
    

    private void buildDateFilterDialog() {
        String date = ThemeUtil.getString(context, R.attr.filter_by_date);

        dateFilterDialog = new MaterialDialog.Builder(context)
                .title(date)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))
                .build();
    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
                     public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                if (traceDate == startDateTextView) {
                    customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                    startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                    startDate = customStartDate;
                }else {
                    customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                    endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                    endDate = customEndDate;
                }
            }
        },
        now.get(Calendar.YEAR),
        now.get(Calendar.MONTH),
        now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });


    }

    private void clickListeners() {
        if (reportName.equalsIgnoreCase(getString(R.string.stock_balance_eng))) {
            ((RVAdapterForCategoryMD) rvAdapterForFilterOneMD).setmItemClickListener(new RVAdapterForCategoryMD.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    filterOneDialog.dismiss();
                    defaultFilterOneText = categoryFilterList.get(position).getName();
                    filterOneTextView.setText(defaultFilterOneText);
                    if (position != 0) {
                        isAll = false;
                        categoryID = categoryList.get(position - 1).getId().toString();
                    } else {
                        isAll = true;
                        categoryID = "0 or 1=1";
                    }
                    scrollListener();
                    new CreateReportTask().execute("");
                }
            });
        } else {
            ((RVAdapterSaleStaffsFilter) rvAdapterForFilterOneMD).setmItemClickListener(new RVAdapterSaleStaffsFilter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    filterOneDialog.dismiss();
                    selectedStaff = staffList.get(position);
                    filterOneTextView.setText(selectedStaff.getUserName());
                    scrollListener();
                    new CreateReportTask().execute("");
                }
            });
        }

        scrollListener();
        filterOneTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterOneDialog.show();
            }
        });

        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                dateFilterDialog.dismiss();

                String selectedItem = filterList.get(position);
                dateFilterTextView.setText(selectedItem);

                if (selectedItem.equalsIgnoreCase(today)) {
                    startDate = endDate = DateUtility.getTodayDate();
                    
                    new CreateReportTask().execute("");
                } else if (selectedItem.equalsIgnoreCase(yesterday)) {
                    startDate = endDate = DateUtility.getYesterDayDate();
                    
                    new CreateReportTask().execute("");
                } else if (selectedItem.equalsIgnoreCase(thisMonth)) {
                    startDate = DateUtility.getThisMonthStartDate();
                    endDate   = DateUtility.getThisMonthEndDate  ();

                    new CreateReportTask().execute("");
                } else if (selectedItem.equalsIgnoreCase(lastMonth)) {
                    startDate = DateUtility.getLastMonthStartDate();
                    endDate   = DateUtility.getLastMonthEndDate  ();

                    new CreateReportTask().execute("");
                } else if (selectedItem.equalsIgnoreCase(thisYear)) {
                    startDate = DateUtility.getThisYearStartDate();
                    endDate   = DateUtility.getThisYearEndDate  ();
                    
                    new CreateReportTask().execute("");
                } else if (selectedItem.equalsIgnoreCase(lastYear)) {
                    startDate = DateUtility.getLastYearStartDate();
                    endDate   = DateUtility.getLastYearEndDate  ();
                    
                    new CreateReportTask().execute("");
                } else if (selectedItem.equalsIgnoreCase(customRange)) {
                    customRangeDialog.show();
                }


            }
        });

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDate = customStartDate;
                endDate   = customEndDate;

                /*String startDayDes[]= DateUtility.dayDes(startDate);
                String startYearMonthDes= DateUtility.monthYearDes(startDate);
                String endDayDes[]= DateUtility.dayDes(endDate);
                String endYearMonthDes= DateUtility.monthYearDes(endDate);*/
                //dateFilterTextView.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));

                dateFilterTextView.setText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));

                new CreateReportTask().execute("");

                customRangeDialog.dismiss();
            }
        });

        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRangeDialog.dismiss();
            }
        });

    }

    private void scrollListener() {
        recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() { }

            @Override
            public void onScrollDown() { }

            @Override
            public void onLoadMore() {
                if (shouldLoad) {
                    rvAdapterForReport.setShowLoader(true);
                    loadMore();
                }
            }
        });
    }

    public void loadMore() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (reportName.equalsIgnoreCase(getString(R.string.stock_balance_eng))) {
                    reportItemList.addAll(reportManager.productValuationByCategoryID(reportItemList.size(), endLimit, categoryID));
                } else {
                    reportItemList.addAll(reportManager.salesDetailBySaleStaff(startDate, endDate, reportItemList.size(), endLimit, selectedStaff.getId()));
                }

                rvAdapterForReport.setShowLoader(false);
                rvAdapterForReport.setReportItemList(reportItemList);
                rvAdapterForReport.notifyDataSetChanged();

            }
        }, 500);
    }

    private void gatherReportData() {
        if (reportName.equalsIgnoreCase(getString(R.string.stock_balance_eng))){
            //reportManager.createPurchasePriceTempTableForStockBalance()
            //reportItemList = reportManager.stockBalance(startLimit, endLimit, categoryID);
            reportManager.dropPurchasePriceTempTable();
            reportManager.createPurchasePriceTempTableForValuation(endDate);
            reportItemList = reportManager.productValuationByCategoryID(startLimit, endLimit, categoryID);
            totalValue = reportManager.totalProductValuationByCategoryID(categoryID);
        } else {
            reportItemList = reportManager.salesDetailBySaleStaff(startDate, endDate, startLimit, endLimit, selectedStaff.getId());
            totalValue = 0;
            for (ReportItem reportItem : reportItemList) {
                totalValue += reportItem.getTotalAmt();
            }
        }
    }

    class CreateReportTask extends AsyncTask<String, String, String> {
        Spinner spinner = new Spinner(context);

        @Override
        protected String doInBackground(String... params) {
            gatherReportData();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.show();
        }

        @Override
        protected void onPostExecute(String a) {
            totalValueTextView.setText(POSUtil.NumberFormat(totalValue));
            rvAdapterForReport.setReportItemList(reportItemList);
            rvAdapterForReport.notifyDataSetChanged();
            recyclerView.invalidate();

            spinner.dismiss();
        }
    }

    private void preparePrint() {
        String itemName = ThemeUtil.getString(context, R.attr.name  );
        String itemQty  = ThemeUtil.getString(context, R.attr.qty   );
        String amount   = ThemeUtil.getString(context, R.attr.amount);
        String total    = ThemeUtil.getString(context, R.attr.total);

        bluetoothUtil = BluetoothUtil.getInstance(this);
        final Spinner spinner = new Spinner(context);


        if (!bluetoothUtil.isConnected() && !POSUtil.isInternalPrinter(context)) {
            Toast.makeText(context, "Not connected to printer", Toast.LENGTH_SHORT).show();

            bluetoothUtil.setBluetoothConnectionListener(new BluetoothUtil.JobListener() {

                @Override
                public void onPrepare() {
                    spinner.show();
                }

                @Override
                public void onSuccess() {
                    spinner.dismiss();

                    if (bluetoothUtil.isConnected()) {

                        String connected = ThemeUtil.getString(context, R.attr.connected);
                        FusionToast.toast(context, FusionToast.TOAST_TYPE.SUCCESS, connected);

                    } else {
                        String notConnected = ThemeUtil.getString(context, R.attr.not_connected);
                        FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR, notConnected);
                    }
                }

                @Override
                public void onFailure() {
                    spinner.dismiss();

                    String notConnected = ThemeUtil.getString(context, R.attr.not_connected);
                    FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR, notConnected);
                }

                @Override
                public void onDisconnected() {
                    String notConnected = ThemeUtil.getString(context, R.attr.not_connected);
                    FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR, notConnected);
                }
            });
            bluetoothUtil.createConnection(false);

        } else {

            Typeface mTypeface = Typeface.createFromAsset(context.getApplicationContext()
                    .getAssets(), String.format("fonts/%s", "Zawgyi-One.ttf"));
            PrintCanvas printCanvas;

            // Paper defaultFilterOneText
            if (POSUtil.is80MMPrinter(getContext())) {
                printCanvas = new PrintCanvas(PrintCanvas.PAPERSIZE._80MM, mTypeface, 30);
            } else {
                printCanvas = new PrintCanvas(PrintCanvas.PAPERSIZE._58MM, mTypeface, 20);
            }

            // start from here
            PrintCanvas.Row    row    = printCanvas.new Row();
            PrintCanvas.Column column = printCanvas.new Column(100, pageTitle, PrintCanvas.ALIGN.CENTER);
            row.addColumn(column);
            printCanvas.writeRow(row);
            printCanvas.nextLine();

            // if start date and end date is the same, leave out the end date
            row    = printCanvas.new Row();
            column = printCanvas.new Column(100,
                    DateUtility.makeDateFormatWithSlash(startDate) + (endDate.equalsIgnoreCase(startDate) ? "" : " - " + DateUtility.makeDateFormatWithSlash(endDate)),
                    PrintCanvas.ALIGN.RIGHT);
            row.addColumn(column);
            printCanvas.writeRow(row);

            row = printCanvas.new Row();
            column = printCanvas.new Column(100, filterOneTextView.getText().toString(),
                    PrintCanvas.ALIGN.RIGHT);
            row.addColumn(column);
            printCanvas.writeRow(row);
            printCanvas.writeLine();

            // name
            row    = printCanvas.new Row();
            column = printCanvas.new Column(40, itemName, PrintCanvas.ALIGN.LEFT);
            row.addColumn(column);

            // qty
            column = printCanvas.new Column(43, itemQty, PrintCanvas.ALIGN.CENTER);
            row.addColumn(column);

            // amount
            column = printCanvas.new Column(29, amount, PrintCanvas.ALIGN.CENTER);
            row.addColumn(column);
            printCanvas.writeRow(row);
            printCanvas.writeLine();

            for (ReportItem item : reportItemList) {
                row = printCanvas.new Row();
                column = printCanvas.new Column(45, item.getName(), PrintCanvas.ALIGN.LEFT);
                row.addColumn(column);

                column = printCanvas.new Column(32, String.valueOf(item.getTotalQty()), PrintCanvas.ALIGN.RIGHT);
                row.addColumn(column);

                column = printCanvas.new Column(30,  POSUtil.doubleToString(item.getTotalAmt()), PrintCanvas.ALIGN.RIGHT);
                row.addColumn(column);
                printCanvas.writeRow(row);
            }

            printCanvas.nextLine();
            printCanvas.writeLine();

            row = printCanvas.new Row();
            column = printCanvas.new Column(100, total + " : " + POSUtil.NumberFormat(totalValue) + AppConstant.CURRENCY, PrintCanvas.ALIGN.CENTER);
            row.addColumn(column);
            printCanvas.writeRow(row);

            //printCanvas.drawWaterMarkDiagonal();

            Bitmap bmpMonochrome = Bitmap.createBitmap(printCanvas.getBitmap().getWidth(), printCanvas.getBitmap().getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas1       = new Canvas(bmpMonochrome);
            Paint  paint1        = new Paint();

            canvas1.drawColor(Color.WHITE);
            canvas1.drawBitmap(printCanvas.getBitmap(), 0, 0, paint1);

            PrintImage printImage = new PrintImage(bmpMonochrome);

            try {

                if (POSUtil.isInternalPrinter(getContext())) {

                    AidlUtil.getInstance().initPrinter();
                    //                    // AidlUtil.getInstance().printText(printViewSlip.getPrintString(), 24, false, false,"Type monospace");
                    AidlUtil.getInstance().printBitmap(printCanvas.getBitmap());
                } else {
                    bluetoothUtil.getOutputStream().write(printImage.getPrintImageData());
                    bluetoothUtil.getOutputStream().write(CUT_PAPER);
                }


                //
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        bluetoothUtil.onActivityResultBluetooth(requestCode, resultCode, data);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_print_export, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_print:
                preparePrint();
                break;
            case R.id.action_export:
                FolderChooserDialog folderChooserDialog =new FolderChooserDialog();
                folderChooserDialog.setmCallback(new FolderChooserDialog.FolderSelectionCallback() {
                    @Override
                    public void onFolderSelection(File folder) {
                        new ExportTask().execute(folder.getAbsolutePath());
                    }
                });
                GrantPermission grantPermission = new GrantPermission(this);
                if (grantPermission.requestPermissions()) {
                    //isImport=true;
                    folderChooserDialog.show(getActivity());
                }
                break;
        }
        return true;
    }


    @SuppressLint("StaticFieldLeak")
    class ExportTask extends AsyncTask<String, Void, Void> {
        Spinner spinner = new Spinner(context);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            String itemName = ThemeUtil.getString(context, R.attr.name  );
            String itemQty  = ThemeUtil.getString(context, R.attr.qty   );
            String amount   = ThemeUtil.getString(context, R.attr.amount);
            String total    = ThemeUtil.getString(context, R.attr.total);
            final String[] titles = {itemName, itemQty, amount, total};
            final String path = params[0];
            ExportImportUtil.exportTwoFiltersReports(reportName, titles, reportItemList, totalValue, startDate, endDate, path, filterOneTextView.getText().toString());
            POSUtil.scanMedia(path, context);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            spinner.dismiss();
        }
    }
}