package com.digitalfusion.android.pos.database.dao.purchase;

import android.content.Context;

import com.digitalfusion.android.pos.database.dao.ParentDAO;
import com.digitalfusion.android.pos.database.dao.IdGeneratorDAO;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.util.AppConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 2/22/18.
 */

public class PurchaseHoldDetailDAO extends ParentDAO {
    private static ParentDAO purchaseDetailDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private List<SalesAndPurchaseItem> salesAndPurchaseItemList;
    private SalesAndPurchaseItem salesAndPurchaseItem;
    private List<Long> idList;

    private PurchaseHoldDetailDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
    }

    public static PurchaseHoldDetailDAO getPurchaseDetailDaoInstance(Context context) {
        if (purchaseDetailDaoInstance == null) {
            purchaseDetailDaoInstance = new PurchaseHoldDetailDAO(context);
        }
        return (PurchaseHoldDetailDAO) purchaseDetailDaoInstance;
    }

    public Long addNewPurchaseDetail(Long purchaseID, Long stockID, int qty, double price, String discount, Long taxID, double total, double taxAmt, double discountAmt, double taxRate) {
        genID = idGeneratorDAO.getLastIdValue("PurchaseHoldDetail");
        genID++;
        query = "insert into " + AppConstant.PURCHASE_HOLD_DETAIL_TABLE_NAME + " (" + AppConstant.PURCHASE_DETAIL_ID + ", " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + ", " + AppConstant.PURCHASE_DETAIL_STOCK_ID
                + ", " + AppConstant.PURCHASE_DETAIL_QTY + ", " + AppConstant.PURCHASE_DETAIL_PRICE + ", " + AppConstant.PURCHASE_DETAIL_DISCOUNT + ", " + AppConstant.PURCHASE_DETAIL_TAX_ID + ", " +
                AppConstant.PURCHASE_DETAIL_TOTAL + ", " + AppConstant.PURCHASE_DETAIL_TAX_AMT + ", " + AppConstant.PURCHASE_DETAIL_DISCOUNT_AMT + ", " + AppConstant.PURCHASE_DETAIL_TAX_RATE +
                ") values (" + genID + ", " + purchaseID + ", " + stockID + ", " + qty + ", " + price + ", ?, " + taxID + ", " + total + ", " + taxAmt + ", " + discountAmt + ", " + taxRate + ")";
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query, new String[]{discount});
        return genID;
    }

    public List<SalesAndPurchaseItem> getPurchaseDetailsByPurchaseID(Long id) {
        query = "select pd." + AppConstant.PURCHASE_DETAIL_ID + ", pd." + AppConstant.PURCHASE_DETAIL_STOCK_ID + ", st." + AppConstant.STOCK_NAME + ", pd." + AppConstant.PURCHASE_DETAIL_QTY
                + ", pd." + AppConstant.PURCHASE_DETAIL_PRICE + ", pd." + AppConstant.PURCHASE_DETAIL_TOTAL + ", pd." + AppConstant.PURCHASE_DETAIL_DISCOUNT + ", pd." + AppConstant.PURCHASE_DETAIL_DISCOUNT_AMT
                + ", st." + AppConstant.STOCK_CODE_NUM + ", st." + AppConstant.STOCK_BARCODE
                + " from " + AppConstant.PURCHASE_HOLD_DETAIL_TABLE_NAME + " pd, " + AppConstant.STOCK_TABLE_NAME + " st where pd." + AppConstant.PURCHASE_DETAIL_STOCK_ID + "=st." + AppConstant.STOCK_ID
                + " and pd." + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = " + id;
        database = databaseHelper.getReadableDatabase();
        cursor = database.rawQuery(query, null);
        salesAndPurchaseItemList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                salesAndPurchaseItem = new SalesAndPurchaseItem();
                salesAndPurchaseItem.setId(cursor.getLong(0));
                salesAndPurchaseItem.setStockID(cursor.getLong(1));
                salesAndPurchaseItem.setItemName(cursor.getString(2));
                salesAndPurchaseItem.setQty(cursor.getInt(3));
                salesAndPurchaseItem.setPrice(cursor.getDouble(4));
                salesAndPurchaseItem.setTotalPrice(cursor.getDouble(5));
                //  salesAndPurchaseItem.setDiscountPercent(cursor.getDouble(6));
                salesAndPurchaseItem.setDiscountAmount(cursor.getDouble(7));
                salesAndPurchaseItem.setStockCode(cursor.getString(8));
                salesAndPurchaseItem.setBarcode(cursor.getString(9));
                salesAndPurchaseItemList.add(salesAndPurchaseItem);
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        return salesAndPurchaseItemList;
    }

    public boolean updatePurchaseDetailByID(Long id, Long purchaseID, Long stockID, int qty, double price, String discount, Long taxID, double total, double taxAmt, double discountAmt, double taxRate) {
        query = "update " + AppConstant.PURCHASE_HOLD_DETAIL_TABLE_NAME + " set " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID +
                " = " + purchaseID + ", " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = " + stockID + ", " + AppConstant.PURCHASE_DETAIL_QTY + " = " + qty + ", " + AppConstant.PURCHASE_DETAIL_PRICE
                + " = " + price + ", " + AppConstant.PURCHASE_DETAIL_DISCOUNT + " = ?, " + AppConstant.PURCHASE_DETAIL_TAX_ID + " = " + taxID + ", " + AppConstant.PURCHASE_DETAIL_TOTAL + " = " + total +
                ", " + AppConstant.PURCHASE_DETAIL_TAX_AMT + " = " + taxAmt + ", " + AppConstant.PURCHASE_DETAIL_DISCOUNT_AMT + " = " + discountAmt + ", "
                + AppConstant.PURCHASE_DETAIL_TAX_RATE + " = " + taxRate + " where " + AppConstant.PURCHASE_DETAIL_ID + "=" + id;
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query, new String[]{discount});
        return true;
    }

    public boolean deletePurchaseDetailByID(Long id) {
        database = databaseHelper.getWritableDatabase();
        database.delete(AppConstant.PURCHASE_HOLD_DETAIL_TABLE_NAME, AppConstant.PURCHASE_DETAIL_ID + "=?", new String[]{id.toString()});
        return true;
    }

    public List<Long> getPurchaseDetailIdList(Long purchaseID) {
        idList = new ArrayList<>();
        query = "select " + AppConstant.PURCHASE_DETAIL_ID + " from " + AppConstant.PURCHASE_HOLD_DETAIL_TABLE_NAME + " where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = " + purchaseID;
        database = databaseHelper.getWritableDatabase();
        cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                idList.add(cursor.getLong(0));
            } while (cursor.moveToNext());
        }
        return idList;
    }
}
