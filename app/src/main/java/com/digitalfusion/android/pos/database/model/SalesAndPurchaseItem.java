package com.digitalfusion.android.pos.database.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by MD003 on 8/29/16.
 */
public class SalesAndPurchaseItem implements Serializable {

    private Long id;
    private Long salesID;
    private String itemName;
    private String barcode;
    private Long stockID;
    private String stockCode;
    private int qty;
    private Long taxID;
    private double taxAmt;
    private double taxRate;
    private Double price;
    private Double totalPrice;
    private Double discountAmount;
    private String discountPercent;
    private String taxType;

    public SalesAndPurchaseItem() {
    }

    public SalesAndPurchaseItem(Long salesID, Long stockID, int qty, Long taxID, Double price, Double totalPrice, Double discountAmount, String discountPercent) {
        this.salesID = salesID;
        this.stockID = stockID;
        this.qty = qty;
        this.taxID = taxID;
        this.price = price;
        this.totalPrice = totalPrice;
        this.discountAmount = discountAmount;
        this.discountPercent = discountPercent;
    }

    public double getTaxAmt() {
        return taxAmt;
    }

    public void setTaxAmt(double taxAmt) {
        this.taxAmt = taxAmt;
    }

    public Long getTaxID() {
        return taxID;
    }

    public void setTaxID(Long taxID) {
        this.taxID = taxID;
    }

    public Long getSalesID() {
        return salesID;
    }

    public void setSalesID(Long salesID) {
        this.salesID = salesID;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public Long getStockID() {
        return stockID;
    }

    public void setStockID(Long stockID) {
        this.stockID = stockID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = Double.valueOf(price);
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getDiscountAmount() {

        return discountAmount;
    }

    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = Double.valueOf(discountAmount);
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    @Override
    public String toString() {
        return "SalesAndPurchaseItem{" +
                "id=" + id +
                ", salesID=" + salesID +
                ", itemName='" + itemName + '\'' +
                ", barcode='" + barcode + '\'' +
                ", stockID=" + stockID +
                ", stockCode='" + stockCode + '\'' +
                ", qty=" + qty +
                ", taxID=" + taxID +
                ", taxAmt=" + taxAmt +
                ", taxRate=" + taxRate +
                ", price=" + price +
                ", totalPrice=" + totalPrice +
                ", discountAmount=" + discountAmount +
                ", discountPercent='" + discountPercent + '\'' +
                ", taxType='" + taxType + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return Objects.equals(stockID, ((SalesAndPurchaseItem) obj).getStockID());
    }


}
