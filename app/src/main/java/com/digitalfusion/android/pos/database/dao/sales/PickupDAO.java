package com.digitalfusion.android.pos.database.dao.sales;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import com.digitalfusion.android.pos.database.dao.ParentDAO;
import com.digitalfusion.android.pos.database.dao.IdGeneratorDAO;
import com.digitalfusion.android.pos.database.model.SalePickup;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

/**
 * Created by MD002 on 8/30/16.
 */
public class PickupDAO extends ParentDAO {
    private static ParentDAO pickupDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private SalePickup salePickup;

    private PickupDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
    }

    public static PickupDAO getPickupDaoInstance(Context context) {
        if (pickupDaoInstance == null) {
            pickupDaoInstance = new PickupDAO(context);
        }
        return (PickupDAO) pickupDaoInstance;
    }

    public boolean addStockPickup(String date, String phoneNo, Long salesID, String day, String month, String year) {
        genID = idGeneratorDAO.getLastIdValue("Pickup");
        genID++;
        query = "insert into " + AppConstant.PICKUP_TABLE_NAME + "(" + AppConstant.PICKUP_ID + ", " + AppConstant.PICKUP_DATE +
                ", " + AppConstant.PICKUP_PHONE_NUM + ", " + AppConstant.PICKUP_SALES_ID + ", " + AppConstant.PICKUP_STATUS + ", " + AppConstant.CREATED_DATE
                + ", " + AppConstant.PICKUP_DAY + ", " + AppConstant.PICKUP_MONTH + ", " + AppConstant.PICKUP_YEAR
                + ") values(" + genID + ",?,?," + salesID + ", " + 0 + ",?,?,?,?);";
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query, new String[]{date, phoneNo, DateUtility.getTodayDate(), day, month, year});
        return true;
    }

    public SalePickup getPickInfoBySalesID(Long salesID, InsertedBooleanHolder flag) {
        query = "select p." + AppConstant.PICKUP_ID + ", p." + AppConstant.PICKUP_DATE +
                ", p." + AppConstant.PICKUP_PHONE_NUM + ", p." + AppConstant.PICKUP_SALES_ID + ", p." + AppConstant.PICKUP_STATUS + ", s." + AppConstant.SALES_CUSTOMER_ID
                + ", c." + AppConstant.CUSTOMER_NAME + ", p." + AppConstant.PICKUP_DAY + ", p." + AppConstant.PICKUP_MONTH + ", p." + AppConstant.PICKUP_YEAR
                + " from " + AppConstant.PICKUP_TABLE_NAME + " as p, " + AppConstant.SALES_TABLE_NAME + " as s, " + AppConstant.CUSTOMER_TABLE_NAME
                + " as c where p." + AppConstant.PICKUP_SALES_ID + "=s." + AppConstant.SALES_ID + " and s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID + " and p."
                + AppConstant.PICKUP_SALES_ID + "=" + salesID;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            salePickup = new SalePickup();
            if (cursor.moveToFirst()) {
                salePickup.setId(cursor.getLong(0));
                salePickup.setPickupDate(cursor.getString(1));
                salePickup.setPhoneNo(cursor.getString(2));
                salePickup.setSalesID(cursor.getLong(3));
                salePickup.setStatus(cursor.getInt(4));
                salePickup.setCustomerID(cursor.getLong(5));
                salePickup.setCustomerName(cursor.getString(6));
                salePickup.setDay(cursor.getString(7));
                salePickup.setMonth(cursor.getString(8));
                salePickup.setYear(cursor.getString(9));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            cursor.close();
        }
        return salePickup;
    }

    public boolean updatePickupByID(Long id, String date, String phoneNo, Long salesID, String day, String month, String year, int status) {
        query = "update " + AppConstant.PICKUP_TABLE_NAME + " set " + AppConstant.PICKUP_DATE +
                "=?, " + AppConstant.PICKUP_PHONE_NUM + "=?, " + AppConstant.PICKUP_SALES_ID + "=" + salesID + ", " + AppConstant.PICKUP_STATUS + "=" + status + ", " + AppConstant.CREATED_DATE
                + "=?, " + AppConstant.PICKUP_DAY + "=?, " + AppConstant.PICKUP_MONTH + "=?, " + AppConstant.PICKUP_YEAR
                + "=? where " + AppConstant.PICKUP_ID + "=" + id;
        database = databaseHelper.getWritableDatabase();
        database.execSQL(query, new String[]{date, phoneNo, day, month, year});
        return true;
    }

    public boolean makeOrderPicked(Long pickupID) {
        query = "update " + AppConstant.PICKUP_TABLE_NAME + " set " + AppConstant.PICKUP_STATUS + " = " + 1 + " where " + AppConstant.PICKUP_ID + " = " + pickupID;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean makeOrderUnPicked(Long pickupID) {
        query = "update " + AppConstant.PICKUP_TABLE_NAME + " set " + AppConstant.PICKUP_STATUS + " = " + 0 + " where " + AppConstant.PICKUP_ID + " = " + pickupID;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }
}
