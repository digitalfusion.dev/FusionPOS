package com.digitalfusion.android.pos.information;

/**
 * Created by MD003 on 8/14/17.
 */


public class RegisterResponse {

    private Long id;
    private String licenceInfo;
    private String paymentInfo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicenceInfo() {
        return licenceInfo;
    }

    public void setLicenceInfo(String licenceInfo) {
        this.licenceInfo = licenceInfo;
    }

    public String getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(String paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    @Override
    public String toString() {
        return "RegisterResponse{" +
                "id=" + id +
                ", licenceInfo='" + licenceInfo + '\'' +
                ", paymentInfo='" + paymentInfo + '\'' +
                '}';
    }
}
