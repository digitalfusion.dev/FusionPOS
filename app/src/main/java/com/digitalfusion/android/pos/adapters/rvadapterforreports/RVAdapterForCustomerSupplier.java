package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;

import java.util.List;

/**
 * Created by MD002 on 10/24/17.
 */

public class RVAdapterForCustomerSupplier extends ParentRVAdapterForReports {
    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    protected boolean showLoader = false;
    private List<CustomerSupplier> customerSupplierList;
    private LoaderViewHolder loaderViewHolder;
    private ClickListener clickListener;

    public RVAdapterForCustomerSupplier(List<CustomerSupplier> customerSupplierList) {
        this.customerSupplierList = customerSupplierList;
    }

    public RVAdapterForCustomerSupplier() {
    }

    public void setCustomerSupplierList(List<CustomerSupplier> customerSupplierList) {
        this.customerSupplierList = customerSupplierList;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            if (customerSupplierList != null && customerSupplierList.size() != 0) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;
            }

        }

        return VIEWTYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEWTYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_supplier_name_item_view, parent, false);


            return new CustomerSupplierViewHolder(v);

        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            return new LoaderViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof LoaderViewHolder) {

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;

            this.loaderViewHolder = loaderViewHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);

        }
        if (viewHolder instanceof CustomerSupplierViewHolder) {

            final CustomerSupplierViewHolder holder = (CustomerSupplierViewHolder) viewHolder;

            holder.nameTextView.setText(customerSupplierList.get(position).getName());
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onClick(position);
                }
            });

        } else {
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public int getItemCount() {
        if (customerSupplierList == null || customerSupplierList.size() == 0) {
            return 0;
        } else {
            return customerSupplierList.size() + 1;
        }

    }

    public void setShowLoader(boolean showLoader) {
        this.showLoader = showLoader;

        if (loaderViewHolder != null) {
            Log.w("hele", "loader full not null");
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }

        }
    }

    public class CustomerSupplierViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView;
        LinearLayout linearLayout;
        View itemView;

        public CustomerSupplierViewHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;
            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linear_layout);
        }
    }


}
