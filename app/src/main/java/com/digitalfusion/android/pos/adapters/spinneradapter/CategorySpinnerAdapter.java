package com.digitalfusion.android.pos.adapters.spinneradapter;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Category;

import java.util.List;

/**
 * Created by MD001 on 8/17/16.
 */
public class CategorySpinnerAdapter extends ArrayAdapter<Category> {

    public Resources res;
    private Context context1;
    private List<Category> data;
    private LayoutInflater inflater;
    private OnItemClickListener mItemClickListener;
    private TextView tvCategory;

    public CategorySpinnerAdapter(Context context, List<Category> objects) {
        super(context, R.layout.spinner_form_popup_item, objects);

        context1 = context;
        data = objects;

        inflater = (LayoutInflater) context1
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public View getDropDownView(final int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("DROPDOWN")) {
            view = inflater.inflate(R.layout.spinner_form_popup_item, parent, false);
            view.setTag("DROPDOWN");
        }

        TextView tvCategory = (TextView) view.findViewById(R.id.spinner_form_dropdown_item);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.w("on clikc", "spinner item");
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(v, position);
                }
            }
        });

        tvCategory.setText(data.get(position).getName().toString());

        return view;
    }

    public List<Category> getData() {
        return data;
    }

    public void setData(List<Category> data) {
        this.data = data;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("NON_DROPDOWN")) {
            view = inflater.inflate(R.layout.spinner_form_item, parent, false);
            view.setTag("NON_DROPDOWN");
        }
        tvCategory = (TextView) view.findViewById(R.id.spinner_form_item);

        tvCategory.setText(data.get(position).getName().toString());
        return view;
    }

    @Override
    public int getCount() {
        return data.size();
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}