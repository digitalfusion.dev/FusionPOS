package com.digitalfusion.android.pos.database.model;

/**
 * Created by MD002 on 8/24/16.
 */
public class BusinessSetting {
    private Long id;
    private String businessName;
    private String phoneNo;
    private String email;
    private String website;
    private String street;
    private String valuationMethod;
    private String sign;
    private String currency;
    private byte[] logo;
    private Long currencyID;
    private String state;
    private String city;
    private String township;
    private String address = null;
    private String businessType;
    private String userName;

    public BusinessSetting() {
    }

    public BusinessSetting(String state, String city, String township, String street) {
        this.state = state;
        this.city = city;
        this.township = township;
        this.street = street;
    }

    public Long getCurrencyID() {
        return currencyID;
    }

    public void setCurrencyID(Long currencyID) {
        this.currencyID = currencyID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getValuationMethod() {
        return valuationMethod;
    }

    public void setValuationMethod(String valuationMethod) {
        this.valuationMethod = valuationMethod;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public boolean isEmptyBusinessName() {

        if (businessName != null) {

            return businessName.isEmpty();

        } else {
            return true;
        }

    }

    public boolean isEmptyAddress() {

        if (street != null) {

            return street.isEmpty();

        } else {
            return true;
        }

    }

    public boolean isEmptyPhoneNo() {

        if (phoneNo != null) {

            return phoneNo.isEmpty();

        } else {
            return true;
        }

    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTownship() {
        return township;
    }

    public void setTownship(String township) {
        this.township = township;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getAddress() {
        if (street != null) {
            address = street;
        }
        if (city != null) {
            if (address != null) {
                address += ", " + city;
            } else {
                address = city;
            }
        }
        if (township != null) {
            if (address != null) {
                address += ", " + township;
            } else {
                address = township;
            }
        }
        if (state != null) {
            if (address != null) {
                address += ", " + state;
            } else {
                address = state;
            }
        }
        return address;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
