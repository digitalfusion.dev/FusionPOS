package com.digitalfusion.android.pos.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.ExpenseAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.ExpenseOrIncomeSearchAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.IncomeAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForExpenseManagerFilter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForExpenseManagerList;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.ExpenseIncome;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.ExpenseManagerUtil;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.TypefaceSpan;
import com.digitalfusion.android.pos.views.FusionToast;
import com.example.searchview.MaterialSearchView;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 12/8/16.
 */

public class ExpenseManager extends Fragment {

    private View mainLayout;
    private FloatingActionButton addNewExpenseFab;
    private RecyclerView expenseManagerListRecyclerView;
    private AutoCompleteTextView incomeManagerNameTextInputEditTextMd;
    private EditText incomeManagerAmountTextInputEditTextMd;
    private EditText incomeManagerRemarkTextInputEditTextMd;
    private LinearLayout dateLLMd_income;
    private TextView addDateIncomeTextView;
    private Button saveIncomeBtnMd;
    private Button cancelIncomeBtnMd;
    private MaterialDialog addNewIncomeManagerMaterialDialog;
    private FloatingActionButton addNewIncomeFab;
    private DatePickerDialog customeDatePickerDialog;
    private MaterialDialog addNewExpenseManagerMaterialDialog;
    private MaterialDialog filterDialog;
    private MaterialDialog typeFilterDialog;
    private TextView filterTextView;
    private TextView typeFilterTextView;
    private FloatingActionMenu floatingActionMenu;
    private TextView noTransactionTextView;
    private Button customRangeOkBtn, customRangeCancelBtn;
    private MaterialDialog customRangeDialog;
    private TextView startDateTextView, endDateTextView;
    private TextView traceDate;
    private DatePickerDialog startDatePickerDialog;
    private MaterialDialog deleteAlertDialog;
    private Button yesSaleDeleteMdButton;

    //UI component for addNewExpenseManagerDialog
    private TextView addDateExpenseTextView;
    private AutoCompleteTextView expenseManagerNameTextInputEditTextMd;
    private EditText expenseManagerAmountTextInputEditTextMd;
    private EditText expenseManagerRemarkTextInputEditTextMd;
    private LinearLayout dateLLMd;
    private Button saveExpenseBtnMd;
    private Button cancelExpenseBtnMd;
    private MaterialDialog viewRemarkExpenseManagerMaterialDialog;
    private DatePickerDialog editDatePickerDialog;
    private DatePickerDialog datePickerDialog;

    //UI component for ViewRemarkDialog
    private TextView viewRemark;
    private MaterialSearchView searchView;

    //Values
    private String incomeOrExpenseName;
    private String expenseManagerType;
    private Double expenseOrIncomeAmount;
    private String expenseOrIncomeRemark;
    private int day;
    private int month;
    private int years;
    private String date;
    private int editDay;
    private int editMonth;
    private int editYear;
    private String editDate;
    private int editPosition;
    private int viewRemarkPosition;
    private List<ExpenseIncome> expenseManagerVOList;
    private List<String> filterList;
    private List<ExpenseManagerUtil.FilterName> filterTypeList;
    private boolean editFlag;
    private String type = ExpenseIncome.Type.All.toString();

    private Calendar now;
    private Calendar editCalendar;
    private ExpenseAutoCompleteAdapter expenseAutoCompleteAdapter;
    private IncomeAutoCompleteAdapter incomeAutoCompleteAdapter;
    private ExpenseOrIncomeSearchAdapter searchAdapter;
    private Context context;
    private com.digitalfusion.android.pos.database.business.ExpenseManager expenseManagerBusiness;
    private RVSwipeAdapterForExpenseManagerList rvSwipeAdapterForExpenseManagerList;
    private RVAdapterForFilter rvAdapterForFilter;
    private RVAdapterForExpenseManagerFilter rvAdapterForTypeFilter;
    private String startDate;
    private String endDate;
    private Calendar calendar;
    private boolean isSearch = false;
    private boolean shouldLoad = true;
    private String searchText = "";
    private String customStartDate,
            customEndDate;
    private int deletepos;
    //    private TextView searchedResultTxt;
    private String allTrans;
    private String thisWeek;
    private String lastWeek;
    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;
    private String customRange;
    private String customDate;
    private boolean darkmode;
    private String incomeStr;
    private String expenseStr;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainLayout = inflater.inflate(R.layout.expense_manager, null);
        context = getContext();
        darkmode = POSUtil.isNightMode(context);
        setHasOptionsMenu(true);
        calendar = Calendar.getInstance();
        MainActivity.setCurrentFragment(this);

        startDate = "000000000000";
        endDate = "9999999999999999";

        String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.expense_manager}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);

        allTrans = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);
        thisMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month}).getString(0);
        lastMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month}).getString(0);
        thisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year}).getString(0);
        lastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString(0);
        customRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_range}).getString(0);
        customDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_date}).getString(0);
        thisWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_week}).getString(0);
        lastWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week}).getString(0);
        incomeStr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.income}).getString(0);
        expenseStr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.expense}).getString(0);

        filterList = new ArrayList<>();
        filterList.add(allTrans);
        filterList.add(thisWeek);
        filterList.add(lastWeek);
        filterList.add(thisMonth);
        filterList.add(lastMonth);
        filterList.add(thisYear);
        filterList.add(lastYear);
        filterList.add(customRange);
        filterList.add(customDate);

        rvAdapterForFilter = new RVAdapterForFilter(filterList);
        filterTypeList = ExpenseManagerUtil.getFilterNameList(getContext());
        rvAdapterForTypeFilter = new RVAdapterForExpenseManagerFilter(filterTypeList);

        buildDateFilterDialog();
        buildAddNewExpenseManagerDialog();
        buildViewRemarkExpenseManagerDialog();
        buildAddNewIncomeManagerDialog();
        loadUI();


        SpannableString s = new SpannableString(expenseStr.toString());
        s.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        addNewExpenseFab.setLabelText(s.toString());

        SpannableString income = new SpannableString(incomeStr.toString());
        income.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, income.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        addNewIncomeFab.setLabelText(income.toString());

        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });

        configCustomDatePickerDialog();
        buildTypeFilterDialog();
        builsCancelAlertDialog();

        typeFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeFilterDialog.show();
            }
        });

        expenseManagerListRecyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {
                if (velocityY > 100) {
                    floatingActionMenu.hideMenu(true);
                } else if (velocityY < -100) {
                    floatingActionMenu.showMenu(true);
                }


                return false;
            }
        });

        rvAdapterForTypeFilter.setCurrentPos(0);

        return mainLayout;
    }

    private void builsCancelAlertDialog() {
        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, false).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //  .positiveText(yes.getString(0))
                //.negativeText(no.getString(0))
                .build();

        String   sureDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete}).getString(0);
        TextView textView   = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(sureDelete);

        yesSaleDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });


    }

    private void buildDateFilterDialog() {

        TypedArray filterByDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date});

        filterDialog = new MaterialDialog.Builder(context).

                title(filterByDate.getString(0))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))

                .build();
    }

    private void buildTypeFilterDialog() {

        String filter = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_type}).getString(0);

        typeFilterDialog = new MaterialDialog.Builder(context).

                title(filter)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForTypeFilter, new LinearLayoutManager(context))

                .build();
    }

    private void configCustomDatePickerDialog() {

        customeDatePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        String date = DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        filterTextView.setText(date);

                        loadExpeseIncomeHistory(0, 10);

                        refreshList();
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );


        if (darkmode)
            customeDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        customeDatePickerDialog.setThemeDark(darkmode);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        expenseManagerBusiness = new com.digitalfusion.android.pos.database.business.ExpenseManager(context);
        now = Calendar.getInstance();
        editCalendar = Calendar.getInstance();
        day = now.get(Calendar.DAY_OF_MONTH);
        month = now.get(Calendar.MONTH) + 1;
        years = now.get(Calendar.YEAR);
        searchAdapter = new ExpenseOrIncomeSearchAdapter(context, expenseManagerBusiness);

        configRecyclerView();
        setttingOnClickListener();

        setFilterTextView(filterList.get(0));

        searchView.setAdapter(searchAdapter);
        mainLayout.findViewById(R.id.interpretor).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                floatingActionMenu.close(true);
                return false;
            }
        });

        setupRecyclerView();

        rvAdapterForTypeFilter.setmItemClickListener(new RVAdapterForExpenseManagerFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                //                searchedResultTxt.setVisibility(View.GONE);
                setupRecyclerView();
                if (filterTextView.getText().toString().equalsIgnoreCase("-")) {
                    filterTextView.setText(filterList.get(1));
                    rvAdapterForFilter.setCurrentPos(1);
                }

                shouldLoad = true;
                isSearch = false;

                if (filterTypeList.get(position).getType().toString().equalsIgnoreCase(ExpenseIncome.Type.All.toString())) {
                    type = filterTypeList.get(position).getType().toString();

                    loadExpeseIncomeHistory(0, 10);
                    refreshList();
                    setTypeFilterTextView(allTrans);

                } else if (filterTypeList.get(position).getType().toString().equalsIgnoreCase(ExpenseIncome.Type.Income.toString())) {
                    type = ExpenseIncome.Type.Expense.toString();

                    loadExpeseIncomeHistory(0, 10);
                    refreshList();
                    setTypeFilterTextView(incomeStr);

                } else if (filterTypeList.get(position).getType().toString().equalsIgnoreCase(ExpenseIncome.Type.Expense.toString())) {
                    type = ExpenseIncome.Type.Income.toString();

                    loadExpeseIncomeHistory(0, 10);
                    refreshList();
                    setTypeFilterTextView(expenseStr);
                }

                typeFilterDialog.dismiss();
            }
        });


        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //expenseManagerVOList = new ArrayList<>();

                //expenseManagerVOList.add(searchAdapter.getSuggestionList().get(position));

                // refreshList();

                // shouldLoad = false;


                searchView.closeSearch();

                //filterTextView.setText("-");

                //typeFilterTextView.setText("-");

                Bundle bundle = new Bundle();
                bundle.putSerializable("expense", searchAdapter.getSuggestion().get(position));
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.EXPENSE_DETAIL);
                detailIntent.putExtras(bundle);
                startActivity(detailIntent);
                //                searchedResultTxt.setVisibility(View.VISIBLE);

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                shouldLoad = true;
                isSearch = true;

                loadExpeseIncomeHistory(0, 10, query.toString());
                refreshList();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        expenseAutoCompleteAdapter = new ExpenseAutoCompleteAdapter(context, expenseManagerBusiness);
        incomeAutoCompleteAdapter = new IncomeAutoCompleteAdapter(context, expenseManagerBusiness);

        expenseManagerNameTextInputEditTextMd.setThreshold(1);
        expenseManagerNameTextInputEditTextMd.setAdapter(expenseAutoCompleteAdapter);
        expenseManagerNameTextInputEditTextMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                expenseManagerNameTextInputEditTextMd.setText(expenseAutoCompleteAdapter.getSuggestion().get(position).getName());

            }
        });

        incomeManagerNameTextInputEditTextMd.setThreshold(1);
        incomeManagerNameTextInputEditTextMd.setAdapter(incomeAutoCompleteAdapter);
        incomeManagerNameTextInputEditTextMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                incomeManagerNameTextInputEditTextMd.setText(incomeAutoCompleteAdapter.getSuggestion().get(position).getName());
            }
        });


        rvSwipeAdapterForExpenseManagerList.setViewDetailClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("expense", expenseManagerVOList.get(postion));
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.EXPENSE_DETAIL);
                detailIntent.putExtras(bundle);
                startActivity(detailIntent);

            }
        });


        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                setupRecyclerView();
                //                searchedResultTxt.setVisibility(View.GONE);
                if (typeFilterTextView.getText().toString().equalsIgnoreCase("-")) {
                    typeFilterTextView.setText(allTrans);
                    rvAdapterForTypeFilter.setCurrentPos(0);
                }

                if (filterList.get(position).equalsIgnoreCase(allTrans)) {
                    setFilterTextView(filterList.get(position));

                    startDate = "00000000000";
                    endDate = "999999999";

                    loadExpeseIncomeHistory(0, 10);
                    refreshList();

                } else if (filterList.get(position).equalsIgnoreCase(thisMonth)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisMonthStartDate();
                    endDate = DateUtility.getThisMonthEndDate();

                    loadExpeseIncomeHistory(0, 10);
                    refreshList();
                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastMonthStartDate();
                    endDate = DateUtility.getLastMonthEndDate();

                    loadExpeseIncomeHistory(0, 10);
                    refreshList();
                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisYearStartDate();
                    endDate = DateUtility.getThisYearEndDate();

                    loadExpeseIncomeHistory(0, 10);
                    refreshList();
                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastYearStartDate();
                    endDate = DateUtility.getLastYearEndDate();

                    loadExpeseIncomeHistory(0, 10);
                    refreshList();
                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {
                    // setFilterTextView(filterList.get(position));
                    customRangeDialog.show();
                } else if (filterList.get(position).equalsIgnoreCase(customDate)) {
                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "customeDate");
                } else if (filterList.get(position).equalsIgnoreCase(thisWeek)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getStartDateOfWeekString();
                    endDate = DateUtility.getEndDateOfWeekString();

                    loadExpeseIncomeHistory(0, 10);
                    refreshList();
                } else if (filterList.get(position).equalsIgnoreCase(lastWeek)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getStartDateOfLastWeekString();
                    endDate = DateUtility.getEndDateOfLastWeekString();

                    loadExpeseIncomeHistory(0, 10);
                    refreshList();
                }

                filterDialog.dismiss();
            }
        });

        rvAdapterForFilter.setCurrentPos(3);
        rvAdapterForTypeFilter.setCurrentPos(0);


        rvSwipeAdapterForExpenseManagerList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                deleteAlertDialog.show();
                deletepos = postion;
            }
        });

        yesSaleDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expenseManagerBusiness.deleteIncomeExpense(expenseManagerVOList.get(deletepos).getId());
                if (deletepos != 0) {
                    expenseManagerVOList.remove(deletepos);
                    rvSwipeAdapterForExpenseManagerList.notifyItemRemoved(deletepos);
                    rvSwipeAdapterForExpenseManagerList.notifyItemRangeChanged(deletepos, expenseManagerVOList.size());
                } else {
                    expenseManagerVOList.remove(deletepos);
                    rvSwipeAdapterForExpenseManagerList.setExpenseManagerVOList(expenseManagerVOList);
                    rvSwipeAdapterForExpenseManagerList.notifyDataSetChanged();
                }

                deleteAlertDialog.dismiss();
                refreshList();
            }
        });

        buildingCustomRangeDialog();

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDate = customStartDate;
                endDate = customEndDate;

                //f.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));
                filterTextView.setText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));

                // loadSaleHistory(0, 10);
                // refreshRecyclerView();
                loadExpeseIncomeHistory(0, 10);
                refreshList();
                customRangeDialog.dismiss();
            }
        });
    }


    private void setupRecyclerView() {
        expenseManagerListRecyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {
                if (shouldLoad) {
                    rvSwipeAdapterForExpenseManagerList.setShowLoader(true);
                    loadmore();
                }
            }
        });
    }

    private void refreshList() {
        if (expenseManagerVOList.size() > 0) {
            expenseManagerListRecyclerView.setVisibility(View.VISIBLE);
            noTransactionTextView.setVisibility(View.GONE);
            rvSwipeAdapterForExpenseManagerList.setExpenseManagerVOList(expenseManagerVOList);
            rvSwipeAdapterForExpenseManagerList.notifyDataSetChanged();
        } else {
            expenseManagerListRecyclerView.setVisibility(View.GONE);
            noTransactionTextView.setVisibility(View.VISIBLE);
        }
    }

    public boolean checkValidationIncome() {
        boolean status = true;

        if (incomeManagerNameTextInputEditTextMd.getText().toString().trim().length() < 1) {
            //
            String name = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_name}).getString(0);
            incomeManagerNameTextInputEditTextMd.setError(name);
            status = false;

        }

        if (incomeManagerAmountTextInputEditTextMd.getText().toString().trim().length() < 1) {

            String amt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_amount}).getString(0);
            incomeManagerAmountTextInputEditTextMd.setError(amt);

            status = false;

        }


        typeFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                typeFilterDialog.show();
            }
        });

        return status;

    }

    public void loadmore() {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isSearch) {
                    expenseManagerVOList.addAll(loadMore(expenseManagerVOList.size(), 9, searchText));
                } else {
                    expenseManagerVOList.addAll(loadMore(expenseManagerVOList.size(), 9));
                }

                expenseManagerVOList.addAll(loadMore(expenseManagerVOList.size(), 9));

                rvSwipeAdapterForExpenseManagerList.setShowLoader(false);

                refreshList();
            }
        }, 500);
    }

    public List<ExpenseIncome> loadMore(int startLimit, int endLimit) {

        return expenseManagerBusiness.getAllIncomeExpenses(startLimit, endLimit, startDate, endDate, type);


    }

    public List<ExpenseIncome> loadMore(int startLimit, int endLimit, String query) {

        return expenseManagerBusiness.getAllExpenseOrIncomeNameSearch(query, startLimit, endLimit);


    }

    private void setttingOnClickListener() {

        dateLLMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                POSUtil.hideKeyboard(mainLayout, context);

                if (editFlag) {
                    editDatePickerDialog.show(getActivity().getFragmentManager(), "show");
                } else {
                    datePickerDialog.show(getActivity().getFragmentManager(), "show");
                }


            }
        });

        dateLLMd_income.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                POSUtil.hideKeyboard(mainLayout, context);

                if (editFlag) {
                    editDatePickerDialog.show(getActivity().getFragmentManager(), "show");
                } else {
                    datePickerDialog.show(getActivity().getFragmentManager(), "show");
                }


            }
        });

        cancelIncomeBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewIncomeManagerMaterialDialog.dismiss();
            }
        });

        saveIncomeBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editFlag) {
                    if (checkValidationIncome()) {

                        expenseManagerType = ExpenseIncome.Type.Income.toString();

                        getIncomeValuesFromView();

                        InsertedBooleanHolder status = new InsertedBooleanHolder();

                        Log.w("income ", "position " + editPosition);

                        status.setInserted(expenseManagerBusiness.updateExpenseOrIncome(editDate, expenseOrIncomeAmount, incomeOrExpenseName,
                                expenseManagerType, expenseOrIncomeRemark, Integer.toString(editDay), Integer.toString(editMonth),
                                Integer.toString(editYear), expenseManagerVOList.get(editPosition).getId()));  ////////

                        if (status.isInserted()) {

                            String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.income_edited_successfully}).getString(0);
                            //  POSUtil.showSnackBar(mainLayout, success);
                            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);

                            addNewIncomeManagerMaterialDialog.dismiss();

                            refreshExpenseManagerList();

                        }
                    }


                } else {
                    if (checkValidationIncome()) {

                        expenseManagerType = ExpenseIncome.Type.Income.toString();

                        getIncomeValuesFromView();

                        InsertedBooleanHolder status = new InsertedBooleanHolder();


                        status.setInserted(expenseManagerBusiness.addNewIncomeOrExpense(incomeOrExpenseName, expenseManagerType, expenseOrIncomeAmount,
                                expenseOrIncomeRemark, Integer.toString(day), Integer.toString(month), Integer.toString(years), date));  ////////

                        if (status.isInserted()) {

                            String addSuccess = context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_income_added_successfully}).getString(0);
                            //POSUtil.showSnackBar(mainLayout, addSuccess);
                            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);


                            refreshExpenseManagerList();

                            addNewIncomeManagerMaterialDialog.dismiss();

                        }


                    }
                }


            }
        });


        addNewIncomeFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                floatingActionMenu.close(true);

                resetDialogDataIncome();

                Log.w("here", "on click");

                String newIncome = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_new_income}).getString(0);
                addNewIncomeManagerMaterialDialog.setTitle(newIncome);

                POSUtil.showKeyboard(incomeManagerNameTextInputEditTextMd);

                addNewIncomeManagerMaterialDialog.show();

            }
        });


        addNewExpenseFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                floatingActionMenu.close(true);

                resetDialogData();

                String newExpense = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_new_expense}).getString(0);
                addNewExpenseManagerMaterialDialog.setTitle(newExpense);

                addNewExpenseManagerMaterialDialog.show();


                POSUtil.showKeyboard(expenseManagerNameTextInputEditTextMd);

            }
        });


        addNewIncomeManagerMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                POSUtil.hideKeyboard(mainLayout, context);
            }
        });

        addNewExpenseManagerMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                Log.w("on dismiss", "dismiss");
                editFlag = false;
                POSUtil.hideKeyboard(mainLayout, context);
            }
        });


        rvSwipeAdapterForExpenseManagerList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                editPosition = postion;

                editFlag = true;

                if (expenseManagerVOList.get(editPosition).isIncome()) {

                    resetDialogDataIncome();

                    setDataEditIncomeDialog(expenseManagerVOList.get(postion));

                    String editIncome = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_income}).getString(0);
                    addNewIncomeManagerMaterialDialog.setTitle(editIncome);

                    POSUtil.showKeyboard(incomeManagerNameTextInputEditTextMd);

                    addNewIncomeManagerMaterialDialog.show();

                } else {

                    resetDialogData();

                    String editExpense = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_expense}).getString(0);
                    addNewExpenseManagerMaterialDialog.setTitle(editExpense);

                    setDataEditExpenseDialog();

                    POSUtil.showKeyboard(expenseManagerNameTextInputEditTextMd);

                    addNewExpenseManagerMaterialDialog.show();
                }


            }
        });


        rvSwipeAdapterForExpenseManagerList.setViewRemarkClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                viewRemarkPosition = postion;

                setDataRemarkDialog();

                viewRemarkExpenseManagerMaterialDialog.show();

            }
        });


        cancelExpenseBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewExpenseManagerMaterialDialog.dismiss();

            }
        });


        saveExpenseBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkValidationExpense()) {

                    if (editFlag) {

                        if (checkValidationExpense()) {
                            expenseManagerType = ExpenseIncome.Type.Expense.toString();

                            getExpenseValuesFromView();

                            InsertedBooleanHolder status = new InsertedBooleanHolder();

                            Log.w("income ", "position " + editPosition);

                            status.setInserted(expenseManagerBusiness.updateExpenseOrIncome(editDate, expenseOrIncomeAmount, incomeOrExpenseName,
                                    expenseManagerType, expenseOrIncomeRemark, Integer.toString(editDay), Integer.toString(editMonth),
                                    Integer.toString(editYear), expenseManagerVOList.get(editPosition).getId()));  ////////

                            if (status.isInserted()) {

                                String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.expense_edited_successfully}).getString(0);
                                // POSUtil.showSnackBar(mainLayout, success);
                                addNewExpenseManagerMaterialDialog.dismiss();
                                FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);

                                refreshExpenseManagerList();

                            }
                        }


                    } else {

                        expenseManagerType = ExpenseIncome.Type.Expense.toString();

                        getExpenseValuesFromView();

                        InsertedBooleanHolder status = new InsertedBooleanHolder();

                        status.setInserted(expenseManagerBusiness.addNewIncomeOrExpense(incomeOrExpenseName, expenseManagerType, expenseOrIncomeAmount,
                                expenseOrIncomeRemark, Integer.toString(day), Integer.toString(month), Integer.toString(years), date));  ////////

                        if (status.isInserted()) {

                            String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_expense_added_successfully}).getString(0);

                            addNewExpenseManagerMaterialDialog.dismiss();
                            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);

                            refreshExpenseManagerList();

                        }

                    }

                }
            }
        });


        expenseManagerNameTextInputEditTextMd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    expenseManagerAmountTextInputEditTextMd.requestFocus();
                }
                return false;
            }
        });

        incomeManagerNameTextInputEditTextMd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    incomeManagerAmountTextInputEditTextMd.requestFocus();
                }
                return false;
            }
        });

    }


    private void setFilterTextView(String msg) {

        filterTextView.setText(msg);

    }

    private void setTypeFilterTextView(String msg) {

        typeFilterTextView.setText(msg);

    }


    public void buildDatePickerDialog() {

        datePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        day = dayOfMonth;

                        month = monthOfYear + 1;

                        years = year;

                        configDateUI();

                    }
                },

                now.get(Calendar.YEAR),

                now.get(Calendar.MONTH),

                now.get(Calendar.DAY_OF_MONTH)
        );


        if (darkmode)
            datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        datePickerDialog.setThemeDark(darkmode);

    }


    public void getIncomeValuesFromView() {

        incomeOrExpenseName = incomeManagerNameTextInputEditTextMd.getText().toString().trim();

        expenseOrIncomeAmount = Double.parseDouble(incomeManagerAmountTextInputEditTextMd.getText().toString().trim());

        expenseOrIncomeRemark = incomeManagerRemarkTextInputEditTextMd.getText().toString().trim();

    }

    public void setDataEditIncomeDialog(ExpenseIncome expenseIncome) {
        incomeManagerNameTextInputEditTextMd.setText(expenseIncome.getName());
        editYear = Integer.parseInt(expenseIncome.getYear());
        editMonth = Integer.parseInt(expenseIncome.getMonth());
        editDay = Integer.parseInt(expenseIncome.getDay());
        editCalendar.set(editYear, editMonth - 1, editDay);

        configEditDateUI();

        incomeManagerAmountTextInputEditTextMd.setText(POSUtil.doubleToString(expenseIncome.getAmount()));/////////
        incomeManagerRemarkTextInputEditTextMd.setText(expenseIncome.getRemark());////////
        buildEditDatePickerDialog();
    }

    public void buildEditDatePickerDialog() {
        editDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        editDay = dayOfMonth;
                        editMonth = monthOfYear + 1;
                        editYear = year;

                        configEditDateUI();
                    }
                },

                editCalendar.get(Calendar.YEAR),
                editCalendar.get(Calendar.MONTH),
                editCalendar.get(Calendar.DAY_OF_MONTH)

        );


        if (darkmode)
            editDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        editDatePickerDialog.setThemeDark(darkmode);
    }


    public void buildAddNewExpenseManagerDialog() {

        //TypedArray save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        String newExpense = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_new_expense}).getString(0);
        addNewExpenseManagerMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.new_expense, true)
                .title(newExpense).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                // .positiveText(save.getString(0))
                //  .negativeText(cancel.getString(0))
                .build();

    }


    public void buildViewRemarkExpenseManagerDialog() {

        TypedArray remark = context.getTheme().obtainStyledAttributes(new int[]{R.attr.remark});
        //TypedArray ok = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ok});

        viewRemarkExpenseManagerMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.view_remark, true)
                .title(remark.getString(0))
                // .negativeText(ok.getString(0))
                .build();

    }

    private void loadExpeseIncomeHistory(int startLimit, int endLimit, String query) {
        expenseManagerVOList = expenseManagerBusiness.getAllExpenseOrIncomeNameSearch(query, startLimit, endLimit);
    }

    private void loadExpeseIncomeHistory(int startLimit, int endLimit) {
        expenseManagerVOList = expenseManagerBusiness.getAllIncomeExpenses(startLimit, endLimit, startDate, endDate, type);
    }

    private void loadExpeseHistory(int startLimit, int endLimit) {
        expenseManagerVOList = expenseManagerBusiness.getAllExpenses(startLimit, endLimit, startDate, endDate);
    }

    private void loadIncomeHistory(int startLimit, int endLimit) {
        expenseManagerVOList = expenseManagerBusiness.getAllIncome(startLimit, endLimit, startDate, endDate);

    }


    public boolean checkValidationExpense() {

        boolean status = true;

        if (expenseManagerNameTextInputEditTextMd.getText().toString().trim().length() < 1) {
            String name = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_name}).getString(0);
            expenseManagerNameTextInputEditTextMd.setError(name);
            status = false;

        }

        if (expenseManagerAmountTextInputEditTextMd.getText().toString().trim().length() < 1) {

            String amt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_amount}).getString(0);
            expenseManagerAmountTextInputEditTextMd.setError(amt);

            status = false;

        }

        return status;

    }


    public void resetDialogData() {
        day = now.get(Calendar.DAY_OF_MONTH);
        month = now.get(Calendar.MONTH) + 1;
        years = now.get(Calendar.YEAR);

        buildDatePickerDialog();
        configDateUI();

        expenseManagerNameTextInputEditTextMd.setText(null);
        expenseManagerNameTextInputEditTextMd.setError(null);
        expenseManagerAmountTextInputEditTextMd.setText(null);
        expenseManagerAmountTextInputEditTextMd.setError(null);
        expenseManagerRemarkTextInputEditTextMd.setText(null);

    }


    public void setDataEditExpenseDialog() {
        expenseManagerNameTextInputEditTextMd.setText(expenseManagerVOList.get(editPosition).getName());

        editYear = Integer.parseInt(expenseManagerVOList.get(editPosition).getYear());
        editMonth = Integer.parseInt(expenseManagerVOList.get(editPosition).getMonth());
        editDay = Integer.parseInt(expenseManagerVOList.get(editPosition).getDay());

        editCalendar.set(editYear, editMonth - 1, editDay);

        configEditDateUI();

        expenseManagerAmountTextInputEditTextMd.setText(POSUtil.doubleToString(expenseManagerVOList.get(editPosition).getAmount()));
        expenseManagerRemarkTextInputEditTextMd.setText(expenseManagerVOList.get(editPosition).getRemark());

        buildEditDatePickerDialog();
    }


    public void setDataRemarkDialog() {
        viewRemark.setText(expenseManagerVOList.get(viewRemarkPosition).getRemark());
    }

    public void getExpenseValuesFromView() {
        incomeOrExpenseName = expenseManagerNameTextInputEditTextMd.getText().toString().trim();
        expenseOrIncomeAmount = Double.parseDouble(expenseManagerAmountTextInputEditTextMd.getText().toString().trim());
        expenseOrIncomeRemark = expenseManagerRemarkTextInputEditTextMd.getText().toString().trim();
    }


    public void refreshExpenseManagerList() {
        expenseManagerVOList = expenseManagerBusiness.getAllIncomeExpenses(0, 10, startDate, endDate, type);
        refreshList();
    }

    public void configRecyclerView() {
        loadExpeseIncomeHistory(0, 10);

        rvSwipeAdapterForExpenseManagerList = new RVSwipeAdapterForExpenseManagerList(expenseManagerVOList);
        expenseManagerListRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        expenseManagerListRecyclerView.setAdapter(rvSwipeAdapterForExpenseManagerList);

    }


    private void loadUIFromToolbar() {
        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        searchView.setCursorDrawable(attributeResourceId);
    }

    public void loadUI() {
        loadUIFromToolbar();

        floatingActionMenu = (FloatingActionMenu) mainLayout.findViewById(R.id.floatingActionMenu);
        noTransactionTextView = (TextView) mainLayout.findViewById(R.id.no_transaction);
        //        searchedResultTxt                     = (TextView) mainLayout.findViewById(R.id.searched_result_txt);
        typeFilterTextView = (TextView) mainLayout.findViewById(R.id.type_filter);
        filterTextView = (TextView) mainLayout.findViewById(R.id.filter_one);
        expenseManagerListRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.expenseManager_list_rv);
        addNewExpenseFab = (FloatingActionButton) mainLayout.findViewById(R.id.expense);
        addDateExpenseTextView = (TextView) addNewExpenseManagerMaterialDialog.findViewById(R.id.expense_date_in_add_expense_TV);
        saveExpenseBtnMd = (Button) addNewExpenseManagerMaterialDialog.findViewById(R.id.save);
        cancelExpenseBtnMd = (Button) addNewExpenseManagerMaterialDialog.findViewById(R.id.cancel);
        dateLLMd = (LinearLayout) addNewExpenseManagerMaterialDialog.findViewById(R.id.sale_date_in_new_sale_ll);
        expenseManagerNameTextInputEditTextMd = (AutoCompleteTextView) addNewExpenseManagerMaterialDialog.findViewById(R.id.expense_name_in_add_expense_TIET);
        expenseManagerAmountTextInputEditTextMd = (EditText) addNewExpenseManagerMaterialDialog.findViewById(R.id.expense_amount_in_add_expense_TIET);
        expenseManagerRemarkTextInputEditTextMd = (EditText) addNewExpenseManagerMaterialDialog.findViewById(R.id.expense_remark_in_add_expense_TIET);
        addNewIncomeFab = (FloatingActionButton) mainLayout.findViewById(R.id.income);
        saveIncomeBtnMd = (Button) addNewIncomeManagerMaterialDialog.findViewById(R.id.save);
        cancelIncomeBtnMd = (Button) addNewIncomeManagerMaterialDialog.findViewById(R.id.cancel);
        incomeManagerNameTextInputEditTextMd = (AutoCompleteTextView) addNewIncomeManagerMaterialDialog.findViewById(R.id.expense_name_in_add_expense_TIET);
        incomeManagerAmountTextInputEditTextMd = (EditText) addNewIncomeManagerMaterialDialog.findViewById(R.id.expense_amount_in_add_expense_TIET);
        incomeManagerRemarkTextInputEditTextMd = (EditText) addNewIncomeManagerMaterialDialog.findViewById(R.id.expense_remark_in_add_expense_TIET);
        dateLLMd_income = (LinearLayout) addNewIncomeManagerMaterialDialog.findViewById(R.id.sale_date_in_new_sale_ll);
        addDateIncomeTextView = (TextView) addNewIncomeManagerMaterialDialog.findViewById(R.id.expense_date_in_add_expense_TV);
        viewRemark = (TextView) viewRemarkExpenseManagerMaterialDialog.findViewById(R.id.viewRemark);
    }


    private void configDateUI() {
        date = DateUtility.makeDate(Integer.toString(years), Integer.toString(month), Integer.toString(day));

        addDateExpenseTextView.setText(DateUtility.makeDateFormatWithSlash(date));
        addDateIncomeTextView.setText(DateUtility.makeDateFormatWithSlash(date));
    }

    private void configEditDateUI() {
        editDate = DateUtility.makeDate(Integer.toString(editYear), Integer.toString(editMonth), Integer.toString(editDay));

        addDateExpenseTextView.setText(DateUtility.makeDateFormatWithSlash(editDate));
        addDateIncomeTextView.setText(DateUtility.makeDateFormatWithSlash(editDate));

        // addDateExpenseTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));
        //  addDateIncomeTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));
    }

    public void buildAddNewIncomeManagerDialog() {
        String income = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_new_income}).getString(0);
        addNewIncomeManagerMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.new_expense, true)
                .title(income).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //  .positiveText(save.getString(0))
                //  .negativeText(cancel.getString(0))
                .build();

    }

    private void resetDialogDataIncome() {
        day = now.get(Calendar.DAY_OF_MONTH);
        month = now.get(Calendar.MONTH) + 1;
        years = now.get(Calendar.YEAR);

        buildDatePickerDialog();
        configDateUI();

        incomeManagerNameTextInputEditTextMd.setText(null);
        incomeManagerNameTextInputEditTextMd.setError(null);
        incomeManagerAmountTextInputEditTextMd.setText(null);
        incomeManagerAmountTextInputEditTextMd.setError(null);
        incomeManagerRemarkTextInputEditTextMd.setText(null);
        incomeManagerRemarkTextInputEditTextMd.setError(null);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        searchView.setMenuItem(item);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }


    public void buildingCustomRangeDialog() {

        TypedArray dateRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});
        //TypedArray ok = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ok});

        buildingCustomRangeDatePickerDialog();
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(dateRange.getString(0))
                .customView(R.layout.custome_range, true).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                // .negativeText(cancel.getString(0))
                //.positiveText(ok.getString(0))
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));

        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    public void buildingCustomRangeDatePickerDialog() {

        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)

                , now.get(Calendar.MONTH)

                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });


        if (darkmode)
            startDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        startDatePickerDialog.setThemeDark(darkmode);
    }


}
