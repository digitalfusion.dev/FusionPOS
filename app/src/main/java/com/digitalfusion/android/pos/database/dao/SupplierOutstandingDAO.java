package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import com.digitalfusion.android.pos.database.model.OutstandingPayment;
import com.digitalfusion.android.pos.database.model.OutstandingPaymentDetail;
import com.digitalfusion.android.pos.database.model.PurchaseHistory;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hninhsuhantharkyaw on 10/24/16.
 */

public class SupplierOutstandingDAO extends ParentDAO {
    private static ParentDAO supplierOutstandingDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private List<PurchaseHistory> supplierOutstandingList;
    private PurchaseHistory supplierOutstanding;
    private OutstandingPaymentDetail supplierOutstandingPaymentDetail;
    private OutstandingPayment supplierOutstandingPayment;

    private SupplierOutstandingDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
    }

    public static SupplierOutstandingDAO getSupplierOutstandingDaoInstance(Context context) {
        if (supplierOutstandingDaoInstance == null) {
            supplierOutstandingDaoInstance = new SupplierOutstandingDAO(context);
        }
        return (SupplierOutstandingDAO) supplierOutstandingDaoInstance;
    }

    public List<PurchaseHistory> getSupplierOutstandingList(int startLimit, int endLimit, String startDate, String endDate) {
        supplierOutstandingList = new ArrayList<>();
        query = "select s." + AppConstant.PURCHASE_ID + ", " + AppConstant.PURCHASE_VOUCHER_NUM + ", " + AppConstant.PURCHASE_DAY + ", " + AppConstant.PURCHASE_MONTH + ", " +
                AppConstant.PURCHASE_YEAR + ", " + AppConstant.PURCHASE_DATE + ", s." + AppConstant.PURCHASE_BALANCE + " - ifnull((select sum(" + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID +
                " ), 0.0) as bal," + AppConstant.SUPPLIER_NAME + ", " + AppConstant.PURCHASE_SUPPLIER_ID + ", s." + AppConstant.PURCHASE_PAID_AMOUNT +
                " from " + AppConstant.PURCHASE_TABLE_NAME + " s, " + AppConstant.SUPPLIER_TABLE_NAME + " p where p." +
                AppConstant.SUPPLIER_ID + " = " + AppConstant.PURCHASE_SUPPLIER_ID + " and bal > 0 and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? order by " +
                AppConstant.PURCHASE_TIME + " desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    supplierOutstanding = new PurchaseHistory();
                    supplierOutstanding.setId(cursor.getLong(0));
                    supplierOutstanding.setVoucherNo(cursor.getString(1));
                    supplierOutstanding.setDay(cursor.getString(2));
                    supplierOutstanding.setMonth(cursor.getString(3));
                    supplierOutstanding.setYear(cursor.getString(4));
                    supplierOutstanding.setDate(cursor.getString(5));
                    supplierOutstanding.setBalance(cursor.getDouble(6));
                    supplierOutstanding.setSupplierName(cursor.getString(7));
                    supplierOutstanding.setSupplierID(cursor.getLong(8));
                    supplierOutstanding.setPaidAmt(cursor.getDouble(9));
                    supplierOutstandingList.add(supplierOutstanding);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return supplierOutstandingList;
    }

    public List<PurchaseHistory> getSupplierOutstandingListOnSearch(int startLimit, int endLimit, String startDate, String endDate, String searchStr) {
        supplierOutstandingList = new ArrayList<>();
        query = "select s." + AppConstant.PURCHASE_ID + ", " + AppConstant.PURCHASE_VOUCHER_NUM + ", " + AppConstant.PURCHASE_DAY + ", " + AppConstant.PURCHASE_MONTH + ", " +
                AppConstant.PURCHASE_YEAR + ", " + AppConstant.PURCHASE_DATE + ", s." + AppConstant.PURCHASE_BALANCE + " - ifnull((select sum(" + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID +
                " ), 0.0) as bal," + AppConstant.SUPPLIER_NAME + ", " + AppConstant.PURCHASE_SUPPLIER_ID + ", s." + AppConstant.PURCHASE_PAID_AMOUNT +
                " from " + AppConstant.PURCHASE_TABLE_NAME + " s, " + AppConstant.SUPPLIER_TABLE_NAME + " p where p." +
                AppConstant.SUPPLIER_ID + " = " + AppConstant.PURCHASE_SUPPLIER_ID + " and bal > 0 and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? and (" +
                AppConstant.PURCHASE_VOUCHER_NUM + " like ? || '%' or " + AppConstant.SUPPLIER_NAME + " like ? || '%') order by " + AppConstant.PURCHASE_TIME + " desc limit " +
                startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {

            cursor = database.rawQuery(query, new String[]{startDate, endDate, searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    supplierOutstanding = new PurchaseHistory();
                    supplierOutstanding.setId(cursor.getLong(0));
                    supplierOutstanding.setVoucherNo(cursor.getString(1));
                    supplierOutstanding.setDay(cursor.getString(2));
                    supplierOutstanding.setMonth(cursor.getString(3));
                    supplierOutstanding.setYear(cursor.getString(4));
                    supplierOutstanding.setDate(cursor.getString(5));
                    supplierOutstanding.setBalance(cursor.getDouble(6));
                    supplierOutstanding.setSupplierName(cursor.getString(7));
                    supplierOutstanding.setSupplierID(cursor.getLong(8));
                    supplierOutstanding.setPaidAmt(cursor.getDouble(9));
                    supplierOutstandingList.add(supplierOutstanding);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return supplierOutstandingList;
    }

    public List<PurchaseHistory> getSupplierOutstandingListBySupplierId(int startLimit, int endLimit, String startDate, String endDate, Long supplierId) {
        supplierOutstandingList = new ArrayList<>();
        query = "select s." + AppConstant.PURCHASE_ID + ", " + AppConstant.PURCHASE_VOUCHER_NUM + ", " + AppConstant.PURCHASE_DAY + ", " + AppConstant.PURCHASE_MONTH + ", " +
                AppConstant.PURCHASE_YEAR + ", " + AppConstant.PURCHASE_DATE + ", s." + AppConstant.PURCHASE_BALANCE + " - ifnull((select sum(" + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID +
                " ), 0.0) as bal," + AppConstant.SUPPLIER_NAME + ", " + AppConstant.PURCHASE_SUPPLIER_ID + ", s." + AppConstant.PURCHASE_PAID_AMOUNT +
                " from " + AppConstant.PURCHASE_TABLE_NAME + " s, " + AppConstant.SUPPLIER_TABLE_NAME + " p where p." +
                AppConstant.SUPPLIER_ID + " = " + AppConstant.PURCHASE_SUPPLIER_ID + " and bal > 0 and p." + AppConstant.PURCHASE_SUPPLIER_ID + " = " + supplierId + AppConstant.PURCHASE_DATE +
                " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? order by " + AppConstant.PURCHASE_TIME + " desc limit " +
                startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    supplierOutstanding = new PurchaseHistory();
                    supplierOutstanding.setId(cursor.getLong(0));
                    supplierOutstanding.setVoucherNo(cursor.getString(1));
                    supplierOutstanding.setDay(cursor.getString(2));
                    supplierOutstanding.setMonth(cursor.getString(3));
                    supplierOutstanding.setYear(cursor.getString(4));
                    supplierOutstanding.setDate(cursor.getString(5));
                    supplierOutstanding.setBalance(cursor.getDouble(6));
                    supplierOutstanding.setSupplierName(cursor.getString(7));
                    supplierOutstanding.setSupplierID(cursor.getLong(8));
                    supplierOutstanding.setPaidAmt(cursor.getDouble(9));
                    supplierOutstandingList.add(supplierOutstanding);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return supplierOutstandingList;
    }

    public boolean addSupplierOutstandingPayment(String invoiceNo, Long supplierID, Long salesID, Double paidAmt, String day, String month, String year, String date) {
        genID = idGeneratorDAO.getLastIdValue("SupplierOutstanding");
        genID++;
        query = "insert into " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + "(" + AppConstant.SUPPLIER_OUTSTANDING_ID +
                ", " + AppConstant.SUPPLIER_OUTSTANDING_INVOICE_NUM + ", " + AppConstant.SUPPLIER_OUTSTANDING_SUPPLIER_ID +
                ", " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + ", " + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                ", " + AppConstant.CREATED_DATE + ", " + AppConstant.SUPPLIER_OUTSTANDING_DAY + ", " + AppConstant.SUPPLIER_OUTSTANDING_MONTH + ", " +
                AppConstant.SUPPLIER_OUTSTANDING_YEAR + ", " + AppConstant.SUPPLIER_OUTSTANDING_DATE + ", " + AppConstant.SUPPLIER_OUTSTANDING_TIME +
                ") values (" + genID + ", ?, " + supplierID + ", " + salesID + ", " + paidAmt + ", ?, ?, ?, ?, ?, strftime('%s', ?, time('now', 'localtime')))";
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{invoiceNo, DateUtility.getTodayDate(), day, month, year, date, formatDateWithDash(day, month, year)});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean deleteSupplierOutstandingPayment(Long id) {
        databaseWriteTransaction(flag);
        try {
            database.delete(AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME, AppConstant.SUPPLIER_OUTSTANDING_ID + " = ?", new String[]{id.toString()});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }


    public OutstandingPaymentDetail getSupplierOutstandingPaymentsByPurchaseID(Long purchaseID) {
        supplierOutstandingPaymentDetail = new OutstandingPaymentDetail();
        query = "select c." + AppConstant.SUPPLIER_OUTSTANDING_ID + ", " + AppConstant.SUPPLIER_OUTSTANDING_INVOICE_NUM + ", c." + AppConstant.SUPPLIER_OUTSTANDING_SUPPLIER_ID + ", s." +
                AppConstant.PURCHASE_ID + ", " + "c." + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT + ", " + AppConstant.PURCHASE_VOUCHER_NUM + ", " + "s." + AppConstant.PURCHASE_BALANCE
                + " - ifnull((select sum(" + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = " + purchaseID +
                " ), 0.0) as bal," + AppConstant.SUPPLIER_NAME + ", c." + AppConstant.SUPPLIER_OUTSTANDING_DAY + ", c." + AppConstant.SUPPLIER_OUTSTANDING_MONTH + ", c." + AppConstant.SUPPLIER_OUTSTANDING_YEAR + ", c." +
                AppConstant.SUPPLIER_OUTSTANDING_DATE + ", " + AppConstant.PURCHASE_TOTAL_AMOUNT + ", s." + AppConstant.PICKUP_DAY + ", s." + AppConstant.PURCHASE_MONTH + ", s." + AppConstant.PURCHASE_YEAR +
                ", s." + AppConstant.PURCHASE_DATE + " from " + AppConstant.PURCHASE_TABLE_NAME + " s left outer join " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " c on s." +
                AppConstant.PURCHASE_ID + " = " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + ", " + AppConstant.SUPPLIER_TABLE_NAME +
                " cm where cm." + AppConstant.SUPPLIER_ID + " = s." + AppConstant.PURCHASE_SUPPLIER_ID + " and s." + AppConstant.SUPPLIER_ID + " = " + purchaseID + " order by c." + AppConstant.SUPPLIER_OUTSTANDING_TIME + " desc;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                supplierOutstandingPaymentDetail.setCustomerSupplierID(cursor.getLong(2));
                supplierOutstandingPaymentDetail.setSalePurchaseID(cursor.getLong(3));
                supplierOutstandingPaymentDetail.setSalePurchaseVoucherNo(cursor.getString(5));
                supplierOutstandingPaymentDetail.setOutstandingBalance(cursor.getDouble(6));
                supplierOutstandingPaymentDetail.setCustomerSupplierName(cursor.getString(7));
                supplierOutstandingPaymentDetail.setSalePurchaseTotalAmount(cursor.getDouble(12));
                supplierOutstandingPaymentDetail.setSalePurchaseDay(cursor.getString(13));
                supplierOutstandingPaymentDetail.setSalePurchaseMonth(cursor.getString(14));
                supplierOutstandingPaymentDetail.setSalePurchaseYear(cursor.getString(15));
                supplierOutstandingPaymentDetail.setSalePurchaseDate(cursor.getString(16));
                do {
                    supplierOutstandingPayment = new OutstandingPayment();
                    supplierOutstandingPayment.setId(cursor.getLong(0));
                    supplierOutstandingPayment.setInvoiceNo(cursor.getString(1));
                    supplierOutstandingPayment.setPaidAmt(cursor.getDouble(4));
                    supplierOutstandingPayment.setDay(cursor.getString(8));
                    supplierOutstandingPayment.setMonth(cursor.getString(9));
                    supplierOutstandingPayment.setYear(cursor.getString(10));
                    supplierOutstandingPayment.setDate(cursor.getString(11));
                    if (!supplierOutstandingPayment.isNull()) {
                        supplierOutstandingPaymentDetail.getOutstandingPaymentList().add(supplierOutstandingPayment);

                    }
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return supplierOutstandingPaymentDetail;
    }

    public boolean updateSupplierOutstandingPayment(Long purchaseID, Long supplierID, Double paidAmt, Long id, String day, String month, String year) {
        query = "update " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " set " + AppConstant.SUPPLIER_OUTSTANDING_SUPPLIER_ID + " = " + supplierID + ", " +
                AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = " + purchaseID + ", " + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                " = " + paidAmt + " , " + AppConstant.SUPPLIER_OUTSTANDING_DAY + "=? ," + AppConstant.SUPPLIER_OUTSTANDING_MONTH + "=? ," + AppConstant.SUPPLIER_OUTSTANDING_YEAR + "=?" + " where " + AppConstant.SUPPLIER_OUTSTANDING_ID + " = " + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{day, month, year});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public Double getPaidAmtByPurchaseID(Long purchaseID) {
        Double amt = 0.0;
        databaseWriteTransaction(flag);
        try {

            query = "select s." + AppConstant.PURCHASE_PAID_AMOUNT + " + ifnull(sum(c." + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT + "), 0.0) total_paid from " + AppConstant.PURCHASE_TABLE_NAME +
                    " s left outer join " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " c on " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID + " where " + "s." + AppConstant.PURCHASE_ID + " = " +
                    purchaseID;
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                amt = cursor.getDouble(0);
            }
        } catch (SQLiteException ex) {
            ex.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return amt;
    }

    public Double getTotalPayableAmt(String startDate, String endDate) {
        Double amt = 0.0;
        databaseWriteTransaction(flag);
        try {
            query = "select sum(bal) from (" +
                    "select s." + AppConstant.PURCHASE_BALANCE + " - ifnull((select sum(" + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                    ") from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID +
                    " ), 0.0) as bal from " + AppConstant.PURCHASE_TABLE_NAME + " s, " + AppConstant.SUPPLIER_TABLE_NAME + " p where p." +
                    AppConstant.SUPPLIER_ID + " = " + AppConstant.PURCHASE_SUPPLIER_ID + " and bal > 0 and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? ) ";
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                amt = cursor.getDouble(0);
            }
        } catch (SQLiteException ex) {
            ex.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return amt;
    }
}
