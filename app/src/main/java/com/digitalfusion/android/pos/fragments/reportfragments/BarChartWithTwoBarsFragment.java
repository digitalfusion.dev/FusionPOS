package com.digitalfusion.android.pos.fragments.reportfragments;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForMonthTwoAmtReport;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.model.ExpenseIncome;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.DefaultYAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class BarChartWithTwoBarsFragment extends Fragment implements Serializable {
    public static int[] colorArr = new int[]{
            ColorTemplate.rgb("#43A047"),
            ColorTemplate.rgb("#F44336")};
    protected String[] mMonths = new String[]{
            " Jan", " Feb", " Mar", " Apr", " May", " Jun", " Jul", " Aug", " Sep", " Oct", " Nov", " Dec"
    };
    private String reportName;
    private View mainLayoutView;
    private Context context;
    private TextView dateFilterTextView;
    private RVAdapterForFilter rvAdapterForDateFilter;
    private MaterialDialog dateFilterDialog;
    private List<String> filterList;
    private RecyclerView recyclerView;
    private ParentRVAdapterForReports rvAdapterForReport;
    private BarChart barChart;
    private ReportManager reportManager;
    private List<ReportItem> incomeReportItemList;
    private List<ReportItem> expenseReportItemList;
    //private ReportItem reportItemView;
    private List<Double> yValList;
    private String startDate;
    private String endDate;
    private int startLimit;
    private int endLimit;
    private ArrayList<String> xValues;
    private List<BarEntry> yValues1;
    private List<BarEntry> yValues2;
    private String thisYear;
    private String lastYear;
    private String last12Month;
    private String orderby;
    private boolean isTwelve;
    private String[] dateFilterArr;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_bar_chart_with_two_bars, container, false);
        if (getArguments() != null) {
            reportName = getArguments().getString("reportType");
        }
        if (reportName.equalsIgnoreCase("comparison of income and expense")) {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.comparison_of_income_expense}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        }

        loadIngUI();

        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        initializeVariables();


        configFilter();

        buildDateFilterDialog();

        setDateFilterTextView(thisYear);

        clickListeners();


        new LoadProgressDialog().execute("");

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void clickListeners() {
        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                dateFilterDialog.dismiss();


                if (filterList.get(position).equalsIgnoreCase(thisYear)) {
                    isTwelve = false;
                    orderby = "asc";
                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    int year = Calendar.getInstance().get(Calendar.YEAR);
                    for (int i = 0; i < 12; i++) {
                        dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
                    }


                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {
                    isTwelve = false;
                    orderby = "asc";

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    int year = Calendar.getInstance().get(Calendar.YEAR) - 1;
                    dateFilterArr = new String[12];
                    for (int i = 0; i < 12; i++) {
                        dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
                    }
                } else if (filterList.get(position).equalsIgnoreCase(last12Month)) {
                    orderby = "asc";
                    isTwelve = true;

                    startDate = DateUtility.getLast12MonthStartDate();

                    endDate = DateUtility.getLast12MotnEndDate();

                    Log.e("1 ", startDate + "  " + endDate);

                    dateFilterArr = new String[12];
                    int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
                    int year  = Calendar.getInstance().get(Calendar.YEAR);
                    for (int i = 0; i < 12; i++) {
                        if (month < 1) {
                            year -= 1;
                            month = 12;
                        }
                        dateFilterArr[i] = "(" + (month) + "," + year + ")";
                        month -= 1;
                    }

                }

                new LoadProgressDialog().execute("");

                setDateFilterTextView(filterList.get(position));
            }
        });
    }

    private void configFilter() {
        TypedArray mthisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year});

        TypedArray mlastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year});

        TypedArray mlastMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_12_month});

        thisYear = mthisYear.getString(0);

        lastYear = mlastYear.getString(0);


        last12Month = mlastMonth.getString(0);


        filterList = new ArrayList<>();

        filterList.add(thisYear);

        filterList.add(lastYear);

        filterList.add(last12Month);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    private void loadIngUI() {

        dateFilterTextView = (TextView) mainLayoutView.findViewById(R.id.date_filter);

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.bar_chart_recycler_view);

        barChart = (BarChart) mainLayoutView.findViewById(R.id.bar_chart);

    }

    private void initializeVariables() {

        incomeReportItemList = new ArrayList<>();

        expenseReportItemList = new ArrayList<>();

        reportManager = new ReportManager(context);

        setDatesAndLimits();

    }

    private void setDateFilterTextView(String msg) {

        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {

        String date = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);

        dateFilterDialog = new MaterialDialog.Builder(context).

                title(date)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))

                .build();

    }


    private void initializeList() {
        Log.e(startDate, endDate);
        incomeReportItemList = reportManager.comparisonOfIncomeExpense(startDate, endDate, dateFilterArr, orderby, ExpenseIncome.Type.Income.toString());
        expenseReportItemList = reportManager.comparisonOfIncomeExpense(startDate, endDate, dateFilterArr, orderby, ExpenseIncome.Type.Expense.toString());

        /*if (reportName.equalsIgnoreCase("top sale by qty")){
            incomeReportItemList = reportManager.topSalesByQty(startDate, endDate, startLimit, endLimit);
        } else if (reportName.equalsIgnoreCase("bottom sale items")){
            incomeReportItemList = reportManager.bottomSalesByQty(startDate, endDate, startLimit, endLimit);
        } else if (reportName.equalsIgnoreCase("top sales by customer")) {
            incomeReportItemList = reportManager.topSalesCustomers(startDate, endDate, startLimit, endLimit);
        } else if (reportName.equalsIgnoreCase("top sale by products")) {
            incomeReportItemList = reportManager.topSalesByProducts(startDate, endDate, startLimit, endLimit);
        } else if (reportName.equalsIgnoreCase("top outstanding suppliers")) {
            incomeReportItemList = reportManager.topOutstandingSupplier(startLimit, endLimit);
        } else if (reportName.equalsIgnoreCase("bottom sales by products")){
            incomeReportItemList = reportManager.bottomSalesByProducts(startDate, endDate, startLimit, endLimit);
        } else if (reportName.equalsIgnoreCase("top suppliers")) {
            incomeReportItemList = reportManager.topSuppliers(startDate, endDate, startLimit, endLimit);
        }*/

        //Log.e("size",incomeReportItemList.size()+" nsss");
    }

    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        if (reportName.equalsIgnoreCase("comparison of income and expense")) {
            rvAdapterForReport = new RVAdapterForMonthTwoAmtReport(incomeReportItemList, expenseReportItemList);
            recyclerView.setAdapter(rvAdapterForReport);
        }
    }

    private void setDatesAndLimits() {
        startDate = Calendar.getInstance().get(Calendar.YEAR) + "0101";
        endDate = Calendar.getInstance().get(Calendar.YEAR) + "1231";
        startLimit = 0;
        endLimit = 10;
        orderby = "asc";
        isTwelve = false;
        dateFilterArr = new String[12];
        int year = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 0; i < 12; i++) {
            dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
        }
    }

    public void settingBarChart() {

        // barChart.setDescriptionPosition(Float.parseFloat(XAxis.XAxisPosition.TOP.toString()),Float.parseFloat(YAxis.AxisDependency.RIGHT.toString()));
        barChart.setDrawBorders(false);
        barChart.setPinchZoom(true);
        barChart.setPivotY(10);
        barChart.setDrawBarShadow(false);
        barChart.setMaxVisibleValueCount(30);
        barChart.setDescription("");
        barChart.setDrawGridBackground(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setTouchEnabled(false);
        barChart.animateXY(1400, 1400, Easing.EasingOption.EaseInBack, Easing.EasingOption.EaseInBack);
        barChart.getLegend().setEnabled(true);
        Legend l = barChart.getLegend();
        if (POSUtil.isLabelWhite(context)) {
            barChart.getAxisLeft().setTextColor(Color.WHITE);
            barChart.getXAxis().setTextColor(Color.WHITE);
            l.setTextColor(Color.WHITE);
        }
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setYOffset(2f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f);
        l.setXOffset(3f);

        configXAxis();

        configYAxis();

        setBarDataToChart();

        barChart.invalidate();
    }

    private void setBarDataToChart() {
        xValues = new ArrayList<>();
        yValList = new ArrayList<>();

        xValues = new ArrayList<String>();
        if (isTwelve) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -11);
            int cal = calendar.get(Calendar.MONTH);
            for (int i = 0; i < 12; i++) {
                xValues.add(mMonths[cal]);
                cal += 1;
                if (cal > 11) {
                    cal = 0;
                }

            }
        } else {
            for (int i = 0; i < 12; i++) {
                //Log.e("ccccc",cal+"");
                xValues.add(mMonths[i]);
            }
        }
        /*if (reportName.equalsIgnoreCase("top sale by qty") || reportName.equalsIgnoreCase("bottom sale items")) {
            for (ReportItem reportItemView : incomeReportItemList) {
                //Log.e("ccccc",cal+"");
                yValList.add((double) reportItemView.getQty());
            }
        }else if (reportName.equalsIgnoreCase("top sales by customer") ||
                reportName.equalsIgnoreCase("top sale by products") || reportName.equalsIgnoreCase("top suppliers") ||
                reportName.equalsIgnoreCase("bottom sales by products")){*/


        for (ReportItem reportItem : incomeReportItemList) {
            //Log.e("ccccc",cal+"");
            yValList.add(reportItem.getBalance());
        }
        //}


        Log.e("x " + xValues.size(), "y " + yValList.size());
        yValues1 = getYAxisBarEntries(yValList);

        yValList = new ArrayList<>();

        for (ReportItem reportItem : expenseReportItemList) {
            yValList.add(reportItem.getBalance());
        }

        yValues2 = getYAxisBarEntries(yValList);
        Log.e(expenseReportItemList.size() + " ", "esize");
        Log.e(yValues1.size() + " y", yValues2.size() + " ");
        BarData data = configBarData(yValues1, yValues2, xValues);

        barChart.setData(data);
        for (IBarDataSet set : barChart.getData().getDataSets())
            set.setDrawValues(!set.isDrawValuesEnabled());

    }

    private void configYAxis() {
        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setValueFormatter(new DefaultYAxisValueFormatter(1));
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(10f);
        leftAxis.setAxisMinValue(0);
        leftAxis.setLabelCount(5, true);
        barChart.getAxisRight().setEnabled(false);
    }

    private void configXAxis() {
        XAxis xl = barChart.getXAxis();

        xl.setLabelRotationAngle(270);
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setDrawGridLines(false);
        xl.setSpaceBetweenLabels(1);
        //xl.setDrawLabels(false);
    }

    /**
     * @param yValue is data want to add to bar entry.
     * @return bart entry.
     */
    @NonNull
    private List<BarEntry> getYAxisBarEntries(List<Double> yValue) {
        List<BarEntry> yVals1 = new ArrayList<BarEntry>();
        int            j      = 0;
        for (Double t : yValue) {
            Float f = Float.valueOf(t.toString());
            yVals1.add(new BarEntry(f, j));
            ++j;
        }
        return yVals1;
    }

    @NonNull
    private BarData configBarData(List<BarEntry> yVals1, List<BarEntry> yVals2, List<String> xVals) {
        String     income    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.income}).getString(0);
        BarDataSet set1      = new BarDataSet(yVals1, "Income");
        Typeface   mTypeface = Typeface.createFromAsset(context.getAssets(), "Zawgyi-One.ttf");
        set1.setValueTypeface(mTypeface);
        set1.setColor(colorArr[0]);
        String     expense = context.getTheme().obtainStyledAttributes(new int[]{R.attr.expense}).getString(0);
        BarDataSet set2    = new BarDataSet(yVals2, "Expense");
        set2.setColor(colorArr[1]);
        List<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        dataSets.add(set2);

        BarData data = new BarData(xVals, dataSets);

        // add space between the dataset groups in percent of bar-width
        data.setGroupSpace(80f);

        //mChart.setDescription(calendar.get(Calendar.YEAR) + " " + xValues.get(0) + " - " + xValues.get(11));

        return data;
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String    label         = "";
            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {

            settingBarChart();
            configRecyclerView();

            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
