package com.digitalfusion.android.pos.util;

import android.content.Context;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.digitalfusion.android.pos.R;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by MD002 on 1/23/18.
 */

public class ExternalFile {
    private static final String PUBLIC_KEY_BASE64_ENCODED = "SGVsbG8=";
    private static final String PRIVATE_KEY_BASE64_ENCODED = "Hello";

    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    private void decryptString(byte[] privateKeyBytes, File inputFile, File outputFile) throws BadPaddingException, IllegalBlockSizeException,
            NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, InvalidKeySpecException {

        PrivateKey privateKey = getPrivateKey(privateKeyBytes);

        Cipher cipher = getDecryptionCipher(privateKey);


        try {
            FileInputStream fileInputStream = new FileInputStream(inputFile);
            byte[]          inputBytes      = new byte[(int) inputFile.length()];
            fileInputStream.read(inputBytes);
            byte[] encryptedMessageBytes = Base64.decode(new String(inputBytes), RSAEncryption.BASE64_FLAG);
            byte[] finalBy               = cipher.doFinal(encryptedMessageBytes);

            Log.w("STRING", new String(finalBy) + " SS");

            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            fileOutputStream.write(finalBy);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private Cipher getDecryptionCipher(PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException,
            InvalidKeyException {
        Cipher cipher = Cipher.getInstance(RSAEncryption.RSA_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return cipher;
    }

    private PrivateKey getPrivateKey(byte[] privateKeyBytes) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeyFactory          keyFactory = KeyFactory.getInstance(RSAEncryption.RSA_ALGORITHM);
        PKCS8EncodedKeySpec keySpec    = new PKCS8EncodedKeySpec(privateKeyBytes);
        return keyFactory.generatePrivate(keySpec);
    }

    public void createFile(String date, Context context) {

        if (isExternalStorageWritable()) {
            String outFile     = "mega_mind.txt";
            String zipFileName = ".mega_mind.zip";
            File   bfFile      = new File(Environment.getExternalStorageDirectory().getPath() + "/" + outFile);
            if (!bfFile.exists()) {
                Log.e("file not exi", " d");
                try {
                    bfFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            //            Log.e("file_loc", bfFile.getAbsolutePath());
            //            Log.e("dkfjafdf", Environment.getExternalStorageDirectory().getPath() + " kdfjakdjf");

            //            Log.e("dir", bfFile.getAbsolutePath());

            FileOutputStream output;
            DateMapping      dateMapping = DateMapping.getInstance();
            String           key         = dateMapping.getKeys(date);
            //            Log.e("key " + key, dateMapping.getValue(key));
            try {
                output = new FileOutputStream(bfFile);
                output.write(key.getBytes());
                //Log.e("path",outFile);
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                ZipFile       zipFile    = new ZipFile(Environment.getExternalStorageDirectory().getPath() + "/" + zipFileName);
                ZipParameters parameters = new ZipParameters();
                parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
                parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

                //Set the encryption flag to true
                parameters.setEncryptFiles(true);

                //Set the encryption method to AES Zip Encryption
                parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
                parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
                parameters.setPassword(context.getString(R.string.title_name) + ";");

                zipFile.addFile(bfFile, parameters);


            } catch (ZipException e) {
                e.printStackTrace();
            }

            bfFile.delete();

        } else {
            Log.e("AH", "ah");
        }
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public String readFile(Context context) {
        String output = null;
        String inFile = Environment.getExternalStorageDirectory() + "/.mega_mind.zip";
        //        Log.e("ijn", inFile + " ");
        if (isExternalStorageReadable()) {
            //String destination = "folder/source/";
            String password = context.getString(R.string.title_name) + ";";
            try {
                ZipFile zipFile = new ZipFile(inFile);
                if (zipFile.isEncrypted()) {
                    //                    Log.e("en", "cryp");
                    zipFile.setPassword(password);
                }
                zipFile.extractAll(Environment.getExternalStorageDirectory().getPath());
                //                Log.e("zip", Environment.getExternalStorageDirectory().getPath());
            } catch (ZipException e) {
                e.printStackTrace();
            }
            inFile = inFile.replace(".zip", ".txt");
            File tempFile  = new File(inFile);
            File inputFile = new File(tempFile.getParentFile(), tempFile.getName().substring(1));
            //            Log.e("storage", "readable " + inputFile.getName());
            inFile = Environment.getExternalStorageDirectory().getPath() + "/" + inputFile.getName();
            StringBuilder text = new StringBuilder();

            try {
                BufferedReader br = new BufferedReader(new FileReader(inFile));
                String         line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    //                    text.append('\n');
                }
                DateMapping dateMapping = DateMapping.getInstance();
                output = dateMapping.getValue(text.toString());
                //                Log.e("resterte " + text.toString() , "success");
                br.close();
                tempFile.delete();
                inputFile.delete();
            } catch (IOException e) {
                e.printStackTrace();
                //You'll need to add proper error handling here
            } finally {
                return output;
            }

        }
        return output;
    }


}
