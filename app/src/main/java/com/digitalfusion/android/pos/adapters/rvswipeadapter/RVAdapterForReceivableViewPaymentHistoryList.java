package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.content.res.TypedArray;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.OutstandingPayment;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.Calendar;
import java.util.List;

/**
 * Created by MD001 on 10/26/16.
 */

public class RVAdapterForReceivableViewPaymentHistoryList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    public int currentDate;
    private List<OutstandingPayment> outstandingPaymentList;
    private ClickListener editClickListener;
    private ClickListener deleteClickListener;


    public RVAdapterForReceivableViewPaymentHistoryList(List<OutstandingPayment> outstandingPaymentList) {
        this.outstandingPaymentList = outstandingPaymentList;

        Calendar now = Calendar.getInstance();

        currentDate = Integer.parseInt(DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(now.get(Calendar.DAY_OF_MONTH))));

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.receivable_view_payment_item_view, parent, false);
        return new ReceivableViewPaymentViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof RVAdapterForReceivableViewPaymentHistoryList.ReceivableViewPaymentViewHolder) {

            final ReceivableViewPaymentViewHolder viewHolder = (ReceivableViewPaymentViewHolder) holder;

            POSUtil.makeZebraStrip(viewHolder.itemView, position);

            viewHolder.paymentID.setText(outstandingPaymentList.get(position).getInvoiceNo());

            viewHolder.paymentAmount.setText(POSUtil.NumberFormat(outstandingPaymentList.get(position).getPaidAmt()));

            //String dayDes[] = DateUtility.dayDes(outstandingPaymentList.get(position).getDate());

            //String yearMonthDes = DateUtility.monthYearDes(outstandingPaymentList.get(position).getDate());

            int date = Integer.parseInt(outstandingPaymentList.get(position).getDate());

            if (date == currentDate) {
                viewHolder.paymentDate.setText(viewHolder.today.getString(0));
            } else if (date == currentDate - 1) {
                viewHolder.paymentDate.setText(viewHolder.yesterday.getString(0));
            } else {
                //  viewHolder.dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

                viewHolder.paymentDate.setText(DateUtility.makeDateFormatWithSlash(outstandingPaymentList.get(position).getYear(),
                        outstandingPaymentList.get(position).getMonth(),
                        outstandingPaymentList.get(position).getDay()));
            }


            //  viewHolder.paymentDate.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

            viewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editClickListener != null) {

                        editClickListener.onClick(position);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                viewHolder.swipeLayout.close();
                            }
                        }, 500);

                    }

                }

            });

            viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (deleteClickListener != null) {

                        deleteClickListener.onClick(position);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                viewHolder.swipeLayout.close();
                            }
                        }, 500);

                    }

                }

            });

            if (position == 0) {
                viewHolder.swipeLayout.setSwipeEnabled(false);
            } else {
                viewHolder.swipeLayout.setSwipeEnabled(true);
            }

            mItemManger.bindView(viewHolder.view, position);
        }
    }

    @Override
    public int getItemCount() {
        return outstandingPaymentList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public List<OutstandingPayment> getOutstandingPaymentList() {
        return outstandingPaymentList;
    }

    public void setOutstandingPaymentList(List<OutstandingPayment> outstandingPaymentList) {
        this.outstandingPaymentList = outstandingPaymentList;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    public class ReceivableViewPaymentViewHolder extends RecyclerView.ViewHolder {

        TextView paymentID;

        TextView paymentAmount;

        TextView paymentDate;

        ImageButton editButton;

        ImageButton deleteButton;

        SwipeLayout swipeLayout;

        TypedArray today;
        TypedArray yesterday;
        View view;

        public ReceivableViewPaymentViewHolder(View itemView) {

            super(itemView);

            this.view = itemView;


            today = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.today});
            yesterday = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday});


            editButton = (ImageButton) itemView.findViewById(R.id.edit_payment_detail_item_history);

            deleteButton = (ImageButton) itemView.findViewById(R.id.delete_payment_detail_item_history);

            paymentID = (TextView) itemView.findViewById(R.id.receivable_item_view_payment_id);

            paymentAmount = (TextView) itemView.findViewById(R.id.receivable_item_view_payment_amount);

            paymentDate = (TextView) itemView.findViewById(R.id.receivable_item_view_payment_date);

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
        }
    }
}