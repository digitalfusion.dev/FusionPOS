package com.digitalfusion.android.pos.information;

import android.content.Context;

import com.digitalfusion.android.pos.database.business.ApiManager;

/**
 * Created by MD003 on 1/20/18.
 */

public class AppUpdateRequest {
    private String uid;
    private String appName;
    private String appVersion;


    public AppUpdateRequest(Context context) {
        AppInfo    appInfo    = new AppInfo();
        ApiManager apiManager = new ApiManager(context);
        this.uid = apiManager.getRegistration().getUserId();
        this.appName = appInfo.getAppName();
        this.appVersion = appInfo.getVersion();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}
