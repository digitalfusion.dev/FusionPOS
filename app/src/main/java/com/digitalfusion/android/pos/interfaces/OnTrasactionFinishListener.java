package com.digitalfusion.android.pos.interfaces;

/**
 * Created by lyx on 6/14/18.
 */
public interface OnTrasactionFinishListener {
    void onSaved();
}
