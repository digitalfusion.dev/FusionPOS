package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;

import java.util.List;

/**
 * Created by MD002 on 10/20/17.
 */

public class RVAdapterForProCatBySupProduct extends ParentRVAdapterForReports {

    private Context context;
    private List<ReportItem> reportItemList;

    public RVAdapterForProCatBySupProduct(Context context, List<ReportItem> reportItemList) {
        super();
        this.context = context;
        this.reportItemList = reportItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.stock_item_auto_complete_view, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.codeTextView.setText(reportItemList.get(position).getName1());
        holder.nameTextView.setText(reportItemList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return reportItemList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView codeTextView;
        TextView nameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.nameTextView = (TextView) itemView.findViewById(R.id.name);
            this.codeTextView = (TextView) itemView.findViewById(R.id.code);
        }
    }

}