package com.digitalfusion.android.pos.database.business;

import android.content.Context;
import android.util.Log;

import com.digitalfusion.android.pos.database.dao.UnitDAO;
import com.digitalfusion.android.pos.database.model.Unit;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/18/16.
 */
public class UnitManager {
    private Context context;
    private UnitDAO unitDAO;
    private List<Unit> unitList;

    public UnitManager(Context context) {
        this.context = context;
        unitDAO = UnitDAO.getUnitDaoInstance(context);
        unitList = new ArrayList<>();
    }

    public boolean addNewUnit(String unit, String description) {
        InsertedBooleanHolder isInserted = new InsertedBooleanHolder();
        unitDAO.addNewUnit(unit, description, isInserted);
        return isInserted.isInserted();
    }

    public boolean deleteUnit(Long id) {
        return unitDAO.deleteUnit(id);
    }

    public Long addNewUnitFromStock(String unit, String description, InsertedBooleanHolder flag) {
        Long id = unitDAO.addNewUnit(unit, description, flag);
        Log.w("Unit y id in database", id + " heehe");
        return id;
    }

    public boolean checkUnitExists(String unit) {
        Long id = unitDAO.checkUnitAlreadyExist(unit);
        return id != null;
    }

    public Long checkUnitExistsLong(String unit) {
        Long id = 0l;
        id = unitDAO.checkUnitAlreadyExist(unit);
        if (id == null) {
            return null;
        } else {
            return id;
        }
        //return id;
    }

    public List<Unit> getAllUnits() {
        unitList = unitDAO.getAllUnits();
        return unitList;
    }

    public boolean updateUnit(String unit, String description, Long id) {
        return unitDAO.updateUnit(unit, description, id);
    }
}
