package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.model.BusinessSetting;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.FusionToast;

/**
 * Created by MD001 on 3/13/17.
 */

public class PrintVoucherSettingFragment extends Fragment {

    //voucher preview dialog
    MaterialDialog previewDialog;
    TextView previewHeaderTV;
    TextView previewFooterTV;
    private View mainLayoutView;
    private Context context;
    private EditText headerTxt;
    private EditText footerTxt;
    //TextView previewHeaderTxt;
    //TextView previewFooterTxt;
    private Button preview;
    private SettingManager settingManager;
    private BusinessSetting businessSetting;
    private RadioGroup rdRadioGroup;
    private RadioButton mm58;
    private RadioButton mm80;
    private RadioButton innerPrinter;
    private boolean isMM80;
    private boolean isMM58;
    private boolean isInternal;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.print_voucher_setting, null);

        context = getContext();

        loadUI();

        setHasOptionsMenu(true);

        isMM80 = POSUtil.is80MMPrinter(getContext());
        isMM58 = POSUtil.is58MMPrinter(getContext());
        isInternal = POSUtil.isInternalPrinter(getContext());

        rdRadioGroup = (RadioGroup) mainLayoutView.findViewById(R.id.rd_group);
        mm58 = (RadioButton) mainLayoutView.findViewById(R.id.mm_58);
        mm80 = (RadioButton) mainLayoutView.findViewById(R.id.mm_80);
        innerPrinter = mainLayoutView.findViewById(R.id.internal);
        settingManager = new SettingManager(mainLayoutView.getContext());

        businessSetting = settingManager.getBusinessSetting();

        TypedArray saleDetail = context.getTheme().obtainStyledAttributes(new int[]{R.attr.print_voucher_setting});

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(saleDetail.getString(0));

        loadOldValue();

        buildPreviewDialog();


        rdRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (mm58.getId() == checkedId) {
                    isMM80 = false;
                    isInternal = false;
                    isMM58 = true;
                } else if (innerPrinter.getId() == checkedId) {
                    isInternal = true;
                    isMM80 = false;
                    isMM58 = false;
                } else {
                    isInternal = false;
                    isMM80 = true;
                    isMM58 = false;
                }

            }
        });


        if (POSUtil.is80MMPrinter(context)) {
            mm80.setChecked(true);
        } else {
            mm80.setChecked(false);
        }
        if (POSUtil.is58MMPrinter(context)) {
            mm58.setChecked(true);
        } else {
            mm58.setChecked(false);
        }
        if (POSUtil.isInternalPrinter(context)) {
            innerPrinter.setChecked(true);
        } else {
            innerPrinter.setChecked(false);
        }
        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (headerTxt.getText().toString().trim().length() > 0) {
                    previewHeaderTV.setVisibility(View.VISIBLE);
                    previewHeaderTV.setText(headerTxt.getText());
                } else {
                    previewHeaderTV.setVisibility(View.GONE);
                }

                if (footerTxt.getText().toString().trim().length() > 0) {
                    previewFooterTV.setVisibility(View.VISIBLE);
                    previewFooterTV.setText(footerTxt.getText());
                } else {
                    previewFooterTV.setVisibility(View.GONE);
                }

                previewDialog.show();
            }
        });


        return mainLayoutView;

    }

    private void loadUI() {
        headerTxt = (EditText) mainLayoutView.findViewById(R.id.header_txt_voucher);
        footerTxt = (EditText) mainLayoutView.findViewById(R.id.footer_txt_voucher);
        // previewHeaderTxt = (TextView) mainLayoutView.findViewById(R.id.preview_header_txt_voucher);
        //  previewFooterTxt = (TextView) mainLayoutView.findViewById(R.id.preview_footer_txt_voucher);
        preview = (Button) mainLayoutView.findViewById(R.id.preview);
    }

    private void loadOldValue() {
        String temp_header = POSUtil.getHeaderTxt(context);
        String temp_footer = POSUtil.getFooterTxt(context);
        headerTxt.setText(temp_header);
        footerTxt.setText(temp_footer);
        /*if(temp_header.equalsIgnoreCase("")|| temp_header.equalsIgnoreCase(null)){
            String temp="";
            if(businessSetting.getBusinessName() != null && businessSetting.getBusinessName().length()>0){
                temp=businessSetting.getBusinessName();
            }
            if(businessSetting.getAddress() != null && businessSetting.getAddress().length()>0){
                if(temp.length()<=0)
                    temp=businessSetting.getAddress();
                else
                    temp+="\n"+businessSetting.getAddress();
            }
            if(businessSetting.getPhoneNo() != null && businessSetting.getPhoneNo().length()>0){
                if(temp.length()<=0)
                    temp=businessSetting.getPhoneNo();
                else
                    temp=temp+"\n"+businessSetting.getPhoneNo();
            }
            if (temp==null || temp== ""){
            }else{
                editPreferences(temp, sharedPref);
                headerTxt.setText(temp);
            }
        } else{
            headerTxt.setText(temp_header);
        }

        if(temp_footer.equalsIgnoreCase("-1")){
            footerTxt.setText("");
        }else{
            footerTxt.setText(temp_footer);
        }*/

    }

    private void buildPreviewDialog() {

        String preview = context.getTheme().obtainStyledAttributes(new int[]{R.attr.preview}).getString(0);
        previewDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.print_voucher_preview, true).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .title(preview)
                .build();

        previewHeaderTV = (TextView) previewDialog.findViewById(R.id.header_text);
        previewFooterTV = (TextView) previewDialog.findViewById(R.id.footer_text);

        previewDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewDialog.dismiss();
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save) {


            POSUtil.editPreferencesVoucher(headerTxt.getText().toString(), context, footerTxt.getText().toString());

            // String success= context.getTheme().obtainStyledAttributes(new int[]{R.attr.successfully_updated}).getString(0);

            if (isMM80) {
                POSUtil.setMM80Printer(getContext(), AppConstant._80MM);
            }
            if (isMM58) {
                POSUtil.setMM80Printer(getContext(), AppConstant._58MM);
            }
            if (isInternal) {
                POSUtil.setMM80Printer(getContext(), AppConstant._Internal);
            }


            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
        }

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void editPreferences(String headerText, SharedPreferences sharedPref) {
        POSUtil.editPreferencesVoucher(headerTxt.getText().toString(), context, footerTxt.getText().toString());
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);

      /*  TypedArray save=context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        FontTextView fontTextView= (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.menu_text,null);

        menu.getItem(0).setActionView(fontTextView);

        fontTextView.setText(save.getString(0));


        fontTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menu.getItem(0));
            }
        });*/


        super.onCreateOptionsMenu(menu, inflater);
    }
}
