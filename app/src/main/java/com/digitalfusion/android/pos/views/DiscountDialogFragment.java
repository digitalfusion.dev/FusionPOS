package com.digitalfusion.android.pos.views;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForDiscountMD;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.interfaces.ActionDoneListener;
import com.digitalfusion.android.pos.database.model.Discount;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

import belka.us.androidtoggleswitch.widgets.BaseToggleSwitch;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiscountDialogFragment extends DialogFragment {

    private RecyclerView recyclerView;
    private RVAdapterForDiscountMD rvAdapterForDiscountMD;
    private List<Discount> discountList;
    private SettingManager settingManager;
    private Context context;
    private Discount defaultDiscount;
    //    private String type;
    private TextView valueTextView;
    private Button saveButton;
    private Button amtToggleButton;
    private Button percentToggleButton;
    private TextView titleTextView;
    private Button cancelButton;
    private ToggleSwitch toggleSwitch;
    private boolean isPercent;

    public static DiscountDialogFragment newInstance(String title) {
        DiscountDialogFragment frag = new DiscountDialogFragment();
        Bundle                 args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        Log.e("title_disc", title);
        return frag;
    }

    public void setDefaultDiscount(Discount defaultDiscount) {
        this.defaultDiscount = defaultDiscount;
    }

    //    @Override
    //    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    //                             Bundle savedInstanceState) {
    //        // Inflate the layout for this fragment
    //        return inflater.inflate(R.layout.fragment_discount_selection_dialog, container, false);
    ////        type = getArguments().getString("title");
    ////        Log.e("tilte", type);
    //
    //    }


    //    @Override
    //    public Dialog onCreateDialog(final Bundle savedInstanceState) {
    //
    //        Dialog dialog =  super.onCreateDialog(savedInstanceState);
    //
    //        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    //
    //        int width=(int)(getResources().getDisplayMetrics().widthPixels*0.1);
    //
    //        dialog.getWindow().setLayout(width,width);
    //
    //        return dialog;
    //    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder builder  = new AlertDialog.Builder(getActivity());
        LayoutInflater      inflater = getActivity().getLayoutInflater();
        View                view     = inflater.inflate(R.layout.fragment_discount_selection_dialog, null);
    /* Anything that needs to be done to the view, ie.
    on click listeners, postivie/negative buttons,
    values being changed etc */

        String title = getArguments().getString("title");
        Log.e("dialog_title", title);
        //        getDialog().setTitle("");
        valueTextView = view.findViewById(R.id.value_text_view);
        saveButton = view.findViewById(R.id.button);
        recyclerView = view.findViewById(R.id.recycler_view);
        context = getContext();
        settingManager = new SettingManager(context);
        discountList = settingManager.getAllDiscounts();
        if (defaultDiscount == null)
            defaultDiscount = settingManager.getDefaultDiscount();
        amtToggleButton = view.findViewById(R.id.amount_toggle_button);
        percentToggleButton = view.findViewById(R.id.percent_toggle_button);
        titleTextView = view.findViewById(R.id.title_text_view);
        cancelButton = view.findViewById(R.id.cancel_btn);
        titleTextView.setText(title);
        toggleSwitch = view.findViewById(R.id.toggle_btn);

        Log.e("stre", AppConstant.CURRENCY);
        amtToggleButton.setText(AppConstant.CURRENCY);

        clickListeners();

        if (defaultDiscount == null) {
            defaultDiscount = new Discount();
            percentToggleButton.setActivated(false);
            amtToggleButton.setActivated(true);
        } else {
            valueTextView.setText(POSUtil.doubleToString(defaultDiscount.getAmount()));
            if (defaultDiscount.getIsPercent() == 1) {
                toggleSwitch.setCheckedTogglePosition(1);
            } else {
                toggleSwitch.setCheckedTogglePosition(0);
            }
        }

        rvAdapterForDiscountMD = new RVAdapterForDiscountMD(discountList, defaultDiscount.getId());

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        setListeners();

        builder.setView(view);
        Dialog dialog = builder.create();
        //        dialog.show();
        //        int width=(int)(getResources().getDisplayMetrics().widthPixels*0.9);
        //
        //        dialog.getWindow().setLayout(width,width);
        return dialog;
    }


    //    @NonNull
    //    @Override
    //    public Dialog onCreateDialog(Bundle savedInstanceState) {
    //        Dialog dialog = super.onCreateDialog(savedInstanceState);
    ////        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    ////        int width=(int)(getResources().getDisplayMetrics().widthPixels*0.3);
    ////        dialog.getWindow().setLayout(width,width);
    //        return dialog;
    //    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.e("on dialog", "created");


    }

    private void clickListeners() {
        percentToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                percentToggleButton.setActivated(true);
                amtToggleButton.setActivated(false);
            }
        });
        //        amtToggleButton.setSelected(true);
        amtToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                percentToggleButton.setActivated(false);
                amtToggleButton.setActivated(true);
            }
        });
    }

    private void setListeners() {

        toggleSwitch.setOnToggleSwitchChangeListener(new BaseToggleSwitch.OnToggleSwitchChangeListener() {
            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {

                if (toggleSwitch.getCheckedTogglePosition() == 0) {
                    isPercent = false;

                } else {
                    isPercent = true;

                }
            }
        });

        recyclerView.setAdapter(rvAdapterForDiscountMD);
        rvAdapterForDiscountMD.setmItemClickListener(new RVAdapterForDiscountMD.OnItemClickListener() {
            //            @Override
            //            public void onItemClick(View view, int position) {
            ////                dismiss();
            //            }

            @Override
            public void onItemClick() {
                //                defaultDiscount = discount;
                valueTextView.setText(null);
            }
        });

        valueTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    rvAdapterForDiscountMD.setNoneChecked();
                    //                    recyclerView.notifyAll();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    rvAdapterForDiscountMD.setDefaultChecked();
                    //                    recyclerView.notifyAll();
                }
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Discount discount;
                if (valueTextView.getText().length() != 0) {
                    discount = new Discount(null, null, Double.parseDouble(valueTextView.getText().toString()));
                    if (isPercent) {
                        discount.setIsPercent(1);
                    } else {
                        discount.setIsPercent(0);
                    }
                } else {
                    discount = rvAdapterForDiscountMD.getDiscount();
                    if (discount == null) {
                        discount = defaultDiscount;
                    }
                }

                //                Log.e("discount Vo", discount.getAmount() + "");
                //                Log.e("parent ", getParentFragment() + "");
                ActionDoneListener actionDoneListener = (ActionDoneListener) getTargetFragment();
                actionDoneListener.onActionDone(discount);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

}
