package com.digitalfusion.android.pos.views;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.AttributeSet;

import com.digitalfusion.android.pos.util.TypefaceSpan;
import com.innovattic.font.FontEditText;

/**
 * Created by MD003 on 4/8/17.
 */

public class IgnobleEditText extends FontEditText {
    public IgnobleEditText(Context context) {
        super(context);
    }

    public IgnobleEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IgnobleEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    public void setError(CharSequence error) {

        if (error != null) {
            SpannableString s = new SpannableString(error.toString());
            s.setSpan(new TypefaceSpan(getContext(), "Zawgyi-One.ttf"), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            super.setError(s);
        } else {
            super.setError(error);
        }

    }

}
