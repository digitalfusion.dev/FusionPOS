package com.digitalfusion.android.pos.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by MD003 on 8/17/16.
 */
public class DamageAndLostViewPagerAdapter extends FragmentStatePagerAdapter {

    List<Fragment> damageAndLostFragment;


    public DamageAndLostViewPagerAdapter(FragmentManager fm, List<Fragment> damageAndLostFragment) {
        super(fm);
        this.damageAndLostFragment = damageAndLostFragment;
    }

    @Override
    public Fragment getItem(int position) {
        return damageAndLostFragment.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "Damage";
        } else {
            return "Lost";
        }
    }

    @Override
    public int getCount() {
        return 1;
    }
}
