package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.database.dao.CustomerOutstandingDAO;
import com.digitalfusion.android.pos.database.model.OutstandingPaymentDetail;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InvoiceNoGenerator;

import java.util.List;

/**
 * Created by hninhsuhantharkyaw on 10/23/16.
 */

public class CustomerOutstandingManager {
    private Context context;
    private CustomerOutstandingDAO customerOutstandingDAO;
    private InvoiceNoGenerator invoiceNoGenerator;
    private boolean flag;

    public CustomerOutstandingManager(Context context) {
        this.context = context;
        customerOutstandingDAO = CustomerOutstandingDAO.getCustomerOutstandingDaoInstance(context);
        invoiceNoGenerator = new InvoiceNoGenerator(context);
    }

    public List<SalesHistory> getCustomerOutstandingList(int startLimit, int endLimit, String startDate, String endDate) {
        return customerOutstandingDAO.getCustomerOutstandingList(startLimit, endLimit, startDate, endDate);
    }


    public List<SalesHistory> getCustomerOutstandingListOnSearch(int startLimit, int endLimit, String startDate, String endDate, String searchStr) {
        return customerOutstandingDAO.getCustomerOutstandingListOnSearch(startLimit, endLimit, startDate, endDate, searchStr);
    }

    public List<SalesHistory> getCustomerOutstandingListByCustomer(int startLimit, int endLimit, String startDate, String endDate, Long customerId) {
        return customerOutstandingDAO.getCustomerOutstandingListByCustomer(startLimit, endLimit, startDate, endDate, customerId);
    }

    public boolean addCustomerOutstandingPayment(String invoiceNo, Long customerID, Long salesID, Double paidAmt, String day, String month, String year) {
        flag = customerOutstandingDAO.addCustomerOutstandingPayment(invoiceNo, customerID, salesID, paidAmt, day, month, year, DateUtility.makeDate(year, month, day));
        if (flag) {
            invoiceNoGenerator.updateInvoiceNextNum();
        }
        return flag;
    }

    public String getCustomerOutstandingPaymentInvoiceNo() {
        return invoiceNoGenerator.getInvoiceNo(AppConstant.CUSTOMER_OUTSTANDING_INVOICE_NO);
    }

    public OutstandingPaymentDetail getCustomerOutstandingPaymentsBySaleID(Long saleID) {
        return customerOutstandingDAO.getCustomerOutstandingPaymentsBySaleID(saleID);
    }

    public Double getTotalReceivableAmt(String startDate, String endDate) {

        return customerOutstandingDAO.getTotalReceivableAmt(startDate, endDate);
    }

    public Double getPaidAmtBySalesID(Long salesID) {

        return customerOutstandingDAO.getReceivablePaidAmtBySalesID(salesID);

    }

    public boolean deleteCustomerOutstandingPayment(Long id) {
        return customerOutstandingDAO.deleteCustomerOutstandingPayment(id);
    }


    public boolean updateCustomerOutstandingPayment(Long customerID, Long salesID, Double paidAmt, Long id, String day, String month, String year) {
        return customerOutstandingDAO.updateCustomerOutstandingPayment(customerID, salesID, paidAmt, id, day, month, year);
    }
}
