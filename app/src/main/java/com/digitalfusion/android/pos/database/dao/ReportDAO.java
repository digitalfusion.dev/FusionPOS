package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.model.ExpenseIncome;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 11/1/16.
 */

public class ReportDAO extends ParentDAO {
    public static ParentDAO reportDaoInstance;
    private Context context;
    private List<ReportItem> reportItemList;
    private ReportItem reportItem;

    private String db_users;

    private ReportDAO(Context context) {
        super(context);

        db_users = context.getDatabasePath(AppConstant.API_DATABASE).getPath();
    }

    public static ReportDAO getReportDaoInstance(Context context) {
        if (reportDaoInstance == null) {
            reportDaoInstance = new ReportDAO(context);
        }
        return (ReportDAO) reportDaoInstance;
    }

    public List<ReportItem> topSalesItems(String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select sum(" + AppConstant.SALES_DETAIL_QTY + ") as sum_qty, " + AppConstant.STOCK_NAME + " from " + AppConstant.SALES_DETAIL_TABLE_NAME + ", " + AppConstant.SALES_TABLE_NAME
                + " s, " + AppConstant.STOCK_TABLE_NAME + " st where " +
                AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE +
                " <= ? and " + AppConstant.SALES_DETAIL_STOCK_ID + " = st." + AppConstant.STOCK_ID + " group by " + AppConstant.SALES_DETAIL_STOCK_ID + " order by sum_qty desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setQty(cursor.getInt(0));
                    reportItem.setName(cursor.getString(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> topSales(String startDate, String endDate, int startLimit, int endLimit, String categoryID) {
        reportItemList = new ArrayList<>();
        query = "select sum(" + AppConstant.SALES_DETAIL_TOTAL + ") as sum_total, " + AppConstant.STOCK_NAME + " from " + AppConstant.SALES_DETAIL_TABLE_NAME + ", " + AppConstant.SALES_TABLE_NAME
                + " s, " + AppConstant.STOCK_TABLE_NAME + " st where " +
                AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE +
                " <= ? and " + AppConstant.SALES_DETAIL_STOCK_ID + " = st." + AppConstant.STOCK_ID + " and " + AppConstant.STOCK_CATEGORY_ID + categoryID + " group by " + AppConstant.SALES_DETAIL_STOCK_ID + " order by sum_total desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setBalance(cursor.getDouble(0));
                    reportItem.setName(cursor.getString(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> allSales(String startDate, String endDate, int startLimit, int endLimit, String categoryID) {
        reportItemList = new ArrayList<>();
        query = "select sum(" + AppConstant.SALES_DETAIL_TOTAL + ") as sum_total, " + AppConstant.STOCK_NAME + ", sum(" + AppConstant.SALES_DETAIL_QTY + ")" + " from " + AppConstant.SALES_DETAIL_TABLE_NAME + ", " + AppConstant.SALES_TABLE_NAME
                + " s, " + AppConstant.STOCK_TABLE_NAME + " st where " +
                AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE +
                " <= ? and " + AppConstant.SALES_DETAIL_STOCK_ID + " = st." + AppConstant.STOCK_ID + " and " + AppConstant.STOCK_CATEGORY_ID + " " + categoryID + " group by " + AppConstant.SALES_DETAIL_STOCK_ID + " order by sum_total desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});

            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setAmount(cursor.getDouble(0));
                    reportItem.setName(cursor.getString(1));
                    reportItem.setQty(cursor.getInt(2));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public double getTotalAmountOfTotalAmountOfAllSales(String startDate, String endDate, String categoryID) {
        String query = "select sum(total_sum.amt) from (" +
                "select sum(" + AppConstant.SALES_DETAIL_TOTAL + ")  as amt from " + AppConstant.SALES_DETAIL_TABLE_NAME + ", " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.STOCK_TABLE_NAME + " st " +
                "where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ? and " + AppConstant.SALES_DETAIL_STOCK_ID + " = st." + AppConstant.STOCK_ID + " and " + AppConstant.STOCK_CATEGORY_ID + " " + categoryID + " group by " + AppConstant.SALES_DETAIL_STOCK_ID +
                ") as total_sum";
        double totalAmt = 0;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});

            if (cursor.moveToFirst()) {
                do {
                    totalAmt = cursor.getDouble(0);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return totalAmt;
    }

    public List<ReportItem> allPurchases(String startDate, String endDate, int startLimit, int endLimit, String categoryID) {
        reportItemList = new ArrayList<>();
        query = "select sum(pdt." + AppConstant.PURCHASE_DETAIL_TOTAL + ") as sum_total, " + AppConstant.STOCK_NAME + ", sum(" + AppConstant.PURCHASE_DETAIL_QTY + ")" + " from " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " pdt, " + AppConstant.PURCHASE_TABLE_NAME
                + " s, " + AppConstant.STOCK_TABLE_NAME + " st where " +
                "pdt." + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE +
                " <= ? and pdt." + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = st." + AppConstant.STOCK_ID + " and " + AppConstant.STOCK_CATEGORY_ID + " " + categoryID + " group by pdt." + AppConstant.PURCHASE_DETAIL_STOCK_ID + " order by sum_total desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});

            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setAmount(cursor.getDouble(0));
                    reportItem.setName(cursor.getString(1));
                    reportItem.setQty(cursor.getInt(2));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public double getTotalAmountOfTotalAmountOfAllPurchases(String startDate, String endDate, String categoryID) {
        String query = "select sum(total_sum.amt) from (" +
                "select sum(pdt." + AppConstant.PURCHASE_DETAIL_TOTAL + ")  as amt from " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " pdt, " + AppConstant.PURCHASE_TABLE_NAME + " s, " + AppConstant.STOCK_TABLE_NAME + " st " +
                "where pdt." + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? and pdt." + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = st." + AppConstant.STOCK_ID + " and " + AppConstant.STOCK_CATEGORY_ID + " " + categoryID + " group by pdt." + AppConstant.PURCHASE_DETAIL_STOCK_ID +
                ") as total_sum";
        double totalAmt = 0;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});

            if (cursor.moveToFirst()) {
                do {
                    totalAmt = cursor.getDouble(0);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return totalAmt;
    }

    public List<ReportItem> getTotalAmtByIncomeExpense(String startDate, String endDate, int startLimit, int endLimit, String type) {
        Log.e("type", "type");
        reportItemList = new ArrayList<>();
        query = "select sum(" + AppConstant.EXPENSE_AMOUNT + ") sum_amt, " + AppConstant.EXPENSE_NAME_NAME + " , " + AppConstant.EXPENSE_NAME_TYPE + " from " +
                AppConstant.EXPENSE_TABLE_NAME + ", " + AppConstant.EXPENSE_NAME_TABLE_NAME +
                " n where " + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_NAME_ID + " and " + AppConstant.EXPENSE_DATE + " between ? and ? and " +
                " type = ? group by " + AppConstant.EXPENSE_NAME_NAME + " order by sum_amt desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, type});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setBalance(cursor.getDouble(0));
                    reportItem.setName(cursor.getString(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    private boolean createMonthTempTable(String[] dataArr) {

        query = "create temp table if not exists " + AppConstant.MONTH_TEMP_TABLE_NAME + "(" + AppConstant.MONTH_TEMP_MONTH + " integer not null, " + AppConstant.MONTH_TEMP_YEAR + " integer, primary key (" + AppConstant.MONTH_TEMP_MONTH + "));";
        //Log.e("month temp", query);
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query);
            database.execSQL("delete from MonthTemp");
            Log.e("delete", "de");
            query = "insert into MonthTemp(" + AppConstant.MONTH_TEMP_MONTH + ", year) values ";// (1) ,(2), (3), (4), (5), (6), (7), (8), (9), (10), (11), (12);";
            for (int i = 0; i < 11; i++) {
                query += dataArr[i] + ",";
                Log.e(dataArr[i] + "i", "idoa");
            }
            //Log.e("query", query);
            query += dataArr[11];
            //Log.e("quer", query);
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }

        return flag.isInserted();
    }

    public List<ReportItem> monthlyGrossProfit(String startDate, String endDate, String[] dateFilterArr, String orderby) {
        Log.e(startDate, endDate + ", " + dateFilterArr.length);
        createMonthTempTable(dateFilterArr);
        //        for (String date : dateFilterArr){
        //            Log.e("month", date);
        //        }
        reportItemList = new ArrayList<>();
        query = "select sum_amt, month, year from (" +
                "select sum((d." + AppConstant.SALES_DETAIL_PRICE + "-t." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ") * ifnull(t." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + "," +
                AppConstant.SALES_DETAIL_QTY + ")) sum_amt, s." + AppConstant.SALES_MONTH + ", s." + AppConstant.SALES_YEAR + " from " + AppConstant.SALES_TABLE_NAME + " s, " +
                AppConstant.SALES_DETAIL_TABLE_NAME + " d, " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " t " + "where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID
                + " and d." + AppConstant.SALES_DETAIL_STOCK_ID + " = t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " and " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE +
                " = 's' and d." + AppConstant.SALES_DETAIL_ID + " = " + AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID + " and " +
                AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ? group by s." + AppConstant.SALES_MONTH +
                " union " +
                "select 0.0, " + AppConstant.MONTH_TEMP_MONTH + ", " + AppConstant.MONTH_TEMP_YEAR + " from " + AppConstant.MONTH_TEMP_TABLE_NAME + " t where not exists(select " + AppConstant.SALES_MONTH + " from " + AppConstant.SALES_TABLE_NAME +
                " s where s." + AppConstant.SALES_MONTH + " = t." + AppConstant.MONTH_TEMP_MONTH + " and " +
                AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ?) " +
                ")order by cast(" + AppConstant.MONTH_TEMP_YEAR + " as integer) " + orderby + ", cast(" + AppConstant.MONTH_TEMP_MONTH + " as integer) " + orderby;
        //Log.e("gross ", query);
        // Log.e("query", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setBalance(cursor.getDouble(0));
                    reportItem.setMonth(cursor.getInt(1));
                    //Log.e("aaf", cursor.getDouble(0)+" dd");
                    Log.e(cursor.getDouble(0) + "month", cursor.getString(1));

                    // reportItem.setName(cursor.getString(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        Log.e("si", reportItemList.size() + "k");
        return reportItemList;
    }

    public List<ReportItem> monthlyGrossProfitByProduct(Long stockID, String startDate, String endDate, String[] dateFilterArr, String orderby) {
        Log.e(startDate, endDate);
        createMonthTempTable(dateFilterArr);
        reportItemList = new ArrayList<>();
        query = "select sum_amt, month, year from (" +
                "select sum((d." + AppConstant.SALES_DETAIL_PRICE + "-t." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ") * ifnull(t." + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + "," +
                AppConstant.SALES_DETAIL_QTY + ")) sum_amt, s." + AppConstant.SALES_MONTH + ", s." + AppConstant.SALES_YEAR + " from " + AppConstant.SALES_TABLE_NAME + " s, " +
                AppConstant.SALES_DETAIL_TABLE_NAME + " d, " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " t " + "where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID
                + " and d." + AppConstant.SALES_DETAIL_STOCK_ID + " = t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " and " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE +
                " = 's' and d." + AppConstant.SALES_DETAIL_STOCK_ID + " = " + stockID + " and " + AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID + " = d." + AppConstant.SALES_DETAIL_ID + " and " +
                AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ? group by s." + AppConstant.SALES_MONTH +
                " union " +
                "select 0.0, " + AppConstant.MONTH_TEMP_MONTH + ", " + AppConstant.MONTH_TEMP_YEAR + " from " + AppConstant.MONTH_TEMP_TABLE_NAME + " t where not exists(select "
                + AppConstant.SALES_MONTH + " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.SALES_DETAIL_TABLE_NAME + " d where s." + AppConstant.SALES_MONTH + " = t." + AppConstant.MONTH_TEMP_MONTH + " and " +
                AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ? and " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DETAIL_STOCK_ID + " = " + stockID + ") " +
                ")order by cast(" + AppConstant.MONTH_TEMP_YEAR + " as integer) " + orderby + ", cast(" + AppConstant.MONTH_TEMP_MONTH + " as integer) " + orderby;

        //Log.e("gross", query);
        databaseReadTransaction(flag);
        try {
            Log.e("stockID", stockID.toString());
            cursor = database.rawQuery(query, new String[]{startDate, endDate, startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setBalance(cursor.getDouble(0));
                    Log.e("aaf", cursor.getDouble(0) + " dd");
                    // reportItem.setName(cursor.getString(1));
                    reportItem.setMonth(cursor.getInt(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        //Log.e("si", reportItemList.size()+"k");
        return reportItemList;
    }

    public List<ReportItem> productPriceTrend(Long stockID, String startDate, String endDate, String orderBy, String[] dateArr) {
        createMonthTempTable(dateArr);
        Log.e("query", stockID.toString());
        Log.e(startDate, endDate);
        reportItemList = new ArrayList<>();
        query = "select " + AppConstant.BUSINESS_SETTING_VALUATION_METHOD + " from " + AppConstant.BUSINESS_SETTING_TABLE_NAME;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                String valuationMethod = cursor.getString(0);
                Cursor cursor1;
                Log.e("value", valuationMethod);
                if (valuationMethod.equalsIgnoreCase("AVG")) {
                    query = "select stockID, price, name, month, year from( " +
                            "select " + AppConstant.PURCHASE_DETAIL_STOCK_ID + ", round(sum(" + AppConstant.PURCHASE_DETAIL_QTY + " * " + AppConstant.PURCHASE_DETAIL_PRICE + ")/sum(" + AppConstant.PURCHASE_DETAIL_QTY + "), 2) as price, " +
                            AppConstant.STOCK_NAME + ", " + AppConstant.PURCHASE_MONTH + ", " + AppConstant.PURCHASE_YEAR + " from " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, "
                            + AppConstant.STOCK_TABLE_NAME + " s where  s." + AppConstant.STOCK_ID + " = " + stockID + " and  " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and " +
                            AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " + AppConstant.PURCHASE_DATE + " >= ? and  " + AppConstant.PURCHASE_DATE + " <= ? group by " + AppConstant.PURCHASE_MONTH +
                            " union " +
                            " select distinct id, 0.0, name, " + AppConstant.MONTH_TEMP_MONTH + ", " + AppConstant.MONTH_TEMP_YEAR + " from Stock s, " + AppConstant.MONTH_TEMP_TABLE_NAME + " t where s.id = " + stockID + " and not exists  " +
                            " (select * from PurchaseDetail d, Purchase p where " +
                            "d.purchaseID = p.id and d.stockID = s.id and t." + AppConstant.MONTH_TEMP_MONTH + " = p.month and date >= ? and  date <= ? group by month, year)) t " +
                            " order by cast(" + AppConstant.MONTH_TEMP_YEAR + " as integer) " + orderBy + ", cast(" + AppConstant.MONTH_TEMP_MONTH + " as integer) " + orderBy;
                    cursor1 = database.rawQuery(query, new String[]{startDate, endDate, startDate, endDate});
                } else if (valuationMethod.equalsIgnoreCase("LIFO")) {
                    query = "select stockID, price, name, month, year, time from( " +
                            "select " + AppConstant.PURCHASE_DETAIL_STOCK_ID + ", " + AppConstant.PURCHASE_DETAIL_PRICE + ", " + AppConstant.STOCK_NAME + ", " + AppConstant.PURCHASE_MONTH + ", " + AppConstant.PURCHASE_YEAR +
                            ", max(p." + AppConstant.PURCHASE_TIME + ") as time " + " from " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, " + AppConstant.STOCK_TABLE_NAME + " s where  s."
                            + AppConstant.STOCK_ID + " = " + stockID + " and  " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." +
                            AppConstant.STOCK_ID + " and " + AppConstant.PURCHASE_DATE + " >= ? and  " + AppConstant.PURCHASE_DATE + " <= ? group by " + AppConstant.PURCHASE_MONTH +
                            " union " +
                            " select distinct " + AppConstant.STOCK_ID + ", 0.0, " + AppConstant.STOCK_NAME + ", " + AppConstant.MONTH_TEMP_MONTH + ", " + AppConstant.MONTH_TEMP_YEAR + ", 0 from " + AppConstant.STOCK_TABLE_NAME
                            + " s, " + AppConstant.MONTH_TEMP_TABLE_NAME + " t where s." + AppConstant.STOCK_ID + " = " + stockID + " and not exists  " +
                            " (select * from " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, " + AppConstant.PURCHASE_TABLE_NAME + " p where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID +
                            " = p." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and t." + AppConstant.MONTH_TEMP_MONTH + " = p." + AppConstant.PURCHASE_MONTH +
                            " and " + AppConstant.PURCHASE_DATE + " >= ? and  " + AppConstant.PURCHASE_DATE + " <= ? group by " + AppConstant.PURCHASE_MONTH + ", " + AppConstant.PURCHASE_YEAR + ")) t " +
                            " order by cast(" + AppConstant.MONTH_TEMP_YEAR + " as integer) " + orderBy + ", cast(" + AppConstant.MONTH_TEMP_MONTH + " as integer) " + orderBy;
                    cursor1 = database.rawQuery(query, new String[]{startDate, endDate, startDate, endDate});
                } else {
                    query = "select stockID, price, name, month, year, time from( " +
                            "select stockID, price, name, month, year, min(p.time) as time " +
                            " from Purchase p, PurchaseDetail d, Stock s where  s.id = " + stockID + " and  " +
                            "purchaseID = p.id and stockID = s.id and date >= ? and  date <= ? group by month " +
                            "union " +
                            " select distinct id, 0.0, name, " + AppConstant.MONTH_TEMP_MONTH + ", " + AppConstant.MONTH_TEMP_YEAR + ", 0 from Stock s, " + AppConstant.MONTH_TEMP_TABLE_NAME + " t where s.id = " + stockID + " and not exists  " +
                            " (select * from PurchaseDetail d, Purchase p where " +
                            "d.purchaseID = p.id and d.stockID = s.id and t." + AppConstant.MONTH_TEMP_MONTH + " = p.month and date >= ? and  date <= ? group by month, year)) t " +
                            " order by cast(" + AppConstant.MONTH_TEMP_YEAR + " as integer) " + orderBy + ", cast(" + AppConstant.MONTH_TEMP_MONTH + " as integer) " + orderBy;
                    cursor1 = database.rawQuery(query, new String[]{startDate, endDate, startDate, endDate});
                }
                if (cursor1.moveToFirst()) {
                    do {
                        reportItem = new ReportItem();
                        reportItem.setId(cursor1.getLong(0));
                        reportItem.setBalance(cursor1.getDouble(1));
                        reportItem.setName(cursor1.getString(2));
                        reportItem.setMonth(cursor1.getInt(3));
                        reportItemList.add(reportItem);
                    } while (cursor1.moveToNext());
                }
            }

            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }

        //Log.e("dae", reportItemList.size()+ " da");
        return reportItemList;
    }

    public List<ReportItem> inventoryValuation(int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select c." + AppConstant.CATEGORY_NAME + ", round( sum(" + AppConstant.PURCHASE_TEMP_QTY + " * " + AppConstant.PURCHASE_TEMP_PRICE + ") , 2) total_value from " + AppConstant.PURCHASE_TEMP_TABLE_NAME +
                " t, " + AppConstant.STOCK_TABLE_NAME + " s, " + AppConstant.CATEGORY_TABLE_NAME + " c where t." + AppConstant.PURCHASE_TEMP_STOCK_ID + " = s." +
                AppConstant.STOCK_ID + " and s." + AppConstant.STOCK_CATEGORY_ID
                + "= c." + AppConstant.CATEGORY_ID + " and " + AppConstant.PURCHASE_TEMP_QTY + " <> 0 group by c." + AppConstant.CATEGORY_ID + " limit " + startLimit + ", " + endLimit;
        //Log.e("inv", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    //Log.e(cursor.getString(0), "io");
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(0));
                    reportItem.setBalance(cursor.getDouble(1));
                    //Log.e("aaf", cursor.getDouble(0)+" dd");
                    // reportItem.setName(cursor.getString(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        //Log.e("si", reportItemList.size()+"k");
        return reportItemList;
    }

    public Double totalInventoryValuation() {
        Double value = 0.0;
        query = "select sum(total_value) from (select round( sum(" + AppConstant.PURCHASE_TEMP_QTY +
                " * " + AppConstant.PURCHASE_TEMP_PRICE + ") , 2) total_value from " + AppConstant.PURCHASE_TEMP_TABLE_NAME +
                " t )";
        //Log.e("inv", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                //Log.e(cursor.getString(0), "io");
                value = cursor.getDouble(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        //        Log.e("sadfadfi", value+"k");
        return value;
    }


    public List<ReportItem> productValuationByCategoryID(int startLimit, int endLimit, String categoryID) {
        reportItemList = new ArrayList<>();
        //        Log.e("catid", categoryID);
        //        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_NAME + ", catName, " + AppConstant.STOCK_INVENTORY_QTY + ", " + AppConstant.PURCHASE_TEMP_PRICE + ", sum(total_value) totalValue " + ", " + AppConstant.UNIT_UNIT +
        //                " from (select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_NAME + ", c." + AppConstant.CATEGORY_NAME + " catName, round(t." + AppConstant.PURCHASE_TEMP_PRICE + " * t." + AppConstant.PURCHASE_TEMP_QTY + ", 2) total_value, " +
        //                 AppConstant.STOCK_INVENTORY_QTY + ", " + AppConstant.STOCK_UNIT_ID + ", t." + AppConstant.PURCHASE_TEMP_PRICE + ", s." + AppConstant.STOCK_ID + " from " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.STOCK_TABLE_NAME +
        //                " s "  + ", " + AppConstant.CATEGORY_TABLE_NAME + " c " + " where t." + AppConstant.PURCHASE_TEMP_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and s." +
        //                AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and s." + AppConstant.STOCK_CATEGORY_ID + " = " + categoryID + " and t." + AppConstant.PURCHASE_TEMP_QTY + " <> 0) s left outer join " +
        //                AppConstant.UNIT_TABLE_NAME + " u on " + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + " group by s." + AppConstant.STOCK_ID + " order by totalValue desc limit " + startLimit + ", " + endLimit;
        //        Log.e("quer", query);
        query = " select s.id, s.name, catName, sum(qty), price, sum(total_value) totalValue, unit from (\n" +
                " select s.id, s.name, c.name as catName, round(t.price * t.qty, 2) total_value,t.qty, unitID,  t.price from PurchaseTemp t, \n" +
                " Stock s , Category c  where t.stockID = s.id and s.categoryID = c.id and (s.categoryID = " + categoryID + ") and t.qty <> 0)s left outer join Unit u on unitID = u.id\n" +
                " group by s.id order by totalValue desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(1));
                    reportItem.setName1(cursor.getString(2));
                    reportItem.setTotalAmt(cursor.getDouble(cursor.getColumnIndex("totalValue")));
                    reportItem.setTotalQty(cursor.getInt(3));
                    reportItem.setName2(cursor.getString(cursor.getColumnIndex(AppConstant.UNIT_UNIT)));
                    reportItem.setAmount(cursor.getDouble(4));
                    //                    Log.e("idoa", cursor.getDouble(4) + "ioe");
                    //Log.e("aaf", cursor.getDouble(0)+" dd");
                    // reportItem.setName(cursor.getString(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            Log.e("size", reportItemList.size() + "o");
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        //Log.e("si", reportItemList.size()+"k");
        return reportItemList;
    }

    public Double totalProductValuationByCategoryID(String categoryID) {
        //        Log.e("catid", categoryID);
        //        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_NAME + ", catName, " + AppConstant.STOCK_INVENTORY_QTY + ", " + AppConstant.PURCHASE_TEMP_PRICE + ", sum(total_value) totalValue " + ", " + AppConstant.UNIT_UNIT +
        //                " from (select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_NAME + ", c." + AppConstant.CATEGORY_NAME + " catName, round(t." + AppConstant.PURCHASE_TEMP_PRICE + " * t." + AppConstant.PURCHASE_TEMP_QTY + ", 2) total_value, " +
        //                AppConstant.STOCK_INVENTORY_QTY + ", " + AppConstant.STOCK_UNIT_ID + ", t." + AppConstant.PURCHASE_TEMP_PRICE + ", s." + AppConstant.STOCK_ID + " from " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.STOCK_TABLE_NAME +
        //                " s "  + ", " + AppConstant.CATEGORY_TABLE_NAME + " c " + " where t." + AppConstant.PURCHASE_TEMP_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and s." +
        //                AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and s." + AppConstant.STOCK_CATEGORY_ID + " = " + categoryID + " and t." + AppConstant.PURCHASE_TEMP_QTY + " <> 0) s left outer join " +
        //                AppConstant.UNIT_TABLE_NAME + " u on " + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + " group by s." + AppConstant.STOCK_ID + " order by totalValue desc limit " + startLimit + ", " + endLimit;
        //        Log.e("quer", query);
        query = "select sum(total_value) totalValue from (\n" +
                " select round(t.price * t.qty, 2) total_value from PurchaseTemp t, \n" +
                " Stock s  where t.stockID = s.id and (s.categoryID = " + categoryID + ") and t.qty <> 0)s ";
        Double value = 0.0;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                value = cursor.getDouble(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        //Log.e("si", reportItemList.size()+"k");
        return value;
    }


    public List<ReportItem> productValuation(Long stockID) {
        ReportItem reportItem;
        reportItemList = new ArrayList<>();
        //        query = "select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_NAME + ", catName, " + AppConstant.STOCK_INVENTORY_QTY + ", " + AppConstant.PURCHASE_TEMP_PRICE + ", sum(total_value) totalValue " + ", " + AppConstant.UNIT_UNIT +
        //                " from (select s." + AppConstant.STOCK_ID + ", s." + AppConstant.STOCK_NAME + ", c." + AppConstant.CATEGORY_NAME + " catName, round(t." + AppConstant.PURCHASE_TEMP_PRICE + " * t." + AppConstant.PURCHASE_TEMP_QTY + ", 2) total_value, " +
        //                AppConstant.STOCK_INVENTORY_QTY + ", " + AppConstant.STOCK_UNIT_ID + ", t." + AppConstant.PURCHASE_TEMP_PRICE + ", s." + AppConstant.STOCK_ID + " from " + AppConstant.PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.STOCK_TABLE_NAME +
        //                " s "  + ", " + AppConstant.CATEGORY_TABLE_NAME + " c " + " where t." + AppConstant.PURCHASE_TEMP_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and s." +
        //                AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and s." + AppConstant.STOCK_CATEGORY_ID + " = " + stockID + " and t." + AppConstant.PURCHASE_TEMP_QTY + " <> 0) s left outer join " +
        //                AppConstant.UNIT_TABLE_NAME + " u on " + AppConstant.STOCK_UNIT_ID + " = u." + AppConstant.UNIT_ID + " group by s." + AppConstant.STOCK_ID + " order by totalValue desc limit " + startLimit + ", " + endLimit;
        //        Log.e("quer", query);
        query = " select s.id, s.name, c.name, round(sum(t.price * t.qty) , 2) total_value, sum(t.qty) , unit, unitID, t.price from PurchaseTemp t, \n" +
                " Stock s left outer join Unit u on unitID = u.id, Category c  where t.stockID = s.id and s.categoryID = c.id and s.id = " + stockID + " and t.qty <> 0 \n group by s.id";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                reportItem = new ReportItem();
                reportItem.setName(cursor.getString(1));
                reportItem.setName1(cursor.getString(2));
                reportItem.setTotalAmt(cursor.getDouble(3));
                reportItem.setTotalQty(cursor.getInt(4));
                reportItem.setName2(cursor.getString(5));
                reportItem.setAmount(cursor.getDouble(7));
                reportItemList.add(reportItem);
                //                    Log.e("idoa", cursor.getDouble(4) + "ioe");
                //Log.e("aaf", cursor.getDouble(0)+" dd");
                // reportItem.setName(cursor.getString(1))
            }
            //            Log.e("size", reportItemList.size()+ "o");
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        //Log.e("si", reportItemList.size()+"k");
        return reportItemList;
    }

    public List<ReportItem> supplierByProduct(Long productID, int startLimit, int endLimit) {
        Log.e("iefoe", "iroeuhje");
        reportItemList = new ArrayList<>();
        query = "select sup." + AppConstant.SUPPLIER_NAME + ", " + AppConstant.PURCHASE_DETAIL_PRICE + ", max(p." + AppConstant.PURCHASE_TIME + "), " + AppConstant.PURCHASE_YEAR + ", " +
                AppConstant.PURCHASE_MONTH + ", " + AppConstant.PURCHASE_DAY + ", " + AppConstant.SUPPLIER_BUSINESS_NAME + " from " + AppConstant.STOCK_TABLE_NAME + " s, " + AppConstant.SUPPLIER_TABLE_NAME +
                " sup, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, " + AppConstant.PURCHASE_TABLE_NAME + " p where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and " +
                AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " + AppConstant.PURCHASE_SUPPLIER_ID + " = sup." + AppConstant.SUPPLIER_ID +
                " and s. " + AppConstant.STOCK_ID + " = " + productID + " group by sup." + AppConstant.SUPPLIER_ID + " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(0));
                    reportItem.setBalance(cursor.getDouble(1));
                    reportItem.setDate(DateUtility.makeDateFormatWithSlash(cursor.getString(3), cursor.getString(4), cursor.getString(5)));
                    //Log.e("aaf", cursor.getDouble(0)+" dd");
                    // reportItem.setName(cursor.getString(1));
                    reportItem.setName1(cursor.getString(6));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        Log.e("si", reportItemList.size() + " dak");
        return reportItemList;
    }

    public List<ReportItem> bottomSalesItems(String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select sum(" + AppConstant.SALES_DETAIL_QTY + ") as sum_qty, " + AppConstant.STOCK_NAME + " from " + AppConstant.SALES_DETAIL_TABLE_NAME + ", " + AppConstant.SALES_TABLE_NAME
                + " s, " + AppConstant.STOCK_TABLE_NAME + " st where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DETAIL_STOCK_ID +
                " = st." + AppConstant.STOCK_ID + " and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE +
                " <= ? group by " + AppConstant.SALES_DETAIL_STOCK_ID + " order by sum_qty asc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setQty(cursor.getInt(0));
                    reportItem.setName(cursor.getString(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> bottomSales(String startDate, String endDate, int startLimit, int endLimit, String categoryID) {
        reportItemList = new ArrayList<>();
        //Log.e(query , "djfak");
        query = "select sum(" + AppConstant.SALES_DETAIL_TOTAL + ") as sum_total, " + AppConstant.STOCK_NAME + " from " + AppConstant.SALES_DETAIL_TABLE_NAME + ", " + AppConstant.SALES_TABLE_NAME
                + " s, " + AppConstant.STOCK_TABLE_NAME + " st where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DETAIL_STOCK_ID +
                " = st." + AppConstant.STOCK_ID + " and " + AppConstant.STOCK_CATEGORY_ID + categoryID + " and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE +
                " <= ? group by " + AppConstant.SALES_DETAIL_STOCK_ID + " order by sum_total asc limit " + startLimit + ", " + endLimit;
        Log.e("data", "date1");
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    Log.e("data", "date");
                    reportItem = new ReportItem();
                    reportItem.setBalance(cursor.getDouble(0));
                    reportItem.setName(cursor.getString(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> topOutstandingCustomer(int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select " + AppConstant.CUSTOMER_NAME + ", " + AppConstant.CUSTOMER_BALANCE + " from " + AppConstant.CUSTOMER_TABLE_NAME + " where " + AppConstant.CUSTOMER_BALANCE +
                " > 0.0 order by " + AppConstant.CUSTOMER_BALANCE + " desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(0));
                    reportItem.setBalance(cursor.getDouble(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> topOutstandingSupplier(int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select " + AppConstant.CATEGORY_NAME + ", " + AppConstant.SUPPLIER_BALANCE + " from " + AppConstant.SUPPLIER_TABLE_NAME + " where " + AppConstant.SUPPLIER_BALANCE +
                " > 0.0 order by " + AppConstant.SUPPLIER_BALANCE + " desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(0));
                    reportItem.setBalance(cursor.getDouble(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> topSalesCustomers(String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select sum(" + AppConstant.SALES_DETAIL_TOTAL + ") as sum_amt, " + AppConstant.CUSTOMER_NAME + " from " + AppConstant.SALES_TABLE_NAME + " s, " +
                AppConstant.CUSTOMER_TABLE_NAME + " c, " + AppConstant.SALES_DETAIL_TABLE_NAME + " d where " + AppConstant.SALES_CUSTOMER_ID + " = c." + AppConstant.CUSTOMER_ID + " and d." +
                AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE + " >= ? and  " +
                AppConstant.SALES_DATE + " <= ? group by c." + AppConstant.CUSTOMER_ID + " order by sum_amt desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(1));
                    reportItem.setBalance(cursor.getDouble(0));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> topSuppliers(String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select sum(d." + AppConstant.PURCHASE_DETAIL_TOTAL + ") as sum_amt, " + AppConstant.SUPPLIER_NAME + ", c." + AppConstant.SUPPLIER_ID + " from " + AppConstant.PURCHASE_TABLE_NAME + " s, " +
                AppConstant.SUPPLIER_TABLE_NAME + " c, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d " + " where " + AppConstant.PURCHASE_SUPPLIER_ID + " = c." + AppConstant.SUPPLIER_ID + " and " +
                AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DATE + " >= ? and  " +
                AppConstant.PURCHASE_DATE + " <= ? group by c." + AppConstant.SUPPLIER_ID + " order by sum_amt desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(1));
                    reportItem.setBalance(cursor.getDouble(0));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> monthlySalesTransaction(String startDate, String endDate, String[] monthYearArr, String orderby) {
        createMonthTempTable(monthYearArr);
        Log.e(startDate, endDate);
        reportItemList = new ArrayList<>();
        query = "select count_id, month, year from (" +
                "select count(" + AppConstant.SALES_ID + " ) count_id, " + AppConstant.SALES_MONTH + ", " + AppConstant.SALES_YEAR + " from " + AppConstant.SALES_TABLE_NAME + " where " + AppConstant.SALES_DATE +
                " >= ? and " + AppConstant.SALES_DATE + " <= ? group by " + AppConstant.SALES_MONTH +
                " union " +
                "select 0, " +
                AppConstant.MONTH_TEMP_MONTH + ", " +
                AppConstant.MONTH_TEMP_YEAR + " from " + AppConstant.MONTH_TEMP_TABLE_NAME + " t where not exists(select " + AppConstant.SALES_MONTH + " from " + AppConstant.SALES_TABLE_NAME
                + " s where s." + AppConstant.SALES_MONTH + " = t." + AppConstant.MONTH_TEMP_MONTH + " and " +
                AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ?))order by cast(" + AppConstant.MONTH_TEMP_YEAR + " as integer) " +
                orderby + ", cast(" + AppConstant.MONTH_TEMP_MONTH + " as integer)" + orderby + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setQty(cursor.getInt(0));
                    // reportItem.setBalance(cursor.getDouble(1));
                    reportItem.setMonth(cursor.getInt(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> monthlyPurchaseTransactions(String startDate, String endDate, String[] monthYearArr, String orderby) {
        //Log.e(startDate, endDate);
        createMonthTempTable(monthYearArr);
        reportItemList = new ArrayList<>();
        query = "select count_id, month, year from (select count(" + AppConstant.PURCHASE_ID + ") count_id, " +
                AppConstant.PURCHASE_MONTH + ", " + AppConstant.PURCHASE_YEAR + " from " + AppConstant.PURCHASE_TABLE_NAME + " where " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE +
                " <= ? group by " + AppConstant.PURCHASE_MONTH +
                " union select 0 as count_id, " + AppConstant.MONTH_TEMP_MONTH + ", " + AppConstant.MONTH_TEMP_YEAR + " from " + AppConstant.MONTH_TEMP_TABLE_NAME + " t where not exists(select " + AppConstant.PURCHASE_MONTH + " from " + AppConstant.PURCHASE_TABLE_NAME + " p " +
                " where p." + AppConstant.PURCHASE_MONTH + " = t." + AppConstant.MONTH_TEMP_MONTH + " and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ?)) " +
                "order by cast(" + AppConstant.MONTH_TEMP_YEAR + " as integer) " + orderby + ", cast(" + AppConstant.MONTH_TEMP_MONTH + " as integer) " + orderby;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setQty(cursor.getInt(0));
                    reportItem.setMonth(cursor.getInt(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> monthlyIncomeTransactions(String startDate, String endDate, String[] monthYearArr, String orderby) {
        createMonthTempTable(monthYearArr);
        reportItemList = new ArrayList<>();
        //        query = "select sum_amt, month, year from (select sum(" + AppConstant.EXPENSE_AMOUNT + ") sum_amt, " +
        //                AppConstant.EXPENSE_MONTH + ", " + AppConstant.EXPENSE_YEAR + " from " + AppConstant.EXPENSE_TABLE_NAME + ", " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where " + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." +
        //                AppConstant.EXPENSE_NAME_ID + " and " + AppConstant.EXPENSE_NAME_TYPE + " = ? and " + AppConstant.EXPENSE_DATE + " >= ? and " + AppConstant.EXPENSE_DATE + " <= ? group by " + AppConstant.EXPENSE_MONTH +
        //                " union select 0, " + AppConstant.MONTH_TEMP_MONTH + ", " + AppConstant.MONTH_TEMP_YEAR + " from " + AppConstant.MONTH_TEMP_TABLE_NAME +" t where not exists(select " + AppConstant.EXPENSE_MONTH +
        //                " from " + AppConstant.EXPENSE_TABLE_NAME + " e, " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n " +
        //                " where " + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_NAME_ID + " and " + AppConstant.EXPENSE_NAME_TYPE + " = ? and e." + AppConstant.EXPENSE_MONTH +
        //                " = t." + AppConstant.MONTH_TEMP_MONTH + " and " + AppConstant.EXPENSE_DATE + " >= ? and " + AppConstant.EXPENSE_DATE + " <= ?)) " +
        //                "order by cast(" + AppConstant.MONTH_TEMP_YEAR + " as integer)" + orderby + ", cast(" + AppConstant.MONTH_TEMP_MONTH + " as integer) " + orderby +";";
        //
        /*
SELECT sum_amt, month, year
FROM
(
	SELECT SUM(sum_amt) sum_amt, month, year
	FROM
	(
		SELECT SUM(amount) sum_amt, mont, year
		FROM Expense, ExpenseName n
		WHERE
			expenseNameID = n.id
			AND
			type = 'Income'
			AND
			date >= '01012010' AND date <= '20202010'
		GROUP BY month
		UNION ALL
			SELECT SUM(totalAmount - taxAmt - + discountAmt ), month, year
			FROM Sales
			WHERE
				date >= '01012010' AND date <= '20202010'
			GROUP BY month
	)
	GROUP BY month
	UNION
		SELECT 0, month, year from MonthTemp t
		WHERE NOT EXISTS
		(
			SELECT month FROM
			(
				SELECT DISTINCT month
				FROM Expense e, ExpenseName n
				WHERE
					expenseNameID = n.id
					AND
					type = 'Income'
					AND
					date >= '01012010' AND date <= '20202010'
				UNION
					SELECT DISTINCT month
					FROM Sales s
					WHERE
						date >= '01012010' AND date <= '20202010'
			)
			WHERE
				month = t.month
		)
)
ORDER BY
	CAST(year as integer) DESC,
	CAST(month as integer) DESC
         */
        query = "select sum_amt, month, year from (\n" +
                "select sum(sum_amt)sum_amt, month, year from(\n" +
                "select sum(amount) sum_amt, month, year from Expense, ExpenseName n where expenseNameID = n.id \n" +
                "and type = ? and date >= ? and date <= ? group by month\n" +
                "union all select sum(totalAmount - taxAmt) , month, year from Sales where date >= ? and date <= ? group by month\n" +
                ") group by month\n" +
                " union \n" +
                "select 0, month, year from MonthTemp t where not exists(select month from (select distinct month from Expense e, ExpenseName n  where expenseNameID = n.id and type = ? and \n" +
                "date >= ? and date <= ? union select distinct month from Sales s where date>=? and date <= ? ) where month = t.month )) order by cast(year as integer)" + orderby + ", cast(month as integer) " + orderby;
        //Log.e("query", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{ExpenseIncome.Type.Income.toString(), startDate, endDate, startDate, endDate, ExpenseIncome.Type.Income.toString(), startDate, endDate, startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setMonth(cursor.getInt(1));
                    reportItem.setBalance(cursor.getDouble(0));
                    //Log.e("b",reportItem.getBalance()+" ie");
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> monthlyExpenseTransactions(String startDate, String endDate, String[] monthYearArr, String orderby) {
        createMonthTempTable(monthYearArr);
        reportItemList = new ArrayList<>();
        //        query = "select sum_amt, month, year from (select sum(" + AppConstant.EXPENSE_AMOUNT + ") sum_amt, " +
        //                AppConstant.EXPENSE_MONTH + ", " + AppConstant.EXPENSE_YEAR + " from " + AppConstant.EXPENSE_TABLE_NAME + ", " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where " + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." +
        //                AppConstant.EXPENSE_NAME_ID + " and " + AppConstant.EXPENSE_NAME_TYPE + " = ? and " + AppConstant.EXPENSE_DATE + " >= ? and " + AppConstant.EXPENSE_DATE + " <= ? group by " + AppConstant.EXPENSE_MONTH +
        //                " union select 0, " + AppConstant.MONTH_TEMP_MONTH + ", " + AppConstant.MONTH_TEMP_YEAR + " from " + AppConstant.MONTH_TEMP_TABLE_NAME +" t where not exists(select " + AppConstant.EXPENSE_MONTH +
        //                " from " + AppConstant.EXPENSE_TABLE_NAME + " e, " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n " +
        //                " where " + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_NAME_ID + " and " + AppConstant.EXPENSE_NAME_TYPE + " = ? and e." + AppConstant.EXPENSE_MONTH +
        //                " = t." + AppConstant.MONTH_TEMP_MONTH + " and " + AppConstant.EXPENSE_DATE + " >= ? and " + AppConstant.EXPENSE_DATE + " <= ?)) " +
        //                "order by cast(" + AppConstant.MONTH_TEMP_YEAR + " as integer)" + orderby + ", cast(" + AppConstant.MONTH_TEMP_MONTH + " as integer) " + orderby +";";
        //
        query = "select sum_amt, month, year from (\n" +
                "select sum(sum_amt)sum_amt, month, year from(\n" +
                "select sum(" + AppConstant.EXPENSE_AMOUNT + ") sum_amt, " + AppConstant.EXPENSE_MONTH + ", " + AppConstant.EXPENSE_YEAR + " year from " + AppConstant.EXPENSE_TABLE_NAME + ", " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where " +
                AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_ID +
                " and type = ? and date >= ? and date <= ? group by month\n" +
                "union all select sum(" + AppConstant.PURCHASE_TOTAL_AMOUNT + " - " + AppConstant.PURCHASE_TAX_AMOUNT + "-" + AppConstant.PURCHASE_DISCOUNT_AMOUNT + ") , month, year from Purchase where date >= ? and date <= ? group by month\n" +
                ") group by month\n" +
                " union \n" +
                "select 0, month, year from MonthTemp t where not exists(select month from (select distinct month from Expense e, ExpenseName n  where expenseNameID = n.id and type = ? and \n" +
                "date >= ? and date <= ? union select distinct month from Purchase s where date >=? and date <= ? ) where month = t.month )) order by cast(year as integer)" + orderby + ", cast(month as integer) " + orderby;
        //Log.e("query", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{ExpenseIncome.Type.Expense.toString(), startDate, endDate, startDate, endDate, ExpenseIncome.Type.Expense.toString(), startDate, endDate, startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setMonth(cursor.getInt(1));
                    reportItem.setBalance(cursor.getDouble(0));
                    //Log.e("b",reportItem.getBalance()+" ie");
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> monthlySalesTax(String startDate, String endDate, String[] monthYearArr, String orderby) {
        createMonthTempTable(monthYearArr);
        reportItemList = new ArrayList<>();
        query = "select sum_amt, month, year from (select sum(" + AppConstant.SALES_TAX_AMOUNT + ") sum_amt, " + AppConstant.SALES_MONTH + ", " + AppConstant.SALES_YEAR + " from " + AppConstant.SALES_TABLE_NAME + " where " +
                AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ? group by month union select 0," + AppConstant.SALES_MONTH + ", " + AppConstant.SALES_YEAR + " from MonthTemp t where not " +
                "exists(select " + AppConstant.SALES_MONTH + " from " + AppConstant.SALES_TABLE_NAME + " s where  s." + AppConstant.SALES_MONTH + " = t.month and " + AppConstant.SALES_DATE + " >= ? " +
                "and " + AppConstant.SALES_DATE + " <= ?)) order by cast(year as integer)" + orderby + ", cast(month as integer) " + orderby + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setBalance(cursor.getDouble(0));
                    reportItem.setMonth(cursor.getInt(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> comparisonOfIncomeExpense(String startDate, String endDate, String[] monthYearArr, String orderby, String type) {
        createMonthTempTable(monthYearArr);
        reportItemList = new ArrayList<>();
        Log.e(startDate + type, endDate);
        query = "select type,sum_amt, month, year from ( select type, sum(amount) sum_amt, month, year from Expense,  " +
                "ExpenseName n where expenseNameID = n.id and type = ? and date >= ? and date <= ? group by month,  " +
                "type  union  " +
                " select ?, 0.0, " + AppConstant.MONTH_TEMP_MONTH + ", " + AppConstant.MONTH_TEMP_YEAR + " from " + AppConstant.MONTH_TEMP_TABLE_NAME + " t where " +
                "  month not in (select month from Expense se,  ExpenseName n where  se.expenseNameID = n.id and t." + AppConstant.MONTH_TEMP_MONTH + " = se.month and t." + AppConstant.MONTH_TEMP_YEAR + " = se.year and type = ?   " +
                " and date >= ? and date <= ?)) " +
                "t group by month, type order by cast(" + AppConstant.MONTH_TEMP_YEAR + " as integer) " + orderby + ", cast(" + AppConstant.MONTH_TEMP_MONTH + " as integer) " + orderby;
        Log.e("query", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{type, startDate, endDate, type, type, startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setBalance(cursor.getDouble(1));
                    reportItem.setMonth(cursor.getInt(2));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public ReportItem profitAndLossAmt(String startDate, String endDate) {
        Log.e("process" + startDate, endDate);
        reportItem = new ReportItem();
        query = "select sum(subTotal) saleAmt, sum(discountAmt) salesDiscount, (select sum(taxAmt) from Sales where date >= ? and date <= ?  and taxType = 'Inclusive') as salesTax,  " +
                "(select sum(purchaseTempPrice * ifnull(purchaseTempQty, " +
                " (select sum(qty) from SalesDetail st where t.stockID = st.stockID  and st.salesID = s.id))) from SalesPurchaseTemp t where  type = 's' and detailID <> 0) as soldItemValue," +
                " (select sum(discountAmt)" +
                " from Purchase  where date >= ? and date <= ? ) as purchaseDiscount, (select  sum(taxAmt) from Purchase as p where date >=" +
                " ? and date <= ? and p.taxType = 'Exclusive') as purchaseTax, (select sum(subTotal) from Purchase p where date >=  ? and date <= ?) as totalPurchase" +
                " from Sales s" +
                " where date >= ? and date <= ?";

        //Log.e("query", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, startDate, endDate, startDate, endDate, startDate, endDate, startDate, endDate});
            if (cursor.moveToFirst()) {
                //Log.e("incur"+ startDate, endDate);
                reportItem.setTotalAmt(cursor.getDouble(0));//for sales total
                //Log.e("total sales", cursor.getDouble(0) + " d");
                reportItem.setAmount(cursor.getDouble(1));//for sales discount
                reportItem.setSalesTax(cursor.getDouble(2));//sales tax
                //Log.e("value", cursor.getDouble(3) + "");
                reportItem.setValue(cursor.getDouble(3));//sold item value
                reportItem.setBalance(cursor.getDouble(4));//purchase discount
                reportItem.setPurTax(cursor.getDouble(5));//purchase tax
                reportItem.setTotalPurchase(cursor.getDouble(6));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
            Log.e("process", "commit");
        }
        return reportItem;
    }

    public List<ReportItem> profitAndLossExpenseMgrAmt(String startDate, String endDate, String type) {
        reportItemList = new ArrayList<>();
        query = "select " + AppConstant.EXPENSE_NAME_NAME + ", " + AppConstant.EXPENSE_NAME_TYPE + ", sum(" + AppConstant.EXPENSE_AMOUNT + ") from " + AppConstant.EXPENSE_TABLE_NAME + ", " +
                AppConstant.EXPENSE_NAME_TABLE_NAME + " n where " + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_NAME_ID + " and " + AppConstant.EXPENSE_DATE + " >= ? and " +
                AppConstant.EXPENSE_DATE + " <= ? and " + AppConstant.EXPENSE_NAME_TYPE + " = ? group by n." + AppConstant.EXPENSE_NAME_ID;

        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, type});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(0));
                    reportItem.setType(cursor.getString(1));
                    reportItem.setAmount(cursor.getDouble(2));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public Double getExpMgrTotal(String startDate, String endDate, String type) {
        Double amt = 0.0;
        query = "select ifnull(sum(amount), 0.0) as total from Expense, ExpenseName e where type = ? and expenseNameID = e.id and date >= ? and date <= ? ";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{type, startDate, endDate});
            if (cursor.moveToFirst()) {
                amt = cursor.getDouble(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return amt;
    }

    public List<ReportItem> topTenSalesItemByCategory(long categoryID, String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select sum(" + AppConstant.SALES_DETAIL_QTY + ") as sum_qty, " + AppConstant.STOCK_NAME + " from " + AppConstant.SALES_DETAIL_TABLE_NAME + " d, " + AppConstant.SALES_TABLE_NAME +
                " s, " + AppConstant.STOCK_TABLE_NAME + " st where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DETAIL_STOCK_ID + " = st." +
                AppConstant.STOCK_ID + " and " + AppConstant.SALES_DETAIL_SALES_ID + " IN " + "(select " + AppConstant.STOCK_ID + " from " + AppConstant.STOCK_TABLE_NAME + " where " +
                AppConstant.STOCK_CATEGORY_ID + " = " + categoryID + ") and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ? group by " + AppConstant.SALES_DETAIL_STOCK_ID +
                " order by sum_qty desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                reportItem = new ReportItem();
                reportItem.setQty(cursor.getInt(0));
                reportItem.setName(cursor.getString(1));
                reportItemList.add(reportItem);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public void topTenPurchaseItemByCategory(long categoryID, String startDate, String endDate, int startLimit, int endLimit) {
        query = "select asum(" + AppConstant.PURCHASE_DETAIL_QTY + ") as sum_qty, " + AppConstant.STOCK_NAME + " from " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, " + AppConstant.PURCHASE_TABLE_NAME +
                " s, " + AppConstant.STOCK_TABLE_NAME + " st where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = st." +
                AppConstant.STOCK_ID + " and " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " IN " + "(select " + AppConstant.STOCK_ID + " from " + AppConstant.STOCK_TABLE_NAME + " where " +
                AppConstant.STOCK_CATEGORY_ID + " = " + categoryID + ") and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? group by " + AppConstant.PURCHASE_DETAIL_STOCK_ID +
                " order by sum_qty desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public List<ReportItem> topSalesByCategory(String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        //        query = "select sum(" + AppConstant.SALES_DETAIL_TOTAL + ") as sum_amt, " + AppConstant.STOCK_NAME + " from " + AppConstant.SALES_DETAIL_TABLE_NAME + " d, " + AppConstant.SALES_TABLE_NAME +
        //                " s, " + AppConstant.STOCK_TABLE_NAME + " st where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DETAIL_STOCK_ID + " = st."
        //                + AppConstant.STOCK_ID + " and " + AppConstant.SALES_DETAIL_STOCK_ID + " IN " + "(select " + AppConstant.STOCK_ID + " from " + AppConstant.STOCK_TABLE_NAME +
        //                " where " + AppConstant.STOCK_CATEGORY_ID + " = " + categoryID + ") and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ? group by " + AppConstant.SALES_DETAIL_STOCK_ID +
        //                " order by sum_amt desc limit " + startLimit + ", " + endLimit;

        query = "select sum(total) as salesDetailTotal, c.name from Sales s, SalesDetail, Stock st, Category c where date >= ? and date <= ? and salesID = s.id " +
                "and stockID = st.id and categoryID = c.id group by categoryID order by salesDetailTotal desc limit " + startLimit + "," + endLimit;
        //Log.e(startDate + " " + endDate, query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setBalance(cursor.getDouble(0));
                    reportItem.setName(cursor.getString(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> damageList(String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select sum(" + AppConstant.DAMAGE_STOCK_QTY + "), " + AppConstant.DAMAGE_DAY + ", " + AppConstant.DAMAGE_MONTH + ", " + AppConstant.DAMAGE_YEAR + ", " + AppConstant.STOCK_NAME +
                " from " + AppConstant.DAMAGE_TABLE_NAME + ", " + AppConstant.STOCK_TABLE_NAME + " s where " + AppConstant.DAMAGE_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " + AppConstant.DAMAGE_DATE +
                " >= ? and " + AppConstant.DAMAGE_DATE + " <= ? group by " + AppConstant.DAMAGE_DATE + ", " + AppConstant.DAMAGE_STOCK_ID + " order by " + AppConstant.DAMAGE_DATE + " desc limit "
                + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setQty(cursor.getInt(0));
                    reportItem.setDate(DateUtility.makeDateFormatWithSlash(cursor.getString(3), cursor.getString(2), cursor.getString(1)));
                    reportItem.setName(cursor.getString(4));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> lostList(String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select sum(stockQty), day, month, year, name from Lost, Stock s where stockID = s.id and date>=? and date <= ? group by date, stockID order by date desc limit "
                + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setQty(cursor.getInt(0));
                    reportItem.setDate(DateUtility.makeDateFormatWithSlash(cursor.getString(3), cursor.getString(2), cursor.getString(1)));
                    reportItem.setName(cursor.getString(4));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> lowStockList(int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select " + AppConstant.STOCK_NAME + ", " +
                    AppConstant.STOCK_INVENTORY_QTY + ", " +
                    AppConstant.STOCK_REORDER_QTY + ", " +
                    AppConstant.STOCK_INVENTORY_QTY + " - " + AppConstant.STOCK_REORDER_QTY + " as fulfillQty  " +
                "from " + AppConstant.STOCK_TABLE_NAME +
                " where " + AppConstant.STOCK_INVENTORY_QTY + " <= " + AppConstant.STOCK_REORDER_QTY +
                    " and " + AppConstant.STOCK_INVENTORY_QTY + " >= 0 " +
                " union select " + AppConstant.STOCK_NAME + ", " +
                    "0, " +
                    AppConstant.STOCK_REORDER_QTY + ", " +
                    AppConstant.STOCK_INVENTORY_QTY + " - " + AppConstant.STOCK_REORDER_QTY + " as fulfillQty " +
                "from " + AppConstant.STOCK_TABLE_NAME +
                " where " + AppConstant.STOCK_INVENTORY_QTY + " <= " + AppConstant.STOCK_REORDER_QTY +
                    " and " + AppConstant.STOCK_INVENTORY_QTY + " < 0 " +
                " order by " + AppConstant.STOCK_INVENTORY_QTY + " asc limit " + startLimit + ", " + endLimit;

        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setQty(cursor.getInt(1));
                    reportItem.setQty1(cursor.getInt(2));
                    reportItem.setName(cursor.getString(0));
                    reportItem.setTotalQty(cursor.getInt(3));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> stockBalance(int startLimit, int endLimit, Long categoryID) {
        reportItemList = new ArrayList<>();
        int totalQty = 0;
        query = "select st." + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_INVENTORY_QTY + ", (select ifnull(sum(" + AppConstant.SALES_DETAIL_QTY + "),0) as inProcessQty from " +
                AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.SALES_DETAIL_TABLE_NAME + " sd, " + AppConstant.DELIVERY_TABLE_NAME + " d where d." + AppConstant.DELIVERY_SALES_ID + " = s." +
                AppConstant.SALES_ID + " and sd." + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.DELIVERY_STATUS + " = 0 and " + AppConstant.SALES_DETAIL_STOCK_ID +
                " = st." + AppConstant.STOCK_ID + ") as inProcessQty, st." + AppConstant.STOCK_NAME + ", c." + AppConstant.CATEGORY_NAME + " as cName, round(" + AppConstant.STOCK_PURCHASE_PRICE + ",2) from " +
                AppConstant.STOCK_TABLE_NAME + " st, " + AppConstant.CATEGORY_TABLE_NAME + " c where " + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and c." +
                AppConstant.CATEGORY_ID + " = " + categoryID + " limit " + startLimit + ", " + endLimit;

        Log.e("quer", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setQty(cursor.getInt(1));
                    reportItem.setQty1(cursor.getInt(2));
                    totalQty = cursor.getInt(1) + cursor.getInt(2);
                    reportItem.setTotalQty(totalQty);
                    reportItem.setName(cursor.getString(3));
                    reportItem.setName1(cursor.getString(4));
                    reportItem.setBalance(cursor.getDouble(5) * totalQty);
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> receivableHeaderList(int startLimit, int endLimit, int token) {
        reportItemList = new ArrayList<>();
        //        query = "select max(s." + AppConstant.SALES_DATE + ") salesDate, max(o." + AppConstant.CUSTOMER_OUTSTANDING_DATE + ") outstandingDate, sum(s." + AppConstant.SALES_TOTAL_AMOUNT +
        //                ") salesTotal, c." + AppConstant.CUSTOMER_BALANCE + ", " + AppConstant.CUSTOMER_NAME + ", c." + AppConstant.CUSTOMER_ID + " from " + AppConstant.CUSTOMER_TABLE_NAME + " c, " +
        //                AppConstant.SALES_TABLE_NAME + " s left outer join " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " o on o." + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID +
        //                " where s." + AppConstant.SALES_CUSTOMER_ID + " = c." + AppConstant.CUSTOMER_ID + " and s." + AppConstant.SALES_BALANCE + " > 0 group by c." + AppConstant.CURRENCY_ID + " limit " + startLimit + ", " + endLimit;
        if (token == -1) {
            query = " select max(s.date) salesDate,last_outstanding_date, sum(s.totalAmount) salesTotal, c.balance, name, c.id from Customer c, " +
                    " Sales s,  (select s.customerID, max(o.date) last_outstanding_date from Sales s left join CustomerOutstanding o on o.salesID = s.id where s.balance > 0  group by s.customerID) as a" +
                    " where s.customerID = c.id  and s.balance > 0 and a.customerID = s.customerID and c.balance > -1 group by c.id order by c.balance desc limit " + startLimit + ", " + endLimit;
        } else if (token == 0) {
            query = " select max(s.date) salesDate,last_outstanding_date, sum(s.totalAmount) salesTotal, c.balance, name, c.id from Customer c, " +
                    " Sales s,  (select s.customerID, max(o.date) last_outstanding_date from Sales s left join CustomerOutstanding o on o.salesID = s.id where s.balance > 0  group by s.customerID) as a" +
                    " where s.customerID = c.id  and s.balance > 0 and a.customerID = s.customerID and c.balance = 0 group by c.id order by c.balance desc limit " + startLimit + ", " + endLimit;
        } else if (token == 1) {
            query = " select max(s.date) salesDate,last_outstanding_date, sum(s.totalAmount) salesTotal, c.balance, name, c.id from Customer c, " +
                    " Sales s,  (select s.customerID, max(o.date) last_outstanding_date from Sales s left join CustomerOutstanding o on o.salesID = s.id where s.balance > 0  group by s.customerID) as a" +
                    " where s.customerID = c.id  and s.balance > 0 and a.customerID = s.customerID and c.balance > 0 group by c.id order by c.balance desc limit " + startLimit + ", " + endLimit;

        }
        databaseReadTransaction(flag);
        String paymentDate;
        try {
            //            Log.e("query", query);
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setDate(DateUtility.makeDateFormatWithSlash(cursor.getString(0)));
                    paymentDate = cursor.getString(1);
                    if (paymentDate == "" || paymentDate == null) {
                        reportItem.setDate1("");
                    } else {
                        reportItem.setDate1(DateUtility.makeDateFormatWithSlash(cursor.getString(1)));
                    }
                    reportItem.setTotalAmt(cursor.getDouble(2));
                    reportItem.setBalance(cursor.getDouble(3));
                    reportItem.setName(cursor.getString(4));
                    reportItem.setId(cursor.getLong(5));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> payableHeaderList(int startLimit, int endLimit, int token) {
        reportItemList = new ArrayList<>();
        //        query = "select max(s." + AppConstant.SALES_DATE + ") purchaseDate, max(o." + AppConstant.SUPPLIER_OUTSTANDING_DATE + ") outstandingDate, sum(s." + AppConstant.PURCHASE_TOTAL_AMOUNT + ") purchaseTotal, c." +
        //                AppConstant.SALES_BALANCE + ", " + AppConstant.SUPPLIER_NAME + ", c." + AppConstant.SUPPLIER_ID + " from " + AppConstant.SUPPLIER_TABLE_NAME + " c, " + AppConstant.PURCHASE_TABLE_NAME +
        //                " s left outer join " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " o on o." + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID + " where s." +
        //                AppConstant.PURCHASE_SUPPLIER_ID + " = c." + AppConstant.SUPPLIER_ID + " and s." + AppConstant.PURCHASE_BALANCE + " > 0 group by c." + AppConstant.SUPPLIER_ID + " limit " + startLimit + ", " + endLimit;

        if (token == -1) {
            query = " select max(s.date) purchaseDate,last_outstanding_date, sum(s.total) purchaseTotal, c.balance, name, c.id from Supplier c, " +
                    " Purchase s,  (select s.supplierID, max(o.date) last_outstanding_date from Purchase s left join SupplierOutstanding o on o.purchaseID = s.id where s.balance > 0  group by s.supplierID) as a" +
                    " where s.supplierID = c.id  and s.balance > 0 and a.supplierID = s.supplierID and c.balance > -1 group by c.id order by c.balance desc limit " + startLimit + ", " + endLimit;
        } else if (token == 0) {
            query = " select max(s.date) purchaseDate,last_outstanding_date, sum(s.total) purchaseTotal, c.balance, name, c.id from Supplier c, " +
                    " Purchase s,  (select s.supplierID, max(o.date) last_outstanding_date from Purchase s left join SupplierOutstanding o on o.purchaseID = s.id where s.balance > 0  group by s.supplierID) as a" +
                    " where s.supplierID = c.id  and s.balance > 0 and a.supplierID = s.supplierID and c.balance = 0 group by c.id order by c.balance desc limit " + startLimit + ", " + endLimit;
        } else if (token == 1) {
            query = " select max(s.date) purchaseDate,last_outstanding_date, sum(s.total) purchaseTotal, c.balance, name, c.id from Supplier c, " +
                    " Purchase s,  (select s.supplierID, max(o.date) last_outstanding_date from Purchase s left join SupplierOutstanding o on o.purchaseID = s.id where s.balance > 0  group by s.supplierID) as a" +
                    " where s.supplierID = c.id  and s.balance > 0 and a.supplierID = s.supplierID and c.balance > 0 group by c.id order by c.balance desc limit " + startLimit + ", " + endLimit;
        }
        databaseReadTransaction(flag);
        String paymentDate;
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setDate(DateUtility.makeDateFormatWithSlash(cursor.getString(0)));
                    paymentDate = cursor.getString(1);
                    if (paymentDate == "" || paymentDate == null) {
                        reportItem.setDate1("");
                    } else {
                        reportItem.setDate1(DateUtility.makeDateFormatWithSlash(cursor.getString(1)));
                    }
                    reportItem.setTotalAmt(cursor.getDouble(2));
                    reportItem.setBalance(cursor.getDouble(3));
                    reportItem.setName(cursor.getString(4));
                    reportItem.setId(cursor.getLong(5));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> receivableDetailList(Long customerID, String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select " + AppConstant.SALES_VOUCHER_NUM + ", " + AppConstant.SALES_TOTAL_AMOUNT + ", (" + AppConstant.SALES_PAID_AMOUNT + " + ifnull((select sum(" + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID + "),0.0)) as totalPaidAmt, s." +
                AppConstant.SALES_DATE + ", " + AppConstant.SALES_ID + " from  " + AppConstant.SALES_TABLE_NAME + " s where " + AppConstant.SALES_BALANCE +
                " > 0 and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ? and " + AppConstant.SALES_CUSTOMER_ID + " = " + customerID + " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            Double balance;
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(0));
                    reportItem.setTotalAmt(cursor.getDouble(1));
                    reportItem.setAmount(cursor.getDouble(2));
                    balance = cursor.getDouble(1) - cursor.getDouble(2);
                    reportItem.setBalance(balance);
                    reportItem.setDate(DateUtility.makeDateFormatWithSlash(cursor.getString(3)));
                    reportItem.setId(cursor.getLong(4));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> payableDetailList(Long supplierID, String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select " + AppConstant.PURCHASE_VOUCHER_NUM + ", " + AppConstant.PURCHASE_TOTAL_AMOUNT + ", (" + AppConstant.PURCHASE_PAID_AMOUNT + " + ifnull((select sum(" + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID + "),0.0)) as totalPaidAmt, s." +
                AppConstant.PURCHASE_DATE + ", " + AppConstant.PURCHASE_ID + " from  " + AppConstant.PURCHASE_TABLE_NAME + " s where " + AppConstant.PURCHASE_BALANCE +
                " > 0 and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? and " + AppConstant.PURCHASE_SUPPLIER_ID + " = " + supplierID + " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            Double balance;
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(0));
                    reportItem.setTotalAmt(cursor.getDouble(1));
                    reportItem.setAmount(cursor.getDouble(2));
                    balance = cursor.getDouble(1) - cursor.getDouble(2);
                    reportItem.setBalance(balance);
                    reportItem.setDate(DateUtility.makeDateFormatWithSlash(cursor.getString(3)));
                    reportItem.setId(cursor.getLong(4));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> customerOutstandingSalesVoucherList(Long customerID, String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select " + AppConstant.SALES_VOUCHER_NUM + ", " + AppConstant.SALES_DATE + ", (" + AppConstant.SALES_PAID_AMOUNT + " + (select ifnull(sum(" + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                "),0.0) from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID + ")) as totalPaidAmt, s." +
                AppConstant.SALES_TOTAL_AMOUNT + ", s." + AppConstant.SALES_ID + ", " + AppConstant.SALES_TYPE + ", " + AppConstant.CUSTOMER_NAME + ", " + AppConstant.SALES_PAID_AMOUNT + " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME +
                " c where " + AppConstant.SALES_CUSTOMER_ID + " = " + customerID + " and s." + AppConstant.SALES_CUSTOMER_ID + " = c." + AppConstant.CUSTOMER_ID + " and s." + AppConstant.SALES_BALANCE +
                " > 0 and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ? order by " + AppConstant.SALES_DATE + "  asc, s." + AppConstant.SALES_ID + " asc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            Double balance;
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(0));
                    reportItem.setDate(DateUtility.makeDateFormatWithSlash(cursor.getString(1)));
                    reportItem.setTotalAmt(cursor.getDouble(2));
                    balance = cursor.getDouble(3) - cursor.getDouble(2);
                    reportItem.setBalance(balance);
                    Log.e("bal", balance.toString());
                    reportItem.setId(cursor.getLong(4));
                    reportItem.setType(cursor.getString(5));
                    reportItem.setName1(cursor.getString(6));
                    reportItem.setAmount(cursor.getDouble(7));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> supplierOutstandingPurchaseVoucherList(Long supplierID, String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        query = "select " + AppConstant.PURCHASE_VOUCHER_NUM + ", " + AppConstant.PURCHASE_DATE + ", (" + AppConstant.PURCHASE_PAID_AMOUNT + " + (select ifnull(sum(" + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                "),0.0) from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID + ")) as totalPaidAmt, s." +
                AppConstant.PURCHASE_TOTAL_AMOUNT + ", s." + AppConstant.PURCHASE_ID + ", " + AppConstant.SUPPLIER_NAME + ", " + AppConstant.PURCHASE_PAID_AMOUNT + " from " + AppConstant.PURCHASE_TABLE_NAME + " s," +
                AppConstant.SUPPLIER_TABLE_NAME + " c where " + AppConstant.PURCHASE_SUPPLIER_ID + " = " + supplierID + " and s." + AppConstant.PURCHASE_BALANCE +
                " > 0 and " + AppConstant.PURCHASE_SUPPLIER_ID + " = c." + AppConstant.SUPPLIER_ID + " and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? order by " + AppConstant.PURCHASE_DATE
                + " asc, s." + AppConstant.PURCHASE_ID + " asc  limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            Double balance;
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(0));
                    reportItem.setDate(DateUtility.makeDateFormatWithSlash(cursor.getString(1)));
                    reportItem.setTotalAmt(cursor.getDouble(2));
                    balance = cursor.getDouble(3) - cursor.getDouble(2);
                    reportItem.setBalance(balance);
                    reportItem.setId(cursor.getLong(4));
                    reportItem.setName1(cursor.getString(5));
                    reportItem.setAmount(cursor.getDouble(6));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> paymentListForSalesID(Long salesID) {
        reportItemList = new ArrayList<>();

        query = "select " + AppConstant.CUSTOMER_OUTSTANDING_INVOICE_NUM + ", " + AppConstant.CUSTOMER_OUTSTANDING_DATE + ", " + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                " from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = " + salesID;
        databaseReadTransaction(flag);
        try {
            Double balance;
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(0));
                    reportItem.setDate(DateUtility.makeDateFormatWithSlash(cursor.getString(1)));
                    reportItem.setAmount(cursor.getDouble(2));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> paymentListForPurchaseID(Long purchaseID) {
        reportItemList = new ArrayList<>();

        query = "select " + AppConstant.SUPPLIER_OUTSTANDING_INVOICE_NUM + ", " + AppConstant.SUPPLIER_OUTSTANDING_DATE + ", " + AppConstant.SUPPLIER_OUTSTANDING_PAID_AMOUNT +
                " from " + AppConstant.SUPPLIER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.SUPPLIER_OUTSTANDING_PURCHASE_ID + " = " + purchaseID;
        databaseReadTransaction(flag);
        try {
            Double balance;
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(0));
                    reportItem.setDate(DateUtility.makeDateFormatWithSlash(cursor.getString(1)));
                    reportItem.setAmount(cursor.getDouble(2));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> noOfProductCategoryBySupplier(String startDate, String endDate, int startLimit, int endLimit) {
        List<ReportItem> reportItemList = new ArrayList<>();
        query = "select count(distinct " + AppConstant.STOCK_CATEGORY_ID + ") noOfCategory, sup." + AppConstant.SUPPLIER_NAME + ", count(distinct s." + AppConstant.STOCK_ID + "), sup." + AppConstant.SUPPLIER_ID + " from " +
                AppConstant.STOCK_TABLE_NAME + " s, " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, " + AppConstant.SUPPLIER_TABLE_NAME + " sup where " +
                AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and " +
                AppConstant.PURCHASE_SUPPLIER_ID + " = sup." + AppConstant.SUPPLIER_ID + " and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? group by sup." +
                AppConstant.SUPPLIER_ID + " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            Log.e("query", query);
            Double balance;
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setQty(cursor.getInt(0));
                    reportItem.setName(cursor.getString(1));
                    reportItem.setQty1(cursor.getInt(2));
                    reportItem.setId(cursor.getLong(3));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            Log.e("sixe", reportItemList.size() + "");
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> productCategoryBySupplier(String startDate, String endDate, int startLimit, int endLimit, Long supplierID) {
        reportItemList = new ArrayList<>();
        query = "select distinct c." + AppConstant.CATEGORY_ID + ", c." + AppConstant.CATEGORY_NAME + ", " + AppConstant.PURCHASE_SUPPLIER_ID + " from " +
                AppConstant.STOCK_TABLE_NAME + " s, " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, " + AppConstant.CATEGORY_TABLE_NAME + " c where " +
                AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and " +
                AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and " + AppConstant.PURCHASE_SUPPLIER_ID + " = " + supplierID + " and "
                + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? " + " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(1));
                    reportItem.setId(cursor.getLong(0));
                    reportItem.setId1(cursor.getLong(2));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public List<ReportItem> productBySupplierCategory(String startDate, String endDate, int startLimit, int endLimit, Long supplierID, Long categoryID) {
        reportItemList = new ArrayList<>();
        query = "select distinct s." + AppConstant.STOCK_ID + ", s." + AppConstant.CATEGORY_NAME + ", " + AppConstant.STOCK_CODE_NUM + " from " +
                AppConstant.STOCK_TABLE_NAME + " s, " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d where " +
                AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and " +
                AppConstant.STOCK_CATEGORY_ID + " = " + categoryID + " and " + AppConstant.PURCHASE_SUPPLIER_ID + " = " + supplierID + " and "
                + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? " + " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(1));
                    reportItem.setId(cursor.getLong(0));
                    reportItem.setName1(cursor.getString(2));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return reportItemList;
    }

    public void topTenPurchaseByCategory(long categoryID, String startDate, String endDate, int startLimit, int endLimit) {
        query = "select sum(" + AppConstant.PURCHASE_DETAIL_TOTAL + ") as sum_amt, " + AppConstant.STOCK_NAME + " from " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, " + AppConstant.PURCHASE_TABLE_NAME +
                " s, " + AppConstant.STOCK_TABLE_NAME + " st where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = s." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = st."
                + AppConstant.STOCK_ID + " and " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " IN " + "(select " + AppConstant.STOCK_ID + " from " + AppConstant.STOCK_TABLE_NAME +
                " where " + AppConstant.STOCK_CATEGORY_ID + " = " + categoryID + ") and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? group by " + AppConstant.PURCHASE_DETAIL_STOCK_ID +
                " order by sum_amt desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void saleTaxReport(String startDate, String endDate) {
        query = "select " + AppConstant.TAX_NAME + ", sum(" + AppConstant.SALES_TAX_AMOUNT + ") from " + AppConstant.SALES_TABLE_NAME + ", " + AppConstant.TAX_TABLE_NAME + " t where " +
                AppConstant.SALES_TAX_ID + " = t." + AppConstant.TAX_ID + " and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ? group by " + AppConstant.SALES_TAX_ID;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void sumOfExpenseAmt(String startDate, String endDate, String type) {
        query = "select sum(" + AppConstant.EXPENSE_AMOUNT + ") from " + AppConstant.EXPENSE_TABLE_NAME + " e, " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where "
                + AppConstant.EXPENSE_EXPENSE_NAME_ID + " = n." + AppConstant.EXPENSE_NAME_ID + " and " + AppConstant.EXPENSE_NAME_TYPE + " = ? and " + AppConstant.EXPENSE_DATE + " >= and " +
                AppConstant.EXPENSE_DATE + " <= ?;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{type, startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void deliveryCharges(String startDate, String endDate) {
        query = "select sum(" + AppConstant.DELIVERY_CHARGES + ") from " + AppConstant.DELIVERY_TABLE_NAME + " where " + AppConstant.DELIVERY_DATE + " >= ? and " +
                AppConstant.DELIVERY_DATE + " <= ?;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /*public boolean createPurchasePriceTempTable(String startDate, String endDate){
        query = "select " + AppConstant.BUSINESS_SETTING_VALUATION_METHOD + " from " + AppConstant.BUSINESS_SETTING_TABLE_NAME ;
        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()){
                String valuationMethod = cursor.getString(0);
                Log.e("value", valuationMethod);
                if (valuationMethod.equalsIgnoreCase("AVG")) {
                    query = " CREATE TEMP TABLE if not exists PurchasePriceTemp AS " +
                            " select " + AppConstant.PURCHASE_DETAIL_STOCK_ID + ", round(sum(" + AppConstant.PURCHASE_DETAIL_QTY + " * " + AppConstant.PURCHASE_DETAIL_PRICE + ")/sum(" +
                            AppConstant.PURCHASE_DETAIL_QTY + "), 2) as price, " + AppConstant.STOCK_NAME + " from " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + ", " + AppConstant.PURCHASE_TABLE_NAME +
                            " p, " + AppConstant.STOCK_TABLE_NAME + " s where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID +
                            " and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? and " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID +
                            " group by " + AppConstant.PURCHASE_DETAIL_STOCK_ID +
                            " union " +
                            " select distinct " + AppConstant.STOCK_ID + ", " + AppConstant.STOCK_PURCHASE_PRICE + ", " + AppConstant.STOCK_NAME + " from " + AppConstant.STOCK_TABLE_NAME + " s where not exists " +
                            " (select * from " + AppConstant.PURCHASE_DETAIL_TABLE_NAME + " d, " + AppConstant.PURCHASE_TABLE_NAME + " p where " +
                            "d." + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and d." + AppConstant.PURCHASE_DETAIL_STOCK_ID + " = s." + AppConstant.STOCK_ID +
                            " and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ?);";
                    database.execSQL(query, new String[]{startDate, endDate, startDate, endDate});
                } else if (valuationMethod.equalsIgnoreCase("LIFO")){
                    query = "CREATE TEMP TABLE if not exists PurchasePriceTemp AS " +
                            " select stockID, price, name from Purchase p, PurchaseDetail d, Stock s where " +
                            " purchaseID = p.id and stockID = s.id and date >= ? and  date <= ? group by stockID having max(time)" +
                            " union " +
                            " select distinct id, purchasePrice, name from Stock s where not exists " +
                            " (select * from PurchaseDetail d, Purchase p where " +
                            " d.purchaseID = p.id and d.stockID = s.id and date >= ? and  date <= ?)";
                    database.execSQL(query, new String[]{startDate, endDate, startDate, endDate});
                } else {
                    query = "CREATE TEMP TABLE if not exists PurchasePriceTemp AS " +
                            " select stockID, price, name from Purchase p, PurchaseDetail d, Stock s where " +
                            " purchaseID = p.id and stockID = s.id and date >= ? and  date <= ? group by stockID having min(time)" +
                            " union " +
                            " select distinct id, purchasePrice, name from Stock s where not exists " +
                            " (select * from PurchaseDetail d, Purchase p where " +
                            " d.purchaseID = p.id and d.stockID = s.id and date >= ? and  date <= ?)";
                    database.execSQL(query, new String[]{startDate, endDate, startDate, endDate});
                }
            }
            database.setTransactionSuccessful();
        }catch (SQLiteException e){
            e.printStackTrace();
        }finally {
            database.endTransaction();
            if (cursor != null){
                cursor.close();
            }
        }
        return flag.isInserted();
    }

    public boolean dropPurchasePriceTempTable(){
        query = "drop table if exists PurchasePriceTemp;"  ;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        }catch (SQLiteException e){
            e.printStackTrace();
        }finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }*/


    public void costOfGoodsSold(String startDate, String endDate) {
        query = "select (" + AppConstant.STOCK_INVENTORY_QTY + " - sum(qty)) * ifnull((select price from PurchasePriceTemp where stockID = s." + AppConstant.STOCK_ID + "), " +
                "(select " + AppConstant.STOCK_PURCHASE_PRICE + " from " + AppConstant.STOCK_TABLE_NAME + " where " + AppConstant.STOCK_ID + " = s." + AppConstant.STOCK_ID + ")) closing_stock_bal, stockID, name from (" +
                "select " + AppConstant.PURCHASE_DETAIL_QTY + ", " + AppConstant.PURCHASE_DETAIL_STOCK_ID + " from " + AppConstant.PURCHASE_TABLE_NAME + " p, " + AppConstant.PURCHASE_DETAIL_TABLE_NAME +
                " where " + AppConstant.PURCHASE_DETAIL_PURCHASE_ID + " = p." + AppConstant.PURCHASE_ID + " and " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE + " <= ? group by " +
                AppConstant.PURCHASE_DETAIL_STOCK_ID + " union all " +
                "select -" + AppConstant.SALES_DETAIL_QTY + ", " + AppConstant.SALES_DETAIL_STOCK_ID + " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.SALES_DETAIL_TABLE_NAME +
                " where " + AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ? group by " +
                AppConstant.SALES_DETAIL_STOCK_ID + " union all " +
                "select -" + AppConstant.DAMAGE_STOCK_QTY + ", " + AppConstant.DAMAGE_STOCK_ID + " from " + AppConstant.DAMAGE_TABLE_NAME +
                " where " + AppConstant.DAMAGE_DATE + " >= ? and " + AppConstant.DAMAGE_DATE + " <= ? group by " +
                AppConstant.DAMAGE_STOCK_ID + " union all " +
                "select -" + AppConstant.LOST_STOCK_QTY + ", " + AppConstant.LOST_STOCK_ID + " from " + AppConstant.LOST_TABLE_NAME + " where " +
                AppConstant.LOST_DATE + " >= ? and " + AppConstant.LOST_DATE + " <= ? group by " +
                AppConstant.LOST_STOCK_ID + ")as q, " + AppConstant.STOCK_TABLE_NAME + " s where " +
                "s." + AppConstant.SALES_ID + " = q.stockID group by stockID";

        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, startDate, endDate, startDate, endDate, startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void saleAmt(String startDate, String endDate) {
        query = "select " + AppConstant.SALES_DETAIL_STOCK_ID + ", " + AppConstant.SALES_DETAIL_QTY + " * ifnull((select price from PurchasePriceTemp where stockID = d." + AppConstant.SALES_DETAIL_STOCK_ID +
                "), (select " + AppConstant.STOCK_PURCHASE_PRICE + " from " + AppConstant.STOCK_TABLE_NAME + ") as total_price from " + AppConstant.SALES_DETAIL_TABLE_NAME + " d, " + AppConstant.SALES_TABLE_NAME + " s where " +
                AppConstant.SALES_DETAIL_SALES_ID + " = s." + AppConstant.SALES_ID + " and " + AppConstant.SALES_DATE + " >= ? and " + AppConstant.SALES_DATE + " <= ? " +
                "group by " + AppConstant.SALES_DETAIL_STOCK_ID;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void purchaseDiscount(String startDate, String endDate) {
        query = "select sum(" + AppConstant.PURCHASE_DISCOUNT_AMOUNT + ") from " + AppConstant.PURCHASE_TABLE_NAME + " where " + AppConstant.PURCHASE_DATE + " >= ? and " + AppConstant.PURCHASE_DATE
                + " <= ?;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void niveaStand(String startDate, String endDate) {
        query = "select sum(" + AppConstant.EXPENSE_AMOUNT + ") from " + AppConstant.EXPENSE_TABLE_NAME + " e, " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where " + AppConstant.EXPENSE_EXPENSE_NAME_ID +
                " = e." + AppConstant.EXPENSE_NAME_ID + " and " + AppConstant.EXPENSE_NAME_TYPE + " = ? and " + AppConstant.EXPENSE_DATE + " >= ? and " + AppConstant.EXPENSE_DATE + " <= ?;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{ExpenseIncome.Type.Income.toString(), startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void saleDiscount(String startDate, String endDate) {
        query = "select sum(" + AppConstant.SALES_DISCOUNT_AMOUNT + ") from " + AppConstant.SALES_TABLE_NAME + " where " + AppConstant.SALES_DATE + " >= ? and " +
                AppConstant.SALES_DATE + " <= ?;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void saleTax(String startDate, String endDate) {
        query = "select sum(" + AppConstant.SALES_TAX_AMOUNT + ") from " + AppConstant.SALES_TABLE_NAME + ", " + AppConstant.TAX_TABLE_NAME + " t where " + AppConstant.SALES_TAX_ID +
                " = t." + AppConstant.TAX_ID + " and t." + AppConstant.TAX_TYPE + " = ? and " + AppConstant.SALES_DATE + ">= ? and " + AppConstant.SALES_DATE + "<= ?";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{"Exclusive", startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void purchaseTax(String startDate, String endDate) {
        query = "select sum(" + AppConstant.PURCHASE_TAX_AMOUNT + ") from " + AppConstant.PURCHASE_TABLE_NAME + ", " + AppConstant.TAX_TABLE_NAME + " t where " + AppConstant.PURCHASE_TAX_ID +
                " = t." + AppConstant.TAX_ID + " and t." + AppConstant.TAX_TYPE + " = ? and " + AppConstant.PURCHASE_DATE + ">= ? and " + AppConstant.PURCHASE_DATE + "<= ?";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{"Exclusive", startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void transportation(String startDate, String endDate) {
        query = "select sum(" + AppConstant.EXPENSE_AMOUNT + ") from " + AppConstant.EXPENSE_TABLE_NAME + " e, " + AppConstant.EXPENSE_NAME_TABLE_NAME + " n where " + AppConstant.EXPENSE_EXPENSE_NAME_ID +
                " = e." + AppConstant.EXPENSE_NAME_ID + " and " + AppConstant.EXPENSE_NAME_TYPE + " = ? and " + AppConstant.EXPENSE_DATE + " >= ? and " + AppConstant.EXPENSE_DATE + " <= ?;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{ExpenseIncome.Type.Expense.toString(), startDate, endDate});
            if (cursor.moveToFirst()) {

            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public List<ReportItem> monthlySaleBySaleStaff(String startDate, String endDate, Long userId) {
        reportItemList = new ArrayList<>();

        database = databaseHelper.getReadableDatabase();

        database.execSQL("ATTACH '" + db_users + "' AS users_db;" );

        String usersTable = "users_db." + AppConstant.TABLE_USER_ROLES;

//        query = " SELECT total_amount, user_name, month, year from (" +
//                " SELECT SUM(" + AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_SUB_TOTAL + ") total_amount, " +
//                    AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_MONTH + ", " +
//                    AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_YEAR + ", " +
//                    usersTable + "." + AppConstant.COLUMN_USER_NAME + " user_name" +
//                " FROM " + AppConstant.SALES_TABLE_NAME +
//                " JOIN " + usersTable + " ON " + AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_USER_ID + " = " + usersTable + "." + AppConstant.COLUMN_USER_ID +
//                " WHERE " +
//                    AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_DATE + " >= ? AND " +
//                    AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_DATE + " <= ? AND " +
//                    AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_USER_ID + " = 1" +
//                " GROUP BY " + AppConstant.SALES_MONTH +
//                " UNION" +
//                " SELECT 0, " +
//                    AppConstant.MONTH_TEMP_MONTH + ", " +
//                    AppConstant.MONTH_TEMP_YEAR + ", " +
//                    "NULL" +
//                " FROM " + AppConstant.MONTH_TEMP_TABLE_NAME + " t" +
//                " WHERE NOT EXISTS( " +
//                    " SELECT " + AppConstant.SALES_MONTH +
//                    " FROM " + AppConstant.SALES_TABLE_NAME + " s " +
//                    " WHERE s." + AppConstant.SALES_MONTH + " = t." + AppConstant.MONTH_TEMP_MONTH +
//                    " AND " +
//                    AppConstant.SALES_DATE + " >= ? AND " + AppConstant.SALES_DATE + " <= ?))" +
//                "ORDER BY " +
//                    "CAST(" + AppConstant.MONTH_TEMP_YEAR + " AS INTEGER) ASC, " +
//                    "CAST(" + AppConstant.MONTH_TEMP_MONTH + " AS INTEGER) ASC;";
        query = " SELECT SUM(" + AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_SUB_TOTAL + ") total_amount, " +
                    AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_MONTH + ", " +
                    usersTable + "." + AppConstant.COLUMN_USER_NAME + " user_name" +
                " FROM " + AppConstant.SALES_TABLE_NAME +
                " JOIN " + usersTable + " ON " + AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_USER_ID + " = " + usersTable + "." + AppConstant.COLUMN_USER_ID +
                " WHERE " +
                    AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_DATE + " >= ? AND " +
                    AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_DATE + " <= ? AND " +
                    AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_USER_ID + " = " + userId +
                " GROUP BY " + AppConstant.SALES_MONTH;

        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setTotalAmt(cursor.getDouble(0));
                    reportItem.setMonth(cursor.getInt(1));
                    reportItem.setName(cursor.getString(2));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            database.execSQL("DETACH users_db");
        }

        return reportItemList;
    }

    public List<ReportItem> salesBySaleStaff(String startDate, String endDate, int startLimit, int endLimit) {
        reportItemList = new ArrayList<>();
        database = databaseHelper.getReadableDatabase();

        database.execSQL("ATTACH '" + db_users + "' AS users_db;" );

        String usersTable = "users_db." + AppConstant.TABLE_USER_ROLES;

        query = " SELECT SUM(" + AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_SUB_TOTAL + ") total_amount, " +
                usersTable + "." + AppConstant.COLUMN_USER_NAME + " user_name" +
                " FROM " + AppConstant.SALES_TABLE_NAME +
                " JOIN " + usersTable + " ON " + AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_USER_ID + " = " + usersTable + "." + AppConstant.COLUMN_USER_ID +
                " WHERE " +
                AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_DATE + " >= ? AND " +
                AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_DATE + " <= ? " +
                " GROUP BY user_name" +
                " LIMIT " + startLimit + ", " + endLimit;

        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setBalance(cursor.getDouble(0));
                    reportItem.setName(cursor.getString(1));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            database.execSQL("DETACH users_db");
        }

        return reportItemList;
    }


    // WIP WIP WIP
    public List<ReportItem> salesDetailBySaleStaff(String startDate, String endDate, int startLimit, int endLimit, Long userId) {
        reportItemList = new ArrayList<>();
        database = databaseHelper.getReadableDatabase();

        database.execSQL("ATTACH '" + db_users + "' AS users_db;" );

        String usersTable = "users_db." + AppConstant.TABLE_USER_ROLES;

        /*
         SELECT Stock.name, Sum(SalesDetail.qty) as qty, Sum(SalesDetail.price) as amount
         FROM SalesDetail
         JOIN Sales ON SalesDetail.salesID = Sales.id
         JOIN Stock ON SalesDetail.stockID = Stock.id
         WHERE Sales.customField2 = 1
         GROUP BY  Stock.id
         */
        query = " SELECT " + AppConstant.STOCK_TABLE_NAME + "." + AppConstant.STOCK_NAME + ", " +
                    "Sum(" + AppConstant.SALES_DETAIL_TABLE_NAME + "." + AppConstant.SALES_DETAIL_QTY +") as qty, " +
                    "Sum(" + AppConstant.SALES_DETAIL_TABLE_NAME + "." + AppConstant.SALES_DETAIL_QTY +") * " + AppConstant.SALES_DETAIL_TABLE_NAME + "." + AppConstant.SALES_DETAIL_PRICE +" as amount" +
                " FROM " + AppConstant.SALES_DETAIL_TABLE_NAME +
                " JOIN " + AppConstant.SALES_TABLE_NAME + " ON " + AppConstant.SALES_DETAIL_TABLE_NAME + "." + AppConstant.SALES_DETAIL_SALES_ID +" = " + AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_ID +
                " JOIN " + AppConstant.STOCK_TABLE_NAME +" ON " + AppConstant.SALES_DETAIL_TABLE_NAME + "." + AppConstant.SALES_DETAIL_STOCK_ID +" = " + AppConstant.STOCK_TABLE_NAME + "." + AppConstant.STOCK_ID +
                " WHERE " + AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_USER_ID + " = " + userId + " AND " +
                AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_DATE + " >= ? AND " +
                AppConstant.SALES_TABLE_NAME + "." + AppConstant.SALES_DATE + " <= ? " +
                " GROUP BY  Stock.id" +
                " LIMIT " + startLimit + ", " + endLimit;

        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    reportItem = new ReportItem();
                    reportItem.setName(cursor.getString(0));
                    reportItem.setTotalQty(cursor.getInt(1));
                    reportItem.setTotalAmt(cursor.getDouble(2));
                    reportItemList.add(reportItem);
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            database.execSQL("DETACH users_db");
        }

        return reportItemList;
    }
}
