package com.digitalfusion.android.pos.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.util.POSUtil;

public class SubscriptionActivity extends ParentActivity {
    private Toolbar toolbar;
    private LinearLayout pinLinearLayout;
    private LinearLayout codeLinearLayout;
    private CheckBox userPormoCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(POSUtil.getDefaultThemeNoActionBar(this));
        setContentView(R.layout.activity_subscription);
        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle("Subscription");

        pinLinearLayout = (LinearLayout) findViewById(R.id.pin_ll);

        codeLinearLayout = (LinearLayout) findViewById(R.id.promo);

        userPormoCheckBox = (CheckBox) findViewById(R.id.promo_check_box);


        userPormoCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    pinLinearLayout.setVisibility(View.GONE);
                    codeLinearLayout.setVisibility(View.VISIBLE);

                } else {
                    pinLinearLayout.setVisibility(View.VISIBLE);
                    codeLinearLayout.setVisibility(View.GONE);

                }

            }
        });

        findViewById(R.id.view_history_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubscriptionActivity.this, SubscriptionHistoryActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == android.R.id.home) {

            super.onBackPressed();

            Log.w("here ComplainFActivity", "OnItem Selected here");
        }


        return false;
    }
}
