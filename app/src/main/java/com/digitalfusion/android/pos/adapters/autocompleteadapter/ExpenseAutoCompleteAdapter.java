package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.ExpenseManager;
import com.digitalfusion.android.pos.database.model.ExpenseName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 9/7/16.
 */
public class ExpenseAutoCompleteAdapter extends ArrayAdapter<ExpenseName> {


    List<ExpenseName> suggestion;

    List<ExpenseName> searchList;

    Context context;

    ExpenseManager expenseManager;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((ExpenseName) resultValue).getName();


            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            searchList.clear();

            if (constraint != null) {

                searchList = expenseManager.expenseNamesSearch(0, 10, constraint.toString());
                FilterResults filterResults = new FilterResults();
                filterResults.values = searchList;
                filterResults.count = searchList.size();

                Log.w("heelo", searchList.size() + " sss");

                return filterResults;

            } else {
                FilterResults filterResults = new FilterResults();
                return filterResults;
            }


        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<ExpenseName> filterList = (ArrayList<ExpenseName>) results.values;
            suggestion = searchList;
            if (results != null && results.count > 0) {
                clear();
                for (ExpenseName vehicle1 : filterList) {
                    add(vehicle1);
                    notifyDataSetChanged();
                }
            }
        }
    };

    public ExpenseAutoCompleteAdapter(Context context, ExpenseManager expenseManager) {

        super(context, -1);

        this.context = context;

        suggestion = new ArrayList<>();

        this.expenseManager = expenseManager;

        searchList = new ArrayList<>();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.expense_auto_complete_suggest_view, parent, false);
        }

        viewHolder = new ViewHolder(convertView);


        if (suggestion.size() > 0) {
            viewHolder.expenseName.setText(suggestion.get(position).getName());
        }


        return convertView;
    }

    @Override
    public int getCount() {
        return suggestion.size();
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    public List<ExpenseName> getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(List<ExpenseName> suggestion) {
        this.suggestion = suggestion;
    }

    static class ViewHolder {
        TextView expenseName;

        public ViewHolder(View itemView) {
            this.expenseName = (TextView) itemView.findViewById(R.id.expense_name);
        }
    }
}
