package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.digitalfusion.android.pos.R;

/**
 * Created by MD003 on 10/30/17.
 */

public class LoaderViewHolder extends RecyclerView.ViewHolder {

    public ProgressBar mProgressBar;

    public View mView;

    public LoaderViewHolder(View itemView) {
        super(itemView);

        mView = itemView;

        mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);

    }
}
