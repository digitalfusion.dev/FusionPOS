package com.digitalfusion.android.pos.fragments.inventoryfragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForCategoryMD;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForImageList;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForUnitMD;
import com.digitalfusion.android.pos.database.business.CategoryManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.business.UnitManager;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.database.model.StockImage;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.database.model.Unit;
import com.digitalfusion.android.pos.util.GrantPermission;
import com.digitalfusion.android.pos.util.ImagePicker;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.TypefaceSpan;
import com.digitalfusion.android.pos.views.FusionToast;
import com.example.imagedialogswipe.ImageDialogSwipe;
import com.google.zxing.client.android.CaptureActivity;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 8/17/16.
 */
public class StockFormFragment extends Fragment {

    private static final int REQUEST_SELECT_PICTURE = 0x01;
    private final int SCANNER_BAR_CODE = 10;
    //For stock form view
    private View mainLayoutView;
    private ImageView imageView;
    private TextView retailSalePercentTextView;
    private TextView wholeSalePercentTextView;
    private LinearLayout generalLinearLayoutBtn;
    private ImageButton priceImageButton;
    private ImageButton generalImageButton;
    private LinearLayout priceLinearLayoutBtn;
    private LinearLayout generalLinearLayout;
    private LinearLayout priceLinearLayout;
    private EditText stockCodeEditText;
    private EditText barCodeEditText;
    private EditText itemNameEditText;
    private TextView categoryTextView;
    private EditText inventoryQtyEditText;
    private EditText reorderLevelEditText;
    private EditText purchasePriceEditText;
    private TextView unitTextView;
    private EditText wholeSaleEditText;
    private EditText retailPriceEditText;
    private ImageButton addNewCategoryBtn;
    private ImageButton addNewUnitBtn;
    private EditText stockDescriptionEditText;
    //for stock from value
    private String stockCode;
    private String barCode;
    private String itemName;
    private int inventoryQty = 0;
    private int reorderLevel = 0;
    private Double purchasePrice = 0.0;
    private Double wholeSalePrice = 0.0;
    private Double wholeSalePricePercent = 0.0;
    private Double normalSalePrice = 0.0;
    private Double normalSalePricePercent = 0.0;
    private Double retailPrice = 0.0;
    private Double retailPricePercent = 0.0;
    private String description;
    private StockManager stockManager;
    private Context context;
    private MaterialDialog addNewCategoryMaterialDialog;
    private MaterialDialog addNewUnitMaterialDialog;
    //views from addNewUnitDialog
    private EditText unitNameTextInputEditText;
    private EditText unitDescriptionTextInputEditText;
    private Button unitSaveBtn, unitCancelBtn;
    //views from addNewCategoryDialog
    private EditText categoryNameTextInputEditText;
    private EditText categoryDescriptionTextInputEditText;
    private Button categorySaveBtn, categoryCancelBtn;
    //value for AddnewUnitDialog
    private String unitName;
    private String unitDescription;
    //value for AddnewCategoryDialog
    private String categoryName;
    private String categoryDescription;
    private CategoryManager categoryManager;
    private UnitManager unitManager;
    //For category list dialog
    private List<Category> categoryList;
    private RVAdapterForCategoryMD rvAdapterForCategoryMD;
    private MaterialDialog categoryChooserMD;
    private MaterialDialog unitChooserMD;
    private List<Unit> unitList;
    private RVAdapterForUnitMD rvAdapterForUnitMD;
    private Long unitId;
    private Long categoryId = 1l;
    private StockItem stockItem;
    private boolean isEdit = false;
    private byte[] stockIcon;
    private RVAdapterForImageList rvAdapterForImageList;
    private List<StockImage> stockImageList;
    private List<StockImage> newStockImageList;
    private List<Long> deleteImgIDList;
    private GrantPermission grantPermission;
    private MaterialDialog imageMaterialDialog;
    private ImageView viewImage;
    private ImageButton barcodeBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.stock_form_fragment_new, null);

        context = getContext();

        setHasOptionsMenu(true);

        deleteImgIDList = new ArrayList<>();

        newStockImageList = new ArrayList<>();

        grantPermission = new GrantPermission(this);

        categoryManager = new CategoryManager(context);

        unitManager = new UnitManager(context);

        stockManager = new StockManager(context);

        buildCategoryChooserDialog();

        buildUnitChooserDialog();

        stockImageList = new ArrayList<>();

        MainActivity.setCurrentFragment(this);

        rvAdapterForImageList = new RVAdapterForImageList(stockImageList);

        buildAddNewCategoryDialog();

        buildAddNewUnitDialog();

        loadUI();

        addUnitSpinner();

        //categoryTextView.setText("Default Category");

        stockImageList = new ArrayList<>();

        RecyclerView listView = (RecyclerView) mainLayoutView.findViewById(R.id.img_list);

        LinearLayoutManager ll = new LinearLayoutManager(context);

        ll.setOrientation(LinearLayoutManager.HORIZONTAL);

        listView.setLayoutManager(ll);

        listView.setAdapter(rvAdapterForImageList);

        rvAdapterForImageList.setDeletClickListener(new RVAdapterForImageList.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                deleteImgIDList.add(stockImageList.get(position).getId());

                if (newStockImageList.contains(stockImageList.get(position))) {

                    newStockImageList.remove(position);

                }

                stockImageList.remove(position);

                refreshRecyclerView();

            }
        });

        mainLayoutView.setOnTouchListener(rvAdapterForImageList);

        rvAdapterForImageList.setLongClickEditModeOn(true);

        settingOnClick();

        textChangeListener();

        generalLinearLayoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (generalLinearLayout.isShown()) {

                    generalLinearLayout.setVisibility(View.GONE);

                    generalImageButton.setSelected(true);

                } else {

                    generalImageButton.setSelected(false);

                    generalLinearLayout.setVisibility(View.VISIBLE);

                }

            }
        });

        priceLinearLayoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (priceLinearLayout.isShown()) {

                    priceLinearLayout.setVisibility(View.GONE);

                    priceImageButton.setSelected(true);

                } else {

                    priceImageButton.setSelected(false);

                    priceLinearLayout.setVisibility(View.VISIBLE);

                }
            }
        });

        stockCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                stockCodeValidation();

            }
        });

        if (getArguments() != null && getArguments().containsKey("stock")) {

            TypedArray editStock = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_stock});
            stockItem = (StockItem) getArguments().getSerializable("stock");
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(editStock.getString(0));
        } else {

            TypedArray newStock = context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_stock});
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(newStock.getString(0));
        }


        if (stockItem != null) {

            isEdit = true;

            assigningOldValue();

        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (grantPermission.requestPermissions()) {

                    onPickImage();

                }
            }
        });


        rvAdapterForImageList.setmClickListener(new RVAdapterForImageList.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                List<byte[]> imageList = new ArrayList<>();

                for (StockImage stockImage : stockImageList) {
                    imageList.add(stockImage.getImage());
                }


                ImageDialogSwipe imageDialogSwipe = new ImageDialogSwipe(imageList);

                imageDialogSwipe.setCurrentPos(position);

                imageDialogSwipe.show(getFragmentManager(), "swipe");

            }
        });


        return mainLayoutView;
    }


    @Override
    public void onResume() {

        super.onResume();

        if (barcodeBtn != null) {

            barcodeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.w("herer", "Bar code onclick");
                    checkPermission();
                    // Here, thisActivity is the current activity
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_GRANTED) {


                        Intent intent = new Intent(getContext(), CaptureActivity.class);
                        intent.setAction("com.google.zxing.client.android.SCAN");
                        intent.putExtra("SAVE_HISTORY", false);
                        startActivityForResult(intent, SCANNER_BAR_CODE);
                    }
                }
            });
        }
    }

    public void checkPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the requestPermissions.

            } else {

                // No explanation needed, we can request the requestPermissions.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA}, 200);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    private boolean stockCodeValidation() {

        boolean flag = true;

        if (!isEdit) {

            if (stockManager.checkStockCodeExists(stockCodeEditText.getText().toString().trim())) {
                TypedArray stockCodeAlreadyExist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.stock_code_already_exist});
                stockCodeEditText.setError(stockCodeAlreadyExist.getString(0));


                flag = false;

            } else {

                stockCodeEditText.setError(null);

                flag = true;
            }
        } else {

            if (!stockItem.getCodeNo().equalsIgnoreCase(stockCodeEditText.getText().toString().trim())) {

                if (stockManager.checkStockCodeExists(stockCodeEditText.getText().toString().trim())) {

                    TypedArray stockCodeAlreadyExist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.stock_code_already_exist});
                    stockCodeEditText.setError(stockCodeAlreadyExist.getString(0));

                    flag = false;

                } else {

                    stockCodeEditText.setError(null);

                    flag = true;

                }
            }

        }

        return flag;
    }


    public boolean checkVaildationForAddNewCategoryDialog() {

        boolean status = true;

        categoryName = categoryNameTextInputEditText.getText().toString().trim();

        categoryDescription = categoryDescriptionTextInputEditText.getText().toString().trim();

        if (categoryNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;

            TypedArray pleaseEnterCategoryName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_category_name});
            categoryNameTextInputEditText.setError(pleaseEnterCategoryName.getString(0));

        } else {
            if (categoryManager.checkNameExist(categoryName)) {

                TypedArray categoryAlreadyExist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.category_already_exist});
                categoryNameTextInputEditText.setError(categoryAlreadyExist.getString(0));

                status = false;

            }
        }

        return status;

    }

    public boolean checkVaildationForAddNewUnitDialog() {

        boolean status = true;

        unitName = unitNameTextInputEditText.getText().toString().trim();

        unitDescription = unitDescriptionTextInputEditText.getText().toString().trim();

        if (unitNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;

            TypedArray pleaseEnterUnitName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_unit_name});

            unitNameTextInputEditText.setError(pleaseEnterUnitName.getString(0));

        } else {

            if (unitManager.checkUnitExists(unitName)) {

                TypedArray unitAlreadyExist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.unit_already_exist});

                unitNameTextInputEditText.setError(unitAlreadyExist.getString(0));

                status = false;

            }
        }

        return status;

    }

    public void settingPricePercent() {

        if (purchasePriceEditText.getText().toString().trim().length() > 0) {

            purchasePrice = Double.parseDouble(purchasePriceEditText.getText().toString().trim());

            if (retailPriceEditText.getText().toString().trim().length() > 0) {

                setRetailPricePercent();

            }

            if (wholeSaleEditText.getText().toString().trim().length() > 0) {

                try {

                    setWholeSalePecent();

                } catch (NumberFormatException e) {


                }
            }

        }
    }

    private void setWholeSalePecent() {
        Double whole;
        if (wholeSaleEditText.getText().toString().trim() != null) {

            whole = Double.parseDouble(wholeSaleEditText.getText().toString());

        } else {

            whole = 0.0;

        }
        //Double whole = Double.parseDouble(wholeSaleEditText.getText().toString().trim());

        wholeSalePricePercent = whole - purchasePrice;

        wholeSalePricePercent = (wholeSalePricePercent / purchasePrice) * 100;

        wholeSalePercentTextView.setText(POSUtil.PercentFromat(wholeSalePricePercent));
    }

    private void setRetailPricePercent() {
        Double retail = Double.parseDouble(retailPriceEditText.getText().toString().trim());

        retailPricePercent = retail - purchasePrice;

        retailPricePercent = (retailPricePercent / purchasePrice) * 100;

        retailSalePercentTextView.setText(POSUtil.PercentFromat(retailPricePercent));
    }

    public void textChangeListener() {


        retailPriceEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                settingPricePercent();

            }
        });

        retailPriceEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                settingPricePercent();

            }
        });


        wholeSaleEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                settingPricePercent();

            }
        });

        purchasePriceEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                settingPricePercent();

            }
        });


    }

    public void assigningOldValue() {

        settingOldValue();

        settingOldValueToUI();

        refreshRecyclerView();

    }

    private void refreshRecyclerView() {
        rvAdapterForImageList.setStockImageList(stockImageList);

        rvAdapterForImageList.notifyDataSetChanged();
    }

    private void settingOldValueToUI() {

        stockCodeEditText.setText(stockItem.getCodeNo());

        barCodeEditText.setText(stockItem.getBarcode());

        itemNameEditText.setText(stockItem.getName());

        categoryTextView.setText(stockItem.getCategoryName());

        unitTextView.setText(stockItem.getUnitName());

        purchasePriceEditText.setText(POSUtil.doubleToString(stockItem.getPurchasePrice()));

        retailPriceEditText.setText(POSUtil.doubleToString(stockItem.getRetailPrice()));

        wholeSaleEditText.setText(POSUtil.doubleToString(stockItem.getWholesalePrice()));

        reorderLevelEditText.setText(stockItem.getReorderLevel().toString());

        inventoryQtyEditText.setText(stockItem.getInventoryQty().toString());

        stockDescriptionEditText.setText(stockItem.getDescription());

    }

    private void settingOldValue() {

        categoryId = stockItem.getCategoryID();

        unitId = stockItem.getUnitID();

        categoryId = stockItem.getCategoryID();

        stockImageList = stockManager.getImagesByStockID(stockItem.getId());

    }


    public void settingOnClick() {

        unitTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unitChooserMD.show();
            }
        });


        rvAdapterForUnitMD.setmItemClickListener(new RVAdapterForUnitMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                unitId = unitList.get(position).getId();

                unitTextView.setText(unitList.get(position).getUnit());

                unitChooserMD.dismiss();

            }
        });

        rvAdapterForCategoryMD.setmItemClickListener(new RVAdapterForCategoryMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                categoryId = categoryList.get(position).getId();

                categoryTextView.setText(categoryList.get(position).getName());

                categoryChooserMD.dismiss();

            }
        });


        addNewUnitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAddNewUnitDialogData();
                addNewUnitMaterialDialog.show();
            }
        });

        unitSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkVaildationForAddNewUnitDialog()) {

                    InsertedBooleanHolder status = new InsertedBooleanHolder();

                    unitId = unitManager.addNewUnitFromStock(unitName, unitDescription, status);

                    if (status.isInserted()) {

                        refreshUnitList();
                        FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                        unitTextView.setText(unitName);
                        addNewUnitMaterialDialog.dismiss();

                    }

                }

            }
        });

        unitCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewUnitMaterialDialog.dismiss();
            }
        });

        categoryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryChooserMD.show();
            }
        });


        addNewCategoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clearAddNewCategoryDialogData();

                addNewCategoryMaterialDialog.show();

            }
        });

        categorySaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkVaildationForAddNewCategoryDialog()) {

                    InsertedBooleanHolder flag = new InsertedBooleanHolder();

                    categoryId = categoryManager.addNewCategoryFromStock(categoryName, categoryDescription, null, 0, flag);

                    if (flag.isInserted()) {

                        categoryTextView.setText(categoryName);

                        refreshCategoryList();

                        addNewCategoryMaterialDialog.dismiss();

                        FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                    }
                }

            }
        });

        categoryCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewCategoryMaterialDialog.dismiss();
            }
        });
    }

    private void saveStockItemInformation() {

        if (checkValidation()) {

            if (!isEdit) {

                takeValuesFromView();

                Boolean status = stockManager.addNewStock(stockCode, barCode, itemName, categoryId, unitId, inventoryQty, reorderLevel,
                        purchasePrice, wholeSalePrice, normalSalePrice, retailPrice
                        , stockImageList, description);

                if (status) {
                    TypedArray stockAddedSuccessfully = context.getTheme().obtainStyledAttributes(new int[]{R.attr.stock_added_successfully});

                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);

                    // MainActivity.replaceFragment(new StockFormFragment());

                    getActivity().onBackPressed();
                }
            } else {

                takeValuesFromView();

                Log.w("description", newStockImageList.size() + " sss");

                Boolean status = stockManager.updateStock(stockCode, barCode, itemName, categoryId,
                        unitId, inventoryQty, reorderLevel, purchasePrice, wholeSalePrice, normalSalePrice, retailPrice, deleteImgIDList,
                        newStockImageList, description, stockItem.getId());

                if (status) {

                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                    getActivity().onBackPressed();
                }
            }


        }
    }

    public void onPickImage() {

        Intent chooseImageIntent = ImagePicker.getPickImageIntent(context);

        startActivityForResult(chooseImageIntent, REQUEST_SELECT_PICTURE);

    }


    public void clearAddNewCategoryDialogData() {

        categoryNameTextInputEditText.setError(null);

        categoryNameTextInputEditText.setText(null);

        categoryDescriptionTextInputEditText.setText(null);

    }

    public void refreshCategoryList() {

        categoryList = categoryManager.getAllCategories();

        rvAdapterForCategoryMD.setCategoryList(categoryList);

        rvAdapterForCategoryMD.notifyDataSetChanged();
    }

    public void refreshUnitList() {

        unitList = unitManager.getAllUnits();

        rvAdapterForUnitMD.setUnitList(unitList);

        rvAdapterForUnitMD.notifyDataSetChanged();

    }

    public void clearAddNewUnitDialogData() {

        unitNameTextInputEditText.setError(null);

        unitNameTextInputEditText.setText(null);

        unitDescriptionTextInputEditText.setText(null);

    }


    public void buildAddNewCategoryDialog() {
        TypedArray addCategory = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_category});

        addNewCategoryMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.add_category_dialog, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.negativeText("Cancel")
                //.positiveText("Save")
                .title(addCategory.getString(0)).build();

    }

    public void buildCategoryChooserDialog() {

        categoryList = categoryManager.getAllCategories();

        rvAdapterForCategoryMD = new RVAdapterForCategoryMD(categoryList);

        categoryChooserMD = new MaterialDialog.Builder(context).adapter(rvAdapterForCategoryMD, null).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").build();
    }


    public void buildUnitChooserDialog() {

        unitList = unitManager.getAllUnits();

        rvAdapterForUnitMD = new RVAdapterForUnitMD(unitList);

        unitChooserMD = new MaterialDialog.Builder(context).adapter(rvAdapterForUnitMD, null).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").build();

    }

    public void buildAddNewUnitDialog() {
        TypedArray addUnit = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_unit});

        addNewUnitMaterialDialog = new MaterialDialog.Builder(context)
                //.positiveText("Save")
                //.negativeText("Cancel")
                .customView(R.layout.new_unit_dialog, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .title(addUnit.getString(0)).build();

    }

    public void showSnackBar(View v, String message) {

        Snackbar snackbar = Snackbar.make(v, message, Snackbar.LENGTH_SHORT);

        snackbar.show();
    }


    public void takeValuesFromView() {

        stockCode = stockCodeEditText.getText().toString().trim();

        barCode = barCodeEditText.getText().toString().trim();

        itemName = itemNameEditText.getText().toString().trim();

        description = stockDescriptionEditText.getText().toString().trim();

        if (inventoryQtyEditText.getText().toString().trim() != null && inventoryQtyEditText.getText().toString().trim().length() > 0) {

            inventoryQty = Integer.parseInt(inventoryQtyEditText.getText().toString());

        } else {

            inventoryQty = 0;

        }

        if (reorderLevelEditText.getText().toString().trim() != null && reorderLevelEditText.getText().toString().trim().length() > 0) {

            reorderLevel = Integer.parseInt(reorderLevelEditText.getText().toString());

        } else {
            reorderLevel = 0;
        }

        if (purchasePriceEditText.getText().toString().trim() != null) {

            purchasePrice = Double.parseDouble(purchasePriceEditText.getText().toString());

        } else {

            purchasePrice = 0.0;

        }

        if (retailPriceEditText.getText().toString().trim() != null) {

            retailPrice = Double.parseDouble(retailPriceEditText.getText().toString());

        } else {

            retailPrice = 0.0;

        }

        if (wholeSaleEditText.getText().toString().trim().length() > 0) {

            wholeSalePrice = Double.parseDouble(wholeSaleEditText.getText().toString());

        } else {

            wholeSalePrice = 0.0;

        }

    }

    public Boolean checkValidation() {

        Boolean status = true;

        TypedArray pleaseEnterItemCode   = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_itemcode_or_choose});
        TypedArray stockCodeAlreadyExist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.stock_code_already_exist});

        if (stockCodeEditText.getText().toString().trim().length() < 1) {


            SpannableString s = new SpannableString(pleaseEnterItemCode.getString(0));
            s.setSpan(new TypefaceSpan(context, "Zawgyi-One.ttf"), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            stockCodeEditText.setError(s);

            status = false;

        } else {

            status = stockCodeValidation();

        }

        if (!isEdit) {

            if (stockManager.checkStockCodeExists(stockCodeEditText.getText().toString().trim())) {

                status = false;

                stockCodeEditText.setError(stockCodeAlreadyExist.getString(0));

            }

        } else {
            if (!stockItem.getCodeNo().equalsIgnoreCase(stockCodeEditText.getText().toString().trim())) {

                if (stockManager.checkStockCodeExists(stockCodeEditText.getText().toString().trim())) {

                    status = false;

                    stockCodeEditText.setError(stockCodeAlreadyExist.getString(0));

                }

            }
        }


        if (itemNameEditText.getText().toString().trim().length() < 1) {
            TypedArray pleaseEnterName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_name});

            itemNameEditText.setError(pleaseEnterName.getString(0));

            status = false;

        }

        if (purchasePriceEditText.getText().toString().trim().length() < 1) {
            TypedArray pleaseEnterPurchasePrice = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_purchase_price});

            status = false;

            purchasePriceEditText.setError(pleaseEnterPurchasePrice.getString(0));

        }

       /* if (wholeSaleEditText.getText().toString().trim().length() < 1) {
            TypedArray pleaseEnterWholesalePrice=context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_wholesale_price});

            status = false;

            wholeSaleEditText.setError(pleaseEnterWholesalePrice.getString(0));

        }*/

        if (retailPriceEditText.getText().toString().trim().length() < 1) {
            TypedArray pleaseEnterRetailPrice = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_retail_price});

            status = false;

            retailPriceEditText.setError(pleaseEnterRetailPrice.getString(0));

        }

        return status;

    }


    public void initializeCategory() {
        categoryList = categoryManager.getAllCategories();
    }


    // add items into spinner dynamically
    public void addUnitSpinner() {

        // Spinner element

        // Spinner Drop down elements
        ArrayList<String> units = new ArrayList<String>();
        units.add("Package");
        units.add("Pkg");

        unitList = unitManager.getAllUnits();

        // Creating adapter for spinner
        //        unitAdapter = new UnitSpinnerAdapter(context, unitList);
        //
        //        // Drop down layout style - list view with radio button
        //        unitAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        // unitTextView.setAdapter(unitAdapter);
    }

    public void loadUI() {
        //loading from stock form
        barcodeBtn = (ImageButton) mainLayoutView.findViewById(R.id.barcode);
        stockCodeEditText = (EditText) mainLayoutView.findViewById(R.id.item_code_et);

        barCodeEditText = (EditText) mainLayoutView.findViewById(R.id.bar_code_et);

        itemNameEditText = (EditText) mainLayoutView.findViewById(R.id.item_name_et);

        categoryTextView = (TextView) mainLayoutView.findViewById(R.id.category_sp);

        inventoryQtyEditText = (EditText) mainLayoutView.findViewById(R.id.inventory_qty_et);

        reorderLevelEditText = (EditText) mainLayoutView.findViewById(R.id.reorder_level_et);

        purchasePriceEditText = (EditText) mainLayoutView.findViewById(R.id.purchase_price_et);

        unitTextView = (TextView) mainLayoutView.findViewById(R.id.unit_sp);

        wholeSaleEditText = (EditText) mainLayoutView.findViewById(R.id.whole_sale_price_et);

        retailPriceEditText = (EditText) mainLayoutView.findViewById(R.id.retail_price_et);

        stockDescriptionEditText = (EditText) mainLayoutView.findViewById(R.id.description_et);

        addNewCategoryBtn = (ImageButton) mainLayoutView.findViewById(R.id.add_new_category_btn);

        addNewUnitBtn = (ImageButton) mainLayoutView.findViewById(R.id.add_new_unit_btn);

        priceLinearLayout = (LinearLayout) mainLayoutView.findViewById(R.id.price_ll);

        priceLinearLayoutBtn = (LinearLayout) mainLayoutView.findViewById(R.id.ll_btn2);

        generalLinearLayout = (LinearLayout) mainLayoutView.findViewById(R.id.general_information_ll);

        generalLinearLayoutBtn = (LinearLayout) mainLayoutView.findViewById(R.id.ll_btn1);

        priceImageButton = (ImageButton) mainLayoutView.findViewById(R.id.price_img_btn);

        generalImageButton = (ImageButton) mainLayoutView.findViewById(R.id.general_img_btn);

        wholeSalePercentTextView = (TextView) mainLayoutView.findViewById(R.id.whole_sale_price_percent_tv);

        retailSalePercentTextView = (TextView) mainLayoutView.findViewById(R.id.retail_price_percent_tv);


        imageView = (ImageView) mainLayoutView.findViewById(R.id.img);

        loadUIFromAddNewCategoryDialog();

        loadUIFromAddNewUnit();

    }

    public void loadUIFromAddNewCategoryDialog() {

        categoryNameTextInputEditText = (EditText) addNewCategoryMaterialDialog.findViewById(R.id.category_name_in_category_add_dg);

        categoryDescriptionTextInputEditText = (EditText) addNewCategoryMaterialDialog.findViewById(R.id.category_desc_in_category_add_dg);

        categorySaveBtn = (Button) addNewCategoryMaterialDialog.findViewById(R.id.save);

        categoryCancelBtn = (Button) addNewCategoryMaterialDialog.findViewById(R.id.cancel);


    }

    public void loadUIFromAddNewUnit() {

        unitNameTextInputEditText = (EditText) addNewUnitMaterialDialog.findViewById(R.id.unit_name_in_unit_add_dg);

        unitDescriptionTextInputEditText = (EditText) addNewUnitMaterialDialog.findViewById(R.id.unit_desc_in_unit_add_dg);

        unitSaveBtn = (Button) addNewUnitMaterialDialog.findViewById(R.id.save);

        unitCancelBtn = (Button) addNewUnitMaterialDialog.findViewById(R.id.cancel);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save) {
            saveStockItemInformation();

        }
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);

        TypedArray sale = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        // FontTextView fontTextView= (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.menu_text,null);

        //  menu.getItem(0).setActionView(fontTextView);

        // fontTextView.setText(sale.getString(0));

        // Log.w("herer","action view"+ fontTextView);

        // fontTextView.setOnClickListener(new View.OnClickListener() {
        //  @Override
        //  public void onClick(View v) {
        //       onOptionsItemSelected(menu.getItem(0));
        //     }
        // });

        super.onCreateOptionsMenu(menu, inflater);

    }

    private void startCropActivity(@NonNull Uri uri) {

        String destinationFileName = "temp.png";

        TypedArray allTrans = context.getTheme().obtainStyledAttributes(new int[]{R.attr.pickerAccentColor});

        TypedArray allTran2 = context.getTheme().obtainStyledAttributes(new int[]{R.attr.colorPrimaryDark});

        int valueInPixels = (int) getResources().getDimension(R.dimen.imageBtn_100dp);

        UCrop.Options options = new UCrop.Options();

        options.setToolbarColor(allTrans.getColor(0, 0));

        options.setStatusBarColor(allTran2.getColor(0, 0));

        options.setActiveWidgetColor(allTrans.getColor(0, 0));

        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(context.getCacheDir(), destinationFileName)))

                .withOptions(options)

                .withAspectRatio(1, 1)

                .withMaxResultSize(valueInPixels, valueInPixels);

        uCrop.start(context, this);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == SCANNER_BAR_CODE) {
            // Handle scan intent
            if (resultCode == Activity.RESULT_OK) {
                // Handle successful scan
                String  contents             = data.getStringExtra("SCAN_RESULT");
                String  formatName           = data.getStringExtra("SCAN_RESULT_FORMAT");
                byte[]  rawBytes             = data.getByteArrayExtra("SCAN_RESULT_BYTES");
                int     intentOrientation    = data.getIntExtra("SCAN_RESULT_ORIENTATION", Integer.MIN_VALUE);
                Integer orientation          = (intentOrientation == Integer.MIN_VALUE) ? null : intentOrientation;
                String  errorCorrectionLevel = data.getStringExtra("SCAN_RESULT_ERROR_CORRECTION_LEVEL");

                Log.w("Contents", contents + " hea");
                Log.w("formatName", formatName + " hea");
                Log.w("rawBytes", rawBytes + " hea");
                Log.w("orientation", contents + " hea");
                Log.w("errorCorrectionLevel", errorCorrectionLevel + " hea");

                barCodeEditText.setText(contents);
                if (stockCodeEditText.getText() == null || stockCodeEditText.getText().toString().trim().length() == 0) {
                    stockCodeEditText.setText(contents);
                    stockCodeEditText.selectAll();
                }
                stockCodeEditText.requestFocus();

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // Handle cancel
            }
        } else {
            // Handle other intents
        }

        if (resultCode == Activity.RESULT_OK) {

            switch (requestCode) {

                case REQUEST_SELECT_PICTURE:

                    final Uri selectedUri = ImagePicker.getUriFromResult(context, resultCode, data);


                    if (selectedUri != null) {

                        startCropActivity(selectedUri);

                    } else {
                        TypedArray cannotRetrieve = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cannot_retrieve_selected_image});

                        Toast.makeText(context, cannotRetrieve.getString(0), Toast.LENGTH_SHORT).show();

                    }


                    break;

                case UCrop.REQUEST_CROP:


                    final Uri resultUri = UCrop.getOutput(data);


                    BitmapFactory.Options options = new BitmapFactory.Options();

                    AssetFileDescriptor fileDescriptor = null;
                    try {

                        fileDescriptor = context.getContentResolver().openAssetFileDescriptor(resultUri, "r");

                    } catch (FileNotFoundException e) {

                        e.printStackTrace();

                    }

                    Bitmap actuallyUsableBitmap = BitmapFactory.decodeFileDescriptor(

                            fileDescriptor.getFileDescriptor(), null, options);


                    // Bitmap bitmap = ImagePicker.getCropImage(data,resultCode);
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), actuallyUsableBitmap);

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();

                    actuallyUsableBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);

                    StockImage stockImage = new StockImage(bos.toByteArray());

                    stockImageList.add(stockImage);

                    newStockImageList.add(stockImage);

                    refreshRecyclerView();

                default:

                    super.onActivityResult(requestCode, resultCode, data);

                    break;

            }

        } else if (resultCode == UCrop.RESULT_ERROR) {

            handleCropError(data);

        }


    }

    private void handleCropError(@NonNull Intent result) {

        final Throwable cropError = UCrop.getError(result);

        if (cropError != null) {

            Log.e("ERROR", "handleCropError: ", cropError);

            Toast.makeText(context, cropError.getMessage(), Toast.LENGTH_LONG).show();

        } else {

            Toast.makeText(context, "I Don't know this error", Toast.LENGTH_SHORT).show();

        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //recreate();
            //reload my activity with requestPermissions granted or use the features what required the requestPermissions
            //requestPermissions(backingUp);

            onPickImage();


        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(getContext())
                    .setTitle(Html.fromHtml("<font color='#424242'>Inform and request</font>"))
                    .setMessage(Html.fromHtml("<font color='#424242'>Please enable storage requestPermissions to perform backup and restore functions!</font>"))
                    .setPositiveButton(Html.fromHtml("<font color='#424242'>Ok</font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.requestPermissions.WRITE_EXTERNAL_STORAGE}, RC_PERMISSION_WRITE_EXTERNAL_STORAGE);
                            //   ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.requestPermissions.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_RC);
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 200);
                        }
                    })
                    .show();
            Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            nbutton.setTextColor(Color.parseColor("#424242"));
        } else {
            AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(getContext())
                    .setTitle(Html.fromHtml("<font color='#424242'>Inform and request</font>"))
                    .setMessage(Html.fromHtml("<font color='#424242'>Please enable storage requestPermissions from settings to get access to the storage!</font>"))
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 200);
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        }
                    })
                    .show();
            Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            nbutton.setTextColor(Color.parseColor("#424242"));
            //}
        }
    }


    private void performCrop(Uri contentUri) {
        try {
            //Start Crop Activity

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");

            cropIntent.putExtra("scale", "false");

            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);

            cropIntent.putExtra("aspectY", 0);
            // indicate output X and Y

            cropIntent.putExtra("outputX", 280);

            cropIntent.putExtra("outputY", 280);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, 400);

        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {

            // display an error message
            TypedArray doesNotSupport = context.getTheme().obtainStyledAttributes(new int[]{R.attr.your_device_doesnot_support_the_crop_action});

            Toast toast = Toast.makeText(context, doesNotSupport.getString(0), Toast.LENGTH_SHORT);

            toast.show();

        }
    }

}
