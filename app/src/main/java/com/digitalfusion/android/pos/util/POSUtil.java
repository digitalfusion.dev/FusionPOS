package com.digitalfusion.android.pos.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.activities.PasscodeActivity;
import com.digitalfusion.android.pos.database.business.ApiManager;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.ThemeObj;
import com.digitalfusion.android.pos.database.model.Device;
import com.digitalfusion.android.pos.information.Registration;
import com.digitalfusion.android.pos.information.Subscription;
import com.digitalfusion.android.pos.database.model.User;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by MD003 on 8/19/16.
 */
public class POSUtil {
    /**
     * Sends a broadcast to have the media scanner scan a file
     *
     * @param path
     *            the file to scan
     */
    public static void scanMedia(String path, Context context) {
        File file = new File(path);
        Uri  uri  = Uri.fromFile(file);
        Intent scanFileIntent = new Intent(
                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
        context.sendBroadcast(scanFileIntent);
    }

    public static void showSnackBar(View v, String message) {
        Snackbar snackbar = Snackbar.make(v, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    /**
     * Thought this would be a good idea. :/
     */
    public static void noUserIsLoggedIn(Context context) {
        Toast.makeText(context, "No user is logged in", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(context, PasscodeActivity.class);
        context.startActivity(intent);
    }

    //    public static byte[] bitmapToBiteArray(Bitmap bmp) {
    //        ByteArrayOutputStream stream = new ByteArrayOutputStream();
    //        bmp.compress(Bitmap.CompressFormat.PNG, 50, stream);
    //
    //        return stream.toByteArray();
    //    }
    //
    //    /**
    //     * https://stackoverflow.com/a/10564727
    //     */
    //    public static String getRealPathFromURI(Uri contentUri, Context context) {
    //        String[] proj = { MediaStore.Images.Media.DATA };
    //        CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
    //        Cursor cursor = loader.loadInBackground();
    //        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
    //        cursor.moveToFirst();
    //        String result = cursor.getString(column_index);
    //        cursor.close();
    //        return result;
    //    }
    //
    //    /**
    //     * Decodes image and scales it to reduce memory consumption
    //     * https://stackoverflow.com/a/823966
    //     */
    //    public static Bitmap rescaleImage(File f) {
    //        try {
    //            // Decode image size
    //            BitmapFactory.Options o = new BitmapFactory.Options();
    //            o.inJustDecodeBounds = true;
    //            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
    //
    //            // The new size we want to scale to
    //            final int REQUIRED_SIZE=70;
    //
    //            // Find the correct scale value. It should be the power of 2.
    //            int scale = 1;
    //            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
    //                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
    //                scale *= 2;
    //            }
    //
    //            // Decode with inSampleSize
    //            BitmapFactory.Options o2 = new BitmapFactory.Options();
    //            o2.inSampleSize = scale;
    //            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
    //        } catch (FileNotFoundException e) {}
    //        return null;
    //    }

    /**
     * Check whether an EditText is empty or not
     *
     * @param textView
     * @return true if empty. otherwise, false
     */
    public static boolean isTextViewEmpty(TextView textView) {
        if (textView.getText().toString().trim().length() > 0) {
            return false;
        }
        return true;
    }

    public static String[] getKeys(Context context) {
        String outputString = "";
        try {
            SecretKey secretKey = new SecretKeySpec(context.getString(R.string.title_name).getBytes(), context.getString(R.string.algorithm));
            Cipher    cipher    = Cipher.getInstance(context.getString(R.string.algorithm));
            //            Log.e("secret key", new String(secretKey.getEncoded()));
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            InputStream inputStream = context.getAssets().open("fusion.fusion");
            //            byte[] buffer = new byte[1024];
            //            int length = inputStream.available();

            byte[] inputBytes = new byte[(int) inputStream.available()];
            inputStream.read(inputBytes);

            byte[] outputBytes = cipher.doFinal(inputBytes);
            outputString = new String(outputBytes);
            inputStream.close();

        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException e) {
            //            e.printStackTrace();
        }
        String[] ouput = outputString.split("\n");
        return ouput;
    }

    public static String doubleToString(Double value) {
        if (value == null) {
            return "0";
        }

        return String.valueOf(value.intValue());
    }

    public static String NumberFormat(Double value) {

        if (value == null) {

            return "0";
        }

        DecimalFormat decimalFormat = new DecimalFormat("###,###,###,###,###,###");

        return decimalFormat.format(value);
    }

    public static String NumberFormat(Long value) {

        if (value == null) {

            return "0";
        }

        DecimalFormat decimalFormat = new DecimalFormat("###,###,###,###,###,###");

        return decimalFormat.format(value);
    }

    private static void showWaring(Context context, int day) {
        final MaterialDialog materialDialog = new MaterialDialog.Builder(context).customView(R.layout.expire_warining_dialog, true).build();
        TextView             dayTextView    = (TextView) materialDialog.findViewById(R.id.day);
        materialDialog.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialDialog.dismiss();
            }
        });

        dayTextView.setText(Integer.toString(day));
        materialDialog.show();
        AppConstant.isAlreadyShowExpire = true;
    }

    public static void checkExpire(Context context) {
        ApiManager apiManager = new ApiManager(context);
        if (POSUtil.isActivatedBoolean(context)) {
            Calendar c = Calendar.getInstance();
            c.setTime(apiManager.getLicense().getEndDate());
            int day = DateUtility.dateDifference(c);
            if (day < 7) {
                if (!AppConstant.isAlreadyShowExpire) {
                    showWaring(context, day);
                }

            }
        }

    }

    public static boolean checkIsExpire(Context context) {
        ApiManager apiManager = new ApiManager(context);
        if (POSUtil.isActivatedBoolean(context)) {
            Calendar c = Calendar.getInstance();
            c.setTime(apiManager.getLicense().getEndDate());
            int day = DateUtility.dateDifference(c);
            Log.w("Day REmaining", day + " SS");
            Log.w("Day REmaining", apiManager.getLicense().getEndDate() + " SS");
            if (day < 0) {
                return true;
            } else {
                return false;
            }

        }
        return false;

    }

    public static int getRemainDay(Context context) {
        ApiManager apiManager = new ApiManager(context);
        Calendar   c          = Calendar.getInstance();
        c.setTime(apiManager.getLicense().getEndDate());
        int day = DateUtility.dateDifference(c);
        return day;
    }

    public static boolean isTabetLand(Context context) {
        return context.getResources().getBoolean(R.bool.isTableLand);
    }

    public static String NumberFormat(int value) {

        DecimalFormat decimalFormat = new DecimalFormat("###,###,###,###,###,###");

        return decimalFormat.format(value);


    }

    public static String PercentFromat(Double value) {
        if (value == null) {

            return "0.00";
        }
        DecimalFormat decimalFormat = new DecimalFormat("###.##");

        return decimalFormat.format(value);


    }

    public static void changeDefaultTheme(ThemeObj themeObj, @NonNull Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.THEME, MODE_PRIVATE).edit();
        editor.putInt(AppConstant.DEFAULT_THEME_NO_ACTION_BAR, themeObj.getThemeResourceNoActionBar());
        editor.putString(AppConstant.THEME_NAME, themeObj.getThemeNameID());
        editor.putInt(AppConstant.DEFAULT_THEME_ACTION_BAR, themeObj.getThemeResourceActionBar());
        editor.commit();

    }


    public static void showKeyboard(View view) {

        // if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1 && view.hasFocus()) {

        //    view.clearFocus();

        //  }


        InputMethodManager imm = (InputMethodManager) view
                .getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);


    }

    public static void activateNightMode(@NonNull Context context) {

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.THEME, MODE_PRIVATE).edit();

        editor.putBoolean(AppConstant.NIGHT_MODE, true);

        editor.commit();

    }

    public static void saveDate(@NonNull Context context, String date) {
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.REGDATE, MODE_PRIVATE).edit();
        editor.putString(AppConstant.REGDATE, date);
        editor.commit();
    }

    public static String getDate(@NonNull Context context) {
        SharedPreferences editor = context.getSharedPreferences(AppConstant.REGDATE, MODE_PRIVATE);

        return editor.getString(AppConstant.REGDATE, "00000000");
    }

    public static void saveCount(@NonNull Context context, int count) {
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.COUNT, MODE_PRIVATE).edit();
        editor.putInt(AppConstant.REGDATE, count);
        editor.commit();
    }

    public static int getCount(@NonNull Context context) {
        SharedPreferences editor = context.getSharedPreferences(AppConstant.COUNT, MODE_PRIVATE);

        return editor.getInt(AppConstant.REGDATE, 7);
    }

    public static void setMM80Printer(@NonNull Context context, String printerType) {

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.PRINTER, MODE_PRIVATE).edit();

        if (printerType.equalsIgnoreCase(AppConstant._80MM)) {
            editor.putBoolean(AppConstant._80MM, true);
            editor.putBoolean(AppConstant._Internal, false);
            editor.putBoolean(AppConstant._58MM, false);
        } else if (printerType.equalsIgnoreCase(AppConstant._58MM)) {
            editor.putBoolean(AppConstant._80MM, false);
            editor.putBoolean(AppConstant._Internal, false);
            editor.putBoolean(AppConstant._58MM, true);
        } else {
            editor.putBoolean(AppConstant._80MM, false);
            editor.putBoolean(AppConstant._Internal, true);
            editor.putBoolean(AppConstant._58MM, false);
        }

        editor.commit();
    }

    public static void activateNoEmbendedFont(@NonNull Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.ISNOEMBEDED, MODE_PRIVATE).edit();
        editor.putBoolean(AppConstant.ISNOEMBEDED, true);
        editor.commit();
    }

    public static void deactivateNoEmbendedFont(@NonNull Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.ISNOEMBEDED, MODE_PRIVATE).edit();
        editor.putBoolean(AppConstant.ISNOEMBEDED, false);
        editor.commit();
    }

    private static void activate(@NonNull Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.ISACTIVATED, MODE_PRIVATE).edit();
        editor.putBoolean(AppConstant.ISACTIVATED, true);
        editor.commit();
    }

    public static boolean isActivatedBoolean(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(AppConstant.ISACTIVATED, MODE_PRIVATE);

        return prefs.getBoolean(AppConstant.ISACTIVATED, false);
    }

    public static void setEndDate(@NonNull Context context, String endDate) {
        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.ENDDATE, MODE_PRIVATE).edit();
        editor.putString(AppConstant.ENDDATE, endDate);
        editor.commit();
    }

    public static void editPreferencesVoucher(String headerText, Context context, String footerText) {
        SharedPreferences        sharedPref = context.getSharedPreferences(AppConstant.VOUCHERSETTING, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor     = sharedPref.edit();

        editor.putString("header", headerText);

        editor.putString("footer", footerText);

        editor.commit();
    }

    public static String getHeaderTxt(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(AppConstant.VOUCHERSETTING, Context.MODE_PRIVATE);


        return sharedPref.getString("header", "");

    }

    public static String getFooterTxt(Context context) {
        SharedPreferences        sharedPref = context.getSharedPreferences(AppConstant.VOUCHERSETTING, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor     = sharedPref.edit();

        return sharedPref.getString("footer", "");

    }

    public static void acitvate(Subscription responseSubscription, Context context) {

        // POSUtil.NotUnExpectedChange(context);
        Registration   registration   = responseSubscription.getRegistration();
        ApiManager     apiManager     = new ApiManager(context);
        SettingManager settingManager = new SettingManager(context);
        if (!POSUtil.checkIsExpire(context)) {
            settingManager.updateBusinessSetting(responseSubscription.getRegistration().getBusinessName(), responseSubscription.getRegistration().getUserName(), responseSubscription.getRegistration().getBusinessType(), responseSubscription.getRegistration().getPhone()
                    , responseSubscription.getRegistration().getTownship(), responseSubscription.getRegistration().getState());

            POSUtil.editPreferencesVoucher(registration.getBusinessName(), context, "Thank you!!");
        }
        activate(context);
        apiManager.updateRegistration(registration.getUserName(), registration.getUserId(), registration.getBusinessName(), registration.getUserStatus());
        apiManager.updateLicense(responseSubscription.getLicenseInfo().getDuration(), responseSubscription.getLicenseInfo().getStartDate(), responseSubscription.getLicenseInfo().getEndDate());

    }

    public static String getEndDate(Context context) {

        SharedPreferences prefs = context.getSharedPreferences(AppConstant.ENDDATE, MODE_PRIVATE);


        return prefs.getString(AppConstant.ENDDATE, "000000");


    }

    public static boolean is80MMPrinter(Context context) {

        SharedPreferences prefs = context.getSharedPreferences(AppConstant.PRINTER, MODE_PRIVATE);

        return prefs.getBoolean(AppConstant._80MM, false);


    }

    public static boolean is58MMPrinter(Context context) {

        SharedPreferences prefs = context.getSharedPreferences(AppConstant.PRINTER, MODE_PRIVATE);


        return prefs.getBoolean(AppConstant._58MM, true);


    }

    public static boolean isInternalPrinter(Context context) {

        SharedPreferences prefs = context.getSharedPreferences(AppConstant.PRINTER, MODE_PRIVATE);


        return prefs.getBoolean(AppConstant._Internal, false);


    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isUnExpectedChangeBoolean(Context context) {
        ApiManager   apiManager   = new ApiManager(context);
        ExternalFile externalFile = new ExternalFile();

        //        Log.w("FROM  FILE",externalFile.readFile()+" S"+getEndDate(context));
        if (isActivatedBoolean(context)) {
            String endDate1 = getEndDate(context);

            String endDate3 = externalFile.readFile(context);
            String endDate2 = "0";
            if (apiManager.getLicense().getEndDate() != null) {
                endDate2 = DateUtility.makeDate(apiManager.getLicense().getEndDate());
            }
            if (endDate1.equalsIgnoreCase(endDate2) && endDate1.equalsIgnoreCase(endDate3)) {
                return false;
            } else {
                return true;
            }
        } else if (externalFile.readFile(context) != null && !externalFile.readFile(context).equalsIgnoreCase("")) {
            return true;
        } else {
            return false;
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isAutomaticTimeZone(Context context) {

        int autoTimeAvailable = 0;
        try {
            autoTimeAvailable = Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME);
        } catch (Settings.SettingNotFoundException e) {

            return false;
        }

        //String endDate1=getEndDate(context);
        //String endDate2= DateUtility.makeDate(new ApiManager(context).getLicense().getEndDate());

        //  if(Long.parseLong(endDate1)!=Long.parseLong(endDate2)){
        //    return true;
        // }

        if (autoTimeAvailable == 1) {


            return true;
        } else {
            return false;
        }

    }


    public static boolean isEmbended(Context context) {

        SharedPreferences prefs = context.getSharedPreferences(AppConstant.ISNOEMBEDED, MODE_PRIVATE);

        boolean s = prefs.getBoolean(AppConstant.ISNOEMBEDED, false);

        if (!s) {
            return true;
        } else {
            return false;
        }

    }


    public static boolean isLabelWhite(Context context) {

        boolean islabelWhite = context.getTheme().obtainStyledAttributes(new int[]{R.attr.isLabelWhite}).getBoolean(0, false);


        return islabelWhite;


    }


    public static void deActivateNightMode(@NonNull Context context) {

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.THEME, MODE_PRIVATE).edit();

        editor.putBoolean(AppConstant.NIGHT_MODE, false);

        editor.commit();

    }


    public static int getDefaultThemeNoActionBar(@NonNull Context context) {

        SharedPreferences prefs = context.getSharedPreferences(AppConstant.THEME, MODE_PRIVATE);

        boolean isNightMode = prefs.getBoolean(AppConstant.NIGHT_MODE, false);

        boolean isMyanmar = prefs.getBoolean(AppConstant.IS_MYANMAR, true);

        String themeName = prefs.getString(AppConstant.THEME_NAME, ThemeUtil.Fusion);

        Log.w("is Myanmar", isMyanmar + "");

        Log.w("Theme Name", themeName + "");

        // boolean isMyanmar=isMyanmarLanguage(context);


        if (isNightMode) {

            if (isMyanmar) {

                return R.style.Theme_Myan_NightMode_NoActionBar;

            } else {

                return R.style.Theme_Eng_NightMode_NoActionBar;

            }

        } else {

            if (isMyanmar) {

                switch (themeName) {

                    case ThemeUtil.Blue: {
                        return R.style.Theme_Myan_Blue_NoActionBar;
                    }
                    case ThemeUtil.Blue_Grey: {
                        return R.style.Theme_Myan_BlueGrey_NoActionBar;
                    }
                    case ThemeUtil.Purple: {
                        return R.style.Theme_Myan_Purple_NoActionBar;
                    }
                    case ThemeUtil.Red: {
                        return R.style.Theme_Myan_Red_NoActionBar;
                    }
                    case ThemeUtil.White: {
                        return R.style.Theme_Myan_White_NoActionBar;
                    }
                    case ThemeUtil.Cyan: {
                        return R.style.Theme_Myan_Cyan_NoActionBar;
                    }
                    case ThemeUtil.Red_Blue: {
                        return R.style.Theme_Myan_sky_blue_NoActionBar;
                    }
                    case ThemeUtil.Night: {
                        return R.style.Theme_Myan_NightMode_NoActionBar;
                    }
                    case ThemeUtil.Light_Green: {
                        return R.style.Theme_Myan_light_green_NoActionBar;
                    }
                    case ThemeUtil.Gold: {
                        return R.style.Theme_Myan_Gold_NoActionBar;
                    }
                    case ThemeUtil.Violet: {
                        return R.style.Theme_Myan_Violet_NoActionBar;
                    }
                    case ThemeUtil.Fusion: {
                        return R.style.Theme_Myan_Fusion_NoActionBar;
                    }
                    case ThemeUtil.Pine_Green: {
                        return R.style.Theme_Myan_Pine_Green_NoActionBar;
                    }
                    default: {
                        return R.style.Theme_Myan_Blue_NoActionBar;
                    }
                }


            } else {
                switch (themeName) {

                    case ThemeUtil.Blue_Grey: {
                        return R.style.Theme_Eng_BlueGrey_NoActionBar;
                    }
                    case ThemeUtil.Purple: {
                        return R.style.Theme_Eng_Purple_NoActionBar;
                    }
                    case ThemeUtil.Cyan: {
                        return R.style.Theme_Eng_Cyan_NoActionBar;
                    }
                    case ThemeUtil.Red: {
                        return R.style.Theme_Eng_Red_NoActionBar;
                    }
                    case ThemeUtil.White: {
                        return R.style.Theme_Eng_White_NoActionBar;
                    }
                    case ThemeUtil.Night: {
                        return R.style.Theme_Eng_NightMode_NoActionBar;
                    }
                    case ThemeUtil.Red_Blue: {
                        return R.style.Theme_Eng_sky_blue_NoActionBar;
                    }
                    case ThemeUtil.Light_Green: {
                        return R.style.Theme_Eng_light_green_NoActionBar;
                    }
                    case ThemeUtil.Gold: {
                        return R.style.Theme_Eng_Gold_NoActionBar;
                    }
                    case ThemeUtil.Violet: {
                        return R.style.Theme_Eng_Violet_NoActionBar;
                    }
                    case ThemeUtil.Fusion: {
                        return R.style.Theme_Eng_Fusion_NoActionBar;
                    }
                    case ThemeUtil.Pine_Green: {
                        return R.style.Theme_Eng_Pine_Green_NoActionBar;
                    }
                    default: {
                        return R.style.Theme_Eng_Blue_NoActionBar;
                    }

                }
            }
        }

    }

    public static boolean isMyanmarLanguage(Context context) {

        SharedPreferences prefs = context.getSharedPreferences(AppConstant.THEME, MODE_PRIVATE);
        return prefs.getBoolean(AppConstant.IS_MYANMAR, true);


    }

    public static int getDefaultThemeActionBar(@NonNull Context context) {

        SharedPreferences prefs = context.getSharedPreferences(AppConstant.THEME, MODE_PRIVATE);


        boolean isNightMode = prefs.getBoolean(AppConstant.NIGHT_MODE, false);

        if (isNightMode) {
            return R.style.Theme_Eng_NightMode;
        } else {

            return prefs.getInt(AppConstant.DEFAULT_THEME_ACTION_BAR, R.style.Theme_Eng_Blue);

        }
    }


    public static boolean isNightMode(@NonNull Context context) {

        SharedPreferences prefs = context.getSharedPreferences(AppConstant.THEME, MODE_PRIVATE);


        boolean isNightMode = prefs.getBoolean(AppConstant.NIGHT_MODE, false);


        return isNightMode;
    }

    public static String getDefaultThemeName(@NonNull Context context) {

        SharedPreferences prefs = context.getSharedPreferences(AppConstant.THEME, MODE_PRIVATE);

        return prefs.getString(AppConstant.THEME_NAME, ThemeUtil.Fusion);

    }


    public static void ChangeActivity(Activity currentActivity, Activity toActivity) {


    }

    public static Bitmap getBitmapFromByteArray(@NonNull byte[] img) {

        if (img != null) {
            Log.w("image is not", " null");
            ByteArrayInputStream imageStream = new ByteArrayInputStream(img);

            return BitmapFactory.decodeStream(imageStream);
        } else {
            return null;
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static Bitmap getBitmapFromByteArray(@NonNull byte[] img, @NonNull Context context) {

        if (img != null) {
            int valueInPixels = (int) context.getResources().getDimension(R.dimen.imageBtn_100dp);

            Log.w("image is not", " null");
            ByteArrayInputStream imageStream = new ByteArrayInputStream(img);
            Bitmap               theImage    = BitmapFactory.decodeStream(imageStream);
            theImage.setHeight(valueInPixels);
            theImage.setWidth(valueInPixels);
            return theImage;
        } else {
            return null;
        }

    }


    public static void makeZebraStrip(View view, int position) {

        /*if (position % 2 != 0) {

            TypedArray allTrans = view.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.evenRow});

            view.setBackgroundColor(allTrans.getColor(0, 0));

        } else {

            TypedArray allTrans = view.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.oddRow});

            view.setBackgroundColor(allTrans.getColor(0, 0));

        }*/
    }

    public static void hideKeyboard(View view, Context context) {

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        imm.hideSoftInputFromWindow(view.getWindowToken(), 1);

    }


}
