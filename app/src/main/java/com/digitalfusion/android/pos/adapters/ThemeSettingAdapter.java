package com.digitalfusion.android.pos.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;

import java.util.ArrayList;

/**
 * Created by MD003 on 11/28/16.
 */

public class ThemeSettingAdapter extends RecyclerView.Adapter<ThemeSettingAdapter.ViewHolder> {

    ArrayList<String> settingListViews;
    Context context;
    private ClickListener clickListener;

    public ThemeSettingAdapter(ArrayList<String> settingListViews, Context context) {

        this.settingListViews = settingListViews;

        this.context = context;

    }

    @Override
    public ThemeSettingAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext())

                .inflate(R.layout.theme_item_view, viewGroup, false);

        ThemeSettingAdapter.ViewHolder viewHolder = new ThemeSettingAdapter.ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ThemeSettingAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.txt.setText(settingListViews.get(i));

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (clickListener != null) {

                    clickListener.onClick(i);

                }
            }
        });

    }

    public ClickListener getClickListener() {

        return clickListener;

    }

    public void setClickListener(ClickListener clickListener) {

        this.clickListener = clickListener;

    }

    public ArrayList<String> getSettingListViews() {
        return settingListViews;

    }

    public void setSettingListViews(ArrayList<String> settingListViews) {

        this.settingListViews = settingListViews;

    }

    @Override
    public int getItemCount() {

        return settingListViews.size();

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt;

        public View itemView;


        public ViewHolder(View itemView) {

            super(itemView);

            this.itemView = itemView;

            txt = (TextView) itemView.findViewById(R.id.theme_name);

        }


    }

}