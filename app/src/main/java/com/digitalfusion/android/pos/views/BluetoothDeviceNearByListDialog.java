package com.digitalfusion.android.pos.views;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.BluetoothDeviceViewAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by MD003 on 6/6/17.
 */

public class BluetoothDeviceNearByListDialog extends DialogFragment {

    private static final boolean D = true;
    // Return Intent extra
    public static String EXTRA_DEVICE_ADDRESS = "device_address";
    Dialog mDialog;
    List<BluetoothDevice> pairedDeviceInfoList;
    List<BluetoothDevice> newDeviceInfoList;
    Button scanButton;
    int count = 0;
    Handler handler;
    boolean stop;
    Toolbar toolbar;
    int height;
    int width;
    // Member fields
    private BluetoothAdapter mBtAdapter;
    private BluetoothDeviceViewAdapter mPairedDevicesArrayAdapter;
    private BluetoothDeviceViewAdapter mNewDevicesArrayAdapter;
    // The BroadcastReceiver that listens for discovered devices and
    // changes the title when discovery is finished
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.w("HEREE", action);
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {

                    Log.w("DEVICE ", "FOLUND");
                    device.getName();
                    Log.w("DEVICE NAME", device.getName());
                    //   mNewDevicesArrayAdapter = new BluetoothDeviceViewAdapter(getApplicationContext(), R.layout.device_name,R.layout.device_name,newDeviceInfoList);;
                    mNewDevicesArrayAdapter.remove(device);

                    mNewDevicesArrayAdapter.add(device);
                    mNewDevicesArrayAdapter.notifyDataSetChanged();
                }
                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //  setProgressBarIndeterminateVisibility(false);
                // setTitle(R.string.select_device);
                scanButton.setVisibility(View.VISIBLE);

            }
        }
    };
    private DeviceConnectListener deviceConnectListener;
    private View mainLayoutView;
    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            // Do something here on the main thread
            //   Log.d("Handlers", "Called on main thread");
            if (!stop) {
                if (count > 0) {
                    Log.d("Handlers", "Called on main thread");
                    scanButton.setText("Scanning (" + count + " sec)");
                    handler.postDelayed(runnableCode, 1000);
                    if (count % 3 == 1) {

                        //                      toolbar.setTitle("Scanning Devices..");

                        // toolbarLabelTextView.setText("Scanning Devices..");
                    } else if (count % 3 == 2) {
                        //                        toolbar.setTitle("Scanning Devices.");

                        // toolbarLabelTextView.setText("Scanning Devices.");
                    } else if (count % 3 == 0) {
                        //                toolbar.setTitle("Scanning Devices...");
                        //   toolbarLabelTextView.setText("Scanning Devices...");
                    }

                } else if (count == 0) {
                    scanButton.setText("Scan");
                    scanButton.setEnabled(true);
                    // toolbarLabelTextView.setText("Bluetooth Devices");
                    //
                }

                count--;
            }


            // Repeat this the same runnable code block again another 2 seconds

        }
    };

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setTitle("Bluetooth");
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int            width   = metrics.widthPixels;
        int            height  = metrics.heightPixels;
        //int width=(int)(getResources().getDisplayMetrics().widthPixels*0.9);

        dialog.getWindow().setLayout((6 * width) / 7, (4 * height) / 5);

        mDialog = dialog;

        return dialog;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //int width=(int)(getResources().getDisplayMetrics().widthPixels*0.90);

        // getDialog().getWindow().setLayout(Matr,width);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int            width   = metrics.widthPixels;
        int            height  = metrics.heightPixels;
        //int width=(int)(getResources().getDisplayMetrics().widthPixels*0.9);

        // getDialog().getWindow().setLayout((6 * width)/7, (4 * height)/5);

        mainLayoutView = inflater.inflate(R.layout.bluetooth_device_list, null);

        scanButton = (Button) mainLayoutView.findViewById(R.id.button_scan);

        handler = new Handler();


        scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                doDiscovery();
                v.setVisibility(View.VISIBLE);
                scanButton.setText("Scanning (12 sec)");
                count = 11;
                scanButton.setEnabled(false);
                handler.postDelayed(runnableCode, 1000);
                //                toolbarLabelTextView.setText("Scanning Devices...");

                // YoYo.with(Techniques.BounceInLeft)
                //   .duration(700)
                //   .playOn(toolbarLabelTextView);
            }
        });


        // Initialize array adapters. One for already paired devices and
        // one for newly discovered devices

        pairedDeviceInfoList = new ArrayList<>();
        newDeviceInfoList = new ArrayList<>();
        mPairedDevicesArrayAdapter = new BluetoothDeviceViewAdapter(mainLayoutView.getContext(), R.layout.device_name, R.layout.device_name, pairedDeviceInfoList);
        mNewDevicesArrayAdapter = new BluetoothDeviceViewAdapter(mainLayoutView.getContext(), R.layout.device_name, R.layout.device_name, newDeviceInfoList);

        // Find and set up the ListView for paired devices
        ListView pairedListView = (ListView) mainLayoutView.findViewById(R.id.paired_devices);
        pairedListView.setAdapter(mPairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mBtAdapter.cancelDiscovery();

                // Get the device MAC address, which is the last 17 chars in the View
                String address = pairedDeviceInfoList.get(position).getAddress();

                // Create the result Intent and include the MAC address
                Intent intent = new Intent();
                intent.putExtra(EXTRA_DEVICE_ADDRESS, address);

                if (deviceConnectListener != null) {
                    deviceConnectListener.onDeviceConnect(address);
                }
                // Set result and finish this Activity
                //setResult(Activity.RESULT_OK, intent);
                //finish();
            }
        });

        // Find and set up the ListView for newly discovered devices
        ListView newDevicesListView = (ListView) mainLayoutView.findViewById(R.id.new_devices);
        newDevicesListView.setAdapter(mNewDevicesArrayAdapter);
        newDevicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = newDeviceInfoList.get(position).getAddress();

                // Create the result Intent and include the MAC address
                Intent intent = new Intent();
                intent.putExtra(EXTRA_DEVICE_ADDRESS, address);
                if (deviceConnectListener != null) {
                    deviceConnectListener.onDeviceConnect(address);
                }
                // Set result and finish this Activity
                //setResult(Activity.RESULT_OK, intent);
                //finish();
            }
        });

        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        mainLayoutView.getContext().registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        mainLayoutView.getContext().registerReceiver(mReceiver, filter);


        // Get the local Bluetooth adapter
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        mainLayoutView.findViewById(R.id.title_new_devices).setVisibility(View.VISIBLE);
        // Get a set of currently paired devices
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

        // If there are paired devices, add each one to the ArrayAdapter
        if (pairedDevices.size() > 0) {
            mainLayoutView.findViewById(R.id.title_paired_devices).setVisibility(View.VISIBLE);

            Log.w("SIZE", pairedDevices.size() + "SSS");
            //pairedDeviceInfoList =new ArrayList<>();
            mPairedDevicesArrayAdapter = new BluetoothDeviceViewAdapter(mainLayoutView.getContext(), R.layout.device_name, R.layout.device_name, pairedDeviceInfoList);
            pairedListView.setAdapter(mPairedDevicesArrayAdapter);
            for (BluetoothDevice device : pairedDevices) {
                // mPairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                mPairedDevicesArrayAdapter.add(device);
            }

        } else {
            String noDevices = "No pair device";
            //    mPairedDevicesArrayAdapter.add(noDevices);
        }


        return mainLayoutView;
    }

    @Override
    public void onResume() {
        ViewGroup.LayoutParams params  = getDialog().getWindow().getAttributes();
        DisplayMetrics         metrics = getResources().getDisplayMetrics();
        int                    width   = metrics.widthPixels;
        int                    height  = metrics.heightPixels;
        //int width=(int)(getResources().getDisplayMetrics().widthPixels*0.9);

        // dialog.getWindow().setLayout((6 * width)/7, (4 * height)/5);

        params.width = (6 * width) / 7;
        params.height = (4 * height) / 5;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        super.onResume();
    }

    public void setDeviceConnectListener(DeviceConnectListener deviceConnectListener) {
        this.deviceConnectListener = deviceConnectListener;
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    private void doDiscovery() {
        if (D) Log.d("B DIALOG", "doDiscovery()");

        // Indicate scanning in the title
        //setProgressBarIndeterminateVisibility(true);

        // Turn on sub-title for new devices
        //  findViewById(R.id.title_new_devices).setVisibility(View.VISIBLE);

        // If we're already discovering, stop it
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }

        // Request discover from BluetoothAdapter
        mBtAdapter.startDiscovery();
    }

    public interface DeviceConnectListener {
        void onDeviceConnect(String address);
    }

}
