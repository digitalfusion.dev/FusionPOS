package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.Discount;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD002 on 6/21/17.
 */

public class RVSwipeAdapterForDiscountList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private List<Discount> discountList;

    private ClickListener editClickListener;
    private ClickListener deleteClickListener;


    public RVSwipeAdapterForDiscountList(List<Discount> discountList) {
        this.discountList = discountList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.discount_item_view, parent, false);

        return new DiscountViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof DiscountViewHolder) {

            final DiscountViewHolder discountViewHolder = (DiscountViewHolder) holder;

            POSUtil.makeZebraStrip(discountViewHolder.itemView, position);

            discountViewHolder.discountNameTextView.setText(discountList.get(position).getName());

            discountViewHolder.discountAmtTextView.setText(POSUtil.doubleToString(discountList.get(position).getAmount()));

            if (discountList.get(position).getIsDefault() > 0) {

                discountViewHolder.isDefaultImageView.setVisibility(View.VISIBLE);

            } else {

                discountViewHolder.isDefaultImageView.setVisibility(View.GONE);

            }

            if (discountList.get(position).getIsPercent() == 0) {
                discountViewHolder.discountAmtOrPercentTextView.setText(AppConstant.CURRENCY);
            } else {
                discountViewHolder.discountAmtOrPercentTextView.setText("%");
            }

            if (discountList.get(position).getDescription() != null && !discountList.get(position).getDescription().equalsIgnoreCase("")) {

                discountViewHolder.desTextView.setText(discountList.get(position).getDescription());

            } else {
                discountViewHolder.desTextView.setText(null);
                discountViewHolder.desTextView.setHint(discountViewHolder.nodes);
            }

            discountViewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editClickListener != null) {

                        discountViewHolder.swipeLayout.close(true);

                        editClickListener.onClick(position);

                    }
                }
            });

            discountViewHolder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteClickListener != null) {
                        discountViewHolder.swipeLayout.close(true);
                        deleteClickListener.onClick(position);
                    }
                }
            });


            mItemManger.bindView(discountViewHolder.view, position);

        }

    }

    @Override
    public int getItemCount() {
        return discountList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public List<Discount> getDiscountList() {
        return discountList;
    }

    public void setDiscountList(List<Discount> discountList) {
        this.discountList = discountList;
    }

    public void deletePos(int deletePos) {

        discountList.remove(deletePos);

        notifyItemRemoved(deletePos);

        notifyItemRangeChanged(deletePos, discountList.size());

    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    public class DiscountViewHolder extends RecyclerView.ViewHolder {

        TextView discountNameTextView;

        TextView discountAmtTextView;

        TextView discountAmtOrPercentTextView;

        AppCompatTextView isDefaultImageView;

        ImageButton editButton;

        SwipeLayout swipeLayout;

        TextView desTextView;

        ImageButton deleteBtn;

        View view;

        String nodes;

        public DiscountViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            desTextView = (TextView) itemView.findViewById(R.id.desc);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            editButton = (ImageButton) itemView.findViewById(R.id.edit_discount_in_discount_item_view);
            discountNameTextView = (TextView) itemView.findViewById(R.id.discount_name_tv);
            discountAmtTextView = (TextView) itemView.findViewById(R.id.discount_amt_tv);
            discountAmtOrPercentTextView = (TextView) itemView.findViewById(R.id.percent_or_amt_textView);
            isDefaultImageView = (AppCompatTextView) itemView.findViewById(R.id.is_defalut_imgv);

            deleteBtn = (ImageButton) itemView.findViewById(R.id.delete);


            nodes = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.no_description}).getString(0);
        }

    }
}
