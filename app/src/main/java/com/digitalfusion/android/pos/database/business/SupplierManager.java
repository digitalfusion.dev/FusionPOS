package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.adapters.rvadapterforreports.CustomerSupplier;
import com.digitalfusion.android.pos.database.model.Supplier;
import com.digitalfusion.android.pos.database.dao.SupplierDAO;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 9/5/16.
 */
public class SupplierManager {

    private Context context;
    private SupplierDAO supplierDAO;

    public SupplierManager(Context context) {
        this.context = context;
        supplierDAO = SupplierDAO.getSupplierDapInstance(context);
    }

    public Long addNewSupplier(String name, String businessName, String address, String phoneNo, InsertedBooleanHolder flag) {
        return supplierDAO.addNewSupplier(name, businessName, address, phoneNo, flag);
    }

    public List<Supplier> getAllSuppliers(int startLimit, int endLimit) {
        return supplierDAO.getAllSuppliers(startLimit, endLimit, 1);
    }

    public Supplier getSupplierById(Long id) {
        return supplierDAO.getSupplierById(id);
    }

    public boolean deleteSupplier(Long id) {
        return supplierDAO.deleteSupplier(id);
    }

    public boolean checkSupplierBalance(Long id) {
        return supplierDAO.checkSupplierBalance(id);
    }

    public List<CustomerSupplier> getSuppliers(int startLimit, int endLimit) {
        return new CustomerSupplier().wrapSupplier(supplierDAO.getAllSuppliers(startLimit, endLimit, 2));
    }

    public List<Supplier> getAllSuppliersOutstand() {
        return supplierDAO.getAllSuppliersOutstand();
    }


    public ArrayList<Supplier> getAllSuppliersArrayList() {
        return supplierDAO.getAllSuppliersArrayList();
    }

    public boolean checkSupplierAlreadyExists(String name, String businessName) {
        return supplierDAO.checkSupplierAlreadyExists(name);
    }

    public Long findIDByNameAndBusinessName(String name, String businessName) {
        return supplierDAO.findIDByNameAndBusinessName(name, businessName);
    }

    public Long findIDByBusinessName(String businessName) {
        return supplierDAO.findIDByBusinessName(businessName);
    }

    public Long findIDByName(String name) {
        return supplierDAO.findIDByName(name);
    }

    public boolean updateSupplierByID(String name, String businessName, String address, String phoneNo, Long id) {
        return supplierDAO.updateSupplierByID(name, businessName, address, phoneNo, id);
    }

    public List<Supplier> getAllSuppliersByNameOnSearch(int startLimit, int endLimit, String queryStr) {
        return supplierDAO.getAllSuppliersByNameOnSearch(startLimit, endLimit, queryStr, 2);
    }

    public List<Supplier> getAllActiveSuppliersByNameOnSearch(int startLimit, int endLimit, String queryStr) {
        return supplierDAO.getAllSuppliersByNameOnSearch(startLimit, endLimit, queryStr, 1);
    }
}
