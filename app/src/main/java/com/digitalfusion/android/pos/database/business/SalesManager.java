package com.digitalfusion.android.pos.database.business;

import android.content.Context;
import android.util.Log;

import com.digitalfusion.android.pos.database.dao.CustomerDAO;
import com.digitalfusion.android.pos.database.dao.sales.DeliveryDAO;
import com.digitalfusion.android.pos.database.dao.sales.PickupDAO;
import com.digitalfusion.android.pos.database.dao.sales.RefundDAO;
import com.digitalfusion.android.pos.database.dao.sales.SalesDAO;
import com.digitalfusion.android.pos.database.dao.sales.SalesDetailDAO;
import com.digitalfusion.android.pos.database.dao.sales.SalesHoldDAO;
import com.digitalfusion.android.pos.database.dao.sales.SalesHoldDetailDAO;
import com.digitalfusion.android.pos.database.model.SaleDelivery;
import com.digitalfusion.android.pos.database.model.SalePickup;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.database.model.SalesHeader;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.InvoiceNoGenerator;

import java.util.List;

/**
 * Created by MD002 on 8/29/16.
 */
public class SalesManager {
    private SalesDAO salesDAO;
    private SalesHoldDAO salesHoldDAO;
    private SalesDetailDAO salesDetailDAO;
    private SalesHoldDetailDAO salesHoldDetailDAO;
    private Context context;
    private PickupDAO pickupDAO;
    private DeliveryDAO deliveryDAO;
    private boolean flag;
    private InvoiceNoGenerator invoiceNoGenerator;
    private CustomerDAO customerDAO;
    private Long customerID;
    private RefundDAO refundDAO;

    public SalesManager(Context context) {
        this.context = context;
        salesDAO = SalesDAO.getSalesDaoInstance(context);
        salesDetailDAO = SalesDetailDAO.getSalesDetailDaoInstance(context);
        salesHoldDetailDAO = SalesHoldDetailDAO.getSaleHoldDetailsDaoInstance(context);
        pickupDAO = PickupDAO.getPickupDaoInstance(context);
        deliveryDAO = DeliveryDAO.getDeliveryDaoInstance(context);
        invoiceNoGenerator = new InvoiceNoGenerator(context);
        customerDAO = CustomerDAO.getCustomerDaoInstance(context);
        refundDAO = RefundDAO.getRefundDaoInstance(context);
        salesHoldDAO = SalesHoldDAO.getSalesDaoInstance(context);
    }

    private String getSalesInvoiceNo() {
        return invoiceNoGenerator.getInvoiceNo(AppConstant.SALE_INVOICE_NO);
    }

    public String addNewSales(String customerName, String phoneNo, String customerAddress, double totalAmt, String discount, Long taxID, double subTotal, String type,
                              double discountAmt, String remark, double paidAmt, double balance, double taxAmt, double taxRate,
                              String day, String month, String year, List<SalesAndPurchaseItem> salesDetailVOs,
                              SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID, Long userId , String changeBalance) {
        //Log.e("type","ioa "+type);

        String invoiceNo = invoiceNoGenerator.getInvoiceNo(AppConstant.SALE_INVOICE_NO);

        if (customerDAO.checkCustomerAlreadyExists(customerName)) {
            customerID = customerDAO.findIdByName(customerName);
            if (phoneNo != null && phoneNo.length() > 0) {
                customerDAO.updateCustomer(phoneNo, customerAddress, customerID);
            }
        } else {
            //customerID = customerDAO.addNewCustomer(customerName, deliveryView.getAddress(), phoneNo, 0.0, new InsertedBooleanHolder());
            customerID = customerDAO.addNewCustomer(customerName,customerAddress,phoneNo,0.0,new InsertedBooleanHolder());
        }

        if (type.equalsIgnoreCase(SaleType.Sales.toString())) {
            Log.w("here", "check Sale ");
            flag = salesDAO.addNewSales(invoiceNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, taxType, salesDetailVOs, discountID, userId , changeBalance);
        } else if (type.equalsIgnoreCase(SaleType.Pickup.toString())) {
            //Log.e("oaiejaoij","pio");
            Log.w("here", "check pickup");
            flag = salesDAO.addNewSalesWithPickup(invoiceNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, salesDetailVOs, pickupView);
        } else {
            Log.w("here", "check Delivery");
            flag = salesDAO.addNewSalesWithDelivery(invoiceNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, salesDetailVOs, deliveryView);
        }
        if (flag) {
            invoiceNoGenerator.updateInvoiceNextNum();
            return invoiceNo;
        }
        return null;
    }

    public boolean addHeldSales(String invoiceNo, String customerName, String phoneNo,String customerAddress, double totalAmt, String discount, Long taxID, double subTotal, String type,
                                double discountAmt, String remark, double paidAmt, double balance, double taxAmt, double taxRate,
                                String day, String month, String year, List<SalesAndPurchaseItem> salesDetailVOs,
                                SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID, Long userId , String changeBalance) {
        //Log.e("type","ioa "+type);

        if (customerDAO.checkCustomerAlreadyExists(customerName)) {


            customerID = customerDAO.findIdByName(customerName);
            if (phoneNo != null && phoneNo.length() > 0) {
                customerDAO.updateCustomer(phoneNo, customerAddress, customerID);
            }
        } else {

            //customerID = customerDAO.addNewCustomer(customerName, deliveryView.getAddress(), phoneNo, 0.0, new InsertedBooleanHolder());
            customerID = customerDAO.addNewCustomer(customerName, customerAddress, phoneNo, 0.0, new InsertedBooleanHolder());
        }


        if (type.equalsIgnoreCase(SaleType.Sales.toString())) {
            Log.w("here", "check Sale ");
            flag = salesDAO.addNewSales(invoiceNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, taxType, salesDetailVOs, discountID, userId , changeBalance);
        } else if (type.equalsIgnoreCase(SaleType.Pickup.toString())) {
            //Log.e("oaiejaoij","pio");
            Log.w("here", "check pickup");
            flag = salesDAO.addNewSalesWithPickup(invoiceNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, salesDetailVOs, pickupView);
        } else {
            Log.w("here", "check Delivery");
            flag = salesDAO.addNewSalesWithDelivery(invoiceNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, salesDetailVOs, deliveryView);
        }
        if (flag) {
            invoiceNoGenerator.updateInvoiceNextNum();
            return true;
        }
        return false;
    }

    public SalesHeader addHeldSalesRest(String invoiceNo, String customerName, String phoneNo, double totalAmt, String discount, Long taxID, double subTotal, String type,
                                        double discountAmt, String remark, double paidAmt, double balance, double taxAmt, double taxRate,
                                        String day, String month, String year, List<SalesAndPurchaseItem> salesDetailVOs,
                                        SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID, Long userId , String changeBalance) {
        //Log.e("type","ioa "+type);
        if (customerDAO.checkCustomerAlreadyExists(customerName)) {

            customerID = customerDAO.findIdByName(customerName);
            if (phoneNo != null && phoneNo.length() > 0) {
                customerDAO.updateCustomer(phoneNo,"", customerID);
            }
        } else {
            customerID = customerDAO.addNewCustomer(customerName, deliveryView.getAddress(), phoneNo, 0.0, new InsertedBooleanHolder());
        }


        flag = salesDAO.addNewSales(invoiceNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, taxType, salesDetailVOs, discountID, userId , changeBalance);

        if (flag) {
            invoiceNoGenerator.updateInvoiceNextNum();
        }

        SalesHeader salesHeader = getSalesHeaderView(findIDBySalesInvoice(invoiceNo));
        salesHeader.setSalesAndPurchaseItemList(salesDetailDAO.getSaleDetailsBySalesID(salesHeader.getId()));

        return salesHeader;
    }

    public SalesHeader addNewSalesRest(String customerName, String phoneNo, double totalAmt, String discount, Long taxID, double subTotal, String type,
                                       double discountAmt, String remark, double paidAmt, double balance, double taxAmt, double taxRate,
                                       String day, String month, String year, List<SalesAndPurchaseItem> salesDetailVOs,
                                       SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID, Long userId , String  changeBalance) {
        //Log.e("type","ioa "+type);
        String voucherNo = invoiceNoGenerator.getInvoiceNo(AppConstant.SALE_INVOICE_NO);

        if (customerDAO.checkCustomerAlreadyExists(customerName)) {
            customerID = customerDAO.findIdByName(customerName);
            if (phoneNo != null && phoneNo.length() > 0) {
                customerDAO.updateCustomer(phoneNo, "",customerID);
            }
        } else {
            customerID = customerDAO.addNewCustomer(customerName, deliveryView == null ? null : deliveryView.getAddress(), phoneNo, 0.0, new InsertedBooleanHolder());
        }

        flag = salesDAO.addNewSales(voucherNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, taxType, salesDetailVOs, discountID, userId,changeBalance);
        if (flag) {
            invoiceNoGenerator.updateInvoiceNextNum();
        }
        SalesHeader salesHeader = getSalesHeaderView(findIDBySalesInvoice(voucherNo));
        salesHeader.setSalesAndPurchaseItemList(salesDetailDAO.getSaleDetailsBySalesID(salesHeader.getId()));
        return salesHeader;
    }

    public boolean holdNewSales(String voucherNo, String customerName, String phoneNo, double totalAmt, String discount, Long taxID, double subTotal, String type,
                                double discountAmt, String remark, double paidAmt, double balance, double taxAmt, double taxRate,
                                String day, String month, String year, List<SalesAndPurchaseItem> salesDetailVOs,
                                SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID , Long userId) {
        //Log.e("type","ioa "+type);

        SalesHoldDAO salesHoldDAO = SalesHoldDAO.getSalesDaoInstance(context);

        if (customerDAO.checkCustomerAlreadyExists(customerName)) {


            customerID = customerDAO.findIdByName(customerName);
            if (phoneNo != null && phoneNo.length() > 0) {
                customerDAO.updateCustomer(phoneNo,"", customerID);
            }
        } else {

            customerID = customerDAO.addNewCustomer(customerName, deliveryView.getAddress(), phoneNo, 0.0, new InsertedBooleanHolder());

        }


        if (type.equalsIgnoreCase(SaleType.Sales.toString())) {
            Log.w("here", "check Sale ");
            flag = salesHoldDAO.holdNewSales(voucherNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, taxType, salesDetailVOs, discountID ,userId);
        }
        //        } else if (type.equalsIgnoreCase(SaleType.Pickup.toString())) {
        //            //Log.e("oaiejaoij","pio");
        //            Log.w("here", "check pickup");
        //            flag = salesDAO.addNewSalesWithPickup(voucherNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, salesDetailVOs, pickupView);
        //        } else {
        //            Log.w("here", "check Delivery");
        //            flag = salesDAO.addNewSalesWithDelivery(voucherNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, salesDetailVOs, deliveryView);
        //        }
        if (flag) {
            invoiceNoGenerator.updateInvoiceNextNum();
        }
        return flag;
    }

    //Server HoldNewSales
   public SalesHeader holdNewSales(String customerName, String phoneNo, double totalAmt, String discount, Long taxID, double subTotal, String type,
                                    double discountAmt, String remark, double paidAmt, double balance, double taxAmt, double taxRate,
                                    String day, String month, String year, List<SalesAndPurchaseItem> salesDetailVOs,
                                    SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID ,Long userId) {

        //Log.e("type","ioa "+type);
        String       voucherNo    = invoiceNoGenerator.getInvoiceNo(AppConstant.SALE_INVOICE_NO);
        SalesHoldDAO salesHoldDAO = SalesHoldDAO.getSalesDaoInstance(context);

        if (customerDAO.checkCustomerAlreadyExists(customerName)) {


            customerID = customerDAO.findIdByName(customerName);
            if (phoneNo != null && phoneNo.length() > 0) {
                customerDAO.updateCustomer(phoneNo,"", customerID);
            }
        } else {

            customerID = customerDAO.addNewCustomer(customerName, deliveryView.getAddress(), phoneNo, 0.0, new InsertedBooleanHolder());

        }

        flag = salesHoldDAO.holdNewSales(voucherNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, taxType, salesDetailVOs, discountID , userId);

        //        } else if (type.equalsIgnoreCase(SaleType.Pickup.toString())) {
        //            //Log.e("oaiejaoij","pio");
        //            Log.w("here", "check pickup");
        //            flag = salesDAO.addNewSalesWithPickup(voucherNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, salesDetailVOs, pickupView);
        //        } else {
        //            Log.w("here", "check Delivery");
        //            flag = salesDAO.addNewSalesWithDelivery(voucherNo, DateUtility.makeDate(year, month, day), customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, taxRate, salesDetailVOs, deliveryView);
        //        }

        if (flag) {
            invoiceNoGenerator.updateInvoiceNextNum();
        }

        SalesHeader salesHeader = getHoldSalesHeaderView(
                findIDBySalesHoldInvoice(voucherNo));
        salesHeader.setSalesAndPurchaseItemList(salesHoldDetailDAO.getSaleHoldDetailsBySalesID(salesHeader.getId()));

        return salesHeader;
    }

    public SalesHeader getSalesHeaderView(Long id) {
        return salesDAO.getSalesBySalesID(id);
    }


    public SalesHeader getHoldSalesHeaderView(Long id) {
        return salesHoldDAO.getSalesBySalesID(id);
    }

    public List<SalesHistory> getAllSaleTransactions(int startLimit, int endLimit, String startDate, String endDate, Long  userId) {
        return salesDAO.getAllSales(startLimit, endLimit, new InsertedBooleanHolder(), startDate, endDate, userId);
    }


    public List<SalesHistory>
    getAllSalesHold(int startLimit, int endLimit, String startDate, String endDate, InsertedBooleanHolder flag , String userId) {

        return salesHoldDAO.getAllSalesHold(startLimit, endLimit, flag, startDate, endDate, userId);
    }

    public SalesHistory getSaleHistoryViewById(Long id) {
        return salesDAO.getSalesHistoryViewByID(id);
    }

    public SalesHistory getHoldSaleHistoryViewById(Long id) {
        return salesHoldDAO.getSalesHistoryViewByID(id);
    }

    public List<SalesHistory> getAllSaleCancels(int startLimit, int endLimit, InsertedBooleanHolder flag, String startDate, String endDate) {
        return salesDAO.getAllSaleCancels(startLimit, endLimit, flag, startDate, endDate);
    }

    public List<SalesAndPurchaseItem> getSaleDetailsBySalesID(Long id) {
        return salesDetailDAO.getSaleDetailsBySalesID(id);
    }

    public List<SalesAndPurchaseItem> getSaleDetailsHoldBySalesID(Long id) {

        return salesHoldDetailDAO.getSaleHoldDetailsBySalesID(id);
    }

    public SalePickup getPickInfoBySalesID(Long salesID, InsertedBooleanHolder flag) {
        return pickupDAO.getPickInfoBySalesID(salesID, flag);
    }

    public SaleDelivery getDeliveryInfoBySalesID(Long salesID, InsertedBooleanHolder flag) {
        return deliveryDAO.getDeliveryInfoBySalesID(salesID, flag);
    }

    public boolean updateHoldSalesBySalesID(Long id, String date, String customerName, String phoneNo, double totalAmt, String discount, Long taxID, double subTotal,
                                            double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year, String type, double taxRate,
                                            List<SalesAndPurchaseItem> updateDetailList, List<Long> deletedIdList, SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID) {

        SalesHoldDAO salesHoldDAO = SalesHoldDAO.getSalesDaoInstance(context);

        if (customerDAO.checkCustomerAlreadyExists(customerName)) {

            Log.w("here", "check already customer name");
            customerID = customerDAO.findIdByName(customerName);

            if (phoneNo != null && phoneNo.length() > 0) {
                customerDAO.updateCustomer(phoneNo,"", customerID);
            }

        } else {
            Log.w("here", "check  add customer name");
            customerID = customerDAO.addNewCustomer(customerName, deliveryView.getAddress(), phoneNo, 0.0, new InsertedBooleanHolder());

        }

        return salesHoldDAO.updateSalesBySalesID(id, date, customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, type, taxRate, updateDetailList,
                deletedIdList, pickupView, deliveryView, taxType, discountID);
    }

    public SalesHeader updateHoldSalesBySalesIDRest(Long id, String date, String customerName, String phoneNo, double totalAmt, String discount, Long taxID, double subTotal,
                                                    double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year, String type, double taxRate,
                                                    List<SalesAndPurchaseItem> updateDetailList, List<Long> deletedIdList, SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID) {

        SalesHoldDAO salesHoldDAO = SalesHoldDAO.getSalesDaoInstance(context);

        if (customerDAO.checkCustomerAlreadyExists(customerName)) {

            Log.w("here", "check already customer name");
            customerID = customerDAO.findIdByName(customerName);

            if (phoneNo != null && phoneNo.length() > 0) {
                customerDAO.updateCustomer(phoneNo,"", customerID);
            }


        } else {
            Log.w("here", "check  add customer name");
            customerID = customerDAO.addNewCustomer(customerName, deliveryView.getAddress(), phoneNo, 0.0, new InsertedBooleanHolder());

        }

        salesHoldDAO.updateSalesBySalesID(id, date, customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, type, taxRate, updateDetailList,
                deletedIdList, pickupView, deliveryView, taxType, discountID);


        SalesHeader salesHeader = getHoldSalesHeaderView(id);
        salesHeader.setSalesAndPurchaseItemList(salesHoldDetailDAO.getSaleHoldDetailsBySalesID(salesHeader.getId()));

        return salesHeader;
    }

    public boolean updateSalesBySalesID(Long id, String date, String customerName, String phoneNo, String customerAddress, double totalAmt, String discount, Long taxID, double subTotal,
                                        double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year, String type, double taxRate,
                                        List<SalesAndPurchaseItem> updateDetailList, List<Long> deletedIdList, SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID, Long currentUserID , String changeBalance) {


        if (customerDAO.checkCustomerAlreadyExists(customerName)) {

            Log.w("here", "check already customer name");
            customerID = customerDAO.findIdByName(customerName);

            if (phoneNo != null && phoneNo.length() > 0) {
                customerDAO.updateCustomer(phoneNo,customerAddress, customerID);
            }

            if(customerAddress != null && customerAddress.length() > 0)
            {
                customerDAO.updateCustomer(phoneNo,customerAddress, customerID);
            }

        } else {
            Log.w("here", "check  add customer name");
            //customerID = customerDAO.addNewCustomer(customerName, deliveryView.getAddress(), phoneNo, 0.0, new InsertedBooleanHolder());
            customerID = customerDAO.addNewCustomer(customerName, customerAddress, phoneNo, 0.0, new InsertedBooleanHolder());
        }


        return salesDAO.updateSalesBySalesID(id, date, customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, type, taxRate, updateDetailList,
                deletedIdList, pickupView, deliveryView, taxType, discountID , currentUserID ,changeBalance);
    }

    public boolean updateHoldToSalesBySalesID(Long id, String date, String customerName, String phoneNo, double totalAmt, String discount, Long taxID, double subTotal,
                                              double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year, String type, double taxRate,
                                              List<SalesAndPurchaseItem> updateDetailList, List<Long> deletedIdList, SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID) {

        if (customerDAO.checkCustomerAlreadyExists(customerName)) {

            Log.w("here", "check already customer name");
            customerID = customerDAO.findIdByName(customerName);

            if (phoneNo != null && phoneNo.length() > 0) {
                customerDAO.updateCustomer(phoneNo, "",customerID);
            }

        } else {
            Log.w("here", "check  add customer name");
            customerID = customerDAO.addNewCustomer(customerName, deliveryView.getAddress(), phoneNo, 0.0, new InsertedBooleanHolder());

        }

        return salesDAO.updateHoldToSalesBySalesID(id, date, customerID, totalAmt, discount, taxID, subTotal, discountAmt, remark, paidAmt, balance, taxAmt, day, month, year, type, taxRate, updateDetailList,
                deletedIdList, pickupView, deliveryView, taxType, discountID);
    }

    public List<SalesHistory> getSaleVoucherCustomerNameOnSearch(String searchStr, int startLimit, int endLimit) {
        return salesDAO.getSaleByVoucherCustomerNameOnSearch(searchStr, startLimit, endLimit);
    }

    public List<SalesHistory> getSalesOnSearchWithVoucherNoOrCustomer(int startLimit, int endLimit, String searchStr) {
        return salesDAO.getSalesWithVoucherNoOrCustomer(startLimit, endLimit, searchStr);
    }

    public List<SalesHistory> getHoldSalesWithVoucherNoOrCustomer(int startLimit, int endLimit, String searchStr , String userId) {
        return salesDAO.getHoldSalesWithVoucherNoOrCustomer(startLimit, endLimit, searchStr , userId);
    }

    public List<SalesHistory> getSaleByVoucherNoOnSearch(String voucherNo, int startLimit, int endLimit) {
        return salesDAO.getSaleByVoucherNoOnSearch(voucherNo, startLimit, endLimit);
    }

    public List<SalesHistory> getSaleByCustomerNameOnSearch(String customerName, int startLimit, int endLimit) {
        return salesDAO.getSaleByCustomerNameOnSearch(customerName, startLimit, endLimit);
    }

    public String getRefundInvoiceNo() {
        return invoiceNoGenerator.getInvoiceNo("Refund");
    }

    public boolean addNewRefund(Long salesID, Long customerID, Double amount, String remark, String date, String day, String month, String year, String voucherNo) {
        flag = refundDAO.addNewRefund(salesID, customerID, amount, remark, date, day, month, year, voucherNo);
        if (flag) {
            invoiceNoGenerator.updateInvoiceNextNum();
        }
        return flag;
    }

    public boolean deleteRefund(Long id) {
        return refundDAO.deleteRefund(id);
    }

    public Long findRefundIdBySalesID(Long salesID) {
        return refundDAO.findRefundIdBySalesID(salesID);
    }

    public Long findIDBySalesInvoice(String invoiceNo) {
        return salesDAO.findIDBySalesInvoice(invoiceNo);
    }

    public long findIDBySalesHoldInvoice(String invoceNo) {
        return salesDAO.findIDBySalesHoldInvoice(invoceNo);
    }

    public boolean isPaymentExists(Long salesID) {
        return salesDAO.isPaymentExists(salesID);
    }

    public boolean deleteSales(Long id) {
        return salesDAO.deleteSales(id);
    }

    public boolean deleteHoldSales(Long id) {
        return salesHoldDAO.deleteSales(id);
    }

    public enum SaleType {
        Sales, Delivery, Pickup, DeliveryCancel, PickupCancel
    }
}
