package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Supplier;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 11/2/16.
 */

public class SupplierAutoCompleteAdapter extends ArrayAdapter<Supplier> {


    List<Supplier> suggestion;

    List<Supplier> searchList;

    ArrayList<Supplier> tempSupplierArrayList;

    List<Supplier> supplierList;

    Supplier supplier;

    Context context;

    int lenght = 0;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {

            String str = ((Supplier) resultValue).getName();

            supplier = (Supplier) resultValue;

            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            Log.w("constraint", constraint + " SSS");

            if (constraint != null) {
                List<Supplier> filterList = new ArrayList<>();


                for (Supplier vehicle : tempSupplierArrayList) {

                    Log.w("herer", vehicle.getName() + " SSS " + constraint.toString());

                    if (vehicle.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {

                        filterList.add(vehicle);

                    }

                }
                lenght = constraint.length();

                FilterResults filterResults = new FilterResults();

                filterResults.values = filterList;

                filterResults.count = filterList.size();

                return filterResults;

            } else {

                return new FilterResults();

            }

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            List<Supplier> suppliers = (ArrayList<Supplier>) results.values;

            if (results != null && results.count > 0) {
                clear();

                addAll(suppliers);

                notifyDataSetChanged();

            }
        }
    };

    public SupplierAutoCompleteAdapter(Context context, ArrayList<Supplier> objects) {

        super(context, -1, objects);

        this.context = context;

        this.supplierList = objects;

        tempSupplierArrayList = (ArrayList<Supplier>) objects.clone();

        suggestion = new ArrayList<>();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomerAutoCompleteAdapter.ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.customer_auto_complete_suggest_view, parent, false);

        }

        viewHolder = new CustomerAutoCompleteAdapter.ViewHolder(convertView);

        Supplier supplier = getItem(position);

        //  if (suggestionList.size() > 0) {

        Spannable spanText = new SpannableString(supplier.getName());

        spanText.setSpan(new ForegroundColorSpan(getContext().getResources()
                .getColor(R.color.accent)), 0, lenght, 0);

        viewHolder.customerName.setText(supplier.getName());

        //}

        return convertView;

    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    public List<Supplier> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(ArrayList<Supplier> supplierList) {

        this.supplierList = supplierList;

        tempSupplierArrayList = (ArrayList<Supplier>) supplierList.clone();

        suggestion = new ArrayList<>();

    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    static class ViewHolder {

        TextView customerName;

        public ViewHolder(View itemView) {

            this.customerName = (TextView) itemView.findViewById(R.id.customer_name);

        }

    }

}