package com.digitalfusion.android.pos.fragments.outstanding;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.util.TabUtil;
import com.example.searchview.MaterialSearchView;

/**
 * Created by MD003 on 10/18/16.
 */

public class PayableAndReceivableTabFragment extends Fragment {

    private TabLayout tabLayout;

    private View mainLayoutView;

    private PayableFragment payableList;

    private ReceivableFragment receivableList;

    private MaterialSearchView searchView;

    private Customer customer;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.outstanding_tab_fragments, null);

        context = getContext();

        String outstanding = context.getTheme().obtainStyledAttributes(new int[]{R.attr.outstanding}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(outstanding);

        tabLayout = (TabLayout) mainLayoutView.findViewById(R.id.tab);

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);
        MainActivity.setCurrentFragment(this);
        setHasOptionsMenu(true);

        setupTabLayout();

        return mainLayoutView;
    }

    private void setupTabLayout() {

        payableList = new PayableFragment();

        receivableList = new ReceivableFragment();
        String receivable = context.getTheme().obtainStyledAttributes(new int[]{R.attr.receivable}).getString(0);
        String payable    = context.getTheme().obtainStyledAttributes(new int[]{R.attr.payable}).getString(0);
        tabLayout.addTab(tabLayout.newTab().setCustomView(TabUtil.renderTabView(mainLayoutView.getContext(), receivable)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(TabUtil.renderTabView(mainLayoutView.getContext(), payable)));

        MainActivity.replacingTabFragment(receivableList);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setCurrentTabFragment(int tabPostion) {

        switch (tabPostion) {

            case 0:
                MainActivity.replacingTabFragment(receivableList);
                break;

            case 1:
                MainActivity.replacingTabFragment(payableList);
                break;
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.main, menu);

        MenuItem item = menu.findItem(R.id.action_search);

        searchView.setMenuItem(item);

        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);

    }

}
