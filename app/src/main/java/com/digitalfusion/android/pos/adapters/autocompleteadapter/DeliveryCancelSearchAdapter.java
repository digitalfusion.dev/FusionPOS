package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.SalesHistory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 12/21/16.
 */

public class DeliveryCancelSearchAdapter extends ArrayAdapter<SalesHistory> {


    public int lenght = 0;
    public String queryText = "";
    List<SalesHistory> suggestion;
    SalesHistory stockAutoCompleteView;

    Context context;

    Filter filter;

    public DeliveryCancelSearchAdapter(Context context, Filter filter) {

        super(context, -1);

        this.context = context;

        suggestion = new ArrayList<>();

        this.filter = filter;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SaleHistorySearchAdapter.ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.sale_history_search_suggest_view, parent, false);

        }

        viewHolder = new SaleHistorySearchAdapter.ViewHolder(convertView);

        if (suggestion.size() > 0 && !suggestion.isEmpty()) {
            if (suggestion.get(position).getVoucherNo().toLowerCase().startsWith(queryText.toLowerCase())) {

                Spannable spanText = new SpannableString("#" + suggestion.get(position).getVoucherNo());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 1, lenght + 1, 0);

                viewHolder.voucherNoTextView.setText(spanText);

            } else if (queryText.startsWith("#") && suggestion.get(position).getVoucherNo().toLowerCase().contains(queryText.substring(1, queryText.length()).toLowerCase())) {

                Spannable spanText = new SpannableString("#" + suggestion.get(position).getVoucherNo());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.voucherNoTextView.setText(spanText);
            } else {

                viewHolder.voucherNoTextView.setText(suggestion.get(position).getVoucherNo());

            }
            // viewHolder.customerNameTextView.setText(salesHistoryViewList.get(position).getItemName());

            if (suggestion.get(position).getCustomer().toLowerCase().startsWith(queryText.toLowerCase())) {

                Spannable spanText = new SpannableString(suggestion.get(position).getCustomer());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.customerNameTextView.setText(spanText);

            } else {

                viewHolder.customerNameTextView.setText(suggestion.get(position).getCustomer());

            }
        }


        return convertView;

    }

    @Override
    public int getCount() {
        return suggestion.size();
    }

    public List<SalesHistory> getSuggestion() {

        return suggestion;

    }

    public void setSuggestion(List<SalesHistory> suggestion) {

        this.suggestion = suggestion;

    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public SalesHistory getSelectedItem() {
        return stockAutoCompleteView;
    }

    static class ViewHolder {

        TextView voucherNoTextView;

        TextView customerNameTextView;

        public ViewHolder(View itemView) {

            this.customerNameTextView = (TextView) itemView.findViewById(R.id.voucher_no);

            this.voucherNoTextView = (TextView) itemView.findViewById(R.id.customer_name);

        }

    }


}
