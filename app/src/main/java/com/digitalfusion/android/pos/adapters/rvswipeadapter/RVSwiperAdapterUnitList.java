package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.Unit;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD003 on 8/24/16.
 */
public class RVSwiperAdapterUnitList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private List<Unit> unitList;

    private ClickListener editClickListener;
    private ClickListener deleteClickListener;

    public RVSwiperAdapterUnitList(List<Unit> unitList) {
        this.unitList = unitList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.unit_item_view, parent, false);

        return new UnitViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof UnitViewHolder) {

            final UnitViewHolder unitViewHolder = (UnitViewHolder) holder;

            POSUtil.makeZebraStrip(unitViewHolder.itemView, position);

            unitViewHolder.unitTextView.setText(unitList.get(position).getUnit());

            if (unitList.get(position).getDescription() != null && !unitList.get(position).getDescription().equalsIgnoreCase("")) {

                unitViewHolder.unitDescTextView.setText(unitList.get(position).getDescription());
            } else {
                unitViewHolder.unitDescTextView.setText(null);
                unitViewHolder.unitDescTextView.setHint(unitViewHolder.nodes);
            }


            unitViewHolder.editTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editClickListener != null) {

                        editClickListener.onClick(position);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                unitViewHolder.swipeLayout.close();
                            }
                        }, 500);
                    }

                }
            });

            unitViewHolder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteClickListener != null) {
                        unitViewHolder.swipeLayout.close();
                        deleteClickListener.onClick(position);
                    }
                }
            });

            mItemManger.bindView(unitViewHolder.view, position);

        }

    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    @Override
    public int getItemCount() {
        return unitList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public List<Unit> getUnitList() {
        return unitList;
    }

    public void setUnitList(List<Unit> unitList) {
        this.unitList = unitList;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {

        this.editClickListener = editClickListener;

    }

    public class UnitViewHolder extends RecyclerView.ViewHolder {

        TextView unitTextView;

        TextView unitDescTextView;

        ImageButton editTextView;

        ImageButton deleteBtn;

        View view;

        SwipeLayout swipeLayout;

        String nodes;

        public UnitViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            deleteBtn = (ImageButton) itemView.findViewById(R.id.delete);

            unitTextView = (TextView) itemView.findViewById(R.id.unit_name_in_unit_item_view);

            unitDescTextView = (TextView) itemView.findViewById(R.id.unit_desc_in_unit_item_view);

            editTextView = (ImageButton) itemView.findViewById(R.id.edit_unit);

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);


            nodes = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.no_description}).getString(0);
        }

    }
}