package com.digitalfusion.android.pos.fragments.reportfragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForOutstandingReport;
import com.digitalfusion.android.pos.database.business.CustomerManager;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.business.SupplierManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SimpleReportFragmentWithDateFilterSearch extends Fragment implements Serializable {
    private TextView dateFilterTextView;


    private RVAdapterForFilter rvAdapterForDateFilter;
    private ParentRVAdapterForReports rvAdapterForReport;

    private MaterialDialog dateFilterDialog;

    private List<String> filterList;

    private String reportName;
    private View mainLayoutView;
    private Context context;

    private RecyclerView recyclerView;

    private ReportManager reportManager;

    private List<ReportItem> reportItemList;
    //private ReportItem reportItemView;
    private List<Double> yValList;

    private String startDate;
    private String endDate;
    private int startLimit;
    private int endLimit;

    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;

    private String allTrans;

    private String customRange;

    private MaterialDialog customRangeDialog;

    private TextView startDateTextView, endDateTextView;

    private String customStartDate,
            customEndDate;

    private TextView traceDate;

    private Button customRangeOkBtn, customRangeCancelBtn;

    private DatePickerDialog startDatePickerDialog;

    private LinearLayout dateFilterLayout;
    private Long custSupID;

    //private MaterialSearchView searchView;
    private CustomerManager customerManager;

    //private CustomerSearchAdapter customerSearchAdapter;

    //private SupplierSearchAdapter supplierSearchAdapter;
    private SupplierManager supplierManager;
    //private TextView headerDescTextView;
    private String headerDesc;

    public SimpleReportFragmentWithDateFilterSearch() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_simple_report_fragment_with_date_filter_search, container, false);

        context = getContext();

        //setHasOptionsMenu(true);

        if (getArguments() != null) {
            reportName = getArguments().getString("reportType");
            custSupID = getArguments().getLong("ID");
        }
        if (reportName.equalsIgnoreCase("customer payment history")) {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.customer_payment_history}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        } else if (reportName.equalsIgnoreCase("supplier payment history")) {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.supplier_payment_history}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        }


        loadIngUI();

        //searchView.showSearch(false);
        return mainLayoutView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //searchView.showSearch(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        Log.e("ini", "var");
        initializeVariables();

        configFilters();

        buildDateFilterDialog();

        setDateFilterTextView(allTrans);

        buildingCustomRangeDialog();

        clickListeners();


        new LoadProgressDialog().execute("");

    }

    private void clickListeners() {
        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                dateFilterDialog.dismiss();
                if (filterList.get(position).equalsIgnoreCase(thisMonth)) {

                    startDate = DateUtility.getThisMonthStartDate();

                    endDate = DateUtility.getThisMonthEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {


                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {


                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));
                } else if (filterList.get(position).equalsIgnoreCase(allTrans)) {

                    startDate = "000000";

                    endDate = "999999";

                    new LoadProgressDialog().execute("");

                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {

                    customRangeDialog.show();

                }


            }
        });

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String startDate = DateUtility.makeDateFormatWithSlash(customStartDate);
                String endDate   = DateUtility.makeDateFormatWithSlash(customEndDate);

                //String startDayDes[]= DateUtility.dayDes(startDate);

                //String startYearMonthDes= DateUtility.monthYearDes(startDate);


                //String endDayDes[]= DateUtility.dayDes(endDate);

                //String endYearMonthDes= DateUtility.monthYearDes(endDate);

                dateFilterTextView.setText(startDate + " - " + endDate);

                new LoadProgressDialog().execute("");
                customRangeDialog.dismiss();
            }
        });

        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRangeDialog.dismiss();
            }
        });

        //        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        //            @Override
        //            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //                Log.e("search view", "on click");
        //
        //                if (reportName.equalsIgnoreCase("customer payment history")){
        //
        //                    custSupID = customerSearchAdapter.getSuggestionList().get(position).getId();
        //
        //                    headerDesc = customerSearchAdapter.getSuggestionList().get(position).getName();
        //                    String saleoutstanding= context.getTheme().obtainStyledAttributes(new int[]{R.attr.sales_outstanding_voucher_of_the_customer}).getString(0);
        //
        //                    headerDescTextView.setText(saleoutstanding+" " + headerDesc);
        //
        //                } else if (reportName.equalsIgnoreCase("supplier payment history")){
        //
        //                    custSupID = supplierSearchAdapter.getSuggestionList().get(position).getId();
        //
        //                    headerDesc = supplierSearchAdapter.getSuggestionList().get(position).getName();
        //
        //                    String purchaseoutstanding= context.getTheme().obtainStyledAttributes(new int[]{R.attr.purchase_outstanding_voucher_of_the_supplier}).getString(0);
        //
        //                    headerDescTextView.setText(purchaseoutstanding+" " + headerDesc);
        //                }
        //                Log.e("header decs", headerDesc);
        //
        //                headerDescTextView.setClickable(false);
        //                searchView.closeSearch();
        //
        //
        //                new CreateReportTask().execute("");
        //
        //            }
        //        });
        //
        //        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
        //            @Override
        //            public boolean onQueryTextSubmit(String query) {
        //                return false;
        //            }
        //
        //            @Override
        //            public boolean onQueryTextChange(String newText) {
        //
        //                return false;
        //            }
        //        });
        //
        //        headerDescTextView.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //                searchView.showSearch();
        //                searchView.showKeyboard(headerDescTextView);
        //            }
        //        });
    }

    private void configFilters() {
        TypedArray mAll = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all});

        TypedArray mthisMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month});

        TypedArray mlastMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month});

        TypedArray mthisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year});

        TypedArray mlastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year});

        TypedArray mcustomRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_range});

        allTrans = mAll.getString(0);

        thisMonth = mthisMonth.getString(0);

        lastMonth = mlastMonth.getString(0);

        thisYear = mthisYear.getString(0);

        lastYear = mlastYear.getString(0);

        //thisMonth=mthisMonth.getString(0);

        customRange = mcustomRange.getString(0);


        filterList = new ArrayList<>();

        filterList.add(allTrans);

        filterList.add(thisMonth);

        filterList.add(lastMonth);

        filterList.add(thisYear);

        filterList.add(lastYear);

        filterList.add(customRange);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });


    }

    public void buildingCustomRangeDialog() {
        buildingCustomRangeDatePickerDialog();
        String date = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range}).getString(0);
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(date)
                .customView(R.layout.custome_range, true).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.negativeText("Cancel")
                //.positiveText("OK")
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    private void setDateFilterTextView(String msg) {

        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {

        String name = ThemeUtil.getString(context, R.attr.filter_by_date);
        dateFilterDialog = new MaterialDialog.Builder(context).

                title(name)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))

                .build();

    }

    private void loadIngUI() {

        Log.e("load", "ui");
        dateFilterTextView = (TextView) mainLayoutView.findViewById(R.id.date_filter);

        dateFilterLayout = (LinearLayout) mainLayoutView.findViewById(R.id.date_filter_layout);

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.recycler_view);

        //searchView=(MaterialSearchView)(this.getActivity()).findViewById(R.id.search_view);
        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        //searchView.setCursorDrawable(attributeResourceId);

        //headerDescTextView = (TextView) mainLayoutView.findViewById(R.id.header_text_view);
    }

    private void initializeVariables() {

        reportItemList = new ArrayList<>();

        reportManager = new ReportManager(context);

        setDatesAndLimits();

        //custSupID = 0l;

        if (reportName.equalsIgnoreCase("customer payment history")) {
            Log.e("customer", "customer");

            customerManager = new CustomerManager(context);

            //customerSearchAdapter = new CustomerSearchAdapter(context, customerManager);

            //searchView.setAdapter(customerSearchAdapter);

            //String searchedResult= context.getTheme().obtainStyledAttributes(new int[]{R.attr.there_is_no_searched_result_for_customer}).getString(0);
            //headerDescTextView.setText(searchedResult);

        } else if (reportName.equalsIgnoreCase("supplier payment history")) {

            supplierManager = new SupplierManager(context);

            //supplierSearchAdapter = new SupplierSearchAdapter(context, supplierManager);

            //            searchView.setAdapter(supplierSearchAdapter);
            //
            //            String searchedResult= context.getTheme().obtainStyledAttributes(new int[]{R.attr.there_is_no_searched_result_for_supplier}).getString(0);
            //            headerDescTextView.setText(searchedResult);
        }

        //headerDescTextView.setClickable(true);

    }

    private void initializeList() {
        Log.e("date", startDate + " " + endDate);
        if (reportName.equalsIgnoreCase("customer payment history")) {
            //Log.e("inventory", "valuation");
            reportItemList = reportManager.customerOutstandingSalesVoucherList(custSupID, startDate, endDate, startLimit, endLimit);

        } else if (reportName.equalsIgnoreCase("supplier payment history")) {

            reportItemList = reportManager.supplierOutstandingPurchaseVoucherList(custSupID, startDate, endDate, startLimit, endLimit);
        }
        Log.e("list ", "size " + reportItemList.size());

    }

    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        //if (!reportItemList.isEmpty()) {
        Log.e("list", "exists");

        recyclerView.setVisibility(View.VISIBLE);
        dateFilterLayout.setVisibility(View.VISIBLE);
        rvAdapterForReport = new RVAdapterForOutstandingReport(reportItemList, 2);
        if (reportName.equalsIgnoreCase("customer payment history")) {
            ((RVAdapterForOutstandingReport) rvAdapterForReport).setCustomerPayment(true);
            ((RVAdapterForOutstandingReport) rvAdapterForReport).setReportOutstandingClickListener(new RVAdapterForOutstandingReport.ClickListener() {
                @Override
                public void onClick(ReportItem reportItem) {
                    Bundle                                bundle                                = new Bundle();
                    SalesOutstandingPaymentDetailFragment salesOutstandingPaymentDetailFragment = new SalesOutstandingPaymentDetailFragment();
                    bundle.putSerializable("sales header", reportItem);
                    salesOutstandingPaymentDetailFragment.setArguments(bundle);
                    // MainActivity.replacingFragment(salesOutstandingPaymentDetailFragment);

                    Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                    addCurrencyIntent.putExtras(bundle);
                    addCurrencyIntent.putExtra("frag", salesOutstandingPaymentDetailFragment);

                    startActivity(addCurrencyIntent);
                }
            });
        } else if (reportName.equalsIgnoreCase("supplier payment history")) {
            ((RVAdapterForOutstandingReport) rvAdapterForReport).setCustomerPayment(false);
            ((RVAdapterForOutstandingReport) rvAdapterForReport).setReportOutstandingClickListener(new RVAdapterForOutstandingReport.ClickListener() {
                @Override
                public void onClick(ReportItem reportItem) {

                    Log.e("enter", "yes");

                    Bundle                                   bundle                                   = new Bundle();
                    PurchaseOutstandingPaymentDetailFragment purchaseOutstandingPaymentDetailFragment = new PurchaseOutstandingPaymentDetailFragment();
                    bundle.putSerializable("purchase header", reportItem);
                    purchaseOutstandingPaymentDetailFragment.setArguments(bundle);
                    // MainActivity.replacingFragment(purchaseOutstandingPaymentDetailFragment);
                    Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                    addCurrencyIntent.putExtras(bundle);
                    addCurrencyIntent.putExtra("frag", purchaseOutstandingPaymentDetailFragment);

                    startActivity(addCurrencyIntent);

                }
            });
        }

        Log.e("r list ", "size " + reportItemList.size());
        recyclerView.setAdapter(rvAdapterForReport);
        //}else {
        //            if (headerDesc != null) {
        //                String noOutstanding= context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_outstanding_voucher_available_for}).getString(0);
        //
        //                headerDescTextView.setText(noOutstanding + headerDesc);
        //            }
        //            headerDescTextView.setClickable(true);
        //            dateFilterLayout.setVisibility(View.GONE);
        //            recyclerView.setVisibility(View.GONE);
        //        }
    }

    private void setDatesAndLimits() {
        startDate = "000000";
        endDate = "999999";
        startLimit = 0;
        endLimit = 10;
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
            Log.e("initialze", "list" + custSupID);
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String label = "";

            String    wait          = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {

            configRecyclerView();

            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    //    @Override
    //    public boolean onOptionsItemSelected(MenuItem item) {
    //        // Handle action bar item clicks here. The action bar will
    //        // automatically handle clicks on the Home/Up button, so long
    //        // as you specify a parent activity in AndroidManifest.xml.
    //        int id = item.getItemId();
    //
    //        //noinspection SimplifiableIfStatement
    //
    //        return super.onOptionsItemSelected(item);
    //    }
    //
    //    @Override
    //    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    //
    //        getActivity().getMenuInflater().inflate(R.menu.main, menu);
    //
    //        MenuItem item = menu.findItem(R.id.action_search);
    //
    //        searchView.setMenuItem(item);
    //
    //        super.onCreateOptionsMenu(menu, inflater);
    //    }

}
