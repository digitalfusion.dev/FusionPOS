package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.database.model.Device;
import com.digitalfusion.android.pos.util.AppConstant;

/**
 * Created by lyx on 3/29/18.
 */

public class AuthorizationManager {
    private Context context;
    private ApiDAO apiDAO;

    public AuthorizationManager(Context context) {
        this.context = context;
        this.apiDAO = ApiDAO.getApiDAOInstance(context);
    }

    public void insertDevice(Device device) {
        Device tempDevice = apiDAO.getDevice(device.getSerialNo());
        if (tempDevice == null) {
            device.setPermission(AppConstant.PERMISSION_ALLOWED);
            apiDAO.insertDevice(device);
        }
    }

    public boolean isDeviceLimitReached() {
        int count = apiDAO.getAllowedDeviceCount();

        return count >= 6;
    }

    /**
     * Check the authorization of the device and generate a response code.
     * @param serialNo of the device
     */
    public int generateResponseCode(String serialNo) {
        String permission = apiDAO.getDevicePermissionStatus(serialNo);
        int responseCode = AppConstant.RESPONSE_NOT_FOUND;

        if (permission != null) {
            if (permission.equalsIgnoreCase(AppConstant.PERMISSION_ALLOWED)) {
                responseCode = AppConstant.RESPONSE_ALLOWED;
            } else if (permission.equalsIgnoreCase(AppConstant.PERMISSION_BLOCKED)) {
                responseCode = AppConstant.RESPONSE_BLOCKED;
            }
        }

        return  responseCode;
    }

    public void updateDevicePermission(Device device) {
        apiDAO.updateDevicePermission(device);
    }

    public Long getDeviceId(String serialNo) {
        return apiDAO.getDeviceId(serialNo) ;
    }

    public String getDeviceSerialNo(Long deviceId) {
        return apiDAO.getDeviceSerialNo(deviceId);
    }
}
