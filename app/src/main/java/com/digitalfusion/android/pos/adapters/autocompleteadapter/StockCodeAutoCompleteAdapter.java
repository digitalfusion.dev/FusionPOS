package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.model.StockAutoComplete;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 9/8/16.
 */
public class StockCodeAutoCompleteAdapter extends ArrayAdapter<StockAutoComplete> {

    List<StockAutoComplete> suggestion;

    List<StockAutoComplete> searchList;

    Callback callback;

    StockManager stockManager;

    int lenght = 0;

    StockAutoComplete stockAutoComplete;

    Context context;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((StockAutoComplete) resultValue).getCodeNo();
            stockAutoComplete = (StockAutoComplete) resultValue;

            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {


            if (constraint != null) {

                searchList.clear();

                searchList = stockManager.findStockByCodeNoForAutoComplete(constraint.toString());

             /*   if(suggestionList.size()==1){

                    if(callback!=null){

                        callback.onOneResut(suggestionList.get(0));

                        return new FilterResults();

                    }else {


                        lenght=constraint.length();

                        FilterResults filterResults = new FilterResults();
                        filterResults.values = suggestionList;
                        filterResults.count = suggestionList.size();
                        return filterResults;
                    }


                }else {*/

                lenght = constraint.length();

                FilterResults filterResults = new FilterResults();
                filterResults.values = searchList;
                filterResults.count = searchList.size();
                return filterResults;
                //   }


            } else {

                return new FilterResults();

            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<StockAutoComplete> filterList = (ArrayList<StockAutoComplete>) results.values;

            suggestion = searchList;
            if (results != null && results.count > 0) {
                clear();
                for (StockAutoComplete vehicle1 : filterList) {
                    add(vehicle1);
                    notifyDataSetChanged();
                }
            }
        }
    };

    public StockCodeAutoCompleteAdapter(Context context, StockManager stockManager, Callback callback) {

        super(context, -1);

        this.context = context;

        this.stockManager = stockManager;

        suggestion = new ArrayList<>();
        searchList = new ArrayList<>();

        this.callback = callback;

    }

    public StockCodeAutoCompleteAdapter(Context context, StockManager stockManager) {

        super(context, -1);

        this.context = context;

        this.stockManager = stockManager;

        suggestion = new ArrayList<>();

        this.callback = callback;
        searchList = new ArrayList<>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.stock_item_auto_complete_view1, parent, false);

        }

        viewHolder = new ViewHolder(convertView);

        if (!suggestion.isEmpty() && suggestion.size() > 0) {


            Spannable spanText = new SpannableString(suggestion.get(position).getCodeNo());

            spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                    .getColor(R.color.accent)), 0, lenght, 0);

            viewHolder.codeTextView.setText(spanText);


            // viewHolder.customerNameTextView.setText(salesHistoryViewList.get(position).getItemName());

            viewHolder.nameTextView.setText(suggestion.get(position).getItemName());

            Log.w("sugg size", suggestion.size() + " SS");


        }

        return convertView;

    }

    @Override
    public int getCount() {
        return suggestion.size();
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    public StockAutoComplete getStockItem() {
        return stockAutoComplete;
    }

    public void setCustomer(StockAutoComplete stockAutoComplete) {

        this.stockAutoComplete = stockAutoComplete;

    }

    public List<StockAutoComplete> getSuggestion() {
        return suggestion;
    }

    public interface Callback {

        void onOneResut(StockAutoComplete stockAutoComplete);

    }

    static class ViewHolder {

        TextView codeTextView;

        TextView nameTextView;

        public ViewHolder(View itemView) {

            this.nameTextView = (TextView) itemView.findViewById(R.id.name);

            this.codeTextView = (TextView) itemView.findViewById(R.id.code);

        }

    }

}
