package com.digitalfusion.android.pos.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.ExpenseAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.ExpenseSearhAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForExpenseManagerList;
import com.digitalfusion.android.pos.database.business.ExpenseManager;
import com.digitalfusion.android.pos.database.model.ExpenseName;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.ExpenseIncome;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.FusionToast;
import com.example.searchview.MaterialSearchView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD001 on 9/5/16.
 */
public class ExpenseManagerListFragment extends Fragment {

    private View mainLayout;


    private FloatingActionButton addNewExpenseFab;

    private RecyclerView expenseManagerListRecyclerView;

    private ExpenseManager expenseManagerBusiness;    ///////////

    private Context context;

    private List<ExpenseIncome> expenseManagerVOList;    //////////

    private RVSwipeAdapterForExpenseManagerList rvSwipeAdapterForExpenseManagerList;        //////////

    private MaterialDialog addNewExpenseManagerMaterialDialog;

    //UI component for addNewExpenseManagerDialog
    private TextView addDateExpenseTextView;


    private AutoCompleteTextView expenseManagerNameTextInputEditTextMd;


    private EditText expenseManagerAmountTextInputEditTextMd;


    private EditText expenseManagerRemarkTextInputEditTextMd;

    private LinearLayout dateLLMd;


    private Button saveExpenseBtnMd, cancelExpenseBtnMd;


    private MaterialDialog viewRemarkExpenseManagerMaterialDialog;


    //UI component for ViewRemarkDialog
    private TextView viewRemark;

    private Button okBtnViewRemark;

    private MaterialSearchView searchView;

    //Values
    private String incomeOrExpenseName;

    private String expenseManagerType;

    private Double expenseOrIncomeAmount;

    private String expenseOrIncomeRemark;

    private DatePickerDialog datePickerDialog;

    private int day;

    private int month;

    private int year;

    private String date;

    private Calendar now;

    private DatePickerDialog editDatePickerDialog;

    private int editDay;

    private int editMonth;

    private int editYear;

    private String editDate;

    private Calendar editCalendar;

    private int editPosition;

    private int viewRemarkPosition;

    private ArrayList<ExpenseName> expenseNameList;

    private ArrayList<ExpenseName> incomNameVOList;

    private ExpenseAutoCompleteAdapter expenseAutoCompleteAdapter;

    private ExpenseAutoCompleteAdapter incomeAutoCompleteAdapter;

    private ExpenseSearhAdapter searchAdapter;

    private MaterialDialog filterDialog;

    private TextView filterTextView;

    private RVAdapterForFilter rvAdapterForFilter;

    private List<String> filterList;

    private String startDate;

    private String endDate;

    private DatePickerDialog customeDatePickerDialog;

    private Calendar calendar;

    private boolean editFlag;

    private boolean darkmode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.expense_manager, null);

        context = getContext();


        darkmode = POSUtil.isNightMode(context);

        calendar = Calendar.getInstance();

        startDate = "000000000000";

        endDate = "9999999999999999";


        MainActivity.setCurrentFragment(this);

        TypedArray allTrans = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all});

        TypedArray thisMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month});

        TypedArray lastMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month});

        TypedArray thisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year});

        TypedArray lastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year});

        TypedArray customRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_range});

        TypedArray customDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_date});

        filterList = new ArrayList<>();

        filterList.add(allTrans.getString(0));

        filterList.add(thisMonth.getString(0));

        filterList.add(lastMonth.getString(0));

        filterList.add(thisYear.getString(0));

        filterList.add(lastYear.getString(0));

        filterList.add(customRange.getString(0));

        filterList.add(customDate.getString(0));

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();


        buildAddNewExpenseManagerDialog();

        buildViewRemarkExpenseManagerDialog();


        loadUI();

        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                filterDialog.show();

            }
        });
        configCustomDatePickerDialog();

        okBtnViewRemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewRemarkExpenseManagerMaterialDialog.dismiss();
            }
        });

        return mainLayout;

    }

    private void buildDateFilterDialog() {

        TypedArray filterByDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date});

        filterDialog = new MaterialDialog.Builder(context).

                title(filterByDate.getString(0))

                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))

                .build();
    }

    private void configCustomDatePickerDialog() {
        customeDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


                        startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        String date = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        String dayDes[] = DateUtility.dayDes(date);

                        String yearMonthDes = DateUtility.monthYearDes(date);

                        filterTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

                        loadExpenseHistory(0, 100);

                        rvSwipeAdapterForExpenseManagerList.setExpenseManagerVOList(expenseManagerVOList);

                        rvSwipeAdapterForExpenseManagerList.notifyDataSetChanged();
                    }
                },
                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)
        );

        if (darkmode)
            customeDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        customeDatePickerDialog.setThemeDark(darkmode);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        MainActivity activity = (MainActivity) getActivity();

        expenseManagerBusiness = new ExpenseManager(context);    //////////

        now = Calendar.getInstance();

        editCalendar = Calendar.getInstance();

        day = now.get(Calendar.DAY_OF_MONTH);

        month = now.get(Calendar.MONTH) + 1;

        year = now.get(Calendar.YEAR);

        expenseNameList = new ArrayList<>();

        expenseNameList = (ArrayList<ExpenseName>) expenseManagerBusiness.getAllExpenseNames();

        incomNameVOList = (ArrayList<ExpenseName>) expenseManagerBusiness.getAllIncomeNames();

        searchAdapter = new ExpenseSearhAdapter(context, expenseManagerBusiness);

        configRecyclerView();

        setttingOnClickListener();

        setFilterTextView(filterList.get(0));

        searchView.setAdapter(searchAdapter);

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                expenseManagerVOList = new ArrayList<>();

                expenseManagerVOList.add(searchAdapter.getSuggestion().get(position));

                rvSwipeAdapterForExpenseManagerList.setExpenseManagerVOList(expenseManagerVOList);

                rvSwipeAdapterForExpenseManagerList.notifyDataSetChanged();

                searchView.closeSearch();

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                loadExpenseHistory(0, 100, query.toString());

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        // expenseAutoCompleteAdapter=new ExpenseAutoCompleteAdapter(context, R.layout.expense_auto_complete_suggest_view, R.id.expense_name,expenseNameList);

        // incomeAutoCompleteAdapter=new ExpenseAutoCompleteAdapter(context, R.layout.expense_auto_complete_suggest_view, R.id.expense_name,incomNameVOList);

        expenseManagerNameTextInputEditTextMd.setThreshold(1);


        expenseManagerNameTextInputEditTextMd.setAdapter(expenseAutoCompleteAdapter);

        expenseManagerNameTextInputEditTextMd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    expenseManagerAmountTextInputEditTextMd.requestFocus();
                }
                return false;
            }
        });

        expenseManagerNameTextInputEditTextMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //expenseManagerNameTextInputEditTextMd.setText(expenseAutoCompleteAdapter.getExpenseName().getName());

            }
        });


        rvSwipeAdapterForExpenseManagerList.setViewDetailClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                Bundle bundle = new Bundle();

                bundle.putSerializable("expense", expenseManagerVOList.get(postion));

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.EXPENSE_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);

            }
        });


        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (position == 0) {

                    setFilterTextView(filterList.get(position));

                    startDate = "000000000000";

                    endDate = "9999999999999999";

                    loadExpenseHistory(0, 100);

                    rvSwipeAdapterForExpenseManagerList.setExpenseManagerVOList(expenseManagerVOList);

                    rvSwipeAdapterForExpenseManagerList.notifyDataSetChanged();

                } else if (position == 1) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisMonthStartDate();

                    endDate = DateUtility.getThisMonthEndDate();

                    loadExpenseHistory(0, 100);

                    rvSwipeAdapterForExpenseManagerList.setExpenseManagerVOList(expenseManagerVOList);

                    rvSwipeAdapterForExpenseManagerList.notifyDataSetChanged();

                } else if (position == 2) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    loadExpenseHistory(0, 100);

                    rvSwipeAdapterForExpenseManagerList.setExpenseManagerVOList(expenseManagerVOList);

                    rvSwipeAdapterForExpenseManagerList.notifyDataSetChanged();

                } else if (position == 3) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    loadExpenseHistory(0, 100);

                    rvSwipeAdapterForExpenseManagerList.setExpenseManagerVOList(expenseManagerVOList);

                    rvSwipeAdapterForExpenseManagerList.notifyDataSetChanged();

                } else if (position == 4) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    loadExpenseHistory(0, 100);

                    rvSwipeAdapterForExpenseManagerList.setExpenseManagerVOList(expenseManagerVOList);

                    rvSwipeAdapterForExpenseManagerList.notifyDataSetChanged();

                } else if (position == 5) {

                    setFilterTextView(filterList.get(position));

                } else if (position == 6) {

                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "customeDate");

                }

                filterDialog.dismiss();
            }
        });

    }

    private void setttingOnClickListener() {

        dateLLMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editFlag) {
                    editDatePickerDialog.show(getActivity().getFragmentManager(), "show");
                } else {
                    datePickerDialog.show(getActivity().getFragmentManager(), "show");
                }


            }
        });


        addNewExpenseFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resetDialogData();

                String newExpense = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_new_expense}).getString(0);
                addNewExpenseManagerMaterialDialog.setTitle(newExpense);

                addNewExpenseManagerMaterialDialog.show();

            }
        });


        rvSwipeAdapterForExpenseManagerList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                editPosition = postion;
                String editExpense = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_expense}).getString(0);

                addNewExpenseManagerMaterialDialog.setTitle(editExpense);

                if (expenseManagerVOList.get(editPosition).isIncome()) {


                } else {
                    editFlag = true;
                    setDataEditExpenseDialog(expenseManagerVOList.get(postion));
                    addNewExpenseManagerMaterialDialog.show();
                }


            }
        });

        addNewExpenseManagerMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                editFlag = false;
            }
        });

        rvSwipeAdapterForExpenseManagerList.setViewRemarkClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                viewRemarkPosition = postion;

                setDataRemarkDialog();

                viewRemarkExpenseManagerMaterialDialog.show();

            }
        });


        saveExpenseBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkValidationExpense()) {

                    if (editFlag) {

                        if (checkValidationExpense()) {
                            expenseManagerType = ExpenseIncome.Type.Expense.toString();

                            getExpenseValuesFromView();

                            InsertedBooleanHolder status = new InsertedBooleanHolder();

                            Log.w("income ", "position " + editPosition);

                            status.setInserted(expenseManagerBusiness.updateExpenseOrIncome(editDate, expenseOrIncomeAmount, incomeOrExpenseName,
                                    expenseManagerType, expenseOrIncomeRemark, Integer.toString(editDay), Integer.toString(editMonth),
                                    Integer.toString(editYear), expenseManagerVOList.get(editPosition).getId()));  ////////

                            if (status.isInserted()) {

                                String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.expense_edited_successfully}).getString(0);

                                addNewExpenseManagerMaterialDialog.dismiss();
                                FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);

                                refreshExpenseManagerList();

                            }
                        }


                    } else {

                        expenseManagerType = ExpenseIncome.Type.Expense.toString();

                        getExpenseValuesFromView();

                        InsertedBooleanHolder status = new InsertedBooleanHolder();

                        status.setInserted(expenseManagerBusiness.addNewIncomeOrExpense(incomeOrExpenseName, expenseManagerType, expenseOrIncomeAmount,
                                expenseOrIncomeRemark, Integer.toString(day), Integer.toString(month), Integer.toString(year), date));  ////////

                        if (status.isInserted()) {

                            String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_expense_added_successfully}).getString(0);

                            addNewExpenseManagerMaterialDialog.dismiss();
                            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);

                            refreshExpenseManagerList();

                        }

                    }

                }
            }
        });
        cancelExpenseBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewExpenseManagerMaterialDialog.dismiss();
            }
        });

    }


    private void setFilterTextView(String msg) {

        filterTextView.setText(msg);

    }

    public void buildDatePickerDialog() {

        datePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        day = dayOfMonth;

                        month = monthOfYear + 1;

                        year = year;

                        configDateUI();

                    }
                },

                now.get(Calendar.YEAR),

                now.get(Calendar.MONTH),

                now.get(Calendar.DAY_OF_MONTH)
        );

        if (darkmode)
            datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        datePickerDialog.setThemeDark(darkmode);

    }

    public void buildEditDatePickerDialog() {

        editDatePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        editDay = dayOfMonth;

                        editMonth = monthOfYear + 1;

                        editYear = year;

                        configEditDateUI();

                    }
                },

                editCalendar.get(Calendar.YEAR),

                editCalendar.get(Calendar.MONTH),

                editCalendar.get(Calendar.DAY_OF_MONTH)

        );

        if (darkmode)
            editDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        editDatePickerDialog.setThemeDark(darkmode);
    }


    public void buildAddNewExpenseManagerDialog() {

        String expense = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_new_expense}).getString(0);
        // TypedArray save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        addNewExpenseManagerMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.new_expense, true)
                .title(expense)
                // .positiveText(save.getString(0))
                // .negativeText(cancel.getString(0))
                .build();

    }


    public void buildViewRemarkExpenseManagerDialog() {

        TypedArray remark = context.getTheme().obtainStyledAttributes(new int[]{R.attr.remark});
        //TypedArray ok = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ok});

        viewRemarkExpenseManagerMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.view_remark, true)
                .title(remark.getString(0))
                //.negativeText(ok.getString(0))
                .build();

    }

    private void loadExpenseHistory(int startLimit, int endLimit, String query) {

        expenseManagerVOList = expenseManagerBusiness.getAllExpenseNameSearch(query, startLimit, endLimit);

    }

    private void loadExpenseHistory(int startLimit, int endLimit) {

        expenseManagerVOList = expenseManagerBusiness.getAllExpenses(0, 100, startDate, endDate);


        Log.w("start date", startDate);

        Log.w("end date", endDate);

        Log.w("sixe", expenseManagerVOList.size() + " ss");

        ///////////

    }


    public boolean checkValidationExpense() {

        boolean status = true;

        if (expenseManagerNameTextInputEditTextMd.getText().toString().trim().length() < 1) {
            String eName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_name}).getString(0);
            expenseManagerNameTextInputEditTextMd.setError(eName);
            status = false;

        }

        if (expenseManagerAmountTextInputEditTextMd.getText().toString().trim().length() < 1) {
            String eAmount = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_amount}).getString(0);

            expenseManagerAmountTextInputEditTextMd.setError(eAmount);

            status = false;

        }

        return status;

    }


    public void resetDialogData() {

        day = now.get(Calendar.DAY_OF_MONTH);

        month = now.get(Calendar.MONTH) + 1;

        year = now.get(Calendar.YEAR);

        buildDatePickerDialog();

        configDateUI();

        expenseManagerNameTextInputEditTextMd.setText(null);

        expenseManagerNameTextInputEditTextMd.setError(null);

        expenseManagerAmountTextInputEditTextMd.setText(null);

        expenseManagerAmountTextInputEditTextMd.setError(null);

        expenseManagerRemarkTextInputEditTextMd.setText(null);

    }

    public void setDataEditExpenseDialog() {

        expenseManagerNameTextInputEditTextMd.setText(expenseManagerVOList.get(editPosition).getName());

        editYear = Integer.parseInt(expenseManagerVOList.get(editPosition).getYear());

        editMonth = Integer.parseInt(expenseManagerVOList.get(editPosition).getMonth());

        editDay = Integer.parseInt(expenseManagerVOList.get(editPosition).getDay());

        editCalendar.set(editYear, editMonth - 1, editDay);

        configEditDateUI();


        expenseManagerAmountTextInputEditTextMd.setText(expenseManagerVOList.get(editPosition).getAmount().toString());/////////

        expenseManagerRemarkTextInputEditTextMd.setText(expenseManagerVOList.get(editPosition).getRemark());////////

        buildEditDatePickerDialog();

    }

    public void setDataEditExpenseDialog(ExpenseIncome expenseIncome) {

        expenseManagerNameTextInputEditTextMd.setText(expenseManagerVOList.get(editPosition).getName());

        editYear = Integer.parseInt(expenseManagerVOList.get(editPosition).getYear());

        editMonth = Integer.parseInt(expenseManagerVOList.get(editPosition).getMonth());

        editDay = Integer.parseInt(expenseManagerVOList.get(editPosition).getDay());

        editCalendar.set(editYear, editMonth - 1, editDay);

        configEditDateUI();


        expenseManagerAmountTextInputEditTextMd.setText(expenseManagerVOList.get(editPosition).getAmount().toString());/////////

        expenseManagerRemarkTextInputEditTextMd.setText(expenseManagerVOList.get(editPosition).getRemark());////////

        buildEditDatePickerDialog();

    }


    public void setDataRemarkDialog() {

        viewRemark.setText(expenseManagerVOList.get(viewRemarkPosition).getRemark());

    }

    public void getExpenseValuesFromView() {

        incomeOrExpenseName = expenseManagerNameTextInputEditTextMd.getText().toString().trim();


        expenseOrIncomeAmount = Double.parseDouble(expenseManagerAmountTextInputEditTextMd.getText().toString().trim());

        expenseOrIncomeRemark = expenseManagerRemarkTextInputEditTextMd.getText().toString().trim();

    }


    public void refreshExpenseManagerList() {

        expenseManagerVOList = expenseManagerBusiness.getAllExpenses(0, 1000, "0000000000", "99999999"); //////////

        rvSwipeAdapterForExpenseManagerList.setExpenseManagerVOList(expenseManagerVOList);

        rvSwipeAdapterForExpenseManagerList.notifyDataSetChanged();

    }

    public void configRecyclerView() {

        loadExpenseHistory(0, 100);

        rvSwipeAdapterForExpenseManagerList = new RVSwipeAdapterForExpenseManagerList(expenseManagerVOList);

        expenseManagerListRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        expenseManagerListRecyclerView.setAdapter(rvSwipeAdapterForExpenseManagerList);

    }


    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});

        int attributeResourceId = a.getResourceId(0, 0);

        searchView.setCursorDrawable(attributeResourceId);

    }

    public void loadUI() {

        loadUIFromToolbar();

        filterTextView = (TextView) mainLayout.findViewById(R.id.filter_one);

        expenseManagerListRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.expenseManager_list_rv);

        addNewExpenseFab = (FloatingActionButton) mainLayout.findViewById(R.id.expense);

        addDateExpenseTextView = (TextView) addNewExpenseManagerMaterialDialog.findViewById(R.id.expense_date_in_add_expense_TV);


        saveExpenseBtnMd = (Button) addNewExpenseManagerMaterialDialog.findViewById(R.id.save);

        cancelExpenseBtnMd = (Button) addNewExpenseManagerMaterialDialog.findViewById(R.id.cancel);

        dateLLMd = (LinearLayout) addNewExpenseManagerMaterialDialog.findViewById(R.id.sale_date_in_new_sale_ll);

        expenseManagerNameTextInputEditTextMd = (AutoCompleteTextView) addNewExpenseManagerMaterialDialog.findViewById(R.id.expense_name_in_add_expense_TIET);


        expenseManagerAmountTextInputEditTextMd = (EditText) addNewExpenseManagerMaterialDialog.findViewById(R.id.expense_amount_in_add_expense_TIET);

        expenseManagerRemarkTextInputEditTextMd = (EditText) addNewExpenseManagerMaterialDialog.findViewById(R.id.expense_remark_in_add_expense_TIET);


        viewRemark = (TextView) viewRemarkExpenseManagerMaterialDialog.findViewById(R.id.viewRemark);

        okBtnViewRemark = (Button) viewRemarkExpenseManagerMaterialDialog.findViewById(R.id.ok);

    }


    private void configDateUI() {

        date = DateUtility.makeDate(Integer.toString(year), Integer.toString(month), Integer.toString(day));

        String dayDes[] = DateUtility.dayDes(date);

        String yearMonthDes = DateUtility.monthYearDes(date);

        addDateExpenseTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));


    }

    private void configEditDateUI() {

        editDate = DateUtility.makeDate(Integer.toString(editYear), Integer.toString(editMonth), Integer.toString(editDay));

        String dayDes[] = DateUtility.dayDes(editDate);

        String yearMonthDes = DateUtility.monthYearDes(editDate);

        addDateExpenseTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));
    }

}
