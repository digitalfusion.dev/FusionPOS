package com.digitalfusion.android.pos.database.business;

import android.content.Context;
import android.util.Log;

import com.digitalfusion.android.pos.database.dao.DocumentNumberSettingDAO;
import com.digitalfusion.android.pos.database.dao.MenuDAO;
import com.digitalfusion.android.pos.database.model.Menu;
import com.digitalfusion.android.pos.database.dao.SettingDAO;
import com.digitalfusion.android.pos.database.model.BusinessSetting;
import com.digitalfusion.android.pos.database.model.Discount;
import com.digitalfusion.android.pos.database.model.DocumentNumberSetting;
import com.digitalfusion.android.pos.database.model.Tax;

import java.util.List;

/**
 * Created by MD002 on 8/24/16.
 */
public class SettingManager {
    private SettingDAO settingDAO;
    private DocumentNumberSettingDAO documentNumberSettingDAO;
    private MenuDAO menuDAO;
    private Context context;
    private BusinessSetting businessSetting;
    private Tax tax;
    private List<Tax> taxList;
    private Long id;

    public SettingManager(Context context) {
        this.context = context;
        settingDAO = SettingDAO.getSettingDaoInstance(context);
        documentNumberSettingDAO = DocumentNumberSettingDAO.getDocumentNumberSettingDaoInstance(context);
        menuDAO = MenuDAO.getMenuDaoInstance(context);
        id = 0l;
    }

    public boolean updateDocumentNumberSetting(String prefix, int digit, String menuName, String suffix) {
        id = menuDAO.findIdByname(menuName);
        return documentNumberSettingDAO.updateDocumentNumberSetting(prefix, digit, id, suffix);
    }

    public boolean addBusinessSetting(String businessName, String phoneNo, String email, String website, String street, String valuationMethod, String currency, byte[] logo, String township, String city, String state) {
        return settingDAO.addBusinessSetting(businessName, phoneNo, email, website, street, valuationMethod, 1l, logo, state, city, township);
    }

    public boolean updateBusinessSetting(String businessName, String userName, String businessType, String phoneNo, String email, String website, String street, String valuationMethod, String currency, byte[] logo, String township, String city, String state) {
        return settingDAO.updateBusinessSetting(businessName, userName, businessType, phoneNo, email, website, street, valuationMethod, 1l, logo, township, city, state);

    }

    public boolean updateBusinessSetting(String businessName, String userName, String businessType, String phoneNo, String city, String state) {
        return settingDAO.updateBusinessSetting(businessName, userName, businessType, phoneNo, city, state);

    }

    public DocumentNumberSetting getDocSettingByMenuID(String menuName) {
        id = menuDAO.findIdByname(menuName);
        return documentNumberSettingDAO.getDocSettingByMenuID(id);
    }

    public String getCurrency() {
        //  return settingDAO.getCurrency();

        return "MMK";
    }

    public BusinessSetting getBusinessSetting() {
        return settingDAO.getBusinessSetting();
    }

    public Long addNewTax(String name, Double rate, String type, String description, Integer isDefault) {
        return settingDAO.addNewTax(name, rate, type, description, isDefault);
    }

    public boolean checkTaxNameAlreadyExists(String name) {
        return settingDAO.checkTaxNameAlreadyExists(name);
    }

    public List<Tax> getAllTaxs() {
        return settingDAO.getAllTaxs();
    }

    public boolean updateTax(String name, Double rate, String type, String description, Integer isDefault, Long id) {
        return settingDAO.updateTax(name, rate, type, description, isDefault, id);
    }

    public Double getTaxRateByID(Long id) {
        return settingDAO.getTaxRateByID(id);
    }


    public Tax getDefaultTax() {
        return settingDAO.getDefaultTax();
    }

    public boolean deleteTax(Long id) {
        Log.e("deleted", " deleted");
        return settingDAO.deleteTax(id);
    }

    public List<Menu> getAllMenus() {
        return menuDAO.getAllMenus();
    }

    public Long addNewDiscount(String name, Double amount, Integer isPercent, String description, Integer isDefault) {
        return settingDAO.addNewDiscount(name, amount, isPercent, description, isDefault);
    }

    public List<Discount> getAllDiscounts() {
        return settingDAO.getAllDiscounts();
    }

    public Discount getDefaultDiscount() {
        return settingDAO.getDefaultDiscount();
    }

    public boolean updateDiscount(String name, Double rate, int isPercent, String description, Integer isDefault, Long id) {
        return settingDAO.updateDiscount(name, rate, isPercent, description, isDefault, id);
    }

    public boolean deleteDiscount(Long id) {
        return settingDAO.deleteDiscount(id);
    }

    public boolean checkDiscountNameAlreadyExists(String name) {
        return settingDAO.checkDiscountNameAlreadyExists(name);
    }
}
