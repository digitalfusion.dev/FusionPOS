package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/18/16.
 */
public class UserDAO extends ParentDAO {
    private static ParentDAO userDaoInstance;
    private Context context;
    private IdGeneratorDAO idGeneratorDAO;
    private User user;
    private List<User> userList;

    private UserDAO(Context context) {
        super(context);
        this.context = context;
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        user = new User();
    }

    public static UserDAO getUserDaoInstance(Context context) {
        if (userDaoInstance == null) {
            userDaoInstance = new UserDAO(context);
        }
        return (UserDAO) userDaoInstance;
    }

    public Long addNewUser(String userName, String password, String role, String description, InsertedBooleanHolder isInserted) {
        id = checkUserNameAlreadyExists(userName);
        if (id == null) {
            genID = idGeneratorDAO.getLastIdValue("UserRole");
            genID += 1;
            query = "insert into " + AppConstant.USER_TABLE_NAME + "(" + AppConstant.USER_ID + ", " + AppConstant.USER_USER_NAME + ", " + AppConstant.USER_PASSWORD + ", " +
                    AppConstant.USER_ROLE + ", " + AppConstant.USER_DESCRIPTION + ", " + AppConstant.CREATED_DATE + ") values(?,?,?,?,?,?)";
            databaseWriteTransaction(isInserted);
            try {
                statement = database.compileStatement(query);
                statement.bindLong(1, genID);
                statement.bindString(2, userName);
                statement.bindString(3, password);
                statement.bindString(4, role);
                if (description == null) {
                    statement.bindNull(5);
                } else {
                    statement.bindString(5, description);
                }
                statement.bindString(6, DateUtility.getTodayDate());
                statement.execute();
                statement.clearBindings();
                database.setTransactionSuccessful();
            } catch (SQLiteException e) {
                e.printStackTrace();
            } finally {
                database.endTransaction();
                //if (databaseHelper != null)
                //databaseHelper.close();
            }
            isInserted.setInserted(true);
        }
        return id;
    }

    public Long checkUserNameAlreadyExists(String userName) {
        query = "select id from " + AppConstant.USER_TABLE_NAME + " where " + AppConstant.USER_USER_NAME + " = ? and " + AppConstant.USER_IS_DELETED + " <> 1;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{userName});
            if (cursor.moveToFirst()) {
                return cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            //    if (databaseHelper != null)
            //       databaseHelper.close();
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public boolean updatePassword(String password) {
        genID = idGeneratorDAO.getLastIdValue("UserRole");
        genID += 1;
        query = "update " + AppConstant.USER_TABLE_NAME + " set " + AppConstant.USER_PASSWORD + " = ? where " + AppConstant.USER_ID + " = " + 1l;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{password});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public Long validatePassword(String password) {
        query = "select id from " + AppConstant.USER_TABLE_NAME + " where " + AppConstant.USER_PASSWORD + " = ? and " + AppConstant.USER_IS_DELETED + " <> 1;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{password});
            if (cursor.moveToFirst()) {
                return cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            //    if (databaseHelper != null)
            //       databaseHelper.close();
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private Long checkUserNameAlreadyExistsForUpdate(String userName, Long userID) {
        query = "select id from " + AppConstant.USER_TABLE_NAME + " where " + AppConstant.USER_USER_NAME + " = ? and " + AppConstant.USER_IS_DELETED + " <> 1 and " + AppConstant.USER_ID + " <> " + id;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{userName});
            if (cursor.moveToFirst()) {
                return cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            // if (databaseHelper != null)
            //     databaseHelper.close();
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public List<User> getAllUsers() {
        userList = new ArrayList<>();
        query = "select " + AppConstant.USER_ID + ", " + AppConstant.USER_USER_NAME + ", " + AppConstant.USER_ROLE + ", " + AppConstant.USER_DESCRIPTION + " from " +
                AppConstant.USER_TABLE_NAME + " where " + AppConstant.USER_IS_DELETED + " <> 1 ;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    user = new User();
                    user.setId(cursor.getLong(0));
                    user.setUserName(cursor.getString(1));
                    user.setRole(cursor.getString(2));
                    user.setDescription(cursor.getString(3));
                    userList.add(user);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            // if (databaseHelper != null)
            //databaseHelper.close();
            if (cursor != null)
                cursor.close();
        }
        return userList;
    }

    public User login(String username, String password) {
        databaseReadTransaction(flag);
        user = new User();
        query = "select " + AppConstant.USER_ID + ", " + AppConstant.USER_USER_NAME + ", case when " + AppConstant.USER_PASSWORD + " = ? then " + AppConstant.USER_ROLE + " else '-' end role, " +
                AppConstant.USER_DESCRIPTION + " from " + AppConstant.USER_TABLE_NAME + " where " + AppConstant.USER_USER_NAME + " = ? group by " + AppConstant.USER_ROLE;
        try {
            cursor = database.rawQuery(query, new String[]{password, username});
            if (cursor.moveToFirst()) {
                Log.e("count", cursor.getCount() + "d");
                if (cursor.getCount() > 1) {
                    if (cursor.getString(2) == "-") {
                        cursor.moveToNext();
                    }
                }
                user.setId(cursor.getLong(0));
                user.setUserName(cursor.getString(1));

                Log.w("user name", cursor.getString(2) + " ss");

                user.setRole(cursor.getString(2));
                user.setDescription(cursor.getString(3));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            //databaseHelper.close();
            if (cursor != null)
                cursor.close();
        }
        return user;
    }

    public User findUser(Long id) {
        databaseReadTransaction(flag);
        user = new User();
        query = "select " + AppConstant.USER_ID + ", " + AppConstant.USER_USER_NAME + ", " + AppConstant.USER_ROLE + ", " + AppConstant.USER_DESCRIPTION + " from " + AppConstant.USER_TABLE_NAME +
                " where " + AppConstant.USER_ID + " =  " + id + " and " + AppConstant.USER_IS_DELETED + " <> 1;";
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                user.setId(cursor.getLong(0));
                user.setUserName(cursor.getString(1));

                Log.w("user name", cursor.getString(1) + " ss");

                user.setRole(cursor.getString(2));
                user.setDescription(cursor.getString(3));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            //databaseHelper.close();
            if (cursor != null)
                cursor.close();
        }
        return user;
    }

    public boolean updateUserRole(String userName, String password, String role, String description, Long userID) {
        id = checkUserNameAlreadyExistsForUpdate(userName, userID);
        if (id == null) {
            genID = idGeneratorDAO.getLastIdValue("UserRole");
            genID += 1;
            query = "update " + AppConstant.USER_TABLE_NAME + " set " + AppConstant.USER_USER_NAME + " = ?, " +
                    AppConstant.USER_ROLE + "=?, " + AppConstant.USER_DESCRIPTION + "=?, " + AppConstant.USER_PASSWORD + " = ? where " + AppConstant.USER_ID + " = " + userID;
            databaseWriteTransaction(flag);
            Log.w("here", "hupdage user");
            try {
                database.execSQL(query, new String[]{userName, role, description, password});
                database.setTransactionSuccessful();
            } catch (SQLiteException e) {
                e.printStackTrace();
            } finally {
                database.endTransaction();
            }

        } else {
            flag.setInserted(false);
        }
        return flag.isInserted();
    }

    public boolean deleteUserRole(Long id) {
        query = "update " + AppConstant.USER_TABLE_NAME + " set " + AppConstant.USER_IS_DELETED + " = 1 where " + AppConstant.USER_ID + " = " + id;
        databaseWriteTransaction();
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }
}
