package com.digitalfusion.android.pos.fragments.inventoryfragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwiperAdapterUnitList;
import com.digitalfusion.android.pos.database.business.UnitManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.fragments.settingfragment.AddEditUnitFragment;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.Unit;
import com.digitalfusion.android.pos.views.FusionToast;

import java.util.List;

/**
 * Created by MD003 on 8/24/16.
 */
public class UnitFragment extends Fragment {

    //UI
    private View mainLayoutView;

    private EditText unitNameTextInputEditText;

    private EditText unitDescTextInputEditText;

    private MDButton unitAddMdButton;

    private MDButton unitCancelMdButton;

    private FloatingActionButton addNewUnitFab;

    private MaterialDialog unitMaterialDialog;

    private MaterialDialog unitMaterialDialogEdit;

    private RecyclerView unitListRecyclerView;


    private EditText unitNameTextInputEditTextEdit;

    private EditText unitDescTextInputEditTextEdit;

    private MDButton unitSaveMdButton;


    //value
    private Context context;

    private UnitManager unitManager;

    private RVSwiperAdapterUnitList rvSwiperAdapterUnitList;

    private List<Unit> unitList;


    private String editInitialUnitName;
    private String unitName;
    private String unitDesc;

    private int editPos;

    private int deletePos;

    private MaterialDialog deleteAlertDialog;

    private Button yesDeleteMdButton;

    private MaterialDialog deniedDeleteAlertDialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.unit, null);

        context = getContext();

        TypedArray unit = context.getTheme().obtainStyledAttributes(new int[]{R.attr.unit});
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(unit.getString(0));

        unitManager = new UnitManager(context);

        unitList = unitManager.getAllUnits();

        buildingAddUnitDialog();

        buildingEditUnitDialog();

        loadUI();

        configRecycerView();

        deleteAlertDialog();

        rvSwiperAdapterUnitList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
               /* setInput(unitList.get(postion));

                editPos=postion;

                unitMaterialDialogEdit.show();*/

                Bundle bundle = new Bundle();

                bundle.putSerializable(AddEditUnitFragment.KEY, unitList.get(postion));

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtras(bundle);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_UNIT);

                startActivity(addCurrencyIntent);
            }
        });

        unitListRecyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {

                Log.w("V X", velocityX + " S");

                Log.w("V Y", velocityY + " S");

                if (velocityY > 100) {
                    addNewUnitFab.hide();
                } else if (velocityY < -100) {
                    addNewUnitFab.show();
                }


                return false;
            }
        });

        addNewUnitFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_UNIT);

                startActivity(addCurrencyIntent);

            }
        });

        unitSaveMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkUnitEditValidation()) {

                    unitManager.updateUnit(unitName, unitDesc, unitList.get(editPos).getId());

                    unitList.get(editPos).setUnit(unitName);

                    unitList.get(editPos).setDescription(unitDesc);

                    unitMaterialDialogEdit.dismiss();


                    refreshUnitList();
                    TypedArray unitEditedSuccessfully = context.getTheme().obtainStyledAttributes(new int[]{R.attr.unit_edited_successfully});

                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                }

            }
        });

        unitAddMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkUnitValidation()) {

                    unitManager.addNewUnit(unitName, unitDesc);

                    unitMaterialDialog.dismiss();

                    refreshUnitList();
                    //TypedArray unitAddedSuccessfully=context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_unit_added_successfully});

                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                    //POSUtil.showSnackBar(mainLayoutView, unitAddedSuccessfully.getString(0));

                }

            }
        });

        rvSwiperAdapterUnitList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                deletePos = postion;
                deleteAlertDialog.show();
            }
        });

        yesDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (unitManager.deleteUnit(unitList.get(deletePos).getId())) {
                    unitList.remove(deletePos);

                    rvSwiperAdapterUnitList.setUnitList(unitList);

                    rvSwiperAdapterUnitList.notifyItemRemoved(deletePos);

                    rvSwiperAdapterUnitList.notifyItemRangeChanged(deletePos, unitList.size());

                    deleteAlertDialog.dismiss();
                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                } else {
                    deleteAlertDialog.dismiss();
                    deniedDeleteAlertDialog.show();
                }
            }
        });

        return mainLayoutView;

    }

    @Override
    public void onResume() {
        super.onResume();
        refreshUnitList();
    }

    public void clearInput() {

        unitNameTextInputEditText.setError(null);

        unitNameTextInputEditText.setText(null);

        unitDescTextInputEditText.setText(null);

    }

    public void setInput(Unit unit) {

        unitNameTextInputEditTextEdit.setError(null);

        unitNameTextInputEditTextEdit.setText(unit.getUnit());
        editInitialUnitName = unit.getUnit();

        unitDescTextInputEditTextEdit.setText(unit.getDescription());

    }


    public void refreshUnitList() {

        unitList = unitManager.getAllUnits();

        rvSwiperAdapterUnitList.setUnitList(unitList);

        rvSwiperAdapterUnitList.notifyDataSetChanged();

    }

    public boolean checkUnitValidation() {

        boolean status = true;

        if (unitNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;
            TypedArray pleaseEnterUnitName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_unit_name});

            unitNameTextInputEditText.setError(pleaseEnterUnitName.getString(0));

        } else {

            unitName = unitNameTextInputEditText.getText().toString().trim();
            unitDesc = unitDescTextInputEditText.getText().toString().trim();

            if (unitManager.checkUnitExists(unitName)) {
                status = false;
                TypedArray unitAlreadyExist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.unit_already_exist});

                unitNameTextInputEditText.setError(unitAlreadyExist.getString(0));
            }
        }

        return status;

    }

    public boolean checkUnitEditValidation() {

        boolean status = true;

        if (unitNameTextInputEditTextEdit.getText().toString().trim().length() < 1) {

            status = false;
            TypedArray pleaseEnterUnitName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_unit_name});

            unitNameTextInputEditTextEdit.setError(pleaseEnterUnitName.getString(0));

        } else {

            unitName = unitNameTextInputEditTextEdit.getText().toString().trim();
            unitDesc = unitDescTextInputEditTextEdit.getText().toString().trim();

            if (unitManager.checkUnitExists(unitName) && !editInitialUnitName.equals(unitName)) {
                status = false;
                TypedArray unitAlreadyExist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.unit_already_exist});

                unitNameTextInputEditTextEdit.setError(unitAlreadyExist.getString(0));
            }
        }

        unitName = unitNameTextInputEditTextEdit.getText().toString().trim();

        unitDesc = unitDescTextInputEditTextEdit.getText().toString().trim();

        return status;

    }


    public void configRecycerView() {

        unitListRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        rvSwiperAdapterUnitList = new RVSwiperAdapterUnitList(unitList);

        unitListRecyclerView.setAdapter(rvSwiperAdapterUnitList);

    }

    public void loadUI() {

        unitNameTextInputEditText = (EditText) unitMaterialDialog.findViewById(R.id.unit_name_in_unit_add_dg);

        unitDescTextInputEditText = (EditText) unitMaterialDialog.findViewById(R.id.unit_desc_in_unit_add_dg);

        unitAddMdButton = unitMaterialDialog.getActionButton(DialogAction.POSITIVE);

        unitNameTextInputEditTextEdit = (EditText) unitMaterialDialogEdit.findViewById(R.id.unit_name_in_unit_add_dg);

        unitDescTextInputEditTextEdit = (EditText) unitMaterialDialogEdit.findViewById(R.id.unit_desc_in_unit_add_dg);

        unitSaveMdButton = unitMaterialDialogEdit.getActionButton(DialogAction.POSITIVE);


        unitCancelMdButton = unitMaterialDialog.getActionButton(DialogAction.NEGATIVE);

        unitListRecyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.unit_list_rv);

        addNewUnitFab = (FloatingActionButton) mainLayoutView.findViewById(R.id.add_new_unit);

    }

    private void buildingAddUnitDialog() {

        unitMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.new_unit, true)
                .negativeText("Cancel").positiveText("Save").title("Add Unit").build();


    }

    private void buildingEditUnitDialog() {

        unitMaterialDialogEdit = new MaterialDialog.Builder(context).customView(R.layout.new_unit, true).negativeText("Cancel").positiveText("Save").title("Edit Unit").build();

    }

    private void deleteAlertDialog() {

        //TypedArray no = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        //TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();

        TypedArray areUSureWantToDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete});
        TextView   textView             = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(areUSureWantToDelete.getString(0));

        //((TextView)deleteAlertDialog.findViewById(R.id.md_title)).setText("Are you sure  want to delete?");

        yesDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });


        deniedDeleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.alert_dialog_with_only_ok_button, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
        String   titleText = context.getTheme().obtainStyledAttributes(new int[]{R.attr.denied_unit_delete}).getString(0);
        TextView textView1 = (TextView) deniedDeleteAlertDialog.findViewById(R.id.title);
        textView1.setText(titleText);

        deniedDeleteAlertDialog.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deniedDeleteAlertDialog.dismiss();
            }
        });
    }
}
