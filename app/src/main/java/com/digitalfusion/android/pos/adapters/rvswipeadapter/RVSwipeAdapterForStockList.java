package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD003 on 8/17/16.
 */
public class RVSwipeAdapterForStockList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    protected boolean showLoader = false;
    private List<StockItem> stockItemList;
    private ClickListener editClickListener;
    private ClickListener deleteClickListener;
    private ClickListener viewDetailClickListener;
    private LoaderViewHolder loaderViewHolder;

    private int editPos;

    public RVSwipeAdapterForStockList(List<StockItem> stockItemList) {

        this.stockItemList = stockItemList;

    }

    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            if (stockItemList != null && stockItemList.size() != 0) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;

            }

        }

        return VIEWTYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEWTYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_item_view_in_stock_list_new, parent, false);


            return new StockItemViewHolder(v);

        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            return new LoaderViewHolder(v);
        }


    }

    public ClickListener getViewDetailClickListener() {
        return viewDetailClickListener;
    }

    public void setViewDetailClickListener(ClickListener viewDetailClickListener) {
        this.viewDetailClickListener = viewDetailClickListener;
    }

    public List<StockItem> getStockItemList() {
        return stockItemList;
    }

    public void setStockItemList(List<StockItem> stockItemList) {
        this.stockItemList = stockItemList;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {


        if (viewHolder instanceof LoaderViewHolder) {

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;

            this.loaderViewHolder = loaderViewHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);

        }
        if (viewHolder instanceof StockItemViewHolder) {

            final StockItemViewHolder holder = (StockItemViewHolder) viewHolder;

            // holder.noTextView.setText(Integer.toString(position+1));

            //   POSUtil.makeZebraStrip(holder.itemView,position);

            holder.stockCodeTextView.setText(stockItemList.get(position).getCodeNo());
            holder.itemNameTextView.setText(stockItemList.get(position).getName());
            holder.categoryTextView.setText(stockItemList.get(position).getCategoryName());

            //holder.categoryTextView.setTextColor(Color.parseColor("#1A237E"));

            String unit = "";

            if (stockItemList.get(position).getUnitName() != null && stockItemList.get(position).getUnitName().length() > 0) {
                unit = " " + stockItemList.get(position).getUnitName();
            }
            //   holder.categoryTextView.setBackgroundColor(Color.parseColor("#4CAF50"));
            holder.inventoryQtyTextView.setText(Integer.toString(stockItemList.get(position).getInventoryQty()) + unit);

            holder.totalValueTextView.setText(POSUtil.NumberFormat(stockItemList.get(position).getPurchasePrice()));

            // holder.totalValueTextView.setText(POSUtil.NumberFormat(10000000000.0));


            //   if(stockItemList.get(position).getUnitName()==null||stockItemList.get(position).getUnitID()==null||stockItemList.get(position).getUnitID()==0){
            //                holder.unitTextView.setText("");
            //     }else {
            //        holder.unitTextView.setText(stockItemList.get(position).getUnitName());
            //     }

            holder.editStockTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editPos = position;

                    if (editClickListener != null) {

                        holder.swipeLayout.close();
                        editClickListener.onClick(position);
                    }
                }
            });
            holder.deleteStockbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteClickListener != null) {
                        holder.swipeLayout.close();
                        deleteClickListener.onClick(position);
                    }
                }
            });

            holder.viewDetailImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (viewDetailClickListener != null) {
                        holder.swipeLayout.close();
                        viewDetailClickListener.onClick(position);

                    }

                }
            });


            //  POSUtil.makeZebraStrip(holder.itemView,position);

            mItemManger.bindView(holder.itemView, position);

        } else {
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }

    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    @Override
    public int getItemCount() {
        if (stockItemList == null || stockItemList.size() == 0) {
            return 0;
        } else {
            return stockItemList.size() + 1;
        }

    }

    public void updateItem(StockItem stockItem) {

        //Log.w("here updateRegistration",salesHistoryView.getId()+"");

        // Log.w("position",purchaseHistoryViewList.indexOf(purchaseHistoryView)+" S");

        //Log.w("position",purchaseHistoryViewList.get(purchaseHistoryViewList.indexOf(purchaseHistoryView)).getSupplierName()+" S");

        // Log.w("position",purchaseHistoryView.getSupplierName()+" S");

        stockItemList.set(editPos, stockItem);

        notifyItemChanged(editPos, stockItem);

        //  notifyDataSetChanged();


    }

    public void setShowLoader(boolean showLoader) {
        this.showLoader = showLoader;

        if (loaderViewHolder != null) {
            Log.w("hele", "loader full not null");
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe2;
    }

    public class StockItemViewHolder extends RecyclerView.ViewHolder {
        TextView stockCodeTextView;
        TextView itemNameTextView;
        TextView categoryTextView;
        TextView inventoryQtyTextView;
        TextView totalValueTextView;
        TextView noTextView;

        ImageButton viewDetailImageButton;

        ImageButton editStockTextView;
        ImageButton deleteStockbtn;

        SwipeLayout swipeLayout;


        LinearLayout linearLayout;

        View itemView;

        public StockItemViewHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe2);

            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll);

            noTextView = (TextView) itemView.findViewById(R.id.no);


            viewDetailImageButton = (ImageButton) itemView.findViewById(R.id.view_detail);

            deleteStockbtn = (ImageButton) itemView.findViewById(R.id.delete);
            editStockTextView = (ImageButton) itemView.findViewById(R.id.edit_stock);
            stockCodeTextView = (TextView) itemView.findViewById(R.id.stock_code_in_stock_item_view);
            itemNameTextView = (TextView) itemView.findViewById(R.id.item_name_in_stock_item_view);
            categoryTextView = (TextView) itemView.findViewById(R.id.category_in_stock_item_view);
            inventoryQtyTextView = (TextView) itemView.findViewById(R.id.qty_in_stock_item_view);
            totalValueTextView = (TextView) itemView.findViewById(R.id.total_value_in_stock_item_view);


        }
    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar mProgressBar;

        public View mView;

        public LoaderViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);

        }
    }

}
