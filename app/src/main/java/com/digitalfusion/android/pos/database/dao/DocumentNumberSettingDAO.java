package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import com.digitalfusion.android.pos.database.model.DocumentNumberSetting;
import com.digitalfusion.android.pos.util.AppConstant;

import java.util.List;

/**
 * Created by MD002 on 8/19/16.
 */
public class DocumentNumberSettingDAO extends ParentDAO {
    private static ParentDAO documentNumberSettingDaoInstance;
    private Context context;
    private List<DocumentNumberSetting> documentNumberSettingList;
    private DocumentNumberSetting documentNumberSetting;

    private DocumentNumberSettingDAO(Context context) {
        super(context);
        this.context = context;
        documentNumberSetting = new DocumentNumberSetting();
    }

    public static DocumentNumberSettingDAO getDocumentNumberSettingDaoInstance(Context context) {
        if (documentNumberSettingDaoInstance == null) {
            documentNumberSettingDaoInstance = new DocumentNumberSettingDAO(context);
        }
        return (DocumentNumberSettingDAO) documentNumberSettingDaoInstance;
    }

    public DocumentNumberSetting getDocSettingByMenuID(Long menuID) {
        query = "select " + AppConstant.DOCUMENT_NUMBER_SETTING_ID + ", " + AppConstant.DOCUMENT_NUMBER_SETTING_PREFIX + ", " + AppConstant.DOCUMENT_NUMBER_SETTING_MIN_DIGIT
                + ", " + AppConstant.DOCUMENT_NUMBER_SETTING_NEXT_NUMBER + ", " + AppConstant.DOCUMENT_NUMBER_SETTING_SUFFIX + " from " + AppConstant.DOCUMENT_NUMBER_SETTING_TABLE_NAME + " where " + AppConstant.DOCUMENT_NUMBER_SETTING_MENU_ID
                + " = ?";
        databaseReadTransaction(flag);
        try {
            documentNumberSetting = new DocumentNumberSetting();
            cursor = database.rawQuery(query, new String[]{menuID.toString()});
            if (cursor.moveToFirst()) {
                documentNumberSetting.setId(cursor.getLong(0));
                documentNumberSetting.setPrefix(cursor.getString(1));
                documentNumberSetting.setMindigit(cursor.getInt(2));
                documentNumberSetting.setNextNumber(cursor.getInt(3));
                documentNumberSetting.setSuffix(cursor.getString(4));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();

            if (cursor != null) {
                cursor.close();
            }
        }
        return documentNumberSetting;
    }

    public boolean updateDocumentNumberSetting(String prefix, int digit, Long menuID, String suffix) {
        id = findIDByMenuID(menuID);
        query = "update " + AppConstant.DOCUMENT_NUMBER_SETTING_TABLE_NAME + " set " + AppConstant.DOCUMENT_NUMBER_SETTING_PREFIX + "= ?, " + AppConstant.DOCUMENT_NUMBER_SETTING_MIN_DIGIT
                + " = " + digit + ", " + AppConstant.DOCUMENT_NUMBER_SETTING_SUFFIX + "=? where " + AppConstant.DOCUMENT_NUMBER_SETTING_ID + "=" + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{prefix, suffix});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();

    }

    private Long findIDByMenuID(Long menuID) {
        query = "select " + AppConstant.DOCUMENT_NUMBER_SETTING_ID + " from " + AppConstant.DOCUMENT_NUMBER_SETTING_TABLE_NAME + " where " + AppConstant.DOCUMENT_NUMBER_SETTING_MENU_ID + "=" + menuID;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }

    public boolean updateNextNumber(Long id, int nextNumber) {
        query = "update " + AppConstant.DOCUMENT_NUMBER_SETTING_TABLE_NAME + " set " + AppConstant.DOCUMENT_NUMBER_SETTING_NEXT_NUMBER + "=" + nextNumber + " where "
                + AppConstant.DOCUMENT_NUMBER_SETTING_ID + "=" + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return flag.isInserted();
    }
}
