package com.digitalfusion.android.pos.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;

/**
 * Created by MD003 on 4/7/17.
 */

public class CustomToolbar extends Toolbar {

    private TextView textView;
    private Context context;

    public CustomToolbar(Context context) {
        super(context);
        this.context = context;

        int color = context.obtainStyledAttributes(new int[]{R.attr.titleColorText}).getColor(0, 000000);
        setTitleTextColor(color);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        int color = context.obtainStyledAttributes(new int[]{R.attr.titleColorText}).getColor(0, 000000);
        setTitleTextColor(color);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;


        int color = context.obtainStyledAttributes(new int[]{R.attr.titleColorText}).getColor(0, 000000);
        setTitleTextColor(color);
    }

    @Override
    public void setTitle(CharSequence title) {

        Log.w("setting title in", "custome toolbar");

        if (!TextUtils.isEmpty(title)) {
            if (textView == null) {
                final Context context = getContext();
                textView = (TextView) LayoutInflater.from(context).inflate(R.layout.toolbar_title, null);
                textView.setSingleLine();
                textView.setEllipsize(TextUtils.TruncateAt.END);
            }

            if (textView.getParent() != null) {
                ((ViewGroup) textView.getParent()).removeView(textView);
            }
            addView(textView);
        }
        if (textView != null) {
            textView.setText(title);
        }

        int color = context.obtainStyledAttributes(new int[]{R.attr.titleColorText}).getColor(0, 000000);
        setTitleTextColor(color);
        textView.setTextColor(color);

    }
}
