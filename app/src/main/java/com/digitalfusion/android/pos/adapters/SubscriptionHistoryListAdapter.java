package com.digitalfusion.android.pos.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.information.Subscription;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.util.POSUtil;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by MD002 on 9/4/17.
 */

public class SubscriptionHistoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Subscription> subscriptionList;
    private Context context;
    private ClickListener clickListener;

    public SubscriptionHistoryListAdapter(List<Subscription> subscriptionList, Context context) {
        this.subscriptionList = subscriptionList;
        this.context = context;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setSubscriptionList(List<Subscription> subscriptionList) {
        this.subscriptionList = subscriptionList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.subscription_history_list, parent, false);
        return new SubscriptionHistoryListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        Log.e("bind", "bind");
        SubscriptionHistoryListViewHolder viewHolder = (SubscriptionHistoryListViewHolder) holder;
        String                            status     = subscriptionList.get(position).getLicenseInfo().getLicenseStatus();

        Log.w("STATUS", status + " satas");
        if (status.equalsIgnoreCase("EXPIRED")) {
            viewHolder.licenceStatusTextView.setBackgroundColor(Color.parseColor("#DD2C00"));
        } else if (status.equalsIgnoreCase("ACTIVE")) {
            viewHolder.licenceStatusTextView.setBackgroundColor(Color.parseColor("#4CAF50"));
        } else {
            viewHolder.licenceStatusTextView.setBackgroundColor(Color.parseColor("#FFF1C70E"));
        }
        viewHolder.licenceStatusTextView.setText(status);
        viewHolder.licenceTypeTextView.setText(subscriptionList.get(position).getPaymentInfo().getAgentName());
        viewHolder.dateTextView.setText(new SimpleDateFormat("dd/MM/yyyy").format(subscriptionList.get(position).getPaymentInfo().getDate()));

        viewHolder.durationTextView.setText(subscriptionList.get(position).getLicenseInfo().getDuration());
        viewHolder.amountTextView.setText(POSUtil.NumberFormat(subscriptionList.get(position).getPaymentInfo().getAmount()));

        viewHolder.itemViewLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onClick(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return subscriptionList.size();
    }

    public class SubscriptionHistoryListViewHolder extends RecyclerView.ViewHolder {
        LinearLayout itemViewLinearLayout;
        TextView licenceTypeTextView;
        TextView dateTextView;
        TextView licenceStatusTextView;
        TextView amountTextView;
        TextView durationTextView;

        public SubscriptionHistoryListViewHolder(View itemView) {
            super(itemView);
            durationTextView = itemView.findViewById(R.id.duration);
            licenceTypeTextView = (TextView) itemView.findViewById(R.id.name);
            licenceStatusTextView = (TextView) itemView.findViewById(R.id.license_status);
            dateTextView = (TextView) itemView.findViewById(R.id.date_text_view);
            itemViewLinearLayout = (LinearLayout) itemView.findViewById(R.id.item_view);
            amountTextView = itemView.findViewById(R.id.amount);
        }
    }
}
