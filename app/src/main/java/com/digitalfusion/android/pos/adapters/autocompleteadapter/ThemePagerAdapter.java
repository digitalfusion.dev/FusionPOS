package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.ThemeObj;

import java.util.List;

/**
 * Created by MD003 on 11/23/16.
 */

public class ThemePagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    private List<ThemeObj> themeObjList;

    public ThemePagerAdapter(Context context, @NonNull List<ThemeObj> imageResources) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.themeObjList = imageResources;
    }

    @Override
    public int getCount() {
        return themeObjList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.theme_image_view, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        imageView.setImageResource(themeObjList.get(position).getImageResource());

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
