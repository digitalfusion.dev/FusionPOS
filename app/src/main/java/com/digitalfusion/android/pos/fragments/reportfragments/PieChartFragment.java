package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForPieChart;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.views.Spinner;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.innovattic.font.FontTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PieChartFragment extends Fragment implements Serializable {

    public static int[] colorArr = new int[]{ColorTemplate.rgb("#03A9F4"),
            ColorTemplate.rgb("#CDDC39"),
            ColorTemplate.rgb("#FF9800"),
            ColorTemplate.rgb("#e040fb"),
            ColorTemplate.rgb("#80DEEA"),
            ColorTemplate.rgb("#3F51B5"),
            ColorTemplate.rgb("#FFEB3B"),
            ColorTemplate.rgb("#00BFA5"),
            ColorTemplate.rgb("#757575"),
            ColorTemplate.rgb("#F44336")};
    private ArrayList<Entry> yValues;
    private ArrayList<String> xValues;
    private List<String> filterList;
    private List<ReportItem> reportItemList;

    private Context context;
    private RVAdapterForFilter rvAdapterForDateFilter;
    private RVAdapterForPieChart rvAdapterForPieChart;
    private ReportManager reportManager;
    private DecimalFormat df;

    private String startDate;
    private String endDate;
    private int startLimit;
    private int endLimit;
    private Double totalAmt;
    private float percent;
    private String reportType;
    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;
    private String customRange;

    private TextView dateFilterTextView;
    private MaterialDialog dateFilterDialog;
    private View mainLayoutView;
    private RecyclerView recyclerView;
    private PieChart pieChart;
    private FontTextView noChartDataText;
    private PieDataSet dataSet;
    private MaterialDialog customRangeDialog;
    private TextView startDateTextView, endDateTextView;
    private String customStartDate,
            customEndDate;
    private TextView traceDate;
    private Button customRangeOkBtn, customRangeCancelBtn;
    private DatePickerDialog startDatePickerDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_pie_chart, container, false);
        context = getContext();
        loadIngUI();
        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            reportType = getArguments().getString("reportType");
        }

        String title;
        if (reportType.equalsIgnoreCase("top sales by category")) {
            title = ThemeUtil.getString(context, R.attr.top_sales_by_category);
        } else {
            title = ThemeUtil.getString(context, R.attr.comparison_of_sales_by_sales_staff);
        }

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);

        initializeVariables();
        new LoadProgressDialog().execute("");

        configFilters();
        buildDateFilterDialog();
        dateFilterTextView.setText(thisMonth);
        buildingCustomRangeDialog();
        clickListeners();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) { }

    private void configFilters() {

        thisMonth   = ThemeUtil.getString(context, R.attr.this_month  );
        lastMonth   = ThemeUtil.getString(context, R.attr.last_month  );
        thisYear    = ThemeUtil.getString(context, R.attr.this_year   );
        lastYear    = ThemeUtil.getString(context, R.attr.last_year   );
        customRange = ThemeUtil.getString(context, R.attr.custom_range);


        filterList = new ArrayList<>();
        filterList.add(thisMonth);
        filterList.add(lastMonth);
        filterList.add(thisYear);
        filterList.add(lastYear);
        filterList.add(customRange);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    private void loadIngUI() {
        dateFilterTextView = (TextView    ) mainLayoutView.findViewById(R.id.date_filter            );
        recyclerView       = (RecyclerView) mainLayoutView.findViewById(R.id.pie_chart_recycler_view);
        pieChart           = (PieChart    ) mainLayoutView.findViewById(R.id.pie_chart              );
        noChartDataText    = (FontTextView) mainLayoutView.findViewById(R.id.no_chart_data_text     );
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);
        df             = new DecimalFormat("#");
        startDate      = DateUtility.getThisMonthStartDate();
        endDate        = DateUtility.getThisMonthEndDate  ();
        startLimit     =  0;
        endLimit       = 10;
    }


    private void initializeList() {
        if (reportType.equalsIgnoreCase("top sales by category")) {
            reportItemList = reportManager.topSalesByCategory(startDate, endDate, startLimit, endLimit);
            // Log.e("size", reportItemList.size()+ " ida");
        }  else {
            reportItemList = reportManager.salesBySaleStaff(startDate, endDate, startLimit, endLimit);
        }
        setPercentsforList();
    }

    public void buildingCustomRangeDialog() {
        buildingCustomRangeDatePickerDialog();
        String date = ThemeUtil.getString(context, R.attr.date_range);
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(date)
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.negativeText("Cancel")
                //.positiveText("OK")
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    private void buildDateFilterDialog() {
        String date = ThemeUtil.getString(context, R.attr.filter_by_date);

        dateFilterDialog = new MaterialDialog.Builder(context)
                .title(date)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))

                .build();

    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) { }});
        }

    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
//        if (reportType.equalsIgnoreCase("top sales by category") || reportType.equalsIgnoreCase("bottom sale items")) {
            rvAdapterForPieChart = new RVAdapterForPieChart(reportItemList);
            recyclerView.setAdapter(rvAdapterForPieChart);
//        }


        /*else if (reportType.equalsIgnoreCase("top sales by customer") || reportType.equalsIgnoreCase("top sale by products") ||
                reportType.equalsIgnoreCase("top outstanding suppliers") || reportType.equalsIgnoreCase("bottom sales by products")){
            rvAdapterForBarChartReportWithNameAmt = new RVAdapterForBarChartReportWithNameAmt(reportItemList);
            recyclerView.setAdapter(rvAdapterForBarChartReportWithNameAmt);
        }*/
    }


    public void setPieChartConfiguration() {
        //mChart.setDescription("01-01-2015 ~ 31-01-2015");


        pieChart.setDescription("");
        // Paint p=pieChart.getPaint(Chart.PAINT_DESCRIPTION);
        //p.setColor(Color.RED);
        pieChart.setHighlightPerTapEnabled(true);
        pieChart.setExtraOffsets(5, 10, 5, 5);
        pieChart.setDragDecelerationFrictionCoef(0.95f);
        pieChart.animateXY(1400, 1400, Easing.EasingOption.EaseInCirc, Easing.EasingOption.EaseInCirc);
        pieChart.setDrawHoleEnabled(true);


        //pieChart.setHoleColorTransparent(true);
        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(110);

        // mChart.setCenterText("Top 10 Categories");
        pieChart.setHoleRadius(58f);
        pieChart.setTransparentCircleRadius(61f);

        pieChart.setDrawCenterText(true);
        pieChart.setRotationAngle(0);

        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);
        pieChart.getLegend().setEnabled(false);

        pieChart.setDrawSliceText(false);
        Legend l = pieChart.getLegend();
        l.setEnabled(false);

        pieChart.highlightValue(null);
        setPieDataToChart();


        pieChart.invalidate();
    }

    private void setPieDataToChart() {

        yValues = new ArrayList<Entry>();
        xValues = new ArrayList<String>();

        for (int i = 0; i < reportItemList.size(); i++) {

            Log.e("per", reportItemList.get(i).getPercent() + " dfa");
            if (reportItemList.get(i).getPercent() < 0.6) {
                yValues.add(new Entry(0.f, i));

            } else {
                yValues.add(new Entry(reportItemList.get(i).getPercent(), i, "HELLLLOOOO"));
            }
        }


        for (int i = 0; i < reportItemList.size(); i++) {
            xValues.add(reportItemList.get(i).getName());
        }


        dataSet = new PieDataSet(yValues, "");

        dataSet.setSliceSpace(2f);
        dataSet.setSelectionShift(5f);
        PieData data = new PieData(xValues, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        dataSet.setColors(colorArr);

        if (xValues.size() < 1) {
            Log.e("list", "empty");
            pieChart.clear();
            // pieChart.invalidate();
           /* xValues=new ArrayList<>();
            yValues=new ArrayList<>();
            dataSet=new PieDataSet(yValues,"");
            PieData data1=new PieData(xValues, dataSet);*/
            pieChart.setVisibility(View.GONE);
            noChartDataText.setVisibility(View.VISIBLE);
        } else {
            pieChart.setVisibility(View.VISIBLE);
            noChartDataText.setVisibility(View.GONE);
            pieChart.setData(data);
        }


    }

    public void setPercentsforList() {
        totalAmt = 0.0;
//        if (reportType.equalsIgnoreCase("top sales by category")) {
            for (ReportItem r : reportItemList) {
                totalAmt += r.getBalance();
            }

            Log.e("si", reportItemList.size() + "ze");

            df.setMaximumFractionDigits(1);
            for (ReportItem r : reportItemList) {
                percent = (float) (r.getBalance() * 100 / totalAmt);
                r.setPercent(Float.parseFloat(df.format(percent)));
                Log.e("r", r.getPercent() + "i");
            }
//        }
    }

    private void clickListeners() {

        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                dateFilterDialog.dismiss();
                if (filterList.get(position).equalsIgnoreCase(thisMonth)) {

                    startDate = DateUtility.getThisMonthStartDate();

                    endDate = DateUtility.getThisMonthEndDate();

                    new LoadProgressDialog().execute("");

                    dateFilterTextView.setText(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {


                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    new LoadProgressDialog().execute("");

                    dateFilterTextView.setText(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {


                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    new LoadProgressDialog().execute("");

                    dateFilterTextView.setText(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    new LoadProgressDialog().execute("");

                    dateFilterTextView.setText(filterList.get(position));
                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {

                    customRangeDialog.show();

                }


            }
        });

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startDate = customStartDate;

                endDate = customEndDate;

                /*String startDayDes[]= DateUtility.dayDes(startDate);

                String startYearMonthDes= DateUtility.monthYearDes(startDate);


                String endDayDes[]= DateUtility.dayDes(endDate);

                String endYearMonthDes= DateUtility.monthYearDes(endDate);*/


                //dateFilterTextView.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));

                dateFilterTextView.setText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));

                new LoadProgressDialog().execute("");

                customRangeDialog.dismiss();
            }
        });

        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRangeDialog.dismiss();
            }
        });
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private Spinner spinner = new Spinner(context);

        @Override
        protected String doInBackground(String... params) {

            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            spinner.show();
        }

        @Override
        protected void onPostExecute(String a) {

            setPieChartConfiguration();
            if (!reportItemList.isEmpty()) {
                recyclerView.setVisibility(View.VISIBLE);
                configRecyclerView();
            } else {
                recyclerView.setVisibility(View.GONE);
            }

            spinner.dismiss();
        }
    }
}
