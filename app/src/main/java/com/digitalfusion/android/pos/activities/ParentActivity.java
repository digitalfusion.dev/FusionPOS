package com.digitalfusion.android.pos.activities;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;

import com.digitalfusion.android.pos.util.POSUtil;

/**
 * Created by MD003 on 1/23/18.
 */

public class ParentActivity extends AppCompatActivity {


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onPostResume() {
        super.onPostResume();
        //TODO: Don't forget to activate subscription check
        if(!POSUtil.isAutomaticTimeZone(this)){
            Intent intent=new Intent(this,VerifyActivity.class);
            startActivity(intent);
            finish();
        }
        if(POSUtil.isUnExpectedChangeBoolean(this)){
            Intent intent=new Intent(this,VerifyActivity.class);
            startActivity(intent);
            finish();
        }
        if(POSUtil.checkIsExpire(this)&&POSUtil.isActivatedBoolean(this)){
            Intent intent=new Intent(this,VerifyActivity.class);
            startActivity(intent);
            finish();
        }
   }
}
