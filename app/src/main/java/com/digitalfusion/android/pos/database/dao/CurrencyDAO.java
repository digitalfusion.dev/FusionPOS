package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.model.Currency;
import com.digitalfusion.android.pos.util.AppConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 12/13/16.
 */

public class CurrencyDAO extends ParentDAO {
    private static ParentDAO currencyDaoInstance;
    private Context context;
    private Currency currency;
    private IdGeneratorDAO idGeneratorDAO;
    private List<Currency> currencyList;

    private CurrencyDAO(Context context) {
        super(context);
        this.context = context;
        currency = new Currency();
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        currency = new Currency();
    }

    public static CurrencyDAO getCurrencyDaoInstance(Context context) {
        if (currencyDaoInstance == null) {
            currencyDaoInstance = new CurrencyDAO(context);
        }
        return (CurrencyDAO) currencyDaoInstance;
    }

    public boolean addNewCurrency(String name, String sign, int isDefault) {
        genID = idGeneratorDAO.getLastIdValue("Currency");
        genID += 1;
        query = "insert into " + AppConstant.CURRENCY_TABLE_NAME + "(" + AppConstant.CURRENCY_ID + ", " + AppConstant.CURRENCY_NAME +
                ", " + AppConstant.CURRENCY_SYMBOL + ", " + AppConstant.CURRENCY_IS_DEFAULT + ") values (" + genID + ", ?, ?, " + isDefault + ")";
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{name, sign});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }

        Log.w("helere", name + sign);

        return flag.isInserted();
    }

    public List<Currency> getAllCurrencies(int startLimit, int endLimit) {
        currencyList = new ArrayList<>();
        query = "select " + AppConstant.CURRENCY_ID + ", " + AppConstant.CURRENCY_NAME + ", " + AppConstant.CURRENCY_SYMBOL + ", " + AppConstant.CURRENCY_IS_DEFAULT + " from " +
                AppConstant.CURRENCY_TABLE_NAME + " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    currency = new Currency();
                    currency.setId(cursor.getLong(0));
                    currency.setDisplayName(cursor.getString(1));
                    currency.setSign(cursor.getString(2));
                    currency.setActive(cursor.getInt(3));
                    currencyList.add(currency);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return currencyList;
    }

    public List<Currency> getAllCurrencies() {
        currencyList = new ArrayList<>();
        query = "select " + AppConstant.CURRENCY_ID + ", " + AppConstant.CURRENCY_NAME + ", " + AppConstant.CURRENCY_SYMBOL + ", " + AppConstant.CURRENCY_IS_DEFAULT + " from " +
                AppConstant.CURRENCY_TABLE_NAME;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    currency = new Currency();
                    currency.setId(cursor.getLong(0));
                    currency.setDisplayName(cursor.getString(1));
                    currency.setSign(cursor.getString(2));
                    currency.setActive(cursor.getInt(3));
                    currencyList.add(currency);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return currencyList;
    }

    public boolean updateCurrency(String name, String sign, int isDefault, Long id) {
        query = "update " + AppConstant.CURRENCY_TABLE_NAME + " set " + AppConstant.CURRENCY_NAME +
                "=?, " + AppConstant.CURRENCY_SYMBOL + "=?, " + AppConstant.CURRENCY_IS_DEFAULT + "=" + isDefault + " where " + AppConstant.CURRENCY_ID + " = " + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{name, sign});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean updateDefault(int isDefault, Long id) {
        query = "update " + AppConstant.CURRENCY_TABLE_NAME + " set " + AppConstant.CURRENCY_IS_DEFAULT + "=" + isDefault + " where " + AppConstant.CURRENCY_ID + " = " + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean deleteCurrency(Long id) {
        query = "delete from " + AppConstant.CURRENCY_TABLE_NAME + " where " + AppConstant.CURRENCY_ID + " = " + id;
        //        Log.e("id", id + " kdjf " + query);
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }
}
