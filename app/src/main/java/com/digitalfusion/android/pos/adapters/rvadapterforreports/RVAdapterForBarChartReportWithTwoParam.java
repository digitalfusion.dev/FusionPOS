package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;

import java.util.List;

/**
 * Created by MD002 on 11/8/16.
 */

public class RVAdapterForBarChartReportWithTwoParam extends RecyclerView.Adapter<RVAdapterForBarChartReportWithTwoParam.ReportItemViewHolder> {

    private List<ReportItem> reportItemList;


    public RVAdapterForBarChartReportWithTwoParam(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    @Override
    public ReportItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_name_qty, parent, false);

        return new ReportItemViewHolder(v);

    }


    @Override
    public void onBindViewHolder(ReportItemViewHolder holder, final int position) {
        holder.nameTextView.setText(reportItemList.get(position).getName());

        holder.qtyTextView.setText(Integer.toString(reportItemList.get(position).getQty()));


    }

    @Override
    public int getItemCount() {
        return reportItemList.size();
    }

    public List<ReportItem> getReportItemList() {
        return reportItemList;
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public class ReportItemViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView;
        TextView qtyTextView;

        View view;

        public ReportItemViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);

            qtyTextView = (TextView) itemView.findViewById(R.id.qty_text_view);

        }

    }

}
