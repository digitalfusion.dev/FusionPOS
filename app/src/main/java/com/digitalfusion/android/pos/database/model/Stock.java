package com.digitalfusion.android.pos.database.model;

/**
 * Created by MD003 on 8/17/16.
 */
public class Stock {

    private Long id;
    private String stockCode;
    private String barCode;
    private String itemName;
    private Long categoryID;
    private int inventoryQty;
    private int reorderLevel;
    private Double purchasePrice;
    private Long unitID;
    private Double wholeSalePrice;
    private Double wholeSalePricePercent;
    private Double normalSalePrice;
    private Double normalSalePricePercent;
    private Double retailPrice;
    private Double retailPricePercent;
    //private List<byte[]> imageList;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /*public List<byte[]> getImageList() {
        return imageList;
    }

    public void setImageList(List<byte[]> imageList) {
        this.imageList = imageList;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getInventoryQty() {
        return inventoryQty;
    }

    public void setInventoryQty(int inventoryQty) {
        this.inventoryQty = inventoryQty;
    }

    public int getReorderLevel() {
        return reorderLevel;
    }

    public void setReorderLevel(int reorderLevel) {
        this.reorderLevel = reorderLevel;
    }

    public Double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(Double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public Double getWholeSalePrice() {
        return wholeSalePrice;
    }

    public void setWholeSalePrice(Double wholeSalePrice) {
        this.wholeSalePrice = wholeSalePrice;
    }

    public Double getWholeSalePricePercent() {
        return wholeSalePricePercent;
    }

    public void setWholeSalePricePercent(Double wholeSalePricePercent) {
        this.wholeSalePricePercent = wholeSalePricePercent;
    }

    public Double getNormalSalePrice() {
        return normalSalePrice;
    }

    public void setNormalSalePrice(Double normalSalePrice) {
        this.normalSalePrice = normalSalePrice;
    }

    public Double getNormalSalePricePercent() {
        return normalSalePricePercent;
    }

    public void setNormalSalePricePercent(Double normalSalePricePercent) {
        this.normalSalePricePercent = normalSalePricePercent;
    }

    public Long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Long categoryID) {
        this.categoryID = categoryID;
    }

    public Long getUnitID() {
        return unitID;
    }

    public void setUnitID(Long unitID) {
        this.unitID = unitID;
    }

    public Double getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(Double retailPrice) {
        this.retailPrice = retailPrice;
    }

    public Double getRetailPricePercent() {
        return retailPricePercent;
    }

    public void setRetailPricePercent(Double retailPricePercent) {
        this.retailPricePercent = retailPricePercent;
    }
}
