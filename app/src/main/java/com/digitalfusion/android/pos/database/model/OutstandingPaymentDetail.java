package com.digitalfusion.android.pos.database.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 10/25/16.
 */

public class OutstandingPaymentDetail {
    private Long salePurchaseID;
    private Long customerSupplierID;
    private String customerSupplierName;
    private Double outstandingBalance;
    private String salePurchaseVoucherNo;
    private Double salePurchaseTotalAmount;
    private String salePurchaseDay;
    private String salePurchaseMonth;
    private String salePurchaseYear;
    private String salePurchaseDate;
    private List<OutstandingPayment> outstandingPaymentList;


    public OutstandingPaymentDetail() {
        outstandingPaymentList = new ArrayList<>();
    }

    public Long getSalePurchaseID() {
        return salePurchaseID;
    }

    public void setSalePurchaseID(Long salePurchaseID) {
        this.salePurchaseID = salePurchaseID;
    }

    public Long getCustomerSupplierID() {
        return customerSupplierID;
    }

    public void setCustomerSupplierID(Long customerSupplierID) {
        this.customerSupplierID = customerSupplierID;
    }

    public String getCustomerSupplierName() {
        return customerSupplierName;
    }

    public void setCustomerSupplierName(String customerSupplierName) {
        this.customerSupplierName = customerSupplierName;
    }

    public Double getOutstandingBalance() {
        return outstandingBalance;
    }

    public void setOutstandingBalance(Double outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

    public String getSalePurchaseVoucherNo() {
        return salePurchaseVoucherNo;
    }

    public void setSalePurchaseVoucherNo(String salePurchaseVoucherNo) {
        this.salePurchaseVoucherNo = salePurchaseVoucherNo;
    }

    public Double getSalePurchaseTotalAmount() {
        return salePurchaseTotalAmount;
    }

    public void setSalePurchaseTotalAmount(Double salePurchaseTotalAmount) {
        this.salePurchaseTotalAmount = salePurchaseTotalAmount;
    }

    public List<OutstandingPayment> getOutstandingPaymentList() {
        return outstandingPaymentList;
    }

    public void setOutstandingPaymentList(List<OutstandingPayment> outstandingPaymentList) {
        this.outstandingPaymentList = outstandingPaymentList;
    }

    public String getSalePurchaseDay() {
        return salePurchaseDay;
    }

    public void setSalePurchaseDay(String salePurchaseDay) {
        this.salePurchaseDay = salePurchaseDay;
    }

    public String getSalePurchaseMonth() {
        return salePurchaseMonth;
    }

    public void setSalePurchaseMonth(String salePurchaseMonth) {
        this.salePurchaseMonth = salePurchaseMonth;
    }

    public String getSalePurchaseYear() {
        return salePurchaseYear;
    }

    public void setSalePurchaseYear(String salePurchaseYear) {
        this.salePurchaseYear = salePurchaseYear;
    }

    public String getSalePurchaseDate() {
        return salePurchaseDate;
    }

    public void setSalePurchaseDate(String salePurchaseDate) {
        this.salePurchaseDate = salePurchaseDate;
    }
}
