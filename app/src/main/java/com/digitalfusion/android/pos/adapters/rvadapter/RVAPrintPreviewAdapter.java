package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD003 on 5/30/17.
 */

public class RVAPrintPreviewAdapter extends RecyclerView.Adapter<RVAPrintPreviewAdapter.ThisViewHolder> {

    private List<SalesAndPurchaseItem> saleItemViewInSaleList;
    private ClickListener editClickListener;
    private ClickListener deleteClickListener;


    public RVAPrintPreviewAdapter(List<SalesAndPurchaseItem> saleItemViewInSaleList) {
        this.saleItemViewInSaleList = saleItemViewInSaleList;
    }

    @Override
    public ThisViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.print_preview_item_view, parent, false);

        return new ThisViewHolder(v);

    }

    @Override
    public void onBindViewHolder(ThisViewHolder holder, final int position) {
        SalesAndPurchaseItem currentitem = saleItemViewInSaleList.get(position);

        holder.noTextView.setText(Integer.toString(position + 1));
        holder.invoiceNoTextView.setText(currentitem.getItemName());
        holder.totalTexView.setText(POSUtil.NumberFormat(currentitem.getTotalPrice()));
        holder.qtyPerPriceTextView.setText(currentitem.getQty() + " x " + POSUtil.NumberFormat(currentitem.getPrice()));

        // saleItemViewInSale.salePriceTextView.setText(POSUtil.NumberFormat(saleItemViewInSaleList.get(position).getPrice()));

        //            saleItemViewInSale.disLinearLayout.setVisibility(View.GONE);

    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    @Override
    public int getItemCount() {
        return saleItemViewInSaleList.size();
    }

    public List<SalesAndPurchaseItem> getSaleItemViewInSaleList() {
        return saleItemViewInSaleList;
    }

    public void setSaleItemViewInSaleList(List<SalesAndPurchaseItem> saleItemViewInSaleList) {
        this.saleItemViewInSaleList = saleItemViewInSaleList;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public class ThisViewHolder extends RecyclerView.ViewHolder {
        TextView noTextView;
        TextView invoiceNoTextView;
        TextView totalTexView;
        TextView qtyPerPriceTextView;
        // TextView salePriceTextView;
        //TextView itemDiscountTextView;
        LinearLayout disLinearLayout;

        public ThisViewHolder(View itemView) {
            super(itemView);

            disLinearLayout = (LinearLayout) itemView.findViewById(R.id.dis_layout);
            noTextView = (TextView) itemView.findViewById(R.id.no_in_sale_item_view_tv);
            invoiceNoTextView = (TextView) itemView.findViewById(R.id.item_name_in_sale_item_view_tv);
            totalTexView = (TextView) itemView.findViewById(R.id.total_amount_in_sale_item_view_tv);
            qtyPerPriceTextView = (TextView) itemView.findViewById(R.id.qty_per_price);

            // salePriceTextView = (TextView) itemView.findViewById(R.id.item_price_in_sale_item_view_tv);
            // itemDiscountTextView = (TextView) itemView.findViewById(R.id.item_discount_in_sale_item_view_tv);

        }

    }
}
