package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.database.model.Currency;
import com.digitalfusion.android.pos.database.dao.CurrencyDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 12/14/16.
 */

public class CurrencyManager {
    private Context context;
    private CurrencyDAO currencyDAO;
    private List<Currency> currencyList;
    private Long id;

    public CurrencyManager(Context context) {
        this.context = context;
        currencyDAO = CurrencyDAO.getCurrencyDaoInstance(context);
        currencyList = new ArrayList<>();
    }


    public boolean addNewCurrency(String name, String sign, int isDefault) {
        return currencyDAO.addNewCurrency(name, sign, isDefault);
    }

    public List<Currency> getAllCurrencies(int startLimit, int endLimit) {
        return currencyDAO.getAllCurrencies(startLimit, endLimit);
    }

    public List<Currency> getAllCurrencies() {
        return currencyDAO.getAllCurrencies();
    }

    public boolean updateCurrency(String name, String sign, int isDefault, Long id) {
        return currencyDAO.updateCurrency(name, sign, isDefault, id);
    }


    public boolean updateDefault(int isDefault, Long id) {
        return currencyDAO.updateDefault(isDefault, id);
    }

    public boolean deleteCurrency(Long id) {
        return currencyDAO.deleteCurrency(id);
    }
}
