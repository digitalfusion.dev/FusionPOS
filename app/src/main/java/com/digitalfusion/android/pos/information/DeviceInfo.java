package com.digitalfusion.android.pos.information;

import android.annotation.SuppressLint;
import android.os.Build;

import com.digitalfusion.android.pos.util.ConnectivityUtil;

import java.io.Serializable;

/**
 * Created by MD003 on 8/14/17.
 */

public class DeviceInfo implements Serializable {

    private String deviceName;
    private String deviceOsName;
    private String deviceOsVersion;
    private String deviceOsPlatform;
    private String deviceModel;
    private String serial;
    private String macAddress;
    private String uniqueId;

    @SuppressLint("MissingPermission")
    public DeviceInfo() {
        deviceName = Build.DEVICE;
        deviceOsName = Build.VERSION_CODES.class.getFields()[Build.VERSION.SDK_INT].getName();
        deviceOsVersion = Build.VERSION.RELEASE;
        deviceOsPlatform = "Android";
        deviceModel = Build.MODEL;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            serial = Build.getSerial();
        } else {
            serial = Build.SERIAL;//"HninSER00027"
        }
        macAddress = ConnectivityUtil.getMacAddr(); //"HninMAC00027"

        uniqueId = Build.ID; //"HninUnit00027"

    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceOsName() {
        return deviceOsName;
    }

    public void setDeviceOsName(String deviceOsName) {
        this.deviceOsName = deviceOsName;
    }

    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    public String getDeviceOsPlatform() {
        return deviceOsPlatform;
    }

    public void setDeviceOsPlatform(String deviceOsPlatform) {
        this.deviceOsPlatform = deviceOsPlatform;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public String toString() {
        return "DeviceInfo{" +
                "deviceName='" + deviceName + '\'' +
                ", deviceOsName='" + deviceOsName + '\'' +
                ", deviceOsVersion='" + deviceOsVersion + '\'' +
                ", deviceOsPlatform='" + deviceOsPlatform + '\'' +
                ", deviceModel='" + deviceModel + '\'' +
                ", serial='" + serial + '\'' +
                ", macAddress='" + macAddress + '\'' +
                ", uniqueId='" + uniqueId + '\'' +
                '}';
    }

}
