package com.digitalfusion.android.pos.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.information.Subscription;
import com.digitalfusion.android.pos.information.wrapper.CodeResendRequest;
import com.digitalfusion.android.pos.information.wrapper.VerifyRequest;
import com.digitalfusion.android.pos.network.ApiRetrofit;
import com.digitalfusion.android.pos.network.NetworkClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivationActivity extends AppCompatActivity {
    final ApiRetrofit apiRetrofit = NetworkClient.createService(ApiRetrofit.class, "digital_fusion", "digital_fusion");
    private String userID;
    private String code;
    private String phoneNo;
    private EditText activationCodeEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_activation);
        activationCodeEditText = (EditText) findViewById(R.id.activation_code);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            userID  = bundle.getString("userID");
            code    = bundle.getString("code"  );
            phoneNo = bundle.getString("phone" );

        }

        activationCodeEditText.setText(userID);
        Log.e("userID = " + userID, "code = " + code);


        clickListener();
    }

    private void clickListener() {
        findViewById(R.id.activate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processVerification();

                //                POSUtil.NotUnExpectedChange(ActivationActivity.this);
                //                POSUtil.activate(ActivationActivity.this);
                //                Intent intent = new Intent(ActivationActivity.this, LoginActivity.class);
                //                startActivity(intent);
            }
        });

        findViewById(R.id.resend_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CodeResendRequest codeResendRequest = new CodeResendRequest();
                Log.e("userID", userID + " " + phoneNo);
                codeResendRequest.setUserId(userID);
                codeResendRequest.setPhone(phoneNo);
                Call call = apiRetrofit.resendCode(codeResendRequest);
                call.enqueue(new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) {

                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        t.printStackTrace();
                        Log.e("failure", "failure");
                    }
                });
            }
        });
    }

    private void processVerification() {
        VerifyRequest      verifyRequest = new VerifyRequest(code, userID, phoneNo);
        Call<Subscription> verification  = apiRetrofit.verify(verifyRequest);
        verification.enqueue(new Callback<Subscription>() {
            @Override
            public void onResponse(Call<Subscription> call, Response<Subscription> response) {
                Log.e("res", response.body() == null ? "null" : response.body().toString());
                if (response.code() == 200) {
                    //  Log.e(response.body().getRegistration().getRegion() + " code", response.code()+" co");
                    //POSUtil.NotUnExpectedChange(ActivationActivity.this);
                    //POSUtil.activate(ActivationActivity.this);

                }

            }

            @Override
            public void onFailure(Call<Subscription> call, Throwable t) {
                t.printStackTrace();
                Log.e("failure", "failure");
            }
        });
    }
}
