package com.digitalfusion.android.pos.fragments.outstanding;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVAdapterForReceivableViewPaymentHistoryList;
import com.digitalfusion.android.pos.database.business.CustomerOutstandingManager;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.OutstandingPayment;
import com.digitalfusion.android.pos.database.model.OutstandingPaymentDetail;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.List;

/**
 * Created by MD001 on 10/26/16.
 */

public class ReceivableItemViewDetailFragment extends Fragment {

    private View mainLayout;

    private Context context;

    private OutstandingPaymentDetail customerOutstandingPaymentDetail;

    private CustomerOutstandingManager customerOutstandingManager;

    private SalesHistory salesHistory;

    private List<OutstandingPayment> outstandingPaymentList;

    private TextView purchaseID_tv;

    private TextView purchaseDate_tv;

    private TextView purchaseTotal_tv;

    private TextView receivable_tv;

    private RecyclerView paymentHistory_rv;

    private LinearLayout layout;

    private LinearLayout layoutDetailInformation;

    private ImageView upDown;

    private RVAdapterForReceivableViewPaymentHistoryList rvAdapterForReceivableViewPaymentHistoryList;

    private MaterialDialog addPaymentMaterialDialog;

    private Button saveBtnMd, cancelBtnMd;

    private EditText paymentDateTxtMd;

    private EditText paymentEditTxtMd;

    private ImageButton calenderBtnMd;

    private TextView paymentId;

    private TextView customerName;

    private int paymentDay;

    private int paymentMonth;

    private int paymentYear;

    private DatePickerDialog datePickerDialog;

    private Calendar calendar;

    private String date;

    private int editPos;

    private boolean editFlag;

    private Double paymentAmount = 0.0;

    private MaterialDialog deleteAlertDialog;

    private Button yesDeleteMdButton;

    private boolean darkmode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.receivable_view_payment, null);

        context = getContext();

        darkmode = POSUtil.isNightMode(context);

        calendar = Calendar.getInstance();

        customerOutstandingManager = new CustomerOutstandingManager(context);

        salesHistory = (SalesHistory) getArguments().getSerializable("saleheader");

        Log.e("salehistory", salesHistory.getId() + " ");

        String detail = context.getTheme().obtainStyledAttributes(new int[]{R.attr.detail}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(detail);

        loadUI();

        loadData();

        configRecycler();

        setValues();

        buildDeleteAlertDialog();
        MainActivity.setCurrentFragment(this);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!layoutDetailInformation.isShown()) {
                    layoutDetailInformation.setVisibility(View.VISIBLE);
                    upDown.setSelected(false);
                } else {
                    layoutDetailInformation.setVisibility(View.GONE);
                    upDown.setSelected(true);
                }
            }
        });

        yesDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customerOutstandingManager.deleteCustomerOutstandingPayment(outstandingPaymentList.get(editPos).getId());

                outstandingPaymentList.remove(editPos);

                rvAdapterForReceivableViewPaymentHistoryList.notifyItemRangeChanged(editPos, outstandingPaymentList.size());

                deleteAlertDialog.dismiss();

            }
        });

        rvAdapterForReceivableViewPaymentHistoryList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                editPos = postion;

                deleteAlertDialog.show();
                loadData();

            }
        });


        rvAdapterForReceivableViewPaymentHistoryList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                editPos = postion;

                setPaymentDialogData();

                addPaymentMaterialDialog.show();

            }
        });

        buildDatePickerDialog();

        buildAddPaymentDialog();

        paymentDateTxtMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog.show(getActivity().getFragmentManager(), "Date Pick");

            }
        });

        calenderBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog.show(getActivity().getFragmentManager(), "Date Pick");

            }
        });

        paymentEditTxtMd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editFlag = false;
                if (paymentEditTxtMd.getText().toString().trim().length() > 0) {

                    if (Double.parseDouble(paymentEditTxtMd.getText().toString()) > customerOutstandingPaymentDetail.getOutstandingBalance()) {
                        String cannotBeGreater = context.getTheme().obtainStyledAttributes(new int[]{R.attr.payment_amount_cannot_be_greater_than_remaining_balance}).getString(0);

                        paymentEditTxtMd.setError(cannotBeGreater);

                    } else {
                        editFlag = true;
                        paymentAmount = Double.parseDouble(paymentEditTxtMd.getText().toString());
                    }


                } else {
                    String mustGreater = context.getTheme().obtainStyledAttributes(new int[]{R.attr.payment_amount_must_be_greater_than_0}).getString(0);

                    paymentEditTxtMd.setError(mustGreater);
                }

            }
        });

        saveBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (editFlag) {
                    customerOutstandingManager.updateCustomerOutstandingPayment(customerOutstandingPaymentDetail.getCustomerSupplierID(), customerOutstandingPaymentDetail.getSalePurchaseID(),
                            paymentAmount, outstandingPaymentList.get(editPos).getId(), Integer.toString(paymentDay), Integer.toString(paymentMonth), Integer.toString(paymentYear));

                    outstandingPaymentList.get(editPos).setDay(Integer.toString(paymentDay));

                    outstandingPaymentList.get(editPos).setMonth(Integer.toString(paymentMonth));

                    outstandingPaymentList.get(editPos).setYear(Integer.toString(paymentYear));

                    outstandingPaymentList.get(editPos).setDay(date);

                    outstandingPaymentList.get(editPos).setPaidAmt(paymentAmount);

                    addPaymentMaterialDialog.dismiss();

                    loadData();

                    setValues();

                    rvAdapterForReceivableViewPaymentHistoryList.notifyItemChanged(editPos);


                }

            }
        });

        calenderBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPaymentMaterialDialog.dismiss();
            }
        });

        return mainLayout;
    }

    private void loadData() {

        customerOutstandingPaymentDetail = customerOutstandingManager.getCustomerOutstandingPaymentsBySaleID(salesHistory.getId());

        Log.e("salehistory", customerOutstandingPaymentDetail.getSalePurchaseID() + " ");

        loadOutstandingPaymentList();

        OutstandingPayment initialPayment = new OutstandingPayment();

        initialPayment.setDate(salesHistory.getDate());

        initialPayment.setDay(salesHistory.getDay());

        initialPayment.setMonth(salesHistory.getMonth());

        initialPayment.setYear(salesHistory.getYear());

        String initPayment = context.getTheme().obtainStyledAttributes(new int[]{R.attr.initial_payment}).getString(0);

        initialPayment.setInvoiceNo(initPayment);

        initialPayment.setPaidAmt(salesHistory.getPaidAmt());

        customerOutstandingPaymentDetail.getOutstandingPaymentList().add(0, initialPayment);
    }

    public void loadOutstandingPaymentList() {
        outstandingPaymentList = customerOutstandingPaymentDetail.getOutstandingPaymentList();
    }

    public void loadUI() {

        purchaseID_tv = (TextView) mainLayout.findViewById(R.id.purchase_id_no);

        purchaseDate_tv = (TextView) mainLayout.findViewById(R.id.purchase_date);

        purchaseTotal_tv = (TextView) mainLayout.findViewById(R.id.purchase_total_receivable);

        receivable_tv = (TextView) mainLayout.findViewById(R.id.purchase_remaining_receivable);

        paymentHistory_rv = (RecyclerView) mainLayout.findViewById(R.id.payment_history_rv);

        layout = (LinearLayout) mainLayout.findViewById(R.id.layout);

        layoutDetailInformation = (LinearLayout) mainLayout.findViewById(R.id.general_information_ll);

        upDown = (ImageView) mainLayout.findViewById(R.id.up_down);
    }

    public void configRecycler() {

        rvAdapterForReceivableViewPaymentHistoryList = new RVAdapterForReceivableViewPaymentHistoryList(outstandingPaymentList);

        paymentHistory_rv.setLayoutManager(new LinearLayoutManager(context));

        paymentHistory_rv.setAdapter(rvAdapterForReceivableViewPaymentHistoryList);

    }

    public void setValues() {

        purchaseID_tv.setText("#" + salesHistory.getVoucherNo().toString());

        purchaseDate_tv.setText(DateUtility.makeDateFormatWithSlash(customerOutstandingPaymentDetail.getSalePurchaseDate()));

        purchaseTotal_tv.setText(POSUtil.NumberFormat(customerOutstandingPaymentDetail.getSalePurchaseTotalAmount()));

        receivable_tv.setText(POSUtil.NumberFormat(customerOutstandingPaymentDetail.getOutstandingBalance()));
    }


    public void buildAddPaymentDialog() {

        String editPayment = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_payment}).getString(0);

        addPaymentMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.receivable_add_payment_md, true)
                .title(editPayment).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.positiveText("Save")
                //.negativeText("Cancel")
                .build();

        saveBtnMd = (Button) addPaymentMaterialDialog.findViewById(R.id.save);

        cancelBtnMd = (Button) addPaymentMaterialDialog.findViewById(R.id.cancel);

        paymentEditTxtMd = (EditText) addPaymentMaterialDialog.findViewById(R.id.add_payment_et);

        paymentDateTxtMd = (EditText) addPaymentMaterialDialog.findViewById(R.id.calender_tv);

        calenderBtnMd = (ImageButton) addPaymentMaterialDialog.findViewById(R.id.calender_btn);

        paymentId = (TextView) addPaymentMaterialDialog.findViewById(R.id.payment_id);

        customerName = (TextView) addPaymentMaterialDialog.findViewById(R.id.customer_name);
    }

    private void setPaymentDialogData() {

        paymentAmount = outstandingPaymentList.get(editPos).getPaidAmt();

        paymentYear = Integer.parseInt(outstandingPaymentList.get(editPos).getYear());

        paymentMonth = Integer.parseInt(outstandingPaymentList.get(editPos).getMonth()) - 1;

        paymentYear = Integer.parseInt(outstandingPaymentList.get(editPos).getDay());

        paymentId.setText(outstandingPaymentList.get(editPos).getInvoiceNo());

        paymentDateTxtMd.setText(DateUtility.makeDateFormatWithSlash(outstandingPaymentList.get(editPos).getDate()));

        paymentEditTxtMd.setText((outstandingPaymentList.get(editPos).getPaidAmt().toString()));

        customerName.setText(customerOutstandingPaymentDetail.getCustomerSupplierName());

    }

    public void buildDatePickerDialog() {

        datePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        paymentDay = dayOfMonth;

                        paymentMonth = monthOfYear + 1;

                        paymentYear = year;

                        date = DateUtility.makeDate(Integer.toString(paymentYear), Integer.toString(paymentMonth), Integer.toString(paymentDay));

                        //  String dayDes[]= DateUtility.dayDes(date);

                        // String yearMonthDes=DateUtility.monthYearDes(date);

                        // paymentDateTxtMd.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

                        paymentDateTxtMd.setText(DateUtility.makeDateFormatWithSlash(date));

                    }
                },

                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)

        );


        if (darkmode)
            datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        datePickerDialog.setThemeDark(darkmode);

    }


    private void buildDeleteAlertDialog() {

        //TypedArray no=context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        //TypedArray yes=context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});
        String areUSureWantToDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete}).getString(0);

        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        TextView textView = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(areUSureWantToDelete);

        yesDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });


    }


}
