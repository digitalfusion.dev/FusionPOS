package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForOutstandingReport;
import com.digitalfusion.android.pos.database.business.PurchaseManager;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.model.PurchaseHistory;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.views.Spinner;
import com.github.mikephil.charting.data.BarEntry;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PayableReceivableDetailViewFragment extends Fragment implements Serializable {
    private TextView dateFilterTextView;

    private RVAdapterForFilter rvAdapterForDateFilter;

    private MaterialDialog dateFilterDialog;

    private List<String> filterList;

    private String reportName;
    private ReportItem reportItem;
    private View mainLayoutView;
    private Context context;

    private TextView headerTextView;

    private RecyclerView recyclerView;
    private RVAdapterForOutstandingReport rvAdapterForOutstandingReport;
    private ReportManager reportManager;

    private List<ReportItem> reportItemList;
    //private ReportItem reportItem;
    private List<Double> yValList;

    private String startDate;
    private String endDate;
    private int startLimit;
    private int endLimit;
    private ArrayList<String> xValues;
    private List<BarEntry> yValues;

    private String allTrans;
    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;

    private String customRange;

    private MaterialDialog customRangeDialog;

    private TextView startDateTextView, endDateTextView;

    private String customStartDate,
            customEndDate;

    private TextView traceDate;

    private Button customRangeOkBtn, customRangeCancelBtn;

    private DatePickerDialog startDatePickerDialog;

    private TextView totalLabelTextView;
    private TextView totalBalanceTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_payable_recivable_detail_view, container, false);
        if (getArguments() != null) {
            reportName = getArguments().getString("reportType");
            reportItem = (ReportItem) getArguments().getSerializable("reportItem");
        }

        if (reportName.equalsIgnoreCase("receivable")) {
            String detail = ThemeUtil.getString(context, R.attr.receivable_detail);

            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(detail);
        } else if (reportName.equalsIgnoreCase("payable")) {
            String detail = ThemeUtil.getString(context, R.attr.payable_detail);

            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(detail);
        }
        loadIngUI();

        return mainLayoutView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) { }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initializeVariables();
        configFilters();
        buildDateFilterDialog();
        setDateFilterTextView(allTrans);
        buildingCustomRangeDialog();
        new LoadProgressDialog().execute("");
        clickListeners();
    }

    private void clickListeners() {
        dateFilterTextView.setOnClickListener(v -> dateFilterDialog.show());

        rvAdapterForDateFilter.setmItemClickListener((view, position) -> {
            dateFilterDialog.dismiss();
            if (filterList.get(position).equalsIgnoreCase(allTrans)) {
                startDate = "00000";
                endDate = "9999999999";

                new LoadProgressDialog().execute("");
                setDateFilterTextView(filterList.get(position));
            } else if (filterList.get(position).equalsIgnoreCase(thisMonth)) {
                startDate = DateUtility.getThisMonthStartDate();
                endDate = DateUtility.getThisMonthEndDate();

                new LoadProgressDialog().execute("");
                setDateFilterTextView(filterList.get(position));
            } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {
                startDate = DateUtility.getLastMonthStartDate();
                endDate = DateUtility.getLastMonthEndDate();

                new LoadProgressDialog().execute("");
                setDateFilterTextView(filterList.get(position));
            } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {
                startDate = DateUtility.getThisYearStartDate();
                endDate = DateUtility.getThisYearEndDate();

                new LoadProgressDialog().execute("");
                setDateFilterTextView(filterList.get(position));
            } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {
                startDate = DateUtility.getLastYearStartDate();
                endDate = DateUtility.getLastYearEndDate();

                new LoadProgressDialog().execute("");
                setDateFilterTextView(filterList.get(position));
            } else if (filterList.get(position).equalsIgnoreCase(customRange)) {
                customRangeDialog.show();
            }
        });

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDate = DateUtility.makeDateFormatWithSlash(customStartDate);
                endDate = DateUtility.makeDateFormatWithSlash(customEndDate);

                //String startDayDes[]= DateUtility.dayDes(startDate);

                //String startYearMonthDes= DateUtility.monthYearDes(startDate);

                //String endDayDes[]= DateUtility.dayDes(endDate);

                //String endYearMonthDes= DateUtility.monthYearDes(endDate);

                dateFilterTextView.setText(startDate + " - " + endDate);

                new LoadProgressDialog().execute("");
                customRangeDialog.dismiss();
            }
        });

        customRangeCancelBtn.setOnClickListener(v -> customRangeDialog.dismiss());
    }

    private void configFilters() {
        thisMonth = ThemeUtil.getString(context, R.attr.this_month);
        lastMonth = ThemeUtil.getString(context, R.attr.last_month);
        thisYear = ThemeUtil.getString(context, R.attr.this_year);
        lastYear = ThemeUtil.getString(context, R.attr.last_year);
        customRange = ThemeUtil.getString(context, R.attr.custom_range);
        allTrans = ThemeUtil.getString(context, R.attr.all);

        filterList = new ArrayList<>();
        filterList.add(allTrans);
        filterList.add(thisMonth);
        filterList.add(lastMonth);
        filterList.add(thisYear);
        filterList.add(lastYear);
        filterList.add(customRange);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance((view, year, monthOfYear, dayOfMonth) -> {
            if (traceDate == startDateTextView) {
                customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                startDate = customStartDate;
            } else {
                customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                endDate = customEndDate;
            }
        }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));

        startDatePickerDialog.setOnCancelListener(dialog -> {

        });
    }

    public void buildingCustomRangeDialog() {
        buildingCustomRangeDatePickerDialog();
        String dateRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range}).getString(0);
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(dateRange)
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(v -> {
            traceDate = startDateTextView;
            startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");
        });

        endDateTextView.setOnClickListener(v -> {
            traceDate = endDateTextView;
            startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");
        });

        customRangeCancelBtn.setOnClickListener(v -> customRangeDialog.dismiss());
    }

    private void setDateFilterTextView(String msg) {
        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {
        String filter = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);
        dateFilterDialog = new MaterialDialog.Builder(context)
                .title(filter)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))
                .build();
    }

    private void loadIngUI() {
        dateFilterTextView = (TextView) mainLayoutView.findViewById(R.id.date_filter);
        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.recycler_view);
        headerTextView = (TextView) mainLayoutView.findViewById(R.id.header_text_view);
        totalLabelTextView = (TextView) mainLayoutView.findViewById(R.id.total_label_text_view);
        totalBalanceTextView = (TextView) mainLayoutView.findViewById(R.id.total_balance_text_view);
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);

        setDatesAndLimits();

        if (reportName.equalsIgnoreCase("receivable")) {
            String report = context.getTheme().obtainStyledAttributes(new int[]{R.attr.s_receivable_report}).getString(0);
            String total  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.total_receivable_balance}).getString(0);
            headerTextView.setText(reportItem.getName() + report);
            totalLabelTextView.setText(total + " : ");
        } else if (reportName.equalsIgnoreCase("payable")) {
            String report = context.getTheme().obtainStyledAttributes(new int[]{R.attr.s_payable_report}).getString(0);
            String total  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.total_payable_balance}).getString(0);
            headerTextView.setText(reportItem.getName() + report);
            totalLabelTextView.setText(total + " : ");
        }
    }

    private void initializeList() {

        if (reportName.equalsIgnoreCase("payable")) {

            reportItemList = reportManager.payableDetailList(reportItem.getId(), startDate, endDate, startLimit, endLimit);

            Log.e("si", reportItemList.size() + " s");

            /*new Handler().post(new Runnable() {
                @Override
                public void run() {
                    headerTextView.setText(reportItem.getName() + "'s Payable Report");
                }
            });*/

        } else if (reportName.equalsIgnoreCase("receivable")) {

            reportItemList = reportManager.receivableDetailList(reportItem.getId(), startDate, endDate, startLimit, endLimit);

            /*new Handler().post(new Runnable() {
                @Override
                public void run() {
                    headerTextView.setText(reportItem.getName() + "'s Payable Report");
                }
            });*/
        }
    }

    public void configRecyclerView() {
        if (reportName.equalsIgnoreCase("payable")) {
            rvAdapterForOutstandingReport = new RVAdapterForOutstandingReport(reportItemList, 1);
            rvAdapterForOutstandingReport.setClickListener(postion -> {

                Bundle          bundle          = new Bundle();
                PurchaseManager purchaseManager = new PurchaseManager(context);
                PurchaseHistory purchaseHistory = purchaseManager.getPurchase(reportItemList.get(postion).getId());

                bundle.putSerializable("purchaseheader", purchaseHistory);

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.PAYABLE_PAYMENT_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);
            });
        } else if (reportName.equalsIgnoreCase("receivable")) {
            rvAdapterForOutstandingReport = new RVAdapterForOutstandingReport(reportItemList, 1);
            rvAdapterForOutstandingReport.setClickListener(postion -> {

                Bundle       bundle       = new Bundle();
                SalesManager salesManager = new SalesManager(context);
                SalesHistory salesHistory = salesManager.getSaleHistoryViewById(reportItemList.get(postion).getId());

                bundle.putSerializable("saleheader", salesHistory);

                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.RECEIVABLE_PAYMENT_DETAIL);
                detailIntent.putExtras(bundle);

                startActivity(detailIntent);
            });
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(rvAdapterForOutstandingReport);
    }

    private void setDatesAndLimits() {
        startDate = "0000";
        endDate = "9999999999";
        startLimit = 0;
        endLimit = 10;
        Log.e(startDate, endDate);
    }

    private void setTotalAmt() {
        Double total = 0.0;
        for (ReportItem r : reportItemList) {
            total += r.getBalance();
        }
        totalBalanceTextView.setText(total.toString());
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        Spinner spinner = new Spinner(context);

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.show();
        }

        @Override
        protected void onPostExecute(String a) {
            configRecyclerView();
            setTotalAmt();

            spinner.dismiss();
        }

        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }
}
