package com.digitalfusion.android.pos.fragments.inventoryfragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.ActiveStockSearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForStockList;
import com.digitalfusion.android.pos.database.business.CategoryManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.util.GrantPermission;
import com.example.searchview.MaterialSearchView;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 8/17/16.
 */
public class StockListViewFragment extends Fragment {

    GestureDetector simpleOnGestureListener;
    int startLimit, endLimit;
    private View mainLayoutView;
    //for stock recycler view
    private RecyclerView recyclerView;
    private RVSwipeAdapterForStockList rvSwipeAdapterForStockList;
    private List<StockItem> stockItemList;
    private Context context;
    //FAB add new stock
    private FloatingActionButton addNewStockFab;
    private StockManager stockManager;
    private MaterialSearchView searchView;
    private List<String> filterList;
    private MaterialDialog filterDialog;
    private RVAdapterForFilter rvAdapterForFilter;
    private List<Category> categoryList;
    private CategoryManager categoryManager;
    private TextView filterTextView;
    private TextView searchedResultTxt;
    private GrantPermission grantPermission;
    private TextView stockCountTextView;
    private TextView stockValueTextView;
    private int stockCount;
    private LinearLayoutManager linearLayoutManager;
    private ActiveStockSearchAdapter stockSearchAdapter;
    private boolean isFilter = false;
    private Long choseCategoryId = 0l;
    private TextView noTransactionTextView;
    private boolean isAll = true;
    private boolean isSearch = true;
    private MaterialDialog deleteAlertDialog;
    private Button yesSaleDeleteMdButton;
    private int deletepos;
    private boolean isAdd;
    private StockItem stockItem;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.stock_list_fragment, null);

        context = getContext();

        // stockCountTextView = (TextView) mainLayoutView.findViewById(R.id.stock_count);

        //((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Stock List");

        stockValueTextView = (TextView) mainLayoutView.findViewById(R.id.stock_value);

        stockManager = new StockManager(context);

        categoryManager = new CategoryManager(context);

        linearLayoutManager = new LinearLayoutManager(context);

        categoryList = categoryManager.getAllCategories();

        filterList = new ArrayList<>();
        buildDeleteAlertDialog();
        loadIngUI();

        // simpleOnGestureListener=new GestureDetector(getContext(), new GestureListener(this));

        addNewStockFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  MainActivity.replacingFragment(new StockFormFragment());


                //    ExportImportUtil exportImportUtil=new ExportImportUtil(getContext(),stockItemList);

                isAdd = true;


                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_STOCK);

                startActivity(addCurrencyIntent);

                //   exportImportUtil.exportStocks();


            }
        });
        // MainActivity.setCurrentFragment(this);
        String all = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);

        filterList.add(all);

        for (Category c : categoryList) {
            filterList.add(c.getName());
        }

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();

        rvSwipeAdapterForStockList = new RVSwipeAdapterForStockList(stockItemList);

        handleOnClick();

        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setAdapter(rvSwipeAdapterForStockList);

        recyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {

                Log.w("V X", velocityX + " S");

                Log.w("V Y", velocityY + " S");

                if (velocityY > 100) {
                    addNewStockFab.hide();
                } else if (velocityY < -100) {
                    addNewStockFab.show();
                }


                return false;
            }
        });

        recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {


                Log.w("herere", "Scroll Up");

            }

            @Override
            public void onScrollDown() {
                Log.w("herere", "Scroll Down");

            }

            @Override
            public void onLoadMore() {

                rvSwipeAdapterForStockList.setShowLoader(true);
                Log.w("MOre", "more" + isFilter + isAll + isSearch);
                loadMore(stockItemList.size(), 10);

            }
        });

        new LoadProgressDialog().execute();

        mainLayoutView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Log.w("here", "toch");

                //                simpleOnGestureListener.onTouchEvent(event);

                return false;
            }
        });

        new LoadProgressDialog().execute();

        rvSwipeAdapterForStockList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                deletepos = postion;

                deleteAlertDialog.show();
            }
        });

        yesSaleDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // supplierBusiness.(salesHistoryViewList.get(deletepos).getId());

                //Log.w("hello delete",stockItemList.get(deletepos).getName()+"DDD");
                stockManager.deleteStock(stockItemList.get(deletepos).getId());

                //rvSwipeAdapterForSaleTransactions.setSalesHistoryList(salesHistoryViewList);

                if (stockItemList.size() == 1) {
                    stockItemList.remove(deletepos);
                    rvSwipeAdapterForStockList.setStockItemList(stockItemList);
                    rvSwipeAdapterForStockList.notifyDataSetChanged();
                } else {
                    stockItemList.remove(deletepos);
                    rvSwipeAdapterForStockList.notifyItemRemoved(deletepos);
                    rvSwipeAdapterForStockList.notifyItemRangeChanged(deletepos, stockItemList.size());

                }

                deleteAlertDialog.dismiss();

            }
        });

        return mainLayoutView;
    }

    @Override
    public void onResume() {
        super.onResume();

        categoryList = categoryManager.getAllCategories();
        filterList = new ArrayList<>();
        for (Category c : categoryList) {
            filterList.add(c.getName());
        }
        rvAdapterForFilter.setFilterNameList(filterList);
        rvAdapterForFilter.notifyDataSetChanged();
        Log.w("is search stock", isSearch + "sss");
        if (stockItem != null) {

            stockItem = stockManager.getStocksByID(stockItem.getId());

            // Log.w("After updateRegistration",salesHistoryView.toString());

            rvSwipeAdapterForStockList.updateItem(stockItem);

            stockItem = null;
        }

        if (isAdd) {
            new LoadProgressDialog().execute();
        }


    }

    private void handleOnClick() {
        rvSwipeAdapterForStockList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle = new Bundle();


                bundle.putSerializable("stock", stockItemList.get(postion));
                bundle.putString("stockedit", "stockedit");

                //   StockFormFragment editStockFormFragment = new StockFormFragment();

                //   editStockFormFragment.setArguments(bundle);

                //   MainActivity.replacingFragment(editStockFormFragment);
                stockItem = stockItemList.get(postion);

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtras(bundle);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_STOCK);

                startActivity(addCurrencyIntent);

            }
        });

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                filterTextView.setText(filterList.get(position));
                isSearch = false;
                //                searchedResultTxt.setVisibility(View.INVISIBLE);
                isFilter = true;
                recyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
                    @Override
                    public void onScrollUp() {


                        Log.w("herere", "Scroll Up");

                    }

                    @Override
                    public void onScrollDown() {
                        Log.w("herere", "Scroll Down");

                    }

                    @Override
                    public void onLoadMore() {

                        rvSwipeAdapterForStockList.setShowLoader(true);
                        Log.w("MOre", "more" + isFilter + isAll + isSearch);
                        loadMore(stockItemList.size(), 10);

                    }
                });
                if (position != 0) {

                    isAll = false;

                    choseCategoryId = categoryList.get(position).getId();
                    startLimit = 0;
                    endLimit = 10;
                    refreshStockList();


                } else {
                    isAll = true;
                    startLimit = 0;
                    endLimit = 10;
                    refreshStockList();
                }

                filterDialog.dismiss();
            }
        });

        stockSearchAdapter = new ActiveStockSearchAdapter(context, stockManager);

        searchView.setAdapter(stockSearchAdapter);

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //stockItemList = new ArrayList<>();

                //                Log.w("here","hello In Search");

                //  stockItemList.add(stockSearchAdapter.getSuggestionList().get(position));

                isSearch = true;

                isAll = false;
                isFilter = false;

                Bundle bundle = new Bundle();

                bundle.putSerializable("stock", stockSearchAdapter.getSuggestion().get(position));

                StockDetailViewFragment stockDetailFragment = new StockDetailViewFragment();

                stockDetailFragment.setArguments(bundle);

                //    MainActivity.replacingFragment(stockDetailFragment);
                Intent stockDetailIntent = new Intent(context, DetailActivity.class);

                stockDetailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.STOCK_DETAIL);

                stockDetailIntent.putExtras(bundle);

                startActivity(stockDetailIntent);

                // refreshRecyclerView();

                searchView.closeSearch();

                // filterTextView.setText("-");

                //                searchedResultTxt.setVisibility(View.VISIBLE);

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });

        rvSwipeAdapterForStockList.setViewDetailClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle = new Bundle();

                bundle.putSerializable("stock", stockItemList.get(postion));

                StockDetailViewFragment stockDetailFragment = new StockDetailViewFragment();

                stockDetailFragment.setArguments(bundle);

                //    MainActivity.replacingFragment(stockDetailFragment);
                Intent stockDetailIntent = new Intent(context, DetailActivity.class);

                stockDetailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.STOCK_DETAIL);

                stockDetailIntent.putExtras(bundle);

                startActivity(stockDetailIntent);

            }
        });

        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });


    }


    private void buildDateFilterDialog() {
        TypedArray filterByCategory = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_category});

        filterDialog = new MaterialDialog.Builder(context).
                title(filterByCategory.getString(0))

                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    private void buildDeleteAlertDialog() {

        //TypedArray no = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        //TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        String   sureToDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete}).getString(0);
        TextView textView     = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(sureToDelete);


        yesSaleDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });

    }

    public void initializingStockViewList() {

        startLimit = 0;
        endLimit = 10;
        if (isAll) {
            stockItemList = stockManager.getAllActiveStocks(startLimit, endLimit);
        } else {
            stockItemList.addAll(stockManager.getStockListByCategory(startLimit, endLimit, choseCategoryId));
        }


    }


    public void loadMore(int startLimit, int endLimit) {
        this.startLimit = startLimit;
        this.endLimit = endLimit;
        if (isAll) {
            Log.w("Here ", "IN ALL");
            stockItemList.addAll(stockManager.getAllActiveStocks(startLimit, endLimit));
        } else if (isSearch) {
            //donothing
            Log.w("Here ", "IN Search");
        } else if (isFilter) {
            Log.w("Here ", "IN filter");
            stockItemList.addAll(stockManager.getStockListByCategory(startLimit, endLimit, choseCategoryId));
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                refreshRecyclerView();

                rvSwipeAdapterForStockList.setShowLoader(false);

            }
        }, 500);
    }

    public void refreshStockList() {

        if (isAll && isFilter) {

            stockItemList = stockManager.getAllActiveStocks(startLimit, endLimit);

        } else if (!isAll && isFilter) {

            //reoder category filter

            stockItemList = stockManager.getStockListByCategory(startLimit, endLimit, choseCategoryId);

        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshRecyclerView();
                rvSwipeAdapterForStockList.setShowLoader(false);
            }
        }, 500);

    }

    private void refreshRecyclerView() {

        if (stockItemList.size() > 0) {

            recyclerView.setVisibility(View.VISIBLE);

            noTransactionTextView.setVisibility(View.GONE);


            rvSwipeAdapterForStockList.setStockItemList(stockItemList);

            rvSwipeAdapterForStockList.notifyDataSetChanged();

        } else {

            recyclerView.setVisibility(View.GONE);

            noTransactionTextView.setVisibility(View.VISIBLE);


        }

    }


    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});

        int attributeResourceId = a.getResourceId(0, 0);

        searchView.setCursorDrawable(attributeResourceId);

    }

    public void loadIngUI() {

        noTransactionTextView = (TextView) mainLayoutView.findViewById(R.id.no_transaction);

        filterTextView = (TextView) mainLayoutView.findViewById(R.id.filter_one);

        searchedResultTxt = (TextView) mainLayoutView.findViewById(R.id.searched_result_txt);

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.stock_list_rv);

        addNewStockFab = (FloatingActionButton) mainLayoutView.findViewById(R.id.add_new_stock);

        loadUIFromToolbar();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //recreate();
            //reload my activity with requestPermissions granted or use the features what required the requestPermissions
            //requestPermissions(backingUp);

        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(getContext())
                    .setTitle(Html.fromHtml("<font color='#424242'>Inform and request</font>"))
                    .setMessage(Html.fromHtml("<font color='#424242'>Please enable storage requestPermissions to perform backup and restore functions!</font>"))
                    .setPositiveButton(Html.fromHtml("<font color='#424242'>Ok</font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.requestPermissions.WRITE_EXTERNAL_STORAGE}, RC_PERMISSION_WRITE_EXTERNAL_STORAGE);
                            //   ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.requestPermissions.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_RC);
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 200);
                        }
                    })
                    .show();
            Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            nbutton.setTextColor(Color.parseColor("#424242"));
        } else {
            AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(getContext())
                    .setTitle(Html.fromHtml("<font color='#424242'>Inform and request</font>"))
                    .setMessage(Html.fromHtml("<font color='#424242'>Please enable storage requestPermissions from settings to get access to the storage!</font>"))
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 200);
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        }
                    })
                    .show();
            Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            nbutton.setTextColor(Color.parseColor("#424242"));
            //}
        }
    }


    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializingStockViewList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String label = "";

            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {

            refreshRecyclerView();

            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
