package com.digitalfusion.android.pos.information;

import java.util.Date;

/**
 * Created by MD003 on 8/14/17.
 */

public class Customer {

    private String name;
    private String email;
    private String otherEmail;
    private String phone;
    private Date dateOfBirth;
    private String socialLink;
    private String otherSocialLink;
    private BillingAddress billingAddress;

    public Customer(String name, String email, String otherEmail, String phone, Date dateOfBirth, String socialLink, String otherSocialLink, BillingAddress billingAddress) {
        this.name = name;
        this.email = email;
        this.otherEmail = otherEmail;
        this.phone = phone;
        this.dateOfBirth = dateOfBirth;
        this.socialLink = socialLink;
        this.otherSocialLink = otherSocialLink;
        this.billingAddress = billingAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOtherEmail() {
        return otherEmail;
    }

    public void setOtherEmail(String otherEmail) {
        this.otherEmail = otherEmail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSocialLink() {
        return socialLink;
    }

    public void setSocialLink(String socialLink) {
        this.socialLink = socialLink;
    }

    public String getOtherSocialLink() {
        return otherSocialLink;
    }

    public void setOtherSocialLink(String otherSocialLink) {
        this.otherSocialLink = otherSocialLink;
    }

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", otherEmail='" + otherEmail + '\'' +
                ", phone='" + phone + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", socialLink='" + socialLink + '\'' +
                ", otherSocialLink='" + otherSocialLink + '\'' +
                ", billingAddress=" + billingAddress +
                '}';
    }
}
