package com.digitalfusion.android.pos.fragments;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.digitalfusion.android.pos.activities.AddNewPasscodeActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.activities.PasscodeActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.activities.RegisterationActivity;
import com.digitalfusion.android.pos.activities.VerifyActivity;
import com.digitalfusion.android.pos.database.business.ApiManager;
import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.information.Subscription;
import com.digitalfusion.android.pos.information.wrapper.DeviceRequest;
import com.digitalfusion.android.pos.network.ApiRetrofit;
import com.digitalfusion.android.pos.network.NetworkClient;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.ConnectivityUtil;
import com.digitalfusion.android.pos.util.GrantPermission;
import com.digitalfusion.android.pos.util.POSUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SplashScreenFragment extends Fragment {
    private View view;
    private GrantPermission grantPermission;

    ApiManager apiManager;
    public SplashScreenFragment() {
        // Required empty public constructor
    }

    private Response<Subscription> responseBody;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.splash_screen, container, false);
        MainActivity.setCurrentFragment(this);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        apiManager = new ApiManager(getContext());
//        try {
//            TempName.fileProcessor(Cipher.ENCRYPT_MODE,key,inputFile,encryptedFile);
//            TempName.fileProcessor(Cipher.DECRYPT_MODE,key,encryptedFile,decryptedFile);
//            System.out.println("Sucess");
//        } catch (Exception ex) {
//            System.out.println(ex.getMessage());
//            ex.printStackTrace();
//        }
//        TempName.encrypt();
//        TempName.decrypt();


//        ExternalFile externalFile=new ExternalFile();
//        String s = externalFile.fileProcessor(Cipher.DECRYPT_MODE,getContext().getString(R.string.title_name), getContext());
//
//        Log.e("sdkajkdfa", ouput[0] + " dkfja");
//        Log.e("sdkajkdfa", ouput[1] + " dkfja");
//        Log.e("sdkajkdfa", ouput[2] + " dkfja");

        ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
        imageView.setBackgroundResource(R.drawable.three_dots_loading);
        AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
        drawable.start();
        grantPermission = new GrantPermission(this);

        if (Build.VERSION.SDK_INT >= 23) {
            if (grantPermission.hasPermissions(getContext())) {
                startRealWork();
            } else {
                grantPermission.requestPermissions();
            }
        } else {
            startRealWork();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == GrantPermission.PERMISSION_ALL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startRealWork();
                //Log.e("backup", flag + " ai");
            } else {
                grantPermission.askAgainPermission();
            }
        }
    }

    /**
     * Launch PasscodeActivity or AddNewPassocdeActivity, or VerifyActivity
     */
    private void launchNextActivity() {
        if(POSUtil.checkIsExpire(getContext())){
            Intent intent = new Intent(getActivity(), VerifyActivity.class);
            startActivity(intent);
        }else {
            if (apiManager.isPasscodeSet(apiManager.getOwnerId())) {
                Intent intent = new Intent(getContext(), PasscodeActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(getContext(), AddNewPasscodeActivity.class);
                startActivity(intent);
            }

        }
    }

    //TODO: Don't forget to activate subscription check
    private void startRealWork() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void run() {
                if(isVisible()){
                    if (!POSUtil.isAutomaticTimeZone(getActivity())) {
                        Intent intent = new Intent(getContext(), VerifyActivity.class);
                        startActivity(intent);

                    } else if(POSUtil.checkIsExpire(getContext())&&POSUtil.isActivatedBoolean(getContext())){
                        Intent intent = new Intent(getContext(), VerifyActivity.class);
                        startActivity(intent);

                    }else if (!POSUtil.isActivatedBoolean(getContext())||POSUtil.isUnExpectedChangeBoolean(getContext())) {
                        final ApiRetrofit   apiRetrofit   =  NetworkClient.createService(ApiRetrofit.class, "digital_fusion", "digital_fusion");
                        final DeviceRequest deviceRequest = new DeviceRequest();
                        Log.e("Mac", deviceRequest.getMacAddress());
                        Log.e("Seiral", deviceRequest.getSerial());
                        Log.w("New Device","OR OLD DEVICE");
                        Call<Subscription> checkDeviceCall = apiRetrofit.checkDevice(deviceRequest);
                        checkDeviceCall.enqueue(new Callback<Subscription>() {
                            @Override
                            public void onResponse(Call<Subscription> call, Response<Subscription> response) {

                                if (response.code() == 200) {
                                    Subscription responseSubscription = response.body();
                                    POSUtil.acitvate(responseSubscription,getContext());

                                    launchNextActivity();

                                } else if (response.code() == 404) {
                                    Log.e("Mac", deviceRequest.getMacAddress());
                                    Log.e("Seiral", deviceRequest.getSerial());
                                    Log.e("body", "New Device");

                                    Intent intent = new Intent(getActivity(), RegisterationActivity.class);

                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onFailure(Call<Subscription> call, Throwable t) {
                                Log.e("SplashScreenFragment", t.getMessage());
                                ConnectivityUtil.handleFailureProcess(t, view);
                                Intent intent = new Intent(getContext(), VerifyActivity.class);
                                startActivity(intent);
                            }
                        });
                    } else {
                        launchNextActivity();
                    }
                }
            }
        }, 2000);


//        if (apiManager.isPasscodeSet(apiManager.getOwnerId())) {
//            Intent intent = new Intent(getContext(), PasscodeActivity.class);
//            startActivity(intent);
//        } else {
//            Intent intent = new Intent(getContext(), AddNewPasscodeActivity.class);
//            startActivity(intent);
//        }
     }
}
