package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD002 on 11/8/16.
 */

public class RVAdapterForSalePurchaseItemReport extends ParentRVAdapterForReports {

    private final int HEADER_TYPE = 10000;
    private List<ReportItem> reportItemList;

    public RVAdapterForSalePurchaseItemReport(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sale_purchase_item_report_list_item_header, parent, false);

            return new HeaderItemView(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sale_purchase_item_report_list_item, parent, false);

            return new ReportItemViewHolder(v);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ReportItemViewHolder) {
            ReportItemViewHolder mholder = (ReportItemViewHolder) holder;
            mholder.nameTextView.setText(reportItemList.get(position - 1).getName());
            mholder.qtyTextView.setText(Integer.toString(reportItemList.get(position - 1).getQty()));
            mholder.amountTextView.setText(POSUtil.doubleToString(reportItemList.get(position - 1).getAmount()));
        }
    }

    @Override
    public int getItemCount() {
        return reportItemList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_TYPE;
        } else {
            return 1;
        }
    }

    public List<ReportItem> getReportItemList() {
        return reportItemList;
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public class ReportItemViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView;
        TextView qtyTextView;
        TextView amountTextView;

        View view;

        public ReportItemViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);

            qtyTextView = (TextView) itemView.findViewById(R.id.qty_text_view);

            amountTextView = itemView.findViewById(R.id.amountView);
        }

    }

    public class HeaderItemView extends RecyclerView.ViewHolder {


        View view;

        public HeaderItemView(View itemView) {
            super(itemView);

            this.view = itemView;


        }

    }

}
