package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockSearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.example.searchview.MaterialSearchView;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class LineChartWithSearchViewFragment extends Fragment implements Serializable {

    protected String[] mMonths = new String[]{
            " Jan", " Feb", " Mar", " Apr", " May", " Jun", " Jul", " Aug", " Sep", " Oct", " Nov", " Dec"
    };
    private TextView dateFilterTextView;
    private RVAdapterForFilter rvAdapterForDateFilter;
    private MaterialDialog dateFilterDialog;
    private List<String> filterList;
    private View mainLayoutView;
    private Context context;
    private List<ReportItem> reportItemList;
    private ReportManager reportManager;
    private RecyclerView recyclerView;
    private LineChart lineChart;
    private String reportType;
    private String startDate;
    private String endDate;
    private ArrayList<Entry> e1;
    private ArrayList<String> xVals;
    private String thisYear;
    private String lastYear;
    private String last12Month;
    private String[] dateFilterArr;
    private boolean isTwelve;
    private String orderby;


    private MaterialSearchView searchView;

    private Long stockID;

    private StockSearchAdapter stockSearchAdapter;

    private StockManager stockManager;

    private TextView chartDescTextView;
    private String chartDesc;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_line_chart_with_search_view, container, false);
        setHasOptionsMenu(true);
        context = getContext();
        loadIngUI();
        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeVariables();
        setDatesAndLimits();

        new LoadProgressDialog().execute("");


        configFilter();

        buildDateFilterDialog();

        setDateFilterTextView(thisYear);

        clickListeners();

        searchView.showSearch(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        searchView.showSearch(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void configFilter() {
        TypedArray mthisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year});

        TypedArray mlastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year});

        TypedArray mlast12Month = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_12_month});


        thisYear = mthisYear.getString(0);

        lastYear = mlastYear.getString(0);


        last12Month = mlast12Month.getString(0);


        filterList = new ArrayList<>();

        filterList.add(thisYear);

        filterList.add(lastYear);

        filterList.add(last12Month);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);
    }

    private void clickListeners() {
        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                dateFilterDialog.dismiss();


                if (filterList.get(position).equalsIgnoreCase(thisYear)) {
                    orderby = "asc";
                    isTwelve = false;

                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    int year = Calendar.getInstance().get(Calendar.YEAR);
                    for (int i = 0; i < 12; i++) {
                        dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
                    }

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {
                    orderby = "asc";
                    isTwelve = false;

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();
                    int year = Calendar.getInstance().get(Calendar.YEAR) - 1;
                    dateFilterArr = new String[12];
                    for (int i = 0; i < 12; i++) {
                        dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
                    }

                } else if (filterList.get(position).equalsIgnoreCase(last12Month)) {
                    orderby = "desc";
                    isTwelve = true;

                    startDate = DateUtility.getLast12MonthStartDate();

                    endDate = DateUtility.getLast12MotnEndDate();

                    Log.e("1 ", startDate + "  " + endDate);

                    dateFilterArr = new String[12];
                    int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
                    int year  = Calendar.getInstance().get(Calendar.YEAR);
                    for (int i = 0; i < 12; i++) {
                        if (month < 1) {
                            year -= 1;
                            month = 12;
                        }
                        dateFilterArr[i] = "(" + (month) + "," + year + ")";
                        month -= 1;
                    }
                }

                new LoadProgressDialog().execute("");

                setDateFilterTextView(filterList.get(position));
            }
        });

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                stockID = stockSearchAdapter.getSuggestion().get(position).getId();

                chartDesc = stockSearchAdapter.getSuggestion().get(position).getName();

                searchView.closeSearch();

                new LoadProgressDialog().execute("");

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });
    }

    private void setDateFilterTextView(String msg) {

        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {


        String filter = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);
        dateFilterDialog = new MaterialDialog.Builder(context).

                title(filter)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))

                .build();

    }

    private void loadIngUI() {
        dateFilterTextView = (TextView) mainLayoutView.findViewById(R.id.date_filter);

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.line_chart_recycler_view);
        lineChart = (LineChart) mainLayoutView.findViewById(R.id.line_chart);
        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        searchView.setCursorDrawable(attributeResourceId);
        chartDescTextView = (TextView) mainLayoutView.findViewById(R.id.chart_desc_text_view);
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);
        isTwelve = false;
        reportType = getArguments().getString("reportType");
        dateFilterArr = new String[12];
        orderby = "asc";
        stockID = 0l;

        stockManager = new StockManager(context);
        stockSearchAdapter = new StockSearchAdapter(context, stockManager);
        searchView.setAdapter(stockSearchAdapter);
    }

    private void initializeList() {
        if (reportType.equalsIgnoreCase("monthly gross profit by product")) {
            Log.e("st", stockID.toString());
            if (stockID != 0) {

                reportManager.createPurchasePriceTempTable(startDate, endDate);
                //Log.e("id", stockID.toString());
                reportItemList = reportManager.monthlyGrossProfitByProduct(stockID, startDate, endDate, dateFilterArr, orderby);
                reportManager.dropPurchasePriceTempTable();

            }

        } /*else if (reportType.equalsIgnoreCase("monthly gross profit")){
            reportManager.createPurchasePriceTempTable(startDate, endDate);
            reportItemList = reportManager.monthlyGrossProfit(startDate, endDate, dateFilterArr, orderby);
            // Log.e("size", reportItemList.size()+ " ida");
        } else if (reportType.equalsIgnoreCase("monthly purchase transactions")){
            reportItemList = reportManager.monthlyPurchaseTransactions(startDate, endDate, dateFilterArr, orderby);
        }*/
    }

    private void setDatesAndLimits() {
        startDate = Calendar.getInstance().get(Calendar.YEAR) + "0101";
        //Log.e("start", startDate );
        endDate = Calendar.getInstance().get(Calendar.YEAR) + "1231";
        Log.e("start", endDate);
            /*Calendar c = Calendar.getInstance();
            c.set(2016,0,1,0,0,0);
            startTime = c.getTimeInMillis();
            c.set(2017,0,1,0,0,0);
            endTime = c.getTimeInMillis();*/

        int year = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 0; i < 12; i++) {
            dateFilterArr[i] = "(" + (i + 1) + "," + year + ")";
        }
        //startLimit = 0;
        //endLimit = 10;
    }

    public void setLineChartConfiguration() {
        lineChart.setDrawGridBackground(false);
        lineChart.animateX(1400, Easing.EasingOption.EaseInOutQuart);
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelRotationAngle(270);
        xAxis.setYOffset(10f);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);
        xAxis.setSpaceBetweenLabels(1);

        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);


        // change the position of the y-labels

        YAxis leftAxis = lineChart.getAxisLeft();
        //leftAxis.setValueFormatter(new MyYAxisValueFormatter());
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        //leftAxis.setLabelCount(1, false);
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)
        lineChart.getAxisRight().setEnabled(false);
        //leftAxis.setSpaceTop(15f);

        if (POSUtil.isLabelWhite(context)) {
            lineChart.getAxisLeft().setTextColor(Color.WHITE);
            lineChart.getXAxis().setTextColor(Color.WHITE);
        }


        //leftAxis.setLabelCount(5, true);        // no description text
        lineChart.setDescription("");
        String need = context.getTheme().obtainStyledAttributes(new int[]{R.attr.u_need_to_provide_data_for_chart}).getString(0);
        lineChart.setNoDataTextDescription(need);

        // enable touch gestures
        lineChart.setTouchEnabled(true);


        // set the marker to the chart
        // enable scaling and dragging
        lineChart.setDragEnabled(false);
        lineChart.setScaleEnabled(false);
        lineChart.setDrawBorders(false);
        lineChart.getLegend().setEnabled(false);

        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(false);
        lineChart.setDrawGridBackground(false);


        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setLabelCount(5, false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)

        // set data
        // do not forget to refresh the chart
        // holder.chart.invalidate();
        //lineChart.setDescriptionPosition(Float.parseFloat(XAxis.XAxisPosition.TOP.toString()),Float.parseFloat(YAxis.AxisDependency.RIGHT.toString()));


        setLineDataToChart();

        chartDescTextView.setText(chartDesc);
        lineChart.invalidate();
    }

    private void setLineDataToChart() {

        initializingDataForLineChart();
        LineDataSet d1 = new LineDataSet(e1, "");
        d1.setLineWidth(2.5f);
        d1.setCircleRadius(4.5f);
        d1.setHighLightColor(Color.rgb(244, 117, 117));
        d1.setDrawValues(false);
        d1.setColor(Color.parseColor("#4dd0e1"));
        d1.setCircleColor(Color.parseColor("#4dd0e1"));
        d1.setDrawFilled(true);
        // fill drawable only supported on api level 18 and above
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.fade_blue);
        d1.setFillDrawable(drawable);

        ArrayList<ILineDataSet> sets = new ArrayList<ILineDataSet>();
        sets.add(d1);
        LineData cd = new LineData(xVals, sets);
        lineChart.setData(cd);
    }

    public void initializingDataForLineChart() {
        reportItemList = new ArrayList<>();
        e1 = new ArrayList<Entry>();
        initializeList();
        int i = 0;
        if (reportType.equalsIgnoreCase("monthly sales transactions") || reportType.equalsIgnoreCase("monthly purchase transactions")) {
            for (ReportItem reportItem : reportItemList) {
                Log.e(reportItem.getQty() + " ", "stockID");
                e1.add(new Entry(Float.parseFloat(Integer.toString(reportItem.getQty())), i));
                i++;
            }
        } else if (reportType.equalsIgnoreCase("monthly gross profit by product")) {
            for (ReportItem reportItem : reportItemList) {
                e1.add(new Entry(Float.parseFloat(reportItem.getBalance().toString()), i));
                i++;
            }
        }
        Log.e("e", e1.size() + " if");
        xVals = new ArrayList<String>();
        if (isTwelve) {
            int cal = Calendar.getInstance().get(Calendar.MONTH);
            for (i = 0; i < 12; i++) {
                xVals.add(mMonths[cal]);
                cal -= 1;
                if (cal < 0) {
                    cal = 11;
                }

            }
        } else {
            for (i = 0; i < 12; i++) {
                //Log.e("ccccc",cal+"");
                xVals.add(mMonths[i]);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.main, menu);

        MenuItem item = menu.findItem(R.id.action_search);

        searchView.setMenuItem(item);

        super.onCreateOptionsMenu(menu, inflater);
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String label = "";

            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {
            setLineChartConfiguration();

            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
