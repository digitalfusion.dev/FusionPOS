package com.digitalfusion.android.pos.adapters.rvadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.User;

import java.util.List;

/**
 * Created by MD003 on 9/9/16.
 */
public class RVAdapterForStaffFilter extends RecyclerView.Adapter<RVAdapterForStaffFilter.FilterViewHolder> {
       List<User> filterNameList;

    private OnItemClickListener mItemClickListener;
    private RadioButton oldRb;
    private int currentPos;
    private int pos;
    private String allTrans;
    private Context context;
    public RVAdapterForStaffFilter(List<User> filterNameList,Context context) {
        this.filterNameList = filterNameList;
        this.context = context;
    }


    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_item_view, parent, false);
           allTrans = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);
            return new FilterViewHolder(view);
         }

    @Override
    public void onBindViewHolder(final FilterViewHolder holder, final int position) {

        if (position == 0) {
            holder.name.setText(allTrans);
        } else {

            if((!filterNameList.get(position-1).getRole().equalsIgnoreCase(User.ROLE.Sale.toString()))) {
                holder.name.setText(filterNameList.get(position-1).getUserName());
            }
            else
            {
                holder.itemView.setVisibility(View.GONE);
                holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0,0));
            }

        }
        if (position == currentPos && currentPos != -1) {
            oldRb = holder.rb;
            holder.rb.setChecked(true);
        } else {
            holder.rb.setChecked(false);
        }
        holder.itemView.setOnClickListener(view -> {
            if (mItemClickListener != null) {

                if (oldRb != null) {
                    oldRb.setChecked(false);
                    oldRb = holder.rb;
                    holder.rb.setChecked(true);
                }
                mItemClickListener.onItemClick(view, position);
            }
        });
    }




    @Override
    public int getItemCount() {
        return filterNameList.size() + 1;
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setCurrentPos(int currentPos) {
        if (currentPos < 0 && currentPos > filterNameList.size() - 1) {
            //throw new Exception("Current Position can't be greater than array size or smaller than 0");
        } else {
            this.currentPos = currentPos;
            //notifyDataSetChanged();

            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(null, currentPos);
            }
        }

    }

    public void setFilterNameList(List<User> filterNameList) {
        this.filterNameList = filterNameList;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class FilterViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        RadioButton rb;
        LinearLayout filterlayout;

        FilterViewHolder(View itemView) {
            super(itemView);
            rb = (RadioButton) itemView.findViewById(R.id.rb);
            name = (TextView) itemView.findViewById(R.id.name);
            filterlayout = (LinearLayout) itemView.findViewById(R.id.filterlayout);


        }
    }
}
