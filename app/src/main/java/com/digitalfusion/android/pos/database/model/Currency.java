package com.digitalfusion.android.pos.database.model;

import java.io.Serializable;

/**
 * Created by MD003 on 11/17/16.
 */

public class Currency implements Serializable {

    private Long id;
    private String sign;
    private String DisplayName;
    private boolean isActive;


    public Currency(Long id, String sign, String displayName) {
        this.id = id;
        this.sign = sign;
        DisplayName = displayName;
        this.isActive = false;
    }

    public Currency(String displayName, String sign) {
        DisplayName = displayName;
        this.sign = sign;
    }

    public Currency() {
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public void setActive(int isActive) {
        this.isActive = isActive != 0;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStatus() {
        if (isActive) {
            return 1;
        } else {
            return 0;
        }
    }
}
