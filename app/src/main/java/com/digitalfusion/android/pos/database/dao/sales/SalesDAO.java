package com.digitalfusion.android.pos.database.dao.sales;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.dao.ParentDAO;
import com.digitalfusion.android.pos.database.dao.IdGeneratorDAO;
import com.digitalfusion.android.pos.database.dao.StockDAO;
import com.digitalfusion.android.pos.database.model.SaleDelivery;
import com.digitalfusion.android.pos.database.model.SaleDeliveryAndPickUp;
import com.digitalfusion.android.pos.database.model.SalePickup;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.database.model.SalesHeader;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.database.model.SalesStock;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/29/16.
 */
public class SalesDAO extends ParentDAO {
    private static ParentDAO salesDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private SalesDetailDAO salesDetailDAO;
    private SalesHeader salesHeader;
    private List<SalesHistory> salesHistoryList;
    private SalesHistory salesHistory;
    private PickupDAO pickupDAO;
    private DeliveryDAO deliveryDAO;
    private List<SaleDeliveryAndPickUp> saleDeliveryAndPickUpList;
    private SaleDeliveryAndPickUp saleDeliveryAndPickUp;
    private List<SalesStock> salesStockList;
    private StockDAO stockDAO;
    private  String db_users;

    private SalesDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        salesDetailDAO = SalesDetailDAO.getSalesDetailDaoInstance(context);
        salesHeader = new SalesHeader();
        pickupDAO = PickupDAO.getPickupDaoInstance(context);
        deliveryDAO = DeliveryDAO.getDeliveryDaoInstance(context);
        stockDAO = StockDAO.getStockDaoInstance(context);
        //db_users = context.getDatabasePath(AppConstant.API_DATABASE).getPath();
        db_users = context.getDatabasePath(AppConstant.API_DATABASE).getPath();
    }

    public static SalesDAO getSalesDaoInstance(Context context) {
        if (salesDaoInstance == null) {
            salesDaoInstance = new SalesDAO(context);
        }
        return (SalesDAO) salesDaoInstance;
    }

    public boolean addNewSales(String voucherNo, String date, Long customerID, double totalAmt, String discount, Long taxID, double subTotal,
                               double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year,
                               Double taxRate, String taxType, List<SalesAndPurchaseItem> salesDetailVOs, Long discountID, Long userId , String changeBalance) {

        genID = idGeneratorDAO.getLastIdValue("Sales");
        genID++;
        query = "insert into " + AppConstant.SALES_TABLE_NAME + "(" +
                    AppConstant.SALES_ID           + ", " + AppConstant.SALES_VOUCHER_NUM     + ", " +
                    AppConstant.SALES_DATE         + ", " + AppConstant.SALES_CUSTOMER_ID     + ", " +
                    AppConstant.SALES_TOTAL_AMOUNT + ", " + AppConstant.SALES_DISCOUNT        + ", " +
                    AppConstant.SALES_TAX_ID       + ", " + AppConstant.SALES_SUB_TOTAL       + ", " +
                    AppConstant.SALES_TYPE         + ", " + AppConstant.SALES_DISCOUNT_AMOUNT + ", " +
                    AppConstant.SALES_REMARK       + ", " + AppConstant.SALES_PAID_AMOUNT     + ", " +
                    AppConstant.SALES_BALANCE      + ", " + AppConstant.SALES_TAX_AMOUNT      + ","  +
                    AppConstant.CREATED_DATE       + ", " + AppConstant.SALES_DAY             + ", " +
                    AppConstant.SALES_MONTH        + ", " + AppConstant.SALES_YEAR            + ", " +
                    AppConstant.SALES_TAX_RATE     + ", " + AppConstant.SALES_TIME            + ", " +
                    AppConstant.SALES_TAX_TYPE     + ", " + AppConstant.SALES_DISCOUNT_ID     + ", " +
                    AppConstant.SALES_USER_ID      + ","  + AppConstant.SALES_CHANGE_CASH    +
                ") " +
                "values(" +
                    genID      + ",?,?,"      + customerID  + ", "                                                 +
                    totalAmt   + ", ?, "      + taxID       + ", "                                                 +
                    subTotal   + ",?, "       + discountAmt + ", ?,"                                               +
                    paidAmt    + ", "         + balance     + ", "                                                 +
                    taxAmt     + ", ?,?,?,?," + taxRate     + ", strftime('%s', ?, time('now', 'localtime')), ?, " +
                    discountID + ", ?,?"    +
                ");";
        databaseWriteTransaction(flag);
        try {
            //Log.w("tota ", totalAmt + " d");
            database.execSQL(query, new String[]{voucherNo, date, discount, SalesManager.SaleType.Sales.toString(), remark, DateUtility.getTodayDate(), day, month, year, formatDateWithDash(day, month, year), taxType, userId.toString() , changeBalance});
            for (SalesAndPurchaseItem s : salesDetailVOs) {
                id = salesDetailDAO.addNewSalesDetail(genID, s.getStockID(), s.getQty(), s.getPrice(), s.getDiscountPercent(), s.getTaxID(), s.getTotalPrice(), s.getTaxAmt(), s.getDiscountAmount(), s.getTaxRate(), s.getTaxType());
            }
            Log.e("date is " + date, month);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    //region ... I don't what this method is for
    public boolean holdNewSales(String voucherNo, String date, Long customerID, double totalAmt, String discount, Long taxID, double subTotal,
                                double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year,
                                Double taxRate, String taxType, List<SalesAndPurchaseItem> salesDetailVOs, Long discountID) {
        genID = idGeneratorDAO.getLastIdValue("Sales");
        genID++;
        query = "insert into " + AppConstant.SALES_TABLE_NAME + "(" + AppConstant.SALES_ID + ", " + AppConstant.SALES_VOUCHER_NUM
                + ", " + AppConstant.SALES_DATE + ", " + AppConstant.SALES_CUSTOMER_ID + ", " + AppConstant.SALES_TOTAL_AMOUNT + ", " + AppConstant.SALES_DISCOUNT + ", " +
                AppConstant.SALES_TAX_ID + ", " + AppConstant.SALES_SUB_TOTAL + ", " + AppConstant.SALES_TYPE + ", " + AppConstant.SALES_DISCOUNT_AMOUNT + ", "
                + AppConstant.SALES_REMARK + ", " + AppConstant.SALES_PAID_AMOUNT + ", " + AppConstant.SALES_BALANCE + ", " + AppConstant.SALES_TAX_AMOUNT + "," + AppConstant.CREATED_DATE
                + ", " + AppConstant.SALES_DAY + ", " + AppConstant.SALES_MONTH + ", " + AppConstant.SALES_YEAR + ", " + AppConstant.SALES_TAX_RATE + ", " +
                AppConstant.SALES_TIME + ", " + AppConstant.SALES_TAX_TYPE + ", " + AppConstant.SALES_DISCOUNT_ID + ", " + AppConstant.SALES_CUSTOM_FIELD_1 + ") values(" + genID + ",?,?," + customerID + ", " + totalAmt + ", ?, " + taxID + ", " + subTotal + ",?, " + discountAmt +
                ", ?," + paidAmt + ", " + balance + ", " + taxAmt + ", ?,?,?,?," + taxRate + ", strftime('%s', ?, time('now', 'localtime')), ?, " + discountID + ",1" + ");";
        databaseWriteTransaction(flag);
        try {
            //Log.w("tota ", totalAmt + " d");
            database.execSQL(query, new String[]{voucherNo, date, discount, SalesManager.SaleType.Sales.toString(), remark, DateUtility.getTodayDate(), day, month, year, formatDateWithDash(day, month, year), taxType});
            for (SalesAndPurchaseItem s : salesDetailVOs) {
                id = salesDetailDAO.addNewSalesDetail(genID, s.getStockID(), s.getQty(), s.getPrice(), s.getDiscountPercent(), s.getTaxID(), s.getTotalPrice(), s.getTaxAmt(), s.getDiscountAmount(), s.getTaxRate(), s.getTaxType());
            }
            Log.e("date is " + date, month);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }
    //endregion

    public boolean addNewSalesWithPickup(String voucherNo, String date, Long customerID, double totalAmt, String discount, Long taxID, double subTotal,
                                         double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year,
                                         Double taxRate, List<SalesAndPurchaseItem> salesDetailVOs, SalePickup pickupView) {
        genID = idGeneratorDAO.getLastIdValue("Sales");
        genID++;
        query = "insert into " + AppConstant.SALES_TABLE_NAME + "(" + AppConstant.SALES_ID + ", " + AppConstant.SALES_VOUCHER_NUM
                + ", " + AppConstant.SALES_DATE + ", " + AppConstant.SALES_CUSTOMER_ID + ", " + AppConstant.SALES_TOTAL_AMOUNT + ", " + AppConstant.SALES_DISCOUNT + ", " +
                AppConstant.SALES_TAX_ID + ", " + AppConstant.SALES_SUB_TOTAL + ", " + AppConstant.SALES_TYPE + ", " + AppConstant.SALES_DISCOUNT_AMOUNT + ", "
                + AppConstant.SALES_REMARK + ", " + AppConstant.SALES_PAID_AMOUNT + ", " + AppConstant.SALES_BALANCE + ", " + AppConstant.SALES_TAX_AMOUNT + "," + AppConstant.CREATED_DATE
                + ", " + AppConstant.SALES_DAY + ", " + AppConstant.SALES_MONTH + ", " + AppConstant.SALES_YEAR + ", " + AppConstant.SALES_TAX_RATE + ") values(" + genID +
                ",?,?," + customerID + ", " + totalAmt + ", ?, " + taxID + ", " + subTotal + ",?, " + discountAmt + ", ?," + paidAmt + ", " + balance + ", " + taxAmt
                + ", ?,?,?,?," + taxRate + ");";
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{voucherNo, date, discount, SalesManager.SaleType.Pickup.toString(), remark, DateUtility.getTodayDate(), day, month, year});
            for (SalesAndPurchaseItem s : salesDetailVOs) {
                id = salesDetailDAO.addNewSalesDetail(genID, s.getStockID(), s.getQty(), s.getPrice(), s.getDiscountPercent(), s.getTaxID(), s.getTotalPrice(), s.getTaxAmt(), s.getDiscountAmount(), s.getTaxRate(), s.getTaxType());
            }
            pickupDAO.addStockPickup(pickupView.getDate(), pickupView.getPhoneNo(), genID, pickupView.getDay(), pickupView.getMonth(), pickupView.getYear());
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean addNewSalesWithDelivery(String voucherNo, String date, Long customerID, double totalAmt, String discount, Long taxID, double subTotal,
                                           double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year,
                                           Double taxRate, List<SalesAndPurchaseItem> salesDetailVOs, SaleDelivery deliveryView) {
        genID = idGeneratorDAO.getLastIdValue("Sales");
        genID++;
        query = "insert into " + AppConstant.SALES_TABLE_NAME + "(" + AppConstant.SALES_ID + ", " + AppConstant.SALES_VOUCHER_NUM
                + ", " + AppConstant.SALES_DATE + ", " + AppConstant.SALES_CUSTOMER_ID + ", " + AppConstant.SALES_TOTAL_AMOUNT + ", " + AppConstant.SALES_DISCOUNT + ", " +
                AppConstant.SALES_TAX_ID + ", " + AppConstant.SALES_SUB_TOTAL + ", " + AppConstant.SALES_TYPE + ", " + AppConstant.SALES_DISCOUNT_AMOUNT + ", "
                + AppConstant.SALES_REMARK + ", " + AppConstant.SALES_PAID_AMOUNT + ", " + AppConstant.SALES_BALANCE + ", " + AppConstant.SALES_TAX_AMOUNT + "," + AppConstant.CREATED_DATE
                + ", " + AppConstant.SALES_DAY + ", " + AppConstant.SALES_MONTH + ", " + AppConstant.SALES_YEAR + ", " + AppConstant.SALES_TAX_RATE + ", " + AppConstant.SALES_TIME + ") values(" + genID +
                ",?,?," + customerID + ", " + totalAmt + ", ?, " + taxID + ", " + subTotal + ",?, " + discountAmt + ", ?," + paidAmt + ", " + balance + ", "
                + taxAmt + ", ?,?,?,?," + taxRate + ", strftime('%s', ?, time('now', 'localtime')));";
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{voucherNo, date, discount, SalesManager.SaleType.Delivery.toString(), remark, DateUtility.getTodayDate(), day, month, year, formatDateWithDash(day, month, year)});
            for (SalesAndPurchaseItem s : salesDetailVOs) {
                id = salesDetailDAO.addNewSalesDetail(genID, s.getStockID(), s.getQty(), s.getPrice(), s.getDiscountPercent(), s.getTaxID(), s.getTotalPrice(), s.getTaxAmt(), s.getDiscountAmount(), s.getTaxRate(), s.getTaxType());
            }

            // Log.w("hero",deliveryView.getPhoneNo()+" HELERE");

            //Log.w("Noob",deliveryView.getCharges()+" CHARGE");

            //Log.w("hrere date delivery",deliveryView.getDay()+ deliveryView.getMonth()+deliveryView.getYear());

            deliveryDAO.addNewStockDelivery(deliveryView.getDate(), deliveryView.getPhoneNo(), deliveryView.getAddress(), deliveryView.getAgent(), deliveryView.getCharges(), genID, deliveryView.getDay(), deliveryView.getMonth(), deliveryView.getYear());


            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public SalesHeader getSalesBySalesID(Long id) {
        query = "select s." + AppConstant.SALES_ID + ", s." + AppConstant.SALES_VOUCHER_NUM + ", s." + AppConstant.SALES_DATE + ", s." + AppConstant.SALES_CUSTOMER_ID + ", c." +
                AppConstant.CUSTOMER_NAME + ", s." + AppConstant.SALES_TOTAL_AMOUNT + ", s." + AppConstant.SALES_DISCOUNT + ", s." + AppConstant.SALES_SUB_TOTAL + ", s." +
                AppConstant.SALES_TAX_ID + ", t." + AppConstant.TAX_RATE + ", t." + AppConstant.TAX_NAME + ", s." + AppConstant.SALES_TYPE + ", s." + AppConstant.SALES_DISCOUNT_AMOUNT
                + ", s." + AppConstant.SALES_REMARK + ", s." + AppConstant.SALES_PAID_AMOUNT + ", s." + AppConstant.SALES_BALANCE + ", s." + AppConstant.SALES_TAX_AMOUNT
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ",s." + AppConstant.SALES_YEAR + ", s."
                + AppConstant.SALES_TAX_RATE + ", " + AppConstant.SALES_TAX_TYPE + ", " + AppConstant.SALES_DISCOUNT_ID + ",s." + AppConstant.SALES_CUSTOM_FIELD_1 + ",s." + AppConstant.SALES_CHANGE_CASH + ",c." + AppConstant.CUSTOMER_PHONE_NUM + " , c." + AppConstant.CUSTOMER_ADDRESS + " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME + " c," + AppConstant.TAX_TABLE_NAME + " t where s."
                + AppConstant.SALES_CUSTOMER_ID + "=c."
                + AppConstant.CUSTOMER_ID +
                " and s." + AppConstant.SALES_TAX_ID + "= t." + AppConstant.TAX_ID + " and s." + AppConstant.SALES_ID + " = " + id;
        databaseReadTransaction(new InsertedBooleanHolder());
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                salesHeader.setId(cursor.getLong(0));
                salesHeader.setSaleVocherNo(cursor.getString(1));
                salesHeader.setSaleDate(cursor.getString(2));
                salesHeader.setCustomerID(cursor.getLong(3));
                salesHeader.setCustomerName(cursor.getString(4));
                salesHeader.setTotal(cursor.getDouble(5));
                salesHeader.setDiscount(cursor.getString(6));
                salesHeader.setSubtotal(cursor.getDouble(7));
                salesHeader.setTaxID(cursor.getLong(8));
                salesHeader.setTaxRate(cursor.getDouble(9));
                salesHeader.setTaxName(cursor.getString(10));
                salesHeader.setType(cursor.getString(11));
                salesHeader.setDiscountAmt(cursor.getDouble(12));
                salesHeader.setRemark(cursor.getString(13));
                salesHeader.setPaidAmt(cursor.getDouble(14));
                salesHeader.setBalance(cursor.getDouble(15));
                salesHeader.setTaxAmt(cursor.getDouble(16));
                salesHeader.setDay(cursor.getString(17));
                salesHeader.setMonth(cursor.getString(18));
                salesHeader.setYear(cursor.getString(19));
                salesHeader.setTaxRate(cursor.getDouble(20));
                salesHeader.setTaxType(cursor.getString(21));
                salesHeader.setDiscountID(cursor.getLong(22));
                salesHeader.setChangeBalance(cursor.getString(cursor.getColumnIndex(AppConstant.SALES_CHANGE_CASH)));
                salesHeader.setCustomerPhone(cursor.getString(cursor.getColumnIndex(AppConstant.CUSTOMER_PHONE_NUM)));
                salesHeader.setCustomerAddress(cursor.getString(cursor.getColumnIndex(AppConstant.CUSTOMER_ADDRESS)));
                if (!cursor.isNull(23)) {
                    salesHeader.setIsHold(Integer.parseInt(cursor.getString(23)));
                } else {

                }
                Log.e(cursor.getString(20), cursor.getString(21) + "lollll");
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return salesHeader;
    }

    public List<SalesHistory> getAllSales(int startLimit, int endLimit, InsertedBooleanHolder flag, String startDate, String endDate, Long userId) {
      /* database = databaseHelper.getReadableDatabase();
       database.execSQL("ATTACH '" + db_users + "' AS userRole_db;" );

        String usersTable = "userRole_db." + AppConstant.TABLE_USER_ROLES;*/
      database = databaseHelper.getReadableDatabase();
      database.execSQL( " ATTACH '" + db_users + "' AS userId_db;");


      String userTable = "userId_db." + AppConstant.TABLE_USER_ROLES;
      String whereCondition = userId == null ? "" : " and s." + AppConstant.SALES_USER_ID  + " = "  + userId;

        query = "select s." + AppConstant.SALES_ID + ", " +
                    "s." + AppConstant.SALES_VOUCHER_NUM + ", " +
                    "s." + AppConstant.SALES_DATE + ", " +
                    "s." + AppConstant.SALES_TOTAL_AMOUNT + ", " +
                    "c." + AppConstant.CUSTOMER_NAME + ", " +
                    "s." + AppConstant.SALES_CUSTOMER_ID + ", " +
                    "s." + AppConstant.SALES_DAY + ", " +
                    "s." + AppConstant.SALES_MONTH + ", " +
                    "s." + AppConstant.SALES_YEAR + ", " +
                    "s." + AppConstant.SALES_DAY + ", " +
                    "s." + AppConstant.SALES_MONTH + ", " +
                    "s." + AppConstant.SALES_YEAR + ", " +
                    "s." + AppConstant.SALES_TYPE + " , " +
                    "s." + AppConstant.SALES_PAID_AMOUNT + ",  " +
                    "s." + AppConstant.SALES_BALANCE + " - ifnull((select sum(" + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT + ") from " +
                        AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID + " ), 0.0) as bal , " +
                    "s." + AppConstant.SALES_CUSTOM_FIELD_1 + "," +
                    "u." + AppConstant.COLUMN_USER_NAME + "," +
                    "s." + AppConstant.SALES_CHANGE_CASH +
                " from " + AppConstant.CUSTOMER_TABLE_NAME + " c ," +
                AppConstant.SALES_TABLE_NAME + " s LEFT JOIN " + userTable + " u ON s." + AppConstant.SALES_USER_ID + " = u." + AppConstant.COLUMN_USER_ID +
                " where s." + AppConstant.SALES_CUSTOMER_ID + " = c." + AppConstant.CUSTOMER_ID +
                    " and s." + AppConstant.SALES_DATE + ">=?" +
                    " and s." + AppConstant.SALES_DATE + "<=?" +
                    " and s." + AppConstant.SALES_TYPE + " not like '%Cancel' " +
                    whereCondition +
                " order by  s." + AppConstant.SALES_TIME + " desc , s." + AppConstant.SALES_ID + " desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        salesHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    salesHistory = new SalesHistory();
                    salesHistory.setId(cursor.getLong(0));
                    salesHistory.setVoucherNo(cursor.getString(1));
                    salesHistory.setDate(cursor.getString(2));
                    salesHistory.setTotalAmount(cursor.getDouble(3));
                    salesHistory.setCustomer(cursor.getString(4));
                    salesHistory.setCustomerID(cursor.getLong(5));
                    salesHistory.setDay(cursor.getString(6));
                    salesHistory.setMonth(cursor.getString(7));
                    salesHistory.setYear(cursor.getString(8));
                    salesHistory.setDay(cursor.getString(9));
                    salesHistory.setMonth(cursor.getString(10));
                    salesHistory.setYear(cursor.getString(11));
                    salesHistory.setType(cursor.getString(12));
                    salesHistory.setPaidAmt(cursor.getDouble(13));
                    salesHistory.setTotalBal(cursor.getDouble(14));
                    salesHistory.setUserRoleName(cursor.getString(16));
                    salesHistory.setChangeBalance(cursor.getString(cursor.getColumnIndex(AppConstant.SALES_CHANGE_CASH)));

                    if (!cursor.isNull(15)) {
                        salesHistory.setIsHold(Integer.parseInt(cursor.getString(15)));
                    } else {
                        salesHistory.setIsHold(0);
                    }
                    salesHistoryList.add(salesHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
            database.execSQL("DETACH userId_db");
        }

        return salesHistoryList;
    }

    //region ... I don't what this method is for 2
    public List<SalesHistory> getAllSalesHold(int startLimit, int endLimit, InsertedBooleanHolder flag, String startDate, String endDate) {

        Log.w("here Sale Db", startDate + "  " + endDate);

        query = "select s." + AppConstant.SALES_ID + ", s." + AppConstant.SALES_VOUCHER_NUM + ", s." + AppConstant.SALES_DATE + ", s." + AppConstant.SALES_TOTAL_AMOUNT
                + ", c." + AppConstant.CUSTOMER_NAME + ", s." + AppConstant.SALES_CUSTOMER_ID
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ", s." + AppConstant.SALES_YEAR
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ",s." + AppConstant.SALES_YEAR + ", s." + AppConstant.SALES_TYPE + " , s." + AppConstant.SALES_PAID_AMOUNT + ",  s."
                + AppConstant.SALES_BALANCE + " - ifnull((select sum(" + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID +
                " ), 0.0) as bal , s." + AppConstant.SALES_CUSTOM_FIELD_1
                + " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME
                + " c where s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID + " and s." + AppConstant.SALES_DATE + ">=? and s." + AppConstant.SALES_DATE + "<=? and s." + AppConstant.SALES_CUSTOM_FIELD_1 + "= 1 and s."
                + AppConstant.SALES_TYPE + " not like '%Cancel' " + " order by  s." + AppConstant.SALES_TIME + " desc , s." + AppConstant.SALES_ID + " desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        salesHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    salesHistory = new SalesHistory();
                    salesHistory.setId(cursor.getLong(0));
                    salesHistory.setVoucherNo(cursor.getString(1));
                    salesHistory.setDate(cursor.getString(2));
                    salesHistory.setTotalAmount(cursor.getDouble(3));
                    salesHistory.setCustomer(cursor.getString(4));
                    salesHistory.setCustomerID(cursor.getLong(5));
                    salesHistory.setDay(cursor.getString(6));
                    salesHistory.setMonth(cursor.getString(7));
                    salesHistory.setYear(cursor.getString(8));
                    salesHistory.setDay(cursor.getString(9));
                    salesHistory.setMonth(cursor.getString(10));
                    salesHistory.setYear(cursor.getString(11));
                    salesHistory.setType(cursor.getString(12));
                    salesHistory.setPaidAmt(cursor.getDouble(13));
                    salesHistory.setTotalBal(cursor.getDouble(14));
                    if (!cursor.isNull(15)) {
                        salesHistory.setIsHold(Integer.parseInt(cursor.getString(15)));
                    } else {
                        salesHistory.setIsHold(0);
                    }
                    salesHistoryList.add(salesHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return salesHistoryList;
    }
    //endregion

    public SalesHistory getSalesHistoryViewByID(Long id) {
        query = "select s." + AppConstant.SALES_ID + ", s." + AppConstant.SALES_VOUCHER_NUM + ", s." + AppConstant.SALES_DATE + ", s." + AppConstant.SALES_TOTAL_AMOUNT
                + ", c." + AppConstant.CUSTOMER_NAME + ", s." + AppConstant.SALES_CUSTOMER_ID
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ", s." + AppConstant.SALES_YEAR
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ",s." + AppConstant.SALES_YEAR + ", s." + AppConstant.SALES_TYPE + " , s." + AppConstant.SALES_PAID_AMOUNT
                + ", s." + AppConstant.SALES_CUSTOM_FIELD_1 + " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME
                + " c where s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID + " and s." + AppConstant.SALES_ID + "=" + id + " and s."
                + AppConstant.SALES_TYPE + " not like '%Cancel'";
        databaseReadTransaction(flag);
        salesHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, null);
            Log.w("here", "queryhing");
            if (cursor.moveToFirst()) {
                salesHistory = new SalesHistory();
                salesHistory.setId(cursor.getLong(0));
                salesHistory.setVoucherNo(cursor.getString(1));
                salesHistory.setDate(cursor.getString(2));
                salesHistory.setTotalAmount(cursor.getDouble(3));
                salesHistory.setCustomer(cursor.getString(4));
                salesHistory.setCustomerID(cursor.getLong(5));
                salesHistory.setDay(cursor.getString(6));
                salesHistory.setMonth(cursor.getString(7));
                salesHistory.setYear(cursor.getString(8));
                salesHistory.setDay(cursor.getString(9));
                salesHistory.setMonth(cursor.getString(10));
                salesHistory.setYear(cursor.getString(11));
                salesHistory.setType(cursor.getString(12));
                salesHistory.setPaidAmt(cursor.getDouble(13));


            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return salesHistory;
    }

    public List<SalesHistory> getSalesWithVoucherNoOrCustomer(int startLimit, int endLimit, String searchStr) {
        query = "select s." + AppConstant.SALES_ID + ", " +
                    "s." + AppConstant.SALES_VOUCHER_NUM + ", " +
                    "s." + AppConstant.SALES_DATE + ", " +
                    "s." + AppConstant.SALES_TOTAL_AMOUNT + ", " +
                    "c." + AppConstant.CUSTOMER_NAME + ", " +
                    "s." + AppConstant.SALES_CUSTOMER_ID + ", " +
                    "s." + AppConstant.SALES_DAY + ", " +
                    "s." + AppConstant.SALES_MONTH + ", " +
                    "s." + AppConstant.SALES_YEAR + ", " +
                    "s." + AppConstant.SALES_DAY + ", " +
                    "s." + AppConstant.SALES_MONTH + "," +
                    "s." + AppConstant.SALES_YEAR + ", " +
                    "s." + AppConstant.SALES_TYPE + ", " +
                    "s." + AppConstant.SALES_PAID_AMOUNT + ", " +
                    "(s." + AppConstant.SALES_BALANCE + " - ifnull((select sum(" + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT + ") " +
                         "from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME +
                        " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID + " ), 0.0)) as bal " +
                    "from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME + " c " +
                    "where s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID + " " +
                        "and s." + AppConstant.SALES_TYPE + " not like '%Cancel' " +
                        "and (c." + AppConstant.CUSTOMER_NAME + " like ? || '%' or s." + AppConstant.SALES_VOUCHER_NUM + " like ? || '%')" +

                " order by  s." + AppConstant.SALES_TIME + " desc " + " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        salesHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    salesHistory = new SalesHistory();
                    salesHistory.setId(cursor.getLong(0));
                    salesHistory.setVoucherNo(cursor.getString(1));
                    salesHistory.setDate(cursor.getString(2));
                    salesHistory.setTotalAmount(cursor.getDouble(3));
                    salesHistory.setCustomer(cursor.getString(4));
                    salesHistory.setCustomerID(cursor.getLong(5));
                    salesHistory.setDay(cursor.getString(6));
                    salesHistory.setMonth(cursor.getString(7));
                    salesHistory.setYear(cursor.getString(8));
                    salesHistory.setDay(cursor.getString(9));
                    salesHistory.setMonth(cursor.getString(10));
                    salesHistory.setYear(cursor.getString(11));
                    salesHistory.setType(cursor.getString(12));
                    salesHistory.setPaidAmt(cursor.getDouble(13));
                    salesHistory.setTotalBal(cursor.getDouble(14));
                    salesHistoryList.add(salesHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return salesHistoryList;
    }

    public List<SalesHistory> getHoldSalesWithVoucherNoOrCustomer(int startLimit, int endLimit, String searchStr , String userId) {
        query = "select s." + AppConstant.SALES_ID + ", s." + AppConstant.SALES_VOUCHER_NUM + ", s." + AppConstant.SALES_DATE + ", s." + AppConstant.SALES_TOTAL_AMOUNT
                + ", c." + AppConstant.CUSTOMER_NAME + ", s." + AppConstant.SALES_CUSTOMER_ID
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ", s." + AppConstant.SALES_YEAR
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ",s." + AppConstant.SALES_YEAR + ", s." + AppConstant.SALES_TYPE + ", s." + AppConstant.SALES_PAID_AMOUNT
                + ", (  s."
                + AppConstant.SALES_BALANCE + " - ifnull((select sum(" + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID +
                " ), 0.0)) as bal from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME
                + " c where s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID + " and s." + AppConstant.SALES_CUSTOM_FIELD_1 + "=1  and s."
                + AppConstant.SALES_TYPE + " not like '%Cancel' and (c." + AppConstant.CATEGORY_NAME + " like ? || '%' or s." + AppConstant.SALES_VOUCHER_NUM + " like ? || '%')"
                + " order by  s." + AppConstant.SALES_TIME + " desc " + " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        salesHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    salesHistory = new SalesHistory();
                    salesHistory.setId(cursor.getLong(0));
                    salesHistory.setVoucherNo(cursor.getString(1));
                    salesHistory.setDate(cursor.getString(2));
                    salesHistory.setTotalAmount(cursor.getDouble(3));
                    salesHistory.setCustomer(cursor.getString(4));
                    salesHistory.setCustomerID(cursor.getLong(5));
                    salesHistory.setDay(cursor.getString(6));
                    salesHistory.setMonth(cursor.getString(7));
                    salesHistory.setYear(cursor.getString(8));
                    salesHistory.setDay(cursor.getString(9));
                    salesHistory.setMonth(cursor.getString(10));
                    salesHistory.setYear(cursor.getString(11));
                    salesHistory.setType(cursor.getString(12));
                    salesHistory.setPaidAmt(cursor.getDouble(13));
                    salesHistory.setTotalBal(cursor.getDouble(14));
                    salesHistoryList.add(salesHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return salesHistoryList;
    }

    public List<SalesHistory> getSaleByVoucherCustomerNameOnSearch(String searchStr, int startLimit, int endLimit) {
        query = "select s." + AppConstant.SALES_ID + ", s." + AppConstant.SALES_VOUCHER_NUM + ", s." + ", c." + AppConstant.CUSTOMER_NAME + " from " + AppConstant.SALES_TABLE_NAME + " s, " +
                AppConstant.CUSTOMER_TABLE_NAME + " c where s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID + " and s."
                + AppConstant.SALES_TYPE + " not like '%Cancel' and (c." + AppConstant.CUSTOMER_NAME + " like ? || '%' or s." + AppConstant.SALES_VOUCHER_NUM + " like ? || '%') order by " + AppConstant.SALES_TIME +
                " desc " + " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        salesHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    salesHistory = new SalesHistory();
                    salesHistory.setId(cursor.getLong(0));
                    salesHistory.setVoucherNo(cursor.getString(1));
                    //salesHistory.setDate(cursor.getString(2));
                    //salesHistory.setTotalAmount(cursor.getDouble(3));
                    salesHistory.setCustomer(cursor.getString(2));
                   /* salesHistory.setCustomerID(cursor.getLong(5));
                    salesHistory.setDay(cursor.getString(6));
                    salesHistory.setMonth(cursor.getString(7));
                    salesHistory.setYear(cursor.getString(8));
                    salesHistory.setDay(cursor.getString(9));
                    salesHistory.setMonth(cursor.getString(10));
                    salesHistory.setYear(cursor.getString(11));
                    salesHistory.setFeedbackType(cursor.getString(12));*/
                    salesHistoryList.add(salesHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return salesHistoryList;
    }

    public List<SalesHistory> getSaleByVoucherNoOnSearch(String voucherNo, int startLimit, int endLimit) {
        query = "select s." + AppConstant.SALES_ID + ", s." + AppConstant.SALES_VOUCHER_NUM + ", s." + ", c." + AppConstant.CUSTOMER_NAME + " from " + AppConstant.SALES_TABLE_NAME + " s, " +
                AppConstant.CUSTOMER_TABLE_NAME + " c where s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID + " and s."
                + AppConstant.SALES_TYPE + " not like '%Cancel' and s." + AppConstant.SALES_VOUCHER_NUM + " like ? || '%') order by " + AppConstant.SALES_TIME +
                " desc " + " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        salesHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{voucherNo});
            if (cursor.moveToFirst()) {
                do {
                    salesHistory = new SalesHistory();
                    salesHistory.setId(cursor.getLong(0));
                    salesHistory.setVoucherNo(cursor.getString(1));
                    //salesHistory.setDate(cursor.getString(2));
                    //salesHistory.setTotalAmount(cursor.getDouble(3));
                    salesHistory.setCustomer(cursor.getString(2));
                   /* salesHistory.setCustomerID(cursor.getLong(5));
                    salesHistory.setDay(cursor.getString(6));
                    salesHistory.setMonth(cursor.getString(7));
                    salesHistory.setYear(cursor.getString(8));
                    salesHistory.setDay(cursor.getString(9));
                    salesHistory.setMonth(cursor.getString(10));
                    salesHistory.setYear(cursor.getString(11));
                    salesHistory.setFeedbackType(cursor.getString(12));*/
                    salesHistoryList.add(salesHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return salesHistoryList;
    }

    public List<SalesHistory> getSaleByCustomerNameOnSearch(String customerName, int startLimit, int endLimit) {
        query = "select s." + AppConstant.SALES_ID + ", s." + AppConstant.SALES_VOUCHER_NUM + ", s." + ", c." + AppConstant.CUSTOMER_NAME + " from " + AppConstant.SALES_TABLE_NAME + " s, " +
                AppConstant.CUSTOMER_TABLE_NAME + " c where s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID + " and s."
                + AppConstant.SALES_TYPE + " not like '%Cancel' and c." + AppConstant.CUSTOMER_NAME + " like ? || '%') order by " + AppConstant.SALES_TIME +
                " desc " + " limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        salesHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{customerName});
            if (cursor.moveToFirst()) {
                do {
                    salesHistory = new SalesHistory();
                    salesHistory.setId(cursor.getLong(0));
                    salesHistory.setVoucherNo(cursor.getString(1));
                    //salesHistory.setDate(cursor.getString(2));
                    //salesHistory.setTotalAmount(cursor.getDouble(3));
                    salesHistory.setCustomer(cursor.getString(2));
                   /* salesHistory.setCustomerID(cursor.getLong(5));
                    salesHistory.setDay(cursor.getString(6));
                    salesHistory.setMonth(cursor.getString(7));
                    salesHistory.setYear(cursor.getString(8));
                    salesHistory.setDay(cursor.getString(9));
                    salesHistory.setMonth(cursor.getString(10));
                    salesHistory.setYear(cursor.getString(11));
                    salesHistory.setFeedbackType(cursor.getString(12));*/
                    salesHistoryList.add(salesHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return salesHistoryList;
    }

    public List<SalesHistory> getAllSaleCancels(int startLimit, int endLimit, InsertedBooleanHolder flag, String startDate, String endDate) {
        query = "select s." + AppConstant.SALES_ID + ", s." + AppConstant.SALES_VOUCHER_NUM + ", s." + AppConstant.SALES_DATE + ", s." + AppConstant.SALES_TOTAL_AMOUNT
                + ", c." + AppConstant.CUSTOMER_NAME + ", s." + AppConstant.SALES_CUSTOMER_ID
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ", s." + AppConstant.SALES_YEAR
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ",s." + AppConstant.SALES_YEAR + ", s." + AppConstant.SALES_TYPE
                + " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME
                + " c where s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID + " and s." + AppConstant.SALES_DATE + ">=? and s." + AppConstant.SALES_DATE + "<=? and s."
                + AppConstant.SALES_TYPE + " like '%Cancel' order by " + AppConstant.SALES_TIME + " desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        salesHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    salesHistory = new SalesHistory();
                    salesHistory.setId(cursor.getLong(0));
                    salesHistory.setVoucherNo(cursor.getString(1));
                    salesHistory.setDate(cursor.getString(2));
                    salesHistory.setTotalAmount(cursor.getDouble(3));
                    salesHistory.setCustomer(cursor.getString(4));
                    salesHistory.setCustomerID(cursor.getLong(5));
                    salesHistory.setDay(cursor.getString(6));
                    salesHistory.setMonth(cursor.getString(7));
                    salesHistory.setYear(cursor.getString(8));
                    salesHistory.setDay(cursor.getString(9));
                    salesHistory.setMonth(cursor.getString(10));
                    salesHistory.setYear(cursor.getString(11));
                    salesHistory.setType(cursor.getString(12));
                    salesHistoryList.add(salesHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return salesHistoryList;
    }

    public List<SalesHistory> getDeliveryCancels(int startLimit, int endLimit, String startDate, String endDate) {
        query = "select s." + AppConstant.SALES_ID + ", s." + AppConstant.SALES_VOUCHER_NUM + ", s." + AppConstant.SALES_DATE + ", s." + AppConstant.SALES_TOTAL_AMOUNT
                + ", c." + AppConstant.CUSTOMER_NAME + ", s." + AppConstant.SALES_CUSTOMER_ID
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ", s." + AppConstant.SALES_YEAR
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ",s." + AppConstant.SALES_YEAR + ", s." + AppConstant.SALES_TYPE
                + " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME
                + " c where s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID + " and s." + AppConstant.SALES_DATE + ">=? and s." + AppConstant.SALES_DATE + "<=? and s."
                + AppConstant.SALES_TYPE + " like '%Cancel' order by " + AppConstant.SALES_TIME + " desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        salesHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    salesHistory = new SalesHistory();
                    salesHistory.setId(cursor.getLong(0));
                    salesHistory.setVoucherNo(cursor.getString(1));
                    salesHistory.setDate(cursor.getString(2));
                    salesHistory.setTotalAmount(cursor.getDouble(3));
                    salesHistory.setCustomer(cursor.getString(4));
                    salesHistory.setCustomerID(cursor.getLong(5));
                    salesHistory.setDay(cursor.getString(6));
                    salesHistory.setMonth(cursor.getString(7));
                    salesHistory.setYear(cursor.getString(8));
                    salesHistory.setDay(cursor.getString(9));
                    salesHistory.setMonth(cursor.getString(10));
                    salesHistory.setYear(cursor.getString(11));
                    salesHistory.setType(cursor.getString(12));
                    salesHistoryList.add(salesHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return salesHistoryList;
    }

    public List<SalesHistory> getDeliveryCancelsOnSearch(int startLimit, int endLimit, String startDate, String endDate, String searchStr) {
        query = "select s." + AppConstant.SALES_ID + ", s." + AppConstant.SALES_VOUCHER_NUM + ", s." + AppConstant.SALES_DATE + ", s." + AppConstant.SALES_TOTAL_AMOUNT
                + ", c." + AppConstant.CUSTOMER_NAME + ", s." + AppConstant.SALES_CUSTOMER_ID
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ", s." + AppConstant.SALES_YEAR
                + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ",s." + AppConstant.SALES_YEAR + ", s." + AppConstant.SALES_TYPE
                + " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME
                + " c where s." + AppConstant.SALES_CUSTOMER_ID + "=c." + AppConstant.CUSTOMER_ID + " and (s."
                + AppConstant.SALES_VOUCHER_NUM + " like ? || '%' or c." + AppConstant.CUSTOMER_NAME + " like ? || '%') and "
                + AppConstant.SALES_DATE + ">=? and s." + AppConstant.SALES_DATE + "<=? and s."
                + AppConstant.SALES_TYPE + " like '%Cancel' order by " + AppConstant.SALES_TIME + " desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        salesHistoryList = new ArrayList<>();
        try {
            cursor = database.rawQuery(query, new String[]{searchStr, searchStr, startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    salesHistory = new SalesHistory();
                    salesHistory.setId(cursor.getLong(0));
                    salesHistory.setVoucherNo(cursor.getString(1));
                    salesHistory.setDate(cursor.getString(2));
                    salesHistory.setTotalAmount(cursor.getDouble(3));
                    salesHistory.setCustomer(cursor.getString(4));
                    salesHistory.setCustomerID(cursor.getLong(5));
                    salesHistory.setDay(cursor.getString(6));
                    salesHistory.setMonth(cursor.getString(7));
                    salesHistory.setYear(cursor.getString(8));
                    salesHistory.setDay(cursor.getString(9));
                    salesHistory.setMonth(cursor.getString(10));
                    salesHistory.setYear(cursor.getString(11));
                    salesHistory.setType(cursor.getString(12));
                    salesHistoryList.add(salesHistory);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return salesHistoryList;
    }

    public boolean updateSalesBySalesID(Long id, String date, Long customerID, double totalAmt, String discount, Long taxID, double subTotal,
                                        double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year, String type, double taxRate,
                                        List<SalesAndPurchaseItem> updateDetailList, List<Long> deletedIdList, SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID ,Long currentUserID , String changeBalance) {


        if (discountID != null && discountID == 0) {
            discountID = null;
        }
        query = "update " + AppConstant.SALES_TABLE_NAME + " set " + AppConstant.SALES_DATE + " =?, " + AppConstant.SALES_CUSTOMER_ID + " = " + customerID + ", "
                + AppConstant.SALES_TOTAL_AMOUNT + "= " + totalAmt + ", " + AppConstant.SALES_DISCOUNT + "=?, " +
                AppConstant.SALES_TAX_ID + "= " + taxID + ", " + AppConstant.SALES_SUB_TOTAL + "= " + subTotal + ","
                + AppConstant.SALES_DISCOUNT_AMOUNT + "= " + discountAmt + "," + AppConstant.SALES_TAX_RATE + "=" + taxRate + ", "
                + AppConstant.SALES_REMARK + "=?, " + AppConstant.SALES_PAID_AMOUNT + "= " + paidAmt + ", " + AppConstant.SALES_BALANCE + "= " + balance + ","
                + AppConstant.SALES_TAX_AMOUNT + "= " + taxAmt + ", " + AppConstant.SALES_DAY + "=?, " + AppConstant.SALES_MONTH + "=?, " + AppConstant.SALES_YEAR + "=?, "
                + AppConstant.SALES_TYPE + "=?, " + AppConstant.SALES_DISCOUNT_ID + " = " + discountID + ", " + AppConstant.SALES_USER_ID + " =? , " +  AppConstant.SALES_CHANGE_CASH + " = '" + changeBalance + "' , " + AppConstant.SALES_TAX_TYPE + " = ? where " + AppConstant.SALES_ID + "=" + id;
        databaseWriteTransaction(flag);
        try {

            database.execSQL(query, new String[]{date, discount, remark, day, month, year, type, currentUserID.toString(), taxType });

            Log.w("discount amut", " upadate");


            for (SalesAndPurchaseItem sd : updateDetailList) {

                Log.w("discount amut", sd.getDiscountAmount() + " Dsicon");

                if (sd.getId() == null) {

                    salesDetailDAO.addNewSalesDetail(id, sd.getStockID(), sd.getQty(), sd.getPrice(), sd.getDiscountPercent(), sd.getTaxID(), sd.getTotalPrice(), sd.getTaxAmt(),
                            sd.getDiscountAmount(), sd.getTaxRate(), sd.getTaxType());
                } else {

                    salesDetailDAO.updateSaleDetailByID(sd.getId(), sd.getStockID(), sd.getQty(), sd.getPrice(), sd.getDiscountPercent(), sd.getTaxID(), sd.getTotalPrice(), sd.getTaxAmt(),
                            sd.getDiscountAmount(), sd.getTaxRate());

                }
            }
            for (Long sID : deletedIdList) {

                salesDetailDAO.deleteSaleDetailByID(sID);

            }
         /*   if (type == SalesManager.SaleType.Pickup.toString() & pickupView != null){

                if (pickupView.getId() != null){

                    pickupDAO.updatePickupByID(pickupView.getId(), pickupView.getDate(), pickupView.getPhoneNo(), pickupView.getSalesID(), pickupView.getDay(), pickupView.getMonth(),
                            pickupView.getYear(), pickupView.getStatus());

                }else {

                    pickupDAO.addStockPickup(pickupView.getDate(), pickupView.getPhoneNo(), id, pickupView.getDay(), pickupView.getMonth(),
                            pickupView.getYear());

                }
            }*/
            if (type == SalesManager.SaleType.Delivery.toString() & deliveryView != null) {

                Log.w("delivery date editt", "helr  " + deliveryView.getDay() + deliveryView.getMonth() + deliveryView.getYear());

                if (deliveryView.getId() != null) {

                    deliveryDAO.updateDeliveryByID(deliveryView.getId(), deliveryView.getDate(), deliveryView.getPhoneNo(), deliveryView.getAddress(), deliveryView.getAgent(), deliveryView.getCharges(),

                            deliveryView.getSalesID(), deliveryView.getDay(), deliveryView.getMonth(), deliveryView.getYear(), deliveryView.getStatus());

                } else {

                    deliveryDAO.addNewStockDelivery(deliveryView.getDate(), deliveryView.getPhoneNo(), deliveryView.getAddress(), deliveryView.getAgent(), deliveryView.getCharges(),

                            id, deliveryView.getDay(), deliveryView.getMonth(), deliveryView.getYear());

                }
            }

            database.setTransactionSuccessful();

        } catch (SQLiteException e) {

            e.printStackTrace();

        } finally {

            database.endTransaction();

        }

        return flag.isInserted();

    }

    public boolean updateHoldToSalesBySalesID(Long id, String date, Long customerID, double totalAmt, String discount, Long taxID, double subTotal,
                                              double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year, String type, double taxRate,
                                              List<SalesAndPurchaseItem> updateDetailList, List<Long> deletedIdList, SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID) {


        if (discountID == 0) {
            discountID = null;
        }
        query = "update " + AppConstant.SALES_TABLE_NAME + " set " + AppConstant.SALES_DATE + " =?, " + AppConstant.SALES_CUSTOMER_ID + " = " + customerID + ", "
                + AppConstant.SALES_TOTAL_AMOUNT + "= " + totalAmt + ", " + AppConstant.SALES_DISCOUNT + "=?, " +
                AppConstant.SALES_TAX_ID + "= " + taxID + ", " + AppConstant.SALES_SUB_TOTAL + "= " + subTotal + ","
                + AppConstant.SALES_DISCOUNT_AMOUNT + "= " + discountAmt + "," + AppConstant.SALES_TAX_RATE + "=" + taxRate + ", "
                + AppConstant.SALES_REMARK + "=?, " + AppConstant.SALES_PAID_AMOUNT + "= " + paidAmt + ", " + AppConstant.SALES_BALANCE + "= " + balance + ","
                + AppConstant.SALES_TAX_AMOUNT + "= " + taxAmt + ", " + AppConstant.SALES_DAY + "=?, " + AppConstant.SALES_MONTH + "=?, " + AppConstant.SALES_YEAR + "=?, "
                + AppConstant.SALES_TYPE + "=?, " + AppConstant.SALES_CUSTOM_FIELD_1 + " = " + 0 + "," + AppConstant.SALES_DISCOUNT_ID + " = " + discountID + ", " + AppConstant.SALES_TAX_TYPE + " = ? where " + AppConstant.SALES_ID + "=" + id;
        databaseWriteTransaction(flag);
        try {

            database.execSQL(query, new String[]{date, discount, remark, day, month, year, type, taxType});

            Log.w("discount amut", " upadate");


            for (SalesAndPurchaseItem sd : updateDetailList) {

                Log.w("discount amut", sd.getDiscountAmount() + " Dsicon");

                if (sd.getId() == null) {

                    salesDetailDAO.addNewSalesDetail(id, sd.getStockID(), sd.getQty(), sd.getPrice(), sd.getDiscountPercent(), sd.getTaxID(), sd.getTotalPrice(), sd.getTaxAmt(),
                            sd.getDiscountAmount(), sd.getTaxRate(), sd.getTaxType());
                } else {

                    salesDetailDAO.updateSaleDetailByID(sd.getId(), sd.getStockID(), sd.getQty(), sd.getPrice(), sd.getDiscountPercent(), sd.getTaxID(), sd.getTotalPrice(), sd.getTaxAmt(),
                            sd.getDiscountAmount(), sd.getTaxRate());

                }
            }
            for (Long sID : deletedIdList) {

                salesDetailDAO.deleteSaleDetailByID(sID);

            }
         /*   if (type == SalesManager.SaleType.Pickup.toString() & pickupView != null){

                if (pickupView.getId() != null){

                    pickupDAO.updatePickupByID(pickupView.getId(), pickupView.getDate(), pickupView.getPhoneNo(), pickupView.getSalesID(), pickupView.getDay(), pickupView.getMonth(),
                            pickupView.getYear(), pickupView.getStatus());

                }else {

                    pickupDAO.addStockPickup(pickupView.getDate(), pickupView.getPhoneNo(), id, pickupView.getDay(), pickupView.getMonth(),
                            pickupView.getYear());

                }
            }*/
            if (type == SalesManager.SaleType.Delivery.toString() & deliveryView != null) {

                Log.w("delivery date editt", "helr  " + deliveryView.getDay() + deliveryView.getMonth() + deliveryView.getYear());

                if (deliveryView.getId() != null) {

                    deliveryDAO.updateDeliveryByID(deliveryView.getId(), deliveryView.getDate(), deliveryView.getPhoneNo(), deliveryView.getAddress(), deliveryView.getAgent(), deliveryView.getCharges(),

                            deliveryView.getSalesID(), deliveryView.getDay(), deliveryView.getMonth(), deliveryView.getYear(), deliveryView.getStatus());

                } else {

                    deliveryDAO.addNewStockDelivery(deliveryView.getDate(), deliveryView.getPhoneNo(), deliveryView.getAddress(), deliveryView.getAgent(), deliveryView.getCharges(),

                            id, deliveryView.getDay(), deliveryView.getMonth(), deliveryView.getYear());

                }
            }

            database.setTransactionSuccessful();

        } catch (SQLiteException e) {

            e.printStackTrace();

        } finally {

            database.endTransaction();

        }

        return flag.isInserted();

    }

    //region ... I don't what this method is for 3
    public boolean updateHoldBySalesID(Long id, String date, Long customerID, double totalAmt, String discount, Long taxID, double subTotal,
                                       double discountAmt, String remark, double paidAmt, double balance, double taxAmt, String day, String month, String year, String type, double taxRate,
                                       List<SalesAndPurchaseItem> updateDetailList, List<Long> deletedIdList, SalePickup pickupView, SaleDelivery deliveryView, String taxType, Long discountID) {


        if (discountID == 0) {
            discountID = null;
        }
        query = "update " + AppConstant.SALES_TABLE_NAME + " set " + AppConstant.SALES_DATE + " =?, " + AppConstant.SALES_CUSTOMER_ID + " = " + customerID + ", "
                + AppConstant.SALES_TOTAL_AMOUNT + "= " + totalAmt + ", " + AppConstant.SALES_DISCOUNT + "=?, " +
                AppConstant.SALES_TAX_ID + "= " + taxID + ", " + AppConstant.SALES_SUB_TOTAL + "= " + subTotal + ","
                + AppConstant.SALES_DISCOUNT_AMOUNT + "= " + discountAmt + "," + AppConstant.SALES_TAX_RATE + "=" + taxRate + ", "
                + AppConstant.SALES_REMARK + "=?, " + AppConstant.SALES_PAID_AMOUNT + "= " + paidAmt + ", " + AppConstant.SALES_BALANCE + "= " + balance + ","
                + AppConstant.SALES_TAX_AMOUNT + "= " + taxAmt + ", " + AppConstant.SALES_DAY + "=?, " + AppConstant.SALES_MONTH + "=?, " + AppConstant.SALES_YEAR + "=?, "
                + AppConstant.SALES_TYPE + "=?, " + AppConstant.SALES_DISCOUNT_ID + " = " + discountID + ", " + AppConstant.SALES_TAX_TYPE + " = ? where " + AppConstant.SALES_ID + "=" + id;
        databaseWriteTransaction(flag);
        try {

            database.execSQL(query, new String[]{date, discount, remark, day, month, year, type, taxType});

            Log.w("discount amut", " upadate");


            for (SalesAndPurchaseItem sd : updateDetailList) {

                Log.w("discount amut", sd.getDiscountAmount() + " Dsicon");

                if (sd.getId() == null) {

                    salesDetailDAO.addNewSalesDetail(id, sd.getStockID(), sd.getQty(), sd.getPrice(), sd.getDiscountPercent(), sd.getTaxID(), sd.getTotalPrice(), sd.getTaxAmt(),
                            sd.getDiscountAmount(), sd.getTaxRate(), sd.getTaxType());
                } else {

                    salesDetailDAO.updateSaleDetailByID(sd.getId(), sd.getStockID(), sd.getQty(), sd.getPrice(), sd.getDiscountPercent(), sd.getTaxID(), sd.getTotalPrice(), sd.getTaxAmt(),
                            sd.getDiscountAmount(), sd.getTaxRate());

                }
            }
            for (Long sID : deletedIdList) {

                salesDetailDAO.deleteSaleDetailByID(sID);

            }
         /*   if (type == SalesManager.SaleType.Pickup.toString() & pickupView != null){

                if (pickupView.getId() != null){

                    pickupDAO.updatePickupByID(pickupView.getId(), pickupView.getDate(), pickupView.getPhoneNo(), pickupView.getSalesID(), pickupView.getDay(), pickupView.getMonth(),
                            pickupView.getYear(), pickupView.getStatus());

                }else {

                    pickupDAO.addStockPickup(pickupView.getDate(), pickupView.getPhoneNo(), id, pickupView.getDay(), pickupView.getMonth(),
                            pickupView.getYear());

                }
            }*/
            if (type == SalesManager.SaleType.Delivery.toString() & deliveryView != null) {

                Log.w("delivery date editt", "helr  " + deliveryView.getDay() + deliveryView.getMonth() + deliveryView.getYear());

                if (deliveryView.getId() != null) {

                    deliveryDAO.updateDeliveryByID(deliveryView.getId(), deliveryView.getDate(), deliveryView.getPhoneNo(), deliveryView.getAddress(), deliveryView.getAgent(), deliveryView.getCharges(),

                            deliveryView.getSalesID(), deliveryView.getDay(), deliveryView.getMonth(), deliveryView.getYear(), deliveryView.getStatus());

                } else {

                    deliveryDAO.addNewStockDelivery(deliveryView.getDate(), deliveryView.getPhoneNo(), deliveryView.getAddress(), deliveryView.getAgent(), deliveryView.getCharges(),

                            id, deliveryView.getDay(), deliveryView.getMonth(), deliveryView.getYear());

                }
            }

            database.setTransactionSuccessful();

        } catch (SQLiteException e) {

            e.printStackTrace();

        } finally {

            database.endTransaction();

        }

        return flag.isInserted();

    }
    //endregion

    public List<SaleDeliveryAndPickUp> getUpcomingPickupAndDelivery(String startDate, String endDate, int startLimit, int endLimit) {
        saleDeliveryAndPickUpList = new ArrayList<>();
        query = "select a.saleID, a.saleDate, a.saleType, a.pickOrDeliID, a.pickOrDeliStatus, a.pickOrDeliDate, a.customerID, c.name, a.saleDay, a.saleMonth, a.saleYear, a.pickOrDeliDay, a.pickDeliMonth, a.pickOrDeliYear, a.saleVoucherNo, a.time"
                + " from ( " + /*"select s." + AppConstant.SALES_ID + " as saleID, s." + AppConstant.SALES_DATE + " as saleDate, s." + AppConstant.SALES_TYPE + " as saleType, p." + AppConstant.PICKUP_ID +
                " as pickOrDeliID, p." + AppConstant.PICKUP_STATUS + " as pickOrDeliStatus, p." + AppConstant.PICKUP_DATE + " as pickOrDeliDate, s." + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_DAY +
                " as saleDay, s." + AppConstant.SALES_MONTH + " as saleMonth, s." + AppConstant.SALES_YEAR + " as saleYear, p." + AppConstant.PICKUP_DAY + " as pickOrDeliDay, p." + AppConstant.PICKUP_MONTH + " as pickDeliMonth, p."
                + AppConstant.PICKUP_YEAR + " as pickOrDeliYear, s. " + AppConstant.SALES_VOUCHER_NUM + " as saleVoucherNo" + " from " +
                AppConstant.SALES_TABLE_NAME + " as s," + AppConstant.PICKUP_TABLE_NAME + " as p where s." + AppConstant.SALES_ID + " = p." + AppConstant.PICKUP_SALES_ID + " and p." +
                AppConstant.PICKUP_DATE + " >= ? and p." + AppConstant.PICKUP_DATE + " <= ?" +
                " union all "
                + */" select s." + AppConstant.SALES_ID + " as saleID, s." + AppConstant.SALES_DATE
                + " as saleDate, s." + AppConstant.SALES_TYPE + " as saleType, d." + AppConstant.DELIVERY_ID + " as pickOrDeliID, d." + AppConstant.DELIVERY_STATUS + " as pickOrDeliStatus, d." +
                AppConstant.DELIVERY_DATE + " as pickOrDeliDate, s." + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_DAY + " as saleDay, s." + AppConstant.SALES_MONTH + " as saleMonth, s." + AppConstant.SALES_YEAR +
                " as saleYear, d." + AppConstant.DELIVERY_DAY + " as pickOrDeliDay, d." + AppConstant.DELIVERY_MONTH + " as pickDeliMonth, d." + AppConstant.DELIVERY_YEAR + " as pickOrDeliYear, s. " + AppConstant.SALES_VOUCHER_NUM + " as saleVoucherNo"
                + ",  s."
                + AppConstant.SALES_BALANCE + " - ifnull((select sum(" + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID +
                " ), 0.0) as bal, d." + AppConstant.DELIVERY_TIME
                + " from " + AppConstant.SALES_TABLE_NAME + " as s," + AppConstant.DELIVERY_TABLE_NAME
                + " as d where s." + AppConstant.SALES_ID + " = d." + AppConstant.DELIVERY_SALES_ID + " and d." + AppConstant.DELIVERY_DATE + " >= ? and d." + AppConstant.DELIVERY_DATE + " <= ?"
                + " ) as a, " + AppConstant.CUSTOMER_TABLE_NAME + " as c where a." + AppConstant.SALES_CUSTOMER_ID + " = c." + AppConstant.CUSTOMER_ID + " and a.pickOrDeliStatus = " + 0 + " and a.saleType not like '%Cancel'order by a.time desc limit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    saleDeliveryAndPickUp = new SaleDeliveryAndPickUp();
                    saleDeliveryAndPickUp.setSaleID(cursor.getLong(0));
                    saleDeliveryAndPickUp.setSaleDate(cursor.getString(1));
                    saleDeliveryAndPickUp.setType(cursor.getString(2));
                    saleDeliveryAndPickUp.setPickupOrDeliveryID(cursor.getLong(3));
                    saleDeliveryAndPickUp.setPickupOrDeliveryStatus(cursor.getInt(4));
                    saleDeliveryAndPickUp.setPickupOrDeliveryDate(cursor.getString(5));
                    saleDeliveryAndPickUp.setCustomerID(cursor.getLong(6));
                    saleDeliveryAndPickUp.setCustomerName(cursor.getString(7));
                    saleDeliveryAndPickUp.setSaleDay(cursor.getString(8));
                    saleDeliveryAndPickUp.setSaleMonth(cursor.getString(9));
                    saleDeliveryAndPickUp.setSaleYear(cursor.getString(10));
                    saleDeliveryAndPickUp.setPickupOrDeliveryDay(cursor.getString(11));
                    saleDeliveryAndPickUp.setPickupOrDeliverymonth(cursor.getString(12));
                    saleDeliveryAndPickUp.setPickupOrDeliveryYear(cursor.getString(13));
                    saleDeliveryAndPickUp.setSaleVoucherNo(cursor.getString(14));
                    saleDeliveryAndPickUpList.add(saleDeliveryAndPickUp);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return saleDeliveryAndPickUpList;
    }

    public List<SaleDeliveryAndPickUp> getUpcomingPickupAndDeliveryOnSearch(String startDate, String endDate, int startLimit, int endLimit, String searchStr) {
        saleDeliveryAndPickUpList = new ArrayList<>();
        query = "select a.saleID, a.saleDate, a.saleType, a.pickOrDeliID, a.pickOrDeliStatus, a.pickOrDeliDate, a.customerID, c.name, a.saleDay, a.saleMonth, a.saleYear, a.pickOrDeliDay, a.pickDeliMonth, a.pickOrDeliYear, a.saleVoucherNo, a.time "
                + " from ( " + /*"select s." + AppConstant.SALES_ID + " as saleID, s." + AppConstant.SALES_DATE + " as saleDate, s." + AppConstant.SALES_TYPE + " as saleType, p." + AppConstant.PICKUP_ID +
                " as pickOrDeliID, p." + AppConstant.PICKUP_STATUS + " as pickOrDeliStatus, p." + AppConstant.PICKUP_DATE + " as pickOrDeliDate, s." + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_DAY +
                " as saleDay, s." + AppConstant.SALES_MONTH + " as saleMonth, s." + AppConstant.SALES_YEAR + " as saleYear, p." + AppConstant.PICKUP_DAY + " as pickOrDeliDay, p." + AppConstant.PICKUP_MONTH + " as pickDeliMonth, p."
                + AppConstant.PICKUP_YEAR + " as pickOrDeliYear, s. " + AppConstant.SALES_VOUCHER_NUM + " as saleVoucherNo" + " from " +
                AppConstant.SALES_TABLE_NAME + " as s," + AppConstant.PICKUP_TABLE_NAME + " as p where s." + AppConstant.SALES_ID + " = p." + AppConstant.PICKUP_SALES_ID + " and p." +
                AppConstant.PICKUP_DATE + " >= ? and p." + AppConstant.PICKUP_DATE + " <= ?" +
                " union all "
                + */" select s." + AppConstant.SALES_ID + " as saleID, s." + AppConstant.SALES_DATE
                + " as saleDate, s." + AppConstant.SALES_TYPE + " as saleType, d." + AppConstant.DELIVERY_ID + " as pickOrDeliID, d." + AppConstant.DELIVERY_STATUS + " as pickOrDeliStatus, d." +
                AppConstant.DELIVERY_DATE + " as pickOrDeliDate, s." + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_DAY + " as saleDay, s." + AppConstant.SALES_MONTH + " as saleMonth, s." + AppConstant.SALES_YEAR +
                " as saleYear, d." + AppConstant.DELIVERY_DAY + " as pickOrDeliDay, d." + AppConstant.DELIVERY_MONTH + " as pickDeliMonth, d." + AppConstant.DELIVERY_YEAR + " as pickOrDeliYear, s. " + AppConstant.SALES_VOUCHER_NUM + " as saleVoucherNo, d."
                + AppConstant.DELIVERY_TIME + " from " + AppConstant.SALES_TABLE_NAME + " as s," + AppConstant.DELIVERY_TABLE_NAME
                + " as d where s." + AppConstant.SALES_ID + " = d." + AppConstant.DELIVERY_SALES_ID + " and d." + AppConstant.DELIVERY_DATE + " >= ? and d." + AppConstant.DELIVERY_DATE + " <= ?"
                + " ) as a, " + AppConstant.CUSTOMER_TABLE_NAME + " as c where a." + AppConstant.SALES_CUSTOMER_ID + " = c." + AppConstant.CUSTOMER_ID + " and a.pickOrDeliStatus = " + 0 +
                " and a.saleType not like '%Cancel' and (c." + AppConstant.CUSTOMER_NAME + " like ? || '%' or saleVoucherNo like ? || '%') order by a.time desc limit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    saleDeliveryAndPickUp = new SaleDeliveryAndPickUp();
                    saleDeliveryAndPickUp.setSaleID(cursor.getLong(0));
                    saleDeliveryAndPickUp.setSaleDate(cursor.getString(1));
                    saleDeliveryAndPickUp.setType(cursor.getString(2));
                    saleDeliveryAndPickUp.setPickupOrDeliveryID(cursor.getLong(3));
                    saleDeliveryAndPickUp.setPickupOrDeliveryStatus(cursor.getInt(4));
                    saleDeliveryAndPickUp.setPickupOrDeliveryDate(cursor.getString(5));
                    saleDeliveryAndPickUp.setCustomerID(cursor.getLong(6));
                    saleDeliveryAndPickUp.setCustomerName(cursor.getString(7));
                    saleDeliveryAndPickUp.setSaleDay(cursor.getString(8));
                    saleDeliveryAndPickUp.setSaleMonth(cursor.getString(9));
                    saleDeliveryAndPickUp.setSaleYear(cursor.getString(10));
                    saleDeliveryAndPickUp.setPickupOrDeliveryDay(cursor.getString(11));
                    saleDeliveryAndPickUp.setPickupOrDeliverymonth(cursor.getString(12));
                    saleDeliveryAndPickUp.setPickupOrDeliveryYear(cursor.getString(13));
                    saleDeliveryAndPickUp.setSaleVoucherNo(cursor.getString(14));
                    saleDeliveryAndPickUpList.add(saleDeliveryAndPickUp);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return saleDeliveryAndPickUpList;
    }

    public int getOverdueDeliveryCount() {
        count = 0;
        query = "select count(" + AppConstant.DELIVERY_ID + ") from " + AppConstant.DELIVERY_TABLE_NAME + " where " + AppConstant.DELIVERY_STATUS + " = 0 and " + AppConstant.DELIVERY_DATE + " < " +
                DateUtility.getTodayDate();
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return count;
    }


    public List<SaleDeliveryAndPickUp> getOverduePickupAndDelivery(String startDate, String endDate, int startLimit, int endLimit) {
        saleDeliveryAndPickUpList = new ArrayList<>();
        query = "select a.saleID, a.saleDate, a.saleType, a.pickOrDeliID, a.pickOrDeliStatus, a.pickOrDeliDate, a.customerID, c.name, a.saleDay, a.saleMonth, a.saleYear, a.pickOrDeliDay, a.pickDeliMonth, a.pickOrDeliYear, a.saleVoucherNo, a.time"
                + " from ( " + /*"select s." + AppConstant.SALES_ID + " as saleID, s." + AppConstant.SALES_DATE + " as saleDate, s." + AppConstant.SALES_TYPE + " as saleType, p." + AppConstant.PICKUP_ID +
                " as pickOrDeliID, p." + AppConstant.PICKUP_STATUS + " as pickOrDeliStatus, p." + AppConstant.PICKUP_DATE + " as pickOrDeliDate, s." + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_DAY +
                " as saleDay, s." + AppConstant.SALES_MONTH + " as saleMonth, s." + AppConstant.SALES_YEAR + " as saleYear, p." + AppConstant.PICKUP_DAY + " as pickOrDeliDay, p." + AppConstant.PICKUP_MONTH + " as pickDeliMonth, p."
                + AppConstant.PICKUP_YEAR + " as pickOrDeliYear, s." + AppConstant.SALES_VOUCHER_NUM + " as saleVoucherNo" + " from " +
                AppConstant.SALES_TABLE_NAME + " as s," + AppConstant.PICKUP_TABLE_NAME + " as p where s." + AppConstant.SALES_ID + " = p." + AppConstant.PICKUP_SALES_ID + " and p." +
                AppConstant.PICKUP_DATE + " > ? and p." + AppConstant.PICKUP_DATE + " < ?" +
                " union all "
                + */" select s." + AppConstant.SALES_ID + " as saleID, s." + AppConstant.SALES_DATE
                + " as saleDate, s." + AppConstant.SALES_TYPE + " as saleType, d." + AppConstant.DELIVERY_ID + " as pickOrDeliID, d." + AppConstant.DELIVERY_STATUS + " as pickOrDeliStatus, d." +
                AppConstant.DELIVERY_DATE + " as pickOrDeliDate, s." + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_DAY + " as saleDay, s." + AppConstant.SALES_MONTH + " as saleMonth, s." + AppConstant.SALES_YEAR +
                " as saleYear, d." + AppConstant.DELIVERY_DAY + " as pickOrDeliDay, d." + AppConstant.DELIVERY_MONTH + " as pickDeliMonth, d." + AppConstant.DELIVERY_YEAR + " as pickOrDeliYear, s." + AppConstant.SALES_VOUCHER_NUM + " as saleVoucherNo, d."
                + AppConstant.DELIVERY_TIME + "  from " + AppConstant.SALES_TABLE_NAME + " as s," + AppConstant.DELIVERY_TABLE_NAME
                + " as d where s." + AppConstant.SALES_ID + " = d." + AppConstant.DELIVERY_SALES_ID + " and d." + AppConstant.DELIVERY_DATE + " >= ? and d." + AppConstant.DELIVERY_DATE + " <= ?"
                + " ) as a, " + AppConstant.CUSTOMER_TABLE_NAME + " as c where a." + AppConstant.SALES_CUSTOMER_ID + " = c." + AppConstant.CUSTOMER_ID + " and a.pickOrDeliStatus = " + 0 + " and a.saleType not like '%Cancel' order by a.time asc limit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    saleDeliveryAndPickUp = new SaleDeliveryAndPickUp();
                    saleDeliveryAndPickUp.setSaleID(cursor.getLong(0));
                    saleDeliveryAndPickUp.setSaleDate(cursor.getString(1));
                    saleDeliveryAndPickUp.setType(cursor.getString(2));
                    saleDeliveryAndPickUp.setPickupOrDeliveryID(cursor.getLong(3));
                    saleDeliveryAndPickUp.setPickupOrDeliveryStatus(cursor.getInt(4));
                    saleDeliveryAndPickUp.setPickupOrDeliveryDate(cursor.getString(5));
                    saleDeliveryAndPickUp.setCustomerID(cursor.getLong(6));
                    saleDeliveryAndPickUp.setCustomerName(cursor.getString(7));
                    saleDeliveryAndPickUp.setSaleDay(cursor.getString(8));
                    saleDeliveryAndPickUp.setSaleMonth(cursor.getString(9));
                    saleDeliveryAndPickUp.setSaleYear(cursor.getString(10));
                    saleDeliveryAndPickUp.setPickupOrDeliveryDay(cursor.getString(11));
                    saleDeliveryAndPickUp.setPickupOrDeliverymonth(cursor.getString(12));
                    saleDeliveryAndPickUp.setPickupOrDeliveryYear(cursor.getString(13));
                    saleDeliveryAndPickUp.setSaleVoucherNo(cursor.getString(14));
                    saleDeliveryAndPickUpList.add(saleDeliveryAndPickUp);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return saleDeliveryAndPickUpList;
    }

    public List<SaleDeliveryAndPickUp> getOverduePickupAndDeliveryOnSearch(String startDate, String endDate, int startLimit, int endLimit, String searchStr) {
        saleDeliveryAndPickUpList = new ArrayList<>();
        query = "select a.saleID, a.saleDate, a.saleType, a.pickOrDeliID, a.pickOrDeliStatus, a.pickOrDeliDate, a.customerID, c.name, a.saleDay, a.saleMonth, a.saleYear, a.pickOrDeliDay, a.pickDeliMonth, a.pickOrDeliYear, a.saleVoucherNo, a.time"
                + " from ( " + /*"select s." + AppConstant.SALES_ID + " as saleID, s." + AppConstant.SALES_DATE + " as saleDate, s." + AppConstant.SALES_TYPE + " as saleType, p." + AppConstant.PICKUP_ID +
                " as pickOrDeliID, p." + AppConstant.PICKUP_STATUS + " as pickOrDeliStatus, p." + AppConstant.PICKUP_DATE + " as pickOrDeliDate, s." + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_DAY +
                " as saleDay, s." + AppConstant.SALES_MONTH + " as saleMonth, s." + AppConstant.SALES_YEAR + " as saleYear, p." + AppConstant.PICKUP_DAY + " as pickOrDeliDay, p." + AppConstant.PICKUP_MONTH + " as pickDeliMonth, p."
                + AppConstant.PICKUP_YEAR + " as pickOrDeliYear, s." + AppConstant.SALES_VOUCHER_NUM + " as saleVoucherNo" + " from " +
                AppConstant.SALES_TABLE_NAME + " as s," + AppConstant.PICKUP_TABLE_NAME + " as p where s." + AppConstant.SALES_ID + " = p." + AppConstant.PICKUP_SALES_ID + " and p." +
                AppConstant.PICKUP_DATE + " > ? and p." + AppConstant.PICKUP_DATE + " < ?" +
                " union all "
                + */" select s." + AppConstant.SALES_ID + " as saleID, s." + AppConstant.SALES_DATE
                + " as saleDate, s." + AppConstant.SALES_TYPE + " as saleType, d." + AppConstant.DELIVERY_ID + " as pickOrDeliID, d." + AppConstant.DELIVERY_STATUS + " as pickOrDeliStatus, d." +
                AppConstant.DELIVERY_DATE + " as pickOrDeliDate, s." + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_DAY + " as saleDay, s." + AppConstant.SALES_MONTH + " as saleMonth, s." + AppConstant.SALES_YEAR +
                " as saleYear, d." + AppConstant.DELIVERY_DAY + " as pickOrDeliDay, d." + AppConstant.DELIVERY_MONTH + " as pickDeliMonth, d." + AppConstant.DELIVERY_YEAR + " as pickOrDeliYear, s."
                + AppConstant.SALES_VOUCHER_NUM + " as saleVoucherNo, d." + AppConstant.DELIVERY_TIME + "  from " + AppConstant.SALES_TABLE_NAME + " as s," + AppConstant.DELIVERY_TABLE_NAME
                + " as d where s." + AppConstant.SALES_ID + " = d." + AppConstant.DELIVERY_SALES_ID + " and d." + AppConstant.DELIVERY_DATE + " > ? and d." + AppConstant.DELIVERY_DATE + " < ?"
                + " ) as a, " + AppConstant.CUSTOMER_TABLE_NAME + " as c where a." + AppConstant.SALES_CUSTOMER_ID + " = c." + AppConstant.CUSTOMER_ID + " and a.pickOrDeliStatus = " + 0 + " and a.saleType not like '%Cancel' and (saleVoucherNo like ? || '%' or "
                + " c." + AppConstant.CUSTOMER_NAME + " like ? || '%') order by a.time asc limit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    saleDeliveryAndPickUp = new SaleDeliveryAndPickUp();
                    saleDeliveryAndPickUp.setSaleID(cursor.getLong(0));
                    saleDeliveryAndPickUp.setSaleDate(cursor.getString(1));
                    saleDeliveryAndPickUp.setType(cursor.getString(2));
                    saleDeliveryAndPickUp.setPickupOrDeliveryID(cursor.getLong(3));
                    saleDeliveryAndPickUp.setPickupOrDeliveryStatus(cursor.getInt(4));
                    saleDeliveryAndPickUp.setPickupOrDeliveryDate(cursor.getString(5));
                    saleDeliveryAndPickUp.setCustomerID(cursor.getLong(6));
                    saleDeliveryAndPickUp.setCustomerName(cursor.getString(7));
                    saleDeliveryAndPickUp.setSaleDay(cursor.getString(8));
                    saleDeliveryAndPickUp.setSaleMonth(cursor.getString(9));
                    saleDeliveryAndPickUp.setSaleYear(cursor.getString(10));
                    saleDeliveryAndPickUp.setPickupOrDeliveryDay(cursor.getString(11));
                    saleDeliveryAndPickUp.setPickupOrDeliverymonth(cursor.getString(12));
                    saleDeliveryAndPickUp.setPickupOrDeliveryYear(cursor.getString(13));
                    saleDeliveryAndPickUp.setSaleVoucherNo(cursor.getString(14));
                    saleDeliveryAndPickUpList.add(saleDeliveryAndPickUp);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return saleDeliveryAndPickUpList;
    }

    public List<SaleDeliveryAndPickUp> getDeliveredPickupAndDelivery(String startDate, String endDate, int startLimit, int endLimit) {
        saleDeliveryAndPickUpList = new ArrayList<>();
        query = "select a.saleID, a.saleDate, a.saleType, a.pickOrDeliID, a.pickOrDeliStatus, a.pickOrDeliDate, a.customerID, c.name, a.saleDay, a.saleMonth, a.saleYear, a.pickOrDeliDay, a.pickDeliMonth, a.pickOrDeliYear, a.saleVoucherNo, a.time"
                + " from ( " + /*select s." + AppConstant.SALES_ID + " as saleID, s." + AppConstant.SALES_DATE + " as saleDate, s." + AppConstant.SALES_TYPE + " as saleType, p." + AppConstant.PICKUP_ID +
                " as pickOrDeliID, p." + AppConstant.PICKUP_STATUS + " as pickOrDeliStatus, p." + AppConstant.PICKUP_DATE + " as pickOrDeliDate, s." + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_DAY +
                " as saleDay, s." + AppConstant.SALES_MONTH + " as saleMonth, s." + AppConstant.SALES_YEAR + " as saleYear, p." + AppConstant.PICKUP_DAY + " as pickOrDeliDay, p." + AppConstant.PICKUP_MONTH + " as pickDeliMonth, p."
                + AppConstant.PICKUP_YEAR + " as pickOrDeliYear, s." + AppConstant.SALES_VOUCHER_NUM + " as saleVoucherNo" + " from " +
                AppConstant.SALES_TABLE_NAME + " as s," + AppConstant.PICKUP_TABLE_NAME + " as p where s." + AppConstant.SALES_ID + " = p." + AppConstant.PICKUP_SALES_ID + " and p." +
                AppConstant.PICKUP_DATE + " >= ? and p." + AppConstant.PICKUP_DATE + " <= ?" +
                " union all "
                +*/ " select s." + AppConstant.SALES_ID + " as saleID, s." + AppConstant.SALES_DATE
                + " as saleDate, s." + AppConstant.SALES_TYPE + " as saleType, d." + AppConstant.DELIVERY_ID + " as pickOrDeliID, d." + AppConstant.DELIVERY_STATUS + " as pickOrDeliStatus, d." +
                AppConstant.DELIVERY_DATE + " as pickOrDeliDate, s." + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_DAY + " as saleDay, s." + AppConstant.SALES_MONTH + " as saleMonth, s." + AppConstant.SALES_YEAR +
                " as saleYear, d." + AppConstant.DELIVERY_DAY + " as pickOrDeliDay, d." + AppConstant.DELIVERY_MONTH + " as pickDeliMonth, d." + AppConstant.DELIVERY_YEAR + " as pickOrDeliYear, s." + AppConstant.SALES_VOUCHER_NUM +
                " as saleVoucherNo, d." + AppConstant.DELIVERY_TIME + "  from " + AppConstant.SALES_TABLE_NAME + " as s," + AppConstant.DELIVERY_TABLE_NAME
                + " as d where s." + AppConstant.SALES_ID + " = d." + AppConstant.DELIVERY_SALES_ID + " and d." + AppConstant.DELIVERY_DATE + " >= ? and d." + AppConstant.DELIVERY_DATE + " <= ?"
                + " ) as a, " + AppConstant.CUSTOMER_TABLE_NAME + " as c where a." + AppConstant.SALES_CUSTOMER_ID + " = c." + AppConstant.CUSTOMER_ID + " and a.pickOrDeliStatus = " + 1 + " and a.saleType not like '%Cancel'order by a.time desc limit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    saleDeliveryAndPickUp = new SaleDeliveryAndPickUp();
                    saleDeliveryAndPickUp.setSaleID(cursor.getLong(0));
                    saleDeliveryAndPickUp.setSaleDate(cursor.getString(1));
                    saleDeliveryAndPickUp.setType(cursor.getString(2));
                    saleDeliveryAndPickUp.setPickupOrDeliveryID(cursor.getLong(3));
                    saleDeliveryAndPickUp.setPickupOrDeliveryStatus(cursor.getInt(4));
                    saleDeliveryAndPickUp.setPickupOrDeliveryDate(cursor.getString(5));
                    saleDeliveryAndPickUp.setCustomerID(cursor.getLong(6));
                    saleDeliveryAndPickUp.setCustomerName(cursor.getString(7));
                    saleDeliveryAndPickUp.setSaleDay(cursor.getString(8));
                    saleDeliveryAndPickUp.setSaleMonth(cursor.getString(9));
                    saleDeliveryAndPickUp.setSaleYear(cursor.getString(10));
                    Log.e(" s " + cursor.getString(11) + " " + cursor.getString(12), "df" + cursor.getString(13));
                    saleDeliveryAndPickUp.setPickupOrDeliveryDay(cursor.getString(11));
                    saleDeliveryAndPickUp.setPickupOrDeliverymonth(cursor.getString(12));
                    saleDeliveryAndPickUp.setPickupOrDeliveryYear(cursor.getString(13));
                    saleDeliveryAndPickUp.setSaleVoucherNo(cursor.getString(14));
                    saleDeliveryAndPickUpList.add(saleDeliveryAndPickUp);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return saleDeliveryAndPickUpList;
    }


    public List<SaleDeliveryAndPickUp> getDeliveredPickupAndDeliveryOnSearch(String startDate, String endDate, int startLimit, int endLimit, String searchStr) {
        saleDeliveryAndPickUpList = new ArrayList<>();
        query = "select a.saleID, a.saleDate, a.saleType, a.pickOrDeliID, a.pickOrDeliStatus, a.pickOrDeliDate, a.customerID, c.name, a.saleDay, a.saleMonth, a.saleYear, a.pickOrDeliDay, a.pickDeliMonth, a.pickOrDeliYear, a.saleVoucherNo, a.time"
                + " from ( " + /*select s." + AppConstant.SALES_ID + " as saleID, s." + AppConstant.SALES_DATE + " as saleDate, s." + AppConstant.SALES_TYPE + " as saleType, p." + AppConstant.PICKUP_ID +
                " as pickOrDeliID, p." + AppConstant.PICKUP_STATUS + " as pickOrDeliStatus, p." + AppConstant.PICKUP_DATE + " as pickOrDeliDate, s." + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_DAY +
                " as saleDay, s." + AppConstant.SALES_MONTH + " as saleMonth, s." + AppConstant.SALES_YEAR + " as saleYear, p." + AppConstant.PICKUP_DAY + " as pickOrDeliDay, p." + AppConstant.PICKUP_MONTH + " as pickDeliMonth, p."
                + AppConstant.PICKUP_YEAR + " as pickOrDeliYear, s." + AppConstant.SALES_VOUCHER_NUM + " as saleVoucherNo" + " from " +
                AppConstant.SALES_TABLE_NAME + " as s," + AppConstant.PICKUP_TABLE_NAME + " as p where s." + AppConstant.SALES_ID + " = p." + AppConstant.PICKUP_SALES_ID + " and p." +
                AppConstant.PICKUP_DATE + " >= ? and p." + AppConstant.PICKUP_DATE + " <= ?" +
                " union all "
                +*/ " select s." + AppConstant.SALES_ID + " as saleID, s." + AppConstant.SALES_DATE
                + " as saleDate, s." + AppConstant.SALES_TYPE + " as saleType, d." + AppConstant.DELIVERY_ID + " as pickOrDeliID, d." + AppConstant.DELIVERY_STATUS + " as pickOrDeliStatus, d." +
                AppConstant.DELIVERY_DATE + " as pickOrDeliDate, s." + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_DAY + " as saleDay, s." + AppConstant.SALES_MONTH + " as saleMonth, s." + AppConstant.SALES_YEAR +
                " as saleYear, d." + AppConstant.DELIVERY_DAY + " as pickOrDeliDay, d." + AppConstant.DELIVERY_MONTH + " as pickDeliMonth, d." + AppConstant.DELIVERY_YEAR + " as pickOrDeliYear, s." + AppConstant.SALES_VOUCHER_NUM +
                " as saleVoucherNo, d." + AppConstant.DELIVERY_TIME + "  from " + AppConstant.SALES_TABLE_NAME + " as s," + AppConstant.DELIVERY_TABLE_NAME
                + " as d where s." + AppConstant.SALES_ID + " = d." + AppConstant.DELIVERY_SALES_ID + " and d." + AppConstant.DELIVERY_DATE + " >= ? and d." + AppConstant.DELIVERY_DATE + " <= ?"
                + " ) as a, " + AppConstant.CUSTOMER_TABLE_NAME + " as c where a." + AppConstant.SALES_CUSTOMER_ID + " = c." + AppConstant.CUSTOMER_ID + " and a.pickOrDeliStatus = " + 1 + " and a.saleType not like '%Cancel' and (saleVoucherNo like ? || '%' or " +
                " c." + AppConstant.CUSTOMER_NAME + " like ? || '%') order by a.time desc limit " + startLimit + ", " + endLimit + ";";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    saleDeliveryAndPickUp = new SaleDeliveryAndPickUp();
                    saleDeliveryAndPickUp.setSaleID(cursor.getLong(0));
                    saleDeliveryAndPickUp.setSaleDate(cursor.getString(1));
                    saleDeliveryAndPickUp.setType(cursor.getString(2));
                    saleDeliveryAndPickUp.setPickupOrDeliveryID(cursor.getLong(3));
                    saleDeliveryAndPickUp.setPickupOrDeliveryStatus(cursor.getInt(4));
                    saleDeliveryAndPickUp.setPickupOrDeliveryDate(cursor.getString(5));
                    saleDeliveryAndPickUp.setCustomerID(cursor.getLong(6));
                    saleDeliveryAndPickUp.setCustomerName(cursor.getString(7));
                    saleDeliveryAndPickUp.setSaleDay(cursor.getString(8));
                    saleDeliveryAndPickUp.setSaleMonth(cursor.getString(9));
                    saleDeliveryAndPickUp.setSaleYear(cursor.getString(10));
                    Log.e(" s " + cursor.getString(11) + " " + cursor.getString(12), "df" + cursor.getString(13));
                    saleDeliveryAndPickUp.setPickupOrDeliveryDay(cursor.getString(11));
                    saleDeliveryAndPickUp.setPickupOrDeliverymonth(cursor.getString(12));
                    saleDeliveryAndPickUp.setPickupOrDeliveryYear(cursor.getString(13));
                    saleDeliveryAndPickUp.setSaleVoucherNo(cursor.getString(14));
                    saleDeliveryAndPickUpList.add(saleDeliveryAndPickUp);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return saleDeliveryAndPickUpList;
    }

    public boolean cancelOrder(Long salesID, String type) {

        query = "update " + AppConstant.SALES_TABLE_NAME + " set " + AppConstant.SALES_TYPE + " = ? where " + AppConstant.SALES_ID + " = " + salesID;

        databaseWriteTransaction(flag);

        try {

            database.execSQL(query, new String[]{type});

            salesStockList = salesDetailDAO.getSaleDetailStocksBySalesID(salesID);

            for (SalesStock s : salesStockList) {

                stockDAO.putOrderCancelIncreaseStockQty(s.getStockID(), s.getStockQty());

            }

            database.setTransactionSuccessful();

        } catch (SQLiteException e) {

            e.printStackTrace();

        } finally {

            database.endTransaction();

        }

        return flag.isInserted();

    }

    public boolean putBackOrder(Long salesID, String type) {
        query = "update " + AppConstant.SALES_TABLE_NAME + " set " + AppConstant.SALES_TYPE + " = ? where " + AppConstant.SALES_ID + " = " + salesID;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{type});
            salesStockList = salesDetailDAO.getSaleDetailStocksBySalesID(salesID);
            for (SalesStock s : salesStockList) {
                stockDAO.putBackOrderNormalDecreaseStockQty(s.getStockID(), s.getStockQty());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public Long findIDBySalesInvoice(String invoiceNo) {
        query = "select " + AppConstant.SALES_ID + " from " + AppConstant.SALES_TABLE_NAME + " where " + AppConstant.SALES_VOUCHER_NUM + " = ?";
        id = 0l;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{invoiceNo});
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }

    public Long findIDBySalesHoldInvoice(String invoiceNo) {
        query = "select " + AppConstant.SALES_ID + " from " + AppConstant.SALES_HOLD_TABLE_NAME + " where " + AppConstant.SALES_VOUCHER_NUM + " = ?";
        id = 0l;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{invoiceNo});
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }

    public boolean isPaymentExists(Long salesID) {
        query = "select " + AppConstant.CUSTOMER_OUTSTANDING_ID + " from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = " + salesID;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                return true;
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return false;
    }

    public boolean deleteSales(Long id) {
        databaseWriteTransaction(flag);
        try {
            query = "delete from " + AppConstant.SALES_DETAIL_TABLE_NAME + " where " + AppConstant.SALES_DETAIL_SALES_ID + " = " + id;
            database.execSQL(query);
            query = "delete from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = " + id;
            database.execSQL(query);
            query = "delete from " + AppConstant.DELIVERY_TABLE_NAME + " where " + AppConstant.DELIVERY_SALES_ID + " = " + id;
            database.execSQL(query);
            query = "delete from " + AppConstant.REFUND_TABLE_NAME + " where " + AppConstant.REFUND_SALES_ID + " = " + id;
            database.execSQL(query);
            query = "delete from " + AppConstant.SALES_TABLE_NAME + " where " + AppConstant.SALES_ID + " = " + id;
            database.execSQL(query);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }
}
