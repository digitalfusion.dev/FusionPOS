package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForTheme;
import com.digitalfusion.android.pos.database.ThemeObj;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by MD003 on 11/28/16.
 */

public class ThemeSettingListViewFragment extends Fragment {

    private View mainLayoutView;

    private Context context;

    private RecyclerView recyclerView;

    private ArrayList<String> themeList;

    private RVAdapterForTheme rvAdapterForTheme;

    // private SwitchCompat nightMode;


    private List<ThemeObj> themeObjList;

    private Button applyThemeBtn;

    private int selectedPos;

    private String currentThemeNameId;

    private int theme;
    private LinearLayout linearLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.theme_setting_list, null);

        context = getContext();

        //AppConstant.IS_THEME = true;

        theme = POSUtil.getDefaultThemeNoActionBar(getContext());

        String themestr = context.getTheme().obtainStyledAttributes(new int[]{R.attr.theme}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(themestr);

        themeObjList = new ArrayList<>();

        themeObjList = ThemeUtil.getThemeNameList(getContext());


        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.theme_rv);

        //  nightMode=(SwitchCompat)mainLayoutView.findViewById(R.id.night_mode);

        applyThemeBtn = (Button) mainLayoutView.findViewById(R.id.apply);

        rvAdapterForTheme = new RVAdapterForTheme(themeObjList);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        currentThemeNameId = POSUtil.getDefaultThemeName(context);

        MainActivity.setCurrentFragment(this);

        SharedPreferences prefs = context.getSharedPreferences(AppConstant.THEME, MODE_PRIVATE);

        boolean isNightMode = prefs.getBoolean(AppConstant.NIGHT_MODE, false);

        Log.w("Is Night Mode", isNightMode + " ss");

        if (isNightMode) {
            currentThemeNameId = ThemeUtil.Night;
        }

        for (ThemeObj themeObj : themeObjList) {

            if (themeObj.getThemeNameID().equalsIgnoreCase(currentThemeNameId) && !isNightMode) {
                selectedPos = themeObjList.indexOf(themeObj);
            } else if (themeObj.getThemeNameID().equalsIgnoreCase(ThemeUtil.Night) && isNightMode) {
                selectedPos = themeObjList.indexOf(themeObj);
            }

        }

        /*if (theme == R.style.Theme_Myan_Blue_NoActionBar || theme == R.style.Theme_Myan_Blue) {
            rvAdapterForTheme.setCurrentTheme("Blue Theme");

        } else if (theme == R.style.Theme_Myan_BlueGrey_NoActionBar || theme == R.style.Theme_Myan_BlueGrey) {
            rvAdapterForTheme.setCurrentTheme("Blue Grey");
        } else if (theme == R.style.Theme_Myan_Yellow_NoActionBar || theme == R.style.Theme_Myan_Yellow) {
            rvAdapterForTheme.setCurrentTheme("Yellow Theme");
        } else if (theme == R.style.Theme_Myan_Red_NoActionBar || theme == R.style.Theme_Myan_Red) {
            rvAdapterForTheme.setCurrentTheme("Red Theme");
        } else if (theme == R.style.Theme_Myan_White_NoActionBar || theme == R.style.Theme_Myan_White) {
            rvAdapterForTheme.setCurrentTheme("White Theme");
        }else {

            rvAdapterForTheme.setCurrentTheme("Night mode");

        }*/


     /*   if(!isNightMode){

            nightMode.setChecked(false);

            currentThemeName=POSUtil.getDefaultThemeName(context);

            rvAdapterForTheme.setCurrentTheme(currentThemeName);

        }else {
            nightMode.setChecked(true);
        }*/


        recyclerView.setAdapter(rvAdapterForTheme);

        rvAdapterForTheme.setCurrentTheme(currentThemeNameId);

        rvAdapterForTheme.setmItemClickListener(new RVAdapterForTheme.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                selectedPos = position;
                new LoadProgressDialog().execute();
                //   nightMode.setChecked(false);

            }
        });
        /*  nightMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                isNightMode=isChecked;

                if(isChecked){

                    rvAdapterForTheme.deSelectedCurrentRb();

                }

            }
        });*/


        applyThemeBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {

                Log.w("here", selectedPos + " SElect pos in aply");

                changeTheme();
            }
        });


        return mainLayoutView;

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void changeTheme() {
        if (themeObjList.get(selectedPos).getThemeNameID().equalsIgnoreCase(ThemeUtil.Night)) {

            POSUtil.activateNightMode(context);

            Intent intent = new Intent(getActivity(), MainActivity.class);

            startActivity(intent);
            getActivity().finishAffinity();
            getActivity().finish();
            MainActivity.end();
        } else {
            POSUtil.deActivateNightMode(context);

            POSUtil.changeDefaultTheme(themeObjList.get(selectedPos), context);

            Intent intent = new Intent(getActivity(), MainActivity.class);

            startActivity(intent);

            getActivity().finish();
            MainActivity.end();
        }
    }

    public void changeDefaultTheme(ThemeObj themeObj) {

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.THEME, MODE_PRIVATE).edit();

        editor.putInt(AppConstant.DEFAULT_THEME_NO_ACTION_BAR, themeObj.getThemeResourceNoActionBar());

        editor.putInt(AppConstant.DEFAULT_THEME_ACTION_BAR, themeObj.getThemeResourceActionBar());

        editor.commit();

    }

    public void loadUI() {


    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected String doInBackground(String... params) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("pre show", "show");
            String    label         = "";
            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setBackgroundColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            if (!hud.isShowing()) {
                hud.show();
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(String a) {

            //Log.e("begin", "idoaj");
            Log.e("on", "post execute");
            if (isAdded()) {
                if (hud.isShowing()) {
                    hud.dismiss();
                    Log.e("dismiss", "showing");
                    //    showSnackBar("Backup process is successful...");
                }
                changeTheme();
                Log.e("dismiss", "dismiss");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}