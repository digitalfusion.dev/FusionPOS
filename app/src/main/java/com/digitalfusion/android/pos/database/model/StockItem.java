package com.digitalfusion.android.pos.database.model;

import android.util.Log;

import java.io.Serializable;

/**
 * Created by MD003 on 8/17/16.
 */
public class StockItem implements Serializable {
    private String name;
    private String codeNo;
    private String categoryName;
    private String unitName;
    private Integer inventoryQty;
    private Integer reorderLevel;
    private Double purchasePrice;
    private Double retailPrice;
    private Double wholesalePrice;
    private String description;
    private Long id;
    private String barcode;
    private Long unitID;
    private Double normalPrice;
    //private List<byte[]> imageList;
    private Long categoryID;
    private boolean isNewCategory;
    private boolean isConflict = false;
    private boolean isNewUnit;


    public StockItem() {
        inventoryQty = 0;
        reorderLevel = 0;
        purchasePrice = 0.0;
        retailPrice = 0.0;
        wholesalePrice = 0.0;
        normalPrice = 0.0;
    }

    public Long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Long categoryID) {
        this.categoryID = categoryID;
    }

    public Long getUnitID() {
        return unitID;
    }

    public void setUnitID(Long unitID) {
        this.unitID = unitID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeNo() {
        return codeNo;
    }

    public void setCodeNo(String codeNo) {
        this.codeNo = codeNo;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }


    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public boolean isUnitEmpty() {
        if (unitName != null && unitName.trim().length() > 0) {

            Log.w("here", unitName.isEmpty() + " SS");

            return unitName.isEmpty();
        } else {
            return true;
        }
    }

    public Integer getInventoryQty() {
        return inventoryQty;
    }

    public void setInventoryQty(Integer inventoryQty) {
        this.inventoryQty = inventoryQty;
    }

    public Integer getReorderLevel() {
        return reorderLevel;
    }

    public void setReorderLevel(Integer reorderLevel) {
        this.reorderLevel = reorderLevel;
    }

    public Double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(Double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public Double getWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(Double wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public Double getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(Double normalPrice) {
        this.normalPrice = normalPrice;
    }

    public Double getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(Double retailPrice) {
        this.retailPrice = retailPrice;
    }

    /*public List<byte[]> getImageList() {
        return imageList;
    }

    public void setImageList(List<byte[]> imageList) {
        this.imageList = imageList;
    }*/

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public boolean isVaild() {
        return codeNo != null && !codeNo.isEmpty()
                && name != null && !name.isEmpty()
                && categoryName != null && !categoryName.isEmpty()
                && unitName != null && !unitName.isEmpty();
    }

    public boolean isNewCategory() {
        return isNewCategory;
    }

    public void setNewCategory(boolean newCategory) {
        isNewCategory = newCategory;
    }

    public boolean isConflict() {
        return isConflict;
    }

    public void setConflict(boolean conflict) {
        isConflict = conflict;
    }

    public boolean isNewUnit() {
        return isNewUnit;
    }

    public void setNewUnit(boolean newUnit) {
        isNewUnit = newUnit;
    }

    @Override
    public String toString() {
        return "StockItem{" +
                "name='" + name + '\'' +
                ", codeNo='" + codeNo + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", unitName='" + unitName + '\'' +
                ", inventoryQty=" + inventoryQty +
                ", reorderLevel=" + reorderLevel +
                ", purchasePrice=" + purchasePrice +
                ", retailPrice=" + retailPrice +
                ", wholesalePrice=" + wholesalePrice +
                ", description='" + description + '\'' +
                ", id=" + id +
                ", barcode='" + barcode + '\'' +
                ", unitID=" + unitID +
                ", normalPrice=" + normalPrice +
                ", categoryID=" + categoryID +
                ", isNewCategory=" + isNewCategory +
                ", isConflict=" + isConflict +
                ", isNewUnit=" + isNewUnit +
                '}';
    }

}
