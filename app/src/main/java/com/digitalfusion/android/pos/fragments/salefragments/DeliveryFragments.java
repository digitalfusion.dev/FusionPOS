package com.digitalfusion.android.pos.fragments.salefragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListenerdeliveryDialog;
import com.digitalfusion.android.pos.database.model.SaleDelivery;
import com.digitalfusion.android.pos.util.DateUtility;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

/**
 * Created by MD003 on 8/30/16.
 */
public class DeliveryFragments extends Fragment implements DatePickerDialog.OnDateSetListener {


    Context context;
    private View mainLayout;
    private TextInputLayout deliveryAgentTextInputLayout;
    private TextInputLayout customerNameTextInputLayout;
    private TextInputLayout deliveryChargesTextInputLayout;
    private TextInputLayout deliveryAddressTextInputLayout;
    private EditText deliveryAgentEditText;
    private EditText deliveryDateEditText;
    private EditText deliveryChargesEditText;
    private EditText deliveryCustomerNameEditText;
    private EditText deliveryPhoneNumberEditText;
    private EditText deliveryAddressEditText;
    private Button saveBtn;
    private Button cancelBtn;
    private ImageButton datePickBtn;
    private ClickListenerdeliveryDialog saveClickListener;
    private ClickListenerdeliveryDialog cancelClickListener;
    private SaleDelivery deliveryVO;
    private String customerName;
    private Calendar now;
    private int day;
    private int month;
    private int year;
    private DatePickerDialog datePickerDialog;
    private String date;
    private boolean isClear = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainLayout = inflater.inflate(R.layout.delivery, null);
        context = getContext();
        loadUI();

        if (deliveryVO == null) {
            deliveryVO = new SaleDelivery();
        }

        MainActivity.setCurrentFragment(this);
        now = Calendar.getInstance();
        buildDatePickerDialog();
        day = now.get(Calendar.DAY_OF_MONTH);
        month = now.get(Calendar.MONTH) + 1;
        year = now.get(Calendar.YEAR);

        deliveryVO.setDay(String.valueOf(day));
        deliveryVO.setMonth(String.valueOf(month));
        deliveryVO.setYear(String.valueOf(year));


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    Log.w("LIStener", "lister" + cancelClickListener);
                    if (saveClickListener != null) {
                        Log.w("Not null", "NOT");
                        getDataFromView();
                        saveClickListener.onClick(v, deliveryVO, customerName);
                    }
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cancelClickListener != null) {
                    cancelClickListener.onClick(v, deliveryVO, customerName);
                }
            }
        });

        datePickBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show(getActivity().getFragmentManager(), "deliver Date");
            }
        });


        configDateUI();

        deliveryCustomerNameEditText.setText(customerName);


        if (isClear) {
            clearData();
        }

        return mainLayout;
    }


    public boolean isClear() {
        return isClear;
    }

    public void setClear(boolean clear) {
        isClear = clear;
    }

    public void configDateUI() {
        date = DateUtility.makeDate(Integer.toString(year), Integer.toString(month), Integer.toString(day));

        Log.w("Delivery date", date + " SSS SSS SSS" + Integer.toString(year) + Integer.toString(month) + Integer.toString(day));
        // String dayDes[]= DateUtility.dayDes(date);
        //  String yearMonthDes=DateUtility.monthYearDes(date);

        //  deliveryDateEditText.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

    }


    public void buildDatePickerDialog() {
        datePickerDialog = DatePickerDialog.newInstance(
                DeliveryFragments.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        TypedArray allTrans = context.getTheme().obtainStyledAttributes(new int[]{R.attr.pickerAccentColor});
        datePickerDialog.setAccentColor(allTrans.getColor(0, 0));
    }


    public ClickListenerdeliveryDialog getSaveClickListener() {
        return saveClickListener;
    }

    public void setSaveClickListener(ClickListenerdeliveryDialog saveClickListener) {
        this.saveClickListener = saveClickListener;
    }

    public ClickListenerdeliveryDialog getCancelClickListener() {
        return cancelClickListener;
    }

    public void setCancelClickListener(ClickListenerdeliveryDialog cancelClickListener) {
        this.cancelClickListener = cancelClickListener;
    }

    private void loadUI() {
        deliveryAgentEditText = (EditText) mainLayout.findViewById(R.id.agent_in_delivery);
        deliveryDateEditText = (EditText) mainLayout.findViewById(R.id.date_in_delivery);
        deliveryChargesEditText = (EditText) mainLayout.findViewById(R.id.charges_in_delivery);
        deliveryCustomerNameEditText = (EditText) mainLayout.findViewById(R.id.customer_name_in_delivery);
        deliveryPhoneNumberEditText = (EditText) mainLayout.findViewById(R.id.customer_phone_in_delivery);
        deliveryAddressEditText = (EditText) mainLayout.findViewById(R.id.address_in_delivery);
        saveBtn = (Button) mainLayout.findViewById(R.id.save);
        cancelBtn = (Button) mainLayout.findViewById(R.id.cancel);

        datePickBtn = (ImageButton) mainLayout.findViewById(R.id.date_pick_btn);

        deliveryAgentTextInputLayout = (TextInputLayout) mainLayout.findViewById(R.id.agent_in_delivery_TIL);
        customerNameTextInputLayout = (TextInputLayout) mainLayout.findViewById(R.id.customer_name_in_delivery_TIL);
        deliveryChargesTextInputLayout = (TextInputLayout) mainLayout.findViewById(R.id.charges_in_delivery_TIL);
        deliveryAddressTextInputLayout = (TextInputLayout) mainLayout.findViewById(R.id.address_in_delivery_TIL);

    }

    public void clearData() {
        Log.w("herer", "clear ing");
        deliveryAgentEditText.setText(null);
        deliveryChargesEditText.setText(null);
        deliveryCustomerNameEditText.setText(null);
        deliveryPhoneNumberEditText.setText(null);
        deliveryAddressEditText.setText(null);
        clearErrorMsg();
    }

    public void shouldClearData(boolean isClear) {
        this.isClear = isClear;
    }

    private void clearErrorMsg() {
        deliveryAgentTextInputLayout.setError(null);
        customerNameTextInputLayout.setError(null);
        deliveryChargesTextInputLayout.setError(null);
        deliveryAddressTextInputLayout.setError(null);
    }

    public void setDataForView(@NonNull SaleDelivery deliveryVO, String customer) {
        this.customerName = customer;
        this.deliveryVO = deliveryVO;
        setData();

    }

    private void setData() {
        configDateUI();
        day = Integer.parseInt(deliveryVO.getDay());
        month = Integer.parseInt(deliveryVO.getMonth()) - 1;
        year = Integer.parseInt(deliveryVO.getYear());
        deliveryAgentEditText.setText(deliveryVO.getAgent());
        deliveryChargesEditText.setText(Double.toString(deliveryVO.getCharges()));
        deliveryCustomerNameEditText.setText(customerName);
        deliveryPhoneNumberEditText.setText(deliveryVO.getPhoneNo());
        deliveryAddressEditText.setText(deliveryVO.getAddress());
        clearErrorMsg();
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    private boolean checkValidation() {
        boolean status = true;
        if (deliveryAgentEditText.getText().toString().trim().length() < 1) {
            String agent = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_Enter_agent}).getString(0);
            deliveryAgentTextInputLayout.setError(agent);
            status = false;
        }
        if (deliveryDateEditText.getText().toString().trim().length() < 1) {

        }
        if (deliveryChargesEditText.getText().toString().trim().length() < 1) {
            String delivery = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_delivery_charges}).getString(0);
            deliveryChargesTextInputLayout.setError(delivery);
            status = false;
        } else {
            deliveryVO.setCharges(Double.parseDouble(deliveryChargesEditText.getText().toString().trim()));
        }

        if (deliveryCustomerNameEditText.getText().toString().trim().length() < 1) {
            String cname = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_customer_name}).getString(0);
            customerNameTextInputLayout.setError(cname);
            status = false;
        }

        if (deliveryAddressEditText.getText().toString().length() < 1) {
            status = false;
            String address = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_address_for_delivery}).getString(0);
            deliveryAddressTextInputLayout.setError(address);
        }


        return status;
    }

    private void getDataFromView() {
        deliveryVO.setAgent(deliveryAgentEditText.getText().toString().trim());
        deliveryVO.setDate(DateUtility.makeDate(Integer.toString(year), Integer.toString(month), Integer.toString(day)));
        deliveryVO.setAddress(deliveryAddressEditText.getText().toString().trim());
        customerName = deliveryCustomerNameEditText.getText().toString().trim();
        deliveryVO.setDay(Integer.toString(day));
        deliveryVO.setMonth(Integer.toString(month));
        deliveryVO.setYear(Integer.toString(year));

        deliveryVO.setCharges(Double.parseDouble(deliveryChargesEditText.getText().toString().trim()));
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        day = dayOfMonth;

        month = monthOfYear + 1;
        this.year = year;

        deliveryVO.setDay(String.valueOf(day));
        deliveryVO.setMonth(String.valueOf(month));
        deliveryVO.setYear(String.valueOf(year));

        configDateUI();

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isClear = false;
        Log.w("on destory", "destory view");
    }
}
