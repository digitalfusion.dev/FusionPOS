package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;

import java.util.List;

/**
 * Created by MD001 on 11/3/16.
 */

public class RVAdapterForUserRoleMD extends RecyclerView.Adapter<RVAdapterForUserRoleMD.UserRoleViewHolder> {

    private List<String> userRoleList;
    private OnItemClickListener mItemClickListener;


    public RVAdapterForUserRoleMD(List<String> userRoleList) {
        this.userRoleList = userRoleList;
    }

    @Override
    public UserRoleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_role_item_view_md, parent, false);

        return new UserRoleViewHolder(v);

    }

    @Override
    public void onBindViewHolder(UserRoleViewHolder holder, final int position) {

        holder.userRoleTextView.setText(userRoleList.get(position).toString());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(v, position);

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return userRoleList.size();
    }

    public List<String> getUserRoleList() {
        return userRoleList;
    }

    public void setUserRoleList(List<String> userRoleList) {
        this.userRoleList = userRoleList;
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class UserRoleViewHolder extends RecyclerView.ViewHolder {
        TextView userRoleTextView;

        View view;

        public UserRoleViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            userRoleTextView = (TextView) itemView.findViewById(R.id.user_role_tv);
        }

    }
}

