package com.digitalfusion.android.pos.information;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by MD003 on 8/15/17.
 */

public class Subscription implements Serializable {
    private long id;
    private Date createdDate;
    private boolean isDeleted;
    private LicenceInfo licenseInfo;
    private PaymentInfo paymentInfo;
    private Registration registration;
    private AppInfo appInfo;
    private DeviceInfo deviceInfo;

    public Subscription() {
        //this.createdDate = new Date();
    }

    public Subscription(boolean isDeleted, LicenceInfo licenseInfo, PaymentInfo paymentInfo, Registration registration, AppInfo appInfo, DeviceInfo deviceInfo) {
        this.isDeleted = isDeleted;
        this.licenseInfo = licenseInfo;
        this.paymentInfo = paymentInfo;
        this.registration = registration;
        this.appInfo = appInfo;
        this.deviceInfo = deviceInfo;
        this.createdDate = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public LicenceInfo getLicenseInfo() {
        return licenseInfo;
    }

    public void setLicenseInfo(LicenceInfo licenseInfo) {
        this.licenseInfo = licenseInfo;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(Registration registration) {
        this.registration = registration;
    }

    public AppInfo getAppInfo() {
        return appInfo;
    }

    public void setAppInfo(AppInfo appInfo) {
        this.appInfo = appInfo;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "id=" + id +
                ", createdDate=" + createdDate +
                ", isDeleted=" + isDeleted +
                ", licenseInfo=" + licenseInfo +
                ", paymentInfo=" + paymentInfo +
                ", registration=" + registration +
                ", appInfo = " + appInfo +
                ", deviceInfo = " + deviceInfo +
                '}';
    }
}
