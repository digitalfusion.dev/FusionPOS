package com.digitalfusion.android.pos.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.ExpenseAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.IncomeSearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForExpenseManagerList;
import com.digitalfusion.android.pos.database.business.ExpenseManager;
import com.digitalfusion.android.pos.database.model.ExpenseName;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.ExpenseIncome;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.FusionToast;
import com.example.searchview.MaterialSearchView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 11/16/16.
 */

public class IncomeManagerListFragment extends Fragment {

    private View mainLayout;

    private FloatingActionButton addNewIncomeFab;

    private RecyclerView incomeListRecyclerView;

    private ExpenseManager expenseManagerBusiness;    ///////////

    private Context context;

    private List<ExpenseIncome> incomeViewList;    //////////

    private RVSwipeAdapterForExpenseManagerList rvSwipeAdapterForIncomeManagerList;        //////////

    private MaterialDialog addNewIncomeManagerMaterialDialog;

    //UI component for addNewExpenseManagerDialog


    private TextView addDateIncomeTextView;

    private AutoCompleteTextView incomeManagerNameTextInputEditTextMd;

    private EditText incomeManagerAmountTextInputEditTextMd;

    private EditText incomeManagerRemarkTextInputEditTextMd;

    private Button saveIncomeBtnMd, cancelIncomeBtnMd;

    private MaterialDialog viewRemarkExpenseManagerMaterialDialog;


    //UI component for ViewRemarkDialog
    private TextView viewRemark;

    private Button okBtnViewRemark;

    private MaterialSearchView searchView;

    //Values
    private String incomeOrExpenseName;

    private String expenseManagerType;

    private Double expenseOrIncomeAmount;

    private String expenseOrIncomeRemark;

    private DatePickerDialog datePickerDialog;

    private int day;

    private int month;

    private int year;

    private String date;

    private Calendar now;

    private DatePickerDialog editDatePickerDialog;

    private int editDay;

    private int editMonth;

    private int editYear;

    private String editDate;

    private Calendar editCalendar;

    private int editPosition;

    private int viewRemarkPosition;

    private ArrayList<ExpenseName> expenseNameList;

    private ArrayList<ExpenseName> incomNameVOList;

    private ExpenseAutoCompleteAdapter expenseAutoCompleteAdapter;

    private ExpenseAutoCompleteAdapter incomeAutoCompleteAdapter;

    private IncomeSearchAdapter incomeSearchAdapter;

    private MaterialDialog filterDialog;

    private TextView filterTextView;

    private RVAdapterForFilter rvAdapterForFilter;

    private List<String> filterList;

    private String startDate;

    private String endDate;

    private DatePickerDialog customeDatePickerDialog;

    private Calendar calendar;

    private boolean editflag;


    private boolean darkmode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.income_manager, null);

        context = getContext();

        darkmode = POSUtil.isNightMode(context);

        calendar = Calendar.getInstance();

        startDate = "000000000000";

        endDate = "9999999999999999";
        MainActivity.setCurrentFragment(this);

        TypedArray allTrans = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all});

        TypedArray thisMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month});

        TypedArray lastMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month});

        TypedArray thisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year});

        TypedArray lastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year});

        TypedArray customRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_range});

        TypedArray customDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_date});

        filterList = new ArrayList<>();

        filterList.add(allTrans.getString(0));

        filterList.add(thisMonth.getString(0));

        filterList.add(lastMonth.getString(0));

        filterList.add(thisYear.getString(0));

        filterList.add(lastYear.getString(0));

        filterList.add(customRange.getString(0));

        filterList.add(customDate.getString(0));

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();

        buildViewRemarkExpenseManagerDialog();

        buildAddNewIncomeManagerDialog();

        loadUI();

        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                filterDialog.show();

            }
        });
        configCustomDatePickerDialog();

        okBtnViewRemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewRemarkExpenseManagerMaterialDialog.dismiss();
            }
        });

        return mainLayout;

    }

    private void buildDateFilterDialog() {

        TypedArray filterByDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date});

        filterDialog = new MaterialDialog.Builder(context).

                title(filterByDate.getString(0))

                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))

                .build();
    }

    private void configCustomDatePickerDialog() {
        customeDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


                        startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                        String date = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        String dayDes[] = DateUtility.dayDes(date);

                        String yearMonthDes = DateUtility.monthYearDes(date);

                        filterTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

                        loadExpenseHistory(0, 100);

                        rvSwipeAdapterForIncomeManagerList.setExpenseManagerVOList(incomeViewList);

                        rvSwipeAdapterForIncomeManagerList.notifyDataSetChanged();
                    }
                },
                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)
        );

        if (darkmode)
            customeDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        customeDatePickerDialog.setThemeDark(darkmode);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        MainActivity activity = (MainActivity) getActivity();

        expenseManagerBusiness = new ExpenseManager(context);    //////////

        now = Calendar.getInstance();

        editCalendar = Calendar.getInstance();

        day = now.get(Calendar.DAY_OF_MONTH);

        month = now.get(Calendar.MONTH) + 1;

        year = now.get(Calendar.YEAR);

        expenseNameList = new ArrayList<>();

        expenseNameList = (ArrayList<ExpenseName>) expenseManagerBusiness.getAllExpenseNames();

        incomNameVOList = (ArrayList<ExpenseName>) expenseManagerBusiness.getAllIncomeNames();

        incomeSearchAdapter = new IncomeSearchAdapter(context, expenseManagerBusiness);

        buildDatePickerDialog();

        configRecyclerView();

        setttingOnClickListener();

        setFilterTextView(filterList.get(0));

        searchView.setAdapter(incomeSearchAdapter);

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                incomeViewList = new ArrayList<>();

                incomeViewList.add(incomeSearchAdapter.getSuggestion().get(position));

                rvSwipeAdapterForIncomeManagerList.setExpenseManagerVOList(incomeViewList);

                rvSwipeAdapterForIncomeManagerList.notifyDataSetChanged();

                searchView.closeSearch();

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                loadExpenseHistory(0, 100, query.toString());

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        // expenseAutoCompleteAdapter=new ExpenseAutoCompleteAdapter(context, R.layout.expense_auto_complete_suggest_view, R.id.expense_name,expenseNameList);

        //   incomeAutoCompleteAdapter=new ExpenseAutoCompleteAdapter(context, R.layout.expense_auto_complete_suggest_view, R.id.expense_name,incomNameVOList);


        incomeManagerNameTextInputEditTextMd.setThreshold(1);


        incomeManagerNameTextInputEditTextMd.setAdapter(incomeAutoCompleteAdapter);

        incomeManagerNameTextInputEditTextMd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    incomeManagerAmountTextInputEditTextMd.requestFocus();
                }
                return false;
            }
        });

        incomeManagerNameTextInputEditTextMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //   incomeManagerNameTextInputEditTextMd.setText(incomeAutoCompleteAdapter.getExpenseName().getName());
            }
        });

        rvSwipeAdapterForIncomeManagerList.setViewDetailClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                Bundle bundle = new Bundle();

                bundle.putSerializable("expense", incomeViewList.get(postion));

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.INCOME_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);

            }
        });


        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (position == 0) {

                    setFilterTextView(filterList.get(position));

                    startDate = "000000000000";

                    endDate = "9999999999999999";

                    loadExpenseHistory(0, 100);

                    rvSwipeAdapterForIncomeManagerList.setExpenseManagerVOList(incomeViewList);

                    rvSwipeAdapterForIncomeManagerList.notifyDataSetChanged();

                } else if (position == 1) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisMonthStartDate();

                    endDate = DateUtility.getThisMonthEndDate();

                    loadExpenseHistory(0, 100);

                    rvSwipeAdapterForIncomeManagerList.setExpenseManagerVOList(incomeViewList);

                    rvSwipeAdapterForIncomeManagerList.notifyDataSetChanged();

                } else if (position == 2) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    loadExpenseHistory(0, 100);

                    rvSwipeAdapterForIncomeManagerList.setExpenseManagerVOList(incomeViewList);

                    rvSwipeAdapterForIncomeManagerList.notifyDataSetChanged();

                } else if (position == 3) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    loadExpenseHistory(0, 100);

                    rvSwipeAdapterForIncomeManagerList.setExpenseManagerVOList(incomeViewList);

                    rvSwipeAdapterForIncomeManagerList.notifyDataSetChanged();

                } else if (position == 4) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    loadExpenseHistory(0, 100);

                    rvSwipeAdapterForIncomeManagerList.setExpenseManagerVOList(incomeViewList);

                    rvSwipeAdapterForIncomeManagerList.notifyDataSetChanged();

                } else if (position == 5) {

                    setFilterTextView(filterList.get(position));

                } else if (position == 6) {

                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "customeDate");

                }

                filterDialog.dismiss();
            }
        });

    }

    private void setttingOnClickListener() {

     /*   expenseManagerTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(income.getId()==checkedId){

                    isIncome=true;

                }else {

                    isIncome=false;

                }

            }
        });
*/


        addDateIncomeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog.show(getActivity().getFragmentManager(), "show");

            }

        });


        addNewIncomeFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resetDialogDataIncome();
                String newIncome = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_new_income}).getString(0);

                addNewIncomeManagerMaterialDialog.setTitle(newIncome);

                addNewIncomeManagerMaterialDialog.show();

            }
        });

        rvSwipeAdapterForIncomeManagerList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                editPosition = postion;
                editflag = true;

                String editIncome = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_income}).getString(0);
                addNewIncomeManagerMaterialDialog.setTitle(editIncome);

                if (incomeViewList.get(editPosition).isIncome()) {
                    setDataEditIncomeDialog(incomeViewList.get(editPosition));

                    addNewIncomeManagerMaterialDialog.show();
                }

            }
        });

        rvSwipeAdapterForIncomeManagerList.setViewRemarkClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {


                viewRemarkPosition = postion;

                setDataRemarkDialog();

                viewRemarkExpenseManagerMaterialDialog.show();

            }
        });
        addNewIncomeManagerMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                editflag = false;
            }
        });


        saveIncomeBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editflag) {
                    if (checkValidationIncome()) {

                        expenseManagerType = ExpenseIncome.Type.Income.toString();

                        getIncomeValuesFromView();

                        InsertedBooleanHolder status = new InsertedBooleanHolder();

                        Log.w("income ", "position " + editPosition);

                        status.setInserted(expenseManagerBusiness.updateExpenseOrIncome(editDate, expenseOrIncomeAmount, incomeOrExpenseName,
                                expenseManagerType, expenseOrIncomeRemark, Integer.toString(editDay), Integer.toString(editMonth),
                                Integer.toString(editYear), incomeViewList.get(editPosition).getId()));  ////////

                        if (status.isInserted()) {

                            String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.income_edited_successfully}).getString(0);

                            addNewIncomeManagerMaterialDialog.dismiss();
                            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);

                            refreshExpenseManagerList();

                        }
                    }


                } else {
                    if (checkValidationIncome()) {

                        expenseManagerType = ExpenseIncome.Type.Income.toString();

                        getIncomeValuesFromView();

                        InsertedBooleanHolder status = new InsertedBooleanHolder();


                        status.setInserted(expenseManagerBusiness.addNewIncomeOrExpense(incomeOrExpenseName, expenseManagerType, expenseOrIncomeAmount,
                                expenseOrIncomeRemark, Integer.toString(day), Integer.toString(month), Integer.toString(year), date));  ////////

                        if (status.isInserted()) {

                            String success = context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_income_added_successfully}).getString(0);

                            refreshExpenseManagerList();

                            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);


                            addNewIncomeManagerMaterialDialog.dismiss();

                        }


                    }
                }


            }
        });


    }


    private void setFilterTextView(String msg) {

        filterTextView.setText(msg);

    }

    private void resetDialogDataIncome() {

        day = now.get(Calendar.DAY_OF_MONTH);

        month = now.get(Calendar.MONTH) + 1;

        year = now.get(Calendar.YEAR);

        buildDatePickerDialog();

        configDateUI();

        incomeManagerNameTextInputEditTextMd.setText(null);

        incomeManagerNameTextInputEditTextMd.setError(null);

        incomeManagerAmountTextInputEditTextMd.setText(null);

        incomeManagerAmountTextInputEditTextMd.setError(null);

        incomeManagerRemarkTextInputEditTextMd.setText(null);

        incomeManagerRemarkTextInputEditTextMd.setError(null);

    }

    public void buildDatePickerDialog() {

        datePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        day = dayOfMonth;

                        month = monthOfYear + 1;

                        year = year;

                        configDateUI();

                    }
                },

                now.get(Calendar.YEAR),

                now.get(Calendar.MONTH),

                now.get(Calendar.DAY_OF_MONTH)
        );

        if (darkmode)
            datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        datePickerDialog.setThemeDark(darkmode);
    }

    public void buildEditDatePickerDialog() {

        editDatePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        editDay = dayOfMonth;

                        editMonth = monthOfYear + 1;

                        editYear = year;

                        configEditDateUI();

                    }
                },

                editCalendar.get(Calendar.YEAR),

                editCalendar.get(Calendar.MONTH),

                editCalendar.get(Calendar.DAY_OF_MONTH)

        );

        if (darkmode)
            editDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        editDatePickerDialog.setThemeDark(darkmode);
    }


    public void buildAddNewIncomeManagerDialog() {

        //TypedArray save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        String newIncome = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_new_income}).getString(0);

        addNewIncomeManagerMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.new_expense, true)
                .title(newIncome)
                //.positiveText(save.getString(0))
                //.negativeText(cancel.getString(0))
                .build();

    }


    public void buildViewRemarkExpenseManagerDialog() {

        TypedArray remark = context.getTheme().obtainStyledAttributes(new int[]{R.attr.remark});
        //TypedArray ok = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ok});

        viewRemarkExpenseManagerMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.view_remark, true)
                .title(remark.getString(0))
                //.negativeText(ok.getString(0))
                .build();

    }

    private void loadExpenseHistory(int startLimit, int endLimit, String query) {

        incomeViewList = expenseManagerBusiness.getAllIncomeNameSearch(query, startLimit, endLimit);

    }

    private void loadExpenseHistory(int startLimit, int endLimit) {

        incomeViewList = expenseManagerBusiness.getAllIncome(0, 1000, startDate, endDate);   ///////////

    }


    public boolean checkValidationIncome() {

        boolean status = true;

        Log.w("check", "income");

        if (incomeManagerNameTextInputEditTextMd.getText().toString().trim().length() < 1) {

            String name = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_name}).getString(0);
            incomeManagerNameTextInputEditTextMd.setError(name);
            status = false;

        }

        if (incomeManagerAmountTextInputEditTextMd.getText().toString().trim().length() < 1) {

            String amt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_amount}).getString(0);
            incomeManagerAmountTextInputEditTextMd.setError(amt);

            status = false;

        }

        Log.w("check", status + " hello ");

        return status;

    }


    public void setDataEditIncomeDialog(ExpenseIncome expenseIncome) {

        incomeManagerNameTextInputEditTextMd.setText(expenseIncome.getName());

        editYear = Integer.parseInt(expenseIncome.getYear());

        editMonth = Integer.parseInt(expenseIncome.getMonth());

        editDay = Integer.parseInt(expenseIncome.getDay());

        editCalendar.set(editYear, editMonth - 1, editDay);

        configEditDateUI();


        incomeManagerAmountTextInputEditTextMd.setText(incomeViewList.get(editPosition).getAmount().toString());/////////

        incomeManagerRemarkTextInputEditTextMd.setText(incomeViewList.get(editPosition).getRemark());////////

        buildEditDatePickerDialog();

    }

    public void setDataRemarkDialog() {

        viewRemark.setText(incomeViewList.get(viewRemarkPosition).getRemark());

    }


    public void getIncomeValuesFromView() {

        incomeOrExpenseName = incomeManagerNameTextInputEditTextMd.getText().toString().trim();

        expenseOrIncomeAmount = Double.parseDouble(incomeManagerAmountTextInputEditTextMd.getText().toString().trim());

        expenseOrIncomeRemark = incomeManagerRemarkTextInputEditTextMd.getText().toString().trim();

    }


    public void refreshExpenseManagerList() {

        incomeViewList = expenseManagerBusiness.getAllIncome(0, 1000, "20150000", "99999999"); //////////

        rvSwipeAdapterForIncomeManagerList.setExpenseManagerVOList(incomeViewList);

        rvSwipeAdapterForIncomeManagerList.notifyDataSetChanged();

    }

    public void configRecyclerView() {

        loadExpenseHistory(0, 100);

        rvSwipeAdapterForIncomeManagerList = new RVSwipeAdapterForExpenseManagerList(incomeViewList);

        incomeListRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        incomeListRecyclerView.setAdapter(rvSwipeAdapterForIncomeManagerList);

    }


    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});

        int attributeResourceId = a.getResourceId(0, 0);

        searchView.setCursorDrawable(attributeResourceId);

    }

    public void loadUI() {

        loadUIFromToolbar();

        filterTextView = (TextView) mainLayout.findViewById(R.id.filter_one);

        incomeListRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.expenseManager_list_rv);

        addNewIncomeFab = (FloatingActionButton) mainLayout.findViewById(R.id.income);


        addDateIncomeTextView = (TextView) addNewIncomeManagerMaterialDialog.findViewById(R.id.expense_date_in_add_expense_TV);


        saveIncomeBtnMd = (Button) addNewIncomeManagerMaterialDialog.findViewById(R.id.save);

        cancelIncomeBtnMd = (Button) addNewIncomeManagerMaterialDialog.findViewById(R.id.cancel);

        incomeManagerNameTextInputEditTextMd = (AutoCompleteTextView) addNewIncomeManagerMaterialDialog.findViewById(R.id.expense_name_in_add_expense_TIET);

        incomeManagerAmountTextInputEditTextMd = (EditText) addNewIncomeManagerMaterialDialog.findViewById(R.id.expense_amount_in_add_expense_TIET);

        incomeManagerRemarkTextInputEditTextMd = (EditText) addNewIncomeManagerMaterialDialog.findViewById(R.id.expense_remark_in_add_expense_TIET);

        viewRemark = (TextView) viewRemarkExpenseManagerMaterialDialog.findViewById(R.id.viewRemark);

        okBtnViewRemark = (Button) viewRemarkExpenseManagerMaterialDialog.findViewById(R.id.ok);

    }


    private void configDateUI() {

        date = DateUtility.makeDate(Integer.toString(year), Integer.toString(month), Integer.toString(day));

        String dayDes[] = DateUtility.dayDes(date);

        String yearMonthDes = DateUtility.monthYearDes(date);

        addDateIncomeTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

    }

    private void configEditDateUI() {

        editDate = DateUtility.makeDate(Integer.toString(editYear), Integer.toString(editMonth), Integer.toString(editDay));

        String dayDes[] = DateUtility.dayDes(editDate);

        String yearMonthDes = DateUtility.monthYearDes(editDate);

        addDateIncomeTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

    }

}
