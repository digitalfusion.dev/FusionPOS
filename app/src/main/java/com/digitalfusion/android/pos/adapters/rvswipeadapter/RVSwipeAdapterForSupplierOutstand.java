package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Supplier;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD003 on 11/4/16.
 */

public class RVSwipeAdapterForSupplierOutstand extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {


    private List<Supplier> supplierList;    ///////

    private ClickListener viewPaymentClickListener;

    private ClickListener viewDetailClickListener;


    public RVSwipeAdapterForSupplierOutstand(List<Supplier> supplierList) {     //////////

        this.supplierList = supplierList;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.supplier_outstand_item_view, parent, false);

        return new RVSwipeAdapterForSupplierOutstand.SupplierViewHolder(v);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof RVSwipeAdapterForSupplierOutstand.SupplierViewHolder) {

            final RVSwipeAdapterForSupplierOutstand.SupplierViewHolder supplierViewHolder = (RVSwipeAdapterForSupplierOutstand.SupplierViewHolder) holder;

            POSUtil.makeZebraStrip(supplierViewHolder.itemView, position);

            supplierViewHolder.supplierNameTextView.setText(supplierList.get(position).getName());

            if (supplierList.get(position).getBalance().toString().length() > 1) {

                supplierViewHolder.supplierBalanceTextView.setText(POSUtil.NumberFormat(supplierList.get(position).getBalance()));

            } else {

                supplierViewHolder.supplierBalanceTextView.setText("0.0");

            }

            if (supplierList.get(position).getPhoneNo().length() > 1) {

                supplierViewHolder.supplierPhoneTextView.setText(supplierList.get(position).getPhoneNo());

            } else {

                supplierViewHolder.supplierPhoneTextView.setText(null);

                supplierViewHolder.supplierPhoneTextView.setHint("No phone");

            }

            supplierViewHolder.businessNameTextView.setText(supplierList.get(position).getBusinessName());

            supplierViewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewDetailClickListener != null) {
                        supplierViewHolder.swipeLayout.close();
                        viewDetailClickListener.onClick(position);
                    }
                }
            });

            supplierViewHolder.viewPaymentBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (viewDetailClickListener != null) {
                        supplierViewHolder.swipeLayout.close();
                        viewDetailClickListener.onClick(position);

                    }
                }
            });

            mItemManger.bindView(supplierViewHolder.view, position);

        }

    }

    public ClickListener getViewDetailClickListener() {
        return viewPaymentClickListener;
    }

    public void setViewDetailClickListener(ClickListener viewDetailClickListener) {
        this.viewDetailClickListener = viewDetailClickListener;
    }

    @Override
    public int getItemCount() {
        return supplierList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public List<Supplier> getSupplierList() {
        return supplierList;
    }    ///////////////

    public void setSupplierList(List<Supplier> supplierList) {  ////////

        this.supplierList = supplierList;

    }

    public ClickListener getViewPaymentClickListener() {
        return viewPaymentClickListener;
    }

    public void setViewPaymentClickListener(ClickListener viewPaymentClickListener) {
        this.viewPaymentClickListener = viewPaymentClickListener;
    }

    public class SupplierViewHolder extends RecyclerView.ViewHolder {

        TextView supplierNameTextView;

        TextView supplierPhoneTextView;

        TextView supplierBalanceTextView;

        ImageButton viewPaymentBtn;

        LinearLayout linearLayout;

        TextView businessNameTextView;

        SwipeLayout swipeLayout;

        View view;

        public SupplierViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);

            businessNameTextView = (TextView) itemView.findViewById(R.id.business_name);

            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll);

            supplierNameTextView = (TextView) itemView.findViewById(R.id.suppl_name_in_supplier_item_view);

            supplierPhoneTextView = (TextView) itemView.findViewById(R.id.suppl_phone_in_supplier_item_view);

            supplierBalanceTextView = (TextView) itemView.findViewById(R.id.suppl_balance_in_supplier_item_view);

            viewPaymentBtn = (ImageButton) itemView.findViewById(R.id.edit_supplier);

        }

    }
}