package com.digitalfusion.android.pos.fragments.dashboardfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;

import java.util.ArrayList;

/**
 * Created by MD003 on 9/15/17.
 */

public class DashBoardWithFilter extends Fragment {

    private ArrayList<String> typeList;

    private RVAdapterForFilter rvAdapterForFilter;

    private TextView typeFilter;

    private Context context;

    private MaterialDialog typeMaterialDialog;

    private View mainLayoutView;
    private DashboardFragmentV1 dashboardFragmentForDay;
    private DashboardFragmentV1 dashboardFragmentForWeek;
    private DashboardFragmentV1 dashboardFragmentForMonth;
    private DashboardFragmentV1 dashboardFragmentForYear;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.fragment_dashboard_filter, container, false);
        String dashboard = getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.dashboard}).getString(0);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(dashboard);

        typeFilter = (TextView) mainLayoutView.findViewById(R.id.type);

        typeList = new ArrayList<>();


        String day = getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.day_view}).getString(0);

        String month = getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.month_view}).getString(0);
        String week  = getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.week_view}).getString(0);

        String year = getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.year_view}).getString(0);

        typeList.add(day);
        typeList.add(week);

        typeList.add(month);

        typeList.add(year);

        dashboardFragmentForDay = new DashboardFragmentV1();

        dashboardFragmentForWeek = new DashboardFragmentV1();

        dashboardFragmentForMonth = new DashboardFragmentV1();

        dashboardFragmentForYear = new DashboardFragmentV1();

        rvAdapterForFilter = new RVAdapterForFilter(typeList);

        typeMaterialDialog = new MaterialDialog.Builder(getContext()).adapter(rvAdapterForFilter, new LinearLayoutManager(getContext())).build();


        typeFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeMaterialDialog.show();
            }
        });

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                typeFilter.setText(typeList.get(position));

                typeMaterialDialog.dismiss();

                Bundle bundle = new Bundle();

                Log.e("position", "tab " + position);
                switch (position) {

                    case 0:
                        Log.e("tab pager adapter", "day");
                        bundle.putString("title", "day");
                        dashboardFragmentForDay = new DashboardFragmentV1();
                        dashboardFragmentForDay.setArguments(bundle);
                        replacingFragment(dashboardFragmentForDay);
                        break;
                    // MainActivity.replacingTabFragmentWithoutSearchView(dashboardFragmentForDay);

                    case 1:
                        Log.e("tab pager adapter", "week");
                        bundle.putString("title", "week");
                        dashboardFragmentForWeek = new DashboardFragmentV1();
                        dashboardFragmentForWeek.setArguments(bundle);
                        replacingFragment(dashboardFragmentForWeek);
                        break;
                    // MainActivity.replacingTabFragmentWithoutSearchView(dashboardFragmentForDay);

                    case 2:
                        dashboardFragmentForMonth = new DashboardFragmentV1();
                        Log.e("tab pager adapter", "month");
                        bundle.putString("title", "month");
                        dashboardFragmentForMonth.setArguments(bundle);
                        replacingFragment(dashboardFragmentForMonth);
                        break;
                    //  MainActivity.replacingTabFragmentWithoutSearchView(dashboardFragmentForDay);

                    case 3:
                        Log.e("tab pager adapter", "year");
                        bundle.putString("title", "year");
                        dashboardFragmentForYear = new DashboardFragmentV1();
                        dashboardFragmentForYear.setArguments(bundle);
                        replacingFragment(dashboardFragmentForYear);
                        break;
                    //MainActivity.replacingTabFragmentWithoutSearchView(dashboardFragmentForDay);

                }
            }
        });


        rvAdapterForFilter.setCurrentPos(0);

        return mainLayoutView;
    }

    @Override
    public void onResume() {
        super.onResume();

        rvAdapterForFilter.setCurrentPos(0);
    }

    public void replacingFragment(Fragment fragment) {

        final FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.replace(R.id.frame_replace_dash, fragment);

        //fragmentTransaction.addToBackStack("back");

        fragmentTransaction.commit();

    }

}
