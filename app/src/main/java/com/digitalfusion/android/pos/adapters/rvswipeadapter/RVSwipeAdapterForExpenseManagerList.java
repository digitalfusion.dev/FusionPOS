package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.ExpenseIncome;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.Calendar;
import java.util.List;

/**
 * Created by MD001 on 9/5/16.
 */
public class RVSwipeAdapterForExpenseManagerList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    public int currentDate;
    protected boolean showLoader = false;
    private List<ExpenseIncome> expenseManagerVOList;     /////////
    private ClickListener editClickListener;
    private ClickListener deleteClickListener;
    private ClickListener viewRemarkClickListener;
    private ClickListener viewDetailClickListener;
    private LoaderViewHolder loaderViewHolder;

    public RVSwipeAdapterForExpenseManagerList(List<ExpenseIncome> expenseManagerVOList) {   /////////

        Calendar now = Calendar.getInstance();

        this.expenseManagerVOList = expenseManagerVOList;

        currentDate = Integer.parseInt(DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(now.get(Calendar.DAY_OF_MONTH))));

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEWTYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.expense_manager_item_view, parent, false);

            return new ExpenseManagerViewHolder(v);

        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            return new LoaderViewHolder(v);
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ExpenseManagerViewHolder) {

            final ExpenseManagerViewHolder expenseManagerViewHolder = (ExpenseManagerViewHolder) holder;

            POSUtil.makeZebraStrip(expenseManagerViewHolder.itemView, position);

            if (expenseManagerVOList.get(position).getName().equalsIgnoreCase("Refund")) {
                expenseManagerViewHolder.editButton.setVisibility(View.GONE);
                expenseManagerViewHolder.deleteButton.setVisibility(View.GONE);
            } else {
                expenseManagerViewHolder.editButton.setVisibility(View.VISIBLE);
                expenseManagerViewHolder.deleteButton.setVisibility(View.VISIBLE);
            }

            // expenseManagerViewHolder.expenseManagerDateTextView.setText(expenseManagerVOList.get(position).get());
            //  expenseManagerViewHolder.expenseManagerNameTextView.setText(expenseManagerVOList.get(position).getName());

            //  String dayDes[]= DateUtility.dayDes(expenseManagerVOList.get(position).getDate());

            // String yearMonthDes=DateUtility.monthYearDes(expenseManagerVOList.get(position).getDate());

            // expenseManagerViewHolder.expenseManagerDateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));


            int date = Integer.parseInt(expenseManagerVOList.get(position).getDate());

            if (date == currentDate) {
                expenseManagerViewHolder.expenseManagerDateTextView.setText(expenseManagerViewHolder.today);
            } else if (date == currentDate - 1) {
                expenseManagerViewHolder.expenseManagerDateTextView.setText(expenseManagerViewHolder.yesterday);
            } else {


                expenseManagerViewHolder.expenseManagerDateTextView.setText(DateUtility.makeDateFormatWithSlash(expenseManagerVOList.get(position).getYear(),
                        expenseManagerVOList.get(position).getMonth(),
                        expenseManagerVOList.get(position).getDay()));

                //viewHolder.dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));
            }

            expenseManagerViewHolder.expenseManagerNameTextView.setText(expenseManagerVOList.get(position).getName());

            expenseManagerViewHolder.expenseManagerAmountTextView.setText(POSUtil.NumberFormat(expenseManagerVOList.get(position).getAmount()));

            if (expenseManagerVOList.get(position).isEmptyRemark()) {

                expenseManagerViewHolder.expenseManagerRemarkTextView.setText(null);

            } else {
                expenseManagerViewHolder.expenseManagerRemarkTextView.setText(expenseManagerVOList.get(position).getRemark());
            }

            expenseManagerViewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!expenseManagerVOList.get(position).getName().equalsIgnoreCase("Refund")) {

                        if (deleteClickListener != null) {
                            expenseManagerViewHolder.swipeLayout.close();
                            deleteClickListener.onClick(position);
                        }
                    }
                }
            });

            expenseManagerViewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editClickListener != null) {
                        expenseManagerViewHolder.swipeLayout.close();
                        editClickListener.onClick(position);

                    }

                }
            });

            expenseManagerViewHolder.detaiButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (viewDetailClickListener != null) {
                        expenseManagerViewHolder.swipeLayout.close();
                        viewDetailClickListener.onClick(position);

                    }

                }
            });

            if (expenseManagerVOList.get(position).getType().toString().equalsIgnoreCase(ExpenseIncome.Type.Expense.toString())) {

                expenseManagerViewHolder.typeTextView.setBackgroundColor(Color.parseColor("#DD2C00"));
                expenseManagerViewHolder.typeTextView.setText(expenseManagerViewHolder.expense);


            } else {
                expenseManagerViewHolder.typeTextView.setBackgroundColor(Color.parseColor("#4CAF50"));
                expenseManagerViewHolder.typeTextView.setText(expenseManagerViewHolder.income);

            }

            mItemManger.bindView(expenseManagerViewHolder.view, position);

        } else {

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) holder;

            this.loaderViewHolder = loaderViewHolder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }


    }

    @Override
    public int getItemCount() {
        if (expenseManagerVOList == null || expenseManagerVOList.size() == 0) {
            return 0;
        } else {
            return expenseManagerVOList.size() + 1;
        }

    }


    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            if (expenseManagerVOList != null && expenseManagerVOList.size() != 0) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;

            }

        }

        return VIEWTYPE_ITEM;
    }

    public void setShowLoader(boolean showLoader) {

        this.showLoader = showLoader;

        if (loaderViewHolder != null) {
            Log.w("hele", "loader full not null");
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }

        }
    }


    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public ClickListener getViewDetailClickListener() {
        return viewDetailClickListener;
    }

    public void setViewDetailClickListener(ClickListener viewDetailClickListener) {
        this.viewDetailClickListener = viewDetailClickListener;
    }

    public List<ExpenseIncome> getExpenseManagerVOList() {   /////////
        return expenseManagerVOList;
    }

    public void setExpenseManagerVOList(List<ExpenseIncome> expenseManagerVOList) {    /////////////
        this.expenseManagerVOList = expenseManagerVOList;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public ClickListener getViewRemarkClickListener() {
        return viewRemarkClickListener;
    }

    public void setViewRemarkClickListener(ClickListener viewRemarkClickListener) {
        this.viewRemarkClickListener = viewRemarkClickListener;
    }

    public class ExpenseManagerViewHolder extends RecyclerView.ViewHolder {

        TextView expenseManagerDateTextView;

        TextView expenseManagerNameTextView;

        TextView expenseManagerAmountTextView;


        TextView expenseManagerRemarkTextView;

        ImageButton editButton;

        ImageButton detaiButton;

        LinearLayout linearLayout;

        TextView typeTextView;

        ImageButton deleteButton;

        SwipeLayout swipeLayout;

        View view;

        String today;
        String yesterday;
        String income;
        String expense;

        public ExpenseManagerViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll);

            deleteButton = (ImageButton) itemView.findViewById(R.id.delete);

            expenseManagerDateTextView = (TextView) itemView.findViewById(R.id.exmanager_date_in_exmanager_item_view);

            expenseManagerNameTextView = (TextView) itemView.findViewById(R.id.exmanager_name_in_exmanager_item_view);

            expenseManagerAmountTextView = (TextView) itemView.findViewById(R.id.exmanager_amount_in_exmanager_item_view);

            expenseManagerRemarkTextView = (TextView) itemView.findViewById(R.id.exmanager_remark_in_exmanager_item_view);

            editButton = (ImageButton) itemView.findViewById(R.id.edit_expenseManager);

            detaiButton = (ImageButton) itemView.findViewById(R.id.view_detail);

            typeTextView = (TextView) itemView.findViewById(R.id.type);

            today = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.today}).getString(0);

            yesterday = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday}).getString(0);

            income = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.income}).getString(0);

            expense = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.expense}).getString(0);

        }

    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar mProgressBar;

        public View mView;

        public LoaderViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);

        }
    }
}

