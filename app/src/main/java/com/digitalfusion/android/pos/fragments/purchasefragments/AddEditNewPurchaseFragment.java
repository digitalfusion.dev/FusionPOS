package com.digitalfusion.android.pos.fragments.purchasefragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockCodeAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockNameAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.StockNameOrCodeAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.SupplierAutoCompleteAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForStockChooser;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForSupplierMD;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForTaxMD;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterforStockListMaterialDialog;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForSaleItemViewInPurchase;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.AuthorizationManager;
import com.digitalfusion.android.pos.database.business.PurchaseManager;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.business.SupplierManager;
import com.digitalfusion.android.pos.database.model.Stock;
import com.digitalfusion.android.pos.database.model.Supplier;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.interfaces.ActionDoneListener;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.Discount;
import com.digitalfusion.android.pos.database.model.PurchaseHeader;
import com.digitalfusion.android.pos.database.model.PurchaseHistory;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.database.model.StockAutoComplete;
import com.digitalfusion.android.pos.database.model.StockImage;
import com.digitalfusion.android.pos.database.model.StockItem;
import com.digitalfusion.android.pos.database.model.Tax;
import com.digitalfusion.android.pos.util.BluetoothUtil;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.views.DiscountDialogFragmentForPurchase;
import com.digitalfusion.android.pos.views.FusionToast;
import com.digitalfusion.android.pos.views.IgnoableAutoCompleteTextView;
import com.example.numberpad.KeyBoardView;
import com.google.zxing.client.android.CaptureActivity;
import com.innovattic.font.FontEditText;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 9/5/16.
 */
public class AddEditNewPurchaseFragment extends Fragment implements DatePickerDialog.OnDateSetListener, ActionDoneListener {

    private final String DEFAULT_SUPPLIER = "Default";
    private final int SCANNER_BAR_CODE = 10;
    //UI components for priceInput
    PopupWindow popupWindow;
    DiscountDialogFragmentForPurchase discountDialogFragment;
    //UI components from main
    private View mainLayout;
    private EditText phoneNoEditText;
    private Button addpurchaseItemBtn;
    private RecyclerView purchaseItemRecyclerView;
    private TextView purchaseIdTextView;
    private TextView dateTextView;
    private LinearLayout dateLinearLayout;
    private TextView supplierTextInputEditText;
    private TextView taxAmtTextView;
    private TextView taxRateTextView;
    private TextView discountTextView;
    private TextView subtotalTextView;
    private TextView totalTextView;
    private ImageButton discountTextViewBtn;
    private AppCompatImageButton taxTextViewBtn;
    private TextView payTextViewBtn;
    private MaterialDialog qtyInputMaterialDialog;
    private MaterialDialog priceInputMaterialDialog;
    //UI components for qtyInput
    private FontEditText qtyInputEditText;
    private IgnoableAutoCompleteTextView valueTextView;
    private FrameLayout dropDownBtn;
    private TextView retailPriceDropDown;
    private TextView wholeSalePriceDropDown;
    private LinearLayout wholeLinearLayout;
    private LinearLayout retailLinearLayout;
    private MaterialDialog addItemMaterialDialog;
    private MaterialDialog editItemMaterialDialog;
    //    private MaterialDialog discountListMaterialDialog;
    //    private RVAdapterForDiscountMD rvAdapterForDiscountMD;
    private MaterialDialog addDiscountMaterialDialog;
    private MaterialDialog payMaterialDialog;
    //UI components from add item MD
    private MaterialDialog supplierListMaterialDialog;
    private AppCompatImageButton stockListBtnAddItemMd;
    private TextView itemCodeTextInputEditTextAddItemMd;
    private TextView itemNameTextInputEditTextAddItemMd;
    private EditText qtyTextInputEditTextAddItemMd;
    //    private AppCompatImageButton minusQtyAppCompatImageButtonAddItemMd;
    //    private AppCompatImageButton plusQtyAppCompatImageButtonAddItemMd;
    //    private EditText priceTextInputEditTextAddItemMd;
    private SwitchCompat focSwitchCompatAddItemMd;
    private EditText discountTextInputEditTextAddItemMd;
    private Button saveMdButtonAddItemMd;
    //UI components from edit item MD
    private Button cancelMdButtonAddItemMd;
    private AppCompatImageButton stockListBtnEditItemMd;
    private TextView itemCodeTextInputEditTextEditItemMd;
    private TextView itemNameTextInputEditTextEditItemMd;
    private EditText qtyTextInputEditTextEditItemMd;
    //    private AppCompatImageButton minusQtyAppCompatImageButtonEditItemMd;
    //    private AppCompatImageButton plusQtyAppCompatImageButtonEditItemMd;
    private EditText priceTextInputEditTextEditItemMd;
    private SwitchCompat focSwitchCompatEditItemMd;
    private EditText discountTextInputEditTextEditItemMd;
    private Button saveMdButtonEditItemMd;
    //UI components from disc MD
    private Button cancelMdButtonEditItemMd;
    private EditText discountTextInputEditTextMd;
    private Button saveDiscountBtn;
    //UI components for Pay MD
    private Button cancelDiscountBtn;
    private EditText totalAmountTextInputEditTextPayMd;
    private EditText paidAmountTextInputEditTextPayMd;
    private EditText purchaseOustandting;
    private Button saveTransaction;
    private Button cancelTransaction;
    // private ImageButton supplierListBtn;
    private TextView supplierPayTextView;
    //Business
    private StockManager stockManager;
    private SettingManager settingManager;
    private PurchaseManager purchaseManager;
    //values
    private Context context;
    private RVSwipeAdapterForSaleItemViewInPurchase rvSwipeAdapterForPurchaseItemViewInPurchase;
    private List<SalesAndPurchaseItem> purchaseItemViewInSaleList;
    private RVAdapterforStockListMaterialDialog rvAdapterforStockListMaterialDialogEdit;
    private RVAdapterforStockListMaterialDialog rvAdapterforStockListMaterialDialog;
    private List<StockItem> stockItemList;
    private MaterialDialog stockListMaterialDialog;
    private MaterialDialog stockListMaterialDialogEdit;
    //Values
    private SalesAndPurchaseItem purchaseItemInPurchase;
    private String purchaseInvoiceNo;
    private int qty = 0;
    private Boolean foc = false;
    private Double totalAmount = 0.0;
    private Double voucherDiscount = 0.0;
    private Double voucherTax = 0.0;
    private Double subtotal = 0.0;
    private Double paidAmount = 0.0;
    private int editposition;
    private Calendar now;
    private int purchaseDay;
    private int purchaseMonth;
    private int purchaseYear;
    private DatePickerDialog datePickerDialog;
    private String date;
    private RVAdapterForTaxMD rvAdapterForTaxMD;

    // private AutoCompleteTextView autoCompleteTextView;
    private Tax tax;
    private List<Tax> taxList;
    //  private StockCodeAutoCompleteAdapter stockCodeAutoCompleteAdapter;
    private MaterialDialog taxListMaterialDialog;
    private StockCodeAutoCompleteAdapter addstockCodeAutoCompleteAdapter;
    private StockNameAutoCompleteAdapter addstockNameAutoCompleteAdapter;
    private StockNameAutoCompleteAdapter editstockNameAutoCompleteAdapter;
    private StockCodeAutoCompleteAdapter editstockCodeAutoCompleteAdapter;
    private ArrayList<StockAutoComplete> stockAutoCompleteList;
    private MaterialDialog alertMaterialDialog;
    private StockCodeAutoCompleteAdapter.Callback addCallback;
    // private StockCodeAutoCompleteAdapter.Callback addItemCallback;
    private StockCodeAutoCompleteAdapter.Callback editCallback;
    private EditText barCodeTIET;
    private ImageButton barCodeBtn;
    private Boolean barCode = true;
    private String supplierBussinessName = DEFAULT_SUPPLIER;
    private SupplierManager supplierManager;
    private SupplierAutoCompleteAdapter supplierAutoCompleteAdapter;
    private ArrayList<Supplier> supplierArrayList;
    private RVAdapterForSupplierMD rvAdapterForSupplierMD;
    private PurchaseHistory purchaseHistory;
    private PurchaseHeader purchaseHeader;
    private boolean isEdit = false;
    private boolean isPurEdit = false;
    private Boolean isHold = false;
    private List<SalesAndPurchaseItem> updateList;
    private List<Long> deleteList;
    private String phoneNo;
    private StockItem stockItem;
    private boolean darkmode;
    private ImageButton img;
    private KeyBoardView keyBoardView;
    private LinearLayout payLinearLayout;
    private LinearLayout addItemLinearLayout;
    private AutoCompleteTextView itemOrCodeAutocomplete;
    private Animation animation1;
    private Animation animation2;
    private boolean isPay;
    private BluetoothUtil bluetoothUtil;
    private KProgressHUD connectHud;
    private TextView statusTextView;
    private TextView printDeviceNameTextView;
    private FrameLayout saleDetailPrint;
    private StockNameOrCodeAutoCompleteAdapter stockNameOrCodeAutoCompleteAdapter;
    private List<Discount> discountList;
    private Discount discount;
    private StockItem stockAutoCompleteView;
    private String discountPercent;
    private ImageButton barcodeBtn;
    private ImageButton addNewStock;
    private boolean isNewStockAdd = false;
    private MaterialDialog addNewStockMaterialDialog;
    private EditText itemName;
    private EditText codeNo;
    private EditText retailPrice;
    private EditText purchasePrice;
    private MaterialDialog supplierMaterialDialog;
    private AutoCompleteTextView supplierNameTextView;
    private EditText supplierPhoneNoEditText;
    private AppCompatImageButton supplierImageButton;
    private Button supplierSaveButton;
    private Button supplierCancelButton;
    private RVAdapterForStockChooser stockChooserAdapter;
    private MaterialDialog stockChooserDialog;
    private ImageButton holdBtn;
    private User currentUser;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.new_purchase, null);

        stockAutoCompleteList = new ArrayList<>();
        stockChooserAdapter = new RVAdapterForStockChooser();
        updateList = new ArrayList<>();

        deleteList = new ArrayList<>();

        purchaseItemInPurchase = new SalesAndPurchaseItem();

        stockManager = new StockManager(context);

        settingManager = new SettingManager(context);

        purchaseManager = new PurchaseManager(context);

        stockItemList = stockManager.getAllActiveStocks(0, 100);

        purchaseInvoiceNo = purchaseManager.getPurchaseInvoiceNo();

        supplierManager = new SupplierManager(context);

        supplierArrayList = supplierManager.getAllSuppliersArrayList();

        rvAdapterForSupplierMD = new RVAdapterForSupplierMD(supplierArrayList);

        stockNameOrCodeAutoCompleteAdapter = new StockNameOrCodeAutoCompleteAdapter(getContext(), stockManager);

        tax = new Tax();

        taxList = settingManager.getAllTaxs();

        tax = settingManager.getDefaultTax();

        discount = settingManager.getDefaultDiscount();

        if (discount == null) {
            discount = new Discount();
        }
        if (tax == null) {
            tax = new Tax();
        }

        discountList = settingManager.getAllDiscounts();

        purchaseItemViewInSaleList = new ArrayList<>();

        context = getContext();

        darkmode = POSUtil.isNightMode(context);

        now = Calendar.getInstance();

        buildDatePickerDialog();

        purchaseDay = now.get(Calendar.DAY_OF_MONTH);

        purchaseMonth = now.get(Calendar.MONTH) + 1;

        purchaseYear = now.get(Calendar.YEAR);

        TypedArray purchaseInvoice = context.getTheme().obtainStyledAttributes(new int[]{R.attr.purchase_invoice});

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(purchaseInvoice.getString(0));

        buildSupplierListDialog();

        buildStockListDialog();

        buildDatePickerDialog();

        buildStockListDialogEdit();

        loadAddItemDialog();

        buildStockChooserDialog();

        buildTaxListDialogEdit();

        loadEditItemDialog();

        buildAddDiscountDialog();

        buildPayDialog();

        loadNewItemDialog();
        buildSupplierDialog();
        loadUI();




        buildingAlertDialog();

        MainActivity.setCurrentFragment(this);

        AccessLogManager accessLogManager = new AccessLogManager(context);
        AuthorizationManager authorizationManager = new AuthorizationManager(context);
        Long deviceId = authorizationManager.getDeviceId(Build.SERIAL);
        currentUser = accessLogManager.getCurrentlyLoggedInUser(deviceId);
        if (currentUser == null) {
            POSUtil.noUserIsLoggedIn(context);
            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            return null;
        }


        supplierImageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                supplierMaterialDialog.show();
            }
        });

        return mainLayout;

    }

    private void buildStockChooserDialog() {
        String stockChooser = context.getTheme().obtainStyledAttributes(new int[]{R.attr.stock_chooser}).getString(0);

        stockChooserDialog = new MaterialDialog.Builder(context).

                title(stockChooser)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(stockChooserAdapter, new LinearLayoutManager(context))

                .build();

    }

    public void loadNewItemDialog() {

        TypedArray addSaleItem = context.getTheme().obtainStyledAttributes(new int[]{R.attr.stock});
        // TypedArray save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        // TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});


        addNewStockMaterialDialog = new MaterialDialog.Builder(context).

                customView(R.layout.add_new_stock_quick, true).title(addSaleItem.getString(0)).
                typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").
                // positiveText(save.getString(0)).

                // negativeText(cancel.getString(0)).

                        build();


        itemName = (EditText) addNewStockMaterialDialog.findViewById(R.id.item_name);

        codeNo = (EditText) addNewStockMaterialDialog.findViewById(R.id.code_no);

        retailPrice = (EditText) addNewStockMaterialDialog.findViewById(R.id.retail_price);

        purchasePrice = (EditText) addNewStockMaterialDialog.findViewById(R.id.purchase_price);

        addNewStockMaterialDialog.findViewById(R.id.cancel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewStockMaterialDialog.dismiss();

            }
        });

        addNewStockMaterialDialog.findViewById(R.id.save).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                StockItem stockItem = new StockItem();

                if (vaildationStock()) {
                    stockItem.setName(itemName.getText().toString());
                    stockItem.setCodeNo(codeNo.getText().toString());
                    stockItem.setRetailPrice(Double.valueOf(retailPrice.getText().toString()));
                    stockItem.setPurchasePrice(Double.valueOf(purchasePrice.getText().toString()));

                }


                if (stockItem.isVaild()) {
                    stockManager.addQuickStockSetupSale(stockItem.getName(), stockItem.getCodeNo(), stockItem.getRetailPrice(), stockItem.getPurchasePrice());

                    addNewStockMaterialDialog.dismiss();
                } else {
                    Log.e("INVALID ", "STOCK WHILE ADDING QUICK SETUP");
                }

            }
        });


    }

    public void checkPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the requestPermissions.

            } else {

                // No explanation needed, we can request the requestPermissions.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA}, 200);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    public boolean vaildationStock() {

        boolean status = true;

        if (POSUtil.isTextViewEmpty(itemName)) {
            status = false;
            itemName.setError("Invalid Item Name");
        }

        if (POSUtil.isTextViewEmpty(codeNo)) {
            status = false;
            codeNo.setError("Invalid Code Number");
        }

        if (POSUtil.isTextViewEmpty(retailPrice)) {
            status = false;
            retailPrice.setError("Invalid Item Name");
        }

        if (POSUtil.isTextViewEmpty(purchasePrice)) {
            status = false;
            purchasePrice.setError("Invalid Item Name");
        }

        return status;
    }

    private void restoreReorderStockValue() {
        purchaseItemInPurchase = new SalesAndPurchaseItem();
        purchaseItemInPurchase.setStockID(stockItem.getId());
        itemCodeTextInputEditTextAddItemMd.setText(stockItem.getCodeNo());
        itemNameTextInputEditTextAddItemMd.setText(stockItem.getName());
        //        priceTextInputEditTextAddItemMd.setText(POSUtil.NumberFormat(stockItem.getPurchasePrice()));
        qtyTextInputEditTextAddItemMd.setText("1");
        qty = 1;
        qtyTextInputEditTextAddItemMd.requestFocus();
    }

    private void showAddItemDialog() {

        purchaseItemInPurchase = new SalesAndPurchaseItem();

        foc = false;
        isEdit = false;
        addItemMaterialDialog.show();

    }


    /**
     * no use
     */
    public void buildSupplierDialog() {
        TypedArray supplier = context.getTheme().obtainStyledAttributes(new int[]{R.attr.supplier});
        supplierMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.fragment_sales_customer_dialog_layout, true).title(supplier.getString(0))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").build();

        supplierTextInputEditText = (AutoCompleteTextView) supplierMaterialDialog.findViewById(R.id.customer_name_edit_text);
        supplierNameTextView = (AutoCompleteTextView) supplierMaterialDialog.findViewById(R.id.customer_name_edit_text);
        supplierPhoneNoEditText = (EditText) supplierMaterialDialog.findViewById(R.id.phone_no_edit_text);
        supplierSaveButton = (Button) supplierMaterialDialog.findViewById(R.id.save_btn);
        supplierCancelButton = (Button) supplierMaterialDialog.findViewById(R.id.cancel_btn);
        supplierSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!POSUtil.isTextViewEmpty(supplierPhoneNoEditText)) {
                    phoneNo = supplierPhoneNoEditText.getText().toString().trim();
                } else {
                    phoneNo = "";
                }

                supplierMaterialDialog.dismiss();
            }
        });


        supplierCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                supplierMaterialDialog.dismiss();
            }
        });
        supplierNameTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!POSUtil.isTextViewEmpty(supplierNameTextView)) {
                    supplierBussinessName = supplierNameTextView.getText().toString().trim();
                    if (supplierBussinessName.equalsIgnoreCase(DEFAULT_SUPPLIER)) {
                        supplierNameTextView.setText(null);
                    }

                } else {
                    supplierBussinessName = DEFAULT_SUPPLIER;
                }

            }
        });
        supplierNameTextView.setThreshold(1);

        supplierAutoCompleteAdapter = new SupplierAutoCompleteAdapter(context, supplierArrayList);

        supplierNameTextView.setAdapter(supplierAutoCompleteAdapter);
      /*  rvAdapterForCustomerMD.setmItemClickListener(new RVAdapterForCustomerMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                supplierNameTextView.setText(customerVOList.get(position).getName().trim());

                customerListMaterialDialog.dismiss();

            }
        });

        supplierNameTextView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    supplierPhoneNoEditText.requestFocus();
                }
                return false;
            }
        });

        supplierPhoneNoEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                phoneNo = supplierPhoneNoEditText.getText().toString().trim();

            }
        });*/
    }

    public void buildSupplierListDialog() {

        supplierListMaterialDialog = new MaterialDialog.Builder(context).

                adapter(rvAdapterForSupplierMD, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        //supplierTextInputEditText.setText(supplierBussinessName);

        //configUI();

        configRecyclerView();

        settingOnClick();

        POSUtil.hideKeyboard(mainLayout, context);

        rvAdapterForSupplierMD.setmItemClickListener(new RVAdapterForSupplierMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                supplierTextInputEditText.setText(supplierArrayList.get(position).getName().trim());

                supplierListMaterialDialog.dismiss();

            }
        });


        supplierAutoCompleteAdapter = new SupplierAutoCompleteAdapter(context, supplierArrayList);

        //   supplierTextInputEditText.setAdapter(supplierAutoCompleteAdapter);

        // supplierTextInputEditText.setThreshold(1);

        addCallback = new StockCodeAutoCompleteAdapter.Callback() {
            @Override
            public void onOneResut(final StockAutoComplete stockAutoComplete) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        assigValuesForitemAdd(stockAutoComplete);

                    }
                });

                addPurchaseItem(stockAutoComplete);

            }
        };

        editCallback = new StockCodeAutoCompleteAdapter.Callback() {
            @Override
            public void onOneResut(final StockAutoComplete stockAutoComplete) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        assigValuesForitemEdit(stockAutoComplete);

                    }
                });

                editPurchaseItem(stockAutoComplete);

            }
        };

/*        addItemCallback = new StockCodeAutoCompleteAdapter.Callback() {
            @Override
            public void onOneResut(StockAutoComplete stockAutoCompleteView) {

                purchaseItemInPurchase = new SalesAndPurchaseItem();

                purchaseItemInPurchase.setQty(1);

                purchaseItemInPurchase.setDiscountPercent("0.0%");

                purchaseItemInPurchase.setDiscountAmount(0.0);

                purchaseItemInPurchase.setItemName(stockAutoCompleteView.getItemName());

                purchaseItemInPurchase.setStockCode(stockAutoCompleteView.getCodeNo());

                purchaseItemInPurchase.setPrice(stockAutoCompleteView.getPurchasePrice());

                purchaseItemInPurchase.setTotalPrice(purchaseItemInPurchase.getQty() * purchaseItemInPurchase.getPrice());

                purchaseItemViewInSaleList.add(purchaseItemInPurchase);

                rvSwipeAdapterForPurchaseItemViewInPurchase.setValueList(purchaseItemViewInSaleList);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        rvSwipeAdapterForPurchaseItemViewInPurchase.notifyItemInserted(purchaseItemViewInSaleList.size());

                        //autoCompleteTextView.setSelection(0, autoCompleteTextView.length());

                        updateViewDataForBarScan();

                    }
                });


            }
        };*/


        addstockCodeAutoCompleteAdapter = new StockCodeAutoCompleteAdapter(context, stockManager, addCallback);
        addstockNameAutoCompleteAdapter = new StockNameAutoCompleteAdapter(context, stockManager);
        editstockNameAutoCompleteAdapter = new StockNameAutoCompleteAdapter(context, stockManager);
        editstockCodeAutoCompleteAdapter = new StockCodeAutoCompleteAdapter(context, stockManager, editCallback);

        //stockCodeAutoCompleteAdapter=new StockCodeAutoCompleteAdapter(context,stockBusiness, addItemCallback);

        // autoCompleteTextView= (AutoCompleteTextView) mainLayout.findViewById(R.id.supplier_name);

        barCodeTIET.setSelectAllOnFocus(true);


    /*    itemNameTextInputEditTextAddItemMd.setAdapter(addstockNameAutoCompleteAdapter);

        itemNameTextInputEditTextEditItemMd.setAdapter(editstockNameAutoCompleteAdapter);

        //   autoCompleteTextView.setThreshold(1);

        //  autoCompleteTextView.setAdapter(stockCodeAutoCompleteAdapter);

        itemCodeTextInputEditTextAddItemMd.setThreshold(1);

        itemCodeTextInputEditTextAddItemMd.setAdapter(addstockCodeAutoCompleteAdapter);

        itemCodeTextInputEditTextEditItemMd.setAdapter(editstockCodeAutoCompleteAdapter);


        itemCodeTextInputEditTextAddItemMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockAutoCompleteView = addstockCodeAutoCompleteAdapter.getSuggestionList().get(position);

                assigValuesForitemAdd(stockAutoCompleteView);

                addPurchaseItem(stockAutoCompleteView);

                itemCodeTextInputEditTextAddItemMd.clearFocus();

            }
        });*/

        itemCodeTextInputEditTextAddItemMd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    //                    priceTextInputEditTextAddItemMd.requestFocus();
                }
                return false;
            }
        });

        itemNameTextInputEditTextAddItemMd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    itemCodeTextInputEditTextAddItemMd.requestFocus();
                }
                return false;
            }
        });

        itemCodeTextInputEditTextEditItemMd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    priceTextInputEditTextEditItemMd.requestFocus();
                }
                return false;
            }
        });

        itemNameTextInputEditTextEditItemMd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    itemCodeTextInputEditTextEditItemMd.requestFocus();
                }
                return false;
            }
        });

        /*itemCodeTextInputEditTextEditItemMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockAutoCompleteView = editstockCodeAutoCompleteAdapter.getSuggestionList().get(position);

                assigValuesForitemEdit(stockAutoCompleteView);

                editPurchaseItem(stockAutoCompleteView);

                itemCodeTextInputEditTextEditItemMd.clearFocus();

            }
        });

        itemNameTextInputEditTextAddItemMd.setThreshold(1);

        itemNameTextInputEditTextAddItemMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockAutoCompleteView = addstockNameAutoCompleteAdapter.getSuggestionList().get(position);

                assigValuesForitemAdd(stockAutoCompleteView);

                addPurchaseItem(stockAutoCompleteView);

                itemNameTextInputEditTextAddItemMd.clearFocus();

            }
        });

       /* itemNameTextInputEditTextEditItemMd.setThreshold(1);

        itemNameTextInputEditTextEditItemMd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockAutoCompleteView = editstockNameAutoCompleteAdapter.getSuggestionList().get(position);

                assigValuesForitemEdit(stockAutoCompleteView);

                editPurchaseItem(stockAutoCompleteView);

                itemNameTextInputEditTextEditItemMd.clearFocus();
            }
        });*/

       /* barCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(barCode){

                    barCodeBtn.setActivated(true);

                    barCode=false;

                    stockCodeTIL.setVisibility(View.GONE);

                    autoCompleteTextView.setVisibility(View.GONE);

                    barCodeTIL.setVisibility(View.VISIBLE);

                    barCodeTIET.setVisibility(View.VISIBLE);

                    barCodeTIET.requestFocus();

                }else {

                    barCodeBtn.setActivated(false);

                    barCode=true;

                    autoCompleteTextView.setVisibility(View.VISIBLE);

                    barCodeTIET.setVisibility(View.VISIBLE);

                    stockCodeTIL.setVisibility(View.VISIBLE);

                    barCodeTIL.setVisibility(View.GONE);

                    autoCompleteTextView.requestFocus();

                }

            }
        });

        barCodeTIET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                Stock stockVO=stockBusiness.findStockByStockCode(s.toString());

                if(stockVO.getStockCode()!=null){

                    purchaseItemInPurchase=new SalesAndPurchaseItem();

                    purchaseItemInPurchase.setItemName(stockVO.getItemName());


                    purchaseItemInPurchase.setQty(1);

                    purchaseItemInPurchase.setDiscountPercent("0.0%");

                    purchaseItemInPurchase.setDiscountAmount(0.0);

                    purchaseItemInPurchase.setStockCode(stockVO.getStockCode());

                    purchaseItemInPurchase.setPrice(stockVO.getNormalSalePrice());

                    purchaseItemInPurchase.setTotalPrice(purchaseItemInPurchase.getQty()*purchaseItemInPurchase.getPrice());

                    purchaseItemViewInSaleList.add(purchaseItemInPurchase);

                    rvSwipeAdapterForPurchaseItemViewInPurchase.setValueList(purchaseItemViewInSaleList);

                    rvSwipeAdapterForPurchaseItemViewInPurchase.notifyItemInserted(purchaseItemViewInSaleList.size());

                    barCodeTIET.setSelection(0,barCodeTIET.length());

                    updateViewDataForBarScan();

                }

            }

        });*/

        /*autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StockAutoComplete stockItem = stockCodeAutoCompleteAdapter.getSuggestionList().get(position);

                purchaseItemInPurchase = new SalesAndPurchaseItem();

                purchaseItemInPurchase.setItemName(stockItem.getItemName());

                purchaseItemInPurchase.setQty(1);

                purchaseItemInPurchase.setDiscountPercent("0.0%");

                purchaseItemInPurchase.setDiscountAmount(0.0);

                purchaseItemInPurchase.setStockCode(stockItem.getCodeNo());

                purchaseItemInPurchase.setPrice(stockItem.getPurchasePrice());

                purchaseItemInPurchase.setTotalPrice(purchaseItemInPurchase.getQty() * purchaseItemInPurchase.getPrice());

                purchaseItemViewInSaleList.add(purchaseItemInPurchase);

                rvSwipeAdapterForPurchaseItemViewInPurchase.setValueList(purchaseItemViewInSaleList);

                rvSwipeAdapterForPurchaseItemViewInPurchase.notifyItemInserted(purchaseItemViewInSaleList.size());

                autoCompleteTextView.setText(null);

                updateViewData();

            }
        });*/


        rvAdapterForTaxMD.setmItemClickListener(new RVAdapterForTaxMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                taxListMaterialDialog.dismiss();

                tax = taxList.get(position);

                updateViewData();

            }
        });


        if (getArguments() != null && getArguments().containsKey("purchaseheader")) {

            //            if (getArguments().containsKey("reorderstock")) {
            //
            //                stockItem = (StockItem) getArguments().getSerializable("reorderstock");
            //
            //
            //                showAddItemDialog();
            //
            //                restoreReorderStockValue();
            //
            //
            //            } else
            if (getArguments().containsKey("purchaseheader")) {

                purchaseHistory = (PurchaseHistory) getArguments().getSerializable("purchaseheader");

                if (purchaseHistory.isHold()) {
                    purchaseHeader = purchaseManager.getHoldPurchaseHeaderByPurchaseID(purchaseHistory.getId(), new InsertedBooleanHolder());

                } else {
                    purchaseHeader = purchaseManager.getPurchaseHeaderByPurchaseID(purchaseHistory.getId(), new InsertedBooleanHolder());
                }

            }

        }


        supplierTextInputEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && event.getAction() == KeyEvent.ACTION_UP) {
                    phoneNoEditText.requestFocus();
                }
                return false;
            }
        });

        phoneNoEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                phoneNo = phoneNoEditText.getText().toString();

            }
        });


        supplierTextInputEditText.setText(DEFAULT_SUPPLIER);

        if (purchaseHistory != null) {

            isPurEdit = true;
            isHold = purchaseHistory.isHold();
            initializeOldData();

            refreshRecyclerView();

            // configUI();
            if (!isHold) {
                holdBtn.setVisibility(View.GONE);
            }
            updateViewData();

        }


        cancelDiscountBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addDiscountMaterialDialog.dismiss();
            }
        });

        cancelMdButtonAddItemMd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addItemMaterialDialog.dismiss();
            }
        });

        cancelMdButtonEditItemMd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                editItemMaterialDialog.dismiss();
            }
        });

        cancelTransaction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                payMaterialDialog.dismiss();
            }
        });


        holdBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isPurEdit) {
                    Double balance = totalAmount - paidAmount;


                    if (balance < 0) {

                        balance = 0.0;

                    }

                    if (discount.getIsPercent() == 1) {
                        discountPercent = discount.getAmount() + " %";
                    } else {
                        discountPercent = "";
                    }

                    boolean status = purchaseManager.addNewHoldPurchase(purchaseInvoiceNo, date, supplierBussinessName, phoneNo,
                            "", totalAmount, discountPercent, tax.getId(), subtotal, voucherDiscount, "No remark", paidAmount,
                            balance, voucherTax, Integer.toString(purchaseDay),
                            Integer.toString(purchaseMonth), Integer.toString(purchaseYear), tax.getRate(), purchaseItemViewInSaleList, tax.getType(), discount.getId(), currentUser.getId());

                    if (status) {

                        FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                        clearInputAndOpenNewVoucher();
                        //Intent addCurrencyIntent=new Intent(context, DetailActivity.class);

                        //addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_PURCHASE);

                        // startActivity(addCurrencyIntent);
                    }
                } else {
                    Double balance = totalAmount - paidAmount;

                    if (balance < 0) {
                        balance = 0.0;

                    }

                    if (discount.getIsPercent() == 1) {
                        discountPercent = discount.getAmount() + " %";
                    } else {
                        discountPercent = "";
                    }

                    boolean status = purchaseManager.updateHoldPurchaseByID(purchaseHeader.getId(), date, supplierBussinessName, phoneNo, totalAmount,

                            discountPercent, tax.getId(), subtotal, voucherDiscount, "No Remark", paidAmount, balance, voucherTax, Integer.toString(purchaseDay),

                            Integer.toString(purchaseMonth), Integer.toString(purchaseYear), tax.getRate(), updateList, deleteList, discount.getId());

                    if (status) {
                        //                        getActivity().onBackPressed();
                        FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                        clearInputAndOpenNewVoucher();
                    }
                }
            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();

        if (!POSUtil.isTabetLand(context)) {

        } else {
            buildSupplierDialog();

        }

        configUI();
        updateViewData();
        stockChooserAdapter.setmItemClickListener(new RVAdapterForStockChooser.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                qty = 1;
                setItemValue(stockChooserAdapter.getSelectedStock());

                if (purchaseItemViewInSaleList.contains(purchaseItemInPurchase)) {

                    SalesAndPurchaseItem salesAndPurchaseItem = purchaseItemViewInSaleList.get(purchaseItemViewInSaleList.indexOf(purchaseItemInPurchase));

                    salesAndPurchaseItem.setQty(salesAndPurchaseItem.getQty() + 1);

                    salesAndPurchaseItem.setTotalPrice((salesAndPurchaseItem.getQty() * salesAndPurchaseItem.getPrice()));

                } else {
                    purchaseItemViewInSaleList.add(0, purchaseItemInPurchase);
                }


                refreshRecyclerView();

                stockChooserDialog.dismiss();

                updateViewData();

                updateList.add(purchaseItemInPurchase);

                foc = false;

                purchaseItemInPurchase = new SalesAndPurchaseItem();

                //clearInputAddItemMaterialDialog();

                stockAutoCompleteView = new StockItem();
                itemOrCodeAutocomplete.setText("");
            }
        });
        if (addNewStock != null) {
            addNewStock.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //                    addNewStockMaterialDialog.show();
                    Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                    addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_STOCK);

                    startActivity(addCurrencyIntent);
                }
            });
        }


        if (barcodeBtn != null) {

            barcodeBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkPermission();
                    // Here, thisActivity is the current activity
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_GRANTED) {


                        Intent intent = new Intent(getContext(), CaptureActivity.class);
                        intent.setAction("com.google.zxing.client.android.SCAN");
                        intent.putExtra("SAVE_HISTORY", false);
                        startActivityForResult(intent, SCANNER_BAR_CODE);
                    }
                }
            });
        }


        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        itemOrCodeAutocomplete.requestFocus();
        itemOrCodeAutocomplete.setThreshold(1);

        itemOrCodeAutocomplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


               /* if( stockBusiness.findStockByName(s.toString()).getItemName()==null){
                    stockAutoCompleteView=new StockItem();
                }
                if( stockBusiness.findStockByStockCode(s.toString()).getStockCode()==null){
                    stockAutoCompleteView=new StockItem();
                }*/
                if (!POSUtil.isTextViewEmpty(itemOrCodeAutocomplete)) {
                    itemOrCodeAutocomplete.setError(null);
                }


            }
        });

        itemOrCodeAutocomplete.setAdapter(stockNameOrCodeAutoCompleteAdapter);

        if (POSUtil.isTabetLand(context)) {
            payLinearLayout.setVisibility(View.GONE);
            saleDetailPrint.setVisibility(View.GONE);
            addItemLinearLayout.setVisibility(View.VISIBLE);

            mainLayout.findViewById(R.id.pay_save_and_print).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.e("INOVICE NO", purchaseInvoiceNo + " ss");
                    //saveTransaction();

                    Log.e("INOVICE NO", purchaseInvoiceNo + " ss");
                    Long purchaseId = purchaseManager.findIDByPurchaseInvoice(purchaseInvoiceNo);

                    PurchaseDetailFragment purchaseDetailFragment = new PurchaseDetailFragment();

                    Bundle bundle = new Bundle();

                    bundle.putLong(PurchaseDetailFragment.KEY, purchaseId);

                    bundle.putBoolean(PurchaseDetailFragment.PRINTKEY, true);

                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

                    fragmentTransaction.replace(R.id.sale_detail_print, purchaseDetailFragment);

                    //fragmentTransaction.addToBackStack("back");
                    purchaseDetailFragment.setArguments(bundle);

                    payLinearLayout.setVisibility(View.GONE);
                    saleDetailPrint.setVisibility(View.VISIBLE);

                    fragmentTransaction.commit();
                    clearInputAndOpenNewVoucher();

                }
            });


            //            itemOrCodeAutocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //                @Override
            //                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //
            //                    stockAutoCompleteView = stockNameOrCodeAutoCompleteAdapter.getSuggestionList().get(position);
            //
            //                    assigValuesForitemAdd(stockAutoCompleteView);
            //
            //                    itemOrCodeAutocomplete.setText("");
            //
            //                    addPurchaseItem(stockAutoCompleteView);
            //
            //                    POSUtil.hideKeyboard(itemOrCodeAutocomplete,getContext());
            //
            //                }
            //            });

            bluetoothUtil = BluetoothUtil.getInstance(this);

            bluetoothUtil.setBluetoothConnectionListener(new BluetoothUtil.JobListener() {
                @Override
                public void onPrepare() {

                    connectHud.show();
                }

                @Override
                public void onSuccess() {

                    if (connectHud.isShowing()) {

                        connectHud.dismiss();

                    }

                    printDeviceNameTextView.setText(bluetoothUtil.getDevice().getName());


                    if (bluetoothUtil.isConnected()) {

                        statusTextView.setTextColor(Color.parseColor("#2E7D32"));
                        String connected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.connected}).getString(0);

                        statusTextView.setText(connected);

                    } else {

                        statusTextView.setTextColor(Color.parseColor("#DD2C00"));
                        String nonConnected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);

                        statusTextView.setText(nonConnected);

                    }
                }

                @Override
                public void onFailure() {
                    String noDevice = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_device}).getString(0);
                    String notConnected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);
                    String toast = context.getTheme().obtainStyledAttributes(new int[]{R.attr.make_sure_bluetooth_turn_on_both_devices_and_choose_printer_only}).getString(0);

                    printDeviceNameTextView.setText(noDevice);

                    statusTextView.setText(notConnected);

                    //DisplayToast(toast);

                    if (connectHud.isShowing()) {

                        connectHud.dismiss();

                    }
                }

                @Override
                public void onDisconnected() {
                    String conLost = context.getTheme().obtainStyledAttributes(new int[]{R.attr.connection_lost}).getString(0);
                    String noDevice = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_device}).getString(0);
                    String notConnected = context.getTheme().obtainStyledAttributes(new int[]{R.attr.not_connected}).getString(0);

                    //DisplayToast(conLost);

                    printDeviceNameTextView.setText(noDevice);

                    statusTextView.setTextColor(Color.parseColor("#DD2C00"));

                    statusTextView.setText(notConnected);

                }
            });

            mainLayout.findViewById(R.id.blue_toothLL).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    bluetoothUtil.createConnection(true);
                }
            });

            //   itemOrCodeAutocomplete.requestFocus();
            keyBoardView.setKeyPressListner(new KeyBoardView.KeyPressListner() {
                @Override
                public void onKeyPress(int num) {

                    if (paidAmountTextInputEditTextPayMd.length() < 1 || Long.parseLong(paidAmountTextInputEditTextPayMd.getText().toString()) == 0) {
                        paidAmountTextInputEditTextPayMd.setText(Long.toString(num));
                    } else {
                        paidAmountTextInputEditTextPayMd.setText(paidAmountTextInputEditTextPayMd.getText().toString() + Long.toString(num));
                    }

                }

                @Override
                public void onClear() {
                    paidAmountTextInputEditTextPayMd.setText("0");
                }

                @Override
                public void onDelete() {
                    if (paidAmountTextInputEditTextPayMd.length() == 1) {
                        paidAmountTextInputEditTextPayMd.setText("0");
                    } else {

                        String temp = paidAmountTextInputEditTextPayMd.getText().toString().substring(0, paidAmountTextInputEditTextPayMd.getText().toString().length() - 1);
                        //paidAmountTextInputEditTextPayMd.getText().charAt(paidAmountTextInputEditTextPayMd.length()-1);
                        Log.e("Here", paidAmountTextInputEditTextPayMd.getText().charAt(paidAmountTextInputEditTextPayMd.length() - 1) + "CHAR LAST INDEX");
                        paidAmountTextInputEditTextPayMd.setText(temp);
                    }
                }

                @Override
                public void onAddNum(int num) {
                    if (paidAmountTextInputEditTextPayMd.length() < 1 || Long.parseLong(paidAmountTextInputEditTextPayMd.getText().toString()) == 0) {
                        paidAmountTextInputEditTextPayMd.setText(Long.toString(num));
                    } else {
                        paidAmountTextInputEditTextPayMd.setText(Long.toString(Long.parseLong(paidAmountTextInputEditTextPayMd.getText().toString()) + num));
                    }
                }
            });

            animation1 = AnimationUtils.loadAnimation(getContext(), R.anim.to_middle);
            animation1.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    if (isPay) {
                        payLinearLayout.clearAnimation();
                        payLinearLayout.setAnimation(animation2);
                        payLinearLayout.startAnimation(animation2);
                        addItemLinearLayout.setVisibility(View.GONE);
                    } else {
                        addItemLinearLayout.clearAnimation();
                        addItemLinearLayout.setAnimation(animation2);
                        addItemLinearLayout.startAnimation(animation2);
                        payLinearLayout.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            animation2 = AnimationUtils.loadAnimation(context, R.anim.from_middle);
            animation2.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    //addItemLinearLayout.setVisibility(View.VISIBLE);

                    if (isPay) {

                        payLinearLayout.setVisibility(View.VISIBLE);

                        addItemLinearLayout.setVisibility(View.GONE);
                    } else {

                        payLinearLayout.setVisibility(View.GONE);

                        addItemLinearLayout.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            mainLayout.findViewById(R.id.pay_cancel).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //payLinearLayout.setVisibility(View.GONE);
                    // slideToRight(payLinearLayout);
                    // payLinearLayout.setVisibility(View.GONE);
                    isPay = false;

                    toggleView();


                }
            });

            payTextViewBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (purchaseItemViewInSaleList.size() >= 1) {
                        toggleViewToPayment();
                    }

                }
            });
        } else {
            payTextViewBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (purchaseItemViewInSaleList.size() < 1) {

                        //  alertMaterialDialog.show();

                    } else {

                        POSUtil.showKeyboard(paidAmountTextInputEditTextPayMd);

                        // show payment fragment

                        showPaymentFragment();
                    }

                }
            });
            itemOrCodeAutocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    stockAutoCompleteView = stockNameOrCodeAutoCompleteAdapter.getSuggestionList().get(position);

                    // assigValuesForitemAdd(stockAutoCompleteView);

                    itemOrCodeAutocomplete.setText("");


                    addSaleItem(stockAutoCompleteView);
                    //itemOrCodeAutocomplete.clearFocus();
                    // discountTextInputEditTextAddItemMd.requestFocus();
                    // discountTextInputEditTextAddItemMd.clearFocus();
                    POSUtil.hideKeyboard(mainLayout, getContext());
                    //addItem();
                    if (purchaseItemViewInSaleList.contains(purchaseItemInPurchase)) {

                        SalesAndPurchaseItem salesAndPurchaseItem = purchaseItemViewInSaleList.get(purchaseItemViewInSaleList.indexOf(purchaseItemInPurchase));
                        salesAndPurchaseItem.setQty(salesAndPurchaseItem.getQty() + 1);
                        salesAndPurchaseItem.setTotalPrice((salesAndPurchaseItem.getQty() * salesAndPurchaseItem.getPrice()));
                        updateList.add(salesAndPurchaseItem);
                    } else {
                        purchaseItemViewInSaleList.add(0, purchaseItemInPurchase);
                        updateList.add(purchaseItemInPurchase);
                    }
                    updateViewData();

                    foc = false;

                    purchaseItemInPurchase = new SalesAndPurchaseItem();

                }
            });
        }

    }

    private void showPaymentFragment() {

        String noRemark = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no_remark}).getString(0);



        if (discount.getIsPercent() == 1) {
            discountPercent = discount.getAmount() + " %";
        } else {
            discountPercent = "";
        }

        PurchaseHeader purchaseHeader = new PurchaseHeader();
        purchaseHeader.setSaleDate(date);
        purchaseHeader.setTotal(totalAmount);
        purchaseHeader.setDiscountPercent(discountPercent);
        purchaseHeader.setTaxID(tax.getId());
        purchaseHeader.setSubtotal(subtotal);
        purchaseHeader.setDiscountAmt(voucherDiscount);
        purchaseHeader.setRemark("No remark");
        purchaseHeader.setPaidAmt(paidAmount);
        purchaseHeader.setTaxAmt(voucherTax);
        purchaseHeader.setDay(Integer.toString(purchaseDay));
        purchaseHeader.setMonth(Integer.toString(purchaseMonth));
        purchaseHeader.setYear(Integer.toString(purchaseYear));
        purchaseHeader.setTaxRate(tax.getRate());
        purchaseHeader.setTaxType(tax.getType());
        purchaseHeader.setDiscountID(discount.getId());
        purchaseHeader.setPurchaseEdit(isPurEdit);
        purchaseHeader.setPurchaseHold(isHold);
        purchaseHeader.setSalesAndPurchaseItemList(purchaseItemViewInSaleList);

        if (isPurEdit) {
            purchaseHeader.setSupplierID(purchaseHistory.getSupplierID());
            purchaseHeader.setSupplierName(supplierBussinessName);
            purchaseHeader.setId(purchaseHistory.getId());
            purchaseHeader.setDeleteList(deleteList);

        }


        Intent addPayIntent = new Intent(context, DetailActivity.class);
        addPayIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.PURCHASE_PAYMENT);
        addPayIntent.putExtra("purchaseHeader", purchaseHeader);
        getActivity().startActivityForResult(addPayIntent, 2);

    }

    private void toggleView() {
        payLinearLayout.clearAnimation();

        payLinearLayout.setAnimation(animation1);

        payLinearLayout.startAnimation(animation1);
    }

    private void toggleViewToPayment() {
        if (!payLinearLayout.isShown()) {
            isPay = true;
            // slideToLeft(payLinearLayout);
            Log.e("Here visible", payLinearLayout.isShown() + " SS");
            toggleView();

        }
    }

    private void addSaleItem(StockItem stockItem) {

        purchaseItemInPurchase.setDiscountAmount(0.0);

        purchaseItemInPurchase.setBarcode(stockItem.getBarcode());

        purchaseItemInPurchase.setStockCode(stockItem.getCodeNo());

        purchaseItemInPurchase.setItemName(stockItem.getName());

        purchaseItemInPurchase.setQty(1);

        qty = 1;

        purchaseItemInPurchase.setStockID(stockItem.getId());

        purchaseItemInPurchase.setTotalPrice(stockItem.getPurchasePrice());

        purchaseItemInPurchase.setPrice(stockItem.getPurchasePrice());

    }

    public void refreshRecyclerView() {

        rvSwipeAdapterForPurchaseItemViewInPurchase.setSaleItemViewInSaleList(purchaseItemViewInSaleList);

        rvSwipeAdapterForPurchaseItemViewInPurchase.notifyDataSetChanged();

    }

    public void initializeOldData() {

        //Log.w("Herer Purchase history View ID",purchaseHistory.getId()+" ID");

        //purchaseHeader = purchaseBusiness.getPurchaseHeaderByPurchaseID(purchaseHistory.getId(), new InsertedBooleanHolder());

        purchaseInvoiceNo = purchaseHeader.getPurchaseVocherNo();


        //Log.w("Herer Purchase history View ID",purchaseHeader.getId()+" ID");

        paidAmount = purchaseHeader.getPaidAmt();

        isHold = purchaseHistory.isHold();
        if (isHold) {
            purchaseItemViewInSaleList = purchaseManager.getHoldPurchaseDetailsByPurchaseID(purchaseHeader.getId());
        } else {
            purchaseItemViewInSaleList = purchaseManager.getPurchaseDetailsByPurchaseID(purchaseHeader.getId());
        }


        purchaseDay = Integer.parseInt(purchaseHeader.getDay());

        purchaseMonth = Integer.parseInt(purchaseHeader.getMonth());

        discount = new Discount(purchaseHeader.getDiscountID(), purchaseHeader.getDiscount(), purchaseHeader.getDiscountAmt());

        purchaseYear = Integer.parseInt(purchaseHeader.getYear());


        now.set(purchaseYear, purchaseMonth - 1, purchaseDay);

        voucherDiscount = purchaseHeader.getDiscountAmt();

        supplierBussinessName = purchaseHeader.getSupplierName();

        supplierPhoneNoEditText.setText(supplierManager.getSupplierById(purchaseHeader.getSupplierID()).getPhoneNo());


        Log.w("HERE IN OLD DATA", supplierBussinessName + " name");

        tax = new Tax(purchaseHeader.getTaxID(), purchaseHeader.getTaxName(), purchaseHeader.getTaxRate(), purchaseHeader.getTaxType(), "");

    }

    private void addPurchaseItem(StockAutoComplete stockAutoComplete) {

        purchaseItemInPurchase.setDiscountAmount(0.0);

        purchaseItemInPurchase.setBarcode(stockAutoComplete.getBarcode());

        purchaseItemInPurchase.setStockCode(stockAutoComplete.getCodeNo());

        purchaseItemInPurchase.setItemName(stockAutoComplete.getItemName());

        purchaseItemInPurchase.setQty(1);

        qty = 1;

        purchaseItemInPurchase.setStockID(stockAutoComplete.getId());
    }

    private void addPurchaseItem(StockItem stockItem) {

        purchaseItemInPurchase.setDiscountAmount(0.0);

        purchaseItemInPurchase.setBarcode(stockItem.getBarcode());

        purchaseItemInPurchase.setStockCode(stockItem.getCodeNo());

        purchaseItemInPurchase.setItemName(stockItem.getName());


        purchaseItemInPurchase.setQty(1);

        qty = 1;

        purchaseItemInPurchase.setStockID(stockItem.getId());

    }

    /*private void updateViewDataForBarScan() {

        calculateValues();

        //   new Handler().postDelayed(new Runnable() {
        //       @Override
        //       public void run() {
        //           barCodeTIET.setText(null);
        //       }
        //   },1000);


        taxAmtTextView.setText(POSUtil.doubleToString(voucherTax));

        taxRateTextView.setText(POSUtil.NumberFormat(tax.getRate()));

        discountTextView.setText(POSUtil.PercentFromat((voucherDiscount)));

        subtotalTextView.setText(POSUtil.PercentFromat(subtotal));

        totalTextView.setText(POSUtil.PercentFromat(totalAmount));

    }*/


    private void buildingAlertDialog() {

        String noItem = context.getTheme().obtainStyledAttributes(new int[]{R.attr.there_is_no_item}).getString(0);
        alertMaterialDialog = new MaterialDialog.Builder(context).

                title(noItem).

                build();

    }

    public void configUI() {

        purchaseIdTextView.setText(purchaseInvoiceNo);

        supplierTextInputEditText.setText(supplierBussinessName);

        supplierNameTextView.setText(supplierBussinessName);

        Log.w("Supplier Busineess name", supplierBussinessName + " SS");

        configDateUI();

    }

    public void configDateUI() {

        date = DateUtility.makeDate(Integer.toString(purchaseYear), Integer.toString(purchaseMonth), Integer.toString(purchaseDay));

        String dayDes[] = DateUtility.dayDes(date);

        String yearMonthDes = DateUtility.monthYearDes(date);

        //  dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

        dateTextView.setText(DateUtility.makeDateFormatWithSlash(date));

    }

    public void buildTaxListDialogEdit() {
        Log.e("isE", isEdit + " ");
        if (isEdit) {
            rvAdapterForTaxMD = new RVAdapterForTaxMD(taxList, purchaseHeader.getTaxID());
            //            rvAdapterForDiscountMD = new RVAdapterForDiscountMD(discountList, purchaseHeader.getDiscountID());
        } else {
            rvAdapterForTaxMD = new RVAdapterForTaxMD(taxList, tax.getId());
            //            rvAdapterForDiscountMD = new RVAdapterForDiscountMD(discountList, discount.getId());
        }

        taxListMaterialDialog = new MaterialDialog.Builder(context).
                adapter(rvAdapterForTaxMD, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

        //        discountListMaterialDialog = new MaterialDialog.Builder(context).
        //
        //                adapter(rvAdapterForDiscountMD, new LinearLayoutManager(context))
        //
        //                .build();

    }

    public void buildDatePickerDialog() {

        datePickerDialog = DatePickerDialog.newInstance(

                AddEditNewPurchaseFragment.this,

                now.get(Calendar.YEAR),

                now.get(Calendar.MONTH),

                now.get(Calendar.DAY_OF_MONTH)

        );


        if (darkmode)
            datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        datePickerDialog.setThemeDark(darkmode);

    }

    public void configRecyclerView() {

        rvSwipeAdapterForPurchaseItemViewInPurchase = new RVSwipeAdapterForSaleItemViewInPurchase(purchaseItemViewInSaleList);

        //        rvAdapterForDiscountMD.setmItemClickListener(new RVAdapterForDiscountMD.OnItemClickListener() {
        //            @Override
        //            public void onItemClick(View view, int position) {
        //
        //                discountListMaterialDialog.dismiss();
        //
        //                discount = discountList.get(position);
        //
        //
        //                updateViewData();
        //
        //            }
        //        });

        purchaseItemRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        purchaseItemRecyclerView.setAdapter(rvSwipeAdapterForPurchaseItemViewInPurchase);

    }

    // lyx Setup listeners on views of quantity input material dialog
    private void setQtyInputMDOnClickListener(final int position) {
        qtyInputMaterialDialog.findViewById(R.id.ok_btn).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        qtyInputMaterialDialog.dismiss();
                        int qty = POSUtil.isTextViewEmpty(qtyInputEditText) ? 1 : Integer.parseInt(qtyInputEditText.getText().toString());
                        purchaseItemViewInSaleList.get(position).setQty(qty);
                        purchaseItemViewInSaleList.get(position).setTotalPrice(qty * purchaseItemViewInSaleList.get(position).getPrice());
                        rvSwipeAdapterForPurchaseItemViewInPurchase.notifyDataSetChanged();
                        rvSwipeAdapterForPurchaseItemViewInPurchase.closeAllItems();
                        updateViewData();
                        if (!updateList.contains(purchaseItemViewInSaleList.get(position))) {
                            updateList.add(purchaseItemViewInSaleList.get(position));
                        }
                    }
                });
        qtyInputMaterialDialog.findViewById(R.id.cancel_btn).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        qtyInputMaterialDialog.dismiss();
                    }
                });
    }

    // lyx Setup listeners on views of price input material dialog
    private void setPriceInputMDOnClickListener(final int position, final List<String> priceList) {
        buildPricePopup();
        dropDownBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.setWidth(valueTextView.getWidth());
                popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                retailPriceDropDown.setText(priceList.get(0));
                wholeSalePriceDropDown.setText(priceList.get(1));
                popupWindow.showAsDropDown(valueTextView);
            }
        });

        priceInputMaterialDialog.findViewById(R.id.ok_btn).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        priceInputMaterialDialog.dismiss();
                        Double price = POSUtil.isTextViewEmpty(valueTextView) ? 0 : Double.parseDouble(valueTextView.getText().toString());
                        purchaseItemViewInSaleList.get(position).setPrice(price);
                        purchaseItemViewInSaleList.get(position).setTotalPrice(purchaseItemViewInSaleList.get(position).getQty() * price);
                        rvSwipeAdapterForPurchaseItemViewInPurchase.notifyDataSetChanged();
                        rvSwipeAdapterForPurchaseItemViewInPurchase.closeAllItems();
                        updateViewData();
                        if (!updateList.contains(purchaseItemViewInSaleList.get(position))) {
                            updateList.add(purchaseItemViewInSaleList.get(position));
                        }
                    }
                });
        priceInputMaterialDialog.findViewById(R.id.cancel_btn).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        priceInputMaterialDialog.dismiss();
                    }
                });
    }

    // lyx

    /**
     * Build a popup dropdown list of retail price and wholesale price.
     *
     * @return built popup window.
     */
    public PopupWindow buildPricePopup() {

        // initialize a pop up window type
        popupWindow = new PopupWindow(getContext());
        popupWindow.setFocusable(true);

        //        Log.w("WIDTH",priceTextInputEditTextEditItemMd.getWidth()+" SS");

        // set the list view as pop up window content

        int labelWhite = getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.backcolor}).getColor(0, Color.TRANSPARENT);
        // if(labelWhite){
        popupWindow.setBackgroundDrawable(new ColorDrawable(labelWhite));
        //  }

        View view = LayoutInflater.from(getContext()).inflate(R.layout.price_list_item_view, null);

        retailPriceDropDown = view.findViewById(R.id.retail_sale_price_amt);
        wholeSalePriceDropDown = view.findViewById(R.id.whole_sale_price_amt);


        retailLinearLayout = view.findViewById(R.id.retail_ll);
        wholeLinearLayout = view.findViewById(R.id.whole_ll);

        retailLinearLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                valueTextView.setText(retailPriceDropDown.getText().toString());
                popupWindow.dismiss();
            }
        });
        wholeLinearLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                valueTextView.setText(wholeSalePriceDropDown.getText().toString());
                popupWindow.dismiss();
            }
        });

        popupWindow.setContentView(view);

        return popupWindow;
    }

    public void settingOnClick() {


        //        qtyTextInputEditTextAddItemMd.addTextChangedListener(new TextWatcher() {
        //            @Override
        //            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //
        //            }
        //
        //            @Override
        //            public void onTextChanged(CharSequence s, int start, int before, int count) {
        //
        //            }
        //
        //            @Override
        //            public void afterTextChanged(Editable s) {
        //
        //                if (qtyTextInputEditTextAddItemMd.getText().toString().trim().length() > 0) {
        //                    qty = Integer.parseInt(qtyTextInputEditTextAddItemMd.getText().toString().trim());
        //                }
        //
        //            }
        //        });

        //        qtyTextInputEditTextEditItemMd.addTextChangedListener(new TextWatcher() {
        //            @Override
        //            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //
        //            }
        //
        //            @Override
        //            public void onTextChanged(CharSequence s, int start, int before, int count) {
        //
        //            }
        //
        //            @Override
        //            public void afterTextChanged(Editable s) {
        //
        //                if (qtyTextInputEditTextEditItemMd.getText().toString().trim().length() > 0) {
        //                    qty = Integer.parseInt(qtyTextInputEditTextEditItemMd.getText().toString().trim());
        //                }
        //
        //            }
        //        });


        taxTextViewBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                taxListMaterialDialog.show();
            }
        });

        dateLinearLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog.show(getActivity().getFragmentManager(), "PurchasePick");

            }
        });

        if (addpurchaseItemBtn != null) {
            addpurchaseItemBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    addItem();
                    //
                    // stockAutoCompleteView = new StockItem();
                }

            });
        }


        rvSwipeAdapterForPurchaseItemViewInPurchase.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                isEdit = true;

                editposition = postion;

                setInputEditItemMaterialDialog();

                if (!POSUtil.isTabetLand(context)) {

                    editItemMaterialDialog.show();

                }

            }


        });

        rvSwipeAdapterForPurchaseItemViewInPurchase.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                deleteList.add(purchaseItemViewInSaleList.get(postion).getId());
                purchaseItemViewInSaleList.remove(postion);
                rvSwipeAdapterForPurchaseItemViewInPurchase.setSaleItemViewInSaleList(purchaseItemViewInSaleList);
                rvSwipeAdapterForPurchaseItemViewInPurchase.notifyItemRemoved(postion);
                rvSwipeAdapterForPurchaseItemViewInPurchase.notifyDataSetChanged();

                updateViewData();

            }


        });


        // lyx set up listener on quantity EditText of sale item
        rvSwipeAdapterForPurchaseItemViewInPurchase.setEditQtyClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                String quantity = ThemeUtil.getString(context, R.attr.quantity);

                qtyInputMaterialDialog = new MaterialDialog.Builder(context).
                        customView(R.layout.qty_input_md, false)
                        .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                        .canceledOnTouchOutside(true)
                        .build();
                qtyInputMaterialDialog
                        .show();

                qtyInputEditText = (FontEditText) qtyInputMaterialDialog.findViewById(R.id.qty_input_edittext);
                POSUtil.showKeyboard(qtyInputEditText);
                //position is needed for changing current item's qty
                setQtyInputMDOnClickListener(postion);
            }


        });

        //lyx set up listener on price EditText of sale item
        rvSwipeAdapterForPurchaseItemViewInPurchase.setEditPriceClickListener(new ClickListener() {
            @Override
            public void onClick(int position) {
                String price = ThemeUtil.getString(context, R.attr.price);
                Log.e("title", price);

                Stock stock = stockManager.findStockByID(purchaseItemViewInSaleList.get(position).getStockID());
                List<String> priceList = new ArrayList<>();
                // priceList = {retail price, wholesale price, current price}
                priceList.add(POSUtil.doubleToString(stock.getRetailPrice()));
                priceList.add(POSUtil.doubleToString(stock.getWholeSalePrice()));
                priceList.add(POSUtil.doubleToString(purchaseItemViewInSaleList.get(position).getPrice()));

                priceInputMaterialDialog = new MaterialDialog.Builder(context).
                        customView(R.layout.price_input_dialog, false)
                        .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                        .canceledOnTouchOutside(true)
                        .build();
                priceInputMaterialDialog.
                        show();

                valueTextView = (IgnoableAutoCompleteTextView) priceInputMaterialDialog.findViewById(R.id.value_text_view);
                dropDownBtn = (FrameLayout) priceInputMaterialDialog.findViewById(R.id.drop_down);

                POSUtil.showKeyboard(valueTextView); // show keyboard as soon as popup shows up
                setPriceInputMDOnClickListener(position, priceList);
            }


        });

        //lyx set up listener on minus button of sale item
        rvSwipeAdapterForPurchaseItemViewInPurchase.setQtyMinusClickListener(new ClickListener() {

            @Override
            public void onClick(int position) {
                SalesAndPurchaseItem currentSaleItem = purchaseItemViewInSaleList.get(position);
                int qty = currentSaleItem.getQty();
                if (qty > 1) {
                    qty -= 1;
                    currentSaleItem.setQty(qty);
                    currentSaleItem.setTotalPrice(qty * currentSaleItem.getPrice());
                    rvSwipeAdapterForPurchaseItemViewInPurchase.notifyDataSetChanged();
                    updateViewData();
                }

                if (!updateList.contains(purchaseItemViewInSaleList.get(position))) {
                    updateList.add(purchaseItemViewInSaleList.get(position));
                }
            }
        });

        //lyx set up listener on plus button of sale item
        rvSwipeAdapterForPurchaseItemViewInPurchase.setQtyPlusClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                SalesAndPurchaseItem currentSaleItem = purchaseItemViewInSaleList.get(postion);
                int qty = currentSaleItem.getQty() + 1;
                currentSaleItem.setQty(qty);
                currentSaleItem.setTotalPrice(qty * currentSaleItem.getPrice());
                rvSwipeAdapterForPurchaseItemViewInPurchase.notifyDataSetChanged();
                updateViewData();
                if (!updateList.contains(purchaseItemViewInSaleList.get(postion))) {
                    updateList.add(purchaseItemViewInSaleList.get(postion));
                }
            }

        });

        discountTextViewBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                //addDiscountMaterialDialog.show();
                //                discountListMaterialDialog.show();
                String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.discount}).getString(0);
                discountDialogFragment = DiscountDialogFragmentForPurchase.newInstance(title);
                discountDialogFragment.setTargetFragment(AddEditNewPurchaseFragment.this, 1);
                if (discount != null) {
                    discountDialogFragment.setDefaultDiscount(discount);
                }
                discountDialogFragment.show(getFragmentManager().beginTransaction(), "dialog");

            }
        });

        payTextViewBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                setDataForPayMd();

                if (purchaseItemViewInSaleList.size() < 1) {

                    //alertMaterialDialog.show();

                } else {

                    POSUtil.showKeyboard(paidAmountTextInputEditTextPayMd);

                    showPaymentFragment();

                }

            }
        });

        payMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                POSUtil.hideKeyboard(paidAmountTextInputEditTextPayMd, context);
            }
        });



        settingOnClickForEditSaleItem();

        settingOnClickForAddSaleItemDialog();

        settingOnClickForPayMd();

        settingOnClickForDiscountMD();

    }

    private void addItem() {
        foc = false;
        isEdit = false;

        purchaseItemInPurchase = new SalesAndPurchaseItem();

        //clearInputAddItemMaterialDialog();

        //addItemMaterialDialog.show();
        if (itemOrCodeAutocomplete.getText().toString().trim().length() > 0) {
            List<StockItem> stockItems = stockManager.getActiveStocksByNameOrCode(0, 10000, itemOrCodeAutocomplete.getText().toString().trim());

            if (stockItems.isEmpty()) {

            } else if (stockItems.size() == 1) {
                setItemValue(stockItems.get(0));

                if (purchaseItemViewInSaleList.contains(purchaseItemInPurchase)) {

                    SalesAndPurchaseItem salesAndPurchaseItem = purchaseItemViewInSaleList.get(purchaseItemViewInSaleList.indexOf(purchaseItemInPurchase));
                    salesAndPurchaseItem.setQty(salesAndPurchaseItem.getQty() + 1);
                    salesAndPurchaseItem.setTotalPrice((salesAndPurchaseItem.getQty() * salesAndPurchaseItem.getPrice()));
                    updateList.add(salesAndPurchaseItem);
                } else {
                    purchaseItemViewInSaleList.add(0, purchaseItemInPurchase);
                    updateList.add(purchaseItemInPurchase);
                }


                refreshRecyclerView();

                addItemMaterialDialog.dismiss();

                updateViewData();


                foc = false;

                purchaseItemInPurchase = new SalesAndPurchaseItem();

                //clearInputAddItemMaterialDialog();

                stockAutoCompleteView = new StockItem();
                itemOrCodeAutocomplete.setText("");


            } else {
                Log.e("SIZE STOCK", stockItems.size() + " SS");
                // stockChooserAdapter.setFilterNameList(stockItems);
                // stockChooserDialog.show();

            }
        }

    }

    public void setItemValue(StockItem stockAutoCompleteView) {
        //purchaseItemInPurchase.setId(stockAutoCompleteView.getId());
        purchaseItemInPurchase.setItemName(stockAutoCompleteView.getName());

        purchaseItemInPurchase.setStockCode(stockAutoCompleteView.getCodeNo());

        purchaseItemInPurchase.setQty(qty);

        purchaseItemInPurchase.setPrice(stockAutoCompleteView.getPurchasePrice());

        purchaseItemInPurchase.setDiscountAmount(0.0);

        Stock stock = stockManager.findStockByStockCode(purchaseItemInPurchase.getStockCode());
        purchaseItemInPurchase.setStockID(stockAutoCompleteView.getId());

        if (stock.getStockCode() == null) {

            //   purchaseItemInPurchase.setStockID(stockBusiness.addQuickStockSetupPurchase(purchaseItemInPurchase.getItemName(), purchaseItemInPurchase.getStockCode(),
            //       purchaseItemInPurchase.getPrice()));

        }

        purchaseItemInPurchase.setTotalPrice(calculatePrice(purchaseItemInPurchase.getPrice(), qty, purchaseItemInPurchase.getDiscountAmount()));

    }



    public boolean checkPayValidation() {

        boolean status = true;

        if (POSUtil.isTextViewEmpty(paidAmountTextInputEditTextPayMd)) {
            status = false;
            String enterPaidAmt = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_paid_amount}).getString(0);
            paidAmountTextInputEditTextPayMd.setError(enterPaidAmt);

        } else {
            paidAmount = Double.valueOf(paidAmountTextInputEditTextPayMd.getText().toString());
            if (paidAmount > totalAmount) {
                paidAmount = totalAmount;
            }
        }


        return status;

    }

    private void clearInputAndOpenNewVoucher() {

        purchaseInvoiceNo = purchaseManager.getPurchaseInvoiceNo();

        purchaseIdTextView.setText(purchaseInvoiceNo);

        supplierTextInputEditText.setText(null);

        phoneNoEditText.setText(null);

        purchaseItemViewInSaleList = new ArrayList<>();

        rvSwipeAdapterForPurchaseItemViewInPurchase.clearData();

        paidAmount = 0.0;

        tax = settingManager.getDefaultTax();

        voucherDiscount = 0.0;

        updateViewData();
        paidAmountTextInputEditTextPayMd.setText("0");

        isPay = false;
        if (POSUtil.isTabetLand(getContext())) {
            toggleView();
        }

    }

    private void setDataForPayMd() {

        supplierPayTextView.setText(supplierBussinessName);

        paidAmountTextInputEditTextPayMd.setError(null);

        totalAmountTextInputEditTextPayMd.setEnabled(false);

        totalAmountTextInputEditTextPayMd.setText(POSUtil.doubleToString(totalAmount));

        if (isPurEdit) {
            paidAmountTextInputEditTextPayMd.setText(POSUtil.doubleToString(paidAmount));
        } else {
            paidAmountTextInputEditTextPayMd.setText(POSUtil.doubleToString(totalAmount));
        }

        paidAmountTextInputEditTextPayMd.setSelectAllOnFocus(true);


    }


    private void settingOnClickForPayMd() {

        paidAmountTextInputEditTextPayMd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!POSUtil.isTextViewEmpty(paidAmountTextInputEditTextPayMd)) {
                    Double changeAmount = Double.parseDouble(paidAmountTextInputEditTextPayMd.getText().toString().replace(",", "")) - totalAmount;

                    if (changeAmount > 0) {
                        purchaseOustandting.setText(POSUtil.doubleToString((changeAmount)));
                    } else {
                        purchaseOustandting.setText("0");

                    }
                }
            }
        });

    }

    private void settingOnClickForDiscountMD() {

        saveDiscountBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!POSUtil.isTextViewEmpty(discountTextInputEditTextMd)) {
                    voucherDiscount = Double.parseDouble(discountTextInputEditTextMd.getText().toString().trim());
                } else {
                    voucherDiscount = 0.0;
                }

                updateViewData();
                addDiscountMaterialDialog.dismiss();

            }
        });

    }

    private void settingOnClickForEditSaleItem() {

        stockListBtnEditItemMd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stockListMaterialDialogEdit.show();
            }
        });

        rvAdapterforStockListMaterialDialogEdit.setmItemClickListener(new RVAdapterforStockListMaterialDialog.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                assigValuesForitemEdit(stockItemList.get(position));

                editPurchaseItem(stockItemList.get(position));

                stockListMaterialDialogEdit.dismiss();

                itemNameTextInputEditTextEditItemMd.clearFocus();
                qtyTextInputEditTextEditItemMd.requestFocus();

            }
        });

        //        minusQtyAppCompatImageButtonEditItemMd.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //
        //                if (qtyTextInputEditTextEditItemMd.getText().toString().trim().length() > 0) {
        //
        //                    qty = Integer.parseInt(qtyTextInputEditTextEditItemMd.getText().toString());
        //
        //                }
        //
        //                if (qty > 0) {
        //
        //                    qty--;
        //
        //                    qtyTextInputEditTextEditItemMd.setText(Integer.toString(qty));
        //
        //                    qtyTextInputEditTextEditItemMd.setSelection(qtyTextInputEditTextEditItemMd.getText().length());
        //
        //                }
        //
        //            }
        //        });

        //        plusQtyAppCompatImageButtonEditItemMd.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //
        //                if (qtyTextInputEditTextEditItemMd.getText().toString().trim().length() > 0) {
        //
        //                    qty = Integer.parseInt(qtyTextInputEditTextEditItemMd.getText().toString());
        //
        //                }
        //
        //                if (qty >= 0) {
        //
        //                    qty++;
        //
        //                    qtyTextInputEditTextEditItemMd.setText(Integer.toString(qty));
        //
        //                    qtyTextInputEditTextEditItemMd.setSelection(qtyTextInputEditTextEditItemMd.getText().length());
        //
        //                }
        //
        //            }
        //        });

        focSwitchCompatEditItemMd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                foc = isChecked;

                if (foc) {

                    discountTextInputEditTextEditItemMd.setText("100%");

                } else {

                    discountTextInputEditTextEditItemMd.setText(null);

                }

            }
        });

        saveMdButtonEditItemMd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                getValuesFromEditSaleItemDialog();

                rvSwipeAdapterForPurchaseItemViewInPurchase.notifyItemChanged(editposition);

                editItemMaterialDialog.dismiss();

                if (!updateList.contains(purchaseItemViewInSaleList.get(editposition))) {

                    updateList.add(purchaseItemViewInSaleList.get(editposition));

                }

                updateViewData();

            }
        });

        discountTextInputEditTextAddItemMd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!POSUtil.isTextViewEmpty(discountTextInputEditTextAddItemMd)) {
                    if (discountTextInputEditTextAddItemMd.getText().toString().trim().contains("%")) {

                    } else {
                        purchaseItemInPurchase.setDiscountAmount(Double.parseDouble(discountTextInputEditTextAddItemMd.getText().toString().trim()));

                    }

                }
            }
        });
    }

    private void editPurchaseItem(StockItem stockItem) {

        purchaseItemViewInSaleList.get(editposition).setDiscountAmount(0.0);

        purchaseItemViewInSaleList.get(editposition).setBarcode(stockItem.getBarcode());

        purchaseItemViewInSaleList.get(editposition).setStockCode(stockItem.getCodeNo());

        purchaseItemViewInSaleList.get(editposition).setItemName(stockItem.getName());

        purchaseItemViewInSaleList.get(editposition).setQty(1);

        qty = 1;

        purchaseItemViewInSaleList.get(editposition).setStockID(stockItem.getId());
    }

    private void editPurchaseItem(StockAutoComplete stockView) {

        purchaseItemViewInSaleList.get(editposition).setDiscountAmount(0.0);

        purchaseItemViewInSaleList.get(editposition).setBarcode(stockView.getBarcode());

        purchaseItemViewInSaleList.get(editposition).setStockCode(stockView.getCodeNo());

        purchaseItemViewInSaleList.get(editposition).setItemName(stockView.getItemName());

        purchaseItemViewInSaleList.get(editposition).setQty(1);

        qty = 1;

        purchaseItemViewInSaleList.get(editposition).setStockID(stockView.getId());
    }


    public void settingOnClickForAddSaleItemDialog() {

        stockListBtnAddItemMd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stockListMaterialDialog.show();
            }
        });

        rvAdapterforStockListMaterialDialog.setmItemClickListener(new RVAdapterforStockListMaterialDialog.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                assigValuesForitemAdd(stockItemList.get(position));

                addPurchaseItem(stockItemList.get(position));

                stockListMaterialDialog.dismiss();

                itemNameTextInputEditTextAddItemMd.clearFocus();

                //                qtyTextInputEditTextAddItemMd.requestFocus();

            }
        });

        //        minusQtyAppCompatImageButtonAddItemMd.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //
        //                if (qtyTextInputEditTextAddItemMd.getText().toString().trim().length() > 0) {
        //
        //                    qty = Integer.parseInt(qtyTextInputEditTextAddItemMd.getText().toString());
        //
        //                }
        //
        //                if (qty > 0) {
        //
        //                    qty--;
        //
        //                    qtyTextInputEditTextAddItemMd.setText(Integer.toString(qty));
        //
        //                    qtyTextInputEditTextAddItemMd.setSelection(qtyTextInputEditTextAddItemMd.getText().length());
        //
        //                }
        //
        //            }
        //        });

        //        plusQtyAppCompatImageButtonAddItemMd.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //
        //                if (qtyTextInputEditTextAddItemMd.getText().toString().trim().length() > 0) {
        //
        //                    qty = Integer.parseInt(qtyTextInputEditTextAddItemMd.getText().toString());
        //
        //                }
        //
        //                if (qty >= 0) {
        //
        //                    qty++;
        //
        //                    qtyTextInputEditTextAddItemMd.setText(Integer.toString(qty));
        //
        //                    qtyTextInputEditTextAddItemMd.setSelection(qtyTextInputEditTextAddItemMd.getText().length());
        //
        //                }
        //
        //            }
        //        });


        focSwitchCompatAddItemMd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                foc = isChecked;

                if (foc) {

                    discountTextInputEditTextAddItemMd.setText("100%");

                } else {

                    discountTextInputEditTextAddItemMd.setText(null);

                }

            }
        });

        saveMdButtonAddItemMd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit) {

                    getValuesFromEditSaleItemDialog();

                    rvSwipeAdapterForPurchaseItemViewInPurchase.notifyItemChanged(editposition);

                    editItemMaterialDialog.dismiss();

                    if (!updateList.contains(purchaseItemViewInSaleList.get(editposition))) {

                        updateList.add(purchaseItemViewInSaleList.get(editposition));

                    }
                    clearInputAddItemMaterialDialog();

                    updateViewData();
                } else {
                    if (checkVaildationFroAddPurchaseItem()) {

                        getValuesFromAddPurchaseItemDialog();

                        purchaseItemViewInSaleList.add(purchaseItemInPurchase);

                        refreshRecyclerView();

                        addItemMaterialDialog.dismiss();

                        updateViewData();

                        updateList.add(purchaseItemInPurchase);

                        foc = false;

                        purchaseItemInPurchase = new SalesAndPurchaseItem();

                        clearInputAddItemMaterialDialog();

                    }
                }


            }
        });

        discountTextInputEditTextAddItemMd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!POSUtil.isTextViewEmpty(discountTextInputEditTextAddItemMd)) {
                    if (discountTextInputEditTextAddItemMd.getText().toString().trim().contains("%")) {

                    } else {
                        purchaseItemInPurchase.setDiscountAmount(Double.parseDouble(discountTextInputEditTextAddItemMd.getText().toString().trim()));

                    }
                }
            }
        });

    }

    public void getValuesFromAddPurchaseItemDialog() {

        purchaseItemInPurchase.setQty(qty);

        //        purchaseItemInPurchase.setPrice(Double.parseDouble(priceTextInputEditTextAddItemMd.getText().toString().trim()));

        purchaseItemInPurchase.setItemName(itemNameTextInputEditTextAddItemMd.getText().toString().trim());

        purchaseItemInPurchase.setStockCode(itemCodeTextInputEditTextAddItemMd.getText().toString().trim());

        Stock stock = stockManager.findStockByStockCode(purchaseItemInPurchase.getStockCode());

        if (!POSUtil.isTextViewEmpty(discountTextInputEditTextAddItemMd)) {
            purchaseItemInPurchase.setDiscountAmount(Double.parseDouble(discountTextInputEditTextAddItemMd.getText().toString().trim()));
        } else {
            purchaseItemInPurchase.setDiscountAmount(0.0);
        }

        if (stock.getStockCode() == null) {

            //  purchaseItemInPurchase.setStockID(stockBusiness.addQuickStockSetupPurchase(purchaseItemInPurchase.getItemName(), purchaseItemInPurchase.getStockCode(),
            //        purchaseItemInPurchase.getPrice()));

        }

        purchaseItemInPurchase.setTotalPrice(calculatePrice(purchaseItemInPurchase.getPrice(), qty, purchaseItemInPurchase.getDiscountAmount()));


    }


    public void getValuesFromEditSaleItemDialog() {
        purchaseItemViewInSaleList.get(editposition).setQty(qty);
        purchaseItemViewInSaleList.get(editposition).setPrice(Double.parseDouble(priceTextInputEditTextEditItemMd.getText().toString().trim()));
        purchaseItemViewInSaleList.get(editposition).setTotalPrice(calculatePrice(purchaseItemViewInSaleList.get(editposition).getPrice(), qty, purchaseItemViewInSaleList.get(editposition).getDiscountAmount()));

    }

    public boolean checkVaildationFroAddPurchaseItem() {

        boolean status = true;
        if (!POSUtil.isTabetLand(context)) {
            if (itemOrCodeAutocomplete.getText().toString().trim().length() < 1) {
                status = false;
                String itemCode = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_itemcode_or_choose}).getString(0);
                itemOrCodeAutocomplete.setError(itemCode);
            }

        }

        if (itemCodeTextInputEditTextAddItemMd.getText().toString().trim().length() < 1) {
            status = false;
            String enterItemcode = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_itemcode_or_choose}).getString(0);
            itemCodeTextInputEditTextAddItemMd.setError(enterItemcode);
        }

        if (itemNameTextInputEditTextAddItemMd.getText().toString().trim().length() < 1) {

            status = false;

            String enterName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_name_or_choose}).getString(0);
            itemNameTextInputEditTextAddItemMd.setError(enterName);

        }


        //        if (priceTextInputEditTextAddItemMd.getText().toString().trim().length() < 1) {
        //
        //            status = false;
        //
        //            String enterPrice=context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_price_or_choose}).getString(0);
        //            priceTextInputEditTextAddItemMd.setError(enterPrice);
        //
        //        }
        //        if (qtyTextInputEditTextAddItemMd.getText().toString().trim().length() < 1 || Integer.parseInt(qtyTextInputEditTextAddItemMd.getText().toString().trim()) < 1) {
        //
        //            status = false;
        //
        //            String enterQty=context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_qty_greater_than_0}).getString(0);
        //            qtyTextInputEditTextAddItemMd.setError(enterQty);
        //
        //        }

      /*  if (itemOrCodeAutocomplete.getText().toString().trim().length() < 1 ) {

            status = false;

            String enterQty=context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_itemcode_or_choose}).getString(0);
            itemOrCodeAutocomplete.setError(enterQty);

        }*/

        return status;

    }

    public void assigValuesForitemAdd(StockItem stockItem) {


        if (!itemCodeTextInputEditTextAddItemMd.getText().toString().trim().equalsIgnoreCase(stockItem.getCodeNo())) {

            itemCodeTextInputEditTextAddItemMd.setText(stockItem.getCodeNo());

        }
        // itemCodeTextInputEditTextAddItemMd.setSelection(stockItem.getCodeNo().length());

        itemNameTextInputEditTextAddItemMd.setText(stockItem.getName());

        // itemNameTextInputEditTextAddItemMd.setSelection(stockItem.getName().length());

        //        qtyTextInputEditTextAddItemMd.setText("1");

        foc = false;

        //        priceTextInputEditTextAddItemMd.setText(POSUtil.doubleToString(stockItem.getPurchasePrice()));
        //
        //        priceTextInputEditTextAddItemMd.setSelection(POSUtil.doubleToString(stockItem.getPurchasePrice()).length());

        focSwitchCompatAddItemMd.setChecked(false);

        discountTextInputEditTextAddItemMd.setText(null);


    }


    public void assigValuesForitemAdd(StockAutoComplete stockView) {

        if (!itemCodeTextInputEditTextAddItemMd.getText().toString().trim().equalsIgnoreCase(stockView.getCodeNo())) {

            itemCodeTextInputEditTextAddItemMd.setText(stockView.getCodeNo());

        }

        //itemCodeTextInputEditTextAddItemMd.setSelection(itemCodeTextInputEditTextAddItemMd.getText().toString().length());

        itemNameTextInputEditTextAddItemMd.setText(stockView.getItemName());

        //itemNameTextInputEditTextAddItemMd.setSelection(stockItem.getItemName().length());

        //        qtyTextInputEditTextAddItemMd.setText("1");

        foc = false;

        //        priceTextInputEditTextAddItemMd.setText(POSUtil.doubleToString(stockItem.getPurchasePrice()));
        //
        //        priceTextInputEditTextAddItemMd.setSelection(POSUtil.doubleToString(stockItem.getPurchasePrice()).length());

        focSwitchCompatAddItemMd.setChecked(false);

        discountTextInputEditTextAddItemMd.setText(null);

        List<StockImage> stockImages = stockManager.getImagesByStockID(stockView.getId());

        if (stockImages.isEmpty()) {
            img.setImageResource(R.drawable.add_image);
        } else {
            img.setImageBitmap(stockImages.get(0).getImageBitmap());
        }

    }


    public void assigValuesForitemEdit(StockItem stockItem) {

        if (!itemCodeTextInputEditTextEditItemMd.getText().toString().trim().equalsIgnoreCase(stockItem.getCodeNo())) {

            itemCodeTextInputEditTextEditItemMd.setText(stockItem.getCodeNo());

        }

        //itemCodeTextInputEditTextEditItemMd.setSelection(itemCodeTextInputEditTextAddItemMd.getText().toString().length());

        itemNameTextInputEditTextEditItemMd.setText(stockItem.getName());

        // itemNameTextInputEditTextEditItemMd.setSelection(stockItem.getName().length());

        qtyTextInputEditTextEditItemMd.setText("1");

        foc = false;

        priceTextInputEditTextEditItemMd.setText(POSUtil.doubleToString(stockItem.getPurchasePrice()));

        priceTextInputEditTextEditItemMd.setSelection(POSUtil.doubleToString(stockItem.getPurchasePrice()).length());

        focSwitchCompatEditItemMd.setChecked(false);

        discountTextInputEditTextEditItemMd.setText(null);

    }


    public void assigValuesForitemEdit(StockAutoComplete stockView) {

        if (!itemCodeTextInputEditTextEditItemMd.getText().toString().trim().equalsIgnoreCase(stockView.getCodeNo())) {

            itemCodeTextInputEditTextEditItemMd.setText(stockView.getCodeNo());

        }

        //itemCodeTextInputEditTextEditItemMd.setSelection(itemCodeTextInputEditTextEditItemMd.getText().toString().length());

        itemNameTextInputEditTextEditItemMd.setText(stockView.getItemName());

        // itemNameTextInputEditTextEditItemMd.setSelection(stockItem.getItemName().length());

        qtyTextInputEditTextEditItemMd.setText("1");

        foc = false;

        priceTextInputEditTextEditItemMd.setText(POSUtil.doubleToString(stockView.getPurchasePrice()));

        priceTextInputEditTextEditItemMd.setSelection(POSUtil.doubleToString(stockView.getPurchasePrice()).length());

        focSwitchCompatEditItemMd.setChecked(false);

        discountTextInputEditTextEditItemMd.setText(null);

        List<StockImage> stockImages = stockManager.getImagesByStockID(stockView.getId());

        if (stockImages.isEmpty()) {
            img.setImageResource(R.drawable.add_image);
        } else {
            img.setImageBitmap(stockImages.get(0).getImageBitmap());
        }


    }

    private void loadUI() {
        holdBtn = mainLayout.findViewById(R.id.hold);
        addNewStock = (ImageButton) mainLayout.findViewById(R.id.add_new_stock);
        barcodeBtn = (ImageButton) mainLayout.findViewById(R.id.barcode);

        supplierImageButton = (AppCompatImageButton) mainLayout.findViewById(R.id.supplier_image_button);
        String pleaseWait = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);

        String connecting = context.getTheme().obtainStyledAttributes(new int[]{R.attr.connecting}).getString(0);

        connectHud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(pleaseWait)
                .setDetailsLabel(connecting)
                .setWindowColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);
        statusTextView = (TextView) mainLayout.findViewById(R.id.bt_status);

        printDeviceNameTextView = (TextView) mainLayout.findViewById(R.id.device_name);

        saleDetailPrint = (FrameLayout) mainLayout.findViewById(R.id.sale_detail_print);
        payLinearLayout = (LinearLayout) mainLayout.findViewById(R.id.pay_view);

        addItemLinearLayout = (LinearLayout) mainLayout.findViewById(R.id.add_item_view);

        keyBoardView = (KeyBoardView) mainLayout.findViewById(R.id.key_board);

        itemOrCodeAutocomplete = (AutoCompleteTextView) mainLayout.findViewById(R.id.item_name_or_code);

        saleDetailPrint = (FrameLayout) mainLayout.findViewById(R.id.sale_detail_print);

        phoneNoEditText = (EditText) mainLayout.findViewById(R.id.phone_no);

        //  supplierTextInputEditText = (AppCompatAutoCompleteTextView) mainLayout.findViewById(R.id.supplier_name);

        barCodeTIET = (EditText) mainLayout.findViewById(R.id.bar_code_TIET);

        barCodeBtn = (ImageButton) mainLayout.findViewById(R.id.bar_code_btn);

        dateLinearLayout = (LinearLayout) mainLayout.findViewById(R.id.purchase_date_in_new_purchase_ll);

        purchaseItemRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.purchase_item_list_rv);

        purchaseIdTextView = (TextView) mainLayout.findViewById(R.id.purchase_id_in_new_purcahse_tv);

        dateTextView = (TextView) mainLayout.findViewById(R.id.purchase_date_in_new_purchase_tv);

        taxAmtTextView = (TextView) mainLayout.findViewById(R.id.tax_amt_in_new_purchase_tv);

        taxRateTextView = (TextView) mainLayout.findViewById(R.id.tax_rate_tv);

        discountTextView = (TextView) mainLayout.findViewById(R.id.discount_in_new_purchase_tv);

        subtotalTextView = (TextView) mainLayout.findViewById(R.id.subtotal_in_new_purchase_tv);

        totalTextView = (TextView) mainLayout.findViewById(R.id.total_in_new_purchase_tv);

        discountTextViewBtn = (ImageButton) mainLayout.findViewById(R.id.discount_in_new_purchase_tv_btn);

        taxTextViewBtn = (AppCompatImageButton) mainLayout.findViewById(R.id.tax_in_new_purchase_tv_btn);

        addpurchaseItemBtn = (Button) mainLayout.findViewById(R.id.add_item_in_new_purchase_btn);

        payTextViewBtn = (TextView) mainLayout.findViewById(R.id.pay_in_new_purchase);

        loadUIFromAddItemMaterialDialog();

        loadUIFromEditItemMaterialDialog();

        discountTextInputEditTextMd = (EditText) addDiscountMaterialDialog.findViewById(R.id.add_vocher_disc_in_sale_md);

        saveDiscountBtn = (Button) addDiscountMaterialDialog.findViewById(R.id.save);

        cancelDiscountBtn = (Button) addDiscountMaterialDialog.findViewById(R.id.cancel);

        if (!POSUtil.isTabetLand(getContext())) {
            totalAmountTextInputEditTextPayMd = (EditText) payMaterialDialog.findViewById(R.id.total_amount_in_purcahse_change);

            paidAmountTextInputEditTextPayMd = (EditText) payMaterialDialog.findViewById(R.id.paid_amount_in_purchase_change);

            purchaseOustandting = (EditText) payMaterialDialog.findViewById(R.id.oustanding_in_purchase_change);

            supplierTextInputEditText = (TextView) payMaterialDialog.findViewById(R.id.supplier_pay);

            saveTransaction = (Button) payMaterialDialog.findViewById(R.id.save);

            cancelTransaction = (Button) payMaterialDialog.findViewById(R.id.cancel);

            supplierPayTextView = (TextView) payMaterialDialog.findViewById(R.id.supplier_pay);

            supplierNameTextView = (AutoCompleteTextView) payMaterialDialog.findViewById(R.id.supplier_pay);
            supplierPhoneNoEditText = (EditText) payMaterialDialog.findViewById(R.id.phone_no_pay);
        } else {

            supplierNameTextView = (AutoCompleteTextView) mainLayout.findViewById(R.id.supplier_pay);
            supplierPhoneNoEditText = (EditText) mainLayout.findViewById(R.id.phone_no_pay);
            supplierTextInputEditText = (AutoCompleteTextView) supplierMaterialDialog.findViewById(R.id.customer_name_edit_text);

            totalAmountTextInputEditTextPayMd = (EditText) mainLayout.findViewById(R.id.total_amount_in_purcahse_change);

            paidAmountTextInputEditTextPayMd = (EditText) mainLayout.findViewById(R.id.paid_amount_in_purchase_change);

            purchaseOustandting = (EditText) mainLayout.findViewById(R.id.oustanding_in_purchase_change);

            saveTransaction = (Button) mainLayout.findViewById(R.id.pay_save);

            cancelTransaction = (Button) mainLayout.findViewById(R.id.cancel);

            supplierPayTextView = (TextView) mainLayout.findViewById(R.id.supplier_pay);

        }


    }


    public void loadAddItemDialog() {

        TypedArray addPurchaseItem = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_purchase_item});
        //TypedArray save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        addItemMaterialDialog = new MaterialDialog.Builder(context).

                customView(R.layout.add_item_sale_purchase, true).title(addPurchaseItem.getString(0)).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").

                // positiveText(save.getString(0)).

                // negativeText(cancel.getString(0)).

                        build();

    }

    public void loadEditItemDialog() {

        TypedArray editPurchaseItem = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_purchase_item});
        //TypedArray save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        editItemMaterialDialog = new MaterialDialog.Builder(context).

                customView(R.layout.add_item_sale_purchase, true).title(editPurchaseItem.getString(0)).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").

                // positiveText(save.getString(0)).

                // negativeText(cancel.getString(0)).

                        build();

    }

    public void clearInputAddItemMaterialDialog() {

        itemCodeTextInputEditTextAddItemMd.setError(null);

        itemCodeTextInputEditTextAddItemMd.setText(null);

        itemNameTextInputEditTextAddItemMd.setText(null);

        itemNameTextInputEditTextAddItemMd.setError(null);

        //        qtyTextInputEditTextAddItemMd.setText("0");

        //        priceTextInputEditTextAddItemMd.setText(null);
        //
        //        priceTextInputEditTextAddItemMd.setError(null);

        //        qtyTextInputEditTextAddItemMd.setError(null);

        focSwitchCompatAddItemMd.setChecked(false);

        discountTextInputEditTextAddItemMd.setText(null);

    }

    public void setInputEditItemMaterialDialog() {

        itemCodeTextInputEditTextEditItemMd.setText(purchaseItemViewInSaleList.get(editposition).getStockCode());

        itemNameTextInputEditTextEditItemMd.setText(purchaseItemViewInSaleList.get(editposition).getItemName());


        qtyTextInputEditTextEditItemMd.setText(Integer.toString(purchaseItemViewInSaleList.get(editposition).getQty()));

        priceTextInputEditTextEditItemMd.setText(POSUtil.doubleToString((purchaseItemViewInSaleList.get(editposition).getPrice())));

        focSwitchCompatEditItemMd.setChecked(false);

        discountTextInputEditTextEditItemMd.setText(POSUtil.doubleToString(purchaseItemViewInSaleList.get(editposition).getDiscountAmount()));

    }

    public void loadUIFromAddItemMaterialDialog() {

        if (!POSUtil.isTabetLand(context)) {

            img = (ImageButton) addItemMaterialDialog.findViewById(R.id.img);

            stockListBtnAddItemMd = (AppCompatImageButton) addItemMaterialDialog.findViewById(R.id.stock_list_in_add_sale_item_btn);

            itemCodeTextInputEditTextAddItemMd = (TextView) addItemMaterialDialog.findViewById(R.id.code_no_in_sale_purchase_TIET);

            itemNameTextInputEditTextAddItemMd = (TextView) addItemMaterialDialog.findViewById(R.id.item_name_in_sale_purchase_TIET);

            //            qtyTextInputEditTextAddItemMd = (EditText) addItemMaterialDialog.findViewById(R.id.qty_in_sale_purchase_TIET);
            //            minusQtyAppCompatImageButtonAddItemMd = (AppCompatImageButton) addItemMaterialDialog.findViewById(R.id.minus_qty_in_sale_purchase_ImgBtn);

            //            plusQtyAppCompatImageButtonAddItemMd = (AppCompatImageButton) addItemMaterialDialog.findViewById(R.id.plus_qty_in_sale_purchase_ImgBtn);

            //            priceTextInputEditTextAddItemMd = (EditText) addItemMaterialDialog.findViewById(R.id.sale_price_in_sale_purchase_TIET);

            focSwitchCompatAddItemMd = (SwitchCompat) addItemMaterialDialog.findViewById(R.id.foc_in_sale_purchase_SC);

            discountTextInputEditTextAddItemMd = (EditText) addItemMaterialDialog.findViewById(R.id.item_discount_in_sale_purchase_TIET);

            saveMdButtonAddItemMd = (Button) addItemMaterialDialog.findViewById(R.id.save);

            cancelMdButtonAddItemMd = (Button) addItemMaterialDialog.findViewById(R.id.cancel);

        } else {

            img = (ImageButton) mainLayout.findViewById(R.id.img);

            stockListBtnAddItemMd = (AppCompatImageButton) mainLayout.findViewById(R.id.stock_list_in_add_sale_item_btn);

            itemCodeTextInputEditTextAddItemMd = (TextView) mainLayout.findViewById(R.id.code_no_in_sale_purchase_TIET);

            itemNameTextInputEditTextAddItemMd = (TextView) mainLayout.findViewById(R.id.item_name_in_sale_purchase_TIET);

            //            qtyTextInputEditTextAddItemMd = (EditText) mainLayout.findViewById(R.id.qty_in_sale_purchase_TIET);
            //            minusQtyAppCompatImageButtonAddItemMd = (AppCompatImageButton) mainLayout.findViewById(R.id.minus_qty_in_sale_purchase_ImgBtn);

            //            plusQtyAppCompatImageButtonAddItemMd = (AppCompatImageButton) mainLayout.findViewById(R.id.plus_qty_in_sale_purchase_ImgBtn);

            //            priceTextInputEditTextAddItemMd = (EditText) mainLayout.findViewById(R.id.sale_price_in_sale_purchase_TIET);

            focSwitchCompatAddItemMd = (SwitchCompat) mainLayout.findViewById(R.id.foc_in_sale_purchase_SC);

            discountTextInputEditTextAddItemMd = (EditText) mainLayout.findViewById(R.id.item_discount_in_sale_purchase_TIET);

            saveMdButtonAddItemMd = (Button) mainLayout.findViewById(R.id.save);

            cancelMdButtonAddItemMd = (Button) mainLayout.findViewById(R.id.cancel);
        }


    }

    public void loadUIFromEditItemMaterialDialog() {

        if (!POSUtil.isTabetLand(context)) {

            img = (ImageButton) editItemMaterialDialog.findViewById(R.id.img);

            stockListBtnEditItemMd = (AppCompatImageButton) editItemMaterialDialog.findViewById(R.id.stock_list_in_add_sale_item_btn);

            itemCodeTextInputEditTextEditItemMd = (TextView) editItemMaterialDialog.findViewById(R.id.code_no_in_sale_purchase_TIET);

            itemNameTextInputEditTextEditItemMd = (TextView) editItemMaterialDialog.findViewById(R.id.item_name_in_sale_purchase_TIET);

            //            qtyTextInputEditTextEditItemMd = (EditText) editItemMaterialDialog.findViewById(R.id.qty_in_sale_purchase_TIET);

            //            minusQtyAppCompatImageButtonEditItemMd = (AppCompatImageButton) editItemMaterialDialog.findViewById(R.id.minus_qty_in_sale_purchase_ImgBtn);

            //            plusQtyAppCompatImageButtonEditItemMd = (AppCompatImageButton) editItemMaterialDialog.findViewById(R.id.plus_qty_in_sale_purchase_ImgBtn);

            //            priceTextInputEditTextEditItemMd = (EditText) editItemMaterialDialog.findViewById(R.id.sale_price_in_sale_purchase_TIET);

            focSwitchCompatEditItemMd = (SwitchCompat) editItemMaterialDialog.findViewById(R.id.foc_in_sale_purchase_SC);

            discountTextInputEditTextEditItemMd = (EditText) editItemMaterialDialog.findViewById(R.id.item_discount_in_sale_purchase_TIET);

            saveMdButtonEditItemMd = (Button) editItemMaterialDialog.findViewById(R.id.save);

            cancelMdButtonEditItemMd = (Button) editItemMaterialDialog.findViewById(R.id.cancel);
        } else {


            img = (ImageButton) mainLayout.findViewById(R.id.img);

            stockListBtnEditItemMd = (AppCompatImageButton) mainLayout.findViewById(R.id.stock_list_in_add_sale_item_btn);

            itemCodeTextInputEditTextEditItemMd = (TextView) mainLayout.findViewById(R.id.code_no_in_sale_purchase_TIET);

            itemNameTextInputEditTextEditItemMd = (TextView) mainLayout.findViewById(R.id.item_name_in_sale_purchase_TIET);

            //            qtyTextInputEditTextEditItemMd = (EditText) mainLayout.findViewById(R.id.qty_in_sale_purchase_TIET);

            //            minusQtyAppCompatImageButtonEditItemMd = (AppCompatImageButton) mainLayout.findViewById(R.id.minus_qty_in_sale_purchase_ImgBtn);

            //            plusQtyAppCompatImageButtonEditItemMd = (AppCompatImageButton) mainLayout.findViewById(R.id.plus_qty_in_sale_purchase_ImgBtn);

            //            priceTextInputEditTextEditItemMd = (EditText) mainLayout.findViewById(R.id.sale_price_in_sale_purchase_TIET);

            focSwitchCompatEditItemMd = (SwitchCompat) mainLayout.findViewById(R.id.foc_in_sale_purchase_SC);

            discountTextInputEditTextEditItemMd = (EditText) mainLayout.findViewById(R.id.item_discount_in_sale_purchase_TIET);

            saveMdButtonEditItemMd = (Button) mainLayout.findViewById(R.id.save);

            cancelMdButtonEditItemMd = (Button) mainLayout.findViewById(R.id.cancel);
        }


    }

    public void buildStockListDialog() {

        rvAdapterforStockListMaterialDialog = new RVAdapterforStockListMaterialDialog(stockItemList);

        stockListMaterialDialog = new MaterialDialog.Builder(context).

                adapter(rvAdapterforStockListMaterialDialog, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    public void buildStockListDialogEdit() {

        rvAdapterforStockListMaterialDialogEdit = new RVAdapterforStockListMaterialDialog(stockItemList);

        stockListMaterialDialogEdit = new MaterialDialog.Builder(context).

                adapter(rvAdapterforStockListMaterialDialogEdit, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    public void buildAddDiscountDialog() {

        TypedArray discount = context.getTheme().obtainStyledAttributes(new int[]{R.attr.discount});
        //TypedArray save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        addDiscountMaterialDialog = new MaterialDialog.Builder(context).

                customView(R.layout.add_discount_dialog, true).title(discount.getString(0)).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").

                // positiveText(save.getString(0)).

                //  negativeText(cancel.getString(0)).

                        build();
    }

    public void buildPayDialog() {

        TypedArray payment = context.getTheme().obtainStyledAttributes(new int[]{R.attr.payment});
        //TypedArray save = context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});

        payMaterialDialog = new MaterialDialog.Builder(context).

                customView(R.layout.purchase_pay, true).title(payment.getString(0)).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").

                //positiveText(save.getString(0)).

                // negativeText(cancel.getString(0)).

                        build();

    }


    private Double calculatePrice(Double salePrice, int qty, Double discAmt) {

        Double totalPrice = 0.0;

        if (foc) {

            return 0.0;

        } else {

            salePrice -= discAmt;

            totalPrice = qty * salePrice;

            return totalPrice;

        }

    }

    private void updateViewData() {

        calculateValues();
        refreshRecyclerView();
        taxAmtTextView.setText(POSUtil.doubleToString(voucherTax));

        taxRateTextView.setText(POSUtil.PercentFromat(tax.getRate()));

        /*if (!  discount.isNull()) {
            if (discount.getIsPercent() == 0) {
                discountTextView.setText(POSUtil.doubleToString(discount.getAmount()));
            } else {
                discountTextView.setText(POSUtil.doubleToString(discount.getAmount()) + "%");
            }
        }*/

        discountTextView.setText(POSUtil.NumberFormat(voucherDiscount));

        subtotalTextView.setText(POSUtil.NumberFormat(subtotal));

        totalTextView.setText(POSUtil.NumberFormat(totalAmount));

    }

    private void calculateValues() {

        subtotal = 0.0;

        for (SalesAndPurchaseItem s : purchaseItemViewInSaleList) {

            subtotal += s.getTotalPrice();

        }

        voucherTax = (subtotal / 100) * tax.getRate();
        if (discount.getIsPercent() == 0) {
            voucherDiscount = discount.getAmount();
        } else {
            Log.e("total ", "amt " + subtotal);
            voucherDiscount = discount.getAmount() * subtotal / 100;
        }
        if (tax.getType().equalsIgnoreCase("Inclusive")) {
            totalAmount = subtotal - voucherDiscount;

        } else {

            totalAmount = subtotal + voucherTax - voucherDiscount;

        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == SCANNER_BAR_CODE) {
            // Handle scan intent
            if (resultCode == Activity.RESULT_OK) {
                // Handle successful scan
                String contents = intent.getStringExtra("SCAN_RESULT");
                String formatName = intent.getStringExtra("SCAN_RESULT_FORMAT");
                byte[] rawBytes = intent.getByteArrayExtra("SCAN_RESULT_BYTES");
                int intentOrientation = intent.getIntExtra("SCAN_RESULT_ORIENTATION", Integer.MIN_VALUE);
                Integer orientation = (intentOrientation == Integer.MIN_VALUE) ? null : intentOrientation;
                String errorCorrectionLevel = intent.getStringExtra("SCAN_RESULT_ERROR_CORRECTION_LEVEL");

                Log.w("Contents", contents + " hea");
                Log.w("formatName", formatName + " hea");
                Log.w("rawBytes", rawBytes + " hea");
                Log.w("orientation", contents + " hea");
                Log.w("errorCorrectionLevel", errorCorrectionLevel + " hea");
                itemOrCodeAutocomplete.clearListSelection();
                itemOrCodeAutocomplete.setText(contents);
                itemOrCodeAutocomplete.setSelection(itemOrCodeAutocomplete.getText().toString().length());
                itemOrCodeAutocomplete.setSelected(true);
                itemOrCodeAutocomplete.setSelectAllOnFocus(true);
                itemOrCodeAutocomplete.selectAll();
                addItem();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // Handle cancel
            }
        } else {
            // Handle other intents
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        purchaseDay = dayOfMonth;

        purchaseMonth = monthOfYear + 1;

        this.purchaseYear = year;

        date = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

        String dayDes[] = DateUtility.dayDes(date);

        String yearMonthDes = DateUtility.monthYearDes(date);

        // dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

        dateTextView.setText(DateUtility.makeDateFormatWithSlash(date));

    }


    @Override
    public void onActionDone(Discount discount) {
        discountDialogFragment.dismiss();
        this.discount = discount;
        discountDialogFragment.setDefaultDiscount(discount);
        updateViewData();
    }
}
