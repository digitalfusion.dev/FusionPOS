package com.digitalfusion.android.pos.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.digitalfusion.android.pos.fragments.settingfragment.controlserver.ServerTabsFragment;

/**
 * Created by lyx on 5/25/18.
 */
public class ServerTabsPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {
    private final int PAGE_COUNT = 2;
    private String tabTitles[];
    private Context context;

    public ServerTabsPagerAdapter(FragmentManager fragmentManager, String[] titles, Context context) {
        super(fragmentManager);
        this.context = context;
        this.tabTitles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        return ServerTabsFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
