package com.digitalfusion.android.pos.database.dao;


import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/17/16.
 */
public class CategoryDAO extends ParentDAO {
    private static ParentDAO categoryDaoInstance;
    private Context context;
    private IdGeneratorDAO idGeneratorDAO;
    private List<Category> categoryList;
    private Category category;
    private List<String> nameList;


    private CategoryDAO(Context context) {
        super(context);
        this.context = context;
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        category = new Category();
    }

    public static CategoryDAO getCategoryDaoInstance(Context context) {
        if (categoryDaoInstance == null) {
            categoryDaoInstance = new CategoryDAO(context);
        }
        return (CategoryDAO) categoryDaoInstance;
    }

    public Long addNewCategory(String name, String description, Long parentId, int level, final InsertedBooleanHolder isInserted) {
        id = checkCategoryAlreadyExist(name);
        if (id == null) {
            genID = idGeneratorDAO.getLastIdValue("Category");
            genID += 1;
            id = genID;
            query = "insert into " + AppConstant.CATEGORY_TABLE_NAME + "(" + AppConstant.CATEGORY_ID + ", " + AppConstant.CATEGORY_NAME + ", " + AppConstant.CATEGORY_DESCRIPTION
                    + ", " + AppConstant.CATEGORY_PARENT_ID + ", " + AppConstant.CATEGORY_LEVEL + ", " + AppConstant.CREATED_DATE + ") values(?,?,?,?,?,?)";
            databaseWriteTransaction(isInserted);
            try {
                statement = database.compileStatement(query);
                statement.bindLong(1, genID);
                statement.bindString(2, name);
                if (description == null) {
                    statement.bindNull(3);
                } else {
                    statement.bindString(3, description);
                }
                if (parentId == null) {
                    statement.bindNull(4);
                } else {
                    statement.bindLong(4, parentId);
                }
                statement.bindString(5, Integer.toString(level));
                statement.bindString(6, DateUtility.getTodayDate());
                statement.execute();
                statement.clearBindings();
                database.setTransactionSuccessful();
            } catch (SQLiteException e) {
                e.printStackTrace();
            } finally {
                database.endTransaction();
            }
        }
        Log.w("IN Deep databaASe", "category id " + id);
        return id;
    }

    public Long checkCategoryAlreadyExist(String name) {
        Long categoryId = null;
        query = " select " + AppConstant.CATEGORY_ID + " from " + AppConstant.CATEGORY_TABLE_NAME + " where " + AppConstant.CATEGORY_NAME + " = ?;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{name});
            if (cursor.moveToFirst()) {
                categoryId = cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return categoryId;
    }


    public List<Category> getAllCategories() {
        categoryList = new ArrayList<>();
        query = "select " + AppConstant.CATEGORY_ID + ", " + AppConstant.CATEGORY_NAME + ", " + AppConstant.CATEGORY_DESCRIPTION + ", " + AppConstant.CATEGORY_PARENT_ID
                + ", " + AppConstant.CATEGORY_LEVEL + " from " + AppConstant.CATEGORY_TABLE_NAME;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    category = new Category();
                    category.setId(cursor.getLong(0));
                    category.setName(cursor.getString(1));
                    category.setDescription(cursor.getString(2));
                    category.setParentID(cursor.getLong(3));
                    category.setLevel(cursor.getInt(4));
                    categoryList.add(category);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return categoryList;
    }

    public int categoryCounts() {
        query = "select count(" + AppConstant.CATEGORY_ID + ") from " + AppConstant.CATEGORY_TABLE_NAME;
        databaseWriteTransaction();
        try {
            cursor = database.rawQuery(query, null);
            cursor.moveToFirst();
            count = cursor.getInt(0);
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            database.endTransaction();
        }
        return count;
    }

    public boolean updateCategory(String name, String description, Long id) {
        query = "update " + AppConstant.CATEGORY_TABLE_NAME + " set " + AppConstant.CATEGORY_NAME + "=?, " + AppConstant.CATEGORY_DESCRIPTION + "=? where " + AppConstant.CATEGORY_ID
                + " = ?";
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{name, description, id.toString()});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public List<String> getAllDistinctCategoryNames() {
        nameList = new ArrayList<>();
        query = "select distinct " + AppConstant.CATEGORY_NAME + " from " + AppConstant.CATEGORY_TABLE_NAME;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    nameList.add(cursor.getString(0));
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return nameList;
    }

    public Long findIdByName(String name) {
        id = null;
        query = "select " + AppConstant.CATEGORY_ID + " from " + AppConstant.CATEGORY_TABLE_NAME + " where " + AppConstant.CATEGORY_NAME + " = ?";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{name});
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }

    public boolean deleteCategory(Long id) {
        databaseWriteTransaction();
        try {
            count = database.delete(AppConstant.CATEGORY_TABLE_NAME, AppConstant.CATEGORY_ID + " = ?", new String[]{id.toString()});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return count > 0;
    }
}
