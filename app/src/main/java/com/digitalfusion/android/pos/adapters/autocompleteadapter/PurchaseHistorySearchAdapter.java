package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.PurchaseManager;
import com.digitalfusion.android.pos.database.model.PurchaseHistory;
import com.digitalfusion.android.pos.database.model.SalesHistory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 9/20/16.
 */
public class PurchaseHistorySearchAdapter extends ArrayAdapter<PurchaseHistory> {


    private List<PurchaseHistory> searchList;

    private List<PurchaseHistory> suggestion;

    private PurchaseManager purchaseManager;

    private int lenght = 0;

    private String queryText = "";

    private PurchaseHistory stockAutoCompleteView;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {

            String str = ((SalesHistory) resultValue).getVoucherNo();

            stockAutoCompleteView = (PurchaseHistory) resultValue;

            return str;

        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            Log.w("filtering", "filtering");

            if (constraint != null && !constraint.equals("")) {

                searchList.clear();

                queryText = constraint.toString();

                lenght = constraint.length();

                if (constraint.toString().startsWith("#")) {
                    searchList = purchaseManager.getPurchaseWithVoucherNoOrSupplier(0, 100, constraint.toString().substring(1, constraint.length()));
                } else {
                    searchList = purchaseManager.getPurchaseWithVoucherNoOrSupplier(0, 100, constraint.toString());
                }

                FilterResults filterResults = new FilterResults();

                filterResults.values = searchList;

                filterResults.count = searchList.size();

                return filterResults;
            } else {

                return new FilterResults();

            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            List<PurchaseHistory> filterList = (ArrayList<PurchaseHistory>) results.values;

            suggestion = searchList;

            if (results != null && results.count > 0) {

                clear();

                for (PurchaseHistory purchaseHistory : filterList) {

                    add(purchaseHistory);

                    notifyDataSetChanged();

                }

            }

        }

    };
    private Context context;

    public PurchaseHistorySearchAdapter(Context context, PurchaseManager purchaseManager) {

        super(context, -1);

        this.context = context;

        this.purchaseManager = purchaseManager;

        suggestion = new ArrayList<>();
        searchList = new ArrayList<>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.sale_history_search_suggest_view, parent, false);

        }

        viewHolder = new ViewHolder(convertView);

        if (!suggestion.isEmpty() && suggestion.size() > 0) {


            if (suggestion.get(position).getVoucherNo().toLowerCase().startsWith(queryText.toLowerCase())) {

                Log.w("hrere", "hrere");

                Log.w("size", position + " SSS");

                Spannable spanText = new SpannableString("#" + suggestion.get(position).getVoucherNo());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 1, lenght + 1, 0);

                viewHolder.voucherNoTextView.setText(spanText);

            } else if (queryText.startsWith("#") && suggestion.get(position).getVoucherNo().toLowerCase().contains(queryText.substring(1, queryText.length()).toLowerCase())) {

                Spannable spanText = new SpannableString("#" + suggestion.get(position).getVoucherNo());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.voucherNoTextView.setText(spanText);
            } else {

                viewHolder.voucherNoTextView.setText("#" + suggestion.get(position).getVoucherNo());

            }
            // viewHolder.customerNameTextView.setText(salesHistoryViewList.get(position).getItemName());

            if (suggestion.get(position).getSupplierName().toLowerCase().startsWith(queryText.toLowerCase())) {

                Spannable spanText = new SpannableString(suggestion.get(position).getSupplierName());

                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, lenght, 0);

                viewHolder.customerNameTextView.setText(spanText);

            } else {

                viewHolder.customerNameTextView.setText(suggestion.get(position).getSupplierName());

            }
        }

        Log.w("here lol", suggestion.size() + " SSS");

        return convertView;

    }

    @Override
    public int getCount() {
        return suggestion.size();
    }

    public List<PurchaseHistory> getSuggestion() {

        return suggestion;

    }

    public void setSuggestion(List<PurchaseHistory> suggestion) {

        this.suggestion = suggestion;

    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    public PurchaseHistory getSelectedItem() {
        return stockAutoCompleteView;
    }

    static class ViewHolder {

        TextView voucherNoTextView;

        TextView customerNameTextView;

        public ViewHolder(View itemView) {

            this.customerNameTextView = (TextView) itemView.findViewById(R.id.voucher_no);

            this.voucherNoTextView = (TextView) itemView.findViewById(R.id.customer_name);

        }

    }

}