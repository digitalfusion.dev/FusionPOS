package com.digitalfusion.android.pos.database.business;

import android.content.Context;
import android.util.Log;

import com.digitalfusion.android.pos.database.dao.ReportDAO;
import com.digitalfusion.android.pos.database.model.ExpenseIncome;
import com.digitalfusion.android.pos.database.model.ReportItem;

import java.util.List;

/**
 * Created by MD002 on 11/8/16.
 */

public class ReportManager {
    private Context context;
    private ReportDAO reportDAO;

    public ReportManager(Context context) {
        this.context = context;
        reportDAO = ReportDAO.getReportDaoInstance(context);
    }

    public List<ReportItem> topSalesByQty(String startDate, String endDate, int startLimit, int endLimit) {
        return reportDAO.topSalesItems(startDate, endDate, startLimit, endLimit);
    }

    public List<ReportItem> bottomSalesByQty(String startDate, String endDate, int startLimit, int endLimit) {
        return reportDAO.bottomSalesItems(startDate, endDate, startLimit, endLimit);
    }

    public List<ReportItem> bottomSalesByProducts(String startDate, String endDate, int startLimit, int endLimit, String categoryID) {
        return reportDAO.bottomSales(startDate, endDate, startLimit, endLimit, categoryID);
    }

    public List<ReportItem> topSalesCustomers(String startDate, String endDate, int startLimit, int endLimit) {
        return reportDAO.topSalesCustomers(startDate, endDate, startLimit, endLimit);
    }

    public List<ReportItem> topSuppliers(String startDate, String endDate, int startLimit, int endLimit) {
        return reportDAO.topSuppliers(startDate, endDate, startLimit, endLimit);
    }

    public List<ReportItem> topOutstandingCustomer(int startLimit, int endLimit) {
        return reportDAO.topOutstandingCustomer(startLimit, endLimit);
    }

    public List<ReportItem> topOutstandingSupplier(int startLimit, int endLimit) {
        return reportDAO.topOutstandingSupplier(startLimit, endLimit);
    }

    public List<ReportItem> topSalesByProducts(String startDate, String endDate, int startLimit, int endLimit, String categoryID) {
        return reportDAO.topSales(startDate, endDate, startLimit, endLimit, categoryID);
    }

    public List<ReportItem> allSales(String startDate, String endDate, int startLimit, int endLimit, String categoryID) {
        return reportDAO.allSales(startDate, endDate, startLimit, endLimit, categoryID);
    }

    public double getTotalAmountOfTotalAmountOfAllSales(String startDate, String endDate, String categoryID) {
        return reportDAO.getTotalAmountOfTotalAmountOfAllSales(startDate, endDate, categoryID);
    }

    public List<ReportItem> allPurchases(String startDate, String endDate, int startLimit, int endLimit, String categoryID) {
        return reportDAO.allPurchases(startDate, endDate, startLimit, endLimit, categoryID);
    }

    public double getTotalAmountOfTotalAmountOfAllPurchases(String startDate, String endDate, String categoryID) {
        return reportDAO.getTotalAmountOfTotalAmountOfAllPurchases(startDate, endDate, categoryID);
    }

    public List<ReportItem> monthlySalesTransactions(String startDate, String endDate, String[] monthYearArr, String orderby) {
        return reportDAO.monthlySalesTransaction(startDate, endDate, monthYearArr, orderby);
    }

    public List<ReportItem> monthlyPurchaseTransactions(String startDate, String endDate, String[] monthYearArr, String orderby) {
        return reportDAO.monthlyPurchaseTransactions(startDate, endDate, monthYearArr, orderby);
    }

    public boolean createPurchasePriceTempTable(String endDate, String startDate) {
        return reportDAO.createPurchasePriceTempTable(endDate, startDate);
    }

    public boolean createTempTableForProfitAndLoss(String startDate, String endDate) {
        return reportDAO.createTempTableForProfitAndLoss(startDate, endDate);
    }

    public boolean createPurchasePriceTempTableForValuation(String endDate) {
        return reportDAO.createPurchasePriceTempTableForValuation(endDate);
    }

    public boolean createPurchasePriceTempTableForStockBalance(String startDate, String endDate) {
        return reportDAO.createPurchasePriceTempTableForStockBalance(startDate, endDate);
    }

    public List<ReportItem> monthlyGrossProfit(String startDate, String endDate, String[] dateFilterArr, String orderby) {
        return reportDAO.monthlyGrossProfit(startDate, endDate, dateFilterArr, orderby);
    }

    public List<ReportItem> monthlyGrossProfitByProduct(Long stockID, String startDate, String endDate, String[] dateFilterArr, String orderby) {
        //Log.e("stockid", stockID.toString());
        return reportDAO.monthlyGrossProfitByProduct(stockID, startDate, endDate, dateFilterArr, orderby);
    }

    public List<ReportItem> topSalesByCategory(String startDate, String endDate, int startLimit, int endLimit) {
        return reportDAO.topSalesByCategory(startDate, endDate, startLimit, endLimit);
    }

    /**
     * @param type         income or expense
     * @param startDate
     * @param endDate
     * @param monthYearArr
     * @param orderby
     * @return
     */
    public List<ReportItem> monthlyIncomeExpenseTransactions(String type, String startDate, String endDate, String[] monthYearArr, String orderby) {
        if (type.equalsIgnoreCase(ExpenseIncome.Type.Income.toString())) {
            return reportDAO.monthlyIncomeTransactions(startDate, endDate, monthYearArr, orderby);
        } else {
            return reportDAO.monthlyExpenseTransactions(startDate, endDate, monthYearArr, orderby);
        }
    }

    public List<ReportItem> monthlySalesTax(String startDate, String endDate, String[] monthYearArr, String orderby) {
        return reportDAO.monthlySalesTax(startDate, endDate, monthYearArr, orderby);
    }

    public List<ReportItem> inventoryValuation(int startLimit, int endLimit) {
        return reportDAO.inventoryValuation(startLimit, endLimit);
    }

    public Double totalInventoryValuation() {
        return reportDAO.totalInventoryValuation();
    }

    public List<ReportItem> productValuationByCategoryID(int startLimit, int endLimit, String categoryID) {
        //        Log.e(startLimit+ " start ", endLimit + "");
        return reportDAO.productValuationByCategoryID(startLimit, endLimit, categoryID);
    }

    public Double totalProductValuationByCategoryID(String categoryID) {
        return reportDAO.totalProductValuationByCategoryID(categoryID);
    }

    public List<ReportItem> productValuation(Long stockID) {
        return reportDAO.productValuation(stockID);
    }

    public List<ReportItem> supplierByProduct(Long productID, int startLimit, int endLimit) {
        Log.e("idoa", "ifjaiodjfa " + productID);
        return reportDAO.supplierByProduct(productID, startLimit, endLimit);
    }


    /**
     * @param startDate
     * @param endDate
     * @param monthYearArr
     * @param orderby      asc
     * @param type         income or expense
     * @return
     */
    public List<ReportItem> comparisonOfIncomeExpense(String startDate, String endDate, String[] monthYearArr, String orderby, String type) {
        if (type.equalsIgnoreCase(ExpenseIncome.Type.Income.toString())) {
            return reportDAO.monthlyIncomeTransactions(startDate, endDate, monthYearArr, orderby);
        } else {
            return reportDAO.monthlyExpenseTransactions(startDate, endDate, monthYearArr, orderby);
        }
    }

    public boolean dropPurchasePriceTempTable() {
        return reportDAO.dropPurchasePriceTempTable();
    }

    public List<ReportItem> damageList(String startDate, String endDate, int startLimit, int endLimit) {
        return reportDAO.damageList(startDate, endDate, startLimit, endLimit);
    }

    public List<ReportItem> lostList(String startDate, String endDate, int startLimit, int endLimit) {
        return reportDAO.lostList(startDate, endDate, startLimit, endLimit);
    }

    public List<ReportItem> lowStockList(int startLimit, int endLimit) {
        return reportDAO.lowStockList(startLimit, endLimit);
    }

    public List<ReportItem> stockBalance(int startLimit, int endLimit, Long categoryID) {
        return reportDAO.stockBalance(startLimit, endLimit, categoryID);
    }


    public List<ReportItem> receivableHeaderList(int startLimit, int endLimit, int token) {
        return reportDAO.receivableHeaderList(startLimit, endLimit, token);
    }

    public List<ReportItem> payableHeaderList(int startLimit, int endLimit, int token) {
        return reportDAO.payableHeaderList(startLimit, endLimit, token);
    }

    public List<ReportItem> receivableDetailList(Long customerID, String startDate, String endDate, int startLimit, int endLimit) {
        return reportDAO.receivableDetailList(customerID, startDate, endDate, startLimit, endLimit);
    }


    public List<ReportItem> payableDetailList(Long supplierID, String startDate, String endDate, int startLimit, int endLimit) {
        return reportDAO.payableDetailList(supplierID, startDate, endDate, startLimit, endLimit);
    }


    public List<ReportItem> customerOutstandingSalesVoucherList(Long customerID, String startDate, String endDate, int startLimit, int endLimit) {
        return reportDAO.customerOutstandingSalesVoucherList(customerID, startDate, endDate, startLimit, endLimit);
    }


    public List<ReportItem> supplierOutstandingPurchaseVoucherList(Long supplierID, String startDate, String endDate, int startLimit, int endLimit) {
        return reportDAO.supplierOutstandingPurchaseVoucherList(supplierID, startDate, endDate, startLimit, endLimit);
    }


    public List<ReportItem> paymentListForSalesID(Long salesID) {
        return reportDAO.paymentListForSalesID(salesID);
    }

    public List<ReportItem> paymentListForPurchaseID(Long purchaseID) {
        return reportDAO.paymentListForPurchaseID(purchaseID);
    }


    public List<ReportItem> noOfProductCategoryBySupplier(String startDate, String endDate, int startLimit, int endLimit) {
        Log.e("start " + startDate, endDate);
        return reportDAO.noOfProductCategoryBySupplier(startDate, endDate, startLimit, endLimit);
    }

    public List<ReportItem> productCategoryBySupplier(String startDate, String endDate, int startLimit, int endLimit, Long supplierID) {
        return reportDAO.productCategoryBySupplier(startDate, endDate, startLimit, endLimit, supplierID);
    }

    public List<ReportItem> productBySupplierCategory(String startDate, String endDate, int startLimit, int endLimit, Long supplierID, Long categoryID) {
        return reportDAO.productBySupplierCategory(startDate, endDate, startLimit, endLimit, supplierID, categoryID);
    }

    public List<ReportItem> productPriceTrend(Long stockID, String startDate, String endDate, String orderBy, String[] dateArr) {
        return reportDAO.productPriceTrend(stockID, startDate, endDate, orderBy, dateArr);
    }

    public ReportItem profitAndLossAmt(String startDate, String endDate) {
        return reportDAO.profitAndLossAmt(startDate, endDate);
    }


    public List<ReportItem> profitAndLossIncomeList(String startDate, String endDate) {
        return reportDAO.profitAndLossExpenseMgrAmt(startDate, endDate, "Income");
    }

    public List<ReportItem> profitAndLossExpenseList(String startDate, String endDate) {
        return reportDAO.profitAndLossExpenseMgrAmt(startDate, endDate, "Expense");
    }

    public Double getExpMgrTotal(String startDate, String endDate, String type) {
        return reportDAO.getExpMgrTotal(startDate, endDate, type);
    }

    public List<ReportItem> getTotalAmtByIncomeExpense(String startDate, String endDate, int startLimit, int endLimit, String type) {
        return reportDAO.getTotalAmtByIncomeExpense(startDate, endDate, startLimit, endLimit, type);
    }

    public List<ReportItem> monthlySalesBySaleStaff(String startDate, String endDate, Long userId) {
        return reportDAO.monthlySaleBySaleStaff(startDate, endDate, userId);
    }

    public List<ReportItem> salesBySaleStaff(String startDate, String endDate, int startLimit, int endLimit) {
        return reportDAO.salesBySaleStaff(startDate, endDate, startLimit, endLimit);
    }

    public List<ReportItem> salesDetailBySaleStaff(String startDate, String endDate, int startLimit, int endLimit, Long userId) {
        return reportDAO.salesDetailBySaleStaff(startDate, endDate, startLimit, endLimit, userId);
    }

    public void dropSaleTempTable() {
    }
}
