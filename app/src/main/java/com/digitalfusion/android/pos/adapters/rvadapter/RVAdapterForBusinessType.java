package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.BusinessType;

import java.util.List;

/**
 * Created by MD002 on 12/29/17.
 */

public class RVAdapterForBusinessType extends RecyclerView.Adapter<RVAdapterForBusinessType.BusinessTypeViewHolder> {


    List<BusinessType> filterNameList;

    private OnItemClickListener mItemClickListener;

    public RVAdapterForBusinessType(List<BusinessType> filterNameList) {
        this.filterNameList = filterNameList;
    }

    @Override
    public BusinessTypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.business_type_item_view, parent, false);

        return new BusinessTypeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BusinessTypeViewHolder holder, final int position) {
        BusinessType businessType = filterNameList.get(position);
        holder.name.setText(filterNameList.get(position).getName());
        //        holder.checkbox.setOnCheckedChangeListener(null);
        if (businessType.isSelected()) {
            holder.checkbox.setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
        }
        //if true, your checkbox will be selected, else unselected

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.checkbox.setChecked(!filterNameList.get(position).isSelected());
            }
        });

        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("check", "change listener");
                filterNameList.get(position).setSelected(isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filterNameList.size();
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class BusinessTypeViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        CheckBox checkbox;
        View itemView;

        public BusinessTypeViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            checkbox = (CheckBox) itemView.findViewById(R.id.checkbox);
            name = (TextView) itemView.findViewById(R.id.name);


        }
    }
}