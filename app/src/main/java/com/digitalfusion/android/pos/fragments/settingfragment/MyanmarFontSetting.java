package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.util.POSUtil;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyanmarFontSetting.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyanmarFontSetting#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyanmarFontSetting extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private View mainLayout;

    private RadioGroup group;

    private RadioButton noembd;

    private RadioButton embd;

    private Button apply;

    public MyanmarFontSetting() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyanmarFontSetting.
     */
    // TODO: Rename and change types and number of parameters
    public static MyanmarFontSetting newInstance(String param1, String param2) {
        MyanmarFontSetting fragment = new MyanmarFontSetting();
        Bundle             args     = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String myanmarFont = getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.myanmar_font_setting}).getString(0);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(myanmarFont);

        mainLayout = inflater.inflate(R.layout.fragment_myanmar_font_setting, container, false);
        group = (RadioGroup) mainLayout.findViewById(R.id.gp);
        embd = (RadioButton) mainLayout.findViewById(R.id.emb);
        noembd = (RadioButton) mainLayout.findViewById(R.id.no_emb);
        apply = (Button) mainLayout.findViewById(R.id.apply);

        Typeface mTypeface = Typeface.createFromAsset(getContext()
                .getAssets(), String.format("fonts/%s", "Zawgyi-One.ttf"));
        embd.setTypeface(mTypeface);

        if (POSUtil.isEmbended(getContext())) {
            embd.setChecked(true);
        } else {
            noembd.setChecked(true);
        }

        return mainLayout;
    }


    @Override
    public void onResume() {
        super.onResume();

        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

            }
        });

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (embd.isChecked()) {
                    POSUtil.deactivateNoEmbendedFont(getContext());
                } else {
                    POSUtil.activateNoEmbendedFont(getContext());
                }
                Intent i = getActivity().getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                System.exit(1);

            }
        });

    }

    // TODO: Rename method, updateRegistration argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
