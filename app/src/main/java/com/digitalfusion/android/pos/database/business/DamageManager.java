package com.digitalfusion.android.pos.database.business;

import android.content.Context;

import com.digitalfusion.android.pos.database.dao.DamageDAO;
import com.digitalfusion.android.pos.database.dao.DamageDetailDAO;
import com.digitalfusion.android.pos.database.model.DamagedItem;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InvoiceNoGenerator;

import java.util.List;

/**
 * Created by MD002 on 8/19/16.
 */
public class DamageManager {
    private Context context;
    private DamageDAO damageDAO;
    private DamageDetailDAO damageDetailDAO;
    private InvoiceNoGenerator invoiceNoGenerator;

    public DamageManager(Context context) {
        this.context = context;
        damageDAO = DamageDAO.getDamageDAOInstance(context);
        damageDetailDAO = DamageDetailDAO.getDamageDetailDaoInstance(context);
        invoiceNoGenerator = new InvoiceNoGenerator(context);
    }

    public String getDamageInvoiceNo() {
        return invoiceNoGenerator.getInvoiceNo(AppConstant.DAMAGE_INVOICE_NO);
    }

    public void addNewDamage(int stockQty, Long stockID, String remark, String day, String month, String year, Long userId) {
        boolean flag = damageDAO.addNewDamage(stockQty, DateUtility.makeDate(year, month, day), stockID, remark, day, month, year, userId);
    }

    public List<DamagedItem> getAllDamages(int startLimit, int endLimit) {
        return damageDAO.getDamages(startLimit, endLimit);
    }

    public List<DamagedItem> getDamageListByDateRange(String startDate, String endDate, int startLimit, int endLimit) {
        return damageDAO.getDamagesByDateRange(startDate, endDate, startLimit, endLimit);
    }

    /*public List<DamageItemInDetail> getDamageDetailByDamageID(Long damageID){
        return damageDetailDAO.getAllDamageDetailByDamageID(damageID);
    }*/

    public boolean updateDamage(Long id, int stockQty, String remark, Long userID,
                                String day, String month, String year, Long stockID) {
        return damageDAO.updateDamage(id, DateUtility.makeDate(year, month, day), stockQty, remark, userID, day, month, year, stockID);
    }

    public boolean deleteDamage(Long id) {
        return damageDAO.deleteDamage(id);
    }

    public List<DamagedItem> getDamagesForSearch(String startDate, String endDate, int startLimit, int endLimit, String searchStr) {
        return damageDAO.getDamagesForSearch(startDate, endDate, startLimit, endLimit, searchStr);
    }

    public List<DamagedItem> getDamagesByDateRangeOnSearch(String startDate, String endDate, int startLimit, int endLimit, Long id) {
        return damageDAO.getDamagesByDateRangeOnSearch(startDate, endDate, startLimit, endLimit, id);
    }
}
