package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.ThemeUtil;

import java.util.List;

/**
 * Created by MD002 on 1/6/17.
 */

public class RVAdapterforProductCategoryBySupplier extends ParentRVAdapterForReports {

    private static final int VIEWTYPE_ITEM = 2;
    private static final int VIEWTYPE_LOADER = 3;
    private final int HEADER_TYPE = 10000;
    private List<ReportItem> reportItemList;
    private int viewType;
    private ClickListener clickListener;
    private Context context;

    /**
     * 1 for stock, category, amount, qty, unit
     * 2 for category, amount
     */
    public RVAdapterforProductCategoryBySupplier(List<ReportItem> reportItemList, Context context) {
        super();
        this.reportItemList = reportItemList;
        Log.e("hello", reportItemList.size() + " ss");
        this.context = context;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_name_two_qty_header, parent, false);

            return new HeaderViewHolder(v);
        } else if (viewType == VIEWTYPE_LOADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);
            return new LoaderViewHolder(v);
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view_name_two_qty, parent, false);

        return new ItemViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        Log.e("vl", "daf");
        if (viewHolder instanceof LoaderViewHolder) {
            Log.e("loader", "loader");

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);

        } else if (viewHolder instanceof ItemViewHolder && !reportItemList.isEmpty()) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
            itemViewHolder.nameTextView.setText(reportItemList.get(position - 1).getName());
            itemViewHolder.inventoryQtyTextView.setText(Integer.toString(reportItemList.get(position - 1).getQty()));
            itemViewHolder.reorderQtyTextView.setText(Integer.toString(reportItemList.get(position - 1).getQty1()));
            itemViewHolder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onClick(position);
                }
            });
        } else if (viewHolder instanceof HeaderViewHolder && reportItemList.isEmpty()) {
            String           erroMsg          = ThemeUtil.getString(context, R.attr.no_report_available);
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            headerViewHolder.noDataTextView.setText(erroMsg);
            headerViewHolder.itemViewLayout.setVisibility(View.GONE);
            headerViewHolder.noDataTextView.setVisibility(View.VISIBLE);
            headerViewHolder.line.setVisibility(View.GONE);
        } else {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            headerViewHolder.nameTextView.setText("SUPPLIER NAME");
            headerViewHolder.numTextView1.setText("NO OF CATEGORY");
            headerViewHolder.numTextView2.setText("NO OF ITEM");
            headerViewHolder.noDataTextView.setVisibility(View.GONE);
            headerViewHolder.itemViewLayout.setVisibility(View.VISIBLE);
            headerViewHolder.line.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {

        return reportItemList.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position != 0 && position == getItemCount() - 1) {

            if (reportItemList != null) {

                return VIEWTYPE_LOADER;

            }

        }
        if (position == 0) {
            return HEADER_TYPE;
        }
        return VIEWTYPE_ITEM;
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layout;
        TextView nameTextView;
        TextView inventoryQtyTextView;
        TextView reorderQtyTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            inventoryQtyTextView = (TextView) itemView.findViewById(R.id.inventory_qty_text_view);
            reorderQtyTextView = (TextView) itemView.findViewById(R.id.reorder_qty_text_view);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        LinearLayout itemViewLayout;
        TextView noDataTextView;
        View line;
        TextView numTextView1;
        TextView numTextView2;
        TextView nameTextView;

        public HeaderViewHolder(View itemView) {
            super(itemView);

            itemViewLayout = (LinearLayout) itemView.findViewById(R.id.item_view_layout);
            noDataTextView = (TextView) itemView.findViewById(R.id.no_data_text_view);
            line = itemView.findViewById(R.id.line);
            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            numTextView1 = (TextView) itemView.findViewById(R.id.num_text_view1);
            numTextView2 = (TextView) itemView.findViewById(R.id.num_text_view2);
        }
    }
}
