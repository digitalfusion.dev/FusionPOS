package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD002 on 12/8/16.
 */

public class RVAdapterForOutstandingReport extends ParentRVAdapterForReports {

    private final int PAYMENT_HEADER_TYPE = 10000;
    private final int PAYMENT_DETAIL_HEADER_TYPE = 20000;
    private List<ReportItem> reportItemList;
    private int viewType;
    private com.digitalfusion.android.pos.interfaces.ClickListener clickListener;
    private boolean isCustomerPayment;
    private ClickListener reportOutstandingClickListener;

    /**
     * 1 for stock, category, amount, qty, unit
     * 2 for category, amount
     */
    public RVAdapterForOutstandingReport(List<ReportItem> reportItemList, int viewType) {
        this.reportItemList = reportItemList;
        this.viewType = viewType;
        //Log.w("hello", reportItemList.size() + " ss");
    }

    public void setClickListener(com.digitalfusion.android.pos.interfaces.ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setReportOutstandingClickListener(ClickListener reportOutstandingClickListener) {
        this.reportOutstandingClickListener = reportOutstandingClickListener;
    }

    public void setCustomerPayment(boolean customerPayment) {
        isCustomerPayment = customerPayment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_outstading_detail_item_view, parent, false);

            return new PayableReceivableDetailViewHolder(v);
        } else if (viewType == 2) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_payment_header_item_view, parent, false);

            return new SalesVoucherViewHolder(v);
        } else if (viewType == 3) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_payment_item_view, parent, false);

            return new PaymentViewHolder(v);
        } else if (viewType == PAYMENT_DETAIL_HEADER_TYPE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_payment_item_view_header, parent, false);
            return new PaymentHeaderViewHolder(v);
        } else {
            if (viewType == PAYMENT_HEADER_TYPE) {

                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_payment_header_header_view, parent, false);

                return new HeaderViewHolder(v);
            }
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        if (viewHolder instanceof PayableReceivableDetailViewHolder) {
            PayableReceivableDetailViewHolder payableReceivableDetailViewHolder = (PayableReceivableDetailViewHolder) viewHolder;
            payableReceivableDetailViewHolder.dateTextView.setText(reportItemList.get(position).getDate());
            payableReceivableDetailViewHolder.voucherNoTextView.setText(reportItemList.get(position).getName());
            payableReceivableDetailViewHolder.totalAmtTextView.setText(POSUtil.NumberFormat(reportItemList.get(position).getTotalAmt()));
            payableReceivableDetailViewHolder.balanceTextView.setText(POSUtil.NumberFormat(reportItemList.get(position).getBalance()));
            payableReceivableDetailViewHolder.paidAmtTextView.setText(POSUtil.NumberFormat(reportItemList.get(position).getAmount()));
            payableReceivableDetailViewHolder.linearLayout.setOnClickListener(v -> clickListener.onClick(position));
//            if (reportType == 0) {

                payableReceivableDetailViewHolder.voucherNoLabelTextView.setText(payableReceivableDetailViewHolder.purchaseID);
                payableReceivableDetailViewHolder.totalAmtLabelTextView.setText(payableReceivableDetailViewHolder.purchaseTotal);
//            } else {
//                payableReceivableDetailViewHolder.voucherNoLabelTextView.setText(payableReceivableDetailViewHolder.saleID);
//                payableReceivableDetailViewHolder.totalAmtLabelTextView.setText(payableReceivableDetailViewHolder.saleTotal);
//            }
        } else if (viewHolder instanceof SalesVoucherViewHolder) {
            SalesVoucherViewHolder salesVoucherViewHolder = (SalesVoucherViewHolder) viewHolder;
            salesVoucherViewHolder.voucherNoTextView.setText(reportItemList.get(position - 1).getName());
            salesVoucherViewHolder.totalBalanceTextView.setText(POSUtil.NumberFormat(reportItemList.get(position - 1).getBalance()));
            salesVoucherViewHolder.dateTextView.setText(reportItemList.get(position - 1).getDate());
            salesVoucherViewHolder.itemView.setOnClickListener(v -> {
                if (reportOutstandingClickListener != null) {
                    reportOutstandingClickListener.onClick(reportItemList.get(position - 1));
                }
            });
        } else if (viewHolder instanceof HeaderViewHolder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            if (isCustomerPayment) {
                headerViewHolder.voucherNoLabelTextView.setText(headerViewHolder.saleID);
            } else {
                headerViewHolder.voucherNoLabelTextView.setText(headerViewHolder.purchaseID);
            }
        } else if (viewHolder instanceof PaymentViewHolder) {
            Log.e("ho", "l");
            PaymentViewHolder paymentViewHolder = (PaymentViewHolder) viewHolder;
            paymentViewHolder.voucherNoTextView.setText(reportItemList.get(position - 1).getName());
            paymentViewHolder.numberTextView.setText(Integer.toString(position));
            paymentViewHolder.totalAmtTextView.setText(POSUtil.NumberFormat(reportItemList.get(position - 1).getAmount()));
            paymentViewHolder.dateTextView.setText(reportItemList.get(position - 1).getDate());
        }
    }

    @Override
    public int getItemCount() {

        if (viewType == 2 || viewType == 3) {
            return reportItemList.size() + 1;
        } else {
            return reportItemList.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (viewType == 2) {
            if (position == 0) {
                return PAYMENT_HEADER_TYPE;
            } else {
                return viewType;
            }
        } else if (viewType == 3) {
            if (position == 0) {
                return PAYMENT_DETAIL_HEADER_TYPE;
            } else {
                return viewType;
            }
        }
        return viewType;
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public interface ClickListener {
        void onClick(ReportItem reportItem);
    }

    public class SalesVoucherViewHolder extends RecyclerView.ViewHolder {

        TextView voucherNoTextView;
        TextView totalBalanceTextView;
        TextView dateTextView;

        View itemView;

        public SalesVoucherViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            voucherNoTextView = (TextView) itemView.findViewById(R.id.voucher_no_text_view);
            totalBalanceTextView = (TextView) itemView.findViewById(R.id.total_balance_text_view);
            dateTextView = (TextView) itemView.findViewById(R.id.date_text_view);
        }
    }

    public class PayableReceivableDetailViewHolder extends RecyclerView.ViewHolder {

        TextView voucherNoLabelTextView;
        TextView voucherNoTextView;
        TextView dateTextView;
        TextView totalAmtLabelTextView;
        TextView totalAmtTextView;
        TextView paidAmtTextView;
        TextView balanceTextView;
        LinearLayout linearLayout;

        String purchaseID;
        String purchaseTotal;
        String saleID;
        String saleTotal;

        public PayableReceivableDetailViewHolder(View itemView) {
            super(itemView);

            voucherNoLabelTextView = (TextView) itemView.findViewById(R.id.voucher_no_label_view);
            voucherNoTextView = (TextView) itemView.findViewById(R.id.voucher_no_text_view);
            dateTextView = (TextView) itemView.findViewById(R.id.date_text_view);
            totalAmtLabelTextView = (TextView) itemView.findViewById(R.id.voucher_total_amt_label_view);
            totalAmtTextView = (TextView) itemView.findViewById(R.id.voucher_total_amt_text_view);
            paidAmtTextView = (TextView) itemView.findViewById(R.id.paid_amt_text_view);
            balanceTextView = (TextView) itemView.findViewById(R.id.total_balance_text_view);

            purchaseID = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.purchase}).getString(0);
            purchaseTotal = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.purchase_total}).getString(0);
            saleID = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.sale}).getString(0);
            saleTotal = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.sales_total}).getString(0);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linear_layout);
        }
    }

    public class PaymentViewHolder extends RecyclerView.ViewHolder {

        TextView voucherNoTextView;

        TextView numberTextView;

        TextView totalAmtTextView;
        TextView dateTextView;

        public PaymentViewHolder(View itemView) {
            super(itemView);

            voucherNoTextView = (TextView) itemView.findViewById(R.id.voucher_no_text_view);
            numberTextView = (TextView) itemView.findViewById(R.id.num_text_view);
            totalAmtTextView = (TextView) itemView.findViewById(R.id.amount_text_view);
            dateTextView = (TextView) itemView.findViewById(R.id.date_text_view);
        }
    }

    public class PayableReceivableHeaderViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView;
        TextView paymentDateTextView;
        TextView voucherDateTextView;
        TextView balanceTextView;
        TextView voucherDateLabelTextView;
        TextView totalAmtTextView;
        LinearLayout outstaindingLayout;

        public PayableReceivableHeaderViewHolder(View itemView) {
            super(itemView);

            // nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            //  paymentDateTextView = (TextView) itemView.findViewById(R.id.paid_text_view);
            //  voucherDateTextView = (TextView) itemView.findViewById(R.id.voucher_date_text_view);
            // balanceTextView = (TextView) itemView.findViewById(R.id.receivable_bal_text_view);
            // voucherDateLabelTextView = (TextView) itemView.findViewById(R.id.voucher_date_label_text_view);
            // totalAmtTextView = (TextView) itemView.findViewById(R.id.sales_total_text_view);
            // outstaindingLayout = (LinearLayout) itemView.findViewById(R.id.outstanding_layout);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView voucherNoLabelTextView;

        String purchaseID;
        String saleID;

        public HeaderViewHolder(View itemView) {
            super(itemView);

            voucherNoLabelTextView = (TextView) itemView.findViewById(R.id.voucher_no_label_view);

            purchaseID = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.purchase}).getString(0);
            saleID = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.sale}).getString(0);
        }
    }

    public class PaymentHeaderViewHolder extends RecyclerView.ViewHolder {

        //TextView voucherNoLabelTextView;

        public PaymentHeaderViewHolder(View itemView) {
            super(itemView);

            // voucherNoLabelTextView = (TextView) itemView.findViewById(R.id.voucher_no_label_view);
        }
    }
}
