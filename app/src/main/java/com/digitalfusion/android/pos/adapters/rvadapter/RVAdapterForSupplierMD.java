package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Supplier;

import java.util.List;

/**
 * Created by MD003 on 11/8/16.
 */

public class RVAdapterForSupplierMD extends RecyclerView.Adapter<RVAdapterForSupplierMD.CategoryViewHolder> {

    private List<Supplier> customerVOList;

    private RVAdapterForSupplierMD.OnItemClickListener mItemClickListener;

    public RVAdapterForSupplierMD(List<Supplier> customerVOList) {

        this.customerVOList = customerVOList;

    }

    @Override
    public RVAdapterForSupplierMD.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item_view_md, parent, false);

        return new RVAdapterForSupplierMD.CategoryViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RVAdapterForSupplierMD.CategoryViewHolder holder, final int position) {

        holder.categoryTextView.setText(customerVOList.get(position).getName());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mItemClickListener != null) {

                    mItemClickListener.onItemClick(v, position);

                }
            }
        });


    }

    @Override
    public int getItemCount() {

        return customerVOList.size();

    }

    public List<Supplier> getSupplierVOList() {

        return customerVOList;

    }

    public void setSupplierVOList(List<Supplier> categoryVOList) {

        this.customerVOList = categoryVOList;

    }

    public RVAdapterForSupplierMD.OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(RVAdapterForSupplierMD.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {

        TextView categoryTextView;

        View view;

        public CategoryViewHolder(View itemView) {

            super(itemView);

            this.view = itemView;

            categoryTextView = (TextView) itemView.findViewById(R.id.category_name_tv);

        }

    }
}