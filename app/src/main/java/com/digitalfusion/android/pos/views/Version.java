package com.digitalfusion.android.pos.views;

import android.content.Context;
import android.util.AttributeSet;

import com.digitalfusion.android.pos.information.AppInfo;
import com.innovattic.font.FontTextView;

/**
 * Created by MD003 on 11/30/16.
 */

public class Version extends FontTextView {

    AppInfo appInfo = new AppInfo();

    public Version(Context context) {
        super(context);
        setText("v " + appInfo.getVersion());
    }

    public Version(Context context, AttributeSet attrs) {
        super(context, attrs);
        setText("v " + appInfo.getVersion());
    }

    public Version(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setText("v " + appInfo.getVersion());
    }


}
