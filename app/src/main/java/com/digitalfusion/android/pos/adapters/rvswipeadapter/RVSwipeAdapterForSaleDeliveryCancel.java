package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.SaleDeliveryAndPickUp;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 12/12/16.
 */

public class RVSwipeAdapterForSaleDeliveryCancel extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    public int currentDate;
    private List<SaleDeliveryAndPickUp> deliveryAndPickUpViewList;
    private ClickListener cancelClickListener;
    private ClickListener viewDetailsClicklistener;

    public RVSwipeAdapterForSaleDeliveryCancel(List<SaleDeliveryAndPickUp> deliveryAndPickUpViewList) {

        this.deliveryAndPickUpViewList = deliveryAndPickUpViewList;

        Calendar now = Calendar.getInstance();

        currentDate = Integer.parseInt(DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(now.get(Calendar.DAY_OF_MONTH))));

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.delivery_pickup_item_view_delivered, parent, false);

        return new RVSwipeAdapterForSaleDeliveryCancel.DeliveryAndPickupItemView(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof RVSwipeAdapterForSaleDeliveryCancel.DeliveryAndPickupItemView) {


            RVSwipeAdapterForSaleDeliveryCancel.DeliveryAndPickupItemView deliveryAndPickupItemView = (RVSwipeAdapterForSaleDeliveryCancel.DeliveryAndPickupItemView) holder;

            POSUtil.makeZebraStrip(deliveryAndPickupItemView.itemView, position);


            int date = Integer.parseInt(deliveryAndPickUpViewList.get(position).getPickupOrDeliveryDate());

            if (date == currentDate) {
                deliveryAndPickupItemView.dueDateTextView.setText(deliveryAndPickupItemView.today.getString(0));
            } else if (date == currentDate - 1) {
                deliveryAndPickupItemView.dueDateTextView.setText(deliveryAndPickupItemView.yesterday.getString(0));
            } else {
                deliveryAndPickupItemView.dueDateTextView.setText(DateUtility.makeDateFormatWithSlash(deliveryAndPickUpViewList.get(position).getPickupOrDeliveryYear(),
                        deliveryAndPickUpViewList.get(position).getPickupOrDeliverymonth(), deliveryAndPickUpViewList.get(position).getPickupOrDeliveryDay()));
            }
            //            deliveryAndPickupItemView.typeTextView.setText(deliveryAndPickUpViewList.get(position).getFeedbackType());
            deliveryAndPickupItemView.invoiceNoTextView.setText("#" + deliveryAndPickUpViewList.get(position).getSaleVoucherNo());
            deliveryAndPickupItemView.customerTextView.setText(deliveryAndPickUpViewList.get(position).getCustomerName());

            deliveryAndPickupItemView.cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cancelClickListener != null) {
                        cancelClickListener.onClick(position);
                    }
                }
            });

            deliveryAndPickupItemView.viewDetailsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewDetailsClicklistener != null) {
                        viewDetailsClicklistener.onClick(position);
                    }
                }
            });
        }

    }

    public ClickListener getCancelClickListener() {
        return cancelClickListener;
    }

    public void setCancelClickListener(ClickListener cancelClickListener) {
        this.cancelClickListener = cancelClickListener;
    }

    @Override
    public int getItemCount() {
        return deliveryAndPickUpViewList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public ClickListener getViewDetailsClicklistener() {
        return viewDetailsClicklistener;
    }

    public void setViewDetailsClicklistener(ClickListener viewDetailsClicklistener) {
        this.viewDetailsClicklistener = viewDetailsClicklistener;
    }

    public List<SaleDeliveryAndPickUp> getDeliveryAndPickUpViewList() {
        return deliveryAndPickUpViewList;
    }

    public void setDeliveryAndPickUpViewList(List<SaleDeliveryAndPickUp> deliveryAndPickUpViewList) {
        this.deliveryAndPickUpViewList = deliveryAndPickUpViewList;
    }

    public class DeliveryAndPickupItemView extends RecyclerView.ViewHolder {

        TextView invoiceNoTextView;
        TextView dueDateTextView;

        TextView customerTextView;
        ImageButton cancelBtn;
        ImageButton viewDetailsBtn;
        View view;
        TypedArray today;
        TypedArray yesterday;

        public DeliveryAndPickupItemView(View itemView) {
            super(itemView);

            today = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.today});

            yesterday = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday});

            this.view = itemView;

            cancelBtn = (ImageButton) itemView.findViewById(R.id.cancel);
            viewDetailsBtn = (ImageButton) itemView.findViewById(R.id.view_detail);

            invoiceNoTextView = (TextView) itemView.findViewById(R.id.voucher_no);
            dueDateTextView = (TextView) itemView.findViewById(R.id.due_date);

            customerTextView = (TextView) itemView.findViewById(R.id.customer_name);


        }

    }


}
