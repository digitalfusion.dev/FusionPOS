package com.digitalfusion.android.pos.fragments.reportfragments;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForProductBySupplier;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SimpleReportFragmentWithSearchView extends Fragment implements Serializable {
    private String reportName;
    private View mainLayoutView;
    private Context context;
    private RecyclerView recyclerView;
    private ReportManager reportManager;
    private List<ReportItem> reportItemList;
    private ParentRVAdapterForReports rvAdapterForReport;

    private String startDate;
    private String endDate;

    private int startLimit;
    private int endLimit;

    //private MaterialSearchView searchView;

    private Long stockID;

    //private StockSearchAdapter stockSearchAdapter;

    private StockManager stockManager;

    private TextView chartDescTextView;
    private String chartDesc;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_simple_report_fragment_with_search_view, container, false);
        if (getArguments() != null) {
            reportName = getArguments().getString("reportType");
            stockID = getArguments().getLong("stockID");
            chartDesc = getArguments().getString("stockName");
        }

        Log.e("detail", stockID + " " + reportName);
        if (reportName.equalsIgnoreCase("supplier by product")) {
            String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.product_by_supplier}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
        }
        setHasOptionsMenu(true);

        loadIngUI();

        //searchView.showSearch(false);
        return mainLayoutView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //searchView.showSearch(false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        initializeVariables();

        new LoadProgressDialog().execute("");
        clickListeners();
    }

    private void clickListeners() {
        //        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        //            @Override
        //            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //
        //                stockID = stockSearchAdapter.getSuggestionList().get(position).getId();
        //
        //                chartDesc = stockSearchAdapter.getSuggestionList().get(position).getName();
        //
        //                searchView.closeSearch();
        //
        //
        //                new CreateReportTask().execute("");
        //
        //            }
        //        });
        //
        //        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
        //            @Override
        //            public boolean onQueryTextSubmit(String query) {
        //                return false;
        //            }
        //
        //            @Override
        //            public boolean onQueryTextChange(String newText) {
        //
        //                return false;
        //            }
        //        });
        //
        //        chartDescTextView.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //                searchView.showSearch();
        //                searchView.showKeyboard(searchView);
        //            }
        //        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void loadIngUI() {
        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.simple_report_recycler_view);
        //        searchView=(MaterialSearchView)(this.getActivity()).findViewById(R.id.search_view);
        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        //        searchView.setCursorDrawable(attributeResourceId);
        chartDescTextView = (TextView) mainLayoutView.findViewById(R.id.stock_name_text_view);
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);
        setDatesAndLimits();
        stockManager = new StockManager(context);
        //        stockSearchAdapter = new StockSearchAdapter(context, stockManager);
        //        searchView.setAdapter(stockSearchAdapter);
    }

    private void initializeList() {
        if (reportName.equalsIgnoreCase("supplier by product")) {
            Log.e("call", "me");
            reportItemList = reportManager.supplierByProduct(stockID, startLimit, endLimit);
        }
    }

    private void setDatesAndLimits() {
        //if (!isTwelve){
        startDate = Calendar.getInstance().get(Calendar.YEAR) + "0101";
        //Log.e("start", startDate );
        endDate = Calendar.getInstance().get(Calendar.YEAR) + "1231";
        Log.e("start", endDate);
        //}
        startLimit = 0;
        endLimit = 10;
    }

    private void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        if (reportName.equalsIgnoreCase("supplier by product")) {
            rvAdapterForReport = new RVAdapterForProductBySupplier(reportItemList);
            Log.e("con", reportItemList.size() + "");
        }
        recyclerView.setAdapter(rvAdapterForReport);
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String label = "";

            String    wait          = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            hud.show();
        }

        @Override
        protected void onPostExecute(String a) {
            configRecyclerView();
            if (!reportItemList.isEmpty()) {
                String ssupplier = context.getTheme().obtainStyledAttributes(new int[]{R.attr.supplied_supplier}).getString(0);
                chartDescTextView.setText(chartDesc);
                chartDescTextView.setClickable(false);
            } else if (chartDesc != null) {
                String ssupplier = context.getTheme().obtainStyledAttributes(new int[]{R.attr.there_is_no_supplied_supplier_for}).getString(0);
                chartDescTextView.setText(ssupplier + " " + chartDesc);
                chartDescTextView.setClickable(true);
            } else {
                chartDescTextView.setClickable(true);
            }
            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    //    @Override
    //    public boolean onOptionsItemSelected(MenuItem item) {
    //        // Handle action bar item clicks here. The action bar will
    //        // automatically handle clicks on the Home/Up button, so long
    //        // as you specify a parent activity in AndroidManifest.xml.
    //        int id = item.getItemId();
    //
    //        //noinspection SimplifiableIfStatement
    //
    //        return super.onOptionsItemSelected(item);
    //    }
    //
    //    @Override
    //    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    //
    //        getActivity().getMenuInflater().inflate(R.menu.main, menu);
    //
    //        MenuItem item = menu.findItem(R.id.action_search);
    //
    //        searchView.setMenuItem(item);
    //
    //        super.onCreateOptionsMenu(menu, inflater);
    //    }
}
