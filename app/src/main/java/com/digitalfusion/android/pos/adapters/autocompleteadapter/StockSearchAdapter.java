package com.digitalfusion.android.pos.adapters.autocompleteadapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.StockManager;
import com.digitalfusion.android.pos.database.model.StockItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 11/9/16.
 */

public class StockSearchAdapter extends ArrayAdapter<StockItem> {


    private List<StockItem> suggestion;

    private List<StockItem> searchList;

    private Context context;

    private String queryText;

    private int lenght;

    private StockManager stockManager;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((StockItem) resultValue).getCodeNo();

            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null && constraint.toString().trim().length() > 0) {
                searchList.clear();

                queryText = constraint.toString().trim();

                lenght = constraint.length();

                searchList = stockManager.getStocksByNameOrCode(0, 10, queryText);

                //                Log.w("searhc size", searchList.size() + " SSS");

                FilterResults filterResults = new FilterResults();
                filterResults.values = searchList;
                filterResults.count = searchList.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (results != null && results.count > 0) {

                suggestion = (List<StockItem>) results.values;


            } else {
                suggestion = new ArrayList<>();
            }
            notifyDataSetChanged();
        }
    };

    public StockSearchAdapter(Context context, StockManager stockManager) {
        super(context, -1);

        this.context = context;

        this.stockManager = stockManager;

        suggestion = new ArrayList<>();
        searchList = new ArrayList<>();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.stock_item_auto_complete_view, parent, false);
        }
        viewHolder = new ViewHolder(convertView);

        if (!suggestion.isEmpty() && suggestion.size() > 0) {

            //            Log.w("QUERY TEXT",queryText+"   ");


            Spannable decText = new SpannableString(suggestion.get(position).getCodeNo());

            if (suggestion.get(position).getCodeNo().contains(queryText)) {

                decText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, queryText.length(), 0);
            }


            viewHolder.codeTextView.setText(decText);

            Spannable spanText = new SpannableString(suggestion.get(position).getName());

            if (suggestion.get(position).getName().contains(queryText)) {


                spanText.setSpan(new ForegroundColorSpan(getContext().getResources()

                        .getColor(R.color.accent)), 0, queryText.length(), 0);
                viewHolder.nameTextView.setText(spanText);
            } else {
                viewHolder.nameTextView.setText(suggestion.get(position).getName());
            }


        }

        Log.w("stock", "view");

        return convertView;
    }

    @Override
    public int getCount() {
        return suggestion.size();
    }

    public List<StockItem> getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(List<StockItem> suggestion) {
        this.suggestion = suggestion;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }


    @Nullable
    @Override
    public StockItem getItem(int position) {
        return suggestion.get(position);
    }

    static class ViewHolder {
        TextView codeTextView;
        TextView nameTextView;

        public ViewHolder(View itemView) {
            this.nameTextView = (TextView) itemView.findViewById(R.id.name);
            this.codeTextView = (TextView) itemView.findViewById(R.id.code);
        }
    }

}