package com.digitalfusion.android.pos.database.dao;

import android.content.Context;

import com.digitalfusion.android.pos.database.DatabaseHelper;
import com.digitalfusion.android.pos.util.AppConstant;

/**
 * Created by MD002 on 8/17/16.
 */
public class IdGeneratorDAO extends ParentDAO {
    private static ParentDAO idGeneratorDaoInstance;
    private Context context;

    private IdGeneratorDAO(Context context) {
        super(context);
        this.context = context;
        databaseHelper = DatabaseHelper.getHelperInstance(context);
    }

    public static IdGeneratorDAO getIdGeneratorDaoInstance(Context context) {
        if (idGeneratorDaoInstance == null) {
            idGeneratorDaoInstance = new IdGeneratorDAO(context);
        }
        return (IdGeneratorDAO) idGeneratorDaoInstance;
    }

    public boolean addNewID(String tableName, Integer value) {
        query = "insert into " + AppConstant.ID_GENERATOR_TABLE_NAME + "(" + AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + ", " + AppConstant.ID_GENERATOR_VALUE + ") values(?,?)";
        database = databaseHelper.getWritableDatabase();
        database.compileStatement(query);
        statement.bindString(1, tableName);
        statement.bindString(2, value.toString());
        statement.execute();
        statement.clearBindings();
        return true;
    }

    public Long getLastIdValue(String tableName) {
        Long value = 0L;
        query = "select value from " + AppConstant.ID_GENERATOR_TABLE_NAME + " where " + AppConstant.ID_GENERATOR_TABLE_NAME_DESCRIPTION + "=?";
        database = databaseHelper.getWritableDatabase();
        cursor = database.rawQuery(query, new String[]{tableName});
        if (cursor.moveToFirst()) {
            value = cursor.getLong(0);
        }
        cursor.close();
        return value;
    }
}
