package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForUserRoleMD;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForUserRoleList;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.UserManager;
import com.digitalfusion.android.pos.database.dao.ApiDAO;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.digitalfusion.android.pos.views.FusionToast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 8/24/16.
 */
public class UserRoleSettingFragment extends Fragment {

    private Context context;
    private UserManager userManager;

    private View mainLayoutView;
    //UI ID
    private RecyclerView recyclerView;
    private FloatingActionButton addUserFab;

    //MD Add ID
    private MaterialDialog userMaterialDialog;
    private EditText userName;
    //    private EditText userPassword;
    //    private ImageButton showPassword;
    //    private EditText userConfirmPassword;
    //    private ImageButton showConfirmPassword;
    private TextView userRole;
    private EditText userDescription;
    private MDButton saveMdButton;

    //MD Edit ID
    private MaterialDialog editUserMaterialDialog;
    private EditText editUserName;
    private TextView editUserRole;
    private EditText editUserDescription;
    private MDButton editSaveMdButton;

    //MD's UserRole MD
    private RVAdapterForUserRoleMD rvAdapterForUserRoleMD;
    private MaterialDialog userRoleChooserMD;

    //UserList
    private RVSwipeAdapterForUserRoleList rvSwipeAdapterForUserRoleList;
    private List<User> userList;

    private MaterialDialog deleteAlertDialog;
    private Button yesDeleteMdButton;

    private List<String> roleList;
    private boolean editFlag;
    private String role;
    private int editposition;
    private int deletePos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        context = getContext();
        mainLayoutView = inflater.inflate(R.layout.user_role, null);
        userManager = new UserManager(context);
        userList = new ArrayList<>();
        role = "Admin";
        roleList = new ArrayList<>();
        roleList.add("Admin");
        roleList.add("Sale");
        roleList.add("Purchase");
        roleList.add("Staff");
        editFlag = false;

        MainActivity.setCurrentFragment(this);

        String user_str = ThemeUtil.getString(context, R.attr.user_role);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(user_str);

        buildingAddUserDialog();
        buildingEditUserDialog();
        buildUserRoleChooserDialog(roleList);
        loadUI();
        configUI();
        buildDeleteAlertDialog();

        //        userPassword.setInputType(129);
        //
        //        showPassword.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //                if (showPassword.isSelected()) {
        //                    userPassword.setInputType(129);
        //                    showPassword.setSelected(false);
        //                } else {
        //                    userPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        //                    showPassword.setSelected(true);
        //                }
        //                userPassword.setSelection(userPassword.getText().length());
        //            }
        //        });
        //
        //        userConfirmPassword.setInputType(129);
        //
        //        showConfirmPassword.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //                if (showConfirmPassword.isSelected()) {
        //                    userConfirmPassword.setInputType(129);
        //                    showConfirmPassword.setSelected(false);
        //                } else {
        //                    userConfirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        //                    showConfirmPassword.setSelected(true);
        //                }
        //                userConfirmPassword.setSelection(userConfirmPassword.getText().length());
        //            }
        //        });

        //region setting up listeners
        addUserFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   clearInput();
                // userMaterialDialog.show();

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);
                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_USER);
                startActivity(addCurrencyIntent);
            }
        });

        rvSwipeAdapterForUserRoleList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
              /*  editFlag=true;
                editposition=postion;
                setDataForUserDialog(userList.get(postion));

                editUserMaterialDialog.show();*/
                Bundle bundle = new Bundle();
                bundle.putSerializable(AddEditUserFragment.KEY, userList.get(postion));
                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);
                addCurrencyIntent.putExtras(bundle);
                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_USER);
                startActivity(addCurrencyIntent);
            }
        });

        userMaterialDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                editFlag = false;
            }
        });

        rvSwipeAdapterForUserRoleList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

            }
        });

        userRole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userRoleChooserMD.show();
            }
        });

        saveMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkVaildation()) {

                    //                    userManager.addNewUser(userName.getText().toString().trim(), userPassword.getText().toString().trim(),
                    //                            editUserRole.getText().toString(), userDescription.getText().toString());
                    //

                    refreshRecyclerList();

                    userMaterialDialog.dismiss();
                }
            }
        });

        editSaveMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //userManager.updateUserRole(userPassword.getText().toString().trim(),
                //userList.get(editposition).getId());
                User temp = userList.get(editposition);

                //                Boolean status = userManager.updateUserRole(temp.getUserName(), temp.getPassword(), editUserRole.getText().toString(), editUserDescription.getText().toString(), temp.getId());

                //                Log.e("status", status + " p");

                refreshRecyclerList();

                editUserMaterialDialog.dismiss();
            }
        });

        editUserRole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editFlag = true;
                userRoleChooserMD.show();
            }
        });

        rvAdapterForUserRoleMD.setmItemClickListener(new RVAdapterForUserRoleMD.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (editFlag) {
                    editUserRole.setText(roleList.get(position).toString());
                } else {
                    userRole.setText(roleList.get(position).toString());
                }

                role = roleList.get(position);
                userRoleChooserMD.dismiss();
            }
        });

        rvSwipeAdapterForUserRoleList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                if (postion != 0) {
                    deletePos = postion;
                    deleteAlertDialog.show();
                }
            }
        });

        yesDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();

                long             deleteUserId     = userList.get(deletePos).getId();
                AccessLogManager accessLogManager = new AccessLogManager(context);
                if (accessLogManager.isUserLoggedIn(deleteUserId)) {
                    FusionToast.toast(context, FusionToast.TOAST_TYPE.ERROR, "User is currently logged in.");
                } else {
                    ApiDAO.getApiDAOInstance(context).deleteUser(deleteUserId);

                    userList.remove(deletePos);

                    rvSwipeAdapterForUserRoleList.setUserList(userList);
                    rvSwipeAdapterForUserRoleList.notifyItemRemoved(deletePos);
                    rvSwipeAdapterForUserRoleList.notifyItemRangeChanged(deletePos, userList.size());
                }

            }
        });
        //endregion

        return mainLayoutView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshRecyclerList();
    }

    private void setDataForUserDialog(@Nullable User user) {

        editUserName.setText(user.getUserName());

        editUserRole.setText(user.getRole());

        editUserDescription.setText(user.getDescription());
    }

    private boolean checkVaildation() {

        boolean status = true;

        if (userName.getText().toString().trim().length() < 1) {

            userName.setError("Enter User name");

            status = false;
        }

        //
        //        if (userPassword.getText().toString().trim().length() < 4) {
        //
        //            userPassword.setError("Password must be at least 4 letters");
        //
        //            status = false;
        //
        //        }
        //
        //
        //        if (userConfirmPassword.getText().toString().equals(userPassword.getText().toString())) {
        //
        //            userConfirmPassword.setError("Password Not Match");
        //
        //            status = false;
        //
        //        }

        //        if (passco)

        return status;
    }

    public void loadUI() {

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.user_list_rv);
        addUserFab = (FloatingActionButton) mainLayoutView.findViewById(R.id.add_new_user);

        //Add User Role MD
        saveMdButton = userMaterialDialog.getActionButton(DialogAction.POSITIVE);
        userName = (EditText) userMaterialDialog.findViewById(R.id.name_in_add_user_role_TIET);
        //        userPassword = (EditText) userMaterialDialog.findViewById(R.id.password_in_add_user_role_TIET);
        //        showPassword = (ImageButton) userMaterialDialog.findViewById(R.id.showpassword);
        //        userConfirmPassword = (EditText) userMaterialDialog.findViewById(R.id.confirm_password_in_add_user_role_TIET);
        //        showConfirmPassword = (ImageButton) userMaterialDialog.findViewById(R.id.showconfirmpassword);
        userRole = (TextView) userMaterialDialog.findViewById(R.id.role_in_add_user_role_TIET);
        userDescription = (EditText) userMaterialDialog.findViewById(R.id.description_in_add_user_role_TIET);

        //Edit User Role MD
        editSaveMdButton = editUserMaterialDialog.getActionButton(DialogAction.POSITIVE);
        editUserName = (EditText) editUserMaterialDialog.findViewById(R.id.name_in_add_user_role_TIET);
        editUserRole = (TextView) editUserMaterialDialog.findViewById(R.id.role_in_add_user_role_TIET);
        editUserDescription = (EditText) editUserMaterialDialog.findViewById(R.id.description_in_add_user_role_TIET);
    }

    public void configUI() {
        userList = ApiDAO.getApiDAOInstance(context).getAllUserRoles();
        rvSwipeAdapterForUserRoleList = new RVSwipeAdapterForUserRoleList(userList);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(rvSwipeAdapterForUserRoleList);
    }

    private void refreshRecyclerList() {
        userList = ApiDAO.getApiDAOInstance(context).getAllUserRoles();
        rvSwipeAdapterForUserRoleList.setUserList(userList);

        rvSwipeAdapterForUserRoleList.notifyDataSetChanged();
    }

    private void clearInput() {
        userName.setError(null);
        userName.setText(null);

        //        userPassword.setError(null);
        //        userPassword.setText(null);

        userRole.setText(null);
        userRole.setError(null);

        userDescription.setText(null);
        userDescription.setError(null);
    }

    private void buildingAddUserDialog() {

        userMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.add_user_role, true)
                .negativeText("Cancel")
                .positiveText("Save")
                .title("Add User Role")
                .build();
    }

    private void buildingEditUserDialog() {
        editUserMaterialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.edit_userrole, true)
                .negativeText("Cancel")
                .positiveText("Save")
                .title("Edit User Role")
                .build();
    }

    private void buildUserRoleChooserDialog(List<String> userRoles) {

        rvAdapterForUserRoleMD = new RVAdapterForUserRoleMD(userRoles);

        userRoleChooserMD = new MaterialDialog.Builder(context).adapter(rvAdapterForUserRoleMD, null).build();
    }

    private void buildDeleteAlertDialog() {

        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

        String   sureToDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete}).getString(0);
        TextView textView     = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(sureToDelete);

        yesDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });
    }
}