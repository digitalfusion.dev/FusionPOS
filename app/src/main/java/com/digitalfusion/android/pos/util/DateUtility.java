package com.digitalfusion.android.pos.util;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by MD002 on 8/17/16.
 */
public class DateUtility {
    //new

    static String[] mMonths = new String[]{
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };

    public DateUtility() {
    }

    /**
     * @return Format yyyy-MM-dd HH:mm:ss
     */
    public static String getCurrentDateTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(Calendar.getInstance().getTime());
    }

    public static String makeDateFromSlash(String date) {

        return date.replace("/", "");
    }

    public static String getTodayDate() {
        Calendar calendar = Calendar.getInstance();
        return DateUtility.makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
    }

    public static String getYesterDayDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 1);
        return DateUtility.makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
    }


    public static String getThisMonthStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return DateUtility.makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
    }

    public static String getLast12MonthStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -11);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return DateUtility.makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
    }


    public static String getLastMonthStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MONTH, -1);
        return DateUtility.makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
    }


    public static String getThisYearStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, 0);
        return DateUtility.makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
    }

    public static String getThisYearEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 11);

        calendar.set(Calendar.DAY_OF_MONTH, getMonthEndDay(calendar));

        return DateUtility.makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
    }

    public static String getLastYearStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, 0);
        calendar.add(Calendar.YEAR, -1);
        return DateUtility.makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
    }

    public static String getLastYearEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DAY_OF_MONTH, getMonthEndDay(calendar));

        return DateUtility.makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
    }

    public static String getLastMonthEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        int day = getMonthEndDay(calendar);

        return DateUtility.makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(day));
    }

    public static String getThisMonthEndDate() {
        Calendar calendar = Calendar.getInstance();
        int      day      = getMonthEndDay(calendar);

        return DateUtility.makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(day));
    }

    public static String getLast12MotnEndDate() {
        Calendar calendar = Calendar.getInstance();
        int      day      = getMonthEndDay(calendar);

        return DateUtility.makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(day));
    }

    public static String makeDate(String year, String month, String day) {
        String date;
        if (Integer.parseInt(month) < 10) {
            if (Integer.parseInt(day) < 10) {
                date = year + "0" + month + "0" + day;
            } else {
                date = year + "0" + month + day;
            }
        } else {
            if (Integer.parseInt(day) < 10) {
                date = year + month + "0" + day;
            } else {
                date = year + month + day;
            }
        }
        return date;
    }

    public static String makeDate(Date indate) {
        String   month;
        String   year;
        String   day;
        String   date;
        Calendar c = Calendar.getInstance();
        c.setTime(indate);
        year = String.valueOf(c.get(Calendar.YEAR));
        month = String.valueOf(c.get(Calendar.MONTH));
        day = String.valueOf(c.get(Calendar.DAY_OF_MONTH));

        if (Integer.parseInt(month) < 10) {
            if (Integer.parseInt(day) < 10) {
                date = year + "0" + month + "0" + day;
            } else {
                date = year + "0" + month + day;
            }
        } else {
            if (Integer.parseInt(day) < 10) {
                date = year + month + "0" + day;
            } else {
                date = year + month + day;
            }
        }
        return date;
    }

    public static int getMonthEndDate(Calendar calendar) {
        int endday = 1;
        if (calendar.get(Calendar.MONTH) == 0) {
            endday = 31;
        } else if (calendar.get(Calendar.MONTH) == 1) {
            calendar.set(Calendar.DAY_OF_MONTH, 29);
            if (calendar.get(Calendar.DAY_OF_MONTH) == 29) {
                endday = 29;
            } else {
                endday = 28;
                calendar.set(Calendar.MONTH, 1);
                calendar.set(Calendar.DAY_OF_MONTH, 28);
            }
        } else if (calendar.get(Calendar.MONTH) == 2) {
            endday = 31;
        } else if (calendar.get(Calendar.MONTH) == 3) {
            endday = 30;

        } else if (calendar.get(Calendar.MONTH) == 4) {
            endday = 31;

        } else if (calendar.get(Calendar.MONTH) == 5) {
            endday = 30;
        } else if (calendar.get(Calendar.MONTH) == 6) {
            endday = 31;
        } else if (calendar.get(Calendar.MONTH) == 7) {
            endday = 31;
        } else if (calendar.get(Calendar.MONTH) == 8) {
            endday = 30;
        } else if (calendar.get(Calendar.MONTH) == 9) {
            endday = 31;
        } else if (calendar.get(Calendar.MONTH) == 10) {
            endday = 30;
        } else {
            endday = 31;
        }
        return endday;
    }

    public static Calendar getStartDateOfWeek() {
        Calendar calendar     = Calendar.getInstance();
        int      weekDistance = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        calendar.add(Calendar.DAY_OF_MONTH, -weekDistance);
        return calendar;
    }

    public static String getStartDateOfWeekString() {
        Calendar calendar     = Calendar.getInstance();
        int      weekDistance = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        calendar.add(Calendar.DAY_OF_MONTH, -weekDistance);
        return makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
    }

    public static String getEndDateOfWeekString() {
        Calendar calendar = getStartDateOfWeek();
        calendar.add(Calendar.DAY_OF_MONTH, 6);
        return makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));

    }

    public static String getStartDateOfLastWeekString() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.WEEK_OF_MONTH, -1);
        int weekDistance = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        calendar.add(Calendar.DAY_OF_MONTH, -weekDistance);
        return makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));
    }

    public static String getEndDateOfLastWeekString() {
        Calendar calendar = getStartDateOfWeek();
        calendar.add(Calendar.WEEK_OF_MONTH, -1);
        calendar.add(Calendar.DAY_OF_MONTH, 6);
        return makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));

    }

    public static String getEndDateOfTwoWeekString() {
        Calendar calendar = getStartDateOfWeek();
        calendar.add(Calendar.WEEK_OF_MONTH, 2);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return makeDate(Integer.toString(calendar.get(Calendar.YEAR)), Integer.toString(calendar.get(Calendar.MONTH) + 1), Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));

    }

    public static Calendar getEndDateOfWeek() {
        Calendar calendar = getStartDateOfWeek();
        calendar.add(Calendar.DAY_OF_MONTH, 6);
        return calendar;
    }

    public static int getMonthEndDay(Calendar calendar) {
        int endday = 1;

        switch (calendar.get(Calendar.MONTH)) {
            case 1: {
                calendar.set(Calendar.DAY_OF_MONTH, 29);

                if (calendar.get(Calendar.DAY_OF_MONTH) == 29) {
                    endday = 29;
                } else {
                    endday = 28;
                    calendar.set(Calendar.MONTH, 1);
                    calendar.set(Calendar.DAY_OF_MONTH, 28);
                }
            }
            break;

            case 3 | 5 | 8 | 10:
                endday = 30;
                break;

            default:
                endday = 31;
        }


        return endday;
    }

    public static String makeDateFormatWithSlash(String year, String month, String day) {
        String date;
        if (Integer.parseInt(month) < 10) {
            if (Integer.parseInt(day) < 10) {
                date = "0" + day + "/" + "0" + month + "/" + year;
            } else {
                date = day + "/" + "0" + month + "/" + year;
            }
        } else {
            if (Integer.parseInt(day) < 10) {
                date = "0" + day + "/" + month + "/" + year;
            } else {
                date = day + "/" + month + "/" + year;
            }
        }
        return date;
    }

    public static String makeDateFormatWithSlash(String date) {
        String outputDate;
        outputDate = date.substring(6, 8) + "/" + date.substring(4, 6) + "/" + date.substring(0, 4);
        return outputDate;
    }

    public static int dateDifference(Calendar c2) {
        Calendar c1 = Calendar.getInstance();
        int      i  = 0;
        long     d1 = Long.parseLong(makeDate(Integer.toString(c1.get(Calendar.YEAR)), Integer.toString(c1.get(Calendar.MONTH)), Integer.toString(c1.get(Calendar.DAY_OF_MONTH))));
        long     d2 = Long.parseLong(makeDate(Integer.toString(c2.get(Calendar.YEAR)), Integer.toString(c2.get(Calendar.MONTH)), Integer.toString(c2.get(Calendar.DAY_OF_MONTH))));

        while (d1 < d2) {
            i++;
            c1.add(Calendar.DAY_OF_MONTH, 1);
            d1 = Long.parseLong(makeDate(Integer.toString(c1.get(Calendar.YEAR)), Integer.toString(c1.get(Calendar.MONTH)), Integer.toString(c1.get(Calendar.DAY_OF_MONTH))));

        }

        if (d2 < d1) {
            i = -1;
        }

        return i;

    }

    //new
    public static String monthYearDes(String startDate) {
        String output = "";
        String temp   = "";
        //Log.e("input",startDate);
        for (int i = 5; i >= 0; i--) {
            temp += startDate.charAt(i);
           /* if (i==6){
                String tempo=new StringBuilder(temp).reverse().toString();
                output=dayDes(tempo);
                temp="";
            }*/
            if (i == 4) {
                if (temp.charAt(1) == '0') {
                    temp = "" + temp.charAt(0);
                    if (temp.charAt(0) == '0' || temp.charAt(0) == '1') {
                        //Log.e("iii",temp);
                        output += " " + mMonths[0];
                    } else {
                        output += " " + mMonths[Integer.parseInt(temp) - 1];
                    }
                } else {

                    String tempo = new StringBuilder(temp).reverse().toString();

                    Log.w("sadfsadf", Integer.parseInt(tempo) - 1 + " ss");

                    output += " " + mMonths[Integer.parseInt(tempo) - 1];
                }
                temp = "";
            }
            if (i == 0) {
                String tempo = new StringBuilder(temp).reverse().toString();
                output += ", " + tempo;
            }
        }

        //Log.e("output",output);
        return output;
    }

    //new
    public static String[] dayDes(String day) {
        //Log.e("input", day);
        String output = "";
        if (day.charAt(6) != '0') {
            output += day.charAt(6);
        }
        if (day.charAt(7) == '1') {
            output += "1st";
        } else if (day.charAt(7) == '2') {
            output += "2nd";
        } else if (day.charAt(7) == '3') {
            output += "3rd";
        } else {
            output += day.charAt(7) + "th";
        }
        //Log.e("output",output);
        String[] op = new String[]{"", ""};
        for (Character c : output.toCharArray()) {
            if (Character.isLetter(c)) {
                if (c.toString() != null) {
                    op[0] += c.toString();
                }
                //Log.e("output",op[0]);
            } else {
                if (c.toString() != null) {
                    op[1] += c.toString();
                }
            }
        }
        return op;
    }

    public String getTodayDateFromatForSummary() {

        Calendar calendar = Calendar.getInstance();
        String   rdate    = "";
        switch (calendar.get(Calendar.MONTH)) {
            case 0:
                rdate += " Jan " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
                break;
            case 1: {
                rdate += "Feb " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
            }
            break;
            case 2:
                rdate += "Mar " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
                break;

            case 3:
                rdate += "Apr " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
                break;

            case 4:
                rdate += "May " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
                break;

            case 5:
                rdate += "Jun " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
                break;

            case 6:
                rdate += "Jul " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
                break;

            case 7:
                rdate += "Aug " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
                break;

            case 8:
                rdate += "Sept " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
                break;
            case 9:
                rdate += "Oct " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
                break;

            case 10:
                rdate += "Nov " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
                break;

            case 11:
                rdate += "Dec " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
                break;
        }
        rdate += ", " + Integer.toString(calendar.get(Calendar.YEAR));
        return rdate;
    }

    public String getWeekDateFromatForSummary() {
        Calendar calendar = getStartDateOfWeek();
        String   rdate    = "";
        switch (calendar.get(Calendar.MONTH)) {
            case 0:
                rdate += " Jan " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;
            case 1: {
                rdate += "Feb " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
            }
            break;
            case 2:
                rdate += "Mar " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 3:
                rdate += "Apr " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 4:
                rdate += "May " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 5:
                rdate += "Jun " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 6:
                rdate += "Jul " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 7:
                rdate += "Aug " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 8:
                rdate += "Sept " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;
            case 9:
                rdate += "Oct " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 10:
                rdate += "Nov " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 11:
                rdate += "Dec " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;
        }

        calendar = getEndDateOfWeek();
        switch (calendar.get(Calendar.MONTH)) {
            case 0:
                rdate += " ~ Jan " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;
            case 1: {
                rdate += " ~ Feb " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));

            }
            break;

            case 2:
                rdate += " ~ Mar " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 3:
                rdate += " ~ Apr " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 4:
                rdate += " ~ May " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 5:
                rdate += " ~ Jun " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 6:
                rdate += " ~ Jul " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 7:
                rdate += " ~ Aug " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 8:
                rdate += " ~ Sept " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;
            case 9:
                rdate += " ~ Oct " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 10:
                rdate += " ~ Nov " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;

            case 11:
                rdate += " ~ Dec " + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + ", " + Integer.toString(calendar.get(Calendar.YEAR));
                break;
        }
        return rdate;

    }

    public String getMonthDateFromatForSummary() {

        Calendar calendar = Calendar.getInstance();

        String rdate = "";

        switch (calendar.get(Calendar.MONTH)) {
            case 0:
                rdate += " Jan ";
                break;
            case 1: {
                rdate += "Feb ";

            }
            break;

            case 2:
                rdate += "Mar ";
                break;

            case 3:
                rdate += "Apr ";
                break;

            case 4:
                rdate += "May ";
                break;

            case 5:
                rdate += "Jun ";
                break;

            case 6:
                rdate += "Jul ";
                break;

            case 7:
                rdate += "Aug ";
                break;

            case 8:
                rdate += "Sept ";
                break;
            case 9:
                rdate += "Oct ";
                break;

            case 10:
                rdate += "Nov ";
                break;

            case 11:
                rdate += "Dec ";
                break;


        }
        rdate += Integer.toString(calendar.get(Calendar.YEAR));
        return rdate;

    }
}
