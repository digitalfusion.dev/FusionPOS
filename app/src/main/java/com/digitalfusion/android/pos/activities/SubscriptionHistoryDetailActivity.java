package com.digitalfusion.android.pos.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.information.Subscription;
import com.digitalfusion.android.pos.util.POSUtil;

import java.text.SimpleDateFormat;

public class SubscriptionHistoryDetailActivity extends ParentActivity {
    private Toolbar toolbar;
    private TextView providerNameTextView;
    private TextView bankNameTextView;
    private TextView amountTextView;
    private TextView licenceTypeTextView;
    private TextView startDateTextView;
    private TextView endDateTextView;
    private TextView durationTextView;
    private TextView licenceStatusTextView;
    private TextView usernameTextView;
    private TextView businessNameTextView;
    private TextView phoneNoTextView;
    private TextView addressTextView;
    private TextView emailTextView;
    private LinearLayout paymentInfoLinearLayout;
    private LinearLayout licenceInfoLinearLayout;
    private LinearLayout registrationLinearLayout;
    private ImageView paymentInfoArrow;
    private ImageView licenceInfoArrow;
    private ImageView registrationArrow;
    private Subscription subscription;
    private LinearLayout paymentInfoTitleLinearLayout;
    private LinearLayout licenceInfoTitleLinearLayout;
    private LinearLayout registrationTitleLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(POSUtil.getDefaultThemeNoActionBar(this));
        setContentView(R.layout.activity_subscription_history_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String license = this.getTheme().obtainStyledAttributes(new int[]{R.attr.licence_information}).getString(0);
        getSupportActionBar().setTitle(license);
        subscription = (Subscription) getIntent().getSerializableExtra("subscription");
        configUI();
        viewUI();
    }

    private void viewUI() {
        //providerNameTextView.setText(subscription.getPaymentInfo().getPaymentProvider());
        if (subscription.getLicenseInfo().getLicenseStatus().equalsIgnoreCase("EXPIRED")) {
            licenceStatusTextView.setBackgroundColor(Color.parseColor("#DD2C00"));
        } else if (subscription.getLicenseInfo().getLicenseStatus().equalsIgnoreCase("ACTIVE")) {
            licenceStatusTextView.setBackgroundColor(Color.parseColor("#4CAF50"));
        } else {
            licenceStatusTextView.setBackgroundColor(Color.parseColor("#FFF1C70E"));
        }
        providerNameTextView.setText(subscription.getPaymentInfo().getAgentName());
        bankNameTextView.setText(subscription.getPaymentInfo().getBank());
        amountTextView.setText(subscription.getPaymentInfo().getAmount().toString());
        licenceTypeTextView.setText(subscription.getLicenseInfo().getLicenceType());
        startDateTextView.setText(new SimpleDateFormat("dd/MM/yyyy").format(subscription.getLicenseInfo().getStartDate()));
        endDateTextView.setText(new SimpleDateFormat("dd/MM/yyyy").format(subscription.getLicenseInfo().getEndDate()));
        durationTextView.setText(subscription.getLicenseInfo().getDuration().toString());
        licenceStatusTextView.setText(subscription.getLicenseInfo().getLicenseStatus());
        usernameTextView.setText(subscription.getRegistration().getUserName());
        businessNameTextView.setText(subscription.getRegistration().getBusinessName());
        phoneNoTextView.setText(subscription.getRegistration().getPhone());
        emailTextView.setText(subscription.getRegistration().getEmail());
        addressTextView.setText(subscription.getRegistration().getAddress());
        paymentInfoTitleLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (paymentInfoLinearLayout.isShown()) {

                    paymentInfoArrow.setSelected(true);

                    paymentInfoLinearLayout.setVisibility(View.GONE);

                } else {

                    paymentInfoArrow.setSelected(false);

                    paymentInfoLinearLayout.setVisibility(View.VISIBLE);

                }
            }
        });
        licenceInfoTitleLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (licenceInfoLinearLayout.isShown()) {

                    licenceInfoArrow.setSelected(true);

                    licenceInfoLinearLayout.setVisibility(View.GONE);

                } else {

                    licenceInfoArrow.setSelected(false);

                    licenceInfoLinearLayout.setVisibility(View.VISIBLE);

                }
            }
        });
        registrationTitleLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (registrationLinearLayout.isShown()) {

                    registrationArrow.setSelected(true);

                    registrationLinearLayout.setVisibility(View.GONE);

                } else {

                    registrationArrow.setSelected(false);

                    registrationLinearLayout.setVisibility(View.VISIBLE);

                }
            }
        });
    }

    private void configUI() {
        bankNameTextView = findViewById(R.id.bank);
        providerNameTextView = (TextView) findViewById(R.id.provider_name_text_view);
        amountTextView = (TextView) findViewById(R.id.amount_text_view);
        licenceTypeTextView = (TextView) findViewById(R.id.license_type_text_view);
        startDateTextView = (TextView) findViewById(R.id.start_date_text_view);
        endDateTextView = (TextView) findViewById(R.id.end_date_text_view);
        durationTextView = (TextView) findViewById(R.id.duration_text_view);
        licenceStatusTextView = (TextView) findViewById(R.id.license_status_text_view);
        usernameTextView = (TextView) findViewById(R.id.user_name_text_view);
        businessNameTextView = (TextView) findViewById(R.id.business_name_text_view);
        phoneNoTextView = (TextView) findViewById(R.id.phone_no_text_view);
        addressTextView = (TextView) findViewById(R.id.address_text_view);
        emailTextView = (TextView) findViewById(R.id.email_text_view);
        paymentInfoLinearLayout = (LinearLayout) findViewById(R.id.payment_info_linear_layout);
        licenceInfoLinearLayout = (LinearLayout) findViewById(R.id.license_info_linear_layout);
        registrationLinearLayout = (LinearLayout) findViewById(R.id.registration_linear_layout);
        paymentInfoArrow = (ImageView) findViewById(R.id.payment_info_arrow);
        licenceInfoArrow = (ImageView) findViewById(R.id.license_info_arrow);
        registrationArrow = (ImageView) findViewById(R.id.registration_arrow);
        paymentInfoTitleLinearLayout = (LinearLayout) findViewById(R.id.payment_info_title_linear_layout);
        licenceInfoTitleLinearLayout = (LinearLayout) findViewById(R.id.licence_info_title_linear_layout);
        registrationTitleLinearLayout = (LinearLayout) findViewById(R.id.registration_title_linear_layout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
