package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.LoaderViewHolder;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.database.model.Category;

import java.util.List;

/**
 * Created by MD003 on 8/19/16.
 */
public class


RVAdapterForCategoryMD extends ParentRVAdapterForReports {

    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    private List<Category> categoryList;
    private OnItemClickListener mItemClickListener;

    public RVAdapterForCategoryMD(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    @Override
    public int getItemViewType(int position) {
        if (position != 0 && position == getItemCount() - 1) {

            if (reportItemList != null && reportItemList.size() != 0) {

                return VIEWTYPE_LOADER;

            }

        }
        return VIEWTYPE_ITEM;


    }

    @Override
    public RVAdapterForCategoryMD.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item_view_md, parent, false);

        return new CategoryViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof LoaderViewHolder) {

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);

        } else if (viewHolder instanceof CategoryViewHolder) {
            CategoryViewHolder holder = (CategoryViewHolder) viewHolder;
            holder.categoryTextView.setText(categoryList.get(position).getName());

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.w("Click ", "aaa");
                    if (mItemClickListener != null) {
                        Log.w("click ", "ateab");
                        mItemClickListener.onItemClick(v, position);

                    }
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView categoryTextView;

        View view;

        public CategoryViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            categoryTextView = (TextView) itemView.findViewById(R.id.category_name_tv);

        }

    }
}
