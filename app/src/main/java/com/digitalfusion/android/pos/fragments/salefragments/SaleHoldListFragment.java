package com.digitalfusion.android.pos.fragments.salefragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.SaleHistorySearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForSaleTransactions;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.AuthorizationManager;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.business.SalesOrderManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.example.searchview.MaterialSearchView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 2/20/18.
 */

public class SaleHoldListFragment extends Fragment {

    String allTrans;
    String thisWeek;
    String lastWeek;
    String thisMonth;
    String lastMonth;
    String thisYear;
    String lastYear;
    String customRange;
    String customDate;
    private View mainLayout;
    private LinearLayout linearLayout;
    private TextView noTransactionTextView;
    private FloatingActionButton addNewSaleFab;
    private RecyclerView saleHistoryListRecyclerView;
    private Context context;
    private SalesManager salesManager;
    private List<SalesHistory> salesHistoryList;
    private RVSwipeAdapterForSaleTransactions rvSwipeAdapterForSaleTransactions;
    private MaterialSearchView searchView;
    private MaterialDialog filterDialog;
    private TextView filterTextView;
    private TextView searchedResultTxt;
    private RVAdapterForFilter rvAdapterForFilter;
    private List<String> filterList;
    private SalesOrderManager salesOrderManager;
    private String startDate;
    private String endDate;
    private DatePickerDialog customeDatePickerDialog;
    private Calendar calendar;
    private SaleHistorySearchAdapter saleHistorySearchAdapter;
    private DatePickerDialog startDatePickerDialog;
    private MaterialDialog customRangeDialog;
    private TextView startDateTextView, endDateTextView;
    private String customStartDate,
            customEndDate;
    private TextView traceDate;
    private int oldPos = 0;
    private int current = 0;
    private Button customRangeOkBtn, customRangeCancelBtn;
    private boolean isSearch = false;
    private boolean shouldLoad = true;
    private String searchText = "";
    private MaterialDialog refundDialog;
    private Button confirmMdButton;
    private Button cancelMdButton;
    private boolean isCancel;
    private int refundPos;
    private TextView totalAmountInRefundTextView;
    private TextView paidAmoutInRefundTextView;
    private EditText refundEditText;
    private EditText remarkInRefundEditText;
    private Double refundAmt = 0.0;
    private LinearLayout refundDateLinearLayout;
    private TextView refundDateTextView;
    private DatePickerDialog refundDatePickerDialog;
    private int refundDay;
    private int refundMonth;
    private int refundYear;
    private String date;
    private Calendar now;
    private MaterialDialog saleDeleteAlertDialog;
    private Button yesSaleDeleteMdButton;
    private int deletepos;
    private SalesHistory deleteValue;

    private boolean darkmode;
    private boolean isAdd;

    private SalesHistory salesHistory;
    private String userId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainLayout = inflater.inflate(R.layout.sale_history_list, null);
        salesHistoryList = new ArrayList<>();
        context = getContext();
        darkmode = POSUtil.isNightMode(context);

        TypedArray title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.sale_history});

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title.getString(0));

        // setHasOptionsMenu(true);

        calendar = Calendar.getInstance();

        mainLayout.findViewById(R.id.filter_display).setVisibility(View.GONE);
        mainLayout.findViewById(R.id.filter_sperater).setVisibility(View.GONE);
        mainLayout.findViewById(R.id.filter_one).setVisibility(View.GONE);
        mainLayout.findViewById(R.id.txt_filter_seller).setVisibility(View.GONE);
        mainLayout.findViewById(R.id.filter_seller_sperater).setVisibility(View.GONE);
        mainLayout.findViewById(R.id.filter_seller).setVisibility(View.GONE);
        now = Calendar.getInstance();

        refundDay = now.get(Calendar.DAY_OF_MONTH);
        refundMonth = now.get(Calendar.MONTH) + 1;
        refundYear = now.get(Calendar.YEAR);

        MainActivity.setCurrentFragment(this);

        allTrans = ThemeUtil.getString(context, R.attr.all);
        thisMonth = ThemeUtil.getString(context, R.attr.this_month);
        lastMonth = ThemeUtil.getString(context, R.attr.last_month);
        thisYear = ThemeUtil.getString(context, R.attr.this_year);
        lastYear = ThemeUtil.getString(context, R.attr.last_year);
        customRange = ThemeUtil.getString(context, R.attr.custom_range);
        customDate = ThemeUtil.getString(context, R.attr.custom_date);
        thisWeek = ThemeUtil.getString(context, R.attr.this_week);
        lastWeek = ThemeUtil.getString(context, R.attr.last_week);

      /* SharedPreferences lastLoginInfo = POSUtil.getLoginInfo(context);
        if (lastLoginInfo.getString(AppConstant.KEY_USER_ROLE, "NULL").equalsIgnoreCase(User.ROLE.Sale.toString())) {
            userId = "= " + lastLoginInfo.getLong(AppConstant.KEY_USER_ID, -1 );
        } else {
            userId = " IS NOT NULL";
        }*/
        AccessLogManager accessLogManager     = new AccessLogManager(context);
        AuthorizationManager authorizationManager = new AuthorizationManager(context);
        Long                 deviceId             = authorizationManager.getDeviceId(Build.SERIAL);
        User currentUser          = accessLogManager.getCurrentlyLoggedInUser(deviceId);
        if (currentUser == null) {
            POSUtil.noUserIsLoggedIn(context);
            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            return null;
        }
        if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Sale.toString())) {
            userId = " = " + currentUser.getId();
        } else {
            userId = " IS NOT NULL";
        }

        filterList = new ArrayList<>();
        filterList.add(allTrans);
        filterList.add(thisWeek);
        filterList.add(lastWeek);
        filterList.add(thisMonth);
        filterList.add(lastMonth);
        filterList.add(thisYear);
        filterList.add(lastYear);
        filterList.add(customRange);
        filterList.add(customDate);

        configCustomDatePickerDialog();

        startDate = "000000000000";
        endDate = "9999999999999999";
        salesOrderManager = new SalesOrderManager(context);
        salesManager = new SalesManager(context);
        saleHistorySearchAdapter = new SaleHistorySearchAdapter(context, R.id.voucher_no, salesManager);

        loadSaleHistory(0, 10);

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildingCustomRangeDialog();
        loadUI();
        buildDateFilterDialog();
        configRecycler();
        buildOrderCancelAlertDialog();

        searchView.setAdapter(saleHistorySearchAdapter);
        searchView.showSuggestions();
        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // shouldLoad = false;
                //isSearch = true;
                //                salesHistoryList = new ArrayList<>();

                Bundle bundle = new Bundle();
                bundle.putLong(SaleDetailFragment.KEY, saleHistorySearchAdapter.getSuggestion().get(position).getId());
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.SALE_DETAIL);
                detailIntent.putExtras(bundle);
                startActivity(detailIntent);

                //salesHistoryList.add(saleHistorySearchAdapter.getSuggestionList().get(position));
                //refreshRecyclerView();

                searchView.closeSearch();

                // filterTextView.setText("-");

                //                searchedResultTxt.setVisibility(View.VISIBLE);
            }
        });

        //        filterTextView.addTextChangedListener(new TextWatcher() {
        //            @Override
        //            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //
        //            }
        //
        //            @Override
        //            public void onTextChanged(CharSequence s, int start, int before, int count) {
        //                if (!filterTextView.getText().toString().trim().equals("-") && !filterTextView.getText().toString().trim().isEmpty() && searchedResultTxt.isShown()) {
        //                    searchedResultTxt.setVisibility(View.INVISIBLE);
        //                }
        //            }
        //
        //            @Override
        //            public void afterTextChanged(Editable s) {
        //
        //            }
        //        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchText = query;

                if (query.contains("#")) {
                    query = query.replaceFirst("#", "");
                }

                shouldLoad = true;
                isSearch = true;

                loadSaleHistory(0, 10, query);
                refreshRecyclerView();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });

        setFilterTextView(filterList.get(1));
        loadThisMonthTransaction(1);

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                tracePos(position);

                shouldLoad = true;
                isSearch = false;


                if (filterList.get(position).equalsIgnoreCase(allTrans)) {
                    setFilterTextView(filterList.get(position));

                    startDate = "000000000000";
                    endDate = "9999999999999999";

                    loadSaleHistory(0, 10);
                    refreshRecyclerView();
                } else if (filterList.get(position).equalsIgnoreCase(thisMonth)) {
                    loadThisMonthTransaction(position);
                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {
                    setFilterTextView(filterList.get(position));
                    startDate = DateUtility.getLastMonthStartDate();
                    endDate = DateUtility.getLastMonthEndDate();

                    loadSaleHistory(0, 10);
                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisYearStartDate();
                    endDate = DateUtility.getThisYearEndDate();

                    loadSaleHistory(0, 10);
                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastYearStartDate();
                    endDate = DateUtility.getLastYearEndDate();

                    loadSaleHistory(0, 10);
                    refreshRecyclerView();
                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {
                    customRangeDialog.show();
                    // setFilterTextView(filterList.get(position));
                } else if (filterList.get(position).equalsIgnoreCase(customDate)) {
                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "customeDate");
                } else if (filterList.get(position).equalsIgnoreCase(thisWeek)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getStartDateOfWeekString();
                    endDate = DateUtility.getEndDateOfWeekString();

                    loadSaleHistory(0, 10);
                    refreshRecyclerView();
                } else if (filterList.get(position).equalsIgnoreCase(lastWeek)) {
                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getStartDateOfLastWeekString();
                    endDate = DateUtility.getEndDateOfLastWeekString();

                    loadSaleHistory(0, 10);
                    refreshRecyclerView();
                }

                listenRV();
                filterDialog.dismiss();
            }
        });

        rvSwipeAdapterForSaleTransactions.setmCancelClickLister(new ClickListener() {
            @Override
            public void onClick(int postion) {

                if (!salesHistoryList.get(postion).getType().equalsIgnoreCase(SalesManager.SaleType.Sales.toString())) {
                    setRefundDialogData(salesHistoryList.get(postion));
                    refundDialog.show();
                    refundPos = postion;
                }
            }
        });

        addNewSaleFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  MainActivity.replacingFragment(new AddEditNewSaleFragment());
                isAdd = true;
                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);
                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_SALE);

                startActivity(addCurrencyIntent);
            }
        });

        rvSwipeAdapterForSaleTransactions.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                salesHistory = salesHistoryList.get(postion);
                Bundle bundle = new Bundle();
                bundle.putSerializable("saleheader", salesHistoryList.get(postion));

                //  AddEditNewSaleFragment saleEditFragments = new AddEditNewSaleFragment();
                // saleEditFragments.setArguments(bundle);
                //  MainActivity.replacingFragment(saleEditFragments);
                // bundle.putSerializable(AddEditNewSaleFragment.KEY, salesHistoryList.get(postion));

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);
                addCurrencyIntent.putExtras(bundle);
                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_SALE);
                startActivity(addCurrencyIntent);
            }
        });

        rvSwipeAdapterForSaleTransactions.setViewDetailsClickLister(new ClickListener() {
            @Override
            public void onClick(int postion) {
                Bundle bundle = new Bundle();
                bundle.putLong(SaleDetailFragment.KEY, salesHistoryList.get(postion).getId());
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.SALE_DETAIL);
                detailIntent.putExtras(bundle);
                startActivity(detailIntent);
            }
        });

        rvSwipeAdapterForSaleTransactions.setViewPaymentsClickLister(new ClickListener() {
            @Override
            public void onClick(int postion) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("saleheader", salesHistoryList.get(postion));
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.RECEIVABLE_PAYMENT_DETAIL);
                detailIntent.putExtras(bundle);
                startActivity(detailIntent);
            }
        });

        rvSwipeAdapterForSaleTransactions.setDeleteClickLister(new ClickListener() {
            @Override
            public void onClick(int postion) {
                deletepos = postion;

                saleDeleteAlertDialog.show();
            }
        });

        yesSaleDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                salesManager.deleteHoldSales(salesHistoryList.get(deletepos).getId());


                Log.w("hello delete", salesHistoryList.get(deletepos).getVoucherNo() + "DDD");


                //rvSwipeAdapterForSaleTransactions.setSalesHistoryList(salesHistoryList);


                if (salesHistoryList.size() == 1) {
                    salesHistoryList.remove(deletepos);
                    rvSwipeAdapterForSaleTransactions.setSalesHistoryList(salesHistoryList);
                    rvSwipeAdapterForSaleTransactions.notifyDataSetChanged();
                } else {
                    salesHistoryList.remove(deletepos);
                    rvSwipeAdapterForSaleTransactions.notifyItemRemoved(deletepos);

                    rvSwipeAdapterForSaleTransactions.notifyItemRangeChanged(deletepos, salesHistoryList.size());

                }

                saleDeleteAlertDialog.dismiss();

            }
        });

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDate = DateUtility.makeDateFromSlash(customStartDate);
                endDate = DateUtility.makeDateFromSlash(customEndDate);

                //String startDayDes[]= DateUtility.dayDes(startDate);
                //String startYearMonthDes= DateUtility.monthYearDes(startDate);
                //String endDayDes[]= DateUtility.dayDes(endDate);
                //  String endYearMonthDes= DateUtility.monthYearDes(endDate);
                //    filterTextView.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));

                filterTextView.setText(customStartDate + " - " + customEndDate);

                loadSaleHistory(0, 10);
                refreshRecyclerView();

                customRangeDialog.dismiss();
            }
        });

        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRangeDialog.dismiss();
            }
        });

        listenRV();
        buildRefundDialog();

        refundDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                isCancel = false;
            }
        });
        confirmMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isCancel) {

                    if (checkRefundValid()) {

                        salesHistoryList.remove(refundPos);

                        rvSwipeAdapterForSaleTransactions.setSalesHistoryList(salesHistoryList);

                        rvSwipeAdapterForSaleTransactions.notifyItemRemoved(refundPos);
                        //}

                        refundDialog.dismiss();

                    }

                    //boolean status = salesOrderManager.cancelOrder(salesHistoryList.get(refundPos).getId(), salesHistoryList.get(refundPos).getFeedbackType() + "Cancel");

                    //if (status) {


                }

            }
        });

        cancelMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refundDialog.dismiss();
            }
        });

        rvAdapterForFilter.setCurrentPos(3);


        saleHistoryListRecyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {

                Log.w("V X", velocityX + " S");

                Log.w("V Y", velocityY + " S");

                if (velocityY > 100) {
                    addNewSaleFab.hide();
                } else if (velocityY < -100) {
                    addNewSaleFab.show();
                }


                return false;
            }
        });


        return mainLayout;

    }

    private void listenRV() {
        saleHistoryListRecyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() { }

            @Override
            public void onScrollDown() { }

            @Override
            public void onLoadMore() {
                if (shouldLoad) {
                    rvSwipeAdapterForSaleTransactions.setShowLoader(true);
                    //                rvSwipeAdapterForSaleTransactions.notifyDataSetChanged();
                    loadmore();
                }
            }
        });
    }

    private void refreshRecyclerView() {

        if (salesHistoryList.isEmpty() && salesHistoryList.size() == 0) {

            noTransactionTextView.setVisibility(View.VISIBLE);

            saleHistoryListRecyclerView.setVisibility(View.GONE);

        } else {

            saleHistoryListRecyclerView.setVisibility(View.VISIBLE);

            noTransactionTextView.setVisibility(View.GONE);

            rvSwipeAdapterForSaleTransactions.setSalesHistoryList(salesHistoryList);

            rvSwipeAdapterForSaleTransactions.notifyDataSetChanged();
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        listenRV();

        POSUtil.checkExpire(getContext());
        Log.w("is search", isSearch + "sss");
        //        if(salesHistory!=null){
        //
        //            salesHistory =salesManager.getHoldSaleHistoryViewById(salesHistory.getId());
        //
        //            // Log.w("After updateRegistration",salesHistory.toString());
        //
        //            rvSwipeAdapterForSaleTransactions.updateItem(salesHistory);
        //
        //        }
        //        if(isAdd){
        //            isAdd=false;
        //            rvAdapterForFilter.setCurrentPos(current);
        //        }


        rvAdapterForFilter.setCurrentPos(0);

        // rvAdapterForFilter.setCurrentPos(current);


    }

    private void loadThisMonthTransaction(int position) {
        setFilterTextView(filterList.get(position));

        startDate = DateUtility.getThisMonthStartDate();

        endDate = DateUtility.getThisMonthEndDate();

        loadSaleHistory(0, 10);

        refreshRecyclerView();
    }

    private void buildOrderCancelAlertDialog() {

        //TypedArray no = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        //TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        saleDeleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        String   sureToDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete}).getString(0);
        TextView textView     = (TextView) saleDeleteAlertDialog.findViewById(R.id.title);
        textView.setText(sureToDelete);


        yesSaleDeleteMdButton = (Button) saleDeleteAlertDialog.findViewById(R.id.save);

        saleDeleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saleDeleteAlertDialog.dismiss();
            }
        });

    }

    private boolean checkRefundValid() {
        boolean status = true;

        refundAmt = 0.0;

        if (refundEditText.getText().toString().trim().length() < 1) {

            status = false;

        } else {

            refundAmt = Double.parseDouble(refundEditText.getText().toString().trim());

        }

        if (salesHistoryList.get(refundPos).getPaidAmt() < refundAmt) {

            status = false;

            String greater = context.getTheme().obtainStyledAttributes(new int[]{R.attr.refund_amount_cannot_be_greater_than_paid_amount}).getString(0);
            refundEditText.setError(greater);

        }

        return status;

    }

    private void tracePos(int pos) {

        oldPos = current;
        current = pos;

    }

    private void setRefundDialogData(SalesHistory salesHistory) {

        clearRefundDialogErrorMsg();

        totalAmountInRefundTextView.setText(POSUtil.NumberFormat(salesHistory.getTotalAmount()));

        paidAmoutInRefundTextView.setText(POSUtil.NumberFormat(salesHistory.getPaidAmt()));

        refundEditText.setText("0");

    }

    private void clearRefundDialogErrorMsg() {
        refundEditText.setError(null);
    }

    public void buildDatePickerDialog() {

        refundDatePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        refundDay = dayOfMonth;

                        refundMonth = monthOfYear + 1;

                        refundYear = year;

                        configDateUI();

                    }
                },

                now.get(Calendar.YEAR),

                now.get(Calendar.MONTH),

                now.get(Calendar.DAY_OF_MONTH)
        );

        if (darkmode)
            refundDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        refundDatePickerDialog.setThemeDark(darkmode);


    }

    private void configDateUI() {

        date = DateUtility.makeDateFormatWithSlash(Integer.toString(refundYear), Integer.toString(refundMonth), Integer.toString(refundDay));

        String dayDes[] = DateUtility.dayDes(date);

        String yearMonthDes = DateUtility.monthYearDes(date);

        refundDateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

        refundDateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

    }

    public void buildRefundDialog() {


        refundDialog = new MaterialDialog.Builder(context).

                customView(R.layout.refund_dialog, true).title("Refund").

                positiveText("Confirm").

                negativeText("Cancel").typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf").


                build();

        totalAmountInRefundTextView = (TextView) refundDialog.findViewById(R.id.total_amt);

        paidAmoutInRefundTextView = (TextView) refundDialog.findViewById(R.id.paid_amt);

        refundEditText = (EditText) refundDialog.findViewById(R.id.refund_amt);

        refundDateLinearLayout = (LinearLayout) refundDialog.findViewById(R.id.date_ll);

        refundDateTextView = (TextView) refundDialog.findViewById(R.id.date);

        confirmMdButton = (Button) refundDialog.findViewById(R.id.confirm);

        cancelMdButton = (Button) refundDialog.findViewById(R.id.cancel);

    }


    private void configCustomDatePickerDialog() {
        customeDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        String tempdate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        startDate = tempdate;

                        endDate = tempdate;

                        filterTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));

                        loadSaleHistory(0, 10);

                        refreshRecyclerView();
                    }
                },
                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)
        );

        if (darkmode)
            customeDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        customeDatePickerDialog.setThemeDark(darkmode);

    }


    private void loadSaleHistory(int startLimit, int endLimit) {

         salesHistoryList = salesManager.getAllSalesHold(startLimit, endLimit, startDate, endDate, new InsertedBooleanHolder(), userId);

    }

    private List<SalesHistory> load(int startLimit, int endLimit) {

        List<SalesHistory> loadList;
        loadList = salesManager.getAllSalesHold(startLimit, endLimit, startDate, endDate, new InsertedBooleanHolder() , userId);

        return loadList;
    }

    private void loadSaleHistory(int startLimit, int endLimit, String query) {

        salesHistoryList = salesManager.getHoldSalesWithVoucherNoOrCustomer(startLimit, endLimit, query , userId);

    }


    private List<SalesHistory> load(int startLimit, int endLimit, String query) {

        List<SalesHistory> salesHistories;

        salesHistories = salesManager.getHoldSalesWithVoucherNoOrCustomer(startLimit, endLimit, query , userId);

        return salesHistories;
    }


    public void loadmore() {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isSearch) {
                    salesHistoryList.addAll(load(salesHistoryList.size(), 9, searchText));
                } else {
                    salesHistoryList.addAll(load(salesHistoryList.size(), 9));
                }

                salesHistoryList.addAll(load(salesHistoryList.size(), 9));

                rvSwipeAdapterForSaleTransactions.setShowLoader(false);

                refreshRecyclerView();
            }
        }, 500);
    }

    private void setFilterTextView(String msg) {

        filterTextView.setText(msg);

    }

    private void buildDateFilterDialog() {

        TypedArray filterByDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date});

        filterDialog = new MaterialDialog.Builder(context).

                title(filterByDate.getString(0))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")

                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))

                .build();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    //    @Override
    //    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    //
    //        getActivity().getMenuInflater().inflate(R.menu.main, menu);
    //
    //        MenuItem item = menu.findItem(R.id.action_search);
    //
    //        searchView.setMenuItem(item);
    //
    //        super.onCreateOptionsMenu(menu, inflater);
    //    }


    public void configRecycler() {

        rvSwipeAdapterForSaleTransactions = new RVSwipeAdapterForSaleTransactions(salesHistoryList);

        saleHistoryListRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        saleHistoryListRecyclerView.setAdapter(rvSwipeAdapterForSaleTransactions);

    }

    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a                   = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});
        int        attributeResourceId = a.getResourceId(0, 0);
        searchView.setCursorDrawable(attributeResourceId);

    }

    public void loadUI() {

        linearLayout = (LinearLayout) mainLayout.findViewById(R.id.ll);

        noTransactionTextView = (TextView) mainLayout.findViewById(R.id.no_transaction);

        addNewSaleFab = (FloatingActionButton) mainLayout.findViewById(R.id.add_new_sale);

        saleHistoryListRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.sale_history_list_rv);

        filterTextView = (TextView) mainLayout.findViewById(R.id.filter_one);

        searchedResultTxt = (TextView) mainLayout.findViewById(R.id.searched_result_txt);

        loadUIFromToolbar();


    }

    public void buildingCustomRangeDialog() {

        TypedArray dateRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});
        //TypedArray ok = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ok});

        buildingCustomRangeDatePickerDialog();
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(dateRange.getString(0))
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.negativeText(cancel.getString(0))
                //.positiveText(ok.getString(0))
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(customStartDate);
                                                                         startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(customEndDate);
                                                                         endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });

        if (darkmode)
            startDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        startDatePickerDialog.setThemeDark(darkmode);


    }

    @Override
    public void onDetach() {

        if (searchView != null) {

            searchView.closeSearch();
            searchView.hideKeyboard(searchView);
        }
        super.onDetach();
    }
}
