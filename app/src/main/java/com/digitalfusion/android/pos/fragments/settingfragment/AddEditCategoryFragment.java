package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.CategoryManager;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.views.FusionToast;

/**
 * Created by MD003 on 3/7/17.
 */

public class AddEditCategoryFragment extends Fragment {


    public static final String KEY = "category";
    private EditText categoryNameTextInputEditText;
    private EditText categoryDescTextInputEditText;
    private View mainLayoutView;
    private Category category;
    private CategoryManager categoryManager;
    private boolean isEdit = false;

    private String categoryName;
    private String categoryDesc;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.add_category, container, false);

        categoryManager = new CategoryManager(getContext());

        setHasOptionsMenu(true);

        loadUI();

        context = getContext();

        if (getArguments().getSerializable(KEY) == null) {
            String newCategory = context.getTheme().obtainStyledAttributes(new int[]{R.attr.new_category}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(newCategory);
            isEdit = false;
        } else {
            String editCategory = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_category}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(editCategory);
            isEdit = true;
            category = (Category) getArguments().get(KEY);
            initializeOldData();
        }

        return mainLayoutView;

    }

    @Override
    public void onResume() {

        super.onResume();

    }

    private void takeValueFromView() {

        categoryName = categoryNameTextInputEditText.getText().toString().trim();

        categoryDesc = categoryDescTextInputEditText.getText().toString().trim();


    }

    private void initializeOldData() {

        categoryName = category.getName();
        categoryDesc = category.getDescription();

        categoryNameTextInputEditText.setText(category.getName());
        categoryDescTextInputEditText.setText(category.getDescription());

    }

    private void clearData() {
        categoryNameTextInputEditText.setText(null);
        categoryNameTextInputEditText.setError(null);
        categoryDescTextInputEditText.setText(null);
        categoryDescTextInputEditText.setError(null);

    }

    private boolean checkValidation() {

        boolean status = true;

        if (categoryNameTextInputEditText.getText().toString().trim().length() < 1) {

            String categoryName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_category_name}).getString(0);
            categoryNameTextInputEditText.setError(categoryName);

            status = false;
        } else {

            categoryName = categoryNameTextInputEditText.getText().toString().trim();
            categoryDesc = categoryDescTextInputEditText.getText().toString().trim();
            if (!isEdit) {

                if (categoryManager.checkNameExist(categoryName)) {
                    status = false;
                    String categoryAlreadyExist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.category_already_exist}).getString(0);
                    categoryNameTextInputEditText.setError(categoryAlreadyExist);
                }
            } else {
                if (categoryManager.checkNameExist(categoryName) && !category.getName().trim().equals(categoryName)) {
                    status = false;
                    String categoryAlreadyExist = context.getTheme().obtainStyledAttributes(new int[]{R.attr.category_already_exist}).getString(0);
                    categoryNameTextInputEditText.setError(categoryAlreadyExist);
                }
            }
        }

        return status;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save) {

            if (!isEdit) {
                saveCategory();
            } else {

                if (checkValidation()) {
                    takeValueFromView();
                    categoryManager.updateCategory(categoryName, categoryDesc, category.getId());
                    getActivity().onBackPressed();
                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                }
            }

        }
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void saveCategory() {
        if (checkValidation()) {

            takeValueFromView();

            categoryManager.addNewCategory(categoryName, categoryDesc, null, 0);
            clearData();
            getActivity().onBackPressed();
            FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);
       /* TypedArray save=context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        FontTextView fontTextView= (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.menu_text,null);

        menu.getItem(0).setActionView(fontTextView);

        fontTextView.setText(save.getString(0));


        fontTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menu.getItem(0));
            }
        });*/


        super.onCreateOptionsMenu(menu, inflater);

    }

    private void loadUI() {
        categoryNameTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.category_name_in_category_add_dg);
        categoryDescTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.category_desc_in_category_add_dg);

    }

}
