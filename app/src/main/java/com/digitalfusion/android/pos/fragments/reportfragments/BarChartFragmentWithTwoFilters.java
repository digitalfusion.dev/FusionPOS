package com.digitalfusion.android.pos.fragments.reportfragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForCategoryMD;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForExpenseManagerFilter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.ParentRVAdapterForReports;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForBarChartReportWithNameAmt;
import com.digitalfusion.android.pos.database.business.CategoryManager;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.model.Category;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.ExpenseManagerUtil;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.DefaultYAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class BarChartFragmentWithTwoFilters extends Fragment implements Serializable {
    public static int[] colorArr = new int[]{ColorTemplate.rgb("#03A9F4"),
            ColorTemplate.rgb("#CDDC39"),
            ColorTemplate.rgb("#FF9800"),
            ColorTemplate.rgb("#e040fb"),
            ColorTemplate.rgb("#80DEEA"),
            ColorTemplate.rgb("#3F51B5"),
            ColorTemplate.rgb("#FFEB3B"),
            ColorTemplate.rgb("#00BFA5"),
            ColorTemplate.rgb("#757575"),
            ColorTemplate.rgb("#F44336")};
    private String type;
    private TextView typeFilterTextView;
    private TextView dateFilterTextView;
    private RVAdapterForFilter rvAdapterForDateFilter;
    private MaterialDialog dateFilterDialog;
    private List<String> filterList;
    private View mainLayoutView;
    private Context context;
    private List<ReportItem> reportItemList;
    private ReportManager reportManager;
    private ParentRVAdapterForReports rvAdapterForReport;
    private RecyclerView recyclerView;
    private BarChart barChart;
    private String reportName;
    private String startDate;
    private String endDate;
    private int startLimit;
    private int endLimit;
    private List<BarEntry> yValues;
    private ArrayList<String> xValues;
    private List<Double> yValList;
    private String allTrans;
    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;
    private String customRange;
    private MaterialDialog customRangeDialog;
    private TextView startDateTextView, endDateTextView;
    private String customStartDate,
            customEndDate;
    private TextView traceDate;
    private Button customRangeOkBtn, customRangeCancelBtn;
    private DatePickerDialog startDatePickerDialog;
    private MaterialDialog typeFilterDialog;
    private List<ExpenseManagerUtil.FilterName> filterTypeList;

    //private String categoryName;
    private List<Category> categoryList;

    //private MaterialDialog categoryChooserMD;
    private ParentRVAdapterForReports rvAdapterForCategoryMD;
    private String categoryID;
    private CategoryManager categoryManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_bar_chart_fragment_with_two_filters, container, false);
        context = getContext();
        loadIngUI();
        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            reportName = getArguments().getString("reportType");
        }
        configToolBarTitle();
        initializeVariables();
        new LoadProgressDialog().execute("");
        configFilters();
        buildDateFilterDialog();
        setDateFilterTextView(thisMonth);

        typeFilterTextView.setText(type);

        buildingCustomRangeDialog();
        buildCategoryChooserDialog();
        clickListeners();

    }

    private void configToolBarTitle() {
        String title = null;
        switch (reportName) {
            case "top amounts expense manager":
                title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.top_income_expense}).getString(0);
                TextView textView = mainLayoutView.findViewById(R.id.type_filter);
                textView.setText(context.getTheme().obtainStyledAttributes(new int[]{R.attr.type}).getString(0));
                break;
            case "bottom sales by products":
                title = ThemeUtil.getString(context, R.attr.bottom_sales_by_product);
                break;
            case "top sale by products":
                title = ThemeUtil.getString(context, R.attr.top_sales_by_product);
                break;
        }
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void configFilters() {
        thisMonth = ThemeUtil.getString(context, R.attr.this_month);
        lastMonth = ThemeUtil.getString(context, R.attr.last_month);
        thisYear = ThemeUtil.getString(context, R.attr.this_year);
        lastYear = ThemeUtil.getString(context, R.attr.last_year);
        customRange = ThemeUtil.getString(context, R.attr.custom_range);

        filterList = new ArrayList<>();
        filterList.add(thisMonth);
        filterList.add(lastMonth);
        filterList.add(thisYear);
        filterList.add(lastYear);
        filterList.add(customRange);

        rvAdapterForDateFilter = new RVAdapterForFilter(filterList);

    }

    private void loadIngUI() {
        typeFilterTextView = (TextView) mainLayoutView.findViewById(R.id.filter_one);
        dateFilterTextView = (TextView) mainLayoutView.findViewById(R.id.date_filter);
        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.pie_chart_recycler_view);
        barChart = (BarChart) mainLayoutView.findViewById(R.id.bar_chart);
    }

    private void initializeVariables() {
        reportItemList = new ArrayList<>();
        reportManager = new ReportManager(context);
        setDatesAndLimits();

        switch (reportName) {
            case "top amounts expense manager":
                filterTypeList = ExpenseManagerUtil.getFilterNameList(getContext());
                filterTypeList.remove(0);
                type = filterTypeList.get(0).getType().toString();
                break;
            case "bottom sales by products":
            case "top sale by products":
                categoryManager = new CategoryManager(context);

                categoryList = categoryManager.getAllCategories();
                String all = ThemeUtil.getString(context, R.attr.all);
                categoryList.add(0, new Category(all));
                type = categoryList.get(0).getName();
                //typeFilterTextView.setText(categoryName);
                categoryID = " IS NOT NULL";
                break;
        }

    }

    public void buildCategoryChooserDialog() {

        switch (reportName) {
            case "top amounts expense manager":
                rvAdapterForCategoryMD = new RVAdapterForExpenseManagerFilter(filterTypeList);
                break;
            case "bottom sales by products":
            case "top sale by products":
                rvAdapterForCategoryMD = new RVAdapterForCategoryMD(categoryList);
                break;
        }

        String filter = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_type}).getString(0);

        typeFilterDialog = new MaterialDialog.Builder(context)
                .title(filter)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForCategoryMD, new LinearLayoutManager(context))
                .build();
    }


    private void initializeList() {
        switch (reportName) {
            case "top amounts expense manager":
                Log.e("expense type", type);
                reportItemList = reportManager.getTotalAmtByIncomeExpense(startDate, endDate, startLimit, endLimit, type);
                break;
            case "bottom sales by products":
                reportItemList = reportManager.bottomSalesByProducts(startDate, endDate, startLimit, endLimit, categoryID);
                break;
            case "top sale by products":
                reportItemList = reportManager.topSalesByProducts(startDate, endDate, startLimit, endLimit, categoryID);
                break;
        }
        Log.e("size", reportItemList.size() + " ida");
    }

    public void buildingCustomRangeDialog() {
        buildingCustomRangeDatePickerDialog();
        String date = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range}).getString(0);
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(date)
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.negativeText("Cancel")
                //.positiveText("OK")
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    private void setDateFilterTextView(String msg) {

        dateFilterTextView.setText(msg);
    }

    private void buildDateFilterDialog() {
        String date = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);

        dateFilterDialog = new MaterialDialog.Builder(context).

                title(date)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForDateFilter, new LinearLayoutManager(context))

                .build();

    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });


    }

    public void configRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        rvAdapterForReport = new RVAdapterForBarChartReportWithNameAmt(reportItemList);
        recyclerView.setAdapter(rvAdapterForReport);
        /*else if (reportName.equalsIgnoreCase("top sales by customer") || reportName.equalsIgnoreCase("top sale by products") ||
                reportName.equalsIgnoreCase("top outstanding suppliers") || reportName.equalsIgnoreCase("bottom sales by products")){
            rvAdapterForReport = new RVAdapterForBarChartReportWithNameAmt(reportItemList);
            recyclerView.setAdapter(rvAdapterForReport);
        }*/
    }

    private void setDatesAndLimits() {
        startDate = DateUtility.getThisMonthStartDate();
        //Log.e("start", startDate );
        endDate = DateUtility.getThisMonthEndDate();
        //Log.e("start", endDate );
        /*Calendar c = Calendar.getInstance();
        c.set(2016,0,1,0,0,0);
        startTime = c.getTimeInMillis();
        c.set(2017,0,1,0,0,0);
        endTime = c.getTimeInMillis();*/
        startLimit = 0;
        endLimit = 10;
    }

    public void setBarChartConfiguration() {
        //mChart.setDescription("01-01-2015 ~ 31-01-2015");


        // barChart.setDescriptionPosition(Float.parseFloat(XAxis.XAxisPosition.TOP.toString()),Float.parseFloat(YAxis.AxisDependency.RIGHT.toString()));
        barChart.setDrawBorders(false);
        barChart.setPinchZoom(true);
        barChart.setPivotY(10);
        barChart.setDrawBarShadow(false);
        barChart.setMaxVisibleValueCount(30);
        barChart.setDescription("");
        barChart.setDrawGridBackground(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setTouchEnabled(false);
        barChart.animateXY(1400, 1400, Easing.EasingOption.EaseInBack, Easing.EasingOption.EaseInBack);
        barChart.getLegend().setEnabled(false);
        if (POSUtil.isLabelWhite(context)) {
            barChart.getAxisLeft().setTextColor(Color.WHITE);
            barChart.getXAxis().setTextColor(Color.WHITE);
        }
        Legend l = barChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setYOffset(2f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f);
        l.setXOffset(3f);

        configXAxis();

        configYAxis();

        setBarDataToChart();

        barChart.invalidate();
    }


    private void setBarDataToChart() {

        xValues = new ArrayList<>();
        yValList = new ArrayList<>();

        for (ReportItem reportItem : reportItemList) {
            //Log.e("ccccc",cal+"");
            xValues.add(reportItem.getName());
            yValList.add(reportItem.getBalance());
        }


        yValues = getYAxisBarEntries(yValList);

        BarData data = configBarData(yValues, xValues);

        barChart.setData(data);
        for (IBarDataSet set : barChart.getData().getDataSets())
            set.setDrawValues(!set.isDrawValuesEnabled());
    }

    /**
     * @param yValue is data want to add to bar entry.
     * @return bart entry.
     */
    @NonNull
    private List<BarEntry> getYAxisBarEntries(List<Double> yValue) {
        List<BarEntry> yVals1 = new ArrayList<BarEntry>();
        int            j      = 0;
        for (Double t : yValue) {
            Float f = Float.valueOf(t.toString());
            yVals1.add(new BarEntry(f, j));
            ++j;
        }
        return yVals1;
    }

    @NonNull
    private BarData configBarData(List<BarEntry> yVals, List<String> xVals) {
        BarDataSet set1 = new BarDataSet(yVals, "");
        set1.setColors(colorArr);
        List<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(xVals, dataSets);

        // add space between the dataset groups in percent of bar-width
        data.setGroupSpace(80f);

        //mChart.setDescription(calendar.get(Calendar.YEAR) + " " + xValues.get(0) + " - " + xValues.get(11));

        return data;
    }

    private void configYAxis() {
        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setValueFormatter(new DefaultYAxisValueFormatter(1));
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(10f);
        leftAxis.setAxisMinValue(0);
        leftAxis.setLabelCount(5, true);
        barChart.getAxisRight().setEnabled(false);
    }

    private void configXAxis() {
        XAxis xl = barChart.getXAxis();

        xl.setLabelRotationAngle(270);
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setDrawGridLines(false);
        xl.setSpaceBetweenLabels(1);
        xl.setDrawLabels(false);
    }

    private void clickListeners() {
        switch (reportName) {
            case "top amounts expense manager":
                ((RVAdapterForExpenseManagerFilter) rvAdapterForCategoryMD).setmItemClickListener(new RVAdapterForExpenseManagerFilter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        typeFilterDialog.dismiss();
                        type = filterTypeList.get(position).getType().toString();
                        typeFilterTextView.setText(type);
                        new LoadProgressDialog().execute("");
                    }
                });
                break;
            case "bottom sales by products":
            case "top sale by products":
                ((RVAdapterForCategoryMD) rvAdapterForCategoryMD).setmItemClickListener(new RVAdapterForCategoryMD.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        typeFilterDialog.dismiss();
                        type = categoryList.get(position).getName();
                        typeFilterTextView.setText(type);
                        if (position == 0) {
                            categoryID = " IS NOT NULL";
                        } else {
                            categoryID = " = " + categoryList.get(position).getId();
                        }
                        new LoadProgressDialog().execute("");
                    }
                });
                break;
        }


        typeFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                typeFilterDialog.show();
                //                switch (reportName){
                //                    case "top amounts expense manager" :
                //                        break;
                //                    case "bottom sales by products" :
                //                    case "top sale by products" :
                //                        categoryChooserMD.show();
                //                        break;
                //                }

            }
        });

        dateFilterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateFilterDialog.show();
            }
        });

        rvAdapterForDateFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                dateFilterDialog.dismiss();
                if (filterList.get(position).equalsIgnoreCase(thisMonth)) {

                    startDate = DateUtility.getThisMonthStartDate();

                    endDate = DateUtility.getThisMonthEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {


                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {


                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    new LoadProgressDialog().execute("");

                    setDateFilterTextView(filterList.get(position));
                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {

                    customRangeDialog.show();

                }


            }
        });

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startDate = customStartDate;

                endDate = customEndDate;

                /*String startDayDes[]= DateUtility.dayDes(startDate);

                String startYearMonthDes= DateUtility.monthYearDes(startDate);


                String endDayDes[]= DateUtility.dayDes(endDate);

                String endYearMonthDes= DateUtility.monthYearDes(endDate);*/


                //dateFilterTextView.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));

                dateFilterTextView.setText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));

                new LoadProgressDialog().execute("");

                customRangeDialog.dismiss();
            }
        });

        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRangeDialog.dismiss();
            }
        });
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String    label         = "";
            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            if (hud != null) {
                hud.show();
            }
        }

        @Override
        protected void onPostExecute(String a) {

            setBarChartConfiguration();
            configRecyclerView();
            if (hud.isShowing()) {
                hud.dismiss();
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
