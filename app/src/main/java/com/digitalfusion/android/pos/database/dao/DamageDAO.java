package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.model.AccessLog;
import com.digitalfusion.android.pos.database.model.DamagedItem;
import com.digitalfusion.android.pos.database.model.StockValue;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/18/16.
 */
public class DamageDAO extends ParentDAO {
    private static ParentDAO damageDAOInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private DamagedItem damagedItem;
    private List<DamagedItem> damagedItemList;
    private DamageDetailDAO damageDetailDAO;
    private List<Long> idList;

    private DamageDAO(Context context) {
        super(context);
        damagedItem = new DamagedItem();
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
        damageDetailDAO = DamageDetailDAO.getDamageDetailDaoInstance(context);
    }

    public static DamageDAO getDamageDAOInstance(Context context) {
        if (damageDAOInstance == null) {
            damageDAOInstance = new DamageDAO(context);
        }
        return (DamageDAO) damageDAOInstance;
    }

    public boolean addNewDamage(int stockQty, String date, Long stockID, String remark, String day, String month, String year, Long currentUserId) {
        Long genId = idGeneratorDAO.getLastIdValue("Damage");

        genId += 1;
        query = "insert into " + AppConstant.DAMAGE_TABLE_NAME + "(" + AppConstant.DAMAGE_ID + ", " + AppConstant.DAMAGE_STOCK_ID + ", " + AppConstant.DAMAGE_DATE + ", " + AppConstant.DAMAGE_STOCK_QTY
                //+ ", " + AppConstant.DAMAGE_TOTAL_VALUE
                + ", " + AppConstant.DAMAGE_REMARK + ", " + AppConstant.DAMAGE_USER_ID + ", " + AppConstant.DAMAGE_DAY
                + ", " + AppConstant.DAMAGE_MONTH + ", " + AppConstant.DAMAGE_YEAR + ", " + AppConstant.CREATED_DATE + ", " + AppConstant.DAMAGE_TIME + ")" +
                " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, strftime('%s', ?, time('now', 'localtime')));";
        databaseWriteTransaction(flag);
        try {
            statement = database.compileStatement(query);
            statement.bindString(1, genId.toString());
            Log.e("i", stockID + "oo");
            statement.bindLong(2, stockID);
            statement.bindString(3, date);
            statement.bindString(4, Integer.toString(stockQty));
            //statement.bindString(5, totalValue.toString());
            statement.bindString(5, remark);
            statement.bindString(6, currentUserId.toString());
            //statement.bindNull(7);
            statement.bindString(7, day);
            statement.bindString(8, month);
            statement.bindString(9, year);
            statement.bindString(10, DateUtility.getTodayDate());
            statement.bindString(11, formatDateWithDash(day, month, year));
            statement.execute();
            statement.clearBindings();
            /*for (DamageItemInDetail d: detailViewList){
               // Log.e("add","add");
                damageDetailDAO.addNewStockDamageDetail(genId, d.getStockID(), d.getQty(), d.getPurchasePrice(), d.getTotal(), d.getRemark());
            }*/
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }

        /*for (int i=0; i<15; i++){
            DamageItemInDetail detailView = new DamageItemInDetail();
            detailView.setStockID(2l);
            detailView.setQty(2);
            detailView.setPurchasePrice(230.0);
            detailView.setTotal(230.0);
            detailViewList.add(detailView);
        }

        DamageItemInDetail detailView = new DamageItemInDetail();
        detailView.setStockID(2l);
        detailView.setQty(2);
        detailView.setPurchasePrice(230.0);
        detailView.setTotal(230.0);
        //detailView.setUnitID(3l);
        detailViewList.add(detailView);*/

        //Log.e("coml","ha"+flag.isInserted());
        return flag.isInserted();
    }

    public List<DamagedItem> getDamagesByDateRange(String startDate, String endDate, int startLimit, int endLimit) {
        Log.e(startDate, endDate);
        Cursor cursor1 = null;
        createPurchasePriceTempTable(startDate, endDate);
        damagedItemList = new ArrayList<>();
        //        String query = "select d." + AppConstant.DAMAGE_ID + ", s." + AppConstant.STOCK_NAME + ", d." + AppConstant.DAMAGE_DATE + ", d." + AppConstant.DAMAGE_STOCK_QTY + ", d." +
        //                //AppConstant.DAMAGE_TOTAL_VALUE + ", d." +
        //                AppConstant.DAMAGE_REMARK //+ ", u." + AppConstant.USER_USER_NAME + ", d." + AppConstant.DAMAGE_USER_ID + ", u." + AppConstant.USER_ROLE
        //                + ", d." + AppConstant.DAMAGE_DAY + ", d." + AppConstant.DAMAGE_MONTH + ", d." + AppConstant.DAMAGE_YEAR + ", sum(ifnull(" + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
        //                ", " + AppConstant.DAMAGE_STOCK_QTY + ") * " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ") totalPrice"
        //                + ", ifnull(u." + AppConstant.UNIT_UNIT+ ", '') unit, s."+ AppConstant.STOCK_CODE_NUM+" , t."+ AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + ", c." + AppConstant.CATEGORY_NAME
        //                + ", d." + AppConstant.DAMAGE_TIME + " from " + AppConstant.CATEGORY_TABLE_NAME + " c, " + AppConstant.DAMAGE_TABLE_NAME + " d," + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.STOCK_TABLE_NAME + " s left outer join "
        //                + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u. " + AppConstant.UNIT_ID //+ AppConstant.USER_TABLE_NAME + " u, " + AppConstant.STOCK_TABLE_NAME + " s
        //                + " where " + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " = s." + AppConstant.STOCK_ID +" and "+ //d." + AppConstant.DAMAGE_USER_ID + " = u." + AppConstant.USER_ID + " and d." +
        //                AppConstant.DAMAGE_DATE + " >= ? and d." + AppConstant.DAMAGE_DATE + " <= ? and d." + AppConstant.DAMAGE_STOCK_ID + " = t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " and " +
        //                "(" + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'd' or " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'a') and d." + AppConstant.DAMAGE_ID + " = " + AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID
        //                + " group by d." + AppConstant.DAMAGE_ID + " order by d." + AppConstant.DAMAGE_TIME + " desc limit " + startLimit + ", "+ endLimit;
        String query = "select d.id, s.name, d.date, d.stockQty, d.remark, d.day, d.month, d.year, sum(ifnull(purchaseTempQty, stockQty) * purchaseTempPrice) totalPrice, ifnull(u.unit, '')" +
                " unit, s.codeNo , t.stockID, c.name, d.time from Category c, Damage d,SalesPurchaseTemp t, \n" +
                "Stock s left outer join Unit u on s.unitID = u. id where categoryID = c.id and t.stockID = s.id and date >= ? and date <= ? and " +
                "d.stockID = t.stockID " +
                "and salesTempType = 'd' and d.id = detailID group by d.id order by d.time desc limit " + startLimit + ", " + endLimit;
        Log.e("query", query);
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            Log.e("cursor", cursor.getCount() + "");
            if (cursor.moveToFirst()) {
                do {
                    damagedItem = new DamagedItem();
                    damagedItem.setId(cursor.getLong(0));
                    damagedItem.setStockName(cursor.getString(1));
                    damagedItem.setDate(cursor.getString(2));

                    //Log.e("daeaoief", cursor.getString(2));
                    damagedItem.setStockQty(cursor.getInt(3));
                    //damagedItem.setTotalValue(cursor.getDouble(4));
                    damagedItem.setRemark(cursor.getString(4));
                    //damagedItem.setUserName(cursor.getString(5));
                    //  damagedItem.setUserID(cursor.getLong(6));
                    //  damagedItem.setRole(cursor.getString(7));
                    damagedItem.setDay(cursor.getString(5));
                    damagedItem.setMonth(cursor.getString(6));
                    damagedItem.setYear(cursor.getString(7));
                    damagedItem.setTotalValue(cursor.getDouble(8));
                    damagedItem.setStockUnit(cursor.getString(9));
                    damagedItem.setStockCode(cursor.getString(10));
                    damagedItem.setStockId(cursor.getLong(11));
                    damagedItem.setCategoryName(cursor.getString(12));
                    Log.e("time", cursor.getString(13));
                    query = " select ifnull(" + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + ", " + AppConstant.DAMAGE_STOCK_QTY + ") qty, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE +
                            " from " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.DAMAGE_TABLE_NAME + " d where t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " = d." + AppConstant.DAMAGE_STOCK_ID
                            + " and " +
                            AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'd' and " + AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID + " = d." + AppConstant.DAMAGE_ID + " and d." + AppConstant.DAMAGE_ID + " = " + cursor.getLong(0);
                    Log.e("query", query);
                    cursor1 = database.rawQuery(query, null);
                    if (cursor1.moveToFirst()) {
                        do {
                            Log.e("value" + cursor1.getDouble(1), cursor.getLong(0) + "qty " + cursor1.getInt(0));
                            damagedItem.getStockValueList().add(new StockValue(cursor1.getDouble(1), cursor1.getInt(0)));
                        } while (cursor1.moveToNext());
                    }
                    damagedItemList.add(damagedItem);

                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
            if (cursor1 != null)
                cursor1.close();
        }
        dropPurchasePriceTempTable();
        Log.e("d am size", damagedItemList.size() + "");
        return damagedItemList;
    }

    public List<DamagedItem> getDamagesForSearch(String startDate, String endDate, int startLimit, int endLimit, String searchStr) {
        damagedItemList = new ArrayList<>();
        query = "select d." + AppConstant.DAMAGE_ID + ", " + AppConstant.STOCK_CODE_NUM + ", " + AppConstant.STOCK_NAME + " from " + AppConstant.DAMAGE_TABLE_NAME + " d, " + AppConstant.STOCK_TABLE_NAME +
                " s where " + AppConstant.DAMAGE_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " + AppConstant.DAMAGE_DATE + " = ? and " + AppConstant.DAMAGE_DATE + " = ? and " +
                "(s." + AppConstant.STOCK_NAME + " like ? || '%' or s." + AppConstant.STOCK_CODE_NUM + " like ? || '%' ) " + " order by d." + AppConstant.DAMAGE_TIME + " desc limit " +
                startLimit + ", " + endLimit + ";";
        databaseWriteTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, searchStr, searchStr});
            if (cursor.moveToFirst()) {
                damagedItem = new DamagedItem();
                damagedItem.setId(cursor.getLong(0));
                damagedItem.setStockCode(cursor.getString(1));
                damagedItem.setStockName(cursor.getString(2));
                damagedItemList.add(damagedItem);
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return damagedItemList;
    }

    public List<DamagedItem> getDamagesByDateRangeOnSearch(String startDate, String endDate, int startLimit, int endLimit, Long id) {
        Log.e(startDate, endDate + " on search " + id);
        createPurchasePriceTempTable(startDate, endDate);
        Cursor cursor1 = null;
        damagedItemList = new ArrayList<>();
        //        query = "select d." + AppConstant.DAMAGE_ID + ", s." + AppConstant.STOCK_NAME + ", d." + AppConstant.DAMAGE_DATE + ", d." + AppConstant.DAMAGE_STOCK_QTY + ", d." +
        //                //AppConstant.DAMAGE_TOTAL_VALUE + ", d." +
        //                AppConstant.DAMAGE_REMARK //+ ", u." + AppConstant.USER_USER_NAME + ", d." + AppConstant.DAMAGE_USER_ID + ", u." + AppConstant.USER_ROLE
        //                + ", d." + AppConstant.DAMAGE_DAY + ", d." + AppConstant.DAMAGE_MONTH + ", d." + AppConstant.DAMAGE_YEAR + ", sum(ifnull(" + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY +
        //                ", " + AppConstant.DAMAGE_STOCK_QTY + ") * " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE + ") totalPrice"
        //                + ", ifnull(u." + AppConstant.UNIT_UNIT+", '') unit, s."+ AppConstant.STOCK_CODE_NUM+" , t."+ AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + ", c." + AppConstant.CATEGORY_NAME
        //                + " from " + AppConstant.DAMAGE_TABLE_NAME + " d," + AppConstant.CATEGORY_TABLE_NAME + " c," + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.STOCK_TABLE_NAME + " s left outer join "
        //                + AppConstant.UNIT_TABLE_NAME + " u on s." + AppConstant.STOCK_UNIT_ID + " = u. " + AppConstant.UNIT_ID //+ AppConstant.USER_TABLE_NAME + " u, " + AppConstant.STOCK_TABLE_NAME + " s
        //                + " where s." + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " and t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " = s." + AppConstant.STOCK_ID +" and "+ //d." + AppConstant.DAMAGE_USER_ID + " = u." + AppConstant.USER_ID + " and d." +
        //                AppConstant.DAMAGE_DATE + " >= ? and d." + AppConstant.DAMAGE_DATE + " <= ? and d." + AppConstant.DAMAGE_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " +
        //                "(" + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'd' or " + AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'a') and d." + AppConstant.DAMAGE_ID + " = " + AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID
        //                + " and s." + AppConstant.STOCK_ID + " = " + id + " group by d." + AppConstant.DAMAGE_ID + " order by d." + AppConstant.DAMAGE_TIME + " desc limit " + startLimit + ", "+ endLimit;
        query = "select d.id, s.name, d.date, d.stockQty, d.remark, d.day, d.month, d.year, sum(ifnull(purchaseTempQty, stockQty) * purchaseTempPrice) totalPrice, ifnull(u.unit, '') unit," +
                " s.codeNo , t.stockID, c.name, d.time from Category c, Damage d,SalesPurchaseTemp t, \n" +
                "Stock s left outer join Unit u on s.unitID = u. id where categoryID = c.id and t.stockID = s.id and date >= ? and date <= ? and d.stockID = t.stockID " +
                "and salesTempType = 'd' and d.id = detailID " + " and s." + AppConstant.STOCK_ID + " = " + id + " group by d.id order by d.time desc limit " + startLimit
                + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            Cursor cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    Log.e("cursor", cursor.getCount() + "id");
                    damagedItem = new DamagedItem();
                    damagedItem.setId(cursor.getLong(0));
                    damagedItem.setStockName(cursor.getString(1));
                    damagedItem.setDate(cursor.getString(2));
                    //Log.e("date da", cursor.getString(2));
                    damagedItem.setStockQty(cursor.getInt(3));
                    //damagedItem.setTotalValue(cursor.getDouble(4));
                    damagedItem.setRemark(cursor.getString(4));
                    //damagedItem.setUserName(cursor.getString(5));
                    //  damagedItem.setUserID(cursor.getLong(6));
                    //  damagedItem.setRole(cursor.getString(7));
                    damagedItem.setDay(cursor.getString(5));
                    damagedItem.setMonth(cursor.getString(6));
                    damagedItem.setYear(cursor.getString(7));
                    //damagedItem.setValue(cursor.getDouble(8));
                    damagedItem.setTotalValue(cursor.getDouble(8));
                    damagedItem.setStockCode(cursor.getString(10));
                    damagedItem.setStockUnit(cursor.getString(9));
                    damagedItem.setStockId(cursor.getLong(11));
                    damagedItem.setCategoryName(cursor.getString(12));
                    query = " select ifnull(" + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_QTY + ", " + AppConstant.DAMAGE_STOCK_QTY + ") qty, " + AppConstant.SALES_PURCHASE_PURCHASE_TEMP_PRICE +
                            " from " + AppConstant.SALES_PURCHASE_TEMP_TABLE_NAME + " t, " + AppConstant.DAMAGE_TABLE_NAME + " d where t." + AppConstant.SALES_PURCHASE_TEMP_STOCK_ID + " = d." + AppConstant.DAMAGE_STOCK_ID
                            + " and " +
                            AppConstant.SALES_PURCHASE_SALES_TEMP_TYPE + " = 'd' and " + AppConstant.SALES_PURCHASE_TEMP_DETAIL_ID + " = d." + AppConstant.DAMAGE_ID + " and d." + AppConstant.DAMAGE_ID + " = " + cursor.getLong(0);
                    //Log.e("id", cursor.getLong(0)+ "f00"+cursor.getDouble(8));
                    cursor1 = database.rawQuery(query, null);
                    if (cursor1.moveToFirst()) {
                        do {
                            //Log.e(cursor1.getInt(0) + " q", cursor1.getDouble(1)+"");
                            damagedItem.getStockValueList().add(new StockValue(cursor1.getDouble(1), cursor1.getInt(0)));
                        } while (cursor1.moveToNext());
                    }
                    damagedItemList.add(damagedItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
            if (cursor1 != null)
                cursor1.close();
        }
        dropPurchasePriceTempTable();

        Log.e("dam lis size", damagedItemList.size() + "fioa");
        return damagedItemList;
    }

    public List<DamagedItem> getDamages(int startLimit, int endLimit) {
        damagedItemList = new ArrayList<>();
        query = "select d." + AppConstant.DAMAGE_ID + ", s." + AppConstant.STOCK_NAME + ", d." + AppConstant.DAMAGE_DATE + ", d." + AppConstant.DAMAGE_STOCK_QTY + ", d." +
                //AppConstant.DAMAGE_TOTAL_VALUE + ", d." +
                AppConstant.DAMAGE_REMARK + ", u." + AppConstant.USER_USER_NAME + ", d." + AppConstant.DAMAGE_USER_ID + ", u." + AppConstant.USER_ROLE
                + ", d." + AppConstant.DAMAGE_DAY + ", d." + AppConstant.DAMAGE_MONTH + ", d." + AppConstant.DAMAGE_YEAR + " ,s." + AppConstant.STOCK_CODE_NUM + " ,s." + AppConstant.STOCK_ID
                + ", c." + AppConstant.CATEGORY_NAME + " from " + AppConstant.CATEGORY_TABLE_NAME + " c"
                + AppConstant.DAMAGE_TABLE_NAME + " d," + AppConstant.STOCK_TABLE_NAME + " s, " + AppConstant.USER_TABLE_NAME + " u where d." + AppConstant.DAMAGE_USER_ID + " = u." + AppConstant.USER_ID +
                " and " + AppConstant.DAMAGE_STOCK_ID + " = s." + AppConstant.STOCK_ID + " and " + AppConstant.STOCK_CATEGORY_ID + " = c." + AppConstant.CATEGORY_ID + " order by d." + AppConstant.DAMAGE_TIME + " desc limit " + startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    damagedItem = new DamagedItem();
                    damagedItem.setId(cursor.getLong(0));
                    damagedItem.setStockName(cursor.getString(1));
                    damagedItem.setDate(cursor.getString(2));
                    damagedItem.setStockQty(cursor.getInt(3));
                    //damagedItem.setTotalValue(cursor.getDouble(4));
                    damagedItem.setRemark(cursor.getString(4));
                    damagedItem.setUserName(cursor.getString(5));
                    damagedItem.setUserID(cursor.getLong(6));
                    damagedItem.setRole(cursor.getString(7));
                    damagedItem.setDay(cursor.getString(8));
                    damagedItem.setMonth(cursor.getString(9));
                    damagedItem.setYear(cursor.getString(10));
                    damagedItem.setStockCode(cursor.getString(11));
                    damagedItem.setStockId(cursor.getLong(12));
                    damagedItem.setCategoryName(cursor.getString(13));
                    damagedItemList.add(damagedItem);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            //Log.e("inohe",e.getLocalizedMessage());
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return damagedItemList;
    }

    public boolean updateDamage(Long id, String date, int stockQty, String remark, Long userID,
                                String day, String month, String year, Long stockID) {
        query = "update " + AppConstant.DAMAGE_TABLE_NAME + " set " + AppConstant.DAMAGE_DATE + "=?, " + AppConstant.DAMAGE_STOCK_QTY
                + "= " + stockQty + ", " //+ AppConstant.DAMAGE_TOTAL_VALUE + "=" + totalValue +  ", "
                + AppConstant.DAMAGE_REMARK + "=?, " + AppConstant.DAMAGE_USER_ID + "=" + userID + ", "
                + AppConstant.DAMAGE_DAY + "=?, " + AppConstant.DAMAGE_MONTH + " =?, " + AppConstant.DAMAGE_STOCK_ID + " = " + stockID + ", " + AppConstant.DAMAGE_YEAR + "=? where " + AppConstant.DAMAGE_ID + "=" + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{date, remark, day, month, year});
            /*for (DamageItemInDetail detailView : updateDetailList){
                if (detailView.getDamageID() == null){
                    damageDetailDAO.addNewStockDamageDetail(id, detailView. getStockID(), detailView.getQty(), detailView.getPurchasePrice(), detailView.getTotal(), detailView.getRemark());
                }else {
                    damageDetailDAO.updateStockDamageDetail(detailView.getId(), detailView.getStockID(), detailView.getQty(), detailView.getPurchasePrice(), detailView.getTotal(), detailView.getRemark());
                }
            }
            for (Long detailID : deleteDetailIDList){
                damageDetailDAO.deleteStockDamageDetail(detailID);
            }*/
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean deleteDamage(Long id) {
        databaseWriteTransaction(flag);
        try {
            /*idList = damageDetailDAO.getDamageDetailIdList(id);
            for (Long detailID : idList){
                damageDetailDAO.deleteStockDamageDetail(detailID);
            }*/
            database.delete(AppConstant.DAMAGE_TABLE_NAME, AppConstant.DAMAGE_ID + " = ?", new String[]{id.toString()});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }


}
