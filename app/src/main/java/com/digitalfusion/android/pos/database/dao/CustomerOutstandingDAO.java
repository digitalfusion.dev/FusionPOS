package com.digitalfusion.android.pos.database.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.digitalfusion.android.pos.database.model.OutstandingPayment;
import com.digitalfusion.android.pos.database.model.OutstandingPaymentDetail;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.DateUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hninhsuhantharkyaw on 10/23/16.
 */

public class CustomerOutstandingDAO extends ParentDAO {
    private static ParentDAO customerOutstandingDaoInstance;
    private IdGeneratorDAO idGeneratorDAO;
    private List<SalesHistory> customerOutstandingList;
    private SalesHistory customerOutstanding;
    private OutstandingPaymentDetail customerOutstandingPaymentDetail;
    private OutstandingPayment customerOutstandingPayment;

    private CustomerOutstandingDAO(Context context) {
        super(context);
        idGeneratorDAO = IdGeneratorDAO.getIdGeneratorDaoInstance(context);
    }

    public static CustomerOutstandingDAO getCustomerOutstandingDaoInstance(Context context) {
        if (customerOutstandingDaoInstance == null) {
            customerOutstandingDaoInstance = new CustomerOutstandingDAO(context);
        }
        return (CustomerOutstandingDAO) customerOutstandingDaoInstance;
    }

    public List<SalesHistory> getCustomerOutstandingList(int startLimit, int endLimit, String startDate, String endDate) {
        customerOutstandingList = new ArrayList<>();
        query = "select s." + AppConstant.SALES_ID + ", " + AppConstant.SALES_VOUCHER_NUM + ", " + AppConstant.SALES_DAY + ", " + AppConstant.SALES_MONTH + ", " +
                AppConstant.SALES_YEAR + ", " + AppConstant.SALES_DATE + ", s." + AppConstant.SALES_BALANCE + " - ifnull((select sum(" + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID +
                " ), 0.0) as bal," + AppConstant.CUSTOMER_NAME + ", " + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_PAID_AMOUNT + ", s." + AppConstant.SALES_TYPE +
                " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME + " c where c." +
                AppConstant.CUSTOMER_ID + " = " + AppConstant.SALES_CUSTOMER_ID + " and bal > 0 and " + AppConstant.SALES_DATE + ">= ? and " + AppConstant.SALES_DATE + " <= ? and s. "
                + AppConstant.SALES_TYPE + " not like '%' || 'Cancel' order by  " + AppConstant.CUSTOMER_OUTSTANDING_TIME + " desc limit " +
                startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    customerOutstanding = new SalesHistory();
                    customerOutstanding.setId(cursor.getLong(0));
                    customerOutstanding.setVoucherNo(cursor.getString(1));
                    customerOutstanding.setDay(cursor.getString(2));
                    customerOutstanding.setMonth(cursor.getString(3));
                    customerOutstanding.setYear(cursor.getString(4));
                    customerOutstanding.setDate(cursor.getString(5));
                    customerOutstanding.setBalance(cursor.getDouble(6));
                    customerOutstanding.setCustomer(cursor.getString(7));
                    customerOutstanding.setCustomerID(cursor.getLong(8));
                    customerOutstanding.setPaidAmt(cursor.getDouble(9));
                    customerOutstanding.setType(cursor.getString(10));
                    customerOutstandingList.add(customerOutstanding);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return customerOutstandingList;
    }


    public List<SalesHistory> getCustomerOutstandingListOnSearch(int startLimit, int endLimit, String startDate, String endDate, String searchStr) {
        customerOutstandingList = new ArrayList<>();
        query = "select s." + AppConstant.SALES_ID + ", " + AppConstant.SALES_VOUCHER_NUM + ", " + AppConstant.SALES_DAY + ", " + AppConstant.SALES_MONTH + ", " +
                AppConstant.SALES_YEAR + ", " + AppConstant.SALES_DATE + ", s." + AppConstant.SALES_BALANCE + " - ifnull((select sum(" + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID +
                " ), 0.0) as bal," + AppConstant.CUSTOMER_NAME + ", " + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_PAID_AMOUNT + ", s." + AppConstant.SALES_TYPE +
                " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME + " c where c." +
                AppConstant.CUSTOMER_ID + " = " + AppConstant.SALES_CUSTOMER_ID + " and bal > 0 and " + AppConstant.SALES_DATE + ">= ? and " + AppConstant.SALES_DATE + " <= ? and ("
                + AppConstant.SALES_VOUCHER_NUM + " like ? || '%' or " + AppConstant.CUSTOMER_NAME + " like ? || '%')  and s."
                + AppConstant.SALES_TYPE + " not like '%' || 'Cancel'  limit " +
                startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate, searchStr, searchStr});
            if (cursor.moveToFirst()) {
                do {
                    customerOutstanding = new SalesHistory();
                    customerOutstanding.setId(cursor.getLong(0));
                    customerOutstanding.setVoucherNo(cursor.getString(1));
                    customerOutstanding.setDay(cursor.getString(2));
                    customerOutstanding.setMonth(cursor.getString(3));
                    customerOutstanding.setYear(cursor.getString(4));
                    customerOutstanding.setDate(cursor.getString(5));
                    customerOutstanding.setBalance(cursor.getDouble(6));
                    customerOutstanding.setCustomer(cursor.getString(7));
                    customerOutstanding.setCustomerID(cursor.getLong(8));
                    customerOutstanding.setPaidAmt(cursor.getDouble(9));
                    customerOutstanding.setType(cursor.getString(10));
                    customerOutstandingList.add(customerOutstanding);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return customerOutstandingList;
    }

    public List<SalesHistory> getCustomerOutstandingListByCustomer(int startLimit, int endLimit, String startDate, String endDate, Long customerId) {
        customerOutstandingList = new ArrayList<>();
        query = "select s." + AppConstant.SALES_ID + ", " + AppConstant.SALES_VOUCHER_NUM + ", " + AppConstant.SALES_DAY + ", " + AppConstant.SALES_MONTH + ", " +
                AppConstant.SALES_YEAR + ", " + AppConstant.SALES_DATE + ", s." + AppConstant.SALES_BALANCE + " - ifnull((select sum(" + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID +
                " ), 0.0) as bal," + AppConstant.CUSTOMER_NAME + ", " + AppConstant.SALES_CUSTOMER_ID + ", s." + AppConstant.SALES_PAID_AMOUNT +
                " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME + " c where c." +
                AppConstant.CUSTOMER_ID + " = " + AppConstant.SALES_CUSTOMER_ID + " and bal > 0 and s." + AppConstant.SALES_CUSTOMER_ID + " = " + customerId + " and " + AppConstant.SALES_TYPE +
                " not like '%' || 'Cancel' and " + AppConstant.SALES_DATE + ">= ? and " + AppConstant.SALES_DATE + " <= ? limit " +
                startLimit + ", " + endLimit;
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                do {
                    customerOutstanding = new SalesHistory();
                    customerOutstanding.setId(cursor.getLong(0));
                    customerOutstanding.setVoucherNo(cursor.getString(1));
                    customerOutstanding.setDay(cursor.getString(2));
                    customerOutstanding.setMonth(cursor.getString(3));
                    customerOutstanding.setYear(cursor.getString(4));
                    customerOutstanding.setDate(cursor.getString(5));
                    customerOutstanding.setBalance(cursor.getDouble(6));
                    customerOutstanding.setCustomer(cursor.getString(7));
                    customerOutstanding.setCustomerID(cursor.getLong(8));
                    customerOutstanding.setPaidAmt(cursor.getDouble(9));
                    customerOutstandingList.add(customerOutstanding);
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return customerOutstandingList;
    }

    public boolean addCustomerOutstandingPayment(String invoiceNo, Long customerID, Long salesID, Double paidAmt, String day, String month, String year, String date) {
        genID = idGeneratorDAO.getLastIdValue("CustomerOutstanding");
        genID++;
        query = "insert into " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + "(" + AppConstant.CUSTOMER_OUTSTANDING_ID + ", " + AppConstant.CUSTOMER_OUTSTANDING_INVOICE_NUM + ", " +
                AppConstant.CUSTOMER_OUTSTANDING_CUSTOMER_ID + ", " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + ", " + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                ", " + AppConstant.CREATED_DATE + ", " + AppConstant.CUSTOMER_OUTSTANDING_DAY + ", " + AppConstant.CUSTOMER_OUTSTANDING_MONTH + ", " + AppConstant.CUSTOMER_OUTSTANDING_YEAR + ", " +
                AppConstant.CUSTOMER_OUTSTANDING_DATE + ", " + AppConstant.CUSTOMER_OUTSTANDING_TIME + ") values (" + genID + ", ?, " + customerID + ", " + salesID + ", " + paidAmt +
                ", ?,?,?,?,?, strftime('%s', ?, time('now', 'localtime')))";
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{invoiceNo, DateUtility.getTodayDate(), day, month, year, date, formatDateWithDash(day, month, year)});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();

    }

    public boolean updateCustomerOutstandingPayment(Long customerID, Long salesID, Double paidAmt, Long id, String day, String month, String year) {
        query = "update " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " set " + AppConstant.CUSTOMER_OUTSTANDING_CUSTOMER_ID + " = " + customerID + ", " +
                AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = " + salesID + ", " + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                " = " + paidAmt + " , " + AppConstant.CUSTOMER_OUTSTANDING_DAY + "=? , " + AppConstant.CUSTOMER_OUTSTANDING_MONTH + "=? , " + AppConstant.CUSTOMER_OUTSTANDING_YEAR + "=?, " +
                AppConstant.CUSTOMER_OUTSTANDING_TIME + " = strftime('%s', ?, time('now', 'localtime'))"
                + " where " + AppConstant.CUSTOMER_OUTSTANDING_ID + " = " + id;
        databaseWriteTransaction(flag);
        try {
            database.execSQL(query, new String[]{day, month, year, formatDateWithDash(day, month, year)});
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public boolean deleteCustomerOutstandingPayment(Long id) {
        databaseWriteTransaction(flag);
        try {
            database.delete(AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME, AppConstant.CUSTOMER_OUTSTANDING_ID + "=?", new String[]{id.toString()});
            database.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            ex.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return flag.isInserted();
    }

    public Double getReceivablePaidAmtBySalesID(Long salesID) {
        Double amt = 0.0;
        String query = "select s." + AppConstant.SALES_PAID_AMOUNT + " , ifnull(sum(c." + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT + "), 0), " + AppConstant.SALES_BALANCE + " from " + AppConstant.SALES_TABLE_NAME + " s left outer join " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME +
                " c  " + " on " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID + "  where s." + AppConstant.SALES_ID + " = ?";
        Log.e("query", query);
        databaseWriteTransaction(flag);
        Cursor cursor = null;
        try {
            cursor = database.rawQuery(query, new String[]{salesID.toString()});
            if (cursor.moveToFirst()) {
                amt = cursor.getDouble(0) + cursor.getDouble(1);
                Log.e("raw" + cursor.getDouble(1), "query" + cursor.getDouble(2));
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            ex.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }

        Log.e("hell paid amt", amt + " Hello");

        return amt;
    }

    public OutstandingPaymentDetail getCustomerOutstandingPaymentsBySaleID(Long saleID) {
        customerOutstandingPaymentDetail = new OutstandingPaymentDetail();
        query = "select c." + AppConstant.CUSTOMER_OUTSTANDING_ID + ", " + AppConstant.CUSTOMER_OUTSTANDING_INVOICE_NUM + ", c." + AppConstant.CUSTOMER_OUTSTANDING_CUSTOMER_ID + ", s." +
                AppConstant.SALES_ID + ", " + "c." + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT + ", " + AppConstant.SALES_VOUCHER_NUM + ", " + "s." + AppConstant.SALES_BALANCE
                + " - ifnull((select sum(" + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                ") from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = " + saleID + " ), 0.0) as bal," +
                AppConstant.CUSTOMER_NAME + ", c." + AppConstant.CUSTOMER_OUTSTANDING_DAY + ", c." + AppConstant.CUSTOMER_OUTSTANDING_MONTH + ", c." + AppConstant.CUSTOMER_OUTSTANDING_YEAR + ", c." +
                AppConstant.CUSTOMER_OUTSTANDING_DATE + ", " + AppConstant.SALES_TOTAL_AMOUNT + ", s." + AppConstant.SALES_DAY + ", s." + AppConstant.SALES_MONTH + ", s." + AppConstant.SALES_YEAR +
                ", s." + AppConstant.SALES_DATE + " from " + AppConstant.SALES_TABLE_NAME + " s left outer join " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " c on s." +
                AppConstant.SALES_ID + " = " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + ", " + AppConstant.CUSTOMER_TABLE_NAME +
                " cm where cm." + AppConstant.CUSTOMER_ID + " = s." + AppConstant.SALES_CUSTOMER_ID + " and s." + AppConstant.SALES_ID + " = " + saleID + " order by c." + AppConstant.CUSTOMER_OUTSTANDING_TIME + " asc;";
        databaseReadTransaction(flag);
        try {
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                customerOutstandingPaymentDetail.setCustomerSupplierID(cursor.getLong(2));
                customerOutstandingPaymentDetail.setSalePurchaseID(cursor.getLong(3));
                customerOutstandingPaymentDetail.setSalePurchaseVoucherNo(cursor.getString(5));
                customerOutstandingPaymentDetail.setOutstandingBalance(cursor.getDouble(6));
                customerOutstandingPaymentDetail.setCustomerSupplierName(cursor.getString(7));
                customerOutstandingPaymentDetail.setSalePurchaseTotalAmount(cursor.getDouble(12));
                customerOutstandingPaymentDetail.setSalePurchaseDay(cursor.getString(13));
                customerOutstandingPaymentDetail.setSalePurchaseMonth(cursor.getString(14));
                customerOutstandingPaymentDetail.setSalePurchaseYear(cursor.getString(15));
                customerOutstandingPaymentDetail.setSalePurchaseDate(cursor.getString(16));
                //                Log.e("date" , cursor.getString(11));
                do {
                    customerOutstandingPayment = new OutstandingPayment();
                    customerOutstandingPayment.setId(cursor.getLong(0));
                    customerOutstandingPayment.setInvoiceNo(cursor.getString(1));
                    customerOutstandingPayment.setPaidAmt(cursor.getDouble(4));
                    customerOutstandingPayment.setDay(cursor.getString(8));
                    customerOutstandingPayment.setMonth(cursor.getString(9));
                    customerOutstandingPayment.setYear(cursor.getString(10));
                    customerOutstandingPayment.setDate(cursor.getString(11));

                    if (!customerOutstandingPayment.isNull()) {
                        customerOutstandingPaymentDetail.getOutstandingPaymentList().add(customerOutstandingPayment);
                    }
                } while (cursor.moveToNext());
            }
            database.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null) {
                cursor.close();
            }
        }
        return customerOutstandingPaymentDetail;
    }

    public Double getPaidAmtBySalesID(Long salesID) {
        Double amt = 0.0;
        databaseWriteTransaction(flag);

        Log.w("sale ID", salesID + " Sale id");
        try {
            query = "select s." + AppConstant.SALES_PAID_AMOUNT + " + ifnull(sum(c." + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT + "), 0.0) total_paid from " + AppConstant.SALES_TABLE_NAME +
                    " s left outer join " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " c on " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID + " where " + "s." + AppConstant.SALES_ID + " = " +
                    salesID;
            cursor = database.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                amt = cursor.getDouble(0);
            }
            Log.w("Amt", query + " qry");
            Log.w("Amt", cursor.getDouble(0) + " Amt");
            database.setTransactionSuccessful();
        } catch (SQLiteException ex) {
            ex.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }

        Log.w("hell paid amt", amt + " Hello");

        return amt;
    }

    public Double getTotalReceivableAmt(String startDate, String endDate) {
        Double amt = 0.0;
        databaseWriteTransaction(flag);
        try {
            query = "select sum(bal) from (" +
                    "select s." + AppConstant.SALES_BALANCE + " - ifnull((select sum(" + AppConstant.CUSTOMER_OUTSTANDING_PAID_AMOUNT +
                    ") from " + AppConstant.CUSTOMER_OUTSTANDING_TABLE_NAME + " where " + AppConstant.CUSTOMER_OUTSTANDING_SALES_ID + " = s." + AppConstant.SALES_ID +
                    " ), 0.0) as bal" + " from " + AppConstant.SALES_TABLE_NAME + " s, " + AppConstant.CUSTOMER_TABLE_NAME + " c where c." +
                    AppConstant.CUSTOMER_ID + " = " + AppConstant.SALES_CUSTOMER_ID + " and bal > 0 and " + AppConstant.SALES_DATE + ">= ? and " + AppConstant.SALES_DATE + " <= ? and s. "
                    + AppConstant.SALES_TYPE + " not like '%' || 'Cancel'" + ")";
            cursor = database.rawQuery(query, new String[]{startDate, endDate});
            if (cursor.moveToFirst()) {
                amt = cursor.getDouble(0);
            }
        } catch (SQLiteException ex) {
            ex.printStackTrace();
        } finally {
            database.endTransaction();
            if (cursor != null)
                cursor.close();
        }
        return amt;
    }
}
