package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.SwipeLayout.DragEdge;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.IgnobleEditText;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by MD003 on 8/29/16.
 */
public class RVSwipeAdapterForSaleItemViewInPurchase extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private List<SalesAndPurchaseItem> saleItemViewInSaleList;
    private ClickListener editClickListener;
    private ClickListener deleteClickListener;
    private ClickListener editQtyClickListener;
    private ClickListener editPriceClickListener;
    private ClickListener qtyMinusClickListener;
    private ClickListener qtyPlusClickListener;


    public RVSwipeAdapterForSaleItemViewInPurchase(List<SalesAndPurchaseItem> saleItemViewInSaleList) {

        this.saleItemViewInSaleList = new ArrayList<>();

        for (SalesAndPurchaseItem salesAndPurchaseItem : saleItemViewInSaleList) {

            this.saleItemViewInSaleList.add(salesAndPurchaseItem);

        }
    }


    public RVSwipeAdapterForSaleItemViewInPurchase() {
        this.saleItemViewInSaleList = new ArrayList<>();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sale_item_view, parent, false);

        return new SaleItemViewInSale(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final SalesAndPurchaseItem currentSaleItemInSale = saleItemViewInSaleList.get(position);
        currentSaleItemInSale.setQty(currentSaleItemInSale.getQty() < 1 ? 1 : currentSaleItemInSale.getQty());
        currentSaleItemInSale.setTotalPrice(currentSaleItemInSale.getQty() * currentSaleItemInSale.getPrice());
        if (holder instanceof SaleItemViewInSale) {

            final SaleItemViewInSale saleItemViewInSale = (SaleItemViewInSale) holder;
            //            POSUtil.makeZebraStrip(saleItemViewInSale.itemView, position);

            saleItemViewInSale.noTextView.setText(Integer.toString(position + 1));
            saleItemViewInSale.itemNameTextView.setText(currentSaleItemInSale.getItemName());
            saleItemViewInSale.qtyTextView.setText(Integer.toString(currentSaleItemInSale.getQty()));
            saleItemViewInSale.totalAmountTextView.setText(POSUtil.NumberFormat(currentSaleItemInSale.getTotalPrice()));
            saleItemViewInSale.salePriceTextView.setText(POSUtil.NumberFormat(currentSaleItemInSale.getPrice() - currentSaleItemInSale.getDiscountAmount()));
            saleItemViewInSale.disLinearLayout.setVisibility(View.GONE);
            saleItemViewInSale.qtyInputEditText.setText(String.valueOf(currentSaleItemInSale.getQty()));
            saleItemViewInSale.priceInputEditText.setText(POSUtil.NumberFormat(currentSaleItemInSale.getPrice()));

            saleItemViewInSale.editBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (editClickListener != null) {
                        saleItemViewInSale.swipeLayout.close();
                        editClickListener.onClick(position);
                    }
                }
            });

            saleItemViewInSale.deleteBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteClickListener != null) {
                        saleItemViewInSale.swipeLayout.close();
                        deleteClickListener.onClick(position);

                    }
                }
            });

            saleItemViewInSale.qtyInputEditText.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //                    saleItemViewInSale.swipeLayout.close();
                    editQtyClickListener.onClick(position);
                }
            });

            saleItemViewInSale.priceInputEditText.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //                    saleItemViewInSale.swipeLayout.close();
                    editPriceClickListener.onClick(position);
                }
            });

            saleItemViewInSale.qtyPlusButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    qtyPlusClickListener.onClick(position);
                }
            });

            saleItemViewInSale.qtyMinusButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    qtyMinusClickListener.onClick(position);
                }
            });

            // Set up left swipe view and right swipe view
            saleItemViewInSale.swipeLayout.addDrag(DragEdge.Left, saleItemViewInSale.leftView);
            saleItemViewInSale.swipeLayout.addDrag(DragEdge.Right, saleItemViewInSale.rightView);
            mItemManger.bindView(saleItemViewInSale.view, position);

        }


    }


    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    @Override
    public int getItemCount() {
        return saleItemViewInSaleList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public List<SalesAndPurchaseItem> getSaleItemViewInSaleList() {
        return saleItemViewInSaleList;
    }

    public void setSaleItemViewInSaleList(List<SalesAndPurchaseItem> saleItemViewInSaleList) {
        this.saleItemViewInSaleList = saleItemViewInSaleList;
    }

    public void addItem(SalesAndPurchaseItem salesAndPurchaseItem) {
        saleItemViewInSaleList.add(salesAndPurchaseItem);
        notifyItemInserted(saleItemViewInSaleList.size() - 1);
        notifyItemRangeChanged(0, saleItemViewInSaleList.size());
    }

    public void clearData() {
        this.saleItemViewInSaleList = new ArrayList<>();
        notifyDataSetChanged();
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public ClickListener getEditQtyClickListener() {
        return editQtyClickListener;
    }

    public void setEditQtyClickListener(ClickListener editQtyClickListener) {
        this.editQtyClickListener = editQtyClickListener;
    }

    public ClickListener getEditPriceClickListener() {
        return editPriceClickListener;
    }

    public void setEditPriceClickListener(ClickListener editPriceClickListener) {
        this.editPriceClickListener = editPriceClickListener;
    }

    public void setQtyMinusClickListener(ClickListener qtyMinusClickListener) {
        this.qtyMinusClickListener = qtyMinusClickListener;
    }

    public void setQtyPlusClickListener(ClickListener qtyPlusClickListener) {
        this.qtyPlusClickListener = qtyPlusClickListener;
    }


    public class SaleItemViewInSale extends RecyclerView.ViewHolder {

        TextView noTextView;
        TextView itemNameTextView;
        TextView totalAmountTextView;
        TextView qtyTextView;
        TextView salePriceTextView;
        TextView itemDiscountTextView;
        LinearLayout disLinearLayout;
        ImageButton editBtn;
        ImageButton deleteBtn;

        IgnobleEditText priceInputEditText;
        AppCompatImageButton qtyMinusButton;
        AppCompatImageButton qtyPlusButton;
        IgnobleEditText qtyInputEditText;

        View view;
        SwipeLayout swipeLayout;
        LinearLayout leftView;
        LinearLayout rightView;

        public SaleItemViewInSale(View itemView) {
            super(itemView);

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            leftView = (LinearLayout) swipeLayout.findViewById(R.id.left_view);
            rightView = (LinearLayout) swipeLayout.findViewById(R.id.right_view);

            this.view = itemView;

            noTextView = (TextView) itemView.findViewById(R.id.no_in_sale_item_view_tv);

            editBtn = (ImageButton) itemView.findViewById(R.id.edit_sale_item);
            deleteBtn = (ImageButton) itemView.findViewById(R.id.delete_sale_item);
            disLinearLayout = (LinearLayout) itemView.findViewById(R.id.dis_layout);

            itemNameTextView = (TextView) itemView.findViewById(R.id.item_name_in_sale_item_view_tv);
            totalAmountTextView = (TextView) itemView.findViewById(R.id.total_amount_in_sale_item_view_tv);
            qtyTextView = (TextView) itemView.findViewById(R.id.qty_in_sale_item_view_tv);
            salePriceTextView = (TextView) itemView.findViewById(R.id.item_sale_price_in_sale_item_view_tv);
            itemDiscountTextView = (TextView) itemView.findViewById(R.id.item_discount_in_sale_item_view_tv);

            priceInputEditText = (IgnobleEditText) itemView.findViewById(R.id.sale_price_in_sale_purchase_TIET);
            qtyMinusButton = (AppCompatImageButton) itemView.findViewById(R.id.minus_qty_in_sale_purchase_ImgBtn);
            qtyPlusButton = (AppCompatImageButton) itemView.findViewById(R.id.plus_qty_in_sale_purchase_ImgBtn);
            qtyInputEditText = (IgnobleEditText) itemView.findViewById(R.id.qty_in_sale_purchase_TIET);
        }

    }
}
