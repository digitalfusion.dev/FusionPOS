package com.digitalfusion.android.pos.fragments.dashboardfragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.List;

import static java.lang.Math.abs;

/**
 * Created by MD002 on 5/25/17.
 */

public class DashboardFragmentV1 extends Fragment {

    private View mainLayoutView;
    private Context dashboardContext;

    private TextView salesHeaderTextView;
    private TextView purchaseHeaderTextView;
    private TextView incomeHeaderTextView;
    private TextView expenseHeaderTextView;

    private RelativeLayout profitOrLossLayout;
    private RelativeLayout salesLayout;
    private RelativeLayout purchaseLayout;
    private RelativeLayout incomeLayout;
    private RelativeLayout expenseLayout;

    private TextView secondSalesDescTextView;
    private TextView secondPurchaseDescTextView;
    private TextView secondIncomeDescTextView;
    private TextView secondExpenseDescTextView;
    private TextView secondProfitLossDescTextView;

    private TextView firstSalesDescTextView;
    private TextView firstPurchaseDescTextView;
    private TextView firstIncomeDescTextView;
    private TextView firstExpenseDescTextView;
    private TextView firstProfitLossDescTextView;

    private TextView percentSalesTextView;
    private TextView percentPurchaseTextView;
    private TextView percentIncomeTextView;
    private TextView percentExpenseTextView;
    private TextView percentProfitLossTextView;

    private TextView firstSalesAmtTextView;
    private TextView firstPurchaseAmtTextView;
    private TextView firstIncomeAmtTextView;
    private TextView firstExpenseAmtTextView;
    private TextView firstProfitLossAmtTextView;

    private TextView secondSalesAmtTextView;
    private TextView secondPurchaseAmtTextView;
    private TextView secondIncomeAmtTextView;
    private TextView secondExpenseAmtTextView;
    private TextView secondProfitLossAmtTextView;

    //private TextView firstTitleTextView;
    // private TextView firstTitleDateTextView;

    // private TextView secondTitleTextView;
    // private TextView secondTitleDateTextView;

    private RVAdapterForFilter rvAdapterForDateFilter;
    private MaterialDialog dateFilterDialog;
    private List<String> filterList;


    private ReportManager reportManager;
    private ReportItem firstReportItem;

    private ReportItem secondReportItem;

    private Double firstIncAmt;
    private Double firstExpAmt;
    private Double secIncAmt;
    private Double secExpAmt;

    private Double totalIncome;
    private Double totalExpense;
    private Double firstProfitLoss, secondProfitLoss;

    private String firstStartDate;
    private String firstEndDate;
    private String secStartDate;
    private String secEndDate;

    private String titleType;

    public DashboardFragmentV1() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayoutView = inflater.inflate(R.layout.fragment_dashboard_v1, container, false);
        //String title= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.dashboard}).getString(0);
        //((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(title);

        dashboardContext = getContext();
        Log.e("dashboardContext", dashboardContext + " diaofajidjfa");

        titleType = "day";

        if (getArguments() != null) {
            Log.e("ar", "not null");
            titleType = getArguments().getString("title");
        }


        loadIngUI();
        return mainLayoutView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        initializeVariables();
        //Log.e(firstStartDate, firstEndDate);
        Log.e("view ", "create");
        checkTitle();
        Log.e("view", "created");
        //new LoadProgressDialog().execute("");


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.e("dashboardContext", dashboardContext + " tada diaofajidjfa" + isVisibleToUser);
        if (isVisibleToUser) {
            Log.e("Visible", "on SetUServisible" + isVisibleToUser);
            //new LoadProgressDialog().execute("");
        }
    }

    private void initializeList() {
        reportManager.createTempTableForProfitAndLoss(firstStartDate, firstEndDate);
        firstReportItem = reportManager.profitAndLossAmt(firstStartDate, firstEndDate);
        reportManager.dropPurchasePriceTempTable();

        reportManager.createTempTableForProfitAndLoss(secStartDate, secEndDate);
        secondReportItem = reportManager.profitAndLossAmt(secStartDate, secEndDate);
        reportManager.dropPurchasePriceTempTable();

        firstIncAmt = reportManager.getExpMgrTotal(firstStartDate, firstEndDate, "Income");
        firstExpAmt = reportManager.getExpMgrTotal(firstStartDate, firstEndDate, "Expense");

        secIncAmt = reportManager.getExpMgrTotal(secStartDate, secEndDate, "Income");
        secExpAmt = reportManager.getExpMgrTotal(secStartDate, secEndDate, "Expense");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    private void loadIngUI() {

        profitOrLossLayout = (RelativeLayout) mainLayoutView.findViewById(R.id.profit_or_loss_layout);
        firstProfitLossAmtTextView = (TextView) profitOrLossLayout.findViewById(R.id.first_amt_text_view);
        secondProfitLossDescTextView = (TextView) profitOrLossLayout.findViewById(R.id.second_desc_text_view);
        firstProfitLossDescTextView = (TextView) profitOrLossLayout.findViewById(R.id.first_desc_text_view);
        secondProfitLossAmtTextView = (TextView) profitOrLossLayout.findViewById(R.id.second_amt_text_view);
        percentProfitLossTextView = (TextView) profitOrLossLayout.findViewById(R.id.percent_text_view);

        salesLayout = (RelativeLayout) mainLayoutView.findViewById(R.id.sales_layout);
        firstSalesDescTextView = (TextView) salesLayout.findViewById(R.id.first_desc_text_view);
        firstSalesAmtTextView = (TextView) salesLayout.findViewById(R.id.first_amt_text_view);
        secondSalesDescTextView = (TextView) salesLayout.findViewById(R.id.second_desc_text_view);
        secondSalesAmtTextView = (TextView) salesLayout.findViewById(R.id.second_amt_text_view);
        percentSalesTextView = (TextView) salesLayout.findViewById(R.id.percent_text_view);


        purchaseLayout = (RelativeLayout) mainLayoutView.findViewById(R.id.purchase_layout);
        firstPurchaseDescTextView = (TextView) purchaseLayout.findViewById(R.id.first_desc_text_view);
        firstPurchaseAmtTextView = (TextView) purchaseLayout.findViewById(R.id.first_amt_text_view);
        secondPurchaseDescTextView = (TextView) purchaseLayout.findViewById(R.id.second_desc_text_view);
        secondPurchaseAmtTextView = (TextView) purchaseLayout.findViewById(R.id.second_amt_text_view);
        percentPurchaseTextView = (TextView) purchaseLayout.findViewById(R.id.percent_text_view);

        incomeLayout = (RelativeLayout) mainLayoutView.findViewById(R.id.income_layout);
        firstIncomeDescTextView = (TextView) incomeLayout.findViewById(R.id.first_desc_text_view);
        firstIncomeAmtTextView = (TextView) incomeLayout.findViewById(R.id.first_amt_text_view);
        secondIncomeDescTextView = (TextView) incomeLayout.findViewById(R.id.second_desc_text_view);
        secondIncomeAmtTextView = (TextView) incomeLayout.findViewById(R.id.second_amt_text_view);
        percentIncomeTextView = (TextView) incomeLayout.findViewById(R.id.percent_text_view);

        expenseLayout = (RelativeLayout) mainLayoutView.findViewById(R.id.expense_layout);
        firstExpenseDescTextView = (TextView) expenseLayout.findViewById(R.id.first_desc_text_view);
        firstExpenseAmtTextView = (TextView) expenseLayout.findViewById(R.id.first_amt_text_view);
        secondExpenseDescTextView = (TextView) expenseLayout.findViewById(R.id.second_desc_text_view);
        secondExpenseAmtTextView = (TextView) expenseLayout.findViewById(R.id.second_amt_text_view);
        percentExpenseTextView = (TextView) expenseLayout.findViewById(R.id.percent_text_view);

        salesHeaderTextView = (TextView) salesLayout.findViewById(R.id.header_text_view);
        purchaseHeaderTextView = (TextView) purchaseLayout.findViewById(R.id.header_text_view);
        incomeHeaderTextView = (TextView) incomeLayout.findViewById(R.id.header_text_view);
        expenseHeaderTextView = (TextView) expenseLayout.findViewById(R.id.header_text_view);

        salesHeaderTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.sale}).getString(0));
        purchaseHeaderTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.purchase}).getString(0));
        incomeHeaderTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.income}).getString(0));
        expenseHeaderTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.expense}).getString(0));

        //firstTitleTextView = (TextView) mainLayoutView.findViewById(R.id.first_title_text_view);
        //firstTitleDateTextView = (TextView) mainLayoutView.findViewById(R.id.first_title_date_text_view);

        // secondTitleTextView = (TextView) mainLayoutView.findViewById(R.id.second_title_text_view);
        //  secondTitleDateTextView = (TextView) mainLayoutView.findViewById(R.id.second_title_date_text_view);
    }

    private void initializeVariables() {

        reportManager = new ReportManager(dashboardContext);

        totalIncome = 0.0;
        totalExpense = 0.0;
        firstProfitLoss = 0.0;

        setDatesAndLimits();

    }

    private void setDatesAndLimits() {
        firstStartDate = DateUtility.getTodayDate();
        firstEndDate = DateUtility.getTodayDate();
        //Log.e(firstStartDate, firstEndDate + " date");
    }

    private void checkTitle() {

        Log.e("title in v1", titleType);
        if (titleType.equalsIgnoreCase("day")) {

            firstEndDate = DateUtility.getTodayDate();

            firstStartDate = DateUtility.getTodayDate();

            secStartDate = DateUtility.getYesterDayDate();

            secEndDate = DateUtility.getYesterDayDate();

        } else if (titleType.equalsIgnoreCase("week")) {

            firstStartDate = DateUtility.getStartDateOfWeekString();

            firstEndDate = DateUtility.getEndDateOfWeekString();

            secStartDate = DateUtility.getStartDateOfLastWeekString();

            secEndDate = DateUtility.getEndDateOfLastWeekString();

        } else if (titleType.equalsIgnoreCase("month")) {

            firstStartDate = DateUtility.getThisMonthStartDate();

            firstEndDate = DateUtility.getThisMonthEndDate();

            secStartDate = DateUtility.getLastMonthStartDate();

            secEndDate = DateUtility.getLastMonthEndDate();


        } else if (titleType.equalsIgnoreCase("year")) {

            firstStartDate = DateUtility.getThisYearStartDate();

            firstEndDate = DateUtility.getThisYearEndDate();

            secStartDate = DateUtility.getLastYearStartDate();

            secEndDate = DateUtility.getLastYearEndDate();

        }

        Log.e("before ", "hook");


    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("on", "resume");

        Log.e("on", "Visible");
        new LoadProgressDialog().execute("");

    }

    private double calculatePercent(Double first, Double second) {

        if (first == null) {
            first = 0.0;
        }
        if (second == null) {
            second = 0.0;
        }

        double diff = first - second;
        //        if(first > second && second != 0.0){
        //            Log.e("f", "true");
        //            return diff/second*100;
        //        }else if(first < second && first != 0.0){
        //            return diff/first*100;
        //        }else {
        //            return diff/(first == 0 ? (second == 0 ? 1 : second) : first) * 100;
        //        }
        if (second == 0) {
            return 0;
        }
        return diff / abs(second) * 100;
    }

    private void configUI() {

        Log.w("Herer", firstReportItem.getTotalAmt() + " SSS");

        firstSalesAmtTextView.setText(POSUtil.NumberFormat(firstReportItem.getTotalAmt()));
        firstPurchaseAmtTextView.setText(POSUtil.NumberFormat(firstReportItem.getTotalPurchase()));
        firstIncomeAmtTextView.setText(POSUtil.NumberFormat(firstIncAmt));
        firstExpenseAmtTextView.setText(POSUtil.NumberFormat(firstExpAmt));

        secondSalesAmtTextView.setText(POSUtil.NumberFormat(secondReportItem.getTotalAmt()));
        secondPurchaseAmtTextView.setText(POSUtil.NumberFormat(secondReportItem.getTotalPurchase()));
        secondIncomeAmtTextView.setText(POSUtil.NumberFormat(secIncAmt));
        secondExpenseAmtTextView.setText(POSUtil.NumberFormat(secExpAmt));

        double percent = calculatePercent(firstReportItem.getTotalAmt(), secondReportItem.getTotalAmt());
        percentSalesTextView.setText(POSUtil.PercentFromat(abs(percent)));
        if (percent < 0) {
            setDownArrow(percentSalesTextView);
            percentSalesTextView.setTextColor(getResources().getColor(R.color.expense));
        } else if (percent > 0) {
            setUpArrow(percentSalesTextView);
            percentSalesTextView.setTextColor(getResources().getColor(R.color.income));
        } else {
            setEqual(percentSalesTextView);
        }

        percent = calculatePercent(firstIncAmt, secIncAmt);
        percentIncomeTextView.setText(POSUtil.PercentFromat(abs(percent)));
        if (percent < 0) {
            setDownArrow(percentIncomeTextView);
            percentIncomeTextView.setTextColor(getResources().getColor(R.color.expense));
        } else if (percent > 0) {
            setUpArrow(percentIncomeTextView);
            percentIncomeTextView.setTextColor(getResources().getColor(R.color.income));
        } else {
            setEqual(percentIncomeTextView);
        }

        percent = calculatePercent(firstReportItem.getTotalPurchase(), secondReportItem.getTotalPurchase());
        percentPurchaseTextView.setText(POSUtil.PercentFromat(abs(percent)));
        if (percent < 0) {
            setDownArrow(percentPurchaseTextView);
            percentPurchaseTextView.setTextColor(getResources().getColor(R.color.expense));
        } else if (percent > 0) {
            setUpArrow(percentPurchaseTextView);
            percentPurchaseTextView.setTextColor(getResources().getColor(R.color.income));
        } else {
            setEqual(percentPurchaseTextView);
        }

        percent = calculatePercent(firstExpAmt, secExpAmt);
        percentExpenseTextView.setText(POSUtil.PercentFromat(abs(percent)));
        if (percent < 0) {
            setDownArrow(percentExpenseTextView);
        } else if (percent > 0) {
            setUpArrow(percentExpenseTextView);
        } else {
            setEqual(percentExpenseTextView);
        }

        if (firstReportItem.getTotalAmt() > secondReportItem.getTotalAmt()) {
            setUpArrow(firstSalesAmtTextView);
            firstSalesAmtTextView.setTextColor(getResources().getColor(R.color.income));
        } else if (firstReportItem.getTotalAmt() < secondReportItem.getTotalAmt()) {
            setDownArrow(firstSalesAmtTextView);
            firstSalesAmtTextView.setTextColor(getResources().getColor(R.color.expense));
        } else {
            //firstSalesAmtTextView.setTextColor(getResources().getColor(R.color.black));
            setEqual(percentSalesTextView);
        }

        if (firstReportItem.getTotalPurchase() > secondReportItem.getTotalPurchase()) {
            setUpArrow(firstPurchaseAmtTextView);
            firstPurchaseAmtTextView.setTextColor(getResources().getColor(R.color.income));
        } else if (firstReportItem.getTotalPurchase() < secondReportItem.getTotalPurchase()) {
            setDownArrow(firstPurchaseAmtTextView);
            firstPurchaseAmtTextView.setTextColor(getResources().getColor(R.color.expense));
        } else {
            //firstPurchaseAmtTextView.setTextColor(getResources().getColor(R.color.black));
            setEqual(percentSalesTextView);
        }

        Log.e("fri" + firstIncAmt, "sec " + secIncAmt);
        if (firstIncAmt > secIncAmt) {
            setUpArrow(firstIncomeAmtTextView);
            firstIncomeAmtTextView.setTextColor(getResources().getColor(R.color.income));
        } else if (firstIncAmt < secIncAmt) {
            setDownArrow(firstIncomeAmtTextView);
            firstIncomeAmtTextView.setTextColor(getResources().getColor(R.color.expense));
        } else {
            //firstIncomeAmtTextView.setTextColor(getResources().getColor(R.color.black));
            setEqual(percentSalesTextView);
        }

        if (firstExpAmt > secExpAmt) {
            setUpArrow(firstExpenseAmtTextView);
            firstExpenseAmtTextView.setTextColor(getResources().getColor(R.color.income));
        } else if (firstExpAmt < secExpAmt) {
            setDownArrow(firstExpenseAmtTextView);
            firstExpenseAmtTextView.setTextColor(getResources().getColor(R.color.expense));
        } else {
            //firstExpenseAmtTextView.setTextColor(getResources().getColor(R.color.black));
            setEqual(percentSalesTextView);
        }

        totalIncome = firstReportItem.getTotalAmt() - firstReportItem.getValue() + firstReportItem.getBalance();
        totalExpense = firstReportItem.getAmount() + firstReportItem.getSalesTax() + firstReportItem.getPurTax();


        totalIncome += firstIncAmt;

        totalExpense += firstExpAmt;

        firstProfitLoss = totalIncome - totalExpense;
        // if (firstProfitLoss >= 0){
        firstProfitLossAmtTextView.setText(POSUtil.NumberFormat(firstProfitLoss).toString());
        //   firstProfitLossAmtTextView.setTextColor(getResources().getColor(R.color.income));
        //} else {
        //   firstProfitLossAmtTextView.setText(POSUtil.NumberFormat(Math.abs(firstProfitLoss)) + "");
        //   firstProfitLossAmtTextView.setTextColor(getResources().getColor(R.color.expense));
        //  }

        totalIncome = secondReportItem.getTotalAmt() - secondReportItem.getValue() + secondReportItem.getBalance();
        totalExpense = secondReportItem.getAmount() + secondReportItem.getSalesTax() + secondReportItem.getPurTax();

        totalIncome += secIncAmt;

        totalExpense += secExpAmt;

        secondProfitLoss = totalIncome - totalExpense;
        Log.e("incom" + totalIncome, secondProfitLoss + "expen" + totalExpense);
        //  if (secondProfitLoss >= 0){
        secondProfitLossAmtTextView.setText(POSUtil.NumberFormat(secondProfitLoss).toString());
        //      secondProfitLossAmtTextView.setTextColor(getResources().getColor(R.color.income));
        //  } else {
        //      secondProfitLossAmtTextView.setText(POSUtil.NumberFormat(Math.abs(secondProfitLoss)) + "");
        //     secondProfitLossAmtTextView.setTextColor(getResources().getColor(R.color.expense));
        // }

        if (firstProfitLoss > secondProfitLoss) {
            firstProfitLossAmtTextView.setTextColor(getResources().getColor(R.color.income));
            setUpArrow(firstProfitLossAmtTextView);
        } else if (firstProfitLoss < secondProfitLoss) {
            firstProfitLossAmtTextView.setTextColor(getResources().getColor(R.color.expense));
            setDownArrow(firstProfitLossAmtTextView);
        } else {
            //firstProfitLossAmtTextView.setTextColor(getResources().getColor(R.color.black));
            setEqual(percentSalesTextView);
        }

        percent = calculatePercent(firstProfitLoss, secondProfitLoss);
        percentProfitLossTextView.setText(POSUtil.PercentFromat(abs(percent)));
        if (percent > 0) {
            setUpArrow(percentProfitLossTextView);
            percentProfitLossTextView.setTextColor(getResources().getColor(R.color.income));
        } else if (percent < 0) {
            setDownArrow(percentProfitLossTextView);
            percentProfitLossTextView.setTextColor(getResources().getColor(R.color.expense));
        } else {
            setEqual(percentProfitLossTextView);
        }

        //Log.e(titleType ,titleType + "blah");

        if (titleType.equalsIgnoreCase("day")) {
            secondExpenseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday}).getString(0));
            secondProfitLossDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday}).getString(0));
            secondSalesDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday}).getString(0));
            secondPurchaseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday}).getString(0));
            secondIncomeDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday}).getString(0));

            firstExpenseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.today}).getString(0));
            firstProfitLossDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.today}).getString(0));
            firstSalesDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.today}).getString(0));
            firstPurchaseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.today}).getString(0));
            firstIncomeDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.today}).getString(0));
            //firstTitleDateTextView.setText(DateUtility.makeDateFormatWithSlash(DateUtility.getTodayDate()));

            //secondTitleTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday}).getString(0));
            // secondTitleDateTextView.setText(DateUtility.makeDateFormatWithSlash(DateUtility.getYesterDayDate()));
        } else if (titleType.equalsIgnoreCase("month")) {
            secondExpenseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month}).getString(0));
            secondProfitLossDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month}).getString(0));
            secondSalesDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month}).getString(0));
            secondPurchaseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month}).getString(0));
            secondIncomeDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month}).getString(0));
            // firstTitleDateTextView.setText(DateUtility.makeDateFormatWithSlash(DateUtility.getThisMonthStartDate()) + " ~ " + DateUtility.makeDateFormatWithSlash(DateUtility.getThisMonthEndDate()));

            //secondTitleTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month}).getString(0));


            firstExpenseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month}).getString(0));
            firstProfitLossDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month}).getString(0));
            firstSalesDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month}).getString(0));
            firstPurchaseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month}).getString(0));
            firstIncomeDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month}).getString(0));

            // secondTitleDateTextView.setText(DateUtility.makeDateFormatWithSlash(DateUtility.getLastMonthStartDate()) + " ~ " + DateUtility.makeDateFormatWithSlash(DateUtility.getLastMonthEndDate()));
        } else if (titleType.equalsIgnoreCase("week")) {
            secondExpenseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week}).getString(0));
            secondProfitLossDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week}).getString(0));
            secondSalesDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week}).getString(0));
            secondPurchaseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week}).getString(0));
            secondIncomeDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week}).getString(0));
            // firstTitleDateTextView.setText(DateUtility.makeDateFormatWithSlash(DateUtility.getStartDateOfWeekString()) + " ~ " + DateUtility.makeDateFormatWithSlash(DateUtility.getEndDateOfWeekString()));

            // secondTitleTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week}).getString(0));

            firstExpenseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_week}).getString(0));
            firstProfitLossDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_week}).getString(0));
            firstSalesDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_week}).getString(0));
            firstPurchaseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_week}).getString(0));
            firstIncomeDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_week}).getString(0));

            //secondTitleDateTextView.setText(DateUtility.makeDateFormatWithSlash(DateUtility.getStartDateOfLastWeekString()) + " ~ " + DateUtility.makeDateFormatWithSlash(DateUtility.getEndDateOfLastWeekString()));
        } else if (titleType.equalsIgnoreCase("year")) {
            secondExpenseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString(0));
            secondProfitLossDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString(0));
            secondSalesDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString(0));
            secondPurchaseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString(0));
            secondIncomeDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString(0));
            //firstTitleDateTextView.setText(DateUtility.makeDateFormatWithSlash(DateUtility.getThisYearStartDate()) + " ~ " + DateUtility.makeDateFormatWithSlash(DateUtility.getThisYearEndDate()));

            firstExpenseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year}).getString(0));
            firstProfitLossDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year}).getString(0));
            firstSalesDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year}).getString(0));
            firstPurchaseDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year}).getString(0));
            firstIncomeDescTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year}).getString(0));

            //secondTitleTextView.setText(dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString(0));
            //    secondTitleDateTextView.setText(DateUtility.makeDateFormatWithSlash(DateUtility.getLastYearStartDate()) + " ~ " + DateUtility.makeDateFormatWithSlash(DateUtility.getLastYearEndDate()));
        }
    }

    private void setDownArrow(TextView textView) {
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.down_arrow, 0, 0, 0);
        textView.setCompoundDrawablePadding(5);
        for (Drawable drawable1 : textView.getCompoundDrawables()) {
            if (drawable1 != null) {
                drawable1.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.expense), PorterDuff.Mode.SRC_IN));
            }
        }
    }

    private void setUpArrow(TextView textView) {
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.up_arrow, 0, 0, 0);
        textView.setCompoundDrawablePadding(5);
        for (Drawable drawable1 : textView.getCompoundDrawables()) {
            if (drawable1 != null) {
                drawable1.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.income), PorterDuff.Mode.SRC_IN));
            }
        }
    }

    private void setEqual(TextView textView) {
        //textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_equal, 0, 0, 0);
        //textView.setCompoundDrawablePadding(5);
    }

    class LoadProgressDialog extends AsyncTask<String, String, String> {
        private KProgressHUD hud;

        @Override
        protected String doInBackground(String... params) {
           /* try{
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            Log.e("do", "in background" + titleType);
            initializeList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("pre show", "show");
            String    label         = "";
            View      processDialog = View.inflate(getContext(), R.layout.process_dialog_custom_layout, null);
            ImageView imageView     = (ImageView) processDialog.findViewById(R.id.image_view);
            imageView.setBackgroundResource(R.drawable.spin_animation);
            AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
            drawable.start();

            //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setCustomView(processDialog)
                    .setDetailsLabel(label)
                    .setWindowColor(Color.parseColor("#66000000"))
                    .setAnimationSpeed(1)
                    .setCancellable(true);
            if (!hud.isShowing()) {
                hud.show();
            }
        }

        @Override
        protected void onPostExecute(String a) {

            //Log.e("begin", "idoaj");
            Log.e("on", "post execute");
            if (isAdded()) {
                configUI();
                // settingBarChart();


                Log.e("dismiss", "dismiss");
            }
            if (hud.isShowing()) {
                hud.dismiss();
                Log.e("dismiss", "showing");
                //    showSnackBar("Backup process is successful...");
            }
        }


        private void showSnackBar(String msg) {
            Snackbar snackbar = Snackbar.make(mainLayoutView, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
