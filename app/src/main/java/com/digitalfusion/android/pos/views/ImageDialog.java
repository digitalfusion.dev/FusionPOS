package com.digitalfusion.android.pos.views;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

/**
 * Created by MD003 on 12/7/16.
 */

public class ImageDialog extends DialogFragment {

    private View mainLayoutView;

    private Dialog mDialog;

    private Bitmap image;


    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        int width = getResources().getDisplayMetrics().widthPixels * 2;

        int height = getResources().getDisplayMetrics().heightPixels * 3;

        dialog.getWindow().setLayout(width, height);

        //  dialog.setContentView(R.layout.image_view);

        mDialog = dialog;

        return dialog;
    }


    @Override
    public Dialog getDialog() {

        return mDialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);

        getDialog().getWindow().setLayout(width, width);

        ImageView imageView = new ImageView(getContext());

        imageView.setLayoutParams(new ViewGroup.LayoutParams(width, width));

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(image, width, width, false);

        imageView.setImageBitmap(resizedBitmap);

        getDialog().getWindow().setLayout(imageView.getWidth(), imageView.getWidth());


        return imageView;

    }


    public Bitmap getImage() {

        return image;

    }

    public void setImage(Bitmap image) {

        this.image = image;

    }
}
