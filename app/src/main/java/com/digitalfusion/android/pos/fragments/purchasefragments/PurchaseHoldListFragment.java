package com.digitalfusion.android.pos.fragments.purchasefragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.PurchaseHistorySearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwiperAdapterForPurchaseTransactions;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.AuthorizationManager;
import com.digitalfusion.android.pos.database.business.PurchaseManager;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.PurchaseHistory;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;
import com.example.searchview.MaterialSearchView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 2/23/18.
 */

public class PurchaseHoldListFragment extends Fragment {

    String allTrans;
    String thisWeek;
    String lastWeek;
    String thisMonth;
    String lastMonth;
    String thisYear;
    String lastYear;
    String customRange;
    String customDate;
    private View mainLayout;
    private LinearLayout linearLayout;
    private TextView noTransactionTextView;
    private FloatingActionButton addNewPurchase;
    private RecyclerView purchaseHistoryListRecyclerView;
    private Context context;
    private List<PurchaseHistory> purchaseHistoryList;
    private RVSwiperAdapterForPurchaseTransactions rvSwiperAdapterForPurchaseTransactions;
    private PurchaseManager purchaseManager;
    private MaterialSearchView searchView;
    private MaterialDialog filterDialog;
    private List<String> filterList;
    private RVAdapterForFilter rvAdapterForFilter;
    private TextView filterTextView;
    private TextView searchedResultTxt;
    private String startDate;
    private String endDate;
    private DatePickerDialog customeDatePickerDialog;
    private Calendar calendar;
    private PurchaseHistorySearchAdapter purchaseHistorySearchAdapter;
    private boolean isSearch = false;
    private boolean shouldLoad = true;
    private String searchText = "";
    private DatePickerDialog startDatePickerDialog;
    private MaterialDialog customRangeDialog;
    private TextView startDateTextView, endDateTextView;
    private String customStartDate,
            customEndDate;
    private TextView traceDate;
    private Button customRangeOkBtn, customRangeCancelBtn;
    private int oldPos = 0;
    private int current = 0;
    private MaterialDialog purchaseDeleteAlertDialog;
    private Button yesSaleDeleteMdButton;
    private int deletepos;
    private boolean darkmode;
    private boolean isAdd;
    private PurchaseHistory purchaseHistory;
    private String userId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.purchase_history_list, null);

        setHasOptionsMenu(true);

        context = getContext();

        darkmode = POSUtil.isNightMode(context);

        startDate = "000000000000";

        endDate = "9999999999999999";

        final TypedArray purchaseHistory = context.getTheme().obtainStyledAttributes(new int[]{R.attr.purchase_history});

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(purchaseHistory.getString(0));

        purchaseManager = new PurchaseManager(context);

        purchaseHistoryList = purchaseManager.getAllHoldPurchases(0, 10, new InsertedBooleanHolder(), "00000000", "999999999999" ,userId);

        Log.w("purchase histroy", purchaseHistoryList.size() + " SSS");

        purchaseHistorySearchAdapter = new PurchaseHistorySearchAdapter(context, purchaseManager);

        filterList = new ArrayList<>();
        allTrans = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);

        thisMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month}).getString(0);

        lastMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month}).getString(0);

        thisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year}).getString(0);

        lastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString(0);

        customRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_range}).getString(0);

        customDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_date}).getString(0);

        thisWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_week}).getString(0);

        lastWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week}).getString(0);



        filterList.add(allTrans);

        filterList.add(thisWeek);

        filterList.add(lastWeek);

        filterList.add(thisMonth);

        filterList.add(lastMonth);

        filterList.add(thisYear);

        filterList.add(lastYear);

        filterList.add(customRange);

        filterList.add(customDate);

        calendar = Calendar.getInstance();

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();

        loadUI();

        buildingCustomRangeDialog();

        configRecycler();


        mainLayout.findViewById(R.id.filter_display).setVisibility(View.GONE);
        mainLayout.findViewById(R.id.filter_sperater).setVisibility(View.GONE);
        mainLayout.findViewById(R.id.filter_one).setVisibility(View.GONE);
        mainLayout.findViewById(R.id.txt_filter_staff).setVisibility(View.GONE);
        mainLayout.findViewById(R.id.filter_staff_sperater).setVisibility(View.GONE);
        mainLayout.findViewById(R.id.filter_staff).setVisibility(View.GONE);

        searchView.setAdapter(purchaseHistorySearchAdapter);

        searchView.showSuggestions();

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // shouldLoad = false;

                // isSearch = true;

                Bundle bundle = new Bundle();

                bundle.putLong(PurchaseDetailFragment.KEY, purchaseHistorySearchAdapter.getSuggestion().get(position).getId());

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.PURCHASE_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);

                //purchaseHistoryList = new ArrayList<>();

                //purchaseHistoryList.add(purchaseHistorySearchAdapter.getSuggestionList().get(position));

                //refreshRecyclerView();

                searchView.closeSearch();

                //filterTextView.setText("-");

                //searchedResultTxt.setVisibility(View.VISIBLE);


            }
        });
        MainActivity.setCurrentFragment(this);

        AccessLogManager accessLogManager     = new AccessLogManager(context);
        AuthorizationManager authorizationManager = new AuthorizationManager(context);
        Long                 deviceId             = authorizationManager.getDeviceId(Build.SERIAL);
        User currentUser          = accessLogManager.getCurrentlyLoggedInUser(deviceId);
        if (currentUser == null) {
            POSUtil.noUserIsLoggedIn(context);
            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            return null;
        }
        if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Sale.toString())) {
            userId = " = " + currentUser.getId();
        } else {
            userId = " IS NOT NULL";
        }
        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });

        //        filterTextView.addTextChangedListener(new TextWatcher() {
        //            @Override
        //            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //
        //            }
        //
        //            @Override
        //            public void onTextChanged(CharSequence s, int start, int before, int count) {
        //                if (!filterTextView.getText().toString().trim().equals("-") && !filterTextView.getText().toString().trim().isEmpty() && searchedResultTxt.isShown()) {
        //                    searchedResultTxt.setVisibility(View.INVISIBLE);
        //                }
        //            }
        //
        //            @Override
        //            public void afterTextChanged(Editable s) {
        //
        //            }
        //        });

        addNewPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // MainActivity.replacingFragment(new AddEditNewPurchaseFragment());

                isAdd = true;

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_PURCHASE);

                startActivity(addCurrencyIntent);
            }
        });

        customeDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        String tempdate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        startDate = tempdate;

                        endDate = tempdate;

                        //String date= DateUtility.makeDateFormatWithSlash(Integer.toString(year),Integer.toString(monthOfYear+1),Integer.toString(dayOfMonth));

                        //String dayDes[]= DateUtility.dayDes(date);

                        //String yearMonthDes=DateUtility.monthYearDes(date);

                        filterTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));

                        loadPurchaseHist(0, 100);

                        refreshRecyclerView();
                    }
                },
                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)
        );


        if (darkmode)
            customeDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        customeDatePickerDialog.setThemeDark(darkmode);


        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                shouldLoad = true;

                isSearch = false;

                tracePos(position);

                if (filterList.get(position).equalsIgnoreCase(allTrans)) {

                    setFilterTextView(filterList.get(position));

                    startDate = "000000000000";

                    endDate = "9999999999999999";

                    loadPurchaseHist(0, 100);

                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(thisMonth)) {

                    loadThisMonthTransaction(position);

                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    loadPurchaseHist(0, 100);

                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    loadPurchaseHist(0, 100);

                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    loadPurchaseHist(0, 100);

                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {

                    customRangeDialog.show();

                } else if (filterList.get(position).equalsIgnoreCase(customDate)) {

                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "customeDate");

                } else if (filterList.get(position).equalsIgnoreCase(thisWeek)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getStartDateOfWeekString();

                    endDate = DateUtility.getEndDateOfWeekString();

                    loadPurchaseHist(0, 100);

                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(lastWeek)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getStartDateOfLastWeekString();

                    endDate = DateUtility.getEndDateOfLastWeekString();

                    loadPurchaseHist(0, 100);

                    refreshRecyclerView();

                }
                listenRV();
                filterDialog.dismiss();
            }
        });

        rvSwiperAdapterForPurchaseTransactions.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle = new Bundle();

                bundle.putSerializable("purchaseheader", purchaseHistoryList.get(postion));

                PurchaseHoldListFragment.this.purchaseHistory = purchaseHistoryList.get(postion);

                // AddEditNewPurchaseFragment editPurchaseFragment=new AddEditNewPurchaseFragment();

                // editPurchaseFragment.setArguments(bundle);

                // MainActivity.replacingFragment(editPurchaseFragment);


                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtras(bundle);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_PURCHASE);

                startActivity(addCurrencyIntent);

            }
        });

        rvSwiperAdapterForPurchaseTransactions.setViewDetailClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle = new Bundle();

                bundle.putLong(PurchaseDetailFragment.KEY, purchaseHistoryList.get(postion).getId());

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.PURCHASE_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                searchText = query;

                shouldLoad = true;

                isSearch = true;
                query = query.replaceFirst("#", "");
                loadPurchaseHist(0, 100, query);

                refreshRecyclerView();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                return false;
            }
        });


        listenRV();

        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDate = DateUtility.makeDateFromSlash(customStartDate);

                endDate = DateUtility.makeDateFromSlash(customEndDate);

                filterTextView.setText(customStartDate + " - " + customEndDate);

                loadPurchaseHist(0, 10);

                refreshRecyclerView();

                customRangeDialog.dismiss();
            }
        });

        setFilterTextView(filterList.get(3));

        rvAdapterForFilter.setCurrentPos(0);

        buildOrderCancelAlertDialog();

        rvSwiperAdapterForPurchaseTransactions.setViewPaymentsClickListerner(new ClickListener() {
            @Override
            public void onClick(int postion) {
                Bundle bundle = new Bundle();

                bundle.putSerializable("purchaseheader", purchaseHistoryList.get(postion));

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.PAYABLE_PAYMENT_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);
            }
        });

        rvSwiperAdapterForPurchaseTransactions.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                deletepos = postion;
                purchaseDeleteAlertDialog.show();
            }
        });

        yesSaleDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                purchaseManager.deleteHoldPurchase(purchaseHistoryList.get(deletepos).getId());

                Log.w("hello delete", purchaseHistoryList.get(deletepos).getVoucherNo() + "DDD");


                //  rvSwiperAdapterForPurchaseTransactions.setPurchaseHistoryList(purchaseHistoryList);

                if (purchaseHistoryList.size() == 1) {
                    purchaseHistoryList.remove(deletepos);
                    rvSwiperAdapterForPurchaseTransactions.setPurchaseHistoryList(purchaseHistoryList);
                    rvSwiperAdapterForPurchaseTransactions.notifyDataSetChanged();
                } else {
                    purchaseHistoryList.remove(deletepos);
                    rvSwiperAdapterForPurchaseTransactions.notifyItemRemoved(deletepos);

                    rvSwiperAdapterForPurchaseTransactions.notifyItemRangeChanged(deletepos, purchaseHistoryList.size());

                }

                purchaseDeleteAlertDialog.dismiss();

            }
        });


        purchaseHistoryListRecyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {

                Log.w("V X", velocityX + " S");

                Log.w("V Y", velocityY + " S");

                if (velocityY > 100) {
                    addNewPurchase.hide();
                } else if (velocityY < -100) {
                    addNewPurchase.show();
                }


                return false;
            }
        });

        return mainLayout;
    }

    private void listenRV() {
        purchaseHistoryListRecyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                Log.w("on load more", "on load more");

                if (shouldLoad) {

                    rvSwiperAdapterForPurchaseTransactions.setShowLoader(true);

                    loadmore();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // rvAdapterForFilter.setCurrentPos(3);
        listenRV();
        rvAdapterForFilter.setCurrentPos(0);
        //        if(purchaseHistory!=null){
        //
        //            purchaseHistory =purchaseManager.getPurchase(purchaseHistory.getId());
        //
        //            Log.w("updateRegistration",purchaseHistory.toString());
        //
        //            rvSwiperAdapterForPurchaseTransactions.updateItem(purchaseHistory);
        //
        //            purchaseHistory=null;
        //        }
        //        if(isAdd){
        //            isAdd=false;
        //            rvAdapterForFilter.setCurrentPos(current);
        //        }


    }

    private void refreshRecyclerView() {

        if (purchaseHistoryList.isEmpty() && purchaseHistoryList.size() < 10) {

            noTransactionTextView.setVisibility(View.VISIBLE);

            purchaseHistoryListRecyclerView.setVisibility(View.GONE);

        } else {

            noTransactionTextView.setVisibility(View.GONE);

            purchaseHistoryListRecyclerView.setVisibility(View.VISIBLE);

        }

        rvSwiperAdapterForPurchaseTransactions.setPurchaseHistoryList(purchaseHistoryList);

        rvSwiperAdapterForPurchaseTransactions.notifyDataSetChanged();
    }

    private void loadThisMonthTransaction(int position) {
        setFilterTextView(filterList.get(position));

        startDate = DateUtility.getThisMonthStartDate();

        endDate = DateUtility.getThisMonthEndDate();

        loadPurchaseHist(0, 10);

        refreshRecyclerView();
    }

    private void buildOrderCancelAlertDialog() {

        //TypedArray no = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        //TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        purchaseDeleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        String   wantToDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete}).getString(0);
        TextView textView     = (TextView) purchaseDeleteAlertDialog.findViewById(R.id.title);
        textView.setText(wantToDelete);

        yesSaleDeleteMdButton = (Button) purchaseDeleteAlertDialog.findViewById(R.id.save);

        purchaseDeleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purchaseDeleteAlertDialog.dismiss();
            }
        });
    }

    private void tracePos(int pos) {

        oldPos = current;
        current = pos;

    }

    public void loadmore() {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isSearch) {

                    purchaseHistoryList.addAll(load(purchaseHistoryList.size(), 9, searchText));
                } else {
                    purchaseHistoryList.addAll(load(purchaseHistoryList.size(), 9));
                }

                rvSwiperAdapterForPurchaseTransactions.setShowLoader(false);


                refreshRecyclerView();
            }
        }, 500);
    }

    private void loadPurchaseHist(int startLimit, int endLimit) {

        Log.e("start" + startDate, " end" + endDate);

        purchaseHistoryList = purchaseManager.getAllHoldPurchases(startLimit, endLimit, new InsertedBooleanHolder(), startDate, endDate, userId);

        Log.e("size", purchaseHistoryList.size() + " dd");
    }

    private List<PurchaseHistory> load(int startLimit, int endLimit) {

        List<PurchaseHistory> loadList;
        loadList = purchaseManager.getAllHoldPurchases(startLimit, endLimit, new InsertedBooleanHolder(), startDate, endDate , userId);

        return loadList;
    }

    private void loadPurchaseHist(int startLimit, int endLimit, String quern) {

        purchaseHistoryList = purchaseManager.getHoldPurchaseWithVoucherNoOrSupplier(startLimit, endLimit, quern);

    }

    private List<PurchaseHistory> load(int startLimit, int endLimit, String quern) {

        List<PurchaseHistory> purchaseHistories;

        purchaseHistories = purchaseManager.getHoldPurchaseWithVoucherNoOrSupplier(startLimit, endLimit, quern);

        return purchaseHistories;
    }


    public void configRecycler() {

        rvSwiperAdapterForPurchaseTransactions = new RVSwiperAdapterForPurchaseTransactions(purchaseHistoryList);

        purchaseHistoryListRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        purchaseHistoryListRecyclerView.setAdapter(rvSwiperAdapterForPurchaseTransactions);

    }

    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});

        int attributeResourceId = a.getResourceId(0, 0);

        searchView.setCursorDrawable(attributeResourceId);

    }

    private void setFilterTextView(String msg) {

        filterTextView.setText(msg);

    }

    public void loadUI() {

        noTransactionTextView = (TextView) mainLayout.findViewById(R.id.no_transaction);

        linearLayout = (LinearLayout) mainLayout.findViewById(R.id.ll);

        addNewPurchase = (FloatingActionButton) mainLayout.findViewById(R.id.add_new_purchase);

        purchaseHistoryListRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.purchase_history_list_rv);

        filterTextView = (TextView) mainLayout.findViewById(R.id.filter_one);

        searchedResultTxt = (TextView) mainLayout.findViewById(R.id.searched_result_txt);

        loadUIFromToolbar();

    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(customStartDate);
                                                                         startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(customEndDate);
                                                                         endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });


        if (darkmode)
            startDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        startDatePickerDialog.setThemeDark(darkmode);

    }

    public void buildingCustomRangeDialog() {


        TypedArray dateRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range});
        //TypedArray ok = context.getTheme().obtainStyledAttributes(new int[]{R.attr.ok});
        //TypedArray cancel = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});
        buildingCustomRangeDatePickerDialog();
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(dateRange.getString(0))
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                // .negativeText(cancel.getString(0))
                // .positiveText(ok.getString(0))
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.main, menu);

        MenuItem item = menu.findItem(R.id.action_search);

        searchView.setMenuItem(item);

        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void buildDateFilterDialog() {


        TypedArray filterByDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date});

        filterDialog = new MaterialDialog.Builder(context).
                title(filterByDate.getString(0))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))
                .build();
    }
}