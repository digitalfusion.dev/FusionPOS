package com.digitalfusion.android.pos.activities;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.ApiManager;
import com.digitalfusion.android.pos.information.Subscription;
import com.digitalfusion.android.pos.information.wrapper.DeviceRequest;
import com.digitalfusion.android.pos.network.ApiRetrofit;
import com.digitalfusion.android.pos.network.NetworkClient;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.ConnectivityUtil;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.FusionToast;
import com.kaopiz.kprogresshud.KProgressHUD;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class VerifyActivity extends AppCompatActivity {
    public static final String FORGETPASS = "forget";
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;
    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private TextView statusTextView;
    private TextView statusExplanationTextView;
    private ImageView automaticTimeZoneOnOffImageView;
    private int autoTimeAvailable = 0;
    private KProgressHUD hud;
    private TextView userID;
    private View timeZoneOffView;
    private View internetOnOffView;
    private boolean isForgetPass;
    private View forgetPasscodeView;
    private View verifyLabelView;
    private EditText verifyCodeEditText;
    private Button verify;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.verify_screen);

        isForgetPass = getIntent().getBooleanExtra(FORGETPASS, false);

        statusTextView = findViewById(R.id.status_msg);
        statusExplanationTextView = findViewById(R.id.status_explanation);

        automaticTimeZoneOnOffImageView = findViewById(R.id.ic_automatic_time_zone);
        userID = findViewById(R.id.user_id);
        timeZoneOffView = findViewById(R.id.time_zone_off);
        internetOnOffView = findViewById(R.id.ic_internet);

        forgetPasscodeView = findViewById(R.id.forget_pasword);
        verifyLabelView = findViewById(R.id.verify_code_label);
        verifyCodeEditText = findViewById(R.id.verify_code);
        verify = findViewById(R.id.verify);

        checkCondition();

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void checkCondition() {
        ApiManager apiManager = new ApiManager(this);
        userID.setText(apiManager.getRegistration().getUserId());

        if (isForgetPass) {

            forgetPasscodeView.setVisibility(View.VISIBLE);
            verifyCodeEditText.setVisibility(View.VISIBLE);
            verifyLabelView.setVisibility(View.VISIBLE);
            verify.setVisibility(View.VISIBLE);
            findViewById(R.id.try_again).setVisibility(View.GONE);

        } else {

            findViewById(R.id.try_again).setVisibility(View.VISIBLE);
            forgetPasscodeView.setVisibility(View.GONE);
            verifyCodeEditText.setVisibility(View.GONE);
            verifyLabelView.setVisibility(View.GONE);
            verify.setVisibility(View.GONE);

        }

        if (POSUtil.isUnExpectedChangeBoolean(this)) {

            findViewById(R.id.unformal_data).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.unformal_data).setVisibility(View.GONE);
        }
        if (ConnectivityUtil.isConnected(this)) {
            Log.w("herer", "Mobile Data ON");
            internetOnOffView.setActivated(true);
        } else {
            internetOnOffView.setActivated(false);
            Log.w("herer", "Mobile Data OFF");
        }

        try {
            autoTimeAvailable = Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        if (POSUtil.isAutomaticTimeZone(this)) {
            automaticTimeZoneOnOffImageView.setActivated(true);
            timeZoneOffView.setVisibility(View.GONE);
        } else {
            timeZoneOffView.setVisibility(View.VISIBLE);
            automaticTimeZoneOnOffImageView.setActivated(false);
        }


        if (!POSUtil.isActivatedBoolean(this)) {
            findViewById(R.id.new_license).setVisibility(View.GONE);
            findViewById(R.id.old_user).setVisibility(View.GONE);
            findViewById(R.id.status_msg).setVisibility(View.GONE);
            timeZoneOffView.setVisibility(View.GONE);
        }


        if (POSUtil.checkIsExpire(this) && POSUtil.isActivatedBoolean(this)) {
            findViewById(R.id.status_msg).setVisibility(View.VISIBLE);
            findViewById(R.id.new_license).setVisibility(View.VISIBLE);
            findViewById(R.id.old_user).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.status_msg).setVisibility(View.GONE);
            findViewById(R.id.new_license).setVisibility(View.GONE);
            findViewById(R.id.old_user).setVisibility(View.GONE);
        }
    }

    private boolean isValidVerifyCode() {

        boolean status = true;

        if (verifyCodeEditText.getText().toString().trim().length() < 1) {
            status = false;
            verifyCodeEditText.setError("Please Enter Verify Code To Reset Passcode!!");
        }

        return status;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onResume() {
        super.onResume();

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ApiRetrofit apiRetrofit = NetworkClient.createService(ApiRetrofit.class, "digital_fusion", "digital_fusion");

                RequestBody uid =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, userID.getText().toString());
                RequestBody code =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, verifyCodeEditText.getText().toString());

                Call<ResponseBody> call = apiRetrofit.verifyCode(code, uid);

                if (isValidVerifyCode()) {
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Log.w("Response Code", response.code() + " ss");
                            if (response.code() == 200) {
                                Intent intent = new Intent(VerifyActivity.this, AddNewPasscodeActivity.class);
                                startActivity(intent);
                            } else if (response.code() == 403) {
                                FusionToast.toast(VerifyActivity.this, FusionToast.TOAST_TYPE.ERROR, "Verification Code has been expired!!");
                            } else if (response.code() == 204) {
                                FusionToast.toast(VerifyActivity.this, FusionToast.TOAST_TYPE.ERROR, "No Verification Code Match!!");
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            ConnectivityUtil.handleFailureProcess(t, userID);
                        }
                    });
                }


            }
        });

        findViewById(R.id.try_again).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ApiRetrofit   apiRetrofit   = NetworkClient.createService(ApiRetrofit.class, "digital_fusion", "digital_fusion");
                final DeviceRequest deviceRequest = new DeviceRequest();
                Log.e("Mac", deviceRequest.getMacAddress());
                Log.e("Seiral", deviceRequest.getSerial());
                Call<Subscription> checkDeviceCall = apiRetrofit.checkDevice(deviceRequest);
                View               processDialog   = View.inflate(VerifyActivity.this, R.layout.process_dialog_custom_layout, null);
                ImageView          imageView       = (ImageView) processDialog.findViewById(R.id.image_view);
                imageView.setBackgroundResource(R.drawable.spin_animation);
                AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
                drawable.start();

                //String please= dashboardContext.getTheme().obtainStyledAttributes(new int[]{R.attr.please_wait}).getString(0);
                hud = KProgressHUD.create(VerifyActivity.this)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setCustomView(processDialog)
                        .setDetailsLabel("")
                        .setBackgroundColor(Color.parseColor("#66000000"))
                        .setAnimationSpeed(1)
                        .setCancellable(true);

                hud.show();
                checkDeviceCall.enqueue(new Callback<Subscription>() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
                    @Override
                    public void onResponse(Call<Subscription> call, Response<Subscription> response) {

                        if (POSUtil.isAutomaticTimeZone(VerifyActivity.this)) {
                            if (response.code() == 200) {
                                Subscription responseSubscription = response.body();
                                POSUtil.acitvate(responseSubscription, VerifyActivity.this);

                          /*  Registration registration=responseSubscription.getRegistration();
                            POSUtil.NotUnExpectedChange(VerifyActivity.this);
                            ApiManager apiBusiness=new ApiManager(VerifyActivity.this);

                            apiBusiness.updateRegistration(registration.getUserName(),registration.getUserId(),registration.getBusinessName(),registration.getUserStatus());
                            apiBusiness.addLicense(responseSubscription.getLicenseInfo().getDuration(),responseSubscription.getLicenseInfo().getStartDate(),responseSubscription.getLicenseInfo().getEndDate());

                            */

                                if (POSUtil.checkIsExpire(VerifyActivity.this)) {
                                    FusionToast.toast(VerifyActivity.this);
                                    checkCondition();
                                } else {

                                    ApiManager apiManager = new ApiManager(VerifyActivity.this);
                                    if (apiManager.isPasscodeSet(apiManager.getOwnerId())) {
                                        Intent intent = new Intent(VerifyActivity.this, PasscodeActivity.class);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(VerifyActivity.this, AddNewPasscodeActivity.class);
                                        startActivity(intent);
                                    }
                                }
                                Log.e("body", responseSubscription.toString());


                            } else if (response.code() == 404) {

                                Log.e("Mac", deviceRequest.getMacAddress());
                                Log.e("Seiral", deviceRequest.getSerial());
                                Log.e("body", "New Device");

                                Intent intent = new Intent(VerifyActivity.this, RegisterationActivity.class);

                                startActivity(intent);

                            }
                        } else {
                            Log.w("NO AUTOMATIC TIME ZXONE", "NO ON CREATE");
                            checkCondition();
                        }


                        hud.dismiss();
                    }

                    @Override
                    public void onFailure(Call<Subscription> call, Throwable t) {
                        t.printStackTrace();
                        ConnectivityUtil.handleFailureProcess(t, automaticTimeZoneOnOffImageView);
                        hud.dismiss();
                    }
                });
            }
        });

        findViewById(R.id.copy_paste).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData         clip      = ClipData.newPlainText("USER ID", userID.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(v.getContext(), "User ID Copied", Toast.LENGTH_LONG).show();

            }
        });

        checkCondition();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.

    }

}
