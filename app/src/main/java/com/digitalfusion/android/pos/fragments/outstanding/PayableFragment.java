package com.digitalfusion.android.pos.fragments.outstanding;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.PayableSearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVAdapterForPurchasePayableList;
import com.digitalfusion.android.pos.database.business.SupplierOutstandingManager;
import com.digitalfusion.android.pos.database.model.Supplier;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.interfaces.RecyclerViewScrollListener;
import com.digitalfusion.android.pos.database.model.PurchaseHistory;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.FusionToast;
import com.example.searchview.MaterialSearchView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 10/18/16.
 */

public class PayableFragment extends Fragment {

    private View mainLayout;

    private Button customRangeOkBtn, customRangeCancelBtn;

    private RecyclerView payableRecyclerView;

    private Context context;

    private SupplierOutstandingManager supplierOutstandingManager;

    private List<PurchaseHistory> purchasePayableList;

    private RVAdapterForPurchasePayableList adapterForPurchasePayableList;

    private TextView noTransactionTextView;

    private MaterialSearchView searchView;

    private MaterialDialog filterDialog;

    private List<String> filterList;

    private RVAdapterForFilter rvAdapterForFilter;

    private TextView filterTextView;

    private String startDate;

    private String endDate;

    private DatePickerDialog customeDatePickerDialog;

    private Calendar calendar;

    private MaterialDialog addPaymentMaterialDialog;

    private Button saveBtnMd;

    private Button cancelBtnMd;

    private EditText paymentDateTxtMd;

    private EditText paymentEditTxtMd;

    private ImageButton calenderBtnMd;

    private TextView paymentId;

    private TextView supplierName;

    private Long supplierIDTemp;

    private Long purchaseIdTemp;

    private int paymentDay;

    private int paymentMonth;

    private int paymentYear;

    private DatePickerDialog datePickerDialog;

    private Supplier supplier;

    private String date;

    private DatePickerDialog startDatePickerDialog;

    private MaterialDialog customRangeDialog;

    private TextView startDateTextView, endDateTextView;

    private String customStartDate, customEndDate;

    private TextView traceDate;

    private int oldPos = 0;

    private int current = 0;

    private PayableSearchAdapter payableSearchAdapter;
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {

            String str = ((SalesHistory) resultValue).getVoucherNo();

            return str;

        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            Log.w("filtering", "filtering");

            if (constraint != null && !constraint.equals("")) {

                if (constraint.toString().startsWith("#")) {
                    payableSearchAdapter.setSuggestion(supplierOutstandingManager.getSupplierOutstandingListOnSearch(0, 10, "00000000000", "99999999999999", constraint.toString().substring(1, constraint.length())));
                } else {
                    payableSearchAdapter.setSuggestion(supplierOutstandingManager.getSupplierOutstandingListOnSearch(0, 10, "00000000000", "99999999999999", constraint.toString()));
                }

                FilterResults filterResults = new FilterResults();

                payableSearchAdapter.lenght = constraint.length();

                payableSearchAdapter.queryText = constraint.toString();

                filterResults.values = payableSearchAdapter.getSuggestion();

                filterResults.count = payableSearchAdapter.getSuggestion().size();

                return filterResults;
            } else {

                return new FilterResults();

            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (results != null && results.count > 0) {

                payableSearchAdapter.notifyDataSetChanged();

            }

        }

    };
    private boolean isSearch = false;
    private boolean shouldLoad = true;
    private String searchText = "";
    private int payPos;
    private TextView searchedResultTxt;
    private String allTrans;
    private String thisWeek;
    private String lastWeek;
    private String thisMonth;
    private String lastMonth;
    private String thisYear;
    private String lastYear;
    private String customRange;
    private String customDate;
    private boolean darkmode;
    private TextView totalPayableTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayout = inflater.inflate(R.layout.payable_list_fragments, null);

        context = getContext();

        calendar = Calendar.getInstance();

        darkmode = POSUtil.isNightMode(context);

        if (getArguments() != null) {
            supplier = (Supplier) getArguments().getSerializable("supplier");
        }
        MainActivity.setCurrentFragment(this);
        buildDatePickerDialog();

        paymentDay = calendar.get(Calendar.DAY_OF_MONTH);

        paymentMonth = calendar.get(Calendar.MONTH) + 1;

        paymentYear = calendar.get(Calendar.YEAR);

        supplierOutstandingManager = new SupplierOutstandingManager(context);

        startDate = "000000000000";

        endDate = "9999999999999999";

        purchasePayableList = supplierOutstandingManager.getSupplierOutstandingList(0, 10, startDate, endDate);

        allTrans = context.getTheme().obtainStyledAttributes(new int[]{R.attr.all}).getString(0);

        thisMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_month}).getString(0);

        lastMonth = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_month}).getString(0);

        thisYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_year}).getString(0);

        lastYear = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_year}).getString(0);

        customRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_range}).getString(0);

        customDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.custom_date}).getString(0);

        thisWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.this_week}).getString(0);

        lastWeek = context.getTheme().obtainStyledAttributes(new int[]{R.attr.last_week}).getString(0);

        filterList = new ArrayList<>();

        filterList.add(allTrans);

        filterList.add(thisWeek);

        filterList.add(lastWeek);

        filterList.add(thisMonth);

        filterList.add(lastMonth);

        filterList.add(thisYear);

        filterList.add(lastYear);

        filterList.add(customRange);

        filterList.add(customDate);

        calendar = Calendar.getInstance();

        rvAdapterForFilter = new RVAdapterForFilter(filterList);

        buildDateFilterDialog();

        buildAddPaymentDialog();


        loadUI();

        configRecycler();

        filterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });

        customeDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        startDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        endDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        String date = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));

                        // String dayDes[]= DateUtility.dayDes(date);

                        //  String yearMonthDes= DateUtility.monthYearDes(date);

                        filterTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));


                        loadSupplierPayable(0, 10);

                        refreshRecyclerView();

                    }
                },

                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)

        );

        if (darkmode)
            customeDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        customeDatePickerDialog.setThemeDark(darkmode);

        buildingCustomRangeDialog();


        return mainLayout;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {


        payableSearchAdapter = new PayableSearchAdapter(context, nameFilter);

        searchView.setAdapter(payableSearchAdapter);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                purchasePayableList = supplierOutstandingManager.getSupplierOutstandingListOnSearch(0, 10, "0000000", "999999999999", query);

                isSearch = true;
                shouldLoad = true;

                searchText = query;

                refreshRecyclerView();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                shouldLoad = false;


                purchasePayableList = new ArrayList<PurchaseHistory>();

                purchasePayableList.add(payableSearchAdapter.getSuggestion().get(position));

                refreshRecyclerView();

                searchView.closeSearch();

                filterTextView.setText("-");

                //                searchedResultTxt.setVisibility(View.VISIBLE);
            }
        });

        setFilterTextView(filterList.get(0));

        rvAdapterForFilter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                //                searchedResultTxt.setVisibility(View.INVISIBLE);

                shouldLoad = true;
                isSearch = false;

                Log.w("filter click", "on filter click" + position);

                if (filterList.get(position).equalsIgnoreCase(allTrans)) {

                    setFilterTextView(filterList.get(position));

                    startDate = "000000000000";

                    endDate = "9999999999999999";

                    loadSupplierPayable(0, 10);

                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(thisMonth)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisMonthStartDate();

                    endDate = DateUtility.getThisMonthEndDate();

                    loadSupplierPayable(0, 10);

                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(lastMonth)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getLastMonthStartDate();

                    endDate = DateUtility.getLastMonthEndDate();

                    loadSupplierPayable(0, 10);

                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(thisYear)) {

                    setFilterTextView(filterList.get(position));

                    startDate = DateUtility.getThisYearStartDate();

                    endDate = DateUtility.getThisYearEndDate();

                    loadSupplierPayable(0, 10);

                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(lastYear)) {

                    setFilterTextView(filterList.get(position));


                    startDate = DateUtility.getLastYearStartDate();

                    endDate = DateUtility.getLastYearEndDate();

                    loadSupplierPayable(0, 10);

                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(customRange)) {

                    //setFilterTextView(filterList.get(position));

                    customRangeDialog.show();

                } else if (filterList.get(position).equalsIgnoreCase(customDate)) {

                    //setFilterTextView(filterList.get(position));

                    customeDatePickerDialog.show(getActivity().getFragmentManager(), "customeDate");

                } else if (filterList.get(position).equalsIgnoreCase(thisWeek)) {

                    setFilterTextView(filterList.get(position));


                    startDate = DateUtility.getStartDateOfWeekString();

                    endDate = DateUtility.getEndDateOfWeekString();

                    loadSupplierPayable(0, 10);

                    refreshRecyclerView();

                } else if (filterList.get(position).equalsIgnoreCase(lastWeek)) {

                    setFilterTextView(filterList.get(position));


                    startDate = DateUtility.getStartDateOfLastWeekString();

                    endDate = DateUtility.getEndDateOfLastWeekString();

                    loadSupplierPayable(0, 10);

                    refreshRecyclerView();

                }

                totalPayableTextView.setText(POSUtil.NumberFormat(supplierOutstandingManager.getTotalPayableAmt(startDate, endDate)));
                filterDialog.dismiss();

            }
        });


        adapterForPurchasePayableList.setViewDetailListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle = new Bundle();

                bundle.putLong(PayableDetailFragment.KEY, purchasePayableList.get(postion).getId());

                Intent detailIntent = new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.PAYABLE_DETIL_VOUCHER);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);
            }
        });

      /*  adapterForPurchasePayableList.setViewPaymentListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle=new Bundle();

                bundle.putSerializable("purchaseheader", purchasePayableList.get(postion));

                Intent detailIntent=new Intent(context, DetailActivity.class);

                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.PAYABLE_PAYMENT_DETAIL);

                detailIntent.putExtras(bundle);

                startActivity(detailIntent);

            }
        });
*/
        adapterForPurchasePayableList.setAddPaymentListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                payPos = postion;

                setInfromationForPayment(postion);

                configDateUI();

                //clearPaymentAmount();

                addPaymentMaterialDialog.show();
            }
        });

        /*adapterForPurchasePayableList.setViewDetailClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                Bundle bundle=new Bundle();

                bundle.putSerializable("purchaseheader", purchasePayableList.get(postion));

                PurchaseDetailFragment purchaseDetailFragment=new PurchaseDetailFragment();

                purchaseDetailFragment.setArguments(bundle);

                MainActivity.replacingFragment(purchaseDetailFragment);

            }
        });*/

        saveBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!paymentEditTxtMd.getText().toString().isEmpty()) {

                    supplierOutstandingManager.addSupplierOutstandingPayment(supplierOutstandingManager.getSupplierOutstandingPaymentInvoiceNo(),
                            supplierIDTemp, purchaseIdTemp, Double.valueOf(paymentEditTxtMd.getText().toString()),
                            Integer.toString(paymentDay), Integer.toString(paymentMonth), Integer.toString(paymentYear));

                    addPaymentMaterialDialog.dismiss();

                    purchasePayableList.get(payPos).setBalance(purchasePayableList.get(payPos).getBalance() - Double.valueOf(paymentEditTxtMd.getText().toString()));

                    if (purchasePayableList.get(payPos).getBalance() > 0) {

                        adapterForPurchasePayableList.notifyItemChanged(payPos);

                        addPaymentMaterialDialog.dismiss();
                    } else if (payPos != 0) {

                        purchasePayableList.remove(payPos);

                        adapterForPurchasePayableList.setPurchasePayableList(purchasePayableList);

                        adapterForPurchasePayableList.notifyItemRemoved(payPos);

                        adapterForPurchasePayableList.notifyItemChanged(payPos, purchasePayableList.size());

                        addPaymentMaterialDialog.dismiss();

                    } else {
                        purchasePayableList.remove(payPos);

                        adapterForPurchasePayableList.setPurchasePayableList(purchasePayableList);

                        adapterForPurchasePayableList.notifyDataSetChanged();
                    }
                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                }
            }
        });

        cancelBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPaymentMaterialDialog.dismiss();
            }
        });

        paymentDateTxtMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show(getActivity().getFragmentManager(), "Date Pick");
            }
        });

        calenderBtnMd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show(getActivity().getFragmentManager(), "Date Pick");
            }
        });


        customRangeOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDate = customStartDate;

                endDate = customEndDate;

                filterTextView.setText(DateUtility.makeDateFormatWithSlash(startDate) + " - " + DateUtility.makeDateFormatWithSlash(endDate));

                //  filterTextView.setText(Html.fromHtml(startDayDes[1] + "<sup><small>" + startDayDes[0] + "</small></sup>"+startYearMonthDes+" ~ "+endDayDes[1] + "<sup><small>" + endDayDes[0] + "</small></sup>"+endYearMonthDes));

                loadSupplierPayable(0, 10);

                refreshRecyclerView();


                customRangeDialog.dismiss();
            }
        });

        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customRangeDialog.dismiss();
            }
        });

        payableRecyclerView.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {

                if (shouldLoad) {

                    adapterForPurchasePayableList.setShowLoader(true);

                    loadmore();
                }
            }
        });

        rvAdapterForFilter.setCurrentPos(3);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void loadmore() {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isSearch) {
                    purchasePayableList.addAll(loadMore(purchasePayableList.size(), 9, searchText));
                } else {
                    purchasePayableList.addAll(loadMore(purchasePayableList.size(), 9));
                }

                purchasePayableList.addAll(loadMore(purchasePayableList.size(), 9));

                adapterForPurchasePayableList.setShowLoader(false);

                refreshRecyclerView();
            }
        }, 500);
    }

    public List<PurchaseHistory> loadMore(int startLimit, int endLimit) {

        return supplierOutstandingManager.getSupplierOutstandingList(startLimit, endLimit, startDate, endDate);


    }

    public List<PurchaseHistory> loadMore(int startLimit, int endLimit, String query) {

        return supplierOutstandingManager.getSupplierOutstandingListOnSearch(startLimit, endLimit, startDate, endDate, query);
    }

    private void clearPaymentAmount() {

        paymentEditTxtMd.setText(null);

        paymentEditTxtMd.setError(null);
    }

    private void setInfromationForPayment(int postion) {

        purchaseIdTemp = purchasePayableList.get(postion).getId();

        supplierIDTemp = purchasePayableList.get(postion).getSupplierID();

        paymentId.setText("#" + supplierOutstandingManager.getSupplierOutstandingPaymentInvoiceNo());

        supplierName.setText(purchasePayableList.get(postion).getSupplierName());

        Log.e("payment", purchasePayableList.get(postion).getBalance().toString() + " kkk");

        paymentEditTxtMd.setText(POSUtil.doubleToString(purchasePayableList.get(postion).getBalance()));

        paymentEditTxtMd.setError(null);
    }

    private void refreshRecyclerView() {

        Log.w("here", "size;" + purchasePayableList.size());

        if (purchasePayableList.size() < 1) {

            Log.w("here", "no transaction;");

            noTransactionTextView.setVisibility(View.VISIBLE);

            payableRecyclerView.setVisibility(View.GONE);

        } else {

            noTransactionTextView.setVisibility(View.GONE);

            payableRecyclerView.setVisibility(View.VISIBLE);

            adapterForPurchasePayableList.setPurchasePayableList(purchasePayableList);

            adapterForPurchasePayableList.notifyDataSetChanged();
        }

    }

    public void buildAddPaymentDialog() {
        String addPayment = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_payment}).getString(0);

        addPaymentMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.payable_add_payment_md, true).title(addPayment)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                // .positiveText("Save")
                //.negativeText("Cancel")
                .build();

    }

    private void loadSupplierPayable(int startLimit, int endLimit) {

        if (supplier != null) {

            purchasePayableList = supplierOutstandingManager.getSupplierOutstandingListBySupplier(startLimit, endLimit, startDate, endDate, supplier.getId());


        } else {

            purchasePayableList = supplierOutstandingManager.getSupplierOutstandingList(startLimit, endLimit, startDate, endDate);

        }


    }

    public void configRecycler() {

        adapterForPurchasePayableList = new RVAdapterForPurchasePayableList(purchasePayableList);

        payableRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        payableRecyclerView.setAdapter(adapterForPurchasePayableList);

        refreshRecyclerView();

    }

    private void loadUIFromToolbar() {

        searchView = (MaterialSearchView) (this.getActivity()).findViewById(R.id.search_view);

        TypedArray a = getActivity().getTheme().obtainStyledAttributes(new int[]{R.attr.cursor});

        int attributeResourceId = a.getResourceId(0, 0);

        searchView.setCursorDrawable(attributeResourceId);

    }

    private void setFilterTextView(String msg) {

        filterTextView.setText(msg);

    }

    public void loadUI() {

        totalPayableTextView = (TextView) mainLayout.findViewById(R.id.total_payable);

        noTransactionTextView = (TextView) mainLayout.findViewById(R.id.no_transaction);

        searchedResultTxt = (TextView) mainLayout.findViewById(R.id.searched_result_txt);

        payableRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.rv);

        filterTextView = (TextView) mainLayout.findViewById(R.id.filter_one);

        saveBtnMd = (Button) addPaymentMaterialDialog.findViewById(R.id.save);

        cancelBtnMd = (Button) addPaymentMaterialDialog.findViewById(R.id.cancel);

        paymentEditTxtMd = (EditText) addPaymentMaterialDialog.findViewById(R.id.add_payment_et);

        paymentId = (TextView) addPaymentMaterialDialog.findViewById(R.id.payment_id);

        supplierName = (TextView) addPaymentMaterialDialog.findViewById(R.id.supplier_name);

        paymentDateTxtMd = (EditText) addPaymentMaterialDialog.findViewById(R.id.calender_tv);

        calenderBtnMd = (ImageButton) addPaymentMaterialDialog.findViewById(R.id.calender_btn);

        loadUIFromToolbar();

    }

    public void configDateUI() {

        date = DateUtility.makeDate(Integer.toString(paymentYear), Integer.toString(paymentMonth), Integer.toString(paymentDay));

        //String dayDes[]= DateUtility.dayDes(date);

        //String yearMonthDes= DateUtility.monthYearDes(date);

        //paymentDateTxtMd.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

        paymentDateTxtMd.setText(DateUtility.makeDateFormatWithSlash(date));

    }

    public void buildDatePickerDialog() {

        datePickerDialog = DatePickerDialog.newInstance(

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        paymentDay = dayOfMonth;

                        paymentMonth = monthOfYear + 1;

                        paymentYear = year;

                        date = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear), Integer.toString(dayOfMonth));

                        String dayDes[] = DateUtility.dayDes(date);

                        String yearMonthDes = DateUtility.monthYearDes(date);

                        //paymentDateTxtMd.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));

                        paymentDateTxtMd.setText(DateUtility.makeDateFormatWithSlash(date));

                    }
                },

                calendar.get(Calendar.YEAR),

                calendar.get(Calendar.MONTH),

                calendar.get(Calendar.DAY_OF_MONTH)

        );


        if (darkmode)
            datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        datePickerDialog.setThemeDark(darkmode);


    }

    private void buildDateFilterDialog() {
        String filterByDate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.filter_by_date}).getString(0);

        filterDialog = new MaterialDialog.Builder(context).

                title(filterByDate)

                .adapter(rvAdapterForFilter, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

    }

    public void buildingCustomRangeDialog() {

        String dateRange = context.getTheme().obtainStyledAttributes(new int[]{R.attr.date_range}).getString(0);
        //TypedArray cancel=context.getTheme().obtainStyledAttributes(new int[]{R.attr.cancel});
        //TypedArray ok=context.getTheme().obtainStyledAttributes(new int[]{R.attr.ok});

        buildingCustomRangeDatePickerDialog();
        customRangeDialog = new MaterialDialog.Builder(getContext())
                .title(dateRange)
                .customView(R.layout.custome_range, true)
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.negativeText(cancel.getString(0))
                // .positiveText(ok.getString(0))
                .build();

        startDateTextView = (TextView) customRangeDialog.findViewById(R.id.startDate);
        endDateTextView = (TextView) customRangeDialog.findViewById(R.id.endDate);
        Calendar now = Calendar.getInstance();
        customRangeOkBtn = (Button) customRangeDialog.findViewById(R.id.save);
        customRangeCancelBtn = (Button) customRangeDialog.findViewById(R.id.cancel);
        startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1)));
        customStartDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(1));
        Integer endDay = DateUtility.getMonthEndDate(now);
        endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay)));
        customEndDate = DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(endDay));

        startDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = startDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "EndDate");

            }
        });


        endDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                traceDate = endDateTextView;
                startDatePickerDialog.show(getActivity().getFragmentManager(), "StartDate");

            }
        });


        customRangeCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customRangeDialog.dismiss();
            }
        });

    }

    public void buildingCustomRangeDatePickerDialog() {
        Calendar now = Calendar.getInstance();

        startDatePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                 @Override
                                                                 public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                     if (traceDate == startDateTextView) {
                                                                         customStartDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         startDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         startDate = customStartDate;
                                                                     } else {
                                                                         customEndDate = DateUtility.makeDate(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth));
                                                                         endDateTextView.setText(DateUtility.makeDateFormatWithSlash(Integer.toString(year), Integer.toString(monthOfYear + 1), Integer.toString(dayOfMonth)));
                                                                         endDate = customEndDate;
                                                                     }
                                                                 }
                                                             }, now.get(Calendar.YEAR)
                , now.get(Calendar.MONTH)
                , now.get(Calendar.DAY_OF_MONTH));


        startDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });


        if (darkmode)
            startDatePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary_darkmode));
        startDatePickerDialog.setThemeDark(darkmode);
    }

}