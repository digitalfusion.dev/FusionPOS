package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.Discount;
import com.digitalfusion.android.pos.util.AppConstant;

import java.util.List;

/**
 * Created by MD002 on 6/22/17.
 */

public class RVAdapterForDiscountMD extends RecyclerView.Adapter<RVAdapterForDiscountMD.DiscountItemViewHolder> {

    List<Discount> discountList;

    private OnItemClickListener mItemClickListener;

    private RadioButton oldRb;

    private Long id;

    private DiscountItemViewHolder discountItemViewHolder;

    private Discount discount;

    public RVAdapterForDiscountMD(List<Discount> discountList, Long id) {
        this.discountList = discountList;
        this.id = id;
    }

    @Override
    public DiscountItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tax_item_view_in_tax_md, parent, false);

        return new DiscountItemViewHolder(view);
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void onBindViewHolder(final DiscountItemViewHolder holder, final int position) {
        this.discountItemViewHolder = holder;
        holder.discountName.setText(discountList.get(position).getName());
        if (discountList.get(position).getIsPercent() == 0) {
            holder.discountAmt.setText(discountList.get(position).getAmount().toString() + AppConstant.CURRENCY);
        } else {
            holder.discountAmt.setText(discountList.get(position).getAmount().toString() + "%");
        }
        if (discountList.get(position).getId() == id) {
            oldRb = holder.discountRb;
            holder.discountRb.setChecked(true);
        } else {
            holder.discountRb.setChecked(false);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    if (oldRb != null) {
                        oldRb.setChecked(false);
                        oldRb = holder.discountRb;
                        holder.discountRb.setChecked(true);
                    } else {
                        oldRb = holder.discountRb;
                        holder.discountRb.setChecked(true);
                    }
                    discount = discountList.get(position);
                    //                    mItemClickListener.onItemClick(v, position);
                    mItemClickListener.onItemClick();
                }
            }

        });
    }

    public void setNoneChecked() {
        if (oldRb != null) {
            oldRb.setChecked(false);
        } else {
            discountItemViewHolder.discountRb.setChecked(false);
        }
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDefaultChecked() {
        if (oldRb != null) {
            oldRb.setChecked(true);
        } else {
            discountItemViewHolder.discountRb.setChecked(true);
        }
    }

    @Override
    public int getItemCount() {
        return discountList.size();
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public List<Discount> getDiscountVOList() {
        return discountList;
    }

    public void setDiscountVOList(List<Discount> stockViewList) {
        this.discountList = stockViewList;
    }

    public interface OnItemClickListener {
        //        void onItemClick(View view, int position);
        void onItemClick();
    }

    public class DiscountItemViewHolder extends RecyclerView.ViewHolder {
        TextView discountName;
        TextView discountAmt;
        RadioButton discountRb;
        View itemView;

        public DiscountItemViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            discountRb = (RadioButton) itemView.findViewById(R.id.rb);
            discountName = (TextView) itemView.findViewById(R.id.tax_name);
            discountAmt = (TextView) itemView.findViewById(R.id.tax_rate);

        }
    }


}
