package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.Customer;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD003 on 11/4/16.
 */

public class RVSwipeAdapterForCustomerOustanding extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private List<Customer> customerList;
    private ClickListener viewDetailClickListener;
    private ClickListener clickListener;
    private ClickListener viewOustandClickListener;


    public RVSwipeAdapterForCustomerOustanding(List<Customer> customerList) {
        this.customerList = customerList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_outstand_item_view, parent, false);

        return new RVSwipeAdapterForCustomerOustanding.CustomerViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof RVSwipeAdapterForCustomerOustanding.CustomerViewHolder) {
            final RVSwipeAdapterForCustomerOustanding.CustomerViewHolder customerViewHolder = (RVSwipeAdapterForCustomerOustanding.CustomerViewHolder) holder;

            POSUtil.makeZebraStrip(customerViewHolder.itemView, position);

            customerViewHolder.customerNameTextView.setText(customerList.get(position).getName());

            if (customerList.get(position).getBalance().toString().length() > 1) {
                customerViewHolder.customerBalanceTextView.setText(POSUtil.NumberFormat(customerList.get(position).getBalance()));
            } else {
                customerViewHolder.customerBalanceTextView.setText("0.0");
            }
            if (customerList.get(position).getPhoneNo() != null && customerList.get(position).getPhoneNo().length() > 1) {

                customerViewHolder.customerPhoneTextView.setText(customerList.get(position).getPhoneNo());
            } else {
                customerViewHolder.customerPhoneTextView.setText(null);
                customerViewHolder.customerPhoneTextView.setHint("No phone");
            }

            customerViewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewDetailClickListener != null) {
                        customerViewHolder.swipeLayout.close();
                        viewDetailClickListener.onClick(position);
                    }
                }
            });

            customerViewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewOustandClickListener != null) {
                        customerViewHolder.swipeLayout.close();
                        viewOustandClickListener.onClick(position);
                    }
                }
            });

            customerViewHolder.ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (clickListener != null) {
                        clickListener.onClick(position);
                    }

                }
            });

            mItemManger.bindView(customerViewHolder.view, position);

        }

    }

    public ClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public ClickListener getViewOustandClickListener() {

        return viewOustandClickListener;

    }

    public void setViewOustandClickListener(ClickListener viewOustandClickListener) {

        this.viewOustandClickListener = viewOustandClickListener;

    }

    @Override
    public int getItemCount() {
        return customerList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {

        this.customerList = customerList;

    }

    public ClickListener getViewDetailClickListener() {
        return viewDetailClickListener;
    }

    public void setViewDetailClickListener(ClickListener viewDetailClickListener) {

        this.viewDetailClickListener = viewDetailClickListener;

    }

    public class CustomerViewHolder extends RecyclerView.ViewHolder {

        TextView customerNameTextView;

        TextView customerPhoneTextView;

        TextView customerBalanceTextView;

        ImageButton editButton;

        ImageButton deleteButton;

        LinearLayout ll;

        SwipeLayout swipeLayout;

        View view;

        public CustomerViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);

            customerNameTextView = (TextView) itemView.findViewById(R.id.cust_name_in_custome_item_view);

            customerPhoneTextView = (TextView) itemView.findViewById(R.id.cust_phone_in_custome_item_view);

            customerBalanceTextView = (TextView) itemView.findViewById(R.id.cust_balance_in_custome_item_view);

            editButton = (ImageButton) itemView.findViewById(R.id.edit_customer);

            ll = (LinearLayout) itemView.findViewById(R.id.ll);

            deleteButton = (ImageButton) itemView.findViewById(R.id.delete);

        }

    }
}

