package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;

import java.util.List;

/**
 * Created by MD001 on 3/6/17.
 */

public class RVAdapterForReportItemList extends RecyclerView.Adapter<RVAdapterForReportItemList.ReportsListViewHolder> {

    private Drawable reportsImage;

    private List<String> reportTxtList;

    private List<String> reportDescriptionList;

    private OnItemClickListener mItemClickListener;

    public RVAdapterForReportItemList(Drawable reportsImage, List<String> reportTxtList, List<String> reportDescriptionList) {
        this.reportsImage = reportsImage;
        this.reportTxtList = reportTxtList;
        this.reportDescriptionList = reportDescriptionList;
    }

    @Override
    public ReportsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_item_view, parent, false);
        return new ReportsListViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ReportsListViewHolder holder, final int position) {
        holder.image.setBackground(reportsImage);

        holder.reportTxt.setText(reportTxtList.get(position));

        holder.reportDescripiton.setText(reportDescriptionList.get(position));

        holder.reportDescripiton.setVisibility(View.GONE);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(v, position);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return reportTxtList.size();
    }

    public Drawable getReportsImage() {
        return reportsImage;
    }

    public void setReportsImage(Drawable reportsImage) {
        this.reportsImage = reportsImage;
    }

    public List<String> getReportTxtList() {
        return reportTxtList;
    }

    public void setReportTxtList(List<String> reportTxtList) {
        this.reportTxtList = reportTxtList;
    }

    public List<String> getReportDescriptionList() {
        return reportDescriptionList;
    }

    public void setReportDescriptionList(List<String> reportDescriptionList) {
        this.reportDescriptionList = reportDescriptionList;
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ReportsListViewHolder extends RecyclerView.ViewHolder {

        View view;

        ImageView image;

        TextView reportTxt;

        TextView reportDescripiton;

        public ReportsListViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            image = (ImageView) itemView.findViewById(R.id.image);

            reportTxt = (TextView) itemView.findViewById(R.id.name);

            reportDescripiton = (TextView) itemView.findViewById(R.id.description);
        }
    }
}
