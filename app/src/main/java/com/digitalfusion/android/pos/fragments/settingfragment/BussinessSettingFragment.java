package com.digitalfusion.android.pos.fragments.settingfragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.CurrencyAdapter;
import com.digitalfusion.android.pos.adapters.autocompleteadapter.TownShipSearchAdapter;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForBusinessType;
import com.digitalfusion.android.pos.adapters.rvadapter.RVAdapterForFilter;
import com.digitalfusion.android.pos.database.business.CurrencyManager;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.model.Currency;
import com.digitalfusion.android.pos.database.model.BusinessSetting;
import com.digitalfusion.android.pos.database.model.BusinessType;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.GrantPermission;
import com.digitalfusion.android.pos.util.ImagePicker;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.views.FusionToast;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 8/23/16.
 */
public class BussinessSettingFragment extends Fragment {

    private static final int REQUEST_SELECT_PICTURE = 0x01;
    private final String VM_AVG = "AVG";
    private final String VM_LIFO = "LIFO";
    private final String VM_FIFO = "FIFO";
    private View mainLayoutView;
    private SettingManager settingManager;
    private Context context;
    private EditText businessTextInputEditText;
    private EditText userEditText;
    private EditText businessTextTypeEditText;
    private EditText businessPhoneTextInputEditText;
    private EditText businessEmailTextInputEditText;
    private EditText businessWebsiteTextInputEditText;
    private EditText businessStreetTextInputEditText;
    private EditText businessCityTextInputEditText;
    private EditText businessStateTextInputEditText;
    private AutoCompleteTextView businessTownshipTextInputEditText;
    private TextView valuationMethodTextView;
    private TextView currencyTextView;
    private LinearLayout currencyLinearLayout;
    private ImageView imageButton;
    //values
    private String businessName;
    private String businessType;
    private String userName;
    private String businessPhone;
    private String businessEmail;
    private String businessWebsite;
    private String businessStreet;
    private String businessState;
    private String businessCity;
    private String businessTownship;
    private byte[] logo;
    private BusinessSetting businessSetting;
    private String currencySign;
    private LinearLayout valuationMethodLinearLayout;
    private MaterialDialog valuationMaterialDialog;
    private MaterialDialog currencyMaterialDialog;
    private RVAdapterForFilter valuationListAdapter;
    private boolean flag = false;
    private List<String> valuationMethodList;
    private String valuationMethod = "";
    private List<Currency> currencyList;
    private CurrencyAdapter currencyAdapter;
    private String currency = "MMK";
    private Long currencyId = 0l;
    private int currentCurrencyPos;
    private int pos;
    private GrantPermission grantPermission;
    private CurrencyManager currencyManager;
    private List<BusinessType> businessTypeList;
    private RVAdapterForBusinessType rvAdapterForBusinessType;
    private MaterialDialog businessTypeMaterialDialog;
    private List<String> townShipList;
    private List<String> stateList;
    private MaterialDialog stateMaterialDialog;
    private RVAdapterForFilter rvAdapterForstateList;
    private TownShipSearchAdapter townShipSearchAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.business_setting, null);

        context = getContext();

        currencyManager = new CurrencyManager(context);

        stateList = new ArrayList<>();
        stateList.add("ကခ်င္");
        stateList.add("ကယား");
        stateList.add("ကရင္");
        stateList.add("ခ်င္း");
        stateList.add("မြန္");
        stateList.add("ရခိုင္");
        stateList.add("ရွမ္း");
        stateList.add("စစ္ကိုင္း");
        stateList.add("တနသၤာရီ");
        stateList.add("ပဲခူး");
        stateList.add("မေကြး");
        stateList.add("ရန္ကုန္");
        stateList.add("ဧရာဝတီ");
        stateList.add("မႏၲေလး");
        stateList.add("ေနျပည္ေတာ္");

        townShipList = new ArrayList<>();
        townShipList.add("ေထာက္ၾကန္႔");
        townShipList.add("ေမွာ္ဘီ");
        townShipList.add("လွည္းကူး");
        townShipList.add("တိုက္ႀကီး");
        townShipList.add("အုတ္ကန္");
        townShipList.add("ထန္းတပင္");
        townShipList.add("ေရႊျပည္သာ");
        townShipList.add("လိွုင္သာယာ");
        townShipList.add("ေတာင္ဒဂံု");
        townShipList.add("ေျမာင္ဒဂံု;");
        townShipList.add("သန္လ်င္");
        townShipList.add("ေက်ာက္တန္း");
        townShipList.add("သံုးခြ");
        townShipList.add("ခရမ္း");
        townShipList.add("တြံေတး");
        townShipList.add("ေကာ့မႉး");
        townShipList.add("ကြမ္းျခံကုန္း");
        townShipList.add("ကိုကိုးကၽြန္း");
        townShipList.add("အင္းစိန္");
        townShipList.add("သာေကတ");
        townShipList.add("သကၤန္းကြ်န္း");
        townShipList.add("တာေမြ");
        townShipList.add("ဗိုလ္တေထာင္");
        townShipList.add("အလံု");
        townShipList.add("ဗဟန္း");
        townShipList.add("ဒဂံုဆိပ္ကမ္း");
        townShipList.add("ဒဂံု");
        townShipList.add("ဒလ");
        townShipList.add("ေဒါပံု");
        townShipList.add("လိွုင္");
        townShipList.add("အင္းစိန္");
        townShipList.add("ကမာရြတ္");
        townShipList.add("လမ္းမေတာ္");
        townShipList.add("လသာ");
        townShipList.add("မရမ္းကုန္း");
        townShipList.add("မဂၤလာေတာင္ညြန္႔");
        townShipList.add("မဂၤလာဒံု");
        townShipList.add("ပန္းပဲတန္း");
        townShipList.add("ပုဇြန္ေတာင္");
        townShipList.add("စမ္းေခ်ာင္း");
        townShipList.add("ဆိပ္ကမ္း");
        townShipList.add("ဆိပ္ႀကီးခေနာင္တုိ");
        townShipList.add("ေတာင္ဥကၠလာ");
        townShipList.add("ေျမာက္ဥကၠလာ");
        townShipList.add("အေရွ႕ဒဂံု");
        townShipList.add("ရန္ကင္း");
        townShipList.add("ေအာင္ေျမသာစံ");
        townShipList.add("ခ်မ္းေအးသာစံ");
        townShipList.add("ခ်မ္းျမသာစည္");
        townShipList.add("ေက်ာက္ပန္းေတာင္း");
        townShipList.add("ေက်ာက္ဆည္");
        townShipList.add("မဟာေအာင္ေျမ");
        townShipList.add("မလုိုင္");
        townShipList.add("မိတၳီလာ");
        townShipList.add("မုိးကုတ္");
        townShipList.add("ျမင္းျခံ");
        townShipList.add("ျမစ္သား");
        townShipList.add("နထိုးႀကီး");
        townShipList.add("ငဇြန္");
        townShipList.add("ေညာင္ဦး");
        townShipList.add("ပုသိမ္ႀကီး");
        townShipList.add("ေပ်ာ္ဘြယ္");
        townShipList.add("ျပည္ႀကီးတံခြန္");
        townShipList.add("ျပင္ဦးလြင္");
        townShipList.add("စဥ့္ကူး");
        townShipList.add("စဥ့္ကိုင္");
        townShipList.add("တံတားဦး");
        townShipList.add("ေတာင္သာ");
        townShipList.add("သပိတ္က်င္း");
        townShipList.add("သာစည္");
        townShipList.add("ဝမ္းတြင္း");
        townShipList.add("ရမည္းသင္း");
        townShipList.add("ငါန္းဇြန္");
        townShipList.add("ဒဂံုၿမိဳ႕သစ္အေရွ႕ပိုင္း");
        townShipList.add("ဒဂံုၿမိဳ႕သစ္ေတာင္ပိုင္း");
        townShipList.add("ဒဂံုၿမိဳ႕သစ္ေျမာက္ပိုင္း");

        businessTypeList = new ArrayList<>();
        businessTypeList.add(new BusinessType("အလွကုန္", true));
        businessTypeList.add(new BusinessType("လူသံုးကုန္"));
        businessTypeList.add(new BusinessType("စားေသာက္ကုန္"));
        businessTypeList.add(new BusinessType("စာေရးကိရိယာ"));
        businessTypeList.add(new BusinessType("အဝတ္အထည္"));
        businessTypeList.add(new BusinessType("အထည္အလိပ္"));
        businessTypeList.add(new BusinessType("ပရိေဘာဂ"));
        businessTypeList.add(new BusinessType("အိမ္ေဆာက္ပစၥည္း/ကုန္မာ"));
        businessTypeList.add(new BusinessType("ကေလးပစၥည္း"));
        businessTypeList.add(new BusinessType("ကုန္ေျခာက္"));
        businessTypeList.add(new BusinessType("ေရပိုက္ႏွင့္ဆက္စပ္ပစၥည္း"));
        businessTypeList.add(new BusinessType("လၽွပ္စစ္ပစၥည္း"));
        businessTypeList.add(new BusinessType("ဖန္စီပစၥည္း"));
        businessTypeList.add(new BusinessType("ေရသန္႔"));
        businessTypeList.add(new BusinessType("စာေရးကိရိယာ"));
        businessTypeList.add(new BusinessType("ကြန္ပ်ဴတာႏွင့္ဆက္စပ္ပစၥည္း"));
        businessTypeList.add(new BusinessType("Mobile ဖုန္းဆိုင္"));
        businessTypeList.add(new BusinessType("ဖိိနပ္ဆိုင္"));
        businessTypeList.add(new BusinessType("Other"));

        rvAdapterForBusinessType = new RVAdapterForBusinessType(businessTypeList);
        townShipSearchAdapter = new TownShipSearchAdapter(getContext(), townShipList);
        String choose = context.getTheme().obtainStyledAttributes(new int[]{R.attr.choose_business_type}).getString(0);
        businessTypeMaterialDialog = new MaterialDialog.Builder(getContext()).
                title(choose)
                .adapter(rvAdapterForBusinessType, new LinearLayoutManager(getContext()))
                .positiveText(Html.fromHtml("<font color='#424242'>Ok</font>"))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        String types = "";
                        for (BusinessType businessType : businessTypeList) {
                            if (businessType.isSelected()) {
                                types += businessType.getName() + ", ";
                            }
                        }
                        if (types != "") {
                            types = types.substring(0, types.length() - 2);
                        }
                        businessTextTypeEditText.setText(types);
                    }
                })
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();

        rvAdapterForstateList = new RVAdapterForFilter(stateList);
        stateMaterialDialog = new MaterialDialog.Builder(context)
                .adapter(rvAdapterForstateList, new LinearLayoutManager(context)).build();

        String businessSetting = context.getTheme().obtainStyledAttributes(new int[]{R.attr.business_setting}).getString(0);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(businessSetting);

        currencyList = new ArrayList<>();

        grantPermission = new GrantPermission(this);

        MainActivity.setCurrentFragment(this);

        Currency myanmarKyat = new Currency("Myanmar Kyat( MMK )", "MMK");

        Currency dollar = new Currency("US Dollar ( $ )", "$");


        currencyList = currencyManager.getAllCurrencies(0, 100);

        currencyAdapter = new CurrencyAdapter(currencyList);

        valuationMethod = VM_AVG;

        setHasOptionsMenu(true);

        settingManager = new SettingManager(context);

        this.businessSetting = settingManager.getBusinessSetting();

        valuationMethodList = new ArrayList<>();

        imageButton = (ImageView) mainLayoutView.findViewById(R.id.img);

        valuationMethodList.add(VM_AVG);

        valuationMethodList.add(VM_FIFO);

        valuationMethodList.add(VM_LIFO);

        valuationListAdapter = new RVAdapterForFilter(valuationMethodList);

        if (this.businessSetting.getBusinessName() != null && this.businessSetting.getBusinessName().trim().length() > 0) {

            flag = true;

            logo = this.businessSetting.getLogo();

            currency = this.businessSetting.getCurrency();

            valuationMethod = this.businessSetting.getValuationMethod();

            currencySign = this.businessSetting.getSign();

            if (this.businessSetting.getLogo() != null) {
                imageButton.setImageBitmap(POSUtil.getBitmapFromByteArray(this.businessSetting.getLogo()));
            }


        }


        buildCurrencyDialog();

        buildValuationDialog();

        loadUI();

        configUI();

        businessTownshipTextInputEditText.setAdapter(townShipSearchAdapter);
        businessStateTextInputEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stateMaterialDialog.show();
            }
        });
        rvAdapterForstateList.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                businessStateTextInputEditText.setText(stateList.get(position));
                stateMaterialDialog.dismiss();
            }
        });
        businessTextTypeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                businessTypeMaterialDialog.show();
            }
        });

        valuationMethodTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valuationMaterialDialog.show();
            }
        });

        valuationListAdapter.setmItemClickListener(new RVAdapterForFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                valuationMethod = valuationMethodList.get(position);

                valuationMethodTextView.setText(valuationMethod);

                valuationMaterialDialog.dismiss();

            }
        });

        pos = 0;

        for (String s : valuationMethodList) {
            if (s.equalsIgnoreCase(this.businessSetting.getValuationMethod())) {

                String temp = valuationMethodList.get(0);

                valuationMethodList.set(0, s);

                valuationMethodList.set(pos, temp);

            } else {
                pos++;
            }
        }

        for (Currency c : currencyList) {

            if (c.isActive()) {
                currentCurrencyPos = currencyList.indexOf(c);
                currencyId = c.getId();

            }

        }

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (grantPermission.requestPermissions()) {
                    onPickImage();
                }

            }
        });


        currencyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currencyMaterialDialog.show();

            }
        });

        currencyAdapter.setmItemClickListener(new CurrencyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                currencyMaterialDialog.dismiss();

                currencyTextView.setText(currencyList.get(position).getDisplayName() + " (" + currencyList.get(position).getSign() + ")");

                currency = currencyList.get(position).getDisplayName();

                currencySign = currencyList.get(position).getSign();

                currencyId = currencyList.get(position).getId();
            }
        });

        imageButton.setImageBitmap(POSUtil.getBitmapFromByteArray(this.businessSetting.getLogo()));

        currencyAdapter.setCurrentPos(currentCurrencyPos);

        return mainLayoutView;

    }

    private void saveOrUpdateBusinessSetting() {

        if (checkValidation()) {

            gatherDataFromView();

            boolean success = settingManager.updateBusinessSetting(businessName, userName, businessType, businessPhone, businessEmail, businessWebsite, businessStreet,
                    valuationMethod, currency, logo, businessTownship, businessCity, businessState);


            if (success) {

                if (currencyId != null) {

                    CurrencyManager currencyManager = new CurrencyManager(context);

                    currencyManager.updateDefault(1, currencyId);

                }

                saveOnSharedPreference();

                AppConstant.CURRENCY = currencySign;

                String successUpdate = context.getTheme().obtainStyledAttributes(new int[]{R.attr.successfully_updated}).getString(0);

                // POSUtil.showSnackBar(mainLayoutView, successUpdate);
                FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
            }

        }
    }

    private void saveOnSharedPreference() {
        SharedPreferences sharedPref  = getActivity().getPreferences(Context.MODE_PRIVATE);
        String            headerTxt   = context.getTheme().obtainStyledAttributes(new int[]{R.attr.header_text}).getString(0);
        String            temp_header = sharedPref.getString("header", headerTxt);
        if (temp_header == "" || temp_header == null || temp_header.trim().length() <= 0 || temp_header.equalsIgnoreCase(headerTxt)) {
            String temp = "";
            if ((businessName != null || businessName != "") && businessName.trim().length() > 0) {
                temp = businessName;
            }
            BusinessSetting businessSetting = new BusinessSetting(businessState, businessCity, businessTownship, businessStreet);
            if ((businessSetting.getAddress() != null || businessSetting.getAddress() != "") && businessSetting.getAddress().trim().length() > 0) {
                if (temp.trim().length() <= 0)
                    temp = businessSetting.getAddress();
                else
                    temp += "\n" + businessSetting.getAddress();
            }
            if (!(businessPhone.equalsIgnoreCase(null) || businessPhone.equalsIgnoreCase("")) && businessPhone.trim().length() > 0) {
                if (temp.trim().length() <= 0)
                    temp = businessPhone;
                else
                    temp += "\n" + businessPhone;
            }

            if (!(temp.equalsIgnoreCase(null) || temp.equalsIgnoreCase(""))) {
                temp_header = temp;
            }

            SharedPreferences.Editor editor = sharedPref.edit();

            if (temp_header.trim().length() <= 0)
                editor.putString("header", headerTxt);
            else
                editor.putString("header", temp_header);
            editor.commit();
        }

    }

    public void onPickImage() {
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(context);
        startActivityForResult(chooseImageIntent, REQUEST_SELECT_PICTURE);
    }

    private void buildValuationDialog() {

        String choose = context.getTheme().obtainStyledAttributes(new int[]{R.attr.choose_valuation_method}).getString(0);
        valuationMaterialDialog = new MaterialDialog.Builder(context).

                title(choose)

                .adapter(valuationListAdapter, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    private void buildCurrencyDialog() {

        String currency = context.getTheme().obtainStyledAttributes(new int[]{R.attr.currency}).getString(0);
        currencyMaterialDialog = new MaterialDialog.Builder(context).

                title(currency)

                .adapter(currencyAdapter, new LinearLayoutManager(context))
                .typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                .build();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save) {

            saveOrUpdateBusinessSetting();

        }

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);

       /* TypedArray save=context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        FontTextView fontTextView= (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.menu_text,null);

        menu.getItem(0).setActionView(fontTextView);

        fontTextView.setText(save.getString(0));


        fontTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menu.getItem(0));
            }
        });*/


        super.onCreateOptionsMenu(menu, inflater);
    }


    public void gatherDataFromView() {

        businessName = businessTextInputEditText.getText().toString().trim();
        businessType = businessTextTypeEditText.getText().toString().trim();
        userName = userEditText.getText().toString().trim();
        businessPhone = businessPhoneTextInputEditText.getText().toString().trim();

        businessEmail = businessEmailTextInputEditText.getText().toString().trim();

        businessWebsite = businessWebsiteTextInputEditText.getText().toString().trim();

        businessStreet = businessStreetTextInputEditText.getText().toString().trim();

        businessCity = businessCityTextInputEditText.getText().toString().trim();

        businessState = businessStateTextInputEditText.getText().toString().trim();

        businessTownship = businessTownshipTextInputEditText.getText().toString().trim();

    }

    private boolean checkValidation() {

        boolean status = true;

        if (businessTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;

            String enterBN = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_business_name}).getString(0);
            businessTextInputEditText.setError(enterBN);

        }

        return status;

    }

    public void configUI() {

        if (flag) {

            businessTextTypeEditText.setText(businessSetting.getBusinessType());

            userEditText.setText(businessSetting.getUserName());

            businessTextInputEditText.setText(businessSetting.getBusinessName());

            businessPhoneTextInputEditText.setText(businessSetting.getPhoneNo());

            businessEmailTextInputEditText.setText(businessSetting.getEmail());

            businessWebsiteTextInputEditText.setText(businessSetting.getWebsite());

            businessStreetTextInputEditText.setText(businessSetting.getStreet());

            businessStateTextInputEditText.setText(businessSetting.getState());

            businessCityTextInputEditText.setText(businessSetting.getCity());

            businessTownshipTextInputEditText.setText(businessSetting.getTownship());

            valuationMethodTextView.setText(businessSetting.getValuationMethod());

            currencyTextView.setText(businessSetting.getCurrency());

        }


    }

    public void loadUI() {

        // valuationMethodLinearLayout=(LinearLayout)mainLayoutView.findViewById(R.id.val_method_ll);

        businessTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.business_name_edit_in_business_setting_TIET);
        businessTextTypeEditText = (EditText) mainLayoutView.findViewById(R.id.businees_type);

        userEditText = (EditText) mainLayoutView.findViewById(R.id.user_name);

        businessPhoneTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.business_phone_edit_in_business_setting_TIET);

        businessEmailTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.business_email_edit_in_business_setting_TIET);

        businessWebsiteTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.business_website_edit_in_business_setting_TIET);

        businessStreetTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.business_street_edit_in_business_setting_TIET);

        businessCityTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.business_city_edit_in_business_setting_TIET);

        businessStateTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.business_state_edit_in_business_setting_TIET);

        businessTownshipTextInputEditText = mainLayoutView.findViewById(R.id.business_township_edit_in_business_setting_TIET);

        valuationMethodTextView = (TextView) mainLayoutView.findViewById(R.id.val_method_tv);

        currencyTextView = (TextView) mainLayoutView.findViewById(R.id.currency_tv);

        //  currencyLinearLayout=(LinearLayout)mainLayoutView.findViewById(R.id.currency_ll);


    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = "temp.png";

        TypedArray allTrans      = context.getTheme().obtainStyledAttributes(new int[]{R.attr.pickerAccentColor});
        TypedArray allTran2      = context.getTheme().obtainStyledAttributes(new int[]{R.attr.colorPrimaryDark});
        int        valueInPixels = (int) getResources().getDimension(R.dimen.image_stock_detail);

        UCrop.Options options = new UCrop.Options();
        options.setToolbarColor(allTrans.getColor(0, 0));
        options.setStatusBarColor(allTran2.getColor(0, 0));
        options.setActiveWidgetColor(allTrans.getColor(0, 0));

        Log.w("crop ", uri.toString());

        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(context.getCacheDir(), destinationFileName)))
                .withOptions(options)
                .withAspectRatio(1, 1)
                .withMaxResultSize(valueInPixels, valueInPixels);


        uCrop.start(context, this);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        Log.w("resutl code", resultCode + " SS");

        //     Log.w("crop code",UCrop.REQUEST_CROP+" SS");

        if (resultCode == Activity.RESULT_OK) {

            switch (requestCode) {
                case REQUEST_SELECT_PICTURE:

                    final Uri selectedUri = ImagePicker.getUriFromResult(context, resultCode, data);
                    Log.w("uri", selectedUri.toString());
                    if (selectedUri != null) {
                        startCropActivity(selectedUri);
                    } else {
                        String cannotRetrieve = context.getTheme().obtainStyledAttributes(new int[]{R.attr.cannot_retrieve_selected_image}).getString(0);
                        Toast.makeText(context, cannotRetrieve, Toast.LENGTH_SHORT).show();
                    }


                    break;

                case UCrop.REQUEST_CROP:


                    final Uri resultUri = UCrop.getOutput(data);


                    BitmapFactory.Options options = new BitmapFactory.Options();

                    AssetFileDescriptor fileDescriptor = null;
                    try {
                        fileDescriptor = context.getContentResolver().openAssetFileDescriptor(resultUri, "r");
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }


                    Bitmap actuallyUsableBitmap = BitmapFactory.decodeFileDescriptor(
                            fileDescriptor.getFileDescriptor(), null, options);


                    // Bitmap bitmap = ImagePicker.getCropImage(data,resultCode);
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), actuallyUsableBitmap);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    actuallyUsableBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
                    logo = bos.toByteArray();

                    imageButton.setImageDrawable(bitmapDrawable);

                default:


                    super.onActivityResult(requestCode, resultCode, data);
                    break;
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }


    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("ERROR", "handleCropError: ", cropError);
            Toast.makeText(context, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "I Don't know this error", Toast.LENGTH_SHORT).show();
        }
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //recreate();
            //reload my activity with requestPermissions granted or use the features what required the requestPermissions
            //requestPermissions(backingUp);

            onPickImage();


        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(getContext())
                    .setTitle(Html.fromHtml("<font color='#424242'>Inform and request</font>"))
                    .setMessage(Html.fromHtml("<font color='#424242'>Please enable storage requestPermissions to perform backup and restore functions!</font>"))
                    .setPositiveButton(Html.fromHtml("<font color='#424242'>Ok</font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.requestPermissions.WRITE_EXTERNAL_STORAGE}, RC_PERMISSION_WRITE_EXTERNAL_STORAGE);
                            //   ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.requestPermissions.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_RC);
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 200);
                        }
                    })
                    .show();
            Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            nbutton.setTextColor(Color.parseColor("#424242"));
        } else {
            AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(getContext())
                    .setTitle(Html.fromHtml("<font color='#424242'>Inform and request</font>"))
                    .setMessage(Html.fromHtml("<font color='#424242'>Please enable storage requestPermissions from settings to get access to the storage!</font>"))
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 200);
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        }
                    })
                    .show();
            Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            nbutton.setTextColor(Color.parseColor("#424242"));
            //}
        }
    }


    private void performCrop(Uri contentUri) {
        try {
            //Start Crop Activity

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("scale", "false");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 0);
            // indicate output X and Y

            cropIntent.putExtra("outputX", 280);
            cropIntent.putExtra("outputY", 280);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, 400);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = context.getTheme().obtainStyledAttributes(new int[]{R.attr.your_device_doesnot_support_the_crop_action}).getString(0);
            Toast  toast        = Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

}
