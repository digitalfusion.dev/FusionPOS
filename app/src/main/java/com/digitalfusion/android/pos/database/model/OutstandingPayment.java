package com.digitalfusion.android.pos.database.model;

/**
 * Created by MD002 on 10/25/16.
 */

public class OutstandingPayment {
    private Long id;
    private String invoiceNo;
    private Double paidAmt;
    private String day;
    private String month;
    private String year;
    private String date;

    public OutstandingPayment() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Double getPaidAmt() {
        return paidAmt;
    }

    public void setPaidAmt(Double paidAmt) {
        this.paidAmt = paidAmt;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isNull() {
        return id == null || invoiceNo == null || paidAmt == null || date == null;
    }

}
