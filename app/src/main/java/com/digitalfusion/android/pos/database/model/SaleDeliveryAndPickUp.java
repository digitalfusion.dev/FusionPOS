package com.digitalfusion.android.pos.database.model;

/**
 * Created by MD002 on 9/7/16.
 */
public class SaleDeliveryAndPickUp {
    private Long saleID;
    private String saleVoucherNo;
    private String saleDate;
    private String type;
    private Long pickupOrDeliveryID;
    private String pickupOrDeliveryDate;
    private int pickupOrDeliveryStatus;
    private Long customerID;
    private String customerName;
    private String pickupOrDeliveryDay;
    private String pickupOrDeliverymonth;
    private String pickupOrDeliveryYear;
    private String saleDay;
    private String saleMonth;
    private String saleYear;

    public SaleDeliveryAndPickUp() {
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Long getSaleID() {

        return saleID;
    }

    public void setSaleID(Long saleID) {
        this.saleID = saleID;
    }

    public String getSaleVoucherNo() {
        return saleVoucherNo;
    }

    public void setSaleVoucherNo(String saleVoucherNo) {
        this.saleVoucherNo = saleVoucherNo;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getPickupOrDeliveryID() {
        return pickupOrDeliveryID;
    }

    public void setPickupOrDeliveryID(Long pickupOrDeliveryID) {
        this.pickupOrDeliveryID = pickupOrDeliveryID;
    }

    public String getPickupOrDeliveryDate() {
        return pickupOrDeliveryDate;
    }

    public void setPickupOrDeliveryDate(String pickupOrDeliveryDate) {
        this.pickupOrDeliveryDate = pickupOrDeliveryDate;
    }

    public int getPickupOrDeliveryStatus() {
        return pickupOrDeliveryStatus;
    }

    public void setPickupOrDeliveryStatus(int pickupOrDeliveryStatus) {
        this.pickupOrDeliveryStatus = pickupOrDeliveryStatus;
    }

    public String getPickupOrDeliveryDay() {
        return pickupOrDeliveryDay;
    }

    public void setPickupOrDeliveryDay(String pickupOrDeliveryDay) {
        this.pickupOrDeliveryDay = pickupOrDeliveryDay;
    }

    public String getPickupOrDeliverymonth() {
        return pickupOrDeliverymonth;
    }

    public void setPickupOrDeliverymonth(String pickupOrDeliverymonth) {
        this.pickupOrDeliverymonth = pickupOrDeliverymonth;
    }

    public String getPickupOrDeliveryYear() {
        return pickupOrDeliveryYear;
    }

    public void setPickupOrDeliveryYear(String pickupOrDeliveryYear) {
        this.pickupOrDeliveryYear = pickupOrDeliveryYear;
    }

    public String getSaleDay() {
        return saleDay;
    }

    public void setSaleDay(String saleDay) {
        this.saleDay = saleDay;
    }

    public String getSaleMonth() {
        return saleMonth;
    }

    public void setSaleMonth(String saleMonth) {
        this.saleMonth = saleMonth;
    }

    public String getSaleYear() {
        return saleYear;
    }

    public void setSaleYear(String saleYear) {
        this.saleYear = saleYear;
    }
}
