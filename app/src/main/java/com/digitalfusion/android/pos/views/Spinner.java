package com.digitalfusion.android.pos.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.widget.ImageView;

import com.digitalfusion.android.pos.R;
import com.kaopiz.kprogresshud.KProgressHUD;

/**
 * Created by M-001 on 3/15/18.
 */

public class Spinner {
    private KProgressHUD hud;

    public Spinner(Context context) {
        View processDialog = View.inflate(context, R.layout.process_dialog_custom_layout, null);
        ImageView imageView = processDialog.findViewById(R.id.image_view);

        imageView.setBackgroundResource(R.drawable.spin_animation);
        AnimationDrawable drawable = (AnimationDrawable) imageView.getBackground();
        drawable.start();
        hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCustomView(processDialog)
                .setDetailsLabel("")
                .setBackgroundColor(Color.parseColor("#66000000"))
                .setAnimationSpeed(1)
                .setCancellable(true);
    }

    public void show( ) {
        dismiss();
        hud.show();
    }

    public void dismiss() {
        if (hud.isShowing()) {
            hud.dismiss();
        }
    }
}
