package com.digitalfusion.android.pos.database.model;

import com.digitalfusion.android.pos.util.POSUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD002 on 8/18/16.
 */
public class LostItem implements Serializable {
    private Long id;
    //private String invoiceNo;
    private String stockName;
    private int stockQty;
    private String date;
    //private String approveBy;
    private Double totalValue;
    private String remark;
    private Long userID;
    private String userName;
    private String role;
    private String day;
    private String month;
    private String year;
    private String stockCodeNo;
    private Double value;
    private String categoryName;

    private Long stockID;

    private Long unitID;
    private String stockUnit;
    private List<StockValue> stockValueList;

    public LostItem() {
        stockValueList = new ArrayList<>();
    }

    public String getStockUnit() {
        return stockUnit;
    }

    public void setStockUnit(String stockUnit) {
        this.stockUnit = stockUnit;
    }

    public Long getUnitID() {
        return unitID;
    }

    public void setUnitID(Long unitID) {
        this.unitID = unitID;
    }

    public String getStockCodeNo() {
        return stockCodeNo;
    }

    public void setStockCodeNo(String stockCodeNo) {
        this.stockCodeNo = stockCodeNo;
    }

    public Long getStockID() {
        return stockID;
    }

    public void setStockID(Long stockID) {
        this.stockID = stockID;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /*public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }*/

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public int getStockQty() {
        return stockQty;
    }

    public void setStockQty(int stockQty) {
        this.stockQty = stockQty;

        if (totalValue != null & value != null) {

            totalValue = stockQty * value;

        }
    }

    public String getStockQtyString() {
        return POSUtil.NumberFormat(stockQty);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    /*public String getApproveBy() {
        return approveBy;
    }

    public void setApproveBy(String approveBy) {
        this.approveBy = approveBy;
    }*/

    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
        if (totalValue != null & value != null) {

            totalValue = stockQty * value;

        }
    }

    public List<StockValue> getStockValueList() {
        return stockValueList;
    }

    public void setStockValueList(List<StockValue> stockValueList) {
        this.stockValueList = stockValueList;
    }

    public String getTotalValueString() {

        Double total = 0.0;

        for (StockValue stockValue : stockValueList) {

            total += stockValue.getValue() * stockValue.getQty();

        }

        return POSUtil.NumberFormat(total);
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
