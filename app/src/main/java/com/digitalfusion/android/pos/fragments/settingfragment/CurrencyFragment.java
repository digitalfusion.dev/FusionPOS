package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVSwipeAdapterForCurrencyList;
import com.digitalfusion.android.pos.database.business.CurrencyManager;
import com.digitalfusion.android.pos.database.model.Currency;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.views.FusionToast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MD003 on 8/24/16.
 */
public class CurrencyFragment extends Fragment {

    private View mainLayoutView;

    private RecyclerView recyclerView;

    private FloatingActionButton addCurrencyFab;

    //    private SettingManager settingBusiness;

    //Add Currency dialog
    private MaterialDialog currencyMaterialDialog;

    private MDButton addCurrencyMdButton;

    private EditText currencyNameTextInputEditText;

    private EditText currencySymbolTextInputEditText;

    private CheckBox useDefaultCheckBox;


    //Edit Currency dialog
    private MaterialDialog editCurrencyMaterialDialog;

    private MDButton editCurrencyMdButton;

    private EditText editCurrencyNameTextInputEditText;

    private EditText editCurrencySymbolTextInputEditText;

    private CheckBox editUseDefaultCheckBox;

    //For Currency list
    private RVSwipeAdapterForCurrencyList rvSwipeAdapterForCurrencyList;

    private List<Currency> currencyList;

    //For value
    private String currencyName;

    private String currencySymbol;


    //For edit value
    private int editPosition;

    private String editCurrencyName;

    private String editCurrencySymbol;

    private int editUseDefault;

    private Context context;

    private CurrencyManager currencyManager;

    private int useDefault;

    private MaterialDialog deleteAlertDialog;

    private Button yesDeleteMdButton;

    private int deletePos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.currency, null);

        context = getContext();

        currencyManager = new CurrencyManager(context);

        //        settingBusiness = new SettingManager(context);

        String currency = context.getTheme().obtainStyledAttributes(new int[]{R.attr.currency}).getString(0);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(currency);

        buildingEditCurrencyDialog();

        buildingAddCurrencyDialog();

        loadUI();

        configUI();

        MainActivity.setCurrentFragment(this);

        addCurrencyFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //clearInput();

                //currencyMaterialDialog.show();

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_CURRENCY);

                startActivity(addCurrencyIntent);


            }
        });

        addCurrencyMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // settingBusiness.addNewTax();

                if (checkValidation()) {

                    takeValueFromView();

                    //  currencyList.get(editPosition).setDisplayName(currencyName);

                    //  currencyList.get(editPosition).setSign(currencySymbol);

                    //   currencyList.get(editPosition).setActive(useDefault);

                    //settingBusiness.addNewTax(taxName,taxRate,taxType,taxDesc,useDefault);

                    currencyManager.addNewCurrency(currencyName, currencySymbol, useDefault);

                    currencyMaterialDialog.dismiss();

                    refreshCurrencyList();

                }

            }
        });

        rvSwipeAdapterForCurrencyList.setEditClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {

                //    editPosition=postion;

                //   editCurrencyNameTextInputEditText.setText(currencyList.get(postion).getDisplayName());

                //  editCurrencySymbolTextInputEditText.setText(currencyList.get(postion).getSign());

                //  if(currencyList.get(postion).isActive()){

                //     editUseDefaultCheckBox.setChecked(true);

                // }else {

                //      editUseDefaultCheckBox.setChecked(false);

                //  }

                // editCurrencyMaterialDialog.show();

                Bundle bundle = new Bundle();

                bundle.putSerializable(AddEditCurrencyFragment.KEY, currencyList.get(postion));

                Intent addCurrencyIntent = new Intent(context, DetailActivity.class);

                addCurrencyIntent.putExtras(bundle);

                addCurrencyIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.ADD_EDIT_CURRENCY);

                startActivity(addCurrencyIntent);

            }
        });

        useDefaultCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    useDefault = 1;
                } else {
                    useDefault = 0;
                }
            }
        });

        editCurrencyMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkEditValidation()) {

                    takeValueFromEditView();

                    currencyList.get(editPosition).setDisplayName(editCurrencyName);

                    currencyList.get(editPosition).setSign(editCurrencySymbol);

                    currencyList.get(editPosition).setActive(editUseDefault);

                    currencyManager.updateCurrency(editCurrencyName, editCurrencySymbol, editUseDefault, currencyList.get(editPosition).getId());

                    AppConstant.CURRENCY = editCurrencySymbol;

                    //settingBusiness.updateTax(editTaxName,editTaxRate,editTaxType,editTaxDesc,editUseDefault,taxVOList.get(editPosition).getId());

                    refreshCurrencyList();

                    editCurrencyMaterialDialog.dismiss();
                    FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);


                }

            }
        });

        editUseDefaultCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editUseDefault = 1;
                } else {
                    editUseDefault = 0;
                }
            }
        });

        refreshCurrencyList();

        buildDeleteAlertDialog();

        rvSwipeAdapterForCurrencyList.setDeleteClickListener(new ClickListener() {
            @Override
            public void onClick(int postion) {
                deletePos = postion;
                deleteAlertDialog.show();
            }
        });

        yesDeleteMdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currencyManager.deleteCurrency(currencyList.get(deletePos).getId())) {
                    if (currencyList.get(deletePos).getStatus() == 1) {
                        currencyManager.updateDefault(1, 1l);
                    }

                    //                    currencyList = currencyManager.getAllCurrencies(0, 50);
                    //                    currencyList.remove(deletePos);
                    //
                    //                    rvSwipeAdapterForCurrencyList.setCurrencyList(currencyList);
                    //
                    //                    rvSwipeAdapterForCurrencyList.notifyItemRemoved(deletePos);
                    //
                    //                    rvSwipeAdapterForCurrencyList.notifyItemRangeChanged(deletePos, currencyList.size());
                    refreshCurrencyList();

                    deleteAlertDialog.dismiss();
                }

            }
        });

        return mainLayoutView;
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.w("here ", "on resume");

        refreshCurrencyList();
    }

    private boolean checkValidation() {

        boolean status = true;

        if (currencyNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;
            String currencyName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_currency_name}).getString(0);

            currencyNameTextInputEditText.setError(currencyName);

        }

        if (currencySymbolTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;
            String currencySymbol = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_currency_symbol}).getString(0);

            currencySymbolTextInputEditText.setError(currencySymbol);

        }

        return status;

    }

    private boolean checkEditValidation() {

        boolean status = true;

        if (editCurrencyNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;

            String currencyName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_currency_name}).getString(0);
            editCurrencyNameTextInputEditText.setError(currencyName);

        }

        if (editCurrencySymbolTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;

            String currencySymbol = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_currency_symbol}).getString(0);
            editCurrencySymbolTextInputEditText.setError(currencySymbol);

        }

        return status;

    }

    private void takeValueFromView() {

        currencyName = currencyNameTextInputEditText.getText().toString().trim();

        currencySymbol = currencySymbolTextInputEditText.getText().toString().trim();


    }

    private void takeValueFromEditView() {

        editCurrencyName = editCurrencyNameTextInputEditText.getText().toString().trim();

        editCurrencySymbol = editCurrencySymbolTextInputEditText.getText().toString().trim();


    }

    private void clearInput() {

        currencyNameTextInputEditText.setError(null);

        currencySymbolTextInputEditText.setText(null);

        currencySymbolTextInputEditText.setText(null);

        currencySymbolTextInputEditText.setError(null);

        useDefaultCheckBox.setChecked(false);

    }

    public void refreshCurrencyList() {

        currencyList = currencyManager.getAllCurrencies(0, 50);

        rvSwipeAdapterForCurrencyList.setCurrencyList(currencyList);

        rvSwipeAdapterForCurrencyList.notifyDataSetChanged();

    }

    public void configUI() {


        //currencyList=settingBusiness.getCurrency()

        currencyList = new ArrayList<>();

        rvSwipeAdapterForCurrencyList = new RVSwipeAdapterForCurrencyList(currencyList);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(rvSwipeAdapterForCurrencyList);

    }


    private void buildingAddCurrencyDialog() {

        currencyMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.add_currency, true).negativeText("Cancel").positiveText("Save").title("Add Currency").build();

    }

    private void buildingEditCurrencyDialog() {

        editCurrencyMaterialDialog = new MaterialDialog.Builder(context).customView(R.layout.add_currency, true).negativeText("Cancel").positiveText("Save").title("Edit Currency").build();

    }


    public void loadUI() {

        recyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.currency_list_rv);

        addCurrencyFab = (FloatingActionButton) mainLayoutView.findViewById(R.id.add_new_currency);


        currencyNameTextInputEditText = (EditText) currencyMaterialDialog.findViewById(R.id.currency_name_in_add_currency_et);

        currencySymbolTextInputEditText = (EditText) currencyMaterialDialog.findViewById(R.id.currency_symbol_in_add_currency_et);

        useDefaultCheckBox = (CheckBox) currencyMaterialDialog.findViewById(R.id.use_default_in_add_currency);

        addCurrencyMdButton = currencyMaterialDialog.getActionButton(DialogAction.POSITIVE);


        editCurrencyNameTextInputEditText = (EditText) editCurrencyMaterialDialog.findViewById(R.id.currency_name_in_add_currency_et);

        editCurrencySymbolTextInputEditText = (EditText) editCurrencyMaterialDialog.findViewById(R.id.currency_symbol_in_add_currency_et);

        editUseDefaultCheckBox = (CheckBox) editCurrencyMaterialDialog.findViewById(R.id.use_default_in_add_currency);

        editCurrencyMdButton = editCurrencyMaterialDialog.getActionButton(DialogAction.POSITIVE);

    }

    private void buildDeleteAlertDialog() {

        TypedArray no  = context.getTheme().obtainStyledAttributes(new int[]{R.attr.no});
        TypedArray yes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.yes});

        deleteAlertDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.confirm_dialog, true).typeface("Zawgyi-One.ttf", "Zawgyi-One.ttf")
                //.title("Are you sure  want to delete?")
                // .positiveText(yes.getString(0))
                // .negativeText(no.getString(0))
                .build();


        String   sureDelete = context.getTheme().obtainStyledAttributes(new int[]{R.attr.are_you_sure_want_to_delete}).getString(0);
        TextView textView   = (TextView) deleteAlertDialog.findViewById(R.id.title);
        textView.setText(sureDelete);

        yesDeleteMdButton = (Button) deleteAlertDialog.findViewById(R.id.save);

        deleteAlertDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertDialog.dismiss();
            }
        });

    }


}