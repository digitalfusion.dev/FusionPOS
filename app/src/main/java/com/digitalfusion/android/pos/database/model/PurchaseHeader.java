package com.digitalfusion.android.pos.database.model;

import com.digitalfusion.android.pos.util.POSUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created by MD002 on 9/5/16.
 */
public class PurchaseHeader implements Serializable {
    private Long id;
    private Long supplierID;
    private String supplierName;
    private String supplierPhone;
    private String supplierAddress;
    private String saleDate;

    public String getSupplierPhone() {
        return supplierPhone;
    }

    public void setSupplierPhone(String supplierPhone) {
        this.supplierPhone = supplierPhone;
    }

    public String getSupplierAddress() {
        return supplierAddress;
    }

    public void setSupplierAddress(String supplierAddress) {
        this.supplierAddress = supplierAddress;
    }

    private String taxName;
    private Long taxID;
    private double taxRate;
    private String discount;
    private String purchaseVocherNo;
    private Double subtotal;
    private Double total;
    private Double discountAmt;
    private String discountPercent;
    private Double taxAmt;
    private Double paidAmt;
    private Double balance;
    private String remark;
    private String day;
    private String month;
    private String year;
    private String taxType;
    private String headerText;
    private String footerText;
    private Long discountID;
    private int isHold;
    private Boolean isPurchaseEdit;
    private Boolean isPurchaseHold;
    private List<SalesAndPurchaseItem> salesAndPurchaseItemList;


    private List<Long> deleteList;


    public PurchaseHeader() {
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public Long getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(Long supplierID) {
        this.supplierID = supplierID;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public Long getTaxID() {
        return taxID;
    }

    public void setTaxID(Long taxID) {
        this.taxID = taxID;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPurchaseVocherNo() {
        return purchaseVocherNo;
    }

    public void setPurchaseVocherNo(String purchaseVocherNo) {
        this.purchaseVocherNo = purchaseVocherNo;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(Double discountAmt) {
        this.discountAmt = discountAmt;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Double getTaxAmt() {
        return taxAmt;
    }

    public void setTaxAmt(Double taxAmt) {
        this.taxAmt = taxAmt;
    }

    public Double getPaidAmt() {
        return paidAmt;
    }

    public void setPaidAmt(Double paidAmt) {
        this.paidAmt = paidAmt;
    }

    public String getPaidAmtString() {
        return POSUtil.NumberFormat(paidAmt);
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public String getFooterText() {
        return footerText;
    }

    public void setFooterText(String footerText) {
        this.footerText = footerText;
    }

    public Long getDiscountID() {
        return discountID;
    }

    public void setDiscountID(Long discountID) {
        this.discountID = discountID;
    }

    public int getIsHold() {
        return isHold;
    }

    public void setIsHold(int isHold) {
        this.isHold = isHold;
    }

    public boolean isHold() {
        return isHold == 1;
    }

    public List<SalesAndPurchaseItem> getSalesAndPurchaseItemList() {
        return salesAndPurchaseItemList;
    }

    public void setSalesAndPurchaseItemList(List<SalesAndPurchaseItem> salesAndPurchaseItemList) {
        this.salesAndPurchaseItemList = salesAndPurchaseItemList;
    }
    public Boolean getPurchaseEdit() {
        return isPurchaseEdit;
    }

    public void setPurchaseEdit(Boolean purchaseEdit) {
        isPurchaseEdit = purchaseEdit;
    }

    public Boolean getPurchaseHold() {
        return isPurchaseHold;
    }

    public void setPurchaseHold(Boolean purchaseHold) {
        isPurchaseHold = purchaseHold;
    }

    public List<Long> getDeleteList() {
        return deleteList;
    }

    public void setDeleteList(List<Long> deleteList) {
        this.deleteList = deleteList;
    }

}
