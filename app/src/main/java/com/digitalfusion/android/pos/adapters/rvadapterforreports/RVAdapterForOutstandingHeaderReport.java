package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;

import java.util.List;

/**
 * Created by MD002 on 1/2/17.
 */

public class RVAdapterForOutstandingHeaderReport extends ParentRVAdapterForReports {
    private static final int VIEWTYPE_ITEM = 2;
    private static final int VIEWTYPE_LOADER = 3;
    private final int HEADER_TYPE = 10000;
    protected boolean showLoader = false;
    private List<ReportItem> reportItemList;
    private int viewType;
    private boolean isPayable;
    private ClickListener rvAdapterForOutstandingHeaderClickListener;

    /**
     * 1 for stock, category, amount, qty, unit
     * 2 for category, amount
     */
    public RVAdapterForOutstandingHeaderReport(List<ReportItem> reportItemList) {
        super();
        this.reportItemList = reportItemList;
        Log.w("hello", reportItemList.size() + " ss");
    }

    public void setPayable(boolean payable) {
        isPayable = payable;
    }

    public void setRvAdapterForOutstandingHeaderClickListener(ClickListener rvAdapterForOutstandingHeaderClickListener) {
        this.rvAdapterForOutstandingHeaderClickListener = rvAdapterForOutstandingHeaderClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_receivable_payable_header_header_view, parent, false);

            return new HeaderViewHolder(v);
        } else if (viewType == VIEWTYPE_LOADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);
            return new LoaderViewHolder(v);
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_receivable_payable_header_item_view, parent, false);

        return new PayableReceivableHeaderViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        if (viewHolder instanceof LoaderViewHolder) {

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);

        } else if (viewHolder instanceof PayableReceivableHeaderViewHolder && !reportItemList.isEmpty()) {
            PayableReceivableHeaderViewHolder payableReceivableHeaderViewHolder = (PayableReceivableHeaderViewHolder) viewHolder;
            payableReceivableHeaderViewHolder.nameTextView.setText(reportItemList.get(position - 1).getName());
            payableReceivableHeaderViewHolder.voucherDateTextView.setText(reportItemList.get(position - 1).getDate());
            if (reportItemList.get(position - 1).getDate1().equalsIgnoreCase("")) {
                payableReceivableHeaderViewHolder.outstaindingLayout.setVisibility(View.GONE);
            } else {
                payableReceivableHeaderViewHolder.outstaindingLayout.setVisibility(View.VISIBLE);
                payableReceivableHeaderViewHolder.paymentDateTextView.setText(reportItemList.get(position - 1).getDate1());
            }
            if (isPayable) {
                payableReceivableHeaderViewHolder.voucherDateLabelTextView.setText(payableReceivableHeaderViewHolder.lastPurchaseOn);
            } else {
                payableReceivableHeaderViewHolder.voucherDateLabelTextView.setText(payableReceivableHeaderViewHolder.lastSaleOn);
            }
            payableReceivableHeaderViewHolder.balanceTextView.setText(POSUtil.NumberFormat(reportItemList.get(position - 1).getBalance()));
            payableReceivableHeaderViewHolder.totalAmtTextView.setText(POSUtil.NumberFormat(reportItemList.get(position - 1).getTotalAmt()));

            payableReceivableHeaderViewHolder.itemView.setOnClickListener(v -> {
                if (rvAdapterForOutstandingHeaderClickListener != null) {
                    rvAdapterForOutstandingHeaderClickListener.onClick(reportItemList.get(position - 1));
                }
            });
        } else if (viewHolder instanceof HeaderViewHolder && reportItemList.isEmpty()) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            headerViewHolder.itemViewLayout.setVisibility(View.GONE);
            headerViewHolder.line.setVisibility(View.GONE);
            headerViewHolder.noDataTextView.setVisibility(View.VISIBLE);
            headerViewHolder.currencyLayout.setVisibility(View.GONE);
        } else {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            headerViewHolder.noDataTextView.setVisibility(View.GONE);
            headerViewHolder.itemViewLayout.setVisibility(View.VISIBLE);
            headerViewHolder.line.setVisibility(View.VISIBLE);
            headerViewHolder.currencyLayout.setVisibility(View.VISIBLE);
            if (isPayable) {
                headerViewHolder.nameTextView.setText(headerViewHolder.supplier);
                headerViewHolder.balanceTextView.setText(headerViewHolder.payableBal);
                headerViewHolder.totalAmtTextView.setText(headerViewHolder.purchaseTotal);
            } else {
                headerViewHolder.totalAmtTextView.setText(headerViewHolder.saleTotal);
            }
        }
    }

    @Override
    public int getItemCount() {

        return reportItemList.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position != 0 && position == getItemCount() - 1) {

            if (reportItemList != null) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;
            }


        }
        if (position == 0) {
            return HEADER_TYPE;
        } else {
            return viewType;
        }
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public interface ClickListener {
        void onClick(ReportItem reportItem);
    }

    public class PayableReceivableHeaderViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView;
        TextView paymentDateTextView;
        TextView voucherDateTextView;
        TextView balanceTextView;
        TextView voucherDateLabelTextView;
        TextView totalAmtTextView;
        LinearLayout outstaindingLayout;

        String lastSaleOn;
        String lastPurchaseOn;

        public PayableReceivableHeaderViewHolder(View itemView) {
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            paymentDateTextView = (TextView) itemView.findViewById(R.id.paid_text_view);
            voucherDateTextView = (TextView) itemView.findViewById(R.id.voucher_date_text_view);
            balanceTextView = (TextView) itemView.findViewById(R.id.receivable_bal_text_view);
            voucherDateLabelTextView = (TextView) itemView.findViewById(R.id.voucher_date_label_text_view);
            totalAmtTextView = (TextView) itemView.findViewById(R.id.sales_total_text_view);
            outstaindingLayout = (LinearLayout) itemView.findViewById(R.id.outstanding_layout);

            lastSaleOn = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.last_sale_on}).getString(0);
            lastPurchaseOn = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.last_purchase_on}).getString(0);

        }
    }


    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        LinearLayout itemViewLayout;
        TextView noDataTextView;
        View line;
        TextView nameTextView;
        TextView balanceTextView;
        TextView totalAmtTextView;
        LinearLayout currencyLayout;

        String supplier;
        String payableBal;
        String purchaseTotal;
        String saleTotal;

        public HeaderViewHolder(View itemView) {
            super(itemView);

            itemViewLayout = (LinearLayout) itemView.findViewById(R.id.item_view_layout);
            noDataTextView = (TextView) itemView.findViewById(R.id.no_data_text_view);
            line = itemView.findViewById(R.id.line);
            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            balanceTextView = (TextView) itemView.findViewById(R.id.bal_text_view);
            currencyLayout = (LinearLayout) itemView.findViewById(R.id.currency_layout);
            totalAmtTextView = (TextView) itemView.findViewById(R.id.sales_total_text_view);


            supplier = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.supplier}).getString(0);
            payableBal = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.payable}).getString(0);
            purchaseTotal = ThemeUtil.getString(itemView.getContext(), R.attr.purchase_total);
            saleTotal = ThemeUtil.getString(itemView.getContext(), R.attr.sales_total);
        }
    }
}
