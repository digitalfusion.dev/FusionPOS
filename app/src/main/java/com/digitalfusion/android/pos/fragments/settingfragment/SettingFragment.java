package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.activities.DetailActivity;
import com.digitalfusion.android.pos.activities.MainActivity;
import com.digitalfusion.android.pos.adapters.SettingListAdapter;
import com.digitalfusion.android.pos.database.model.SettingList;
import com.digitalfusion.android.pos.fragments.salefragments.SaleListHistoryTabFragment;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;

import java.util.ArrayList;
import java.util.Arrays;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by MD003 on 8/24/16.
 */
public class SettingFragment extends Fragment {

    private Context context;
    private View mainLayoutView;

    private RecyclerView settingRecyclerView;
    private ArrayList<SettingList> settingListViewArrayList;
    private SettingListAdapter settingListAdapter;

    private SettingList companySetting;
    private SettingList documentSetting;
    private SettingList printVoucherSetting;
    private SettingList dataManagementSetting;
    private SettingList userRoleSetting;
    private SettingList taxtSetting;
    private SettingList currencySetting;
    private SettingList customerSetting;
    private SettingList supplierSetting;
    private SettingList unitSetting;
    private SettingList categorySetting;
    private SettingList themeSetting;
    private SettingList nightMode;
    private SettingList myanmarSetting;
    private SettingList discountSetting;
    private SettingList controlServerSetting;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.setting, null);

        context = getContext();

        settingRecyclerView = (RecyclerView) mainLayoutView.findViewById(R.id.setting_rv);

        String setting = context.getTheme().obtainStyledAttributes(new int[]{R.attr.setting}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(setting);

        configRecyclerView();
        MainActivity.setCurrentFragment(this);
        settingListAdapter.setClickListener(postion -> {

            if (settingListViewArrayList.get(postion).equals(companySetting)) {
                // MainActivity.replaceFragment(new BussinessSettingFragment());
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.BUSSINESS_SETTING);
                startActivity(detailIntent);
            } else if (settingListViewArrayList.get(postion).equals(documentSetting)) {
                //                    MainActivity.replaceFragment(new DocumentSettingFragment());
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.DOCUMENT_SETTING);
                startActivity(detailIntent);
            } else if (settingListViewArrayList.get(postion).equals(printVoucherSetting)) {
                //                    MainActivity.replaceFragment(new DocumentSettingFragment());
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.PRINT_VOUCHER_SETTING);
                startActivity(detailIntent);
            } else if (settingListViewArrayList.get(postion).equals(dataManagementSetting)) {
                //MainActivity.replaceFragment(new DataManagementFragment());
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.DATA_MANAGEMENT_SETTING);
                startActivity(detailIntent);
            } else if (settingListViewArrayList.get(postion).equals(userRoleSetting)) {
                // MainActivity.replaceFragment(new UserRoleSettingFragment());
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.USER_SETTING);
                startActivity(detailIntent);
            } else if (settingListViewArrayList.get(postion).equals(taxtSetting)) {
                //  MainActivity.replaceFragment(new TaxSetupFragment());
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.TAX_SETUP);
                startActivity(detailIntent);
            } else if (settingListViewArrayList.get(postion).equals(customerSetting)) {
                // MainActivity.replaceFragment(new CustomerSupplierTabFragment());
                //                  Intent detailIntent=new Intent(context, DetailActivity.class);
                //                    detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.CU);
                //   startActivity(detailIntent);

            } else if (settingListViewArrayList.get(postion).equals(supplierSetting)) {
                //    MainActivity.replaceFragment(new SupplierListFragment());

            } else if (settingListViewArrayList.get(postion).equals(categorySetting)) {
                //  MainActivity.replaceFragment(new CategoryFragmentDialog());
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.CATEGORY_SETTING);
                startActivity(detailIntent);
            } else if (settingListViewArrayList.get(postion).equals(unitSetting)) {
                // MainActivity.replaceFragment(new UnitFragment());
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.UNIT_SETTING);
                startActivity(detailIntent);
            } else if (settingListViewArrayList.get(postion).equals(themeSetting)) {
                // MainActivity.replaceFragment(new ThemeSettingListViewFragment());
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.THEME_SETTING);
                startActivity(detailIntent);
            } else if (settingListViewArrayList.get(postion).equals(currencySetting)) {
                // MainActivity.replaceFragment(new CurrencyFragment());
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.CURRENCY_SETTING);
                startActivity(detailIntent);
            } else if (settingListViewArrayList.get(postion).equals(myanmarSetting)) {
                // MainActivity.replaceFragment(new CurrencyFragment());
                ///Toast.makeText(getContext(), "Not Support Yet", Toast.LENGTH_SHORT).show();
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.MYANMAR_FONT_SETTING);
                startActivity(detailIntent);
            } else if (settingListViewArrayList.get(postion).equals(discountSetting)) {
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.DISCOUNT_SETUP);
                startActivity(detailIntent);
            } else if (settingListViewArrayList.get(postion).equals(controlServerSetting)) {
                Intent detailIntent = new Intent(context, DetailActivity.class);
                detailIntent.putExtra(DetailActivity.TYPE_KEY, DetailActivity.DETAILS_TYPE.CONTROL_SERVER);
                startActivity(detailIntent);
            }
        });

        settingListAdapter.setLanguageViewOnCheckChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.THEME, MODE_PRIVATE).edit();
                editor.putBoolean(AppConstant.IS_MYANMAR, false);
                editor.commit();
                MainActivity.setCurrentFragment(new SaleListHistoryTabFragment());

                Intent mainActivity = new Intent(getActivity(), MainActivity.class);
                mainActivity.putExtra("navi", true);
                startActivity(mainActivity);

                getActivity().finish();
            } else {
                SharedPreferences.Editor editor = context.getSharedPreferences(AppConstant.THEME, MODE_PRIVATE).edit();
                editor.putBoolean(AppConstant.IS_MYANMAR, true);
                editor.commit();

                MainActivity.setCurrentFragment(new SaleListHistoryTabFragment());
                Intent mainActivity = new Intent(getActivity(), MainActivity.class);
                mainActivity.putExtra("navi", true);
                startActivity(mainActivity);

                getActivity().finish();
            }
        });

        return mainLayoutView;
    }

    private void configRecyclerView() {

        Drawable companyIcon            = ThemeUtil.getDrawable(context, R.attr.ic_business);
        Drawable documentIcon           = ThemeUtil.getDrawable(context, R.attr.ic_document);
        Drawable printVoucherIcon       = ThemeUtil.getDrawable(context, R.attr.ic_print_voucher);
        Drawable currencyIcon           = ThemeUtil.getDrawable(context, R.attr.ic_currency);
        Drawable taxIcon                = ThemeUtil.getDrawable(context, R.attr.ic_tax);
        Drawable userRoleIcon           = ThemeUtil.getDrawable(context, R.attr.ic_user);
        Drawable customerIcon           = ThemeUtil.getDrawable(context, R.attr.ic_user);
        Drawable supplierSettingIcon    = ThemeUtil.getDrawable(context, R.attr.ic_user);
        Drawable unitIcon               = ThemeUtil.getDrawable(context, R.attr.ic_unit);
        Drawable categoryIcon           = ThemeUtil.getDrawable(context, R.attr.ic_category);
        Drawable dataManagementIcon     = ThemeUtil.getDrawable(context, R.attr.ic_data);
        Drawable themeIcon              = ThemeUtil.getDrawable(context, R.attr.ic_theme);
        Drawable myanmarFontSettingIcon = ThemeUtil.getDrawable(context, R.attr.ic_myanmar_font_setting);
        Drawable discountIcon           = ThemeUtil.getDrawable(context, R.attr.disc);
        Drawable controlServerIcon      = ThemeUtil.getDrawable(context, R.attr.ic_control_server);

        String businessSetting_str     = ThemeUtil.getString(context, R.attr.business_setting);
        String documentSetting_str     = ThemeUtil.getString(context, R.attr.document_setting);
        String printVoucherSetting_str = ThemeUtil.getString(context, R.attr.print_voucher_setting);
        String currency_str            = ThemeUtil.getString(context, R.attr.currency);
        String tax_str                 = ThemeUtil.getString(context, R.attr.tax_setup);
        String user_str                = ThemeUtil.getString(context, R.attr.user_role);
        String unit_str                = ThemeUtil.getString(context, R.attr.unit);
        String category_str            = ThemeUtil.getString(context, R.attr.category);
        String dataManagement_str      = ThemeUtil.getString(context, R.attr.data_management);
        String theme_str               = ThemeUtil.getString(context, R.attr.theme);
        String myanmarFont             = ThemeUtil.getString(context, R.attr.myanmar_font_setting);
        String discount_str            = ThemeUtil.getString(context, R.attr.discount);
        String controlServerStr        = ThemeUtil.getString(context, R.attr.str_control_server);

        companySetting = new SettingList(companyIcon, businessSetting_str);
        documentSetting = new SettingList(documentIcon, documentSetting_str);
        printVoucherSetting = new SettingList(printVoucherIcon, printVoucherSetting_str);
        currencySetting = new SettingList(currencyIcon, currency_str);
        taxtSetting = new SettingList(taxIcon, tax_str);
        userRoleSetting = new SettingList(userRoleIcon, user_str);
        myanmarSetting = new SettingList(myanmarFontSettingIcon, myanmarFont);
        //customerSetting = new SettingList(R.drawable.test2_customer, "Customer/Supplier");
        //supplierSetting = new SettingList(R.drawable.test2_supplier, "Supplier");
        unitSetting = new SettingList(unitIcon, unit_str);
        categorySetting = new SettingList(categoryIcon, category_str);
        dataManagementSetting = new SettingList(dataManagementIcon, dataManagement_str);
        themeSetting = new SettingList(themeIcon, theme_str);
        // nightMode=new SettingList(.getDrawable(0),"Night Mode");
        discountSetting = new SettingList(discountIcon, discount_str);
        controlServerSetting = new SettingList(controlServerIcon, controlServerStr);

        // TODO: don't forgot to remove ControlServer
        settingListViewArrayList = new ArrayList<>(Arrays.asList(companySetting, userRoleSetting, controlServerSetting,
                documentSetting, printVoucherSetting, myanmarSetting,
                discountSetting, currencySetting, taxtSetting, unitSetting, categorySetting,
                dataManagementSetting, themeSetting));
        settingRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        settingListAdapter = new SettingListAdapter(settingListViewArrayList);
        settingRecyclerView.setAdapter(settingListAdapter);
    }
}
