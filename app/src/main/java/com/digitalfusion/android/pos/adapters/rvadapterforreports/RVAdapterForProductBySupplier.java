package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD002 on 1/6/17.
 */

public class RVAdapterForProductBySupplier extends ParentRVAdapterForReports {

    private final int HEADER_TYPE = 10000;
    protected boolean showLoader = false;
    private List<ReportItem> reportItemList;

    /**
     * 1 for stock, category, amount, qty, unit
     * 2 for category, amount
     */
    public RVAdapterForProductBySupplier(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
        Log.w("hello", reportItemList.size() + " ss");
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_product_by_supplier_header_view, parent, false);

            return new HeaderViewHolder(v);
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_by_supplier_item_view, parent, false);

        return new SupplierProductViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof SupplierProductViewHolder) {
            SupplierProductViewHolder supplierProductViewHolder = (SupplierProductViewHolder) viewHolder;
            supplierProductViewHolder.supplierNameTextView.setText(reportItemList.get(position - 1).getName());
            supplierProductViewHolder.businessNameTextView.setText(reportItemList.get(position - 1).getName1());
            supplierProductViewHolder.totalAmtTextView.setText(POSUtil.NumberFormat(reportItemList.get(position - 1).getBalance()));
            supplierProductViewHolder.dateTextView.setText(reportItemList.get(position - 1).getDate());
        }

    }

    @Override
    public int getItemCount() {
        return reportItemList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_TYPE;
        } else {
            return 1;
        }
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }


    public class SupplierProductViewHolder extends RecyclerView.ViewHolder {

        TextView supplierNameTextView;

        TextView businessNameTextView;

        TextView totalAmtTextView;
        TextView dateTextView;

        public SupplierProductViewHolder(View itemView) {
            super(itemView);

            supplierNameTextView = (TextView) itemView.findViewById(R.id.supplier_name_text_view);
            businessNameTextView = (TextView) itemView.findViewById(R.id.business_name_text_view);
            totalAmtTextView = (TextView) itemView.findViewById(R.id.total_amt_text_view);
            dateTextView = (TextView) itemView.findViewById(R.id.date_text_view);

        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }
}
