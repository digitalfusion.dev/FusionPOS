package com.digitalfusion.android.pos.database.model;

/**
 * Created by MD002 on 8/19/16.
 */
public class Menu {
    private Long id;
    private String name;

    public Menu() {
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
