package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;

import java.util.List;

/**
 * Created by MD001 on 11/8/16.
 */

public class RVAdapterForReportReportsList extends RecyclerView.Adapter<RVAdapterForReportReportsList.ReportsListViewHolder> {

    private int reportsImage;
    private List<String> reportsNameList;
    private List<String> reportsDescriptionList;

    private OnItemClickListener mItemClickListener;

    public RVAdapterForReportReportsList(int reportsImage, List<String> reportsNameList, List<String> reportsDescriptionList) {
        this.reportsImage = reportsImage;
        this.reportsNameList = reportsNameList;
        this.reportsDescriptionList = reportsDescriptionList;
    }

    @Override
    public ReportsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_reports_list_item_view, parent, false);
        return new ReportsListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ReportsListViewHolder holder, final int position) {

        holder.reportImage.setBackgroundResource(reportsImage);

        holder.reportName.setText(reportsNameList.get(position).toString());

        holder.reportDescription.setText(reportsDescriptionList.get(position).toString());

        holder.reportDescription.setVisibility(View.GONE);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(v, position);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return reportsNameList.size();
    }

    public List<String> getReportsNameList() {
        return reportsNameList;
    }

    public void setReportsNameList(List<String> reportsNameList) {
        this.reportsNameList = reportsNameList;
    }

    public OnItemClickListener getmItemClickListener() {
        return mItemClickListener;
    }

    public void setmItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ReportsListViewHolder extends RecyclerView.ViewHolder {

        ImageView reportImage;

        TextView reportName;

        TextView reportDescription;

        View view;

        public ReportsListViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;

            reportImage = (ImageView) itemView.findViewById(R.id.report_image);

            reportName = (TextView) itemView.findViewById(R.id.report_name);

            reportDescription = (TextView) itemView.findViewById(R.id.report_description);
        }
    }
}
