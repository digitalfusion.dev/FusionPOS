package com.digitalfusion.android.pos.util;

import android.content.Context;

import com.digitalfusion.android.pos.database.dao.DocumentNumberSettingDAO;
import com.digitalfusion.android.pos.database.dao.MenuDAO;
import com.digitalfusion.android.pos.database.model.Menu;
import com.digitalfusion.android.pos.database.model.DocumentNumberSetting;

/**
 * Created by MD002 on 8/19/16.
 */
public class InvoiceNoGenerator {
    private DocumentNumberSettingDAO documentNumberSettingDAO;
    private MenuDAO menuDAO;
    private DocumentNumberSetting documentNumberSetting;
    private Menu menu;
    private Context context;

    public InvoiceNoGenerator(Context context) {
        this.context = context;
        documentNumberSettingDAO = DocumentNumberSettingDAO.getDocumentNumberSettingDaoInstance(context);
        menuDAO = MenuDAO.getMenuDaoInstance(context);
        documentNumberSetting = new DocumentNumberSetting();
        menu = new Menu();
    }

    public String getInvoiceNo(String menuName) {
        Long menuID = menuDAO.findIdByname(menuName);
        documentNumberSetting = documentNumberSettingDAO.getDocSettingByMenuID(menuID);
        String invoiceNo = "";
        if (documentNumberSetting.getPrefix() != null) {
            invoiceNo += documentNumberSetting.getPrefix();
        }
        invoiceNo += generateInvoiceNo(documentNumberSetting.getNextNumber(), documentNumberSetting.getMindigit());
        if (documentNumberSetting.getSuffix() != null) {
            invoiceNo += documentNumberSetting.getSuffix();
        }
        //Log.e("invno",invoiceNo);
        return invoiceNo;
    }

    public void updateInvoiceNextNum() {
        documentNumberSettingDAO.updateNextNumber(documentNumberSetting.getId(), documentNumberSetting.getNextNumber() + 1);
    }

    private String generateInvoiceNo(int nextNumber, int minDigit) {
        String output    = Integer.toString(nextNumber);
        String newOutput = "";
        if (output.length() == minDigit) {
            newOutput = output;
        } else {
            for (int i = 0; i < minDigit - output.length(); i++) {
                newOutput += "0";
            }
            newOutput += output;
        }
        return newOutput;
    }
}
