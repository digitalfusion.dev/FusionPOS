package com.digitalfusion.android.pos.network;

import com.digitalfusion.android.pos.information.AppUpdateRequest;
import com.digitalfusion.android.pos.information.Feedback;
import com.digitalfusion.android.pos.information.Registration;
import com.digitalfusion.android.pos.information.Subscription;
import com.digitalfusion.android.pos.information.wrapper.CodeResendRequest;
import com.digitalfusion.android.pos.information.wrapper.DeviceRequest;
import com.digitalfusion.android.pos.information.wrapper.SubscriptionWrapper;
import com.digitalfusion.android.pos.information.wrapper.UserWrapper;
import com.digitalfusion.android.pos.information.wrapper.VerifyRequest;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;

/**
 * Created by MD003 on 8/14/17.
 */

public interface ApiRetrofit {

    @POST("customer/register")
    Call<Subscription> register(@Body Subscription subscription);

    @POST("customer/checkDevice")
    Call<Subscription> checkDevice(@Body DeviceRequest deviceRequest);

    @POST("customer/updateAppVersion")
    Call<ResponseBody> upadateVersion(@Body AppUpdateRequest appUpdateRequest);

    @Multipart
    @POST("customer/subscriptions")
    Call<List<Subscription>> findAllSubscriptions(
            @Part("uid") RequestBody uid);

    @Multipart
    @POST("admin/verifyCode")
    Call<ResponseBody> verifyCode(
            @Part("code") RequestBody code, @Part("uid") RequestBody uid);

    @POST("customer/registrationInfo")
    Call<Registration> getRegistrationInfoByUser(@Body UserWrapper userWrapper);

    @POST("customer/subscribe")
    Call<HashMap> registerDevice(@Body Subscription subscription);

    @PUT("customer/verify")
    Call<Subscription> verify(@Body VerifyRequest verifyRequest);

    @PUT("customer/resend")
    Call resendCode(@Body CodeResendRequest codeResendRequest);

    @PUT("customer/extendSubscription")
    Call<Subscription> extendSubscription(@Body SubscriptionWrapper subscriptionWrapper);

    @POST("customer/feedback")
    Call<Feedback> giveFeedback(@Body Feedback feedback);

    @GET("customer/demo")
    Call<UserWrapper> demoCustomer();

    @Multipart
    @POST("customer/feedbackScreenshot")
    Call<ResponseBody> feedbackScreenshot(
            @Part("uid") RequestBody uid,
            @Part MultipartBody.Part file
    );

    @Multipart
    @POST("customer/backupDB")
    Call<ResponseBody> backupDB(@Part("uid") RequestBody uid, @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("customer/restoreDB")
    Call<ResponseBody> restoreDB(@Field("uid") String uid);
}
