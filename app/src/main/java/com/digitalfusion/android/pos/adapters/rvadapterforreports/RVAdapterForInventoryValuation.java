package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.List;

/**
 * Created by MD002 on 1/6/17.
 */

public class RVAdapterForInventoryValuation extends ParentRVAdapterForReports {

    private static final int VIEWTYPE_ITEM = 2;
    private static final int VIEWTYPE_LOADER = 3;
    private final int HEADER_TYPE = 10000;
    private List<ReportItem> reportItemList;

    /**
     * 1 for stock, category, amount, qty, unit
     * 2 for category, amount
     */
    public RVAdapterForInventoryValuation(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
        Log.w("hello", reportItemList.size() + " ss");
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == HEADER_TYPE) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_inventory_valuation_header_view, parent, false);

            return new HeaderViewHolder(v);
        } else if (viewType == VIEWTYPE_LOADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);
            return new LoaderViewHolder(v);
        }
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_valuation_report_item_view, parent, false);

        return new CategoryViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof LoaderViewHolder) {
            Log.e("loader", "loader");

            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
            //            mItemManger.bindView(loaderViewHolder.mView, position);

        } else if (viewHolder instanceof CategoryViewHolder) {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) viewHolder;
            categoryViewHolder.nameTextView.setText(reportItemList.get(position - 1).getName());
            categoryViewHolder.totalAmtTextView.setText(POSUtil.NumberFormat(reportItemList.get(position - 1).getBalance()));
        }
    }

    @Override
    public int getItemCount() {

        return reportItemList.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position != 0 && position == getItemCount() - 1) {

            if (reportItemList != null) {

                return VIEWTYPE_LOADER;

            }

        }
        if (position == 0) {
            return HEADER_TYPE;
        }
        return VIEWTYPE_ITEM;
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView;
        TextView totalAmtTextView;


        View itemView;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            totalAmtTextView = (TextView) itemView.findViewById(R.id.total_amt_text_view);

        }
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView customerNameTextView;
        TextView categoryNameTextView;
        TextView totalAmtTextView;
        TextView qtyTextView;
        TextView unitTextView;
        TextView amountTextView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            customerNameTextView = (TextView) itemView.findViewById(R.id.stock_name_text_view);
            categoryNameTextView = (TextView) itemView.findViewById(R.id.category_name_text_view);
            totalAmtTextView = (TextView) itemView.findViewById(R.id.total_amt_text_view);
            qtyTextView = (TextView) itemView.findViewById(R.id.qty_text_view);
            unitTextView = (TextView) itemView.findViewById(R.id.unit_text_view);
            amountTextView = (TextView) itemView.findViewById(R.id.amount_text_view);
        }
    }


    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }
}
