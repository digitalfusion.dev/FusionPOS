package com.digitalfusion.android.pos.adapters.rvadapterforreports;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.model.ReportItem;

import java.util.List;

/**
 * Created by MD002 on 10/20/17.
 */

public class RVAdapterForProCatBySupCategory extends ParentRVAdapterForReports {
    protected boolean showLoader = false;
    private List<ReportItem> reportItemList;
    private Context context;
    private ReportManager reportManager;
    private String startDate;
    private String endDate;

    /**
     * 1 for stock, category, amount, qty, unit
     * 2 for category, amount
     */
    public RVAdapterForProCatBySupCategory(List<ReportItem> reportItemList, Context context, String startDate, String endDate) {
        super();
        this.context = context;
        this.reportItemList = reportItemList;
        Log.w("hello", reportItemList.size() + " ss");
        reportManager = new ReportManager(context);
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_category_supplier_detail_category_item_view, parent, false);

        return new ItemViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        //Log.e("vl", "daf");
        final ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
        //Log.e("name", reportItemList.get(position).getName());
        itemViewHolder.categoryName.setText(reportItemList.get(position).getName());
        itemViewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        itemViewHolder.recyclerView.setAdapter(new RVAdapterForProCatBySupProduct(context, reportManager.productBySupplierCategory(startDate, endDate, 0, 10,
                reportItemList.get(position).getId1(),
                reportItemList.get(position).getId())));
        itemViewHolder.titleLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (itemViewHolder.detailLinearLayout.isShown()) {
                    itemViewHolder.arrowImageView.setSelected(true);
                    itemViewHolder.detailLinearLayout.setVisibility(View.GONE);

                } else {

                    itemViewHolder.arrowImageView.setSelected(false);
                    itemViewHolder.detailLinearLayout.setVisibility(View.VISIBLE);

                }

            }
        });
    }

    @Override
    public int getItemCount() {

        return reportItemList.size();
    }

    public void setReportItemList(List<ReportItem> reportItemList) {
        this.reportItemList = reportItemList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName;
        RecyclerView recyclerView;
        LinearLayout titleLinearLayout;
        LinearLayout detailLinearLayout;
        ImageView arrowImageView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            categoryName = (TextView) itemView.findViewById(R.id.category_name);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.recycler_view);
            titleLinearLayout = (LinearLayout) itemView.findViewById(R.id.tiltle_linear_layout);
            detailLinearLayout = (LinearLayout) itemView.findViewById(R.id.detail_linear_layout);
            arrowImageView = (ImageView) itemView.findViewById(R.id.title_arrow);
        }
    }
}
