package com.digitalfusion.android.pos.information;

import java.util.Date;

/**
 * Created by MD002 on 8/17/17.
 */

public class Feedback {
    private long id;
    private Date createdDate = new Date();
    private boolean isDeleted = false;
    private String feedbackType;//{ error/feature request/question/customer service }
    private String message;
    private String reportedCustomer;
    private String uid;

    public Feedback() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(String feedbackType) {
        this.feedbackType = feedbackType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReportedCustomer() {
        return reportedCustomer;
    }

    public void setReportedCustomer(String reportedCustomer) {
        this.reportedCustomer = reportedCustomer;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "id=" + id +
                ", createdDate=" + createdDate +
                ", isDeleted=" + isDeleted +
                ", feedbackType='" + feedbackType + '\'' +
                ", message='" + message + '\'' +
                ", reportedCustomer='" + reportedCustomer + '\'' +
                '}';
    }
}
