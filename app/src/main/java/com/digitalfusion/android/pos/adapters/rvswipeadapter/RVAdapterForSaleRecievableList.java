package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.content.res.TypedArray;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.database.model.SalesHistory;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;

import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 10/24/16.
 */

public class RVAdapterForSaleRecievableList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    public int currentDate;
    protected boolean showLoader = false;
    private List<SalesHistory> receivableList;
    private ClickListener moreClickListener;
    private ClickListener deleteClickListener;
    private ClickListener editClickListener;
    private ClickListener viewDetailListener;
    private ClickListener viewPaymentListener;
    private ClickListener addPaymentListener;
    private ClickListener mItemClickLister;
    private LoaderViewHolder loaderViewHolder;

    public RVAdapterForSaleRecievableList(List<SalesHistory> receivableList) {
        Calendar now = Calendar.getInstance();

        this.receivableList = receivableList;

        currentDate = Integer.parseInt(DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(now.get(Calendar.DAY_OF_MONTH))));

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEWTYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.receivable_item_view, parent, false);

            return new RVAdapterForSaleRecievableList.SaleHistoryViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            return new LoaderViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof RVAdapterForSaleRecievableList.SaleHistoryViewHolder) {

            final RVAdapterForSaleRecievableList.SaleHistoryViewHolder viewHolder = (RVAdapterForSaleRecievableList.SaleHistoryViewHolder) holder;

            POSUtil.makeZebraStrip(viewHolder.itemView, position);

            viewHolder.saleIdTextView.setText("#" + receivableList.get(position).getVoucherNo());

            viewHolder.customerTextView.setText(receivableList.get(position).getCustomer());

            viewHolder.totalAmountTextView.setText(POSUtil.NumberFormat(receivableList.get(position).getBalance()));

            String dayDes[] = DateUtility.dayDes(receivableList.get(position).getDate());

            String yearMonthDes = DateUtility.monthYearDes(receivableList.get(position).getDate());

            // viewHolder.dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>"+yearMonthDes));


            viewHolder.addPaymentImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (addPaymentListener != null) {
                        addPaymentListener.onClick(position);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                viewHolder.swipeLayout.close();
                            }
                        }, 500);
                    }
                }
            });

            viewHolder.viewDetailBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewDetailListener != null) {
                        viewHolder.swipeLayout.close();
                        viewDetailListener.onClick(position);
                    }
                }
            });


         /*   viewHolder.moreImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (moreClickListener !=null){

                    }
                    PopupMenu popupMenu=new PopupMenu(v.getContext(),viewHolder.moreImageButton);
                    popupMenu.getMenuInflater().inflate(R.menu.outstanding_popup_menu,popupMenu.getMenu());

                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            if(item.getItemId()==R.id.item_add_payment){

                                if(addPaymentListener!=null){

                                    viewHolder.swipeLayout.close(true);

                                    addPaymentListener.onClick(position);
                                }

                            }else if(item.getItemId()==R.id.item_view_payment){

                                Log.w("click ","catch");

                                if(viewPaymentListener!=null){

                                    viewHolder.swipeLayout.close(true);

                                    viewPaymentListener.onClick(position);
                                }

                            }else if(item.getItemId()==R.id.item_view_detail){

                                Log.w("click ","catch");
                                if(viewDetailListener!=null){

                                    viewHolder.swipeLayout.close(true);

                                    viewDetailListener.onClick(position);
                                }
                            }
                            return false;
                        }
                    });
                    popupMenu.show();
                }
            });//closing the popupmenu click listener*/

            int date = Integer.parseInt(receivableList.get(position).getDate());

            if (date == currentDate) {

                viewHolder.dateTextView.setText(viewHolder.today.getString(0));

            } else if (date == currentDate - 1) {

                viewHolder.dateTextView.setText(viewHolder.yesterday.getString(0));

            } else {

                viewHolder.dateTextView.setText(DateUtility.makeDateFormatWithSlash(receivableList.get(position).getYear(),
                        receivableList.get(position).getMonth(),
                        receivableList.get(position).getDay()));
            }

            viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mItemClickLister != null) {


                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                viewHolder.swipeLayout.close();
                            }
                        }, 500);

                        mItemClickLister.onClick(position);

                    }

                }

            });

            mItemManger.bindView(viewHolder.view, position);

        } else {
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) holder;

            this.loaderViewHolder = loaderViewHolder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public int getItemCount() {
        if (receivableList == null || receivableList.size() == 0) {
            return 0;
        } else {
            return receivableList.size() + 1;
        }

    }


    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            if (receivableList != null && receivableList.size() != 0) {

                return VIEWTYPE_LOADER;

            } else {

                return VIEWTYPE_ITEM;

            }

        }

        return VIEWTYPE_ITEM;
    }

    public void setShowLoader(boolean showLoader) {
        this.showLoader = showLoader;

        if (loaderViewHolder != null) {
            Log.w("hele", "loader full not null");
            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public ClickListener getmItemClickLister() {
        return mItemClickLister;
    }

    public void setmItemClickLister(ClickListener mItemClickLister) {
        this.mItemClickLister = mItemClickLister;
    }

    public List<SalesHistory> getReceivableList() {
        return receivableList;
    }

    public void setReceivableList(List<SalesHistory> receivableList) {
        this.receivableList = receivableList;
    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public ClickListener getViewDetailListener() {
        return viewDetailListener;
    }

    public void setViewDetailListener(ClickListener viewDetailListener) {
        this.viewDetailListener = viewDetailListener;
    }

    public ClickListener getViewPaymentListener() {
        return viewPaymentListener;
    }

    public void setViewPaymentListener(ClickListener viewPaymentListener) {
        this.viewPaymentListener = viewPaymentListener;
    }

    public ClickListener getAddPaymentListener() {
        return addPaymentListener;
    }

    public void setAddPaymentListener(ClickListener addPaymentListener) {
        this.addPaymentListener = addPaymentListener;
    }

    public ClickListener getMoreClickListener() {
        return moreClickListener;
    }

    public void setMoreClickListener(ClickListener moreClickListener) {
        this.moreClickListener = moreClickListener;
    }

    public class SaleHistoryViewHolder extends RecyclerView.ViewHolder {


        TextView saleIdTextView;

        TextView customerTextView;

        TextView dateTextView;

        TextView totalAmountTextView;

        ImageButton addPaymentImageButton;

        SwipeLayout swipeLayout;

        LinearLayout linearLayout;

        TypedArray today;

        TypedArray yesterday;

        ImageButton viewDetailBtn;

        View view;

        public SaleHistoryViewHolder(View itemView) {

            super(itemView);

            this.view = itemView;

            viewDetailBtn = (ImageButton) itemView.findViewById(R.id.view_detail);

            today = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.today});

            yesterday = itemView.getContext().getTheme().obtainStyledAttributes(new int[]{R.attr.yesterday});

            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll);

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);

            saleIdTextView = (TextView) itemView.findViewById(R.id.sale_id);

            customerTextView = (TextView) itemView.findViewById(R.id.customer);

            dateTextView = (TextView) itemView.findViewById(R.id.date);

            totalAmountTextView = (TextView) itemView.findViewById(R.id.balance);

            addPaymentImageButton = (ImageButton) itemView.findViewById(R.id.add_payment_sale_receivable);

        }

    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar mProgressBar;

        public View mView;

        public LoaderViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);

        }
    }
}
