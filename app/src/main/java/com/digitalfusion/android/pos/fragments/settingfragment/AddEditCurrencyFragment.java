package com.digitalfusion.android.pos.fragments.settingfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.CurrencyManager;
import com.digitalfusion.android.pos.database.model.Currency;
import com.digitalfusion.android.pos.util.AppConstant;
import com.digitalfusion.android.pos.views.FusionToast;

/**
 * Created by MD003 on 3/7/17.
 */

public class AddEditCurrencyFragment extends Fragment {


    public static final String KEY = "currency";
    public Currency currency;
    public boolean isEdit = false;
    private EditText currencyNameTextInputEditText;
    private EditText currencySymbolTextInputEditText;
    private CheckBox useDefaultCheckBox;
    private View mainLayoutView;
    private int useDefault = 0;
    private CurrencyManager currencyManager;
    //For value
    private String currencyName;
    private String currencySymbol;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainLayoutView = inflater.inflate(R.layout.add_currency, container, false);

        currencyManager = new CurrencyManager(getContext());

        setHasOptionsMenu(true);

        loadUI();

        context = getContext();
        useDefaultCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    useDefault = 1;
                } else {
                    useDefault = 0;
                }
            }
        });
        if (getArguments().getSerializable(KEY) == null) {
            String newCategory = context.getTheme().obtainStyledAttributes(new int[]{R.attr.add_currency}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(newCategory);
            isEdit = false;
        } else {
            String editCategory = context.getTheme().obtainStyledAttributes(new int[]{R.attr.edit_currency}).getString(0);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(editCategory);
            isEdit = true;
            currency = (Currency) getArguments().get(KEY);
            initializeOldData();
        }

        return mainLayoutView;

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    private void takeValueFromView() {

        currencyName = currencyNameTextInputEditText.getText().toString().trim();

        currencySymbol = currencySymbolTextInputEditText.getText().toString().trim();


    }

    private void initializeOldData() {

        currencyName = currency.getDisplayName();
        currencySymbol = currency.getSign();

        currencyNameTextInputEditText.setText(currency.getDisplayName());
        currencySymbolTextInputEditText.setText(currency.getSign());

        useDefaultCheckBox.setChecked(currency.isActive());
    }

    private void clearData() {
        currencyNameTextInputEditText.setText(null);
        currencyNameTextInputEditText.setError(null);
        currencySymbolTextInputEditText.setText(null);
        currencySymbolTextInputEditText.setError(null);
        useDefaultCheckBox.setChecked(false);
    }

    private boolean checkValidation() {

        boolean status = true;

        if (currencyNameTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;

            String currencyName = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_currency_name}).getString(0);
            currencyNameTextInputEditText.setError(currencyName);

        }

        if (currencySymbolTextInputEditText.getText().toString().trim().length() < 1) {

            status = false;

            String currencySymbol = context.getTheme().obtainStyledAttributes(new int[]{R.attr.please_enter_currency_symbol}).getString(0);
            currencySymbolTextInputEditText.setError(currencySymbol);

        }

        return status;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save) {

            if (!isEdit) {
                saveCurrency();
            } else {

                if (checkValidation()) {
                    takeValueFromView();
                    boolean status = currencyManager.updateCurrency(currencyName, currencySymbol, useDefault, currency.getId());

                    if (status) {
                        getActivity().onBackPressed();

                        if (useDefault == 1) {
                            AppConstant.CURRENCY = currencySymbol;
                        }
                        FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
                    }
                }
            }

        }
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void saveCurrency() {
        if (checkValidation()) {

            takeValueFromView();

            boolean status = currencyManager.addNewCurrency(currencyName, currencySymbol, useDefault);

            if (status) {

                if (useDefault == 1) {
                    AppConstant.CURRENCY = currencySymbol;
                }

                clearData();
                getActivity().onBackPressed();
                FusionToast.toast(getContext(), FusionToast.TOAST_TYPE.SUCCESS);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.stock_from, menu);
      /*  TypedArray save=context.getTheme().obtainStyledAttributes(new int[]{R.attr.save});

        FontTextView fontTextView= (FontTextView) LayoutInflater.from(getContext()).inflate(R.layout.menu_text,null);

        menu.getItem(0).setActionView(fontTextView);

        fontTextView.setText(save.getString(0));


        fontTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menu.getItem(0));
            }
        });
*/


        super.onCreateOptionsMenu(menu, inflater);

    }

    private void loadUI() {
        currencyNameTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.currency_name_in_add_currency_et);

        currencySymbolTextInputEditText = (EditText) mainLayoutView.findViewById(R.id.currency_symbol_in_add_currency_et);

        useDefaultCheckBox = (CheckBox) mainLayoutView.findViewById(R.id.use_default_in_add_currency);

    }

}
