package com.digitalfusion.android.pos.fragments.reportfragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.adapters.rvadapterforreports.RVAdapterForOutstandingReport;
import com.digitalfusion.android.pos.adapters.rvswipeadapter.RVAdapterForStockItemInSaleAndPurchaseDetail;
import com.digitalfusion.android.pos.database.business.ReportManager;
import com.digitalfusion.android.pos.database.business.SalesManager;
import com.digitalfusion.android.pos.database.business.SettingManager;
import com.digitalfusion.android.pos.database.model.BusinessSetting;
import com.digitalfusion.android.pos.database.model.ReportItem;
import com.digitalfusion.android.pos.database.model.SaleDelivery;
import com.digitalfusion.android.pos.database.model.SalesAndPurchaseItem;
import com.digitalfusion.android.pos.database.model.SalesHeader;
import com.digitalfusion.android.pos.util.InsertedBooleanHolder;
import com.digitalfusion.android.pos.util.POSUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OutstandingPaymentDetailFragment extends Fragment implements Serializable {

    private View mainLayout;

    //UI components from main

    private View deliverDashLine;

    private RecyclerView saleItemRecyclerView;

    private TextView saleIdTextView;

    private TextView dateTextView;

    private TextView discountTextView;

    private TextView subtotalTextView;

    private TextView totalTextView;

    private TextView deliveryAddress;

    private LinearLayout deliveryAddressLinearLayout;

    private TextView deliverChargesTextView;

    private TextView customerNameTextView;

    private TextView taxRateTextView;

    private TextView businessNameTextView;

    private TextView addressTextView;

    private TextView phoneTextView;

    private TextView deliveryLabelTextView;

    private TextView initialPaidAmtTextView;

    //Business

    private SalesManager salesManager;

    //values
    private Context context;

    private RVAdapterForStockItemInSaleAndPurchaseDetail rvAdapterForStockItemInSaleAndPurchaseDetail;

    private List<SalesAndPurchaseItem> salesAndPurchaseItemList;

    //Values

    private String saleID;

    private Double totalAmount = 0.0;

    private Double voucherDiscount = 0.0;

    private Double voucherTax = 0.0;

    private Double subtotal = 0.0;

    private String date;

    private ReportItem reportItem;

    private SaleDelivery saleDelivery;

    private SalesHeader salesHeader;

    private Double deliveryCharges = 0.0;

    private BusinessSetting businessSetting;

    private SettingManager settingManager;

    private RVAdapterForOutstandingReport rvAdapterForOutstandingReport;

    private List<ReportItem> reportItemList;
    private List<ReportItem> tempItemViewList;

    private RecyclerView outstandingRecyclerView;

    private ReportManager reportManager;

    private LinearLayout outstandingLinearLayout;

    private TextView receivableAmtTextView;

    private TextView totalPaidTextView;

    private TextView totalReceivableTextView;

    private Double totalPaidAmt;

    // private DeliveryAndPickUpDialogFragment deliveryAndPickUpDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainLayout = inflater.inflate(R.layout.fragment_outstanding_payment_detail, container, false);

        initializeVariables();

        reportItem = (ReportItem) getArguments().getSerializable("saleheader");

        initializeOldData();

        loadUI();

        configUI();

        configRecyclerView();

        updateViewData();

        return mainLayout;
    }

    private void initializeVariables() {
        salesAndPurchaseItemList = new ArrayList<>();

        context = getContext();

        settingManager = new SettingManager(context);

        businessSetting = settingManager.getBusinessSetting();

        String title = context.getTheme().obtainStyledAttributes(new int[]{R.attr.sale_detail}).getString(0);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);

        salesManager = new SalesManager(context);

        reportItemList = new ArrayList<>();

        reportManager = new ReportManager(context);

        totalPaidAmt = 0.0;

    }

    public void initializeOldData() {

        saleID = reportItem.getName();//voucher no

        salesAndPurchaseItemList = salesManager.getSaleDetailsBySalesID(reportItem.getId());

        salesHeader = salesManager.getSalesHeaderView(reportItem.getId());

        voucherDiscount = salesHeader.getDiscountAmt();

        String initial = context.getTheme().obtainStyledAttributes(new int[]{R.attr.initial_paid}).getString(0);
        reportItemList.add(new ReportItem(initial, reportItem.getDate(), reportItem.getTotalAmt()));
        tempItemViewList = reportManager.paymentListForSalesID(reportItem.getId());
        reportItemList.addAll(tempItemViewList);

        if (reportItem.getType().equalsIgnoreCase(SalesManager.SaleType.Delivery.toString())) {

            saleDelivery = salesManager.getDeliveryInfoBySalesID(reportItem.getId(), new InsertedBooleanHolder());

            deliveryCharges = saleDelivery.getCharges();

        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    public void configUI() {

        if (businessSetting.isEmptyBusinessName()) {

            businessNameTextView.setVisibility(View.GONE);

        } else {

            businessNameTextView.setText(businessSetting.getBusinessName());

        }

        if (businessSetting.isEmptyAddress()) {
            Log.w("address", "empty");

            addressTextView.setVisibility(View.GONE);

        } else {

            addressTextView.setText(businessSetting.getAddress());

        }
        if (businessSetting.isEmptyPhoneNo()) {
            Log.w("phone", "empty");
            phoneTextView.setVisibility(View.GONE);

        } else {

            phoneTextView.setText(businessSetting.getPhoneNo());

        }

        saleIdTextView.setText(saleID);

        taxRateTextView.setText(Double.toString(salesHeader.getTaxRate()));

        customerNameTextView.setText(reportItem.getName1());//customer

        if (reportItem.getType().equalsIgnoreCase(SalesManager.SaleType.Delivery.toString())) {

            deliveryAddressLinearLayout.setVisibility(View.VISIBLE);

            deliveryAddress.setText(saleDelivery.getAddress());

            deliveryLabelTextView.setVisibility(View.VISIBLE);

            deliverChargesTextView.setVisibility(View.VISIBLE);


        } else {
            deliveryAddressLinearLayout.setVisibility(View.GONE);
            deliverDashLine.setVisibility(View.GONE);
            deliveryLabelTextView.setVisibility(View.GONE);
            deliverChargesTextView.setVisibility(View.GONE);
        }

        dateTextView.setText(reportItem.getDate());
    }


    public void configRecyclerView() {

        rvAdapterForStockItemInSaleAndPurchaseDetail = new RVAdapterForStockItemInSaleAndPurchaseDetail(salesAndPurchaseItemList);

        saleItemRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        saleItemRecyclerView.setAdapter(rvAdapterForStockItemInSaleAndPurchaseDetail);

        Log.e("si", reportItemList.size() + " da");
        if (!reportItemList.isEmpty()) {
            outstandingRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            outstandingLinearLayout.setVisibility(View.VISIBLE);
            rvAdapterForOutstandingReport = new RVAdapterForOutstandingReport(reportItemList, 3);
            outstandingRecyclerView.setAdapter(rvAdapterForOutstandingReport);
        }

    }

    private void loadUI() {

        deliverDashLine = mainLayout.findViewById(R.id.deliver_dash_line);

        businessNameTextView = (TextView) mainLayout.findViewById(R.id.business_name);

        addressTextView = (TextView) mainLayout.findViewById(R.id.address);

        phoneTextView = (TextView) mainLayout.findViewById(R.id.phone_no);

        customerNameTextView = (TextView) mainLayout.findViewById(R.id.customer_in_sale_detail_tv);

        taxRateTextView = (TextView) mainLayout.findViewById(R.id.tax_rate_in_sale_detail_tv);

        deliverChargesTextView = (TextView) mainLayout.findViewById(R.id.delivery_in_sale_detail_tv);

        deliveryAddress = (TextView) mainLayout.findViewById(R.id.delivery_address_tv);

        deliveryAddressLinearLayout = (LinearLayout) mainLayout.findViewById(R.id.ll_delivery_address);

        deliveryLabelTextView = (TextView) mainLayout.findViewById(R.id.delivery_label_view);

        saleItemRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.sale_item_list_rv);

        saleIdTextView = (TextView) mainLayout.findViewById(R.id.sale_id_in_sale_detail_tv);

        dateTextView = (TextView) mainLayout.findViewById(R.id.sale_date_in_sale_detail_tv);

        discountTextView = (TextView) mainLayout.findViewById(R.id.discount_in_sale_detail_tv);

        subtotalTextView = (TextView) mainLayout.findViewById(R.id.subtotal_in_sale_detail_tv);

        totalTextView = (TextView) mainLayout.findViewById(R.id.total_in_sale_detail_tv);

        initialPaidAmtTextView = (TextView) mainLayout.findViewById(R.id.initial_paid_amt_text_view);

        outstandingRecyclerView = (RecyclerView) mainLayout.findViewById(R.id.outstanding_recycler_view);

        outstandingLinearLayout = (LinearLayout) mainLayout.findViewById(R.id.outstanding_linear_layout);

        receivableAmtTextView = (TextView) mainLayout.findViewById(R.id.receivable_bal_text_view);

        totalPaidTextView = (TextView) mainLayout.findViewById(R.id.total_paid_amt_text_view);

        totalReceivableTextView = (TextView) mainLayout.findViewById(R.id.receivable_bal_text_view);
    }

    private void updateViewData() {

        calculateValues();

        discountTextView.setText(POSUtil.NumberFormat(voucherDiscount));

        subtotalTextView.setText(POSUtil.NumberFormat(subtotal));

        totalTextView.setText(POSUtil.NumberFormat(totalAmount));

        deliverChargesTextView.setText(POSUtil.NumberFormat(deliveryCharges));

        initialPaidAmtTextView.setText(POSUtil.NumberFormat(reportItem.getAmount()));

        totalAmount -= reportItem.getAmount();

        receivableAmtTextView.setText(POSUtil.NumberFormat(totalAmount));

        totalPaidAmt = 0.0;
        for (ReportItem r : reportItemList) {
            totalPaidAmt += r.getTotalAmt();
        }

        totalPaidTextView.setText(POSUtil.NumberFormat(totalPaidAmt));

        totalReceivableTextView.setText(POSUtil.NumberFormat(totalAmount - totalPaidAmt));
    }

    private void calculateValues() {
        subtotal = 0.0;
        for (SalesAndPurchaseItem s : salesAndPurchaseItemList) {
            subtotal += s.getTotalPrice();
        }

        totalAmount = subtotal + voucherTax - voucherDiscount + deliveryCharges;

    }

    public void replacingFragment(Fragment fragment) {
        final FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.replace(R.id.frame_replace, fragment);
        fragmentTransaction.commit();

    }
}
