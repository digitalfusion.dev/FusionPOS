package com.digitalfusion.android.pos.database.model;

import com.digitalfusion.android.pos.util.POSUtil;

import java.io.Serializable;

/**
 * Created by MD002 on 8/24/16.
 */
public class Tax implements Serializable {
    private Long id;
    private String name;
    private Double rate;
    private String type;
    private String description;
    private int isDefault;

    public Tax(Long id, String name, Double rate, String type, String description) {
        this.id = id;
        this.name = name;
        this.rate = rate;
        this.type = type;
        this.description = description;
    }

    public Tax() {
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getRate() {
        if (rate == null) {
            return 0.0;
        }
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public String getRateString() {
        return POSUtil.doubleToString(rate);
    }

    public String getType() {
        if (type != null)
            return type;
        return "";
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }

    public boolean isActive() {
        return isDefault == 1;
    }


    public enum TaxType {
        Inclusive, Exclusive
    }
}
