package com.digitalfusion.android.pos.adapters.rvswipeadapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.business.AccessLogManager;
import com.digitalfusion.android.pos.database.business.AuthorizationManager;
import com.digitalfusion.android.pos.database.model.LostItem;
import com.digitalfusion.android.pos.database.model.User;
import com.digitalfusion.android.pos.interfaces.ClickListener;
import com.digitalfusion.android.pos.util.DateUtility;
import com.digitalfusion.android.pos.util.POSUtil;
import com.digitalfusion.android.pos.util.ThemeUtil;

import java.util.Calendar;
import java.util.List;

/**
 * Created by MD003 on 8/19/16.
 */
public class RVSwipeAdapterForLostItemList extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;
    public int currentDate;
    protected boolean showLoader = false;
    List<LostItem> lostItemViewList;
    private ClickListener editClickListener;
    private ClickListener deleteClickListener;
    private LoaderViewHolder loaderViewHolder;
    private ClickListener viewDetailClickListener;

    public RVSwipeAdapterForLostItemList(List<LostItem> lostItemViewList) {

        this.lostItemViewList = lostItemViewList;

        Calendar now = Calendar.getInstance();

        currentDate = Integer.parseInt(DateUtility.makeDate(Integer.toString(now.get(Calendar.YEAR)), Integer.toString(now.get(Calendar.MONTH) + 1), Integer.toString(now.get(Calendar.DAY_OF_MONTH))));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEWTYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lost_item_view_in_details_swipe, parent, false);
            return new LostItemViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.loader_item_layout, parent, false);

            return new LoaderViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        if (viewHolder instanceof LostItemViewHolder) {

            final LostItemViewHolder holder = (LostItemViewHolder) viewHolder;

            POSUtil.makeZebraStrip(holder.itemView, position);

            holder.nameTextView.setText(lostItemViewList.get(position).getStockName());

            holder.stockCodeTextView.setText(lostItemViewList.get(position).getStockCodeNo());

            String unit = "";

            if (lostItemViewList.get(position).getStockUnit() != null && lostItemViewList.get(position).getStockUnit().length() > 0) {
                unit = " " + lostItemViewList.get(position).getStockUnit();
            }

            holder.qtyTextView.setText(POSUtil.NumberFormat(lostItemViewList.get(position).getStockQty()) + unit);

            if (lostItemViewList.get(position).getValue() != null) {
                holder.purchasePriceTextView.setText(POSUtil.NumberFormat(lostItemViewList.get(position).getValue()));
            } else {
                holder.purchasePriceTextView.setText(POSUtil.NumberFormat(0));
            }

            if (lostItemViewList.get(position).getTotalValue() != null) {
                holder.totalValueTextView.setText(POSUtil.NumberFormat(lostItemViewList.get(position).getTotalValue()));
            }

            holder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editClickListener != null) {
                        holder.swipeLayout.close();
                        editClickListener.onClick(position);
                    }
                }
            });

            int date = Integer.parseInt(lostItemViewList.get(position).getDate());

            if (date == currentDate) {
                holder.dateTextView.setText(holder.today);
            } else if (date == currentDate - 1) {
                holder.dateTextView.setText(holder.yesterday);
            } else {
                //  viewHolder.dateTextView.setText(Html.fromHtml(dayDes[1] + "<sup><small>" + dayDes[0] + "</small></sup>" + yearMonthDes));

                holder.dateTextView.setText(DateUtility.makeDateFormatWithSlash(lostItemViewList.get(position).getYear(),
                        lostItemViewList.get(position).getMonth(),
                        lostItemViewList.get(position).getDay()));
            }

            if (lostItemViewList.get(position).getRemark() != null && !lostItemViewList.get(position).getRemark().equalsIgnoreCase("") && lostItemViewList.get(position).getRemark().trim().length() > 0) {

                holder.remarkTextView.setText(lostItemViewList.get(position).getRemark().toString());
            } else {

                holder.remarkTextView.setText(null);

                holder.remarkTextView.setHint("No Remark...");
            }

            holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (deleteClickListener != null) {
                        holder.swipeLayout.close();
                        deleteClickListener.onClick(position);
                    }
                }
            });

            holder.viewDetailBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewDetailClickListener != null) {
                        holder.swipeLayout.close();
                        viewDetailClickListener.onClick(position);
                    }
                }
            });

            mItemManger.bindView(holder.itemView, position);
        } else {
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;

            this.loaderViewHolder = loaderViewHolder;

            if (showLoader) {
                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    public List<LostItem> getLostItemViewList() {
        return lostItemViewList;
    }

    public void setLostItemViewList(List<LostItem> lostItemViewList) {
        this.lostItemViewList = lostItemViewList;
    }

    public ClickListener getEditClickListener() {
        return editClickListener;
    }

    public void setEditClickListener(ClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public ClickListener getDeleteClickListener() {
        return deleteClickListener;
    }

    public void setDeleteClickListener(ClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    @Override
    public int getItemCount() {

        Log.w("get item count", lostItemViewList.size() + " sss");
        if (lostItemViewList.size() == 0 || lostItemViewList == null) {
            return 0;
        } else {
            return lostItemViewList.size() + 1;
        }
    }

    @Override
    public int getItemViewType(int position) {

        // loader can't be at position 0
        // loader can only be at the last position
        if (position != 0 && position == getItemCount() - 1) {

            if (lostItemViewList != null && lostItemViewList.size() != 0) {

                return VIEWTYPE_LOADER;
            } else {

                return VIEWTYPE_ITEM;
            }
        }

        return VIEWTYPE_ITEM;
    }

    public void setShowLoader(boolean showLoader) {
        this.showLoader = showLoader;

        if (loaderViewHolder != null) {

            if (showLoader) {

                loaderViewHolder.mProgressBar.setVisibility(View.VISIBLE);
            } else {

                loaderViewHolder.mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public void setViewDetailClickListener(ClickListener viewDetailClickListener) {
        this.viewDetailClickListener = viewDetailClickListener;
    }

    public class LostItemViewHolder extends RecyclerView.ViewHolder {

        TextView stockCodeTextView;
        TextView nameTextView;
        TextView qtyTextView;
        TextView purchasePriceTextView;
        TextView totalValueTextView;
        ImageButton deleteButton;
        ImageButton editButton;
        TextView remarkTextView;
        TextView dateTextView;
        String today;
        String yesterday;
        ImageButton viewDetailBtn;
        SwipeLayout swipeLayout;

        public LostItemViewHolder(View itemView) {
            super(itemView);
            Context context = itemView.getContext();

            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            viewDetailBtn = (ImageButton) itemView.findViewById(R.id.view_detail);
            today = ThemeUtil.getString(context, R.attr.today);
            yesterday = ThemeUtil.getString(context, R.attr.yesterday);
            dateTextView = (TextView) itemView.findViewById(R.id.date);
            remarkTextView = (TextView) itemView.findViewById(R.id.remark_tv);
            stockCodeTextView = (TextView) itemView.findViewById(R.id.stock_code_in_lost_stock_item_view);
            nameTextView = (TextView) itemView.findViewById(R.id.name_in_lost_stock_item_view);
            qtyTextView = (TextView) itemView.findViewById(R.id.qty_in_lost_stock_item_view);
            purchasePriceTextView = (TextView) itemView.findViewById(R.id.purchase_price_in_lost_stock_item_view);
            totalValueTextView = (TextView) itemView.findViewById(R.id.total_value_in_lost_stock_item_view);
            deleteButton = (ImageButton) itemView.findViewById(R.id.delete_lost_item);
            editButton = (ImageButton) itemView.findViewById(R.id.edit_lost_item);

            AccessLogManager     accessLogManager     = new AccessLogManager(context);
            AuthorizationManager authorizationManager = new AuthorizationManager(context);
            Long                 deviceId             = authorizationManager.getDeviceId(Build.SERIAL);
            User                 currentUser          = accessLogManager.getCurrentlyLoggedInUser(deviceId);

            if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Admin.toString())) {
            } else if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Manager.toString())) {
            } else if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Supervisor.toString())) {
                deleteButton.setVisibility(View.GONE);
            } else if (currentUser.getRole().equalsIgnoreCase(User.ROLE.Sale.toString())) {
                editButton.setVisibility(View.GONE);
                deleteButton.setVisibility(View.GONE);
            }
        }
    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar mProgressBar;

        public View mView;

        public LoaderViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);
        }
    }
}