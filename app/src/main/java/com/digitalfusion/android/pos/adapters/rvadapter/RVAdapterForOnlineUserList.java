package com.digitalfusion.android.pos.adapters.rvadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalfusion.android.pos.R;
import com.digitalfusion.android.pos.database.model.User;

import java.util.List;

/**
 * Created by lyx on 5/28/18.
 */
public class RVAdapterForOnlineUserList extends RecyclerView.Adapter<RVAdapterForOnlineUserList.ViewHolder>{
    List<User> userList;

    public RVAdapterForOnlineUserList(List<User> userList) {
        this.userList = userList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_online_users, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User currentUser = userList.get(position);
        holder.userRole.setText(currentUser.getRole());
        holder.userName.setText(currentUser.getUserName());
        holder.deviceNo.setText(currentUser.getDeviceSerialNo());
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void swapData(List<User> newUserList) {
        if (newUserList == null || newUserList.size() == 0) {
            return;
        }
        userList.clear();
        userList.addAll(newUserList);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView userName;
        TextView userRole;
        TextView deviceNo;

        public ViewHolder(View itemView) {
            super(itemView);

            userName = itemView.findViewById(R.id.user_name);
            userRole = itemView.findViewById(R.id.user_role);
            deviceNo = itemView.findViewById(R.id.device_no);
        }
    }
}
