package com.example.imagedialogswipe;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

@SuppressLint("ValidFragment")
public class ImageDialogSwipe extends DialogFragment implements View.OnTouchListener, ViewPager.OnPageChangeListener {

    private View mainLayoutView;

    private Dialog mDialog;

    private ViewPager viewPager;

    private int currentPos;

    private List<byte[]> imagelist;

    private ImageButton leftNavBtn;

    private ImageButton rightNavBtn;

    Handler handler = new Handler();

    private Context context;

    @SuppressLint("ValidFragment")
    public ImageDialogSwipe(List<byte[]> imagelist) {
        this.imagelist = imagelist;
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        Dialog dialog =  super.onCreateDialog(savedInstanceState);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        int width=(int)(getResources().getDisplayMetrics().widthPixels*0.9);

        dialog.getWindow().setLayout(width,width);

        mDialog=dialog;

        return dialog;
    }


    @Override
    public Dialog getDialog() {

        return mDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        int width=(int)(getResources().getDisplayMetrics().widthPixels*0.90);

        getDialog().getWindow().setLayout(width,width);

        context=getContext();

        mainLayoutView=inflater.inflate(R.layout.dialog_layout,null);

        viewPager=(ViewPager)mainLayoutView.findViewById(R.id.viewpager);

        viewPager.setAdapter(new ImagePagerAdapter(getContext(),imagelist));

        viewPager.setLayoutParams(new FrameLayout.LayoutParams(width,width));

        leftNavBtn=(ImageButton)mainLayoutView.findViewById(R.id.left);

        rightNavBtn=(ImageButton)mainLayoutView.findViewById(R.id.right);

        viewPager.setCurrentItem(currentPos);

        viewPager.setOnTouchListener(this);

        leftNavBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentPos=viewPager.getCurrentItem();

                if(currentPos>0){
                    viewPager.setCurrentItem(--currentPos);
                }
            }
        });

        rightNavBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentPos=viewPager.getCurrentItem();

                if(currentPos<imagelist.size()-1){

                    viewPager.setCurrentItem(++currentPos);

                }
            }
        });

        viewPager.addOnPageChangeListener(this);

        decideArrowVisibility();

        return mainLayoutView;

    }

    public void setCurrentPos(int currentPos) {

        if(currentPos>=imagelist.size()){

            throw new ArrayIndexOutOfBoundsException("Current Position can't be greater than Array Size!!");

        }else {

            this.currentPos = currentPos;

        }

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (event.getAction()==MotionEvent.ACTION_DOWN) {

            if (leftNavBtn.getVisibility()==View.VISIBLE||leftNavBtn.getVisibility()==View.VISIBLE) {

                rightNavBtn.setVisibility(View.GONE);

                leftNavBtn.setVisibility(View.GONE);

            }else {

                decideArrowVisibility();

            }
        }

        return false;
    }

    //
    private void decideArrowVisibility() {

        //for left most position
        if(viewPager.getCurrentItem()==0){

            leftNavBtn.setVisibility(View.GONE);

        }
        //For right most position
        if(viewPager.getCurrentItem()==imagelist.size()-1){

            rightNavBtn.setVisibility(View.GONE);

        }

        //For other position
        if(viewPager.getCurrentItem()!=0&&viewPager.getCurrentItem()!=imagelist.size()-1){

            rightNavBtn.setVisibility(View.GONE);

            leftNavBtn.setVisibility(View.GONE);

        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        decideArrowVisibility();

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
