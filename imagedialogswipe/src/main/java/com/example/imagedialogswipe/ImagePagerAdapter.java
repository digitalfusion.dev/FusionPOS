package com.example.imagedialogswipe;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

/**
 * Created by MD003 on 12/7/16.
 */

public class ImagePagerAdapter  extends PagerAdapter {

    Context mContext;

    LayoutInflater mLayoutInflater;

    List<byte[]> imageList;


    public ImagePagerAdapter(Context mContext, List<byte[]> imageList) {

        this.mContext = mContext;

        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.imageList = imageList;

    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ( object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.image_item_view, null, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);

        int width=(int)(itemView.getContext().getResources().getDisplayMetrics().widthPixels*0.90);

        imageView.setLayoutParams(new ViewGroup.LayoutParams(width,width));


        Bitmap resizedBitmap =  Bitmap.createScaledBitmap(Util.getBitmapFromByteArray(imageList.get(position)), width, width, false);

        imageView.setImageBitmap(resizedBitmap);



        Log.w("here in","library");



        container.addView(itemView);

        //  container.setLayoutParams(new LinearLayout.LayoutParams(width, width));

        return itemView;
    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((View) object);

    }
}