package com.example.imagedialogswipe;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.ByteArrayInputStream;

/**
 * Created by MD003 on 12/7/16.
 */

public class Util {
    public static Bitmap getBitmapFromByteArray(@NonNull byte[] img){

        if(img!=null){
            Log.w("image is not"," null");
            ByteArrayInputStream imageStream = new ByteArrayInputStream(img);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);

            return theImage;
        }else {
            return null;
        }


    }
}
