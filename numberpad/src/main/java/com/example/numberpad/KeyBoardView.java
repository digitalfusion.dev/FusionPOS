package com.example.numberpad;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by MD003 on 6/1/17.
 */

public class KeyBoardView extends RelativeLayout{

    private View keyboardView;

    private KeyPressListner keyPressListner;

    public KeyBoardView(Context context) {
        super(context);
        init(context);
    }

    public KeyBoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public KeyBoardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public KeyBoardView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void init(Context context){
        keyboardView= LayoutInflater.from(context).inflate(R.layout.key_board_view,this,true);

        keyboardView.findViewById(R.id.one).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onKeyPress(1);
                }
            }
        });
        keyboardView.findViewById(R.id.two).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onKeyPress(2);
                }
            }
        });
        keyboardView.findViewById(R.id.three).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onKeyPress(3);
                }
            }
        });
        keyboardView.findViewById(R.id.four).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onKeyPress(4);
                }
            }
        });
        keyboardView.findViewById(R.id.five).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onKeyPress(5);
                }
            }
        });
        keyboardView.findViewById(R.id.six).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onKeyPress(6);
                }
            }
        });
        keyboardView.findViewById(R.id.seven).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onKeyPress(7);
                }
            }
        });
        keyboardView.findViewById(R.id.eight).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onKeyPress(8);
                }
            }
        });
        keyboardView.findViewById(R.id.nine).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onKeyPress(9);
                }
            }
        });
        keyboardView.findViewById(R.id.zero).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onKeyPress(0);
                }
            }
        });
        keyboardView.findViewById(R.id.delete).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onDelete();
                }
            }
        });
        keyboardView.findViewById(R.id.clear).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onClear();
                }
            }
        });

        keyboardView.findViewById(R.id.five_hundred).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onAddNum(500);
                }
            }
        });

        keyboardView.findViewById(R.id.one_thousand).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onAddNum(1000);
                }
            }
        });
        keyboardView.findViewById(R.id.five_thousand).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onAddNum(5000);
                }
            }
        });

        keyboardView.findViewById(R.id.ten_thousand).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keyPressListner!=null){
                    keyPressListner.onAddNum(10000);
                }
            }
        });



    }


    public void setKeyPressListner(KeyPressListner keyPressListner) {
        this.keyPressListner = keyPressListner;
    }

    public interface KeyPressListner{
        void onKeyPress(int num);

        void onClear();

        void onDelete();

        void onAddNum(int num);
    }
}
